#pragma once
#define NumOfRTABands 30
class AdaptiveTuning
{
public:
	AdaptiveTuning(void);
	~AdaptiveTuning(void);
	double getRef(double *meterInput,int startIdx,int count);
	void calculateAutomatedGain(double *meterInput,double *targetCurve,double *result, 
								double mu,int iter,bool limit);
private:

	double dBtoLinear(double linVal);
	double LineartodB(double dBVal);
	double errtst[200];
	int m_autoAssistIndex;
};

