#ifndef BASIC_SETTING_WIDGET_H
#define BASIC_SETTING_WIDGET_H

#include <QWidget>
#include <QSlider>
#include <QGroupBox>
#include <QPushButton>
#include <QSpinBox>
#include <QComboBox>
//#include <RadioButton.h>
#include <QButtonGroup>
#include <QFrame>
#include <QLabel>
#include "ButtonSlider.h"
#include <QDoubleSpinBox>
#include "MarkButton.h"
#include "HeadFrame.h"
#include <QRadioButton>
#include <QCheckBox>
#include "CPi2000Setting.h"
#include "VolumeSlider.h"

#define MAX_DISTANCE	100.0
typedef enum
{
	DISTANCE_TYPE_METER,
	DISTANCE_TYPE_FEET,
} DISTANCE_TYPE;



class BasicSettingWidget : public QWidget
{
	Q_OBJECT

public:
	BasicSettingWidget(QWidget *pParent);
	void refresh();
	void refreshMasterVolume();
	void refreshMasterVolumePreset();
	void refreshMasterMuteFlag();
	void refreshLoadDetection();
//	void setPowerOnMode(POWER_ON_MODE mode);
	void setLoadDetectionName(SPEAKER_CHANNEL channel, SPEAKER_TYPE type);
	void changeLoadDetection() { onLoadDetectionChanged(); }
	void retranslateUi();
	void setSyncChecked(bool flag);

private:
	void resizeEvent(QResizeEvent * /* event */);
	void clickFeetButton();
	void clickMeterButton();

private slots:
	void onSDToggled(bool meterFlag);
	void onClickMuteButton();
	void on51ChannelChanged(bool flag);
	void onMasterVolumeSliderChanged(int);
	void onMasterVolumeSpinBoxChanged(double value);
	void onFadeInSpinBoxChanged(double value);
	void onFadeOutSpinBoxChanged(double value);
	void onLengthDistanceSpinBoxChanged(double value);
	void onWidthDistanceSpinBoxChanged(double value);
	void onSurroundDelaySpinBoxChanged(int);
	void onLoadDetectionChanged();
	void onSyncClicked();
	void onClickCalcButton();

#if 0
	void onRS1Clicked();
	void onRS2Clicked();
	void onLeft1Clicked();
	void onLeft2Clicked();
	void onCenter1Clicked();
	void onCenter2Clicked();
	void onRight1Clicked();
	void onBRS1Clicked();
	void onSW1Clicked();
	void onSW2Clicked();
#endif
private:
	QFrame * m_pMasterVolumeFrame;
	HeadFrame *m_pSurroundDelayFrame, *m_pLoadDetectionFrame, *m_pMainOutputFrame;
	DISTANCE_TYPE m_distanceType;

	/* Child Widget of m_pMasterVolumeFrame */
	QLabel *m_pMasterVolumeLabel, *m_pMuteDurationLabel, *m_pFadeInLabel, *m_pFadeOutLabel;
	VolumeSlider *m_pMasterVolume;
	QDoubleSpinBox *m_pMasterVolumeSpinBox;
	QDoubleSpinBox *m_pFadeInSpinBox, *m_pFadeOutSpinBox;
	MarkButton *m_pMuteButton;
	QCheckBox *m_pSyncCheckBox;

	/* Child Widget of m_pSurroundDelayFrame */
	QLabel *m_pTotalDistanceLabel, *m_pAverageDistanceLabel;
	QLabel *m_pMeterOrFeetLabel1, *m_pMeterOrFeetLabel2;
	QDoubleSpinBox *m_pLengthDistanceSpinBox, *m_pWidthDistanceSpinBox;
	QRadioButton *m_pFeetRadioButton, *m_pMeterRadioButton;

	QFrame *m_pHorizonLineTop, *m_pHorizonLineBottom, *m_pVerticalLine; 
	QPushButton *m_pCalcButton;
	QLabel *m_pSurroundExLabel;
	QSpinBox *m_pSurroundDelaySpinBox; 
//	QPushButton *m_pApplyButton;

	/* Child Widget of m_pLoadDetectionFrame */
	QCheckBox *m_pLoadDetectMaskCheck[20];
	QLabel *m_pNoteLabel;

	/* Child Widget of Main Audio Output Setting */
	QRadioButton *m_p51System, *m_p71System;

	/* Child Widget of Power on Mode */
//	QRadioButton *m_pLastLoadRadio, *m_pAnalogRadio, *m_pDigitalRadio, *m_pMicRadio, *m_pMusicRadio;
	bool m_slotEnableFlag;
};


#endif
