// This file was created by computeBesselCoefficients.py              //
//      located in Stockbridge/DSPBlocks/Resources/                   //
// (Bessel filter design documentation is in the same directory.)     //
// It contains coefficients for analog low pass second-order sections //
// of a Bessel Filter. The high pass coefficients can be made by      //
// reversing the order of the LP coefficients and then normalizing    //
// (you don't swap the highest-order coefficient when it is zero).    //
// The gains are only necessary to normalize the low pass response.   //
// The f0 normalization factors are used to align all filters at -3 dB//
////////////////////////////////////////////////////////////////////////

#ifndef __BESSEL_ANALOG_LUT__
#define __BESSEL_ANALOG_LUT__

static const double Bessel1[3] =
{
	0.0f, 1.0f, 1.0f
};

static const double Bessel2[3] =
{
	1.0f, 3.0f, 3.0f
};

static const double Bessel3[2][3] =
{
	{0.0f, 1.0f, 2.32218535463f},
	{1.0f, 3.67781464537f, 6.45943269348f}
};

static const double Bessel4[2][3] =
{
	{1.0f, 5.79242120564f, 9.14013089028f},
	{1.0f, 4.20757879436f, 11.4878004769f}
};

static const double Bessel5[3][3] =
{
	{0.0f, 1.0f, 3.64673859533f},
	{1.0f, 6.70391279831f, 14.2724805133f},
	{1.0f, 4.64934860636f, 18.1563153135f}
};

static const double Bessel6[3][3] =
{
	{1.0f, 8.49671879173f, 18.8011305896f},
	{1.0f, 7.47141671265f, 20.8528231774f},
	{1.0f, 5.03186449562f, 26.5140253441f}
};

static const double Bessel7[4][3] =
{
	{0.0f, 1.0f, 4.97178685853f},
	{1.0f, 9.51658105631f, 25.6664447528f},
	{1.0f, 8.14027832728f, 28.9365460933f},
	{1.0f, 5.37135375789f, 36.5967851569f}
};

static const double Bessel8[4][3] =
{
	{1.0f, 11.1757720865f, 31.9772252583f},
	{1.0f, 10.4096815813f, 33.9347400852f},
	{1.0f, 8.73657843441f, 38.5692532751f},
	{1.0f, 5.6779678978f, 48.4320186526f}
};

static const double Bessel9[5][3] =
{
	{0.0f, 1.0f, 6.29701918171f},
	{1.0f, 12.2587358086f, 40.58926791f},
	{1.0f, 11.208843639f, 43.6466457531f},
	{1.0f, 9.27687977436f, 49.7885026574f},
	{1.0f, 5.95852159636f, 62.041437622f}
};

static const double Bessel10[5][3] =
{
	{1.0f, 13.8440898108f, 48.6675485642f},
	{1.0f, 13.230581931f, 50.5823615629f},
	{1.0f, 11.9350566571f, 54.8391562023f},
	{1.0f, 9.77243913373f, 62.6255859126f},
	{1.0f, 6.2178324673f, 77.4427005313f}
};

static const double Bessel11[6][3] =
{
	{0.0f, 1.0f, 7.62233984587f},
	{1.0f, 14.9684597214f, 59.0312228174f},
	{1.0f, 14.1157847753f, 61.987067162f},
	{1.0f, 12.6026749098f, 67.5450530366f},
	{1.0f, 10.2312965678f, 77.1069226774f},
	{1.0f, 6.45944417984f, 94.6504815227f}
};

static const double Bessel12[6][3] =
{
	{1.0f, 16.5068440222f, 68.8718670308f},
	{1.0f, 15.9945412005f, 70.7635652426f},
	{1.0f, 14.9311424798f, 74.8331362156f},
	{1.0f, 13.2220085003f, 81.7932213648f},
	{1.0f, 10.6594171817f, 93.2551026277f},
	{1.0f, 6.68604661561f, 113.677190678f}
};

static const double Bessel13[7][3] =
{
	{0.0f, 1.0f, 8.94770967478f},
	{1.0f, 17.6605041673f, 80.9893620477f},
	{1.0f, 16.9411835439f, 83.8882643477f},
	{1.0f, 15.6887605536f, 89.1483117404f},
	{1.0f, 13.8007456524f, 97.6091561294f},
	{1.0f, 11.0613619667f, 111.089662278f},
	{1.0f, 6.89973444126f, 134.53347742f}
};

static const double Bessel14[7][3] =
{
	{1.0f, 19.1663427836f, 92.5900963594f},
	{1.0f, 18.726291709f, 94.4678346321f},
	{1.0f, 17.822001109f, 98.4295218777f},
	{1.0f, 16.3976939394f, 104.958045246f},
	{1.0f, 14.3447919246f, 115.015458817f},
	{1.0f, 11.4407047677f, 130.627669834f},
	{1.0f, 7.10217376675f, 157.228593274f}
};

static const double Bessel15[8][3] =
{
	{0.0f, 1.0f, 10.2731096484f},
	{1.0f, 20.3418280223f, 106.462538332f},
	{1.0f, 19.7191344416f, 109.326137786f},
	{1.0f, 18.6471986449f, 114.410782731f},
	{1.0f, 17.064918105f, 122.285551118f},
	{1.0f, 14.8587939854f, 134.032272074f},
	{1.0f, 11.8003034274f, 151.884171584f},
	{1.0f, 7.29471372497f, 181.770660128f}
};

static const double Bessel16[8][3] =
{
	{1.0f, 21.8237721604f, 119.82220018f},
	{1.0f, 21.4379716384f, 121.690848562f},
	{1.0f, 20.6502391913f, 125.584254159f},
	{1.0f, 19.4246526828f, 131.854229338f},
	{1.0f, 17.6959363823f, 141.15198017f},
	{1.0f, 15.3464815851f, 154.677645394f},
	{1.0f, 12.1424827653f, 174.872537497f},
	{1.0f, 7.47846359435f, 208.166873055f}
};

static const double BesselGains[16] =
{
	1.0f, 3.0f, 15.0f, 
	105.0f, 945.0f, 10395.0f, 
	135135.0f, 2027025.0f, 34459425.0f, 
	654729075.0f, 13749310575.0f, 316234143225.0f, 
	7.90585358063e+12f, 2.13458046677e+14f, 6.19028335363e+15f, 
	1.91898783963e+17f
};

static const double f0NormFactors[16] =
{
	1.0f, 1.36165412872f, 1.75567236868f, 
	2.1139176749f, 2.42741070215f, 2.7033950612f, 
	2.95172214704f, 3.17961723751f, 3.39169313891f, 
	3.59098059457f, 3.77960741644f, 3.95915082114f, 
	4.13082549938f, 4.29559340953f, 4.45423302162f, 
	4.60738546547f
};

#endif      // #ifndef __BESSEL_ANALOG_LUT__
