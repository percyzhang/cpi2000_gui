//==============================================================================
//    HMG Confidential - Subject to Non-Disclosure Restriction  
//    
//    Copyright [yearCreated] by Harman International Industries, Inc.  
//    This is an unpublished work protected by Harman International Industries
//    as a trade secret.  All Rights Reserved   
//    
//=============================================================================
#ifndef __C_BIQUAD_H__
#define __C_BIQUAD_H__

/**
 * a structure to store the coefs used in a 2nd order IIR filter
 */
struct sBiquadCoefs
{
    double b0,b1,b2,a1,a2;

	inline struct sBiquadCoefs operator*(const double& interpolator)
	{
		struct sBiquadCoefs returnVal;

		returnVal.b0 = this->b0 * interpolator;
		returnVal.b1 = this->b1 * interpolator;
		returnVal.b2 = this->b2 * interpolator;
		returnVal.a1 = this->a1 * interpolator;
		returnVal.a2 = this->a2 * interpolator;

		return returnVal;
	}
	inline struct sBiquadCoefs operator+(const struct sBiquadCoefs& coefs)
	{
		struct sBiquadCoefs returnVal;

		returnVal.b0 = this->b0 + coefs.b0;
		returnVal.b1 = this->b1 + coefs.b1;
		returnVal.b2 = this->b2 + coefs.b2;
		returnVal.a1 = this->a1 + coefs.a1;
		returnVal.a2 = this->a2 + coefs.a2;

		return returnVal;
	}
};




/**
 * Biquad class which contains the filter state
 */
class CBiquad
{
public:
	/**
     * constructs a CBiquad that will use the internal sBiqiadCoefs struct
     */
	CBiquad()	{ClearZ();}

	/**
     * destructor
     */
	~CBiquad(){};
	static const sBiquadCoefs _bypassCoefs;	

	/**
     * process one sample of audio
	 *
	 * @param input
     *               pointer to the double value to be processed
	 * @param output
     *               pointer to the output double value
	 * @param coefs
     *               const reference of the biquad coefficients
	 */
	inline void Process(double *input, 
						double *output,
						const struct sBiquadCoefs& coefs)
	{
		register double in = input[0];

		output[0]	= _z[0] + in * coefs.b0;
		_z[0]		= _z[1] + in * coefs.b1 + output[0] * coefs.a1;
		_z[1]		=		  in * coefs.b2 + output[0] * coefs.a2;
	}
				 			 	 
	/**
     * processes a block of audio
	 *
	 * @param input
     *               pointer to the double array to be processed
	 * @param output
     *               pointer to the output double array
	 * @param coefs
     *               const reference of the biquad coefficients	 *
	 * @param numSamples
     *               the number of samples to be processed
	 */
	inline void ProcessBlock(const double *input, 
							 double *output, 
							 int numSamples,
		   					 const struct sBiquadCoefs& coefs)
	{
		register double a1 = coefs.a1;
		register double a2 = coefs.a2;
		register double b0 = coefs.b0;
		register double b1 = coefs.b1;
		register double b2 = coefs.b2;	

		for (int i = 0; i < numSamples; i++)
		{	
                output[i]  = _z[0] + input[i] * b0;
                _z[0]      = _z[1] + input[i] * b1 + output[i] * a1;
                _z[1]      =         input[i] * b2 + output[i] * a2;
		}
	}

	/**
     * clear the filter history
	 */
	inline void ClearZ()
	{
		_z[0] = 0.0f;
		_z[1] = 0.0f;
	}

private:
	double					_z[2];
};

#endif      // #ifndef __C_BIQUAD_H__
