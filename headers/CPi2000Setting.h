#ifndef CPI2000_SETTING_H
#define CPI2000_SETTING_H

#include <QWidget>
#include <QSettings>
#include <QHostAddress>
#include <QJsonDocument>
#include <QJsonObject>
#include "OutputChannel.h"
#include "types.h"
#include <QTime>
#include <QDate>
#include "SpeakerData.h"

#define MAX_EQ_FREQUENCY 20000
#define MIN_EQ_FREQUENCY 20
#define MAX_MAGNITUDE 20


typedef enum
{
	SIGNAL_OFF = 0,
	SIGNAL_100Hz,
	SIGNAL_1kHz,
	SIGNAL_10kHz,
	SIGNAL_PINK_NOISE,
} SIGNAL_MODE;

typedef enum
{
	RTA_FULL = 0,
	RTA_X_CURVE
} RTA_MODE;

QString formatFreq(qreal freq);
QString formatGain(qreal gain);

class RTAData
{
public:
	RTAData();

	void init();

	SIGNAL_MODE getSignalMode() { return m_signalMode; }
	void setSignalMode(SIGNAL_MODE mode) { m_signalMode = mode; }

	void setEQTuningPinkNoise(int index) {  m_pinkNoiseIndex_EQTurning = index; }
	int getEQTuningPinkNoise() { return m_pinkNoiseIndex_EQTurning; }

	void setRoomLevelPinkNoise(bool roomLevelPinkNoise[8]);
	void getRoomLevelPinkNoise(bool roomLevelPinkNoise[8]);

	bool getRotate() { return m_rotateFlag; }
	void setRotate(bool flag) { m_rotateFlag = flag; }

	double getMeasuredValue()	{ return m_measuredValue; }
	void setMeasuredValue(double measuredValue)	{ m_measuredValue = measuredValue; }

	void setSPLValue(float splValue) { m_splValue = splValue; }
	float getSPLValue() { return m_splValue; }

	double getAddValue() { return m_addValue; }
	void setAddValue(double addValue) { m_addValue = addValue; }

	void setRTAValue(float RTAValue[30]);
	float *getRTAValue();

	void setRTAMode(RTA_MODE rtaMode) { m_RTAMode = rtaMode; }
	RTA_MODE getRTAMode() { return m_RTAMode; }

protected:
	SIGNAL_MODE m_signalMode;
	double m_measuredValue;
	float m_splValue;	/* It is from DSP meter */
	double m_addValue;		/* It is decided by m_measuredValue and the feedbackValue */

	int m_pinkNoiseIndex_EQTurning;	/* which channel will send pink noise (0 - 7) of EQ Tuning (There is one and only one channel can send pink noise) */
	bool m_pinkNoise_RoomLevel[8];	/* true: the channel will send pink noise;  false: the channel won't send pink noise */
	bool m_rotateFlag;	

	RTA_MODE m_RTAMode;		/* RTA_FULL or RTA_X_CURVE */
	float m_RTAValue[30];	/* It is from DSP meter */
};

extern float masterVolumeToDB(float volume);

extern float DBToMasterVolume(float DB);

extern float volumeToDB(float volume);

extern float DBToVolume(float DB);

class CPi2000Settings : public QObject
{
    Q_OBJECT
public:
    explicit CPi2000Settings(QObject *parent = 0);

    Q_INVOKABLE virtual void setValue(const QString & key, const QVariant & value){
        QSettings settings("Harman", "CPi2000");
        settings.setValue(key, value);
    }

    Q_INVOKABLE virtual QVariant value(const QString & key, const QVariant& defaultValue = QVariant("")){
        QSettings settings("Harman", "CPi2000");
        return settings.value(key, defaultValue);
    }

    Q_INVOKABLE virtual void remove(const QString & key){
        QSettings settings("Harman", "CPi2000");
        settings.remove(key);
    }

	Q_INVOKABLE virtual bool contains(const QString & key){
		QSettings settings("Harman", "CPi2000");
		return settings.contains(key);
	}

	Q_INVOKABLE virtual void clear(){
		QSettings settings("Harman", "CPi2000");
		settings.clear();
	}

signals:

public slots:

};

typedef enum
{
	SPEAKER_CHANNEL_L = 0,
	SPEAKER_CHANNEL_R = 1,
	SPEAKER_CHANNEL_CENTER = 2,
	SPEAKER_CHANNEL_SW = 3,
	SPEAKER_CHANNEL_LS = 4,
	SPEAKER_CHANNEL_RS = 5,
	SPEAKER_CHANNEL_BLS = 6,
	SPEAKER_CHANNEL_BRS = 7,
} SPEAKER_CHANNEL;

typedef enum
{
	SPEAKER_5_1,	// output: 5.1 channel
	SPEAKER_7_1,	// output: 7.1 channel
} OUTPUT_CHANNEL_TYPE;

typedef struct
{
	bool m_dhcpEnable;
	QHostAddress m_ipAddress, m_networkMask, m_ipGateway;
} NETWORK_SETTING;


typedef enum
{
	INPUT_SOURCE_DIGITAL = 0,
	INPUT_SOURCE_MIC = 1,
	INPUT_SOURCE_ANALOG = 2,
	INPUT_SOURCE_MUSIC = 3,
	INPUT_SOURCE_NULL = 4,
} INPUT_SOURCE;

typedef struct
{
//	bool m_bPLEnableFlag;
	bool m_centerGenFlag;
	bool m_inverseFlag;
} SWConfiguration;

class CPi2000Data : public QObject
{
	Q_OBJECT
public:
	QString m_strTheaterName, m_strHallName, m_serverName, m_projectorName, m_speakerName[8], m_amplifierName[8], m_strSerialNumber;

public:
	CPi2000Data();

	bool loadFromJSON(QString presetDBFileName);
    bool saveToJSON(QString presetDBFileName);

	void addSpeaker(QString strSpeakerName, SPEAKER_TYPE speakerType);
	void clearSpeaker();

	QStringList getAvailableSpeakerName(SPEAKER_CHANNEL speakerType);
	OUTPUT_CHANNEL_TYPE getSpeakerNumber() { return m_speakerNumber; }
	void setSpeakerNumber(OUTPUT_CHANNEL_TYPE speakerNumber) { m_speakerNumber = speakerNumber; }

	void setOutputDB(float outputDB[8]);
	float *getOutputDB() { return m_outputDB; }

	void setInputDB(float inputDB[8]);
	float *getInputDB() { return m_inputDB; }
	float *getMaxInputDB() { return &m_maxInputDB; }

	void setNetworkAddress(NETWORK_SETTING networkSetting) { m_networkSetting = networkSetting; }
	NETWORK_SETTING getNetworkAddress() { return m_networkSetting; }

	INPUT_SOURCE getInputSource()  { return  m_inputSource; }
	void setInputSource(INPUT_SOURCE inputSource) { m_inputSource = inputSource; }

	float getMasterVolume() { return m_masterVolume; }
	void setMasterVolume(float volume) { m_masterVolume = volume; }

	float getMasterVolumePreset() { return m_masterVolumePreset; }
	void setMasterVolumePreset(float volume) { m_masterVolumePreset = volume; }

	/* set volume preset for input source*/
	float getDigitalVolume() { return m_digitalVolume; }
	void setDigitalVolume(float volume) { m_digitalVolume = volume; }
	float getMicVolume() { return m_micVolume; }
	void setMicVolume(float volume) { m_micVolume = volume; }
	float getAnalogVolume() { return m_analogVolume; }
	void setAnalogVolume(float volume) { m_analogVolume = volume; }
	float getMusicVolume() { return m_musicVolume; }
	void setMusicVolume(float volume) { m_musicVolume = volume; }

	
	void setMasterOutputMuteFlag(bool muteFlag) { m_masterOutputMuteFlag = muteFlag; }
	bool getMasterOutputMuteFlag() { return m_masterOutputMuteFlag; }

	void setFadeIn(float fadeInSecond) { m_fadeIn = fadeInSecond; }
	float getFadeIn() { return m_fadeIn; }

	void setSurroundDelay(int delayInMS) { m_surroundDelay = delayInMS; }
	void setLengthDistance(float lengthInMeter)	{ m_hallLength = lengthInMeter; }
	void setWidthDistance(float widthInMeter)	{ m_hallWidth = widthInMeter; }

	int getSurroundDelay() { return m_surroundDelay; }
	float getLengthDistance() { return m_hallLength; }
	float getWidthDistance() { return m_hallWidth; }

	void setFadeOut(float fadeOutSecond) { m_fadeOut = fadeOutSecond; }
	float getFadeOut() { return m_fadeOut; }

	void setDetectionMask(UINT32 detectionMask) { m_detectionMask = detectionMask; }
	UINT32  getDetectionMask() { return m_detectionMask; }

	void setAnalogChannel(int channel[8]);
	void setDigitalChannel(int channel[8]);

	void setMusicMute(bool flag) { m_musicMute = flag; }
	void setMicMute(bool flag) { m_micMute = flag; }
	void setDigitalMute(bool flag) { m_digitalMute = flag; }
	void setAnalogMute(bool flag) { m_analogMute = flag; }

	bool getDigitalMute() { return m_digitalMute; }
	bool getAnalogMute() { return m_analogMute; }
	bool getMicMute() { return m_micMute; }
	bool getMusicMute() { return m_musicMute; }

	bool getMusicDelayEnable()		{ return m_musicDelayEnable; }
	bool getAnalogDelayEnable()		{ return m_analogDelayEnable; }
	bool getMicDelayEnable()		{ return m_micDelayEnable; }
	bool getDigitalDelayEnable()	{ return m_digitalDelayEnable; }

	void setMusicDelayEnable(bool flag)			{ m_musicDelayEnable = flag; }
	void setAnalogDelayEnable(bool flag)		{ m_analogDelayEnable = flag; }
	void setDigitalDelayEnable(bool flag)		{ m_digitalDelayEnable = flag; }
	void setMicDelayEnable(bool flag)			{ m_micDelayEnable = flag; }

	void setMicChannel(MIC_CHANNEL channel)		{ m_micChannel = channel; }
	MIC_CHANNEL getMicChannel()		{ return m_micChannel;	}

	void setMusicChannel(int channel)		{ m_musicChannel = channel; }
	int getMusicChannel()		{ return m_musicChannel;	}

	void setMicDelay(int delay)			{ m_micDelay = delay; }
	void setMusicDelay(int delay)		{ m_musicDelay = delay; }
	void setAnalogDelay(int delay)		{ m_analogDelay = delay; }
	void setDigitalDelay(int delay)		{ m_digitalDelay = delay; }

	int getMicDelay() { return m_micDelay; }
	int getMusicDelay() { return m_musicDelay; }
	int getAnalogDelay() { return m_analogDelay; }
	int getDigitalDelay() { return m_digitalDelay; }

//	void setLFEInverse(bool flag)			{ m_LFEInverse = flag;	}
//	bool getLFEInverse()	{ return m_LFEInverse; }
	void setRoomLevelPinkNoise(bool channel[8])		{ m_RTAData.setRoomLevelPinkNoise(channel);	}
	void getRoomLevelPinkNoise(bool *channel)		{ return m_RTAData.getRoomLevelPinkNoise(channel);	}

	void setEQTuningPinkNoise(int channelID)	{ m_RTAData.setEQTuningPinkNoise(channelID); }
	int  getEQTuningPinkNoise()					{ return m_RTAData.getEQTuningPinkNoise(); }

	void enablePhantom(bool flag)			{ m_phantomEnable = flag; }
	bool getPhantomFlag() { return m_phantomEnable; }

	void setSpeakerRoomLevelMuteFlag(int index, bool muteFlag)	{ m_pOutputChannel[index].setRoomLevelMute(muteFlag);	}
	void setSpeakerRoomLevelGain(int index, float value)  { m_pOutputChannel[index].setRoomLevelGain(value);	}
	float getSpeakerRoomLevelGain(int index)		{ return m_pOutputChannel[index].getRoomLevelGain();	}
	bool getSpeakerRoomLevelMuteFlag(int index) { return m_pOutputChannel[index].getRoomLevelMute(); }

	void setGEQ(int speakerIndex, int geqIndex, float value) { m_pOutputChannel[speakerIndex].setGEQ(geqIndex, value); }
	float getGEQ(int speakerIndex, int geqIndex) { return m_pOutputChannel[speakerIndex].getGEQ(geqIndex); }

	int * getAnalogChannel() { return m_AnalogChannel; }
	int * getDigitalChannel() { return m_DigitalChannel; }

	void setUSBStatus(bool flag) { m_usbConnectStatus = flag; }
	bool getUSBStatus() { return m_usbConnectStatus; }

	void getDeviceTime(QDate & deviceDate, QTime & deviceTime) { deviceDate = m_deviceDate;  deviceTime = m_deviceTime; }
	void setDeviceTime(QDate *pDeviceDate, QTime *pDeviceTime) { m_deviceDate = *pDeviceDate;  m_deviceTime = *pDeviceTime; }

	RTAData* getRTA() { return &m_RTAData; }

	int getCurrentOutputChannelIndex() { return m_currentOutputChannel; }
	void setCurrentOutputChannelIndex(int index) { m_currentOutputChannel = index; }
	OutputChannel *getCurrentOutputChannel() { return m_pOutputChannel + m_currentOutputChannel; }

	SWConfiguration* getSWConfiguration() { return &m_swConfiguration; }

	void setRTACenterLevel(float level) { m_RTACenterLevel = level; }
	float getRTACenterLevel() { return m_RTACenterLevel; }

	void setRTAAverageLevel(float level) { m_RTAAverageLevel = level; }
	float getRTAAverageLevel() { return m_RTAAverageLevel; }

protected:

private:
	QDate m_deviceDate;
	QTime m_deviceTime;

	/* Tab 0: Overview  */
	NETWORK_SETTING m_networkSetting;

	INPUT_SOURCE m_inputSource;
	float m_outputDB[8];	/* -90dB - 0dB */
	float m_inputDB[8];		/* -90dB - 0dB*/
	float m_maxInputDB;		/* -90dB - 0dB*/

	/* Tab 1: Basic Setting */
	float m_masterVolumePreset;		/* from 0.0 - 10.0 */
	float m_masterVolume;		/* from 0.0 - 10.0 */
	float m_fadeIn, m_fadeOut;
	bool m_masterOutputMuteFlag;
	OUTPUT_CHANNEL_TYPE m_speakerNumber;
#if 0
	bit 0, 	//left1_ch1 <-> l_mf
	bit 1,	//left1_ch2 <-> l_hf
	bit 2,	//left2_ch1 <-> l_lf
	bit 3,	//left2_ch2 <-> r_lf
	bit 4,  //center1_ch1<->c_mf
	bit 5,	//center1_ch2<->c_hf
	bit 6,	//center2_ch1<->c_lf
	bit 7,	//right1_ch1<-> r_mf
	bit 8,	//right1_ch2<-> r_hf
	bit 9,	//ls_rs1_ch1<-> ls
	bit 10,	//ls_rs1_ch2<-> rs
	bit 11,	//ls_rs2_ch1<-> ls
	bit 12,	//ls_rs2_ch2<-> rs
	bit 13,	//bls_brs1_ch1<->bls
	bit 14,	//bls_brs1_ch2<->brs
	bit 15,	//sw1_ch1	<-> sw
	bit 16,	//sw1_ch2	<-> sw
	bit 17,	//sw2_ch1	<-> sw
	bit 18,	//sw2_ch2	<-> sw
#endif	
	UINT32 m_detectionMask;	
	float m_hallLength, m_hallWidth;	// Unit: meter
	int m_surroundDelay;	// surround delay in ms

	/* Tab 2: Input Source */

	int m_currentOutputChannel;
	OutputChannel m_pOutputChannel[8];		/* L / C / R / LFE / Ls / Rs / Bsl / Bsr */
	SWConfiguration m_swConfiguration;		/* Only for SW Configuration */

	RTAData m_RTAData;
	float m_RTACenterLevel;		/* The Blue RTA level for Center Speaker */
	float m_RTAAverageLevel;	/* The current RTA level of other speaker */


	int m_AnalogChannel[8];	/* Channel for analog input, range: 0 - 7 */
	int m_DigitalChannel[8];	/* Channel for analog input, range: 0 - 7 */

	float m_digitalVolume, m_analogVolume, m_micVolume, m_musicVolume;	/* Range from 0 - 10 */

	bool m_digitalMute, m_analogMute, m_micMute, m_musicMute;
	bool m_digitalDelayEnable, m_analogDelayEnable, m_musicDelayEnable, m_micDelayEnable; 
	int m_micDelay, m_musicDelay, m_analogDelay, m_digitalDelay;

//	bool	m_LFEInverse;	
	bool	m_phantomEnable;

	MIC_CHANNEL	m_micChannel;
	int m_musicChannel;

	bool m_usbConnectStatus;
	QStringList speakerList_LCR, speakerList_surround, speakerList_subwoofer;
};

#define MAX_LOG_COUNT 10000

typedef enum
{
	LOG_INFO = 0,
	LOG_WARNING = 1,
	LOG_ERROR = 2,
	LOG_CRITICAL = 3,
} LOG_WARNING_LEVEL;

typedef struct
{
	QDate m_date;
	QTime m_time;
	LOG_WARNING_LEVEL m_level;
	QString m_content;
} Log;

class LogManager
{
public:
	LogManager();
	int getCount();
	Log *getLog(int i);
	void addLog(Log *pLog);
	void clearAll();
	void filter(int filterMask, LogManager &newLogManager);
	void addLogFile(QString fileName);

private:
	Log m_pLog[MAX_LOG_COUNT];
	int m_head;
	int m_tail;
	int m_filterMask;
} ;

extern void dump(Log *pLog);
extern LogManager* getLogManager();
extern CPi2000Settings	g_CPi2000Setting;
extern CPi2000Data *getCPi2000Data();

#endif
