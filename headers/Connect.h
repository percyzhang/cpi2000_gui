#ifndef CONNECT_H
#define CONNECT_H

#include <QObject>
#include "types.h"

class Connect : public QObject
{
	Q_OBJECT
public:
	Connect() {}
	~Connect() {}

	/* To find the device, no matter it is connected or disconnected */
	virtual bool findDevice() = 0;

	// Open the device using the VID and PID.
//	int USBConnect(int vid, int pid);
//	int NetworkConnect(UINT32 ipAddr, UINT32 port);
	virtual bool connectDevice(UINT32 param1, UINT32 param2) = 0;
    virtual void disconnect() = 0;

	/* 
	Function Description:
		Send a translate layer packet (64 bytes + ReportID (Fix Byte 0)) to MCU.

	Return Value:
		ERR_NONE	- Send Successfully.
		other value - Send Failed. 
	*/
//	int USBSendPacket(BYTE data[65]);	
	virtual bool sendData(BYTE *pBuffer, int nLen) = 0;

	/*
	readPacket: get a packet from translate layer. The max size of the packet should be 64 bytes.

	Return Value:  
		true: get a valid packet from translate layer.
		false: no valid packet is returned.

	pBuffer	- valid only return value is true, which means a valid translate packet start from Magic 1
	*/
//	bool USBreadPacket(BYTE pBuffer[64]);
	virtual int recvData(BYTE *pBuffer, int nMaxLen) = 0;

signals:
	void onConnectBroken();


};

#endif // CONNECT_H
