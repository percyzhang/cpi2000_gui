#ifndef DEVICE_SOCKET_H
#define DEVICE_SOCKET_H

#include <QObject>
#include <QTcpSocket>
#include <QHostAddress>
#include "types.h"
#include <QTextStream>
#include "CPi2000Setting.h"

#define CMD_NULL							-1
#define CMD_WAIT_LOGIN						0
#define CMD_GET_HALL_NAME					1
#define CMD_GET_THEATER_NAME				2
#define CMD_GET_NETWORK_INFO				3
#define	CMD_GET_USB_FILE_NUMBER				4
#define CMD_GET_USB_FILE_NAME				5
#define CMD_GET_USB_NEW_FILE_NAME			6
#define CMD_GET_DEVICE_FIRMWARE_VERSION		7
#define CMD_GET_FT_FIRMWARE_VERSION			8
#define	CMD_GET_A5_MODEL_VERSION			9
#define CMD_GET_A5_FIRMWARE_VERSION			10
#define CMD_GET_DSP_FIRMWARE_VERSION		11
#define CMD_GET_SERIAL_NUMBER				12
#define CMD_GET_SYSTEM_TIME					13
#define CMD_GET_MASTER_VOLUME				14
#define CMD_RESERVED						15

#define CMD_TOTAL_COUNT						16



/* Platform */
#define	NODE_PC_CONNECTION		"\\\\Node\\SV\\PCConnectStatus"
#define NODE_HEART_BEAT			"\\\\Node\\SV\\HeartBeat\\r"
#define	NODE_INPUT_VOLUME		"\\\\Preset\\PeakMeters\\SV\\InChArray\\r"
#define NODE_OUTPUT_VOLUME		"\\\\Preset\\PeakMeters\\SV\\OutChArray\\r"
#define NODE_RTA_METER			"\\\\Preset\\RTA\\SV\\TotalValue\\r"
#define NODE_GUI_CONNECTION_STATUS "\\\\Node\\SV\\PCConnectStatus"

#define	NODE_USB_STATUS         "\\\\Node\\SV\\USBStatus"
#define NODE_USB_FILE_NUMBER	"\\\\Node\\SV\\USBTotalFilesNum\\r"
#define	NODE_USB_FILE_INDEX		"\\\\Node\\SV\\USBCurFileIndex\\r"
#define NODE_USB_FILE_NAME		"\\\\Node\\SV\\USBCurFileName\\r"
#define NODE_USB_NEW_FILE_NAME	"\\\\Node\\SV\\USBNewFileName\\r"



#define NODE_SYSTEM_TIME		"\\\\Node\\SV\\SysTime\\r"

/* Version */
#define NODE_DEVICE_FIRMWARE_VERSION	"\\\\Node\\SV\\Version\\r"
#define NODE_FT_FIRMWARE_VERSION		"\\\\Node\\SV\\DisplayFirmwareRev\\r"
#define NODE_A5_MODEL_VERSION			"\\\\Node\\SV\\ModelVersion\\r"
#define NODE_A5_FIRMWARE_VERSION		"\\\\Node\\SV\\FirmwareRev\\r"
#define NODE_DSP_FIRMWARE_VERSION		"\\\\Node\\SV\\DSPRev\\r"


/* TAB 1: Overview */
#define NODE_HALL_NAME			"\\\\Node\\SV\\Hall\\r"
#define NODE_THEATER_NAME		"\\\\Node\\SV\\Theater\\r"
#define NODE_PROJECTOR_NAME		"\\\\Node\\SV\\Projector\\r"
#define NODE_SERVER_NAME		"\\\\Node\\SV\\Server\\r"
#define NODE_SERIAL_NUMBER		"\\\\Node\\SV\\SysSerialNumber\\r"

#define NODE_NETWORK_INFO		"\\\\Node\\SV\\NetworkInfo\\r"

#define NODE_SPEAKER1_NAME		"\\\\Node\\SV\\SpeakerSettingL\\r"
#define NODE_SPEAKER2_NAME		"\\\\Node\\SV\\SpeakerSettingR\\r"
#define NODE_SPEAKER3_NAME		"\\\\Node\\SV\\SpeakerSettingC\\r"
#define NODE_SPEAKER4_NAME		"\\\\Node\\SV\\SpeakerSettingLFE\\r"
#define NODE_SPEAKER5_NAME		"\\\\Node\\SV\\SpeakerSettingLS\\r"
#define NODE_SPEAKER6_NAME		"\\\\Node\\SV\\SpeakerSettingRS\\r"
#define NODE_SPEAKER7_NAME		"\\\\Node\\SV\\SpeakerSettingBLS\\r"
#define NODE_SPEAKER8_NAME		"\\\\Node\\SV\\SpeakerSettingBRS\\r"

#define NODE_AMPLIFIER1_NAME		"\\\\Node\\SV\\Amplifier_L\\r"
#define NODE_AMPLIFIER2_NAME		"\\\\Node\\SV\\Amplifier_R\\r"
#define NODE_AMPLIFIER3_NAME		"\\\\Node\\SV\\Amplifier_C\\r"
#define NODE_AMPLIFIER4_NAME		"\\\\Node\\SV\\Amplifier_LFE\\r"
#define NODE_AMPLIFIER5_NAME		"\\\\Node\\SV\\Amplifier_LS\\r"
#define NODE_AMPLIFIER6_NAME		"\\\\Node\\SV\\Amplifier_RS\\r"
#define NODE_AMPLIFIER7_NAME		"\\\\Node\\SV\\Amplifier_BLS\\r"
#define NODE_AMPLIFIER8_NAME		"\\\\Node\\SV\\Amplifier_BRS\\r"


/* TAB 2: Basic Setting */
/* Verified */
//#define NODE_MASTER_MUTE			"\\\\Node\\SV\\MasterMute\\r"
#define NODE_FADE_INOUT_MUTE		"\\\\Preset\\Fader\\SV\\Mute\\r"
#define NODE_MASTER_VOLUME			"\\\\Preset\\MasterVol\\SV\\Gain\\r"
#define NODE_MASTER_VOLUME_PRESET	"\\\\Preset\\MasterVol\\SV\\GainPreset\\r"
#define NODE_OUTPUT_CHANNEL_TYPE	"\\\\Node\\SV\\AudioInputType\\r"
#define	NODE_FADE_IN				"\\\\Preset\\Fader\\SV\\FadeIn\\r"
#define	NODE_FADE_OUT				"\\\\Preset\\Fader\\SV\\FadeOut\\r"

	/* Not Verified */
//#define	NODE_HALL_LENGTH_1		"\\\\Preset\\InDelay\\SV\\Channel_7_Amount"
//#define	NODE_HALL_LENGTH_2		"\\\\Preset\\InDelay\\SV\\Channel_8_Amount"
//#define	NODE_HALL_WIDTH_1		"\\\\Preset\\InDelay\\SV\\Channel_5_Amount"
//#define	NODE_HALL_WIDTH_2		"\\\\Preset\\InDelay\\SV\\Channel_6_Amount"
#define	NODE_HALL_LENGTH			"\\\\Node\\SV\\DistanceL\\r"
#define	NODE_HALL_WIDTH				"\\\\Node\\SV\\DistanceW\\r"

#define NODE_SURROUND_DELAY_5		"\\\\Preset\\InDelay\\SV\\Channel_5_Amount"
#define NODE_SURROUND_DELAY_6		"\\\\Preset\\InDelay\\SV\\Channel_6_Amount"
#define NODE_SURROUND_DELAY_7		"\\\\Preset\\InDelay\\SV\\Channel_7_Amount"
#define NODE_SURROUND_DELAY_8		"\\\\Preset\\InDelay\\SV\\Channel_8_Amount"

#define NODE_LOAD_DETECT		"\\\\Node\\SV\\FaultMask\\r"

/* TAB 3: Input Setting */
/* Verified */
#define NODE_INPUT_SOURCE			"\\\\Preset\\SourceRouter\\SV\\InputSource\\r"

#define NODE_DIGITAL_VOLUME			"\\\\Preset\\SourceRouter\\SV\\DigitalGain\\r"
#define NODE_ANALOG_VOLUME			"\\\\Preset\\SourceRouter\\SV\\AnalogGain\\r"
#define NODE_MIC_VOLUME				"\\\\Preset\\SourceRouter\\SV\\MicGain\\r"
#define NODE_MUSIC_VOLUME			"\\\\Preset\\SourceRouter\\SV\\MusicGain\\r"

#define NODE_DIGITAL_MUTE			"\\\\Preset\\SourceRouter\\SV\\DigitalMute\\r"
#define NODE_ANALOG_MUTE			"\\\\Preset\\SourceRouter\\SV\\AnalogMute\\r"
#define NODE_MUSIC_MUTE				"\\\\Preset\\SourceRouter\\SV\\MusicMute\\r"
#define NODE_MIC_MUTE				"\\\\Preset\\SourceRouter\\SV\\MicMute\\r"

#define NODE_MUSIC_DELAY_ENABLE		"\\\\Preset\\SourceRouter\\SV\\MusicDelay\\r"
#define NODE_MIC_DELAY_ENABLE		"\\\\Preset\\SourceRouter\\SV\\MicDelay\\r"
#define NODE_ANALOG_DELAY_ENABLE	"\\\\Preset\\SourceRouter\\SV\\AnalogDelay\\r"
#define NODE_DIGITAL_DELAY_ENABLE	"\\\\Preset\\SourceRouter\\SV\\DigitalDelay\\r"

#define NODE_MUSIC_DELAY			"\\\\Preset\\SourceRouter\\SV\\MusicDelayAmount\\r"
#define NODE_MIC_DELAY				"\\\\Preset\\SourceRouter\\SV\\MicDelayAmount\\r"
#define NODE_ANALOG_DELAY			"\\\\Preset\\SourceRouter\\SV\\AnalogDelayAmount\\r"
#define NODE_DIGITAL_DELAY			"\\\\Preset\\SourceRouter\\SV\\DigitalDelayAmount\\r"

#define NODE_ANALOG_CHANNEL			"\\\\Preset\\SourceRouter\\SV\\AnalogChannel\\r"
#define NODE_DIGITAL_CHANNEL		"\\\\Preset\\SourceRouter\\SV\\DigitalChannel\\r"

#define NODE_MIC_CHANNEL			"\\\\Preset\\SourceRouter\\SV\\MicChannelAssign\\r"
#define NODE_MUSIC_CHANNEL			"\\\\Preset\\SourceRouter\\SV\\MusicChannelAssign\\r"

#define NODE_PHANTOM_ENABLE			"\\\\Preset\\SourceRouter\\SV\\PhantomPower\\r"
/* Not Verified */


/* TAB 4_1: ROOM_LEVEL */
#define NODE_SIGNAL_MODE			"\\\\Preset\\SourceRouter\\SV\\SignalEnable"

#define NODE_SPL_VALUE				"\\\\Preset\\SplMeters\\SV\\SplValue"

#define NODE_SIGNAL_TYPE			"\\\\Preset\\SignalGenerator\\SV\\SignalType"
#define NODE_SIGNAL_FREQUENCY		"\\\\Preset\\SignalGenerator\\SV\\SineFrequency"


#define NODE_PINKNOISE_LIST			"\\\\Preset\\SourceRouter\\SV\\PinkNoise\\r"
#define	NODE_PINKNOISE_LEFT			"\\\\Node\\SV\\PinkNoise\\Left\\r"	
#define	NODE_PINKNOISE_RIGHT		"\\\\Node\\SV\\PinkNoise\\Right\\r"	
#define	NODE_PINKNOISE_CENTER		"\\\\Node\\SV\\PinkNoise\\Center\\r"	
#define	NODE_PINKNOISE_LFE			"\\\\Node\\SV\\PinkNoise\\LFE\\r"	
#define	NODE_PINKNOISE_LS			"\\\\Node\\SV\\PinkNoise\\Ls\\r"	
#define	NODE_PINKNOISE_RS			"\\\\Node\\SV\\PinkNoise\\Rs\\r"	
#define	NODE_PINKNOISE_BSL			"\\\\Node\\SV\\PinkNoise\\Bsl\\r"	
#define	NODE_PINKNOISE_BSR			"\\\\Node\\SV\\PinkNoise\\Bsr\\r"	

#define	NODE_MUTE_SPEAKER_LEFT		"\\\\Preset\\InputGains\\SV\\Channel_1_Mute\\r"
#define	NODE_MUTE_SPEAKER_RIGHT		"\\\\Preset\\InputGains\\SV\\Channel_2_Mute\\r"
#define	NODE_MUTE_SPEAKER_CENTER	"\\\\Preset\\InputGains\\SV\\Channel_3_Mute\\r"
#define	NODE_MUTE_SPEAKER_LFE		"\\\\Preset\\InputGains\\SV\\Channel_4_Mute\\r"
#define	NODE_MUTE_SPEAKER_LS		"\\\\Preset\\InputGains\\SV\\Channel_5_Mute\\r"
#define	NODE_MUTE_SPEAKER_RS		"\\\\Preset\\InputGains\\SV\\Channel_6_Mute\\r"
#define	NODE_MUTE_SPEAKER_BSL		"\\\\Preset\\InputGains\\SV\\Channel_7_Mute\\r"
#define	NODE_MUTE_SPEAKER_BSR		"\\\\Preset\\InputGains\\SV\\Channel_8_Mute\\r"

#define	NODE_VOLUME_SPEAKER_LEFT	"\\\\Preset\\InputGains\\SV\\Channel_1_Gain\\r"
#define	NODE_VOLUME_SPEAKER_RIGHT	"\\\\Preset\\InputGains\\SV\\Channel_2_Gain\\r"
#define	NODE_VOLUME_SPEAKER_CENTER	"\\\\Preset\\InputGains\\SV\\Channel_3_Gain\\r"
#define	NODE_VOLUME_SPEAKER_LFE		"\\\\Preset\\InputGains\\SV\\Channel_4_Gain\\r"
#define	NODE_VOLUME_SPEAKER_LS		"\\\\Preset\\InputGains\\SV\\Channel_5_Gain\\r"
#define	NODE_VOLUME_SPEAKER_RS		"\\\\Preset\\InputGains\\SV\\Channel_6_Gain\\r"
#define	NODE_VOLUME_SPEAKER_BSL		"\\\\Preset\\InputGains\\SV\\Channel_7_Gain\\r"
#define	NODE_VOLUME_SPEAKER_BSR		"\\\\Preset\\InputGains\\SV\\Channel_8_Gain\\r"


/* TAB 4_2: GEQ */

/* TAB 4_3: PEQ - gain */
#define NODE_PEQ_SPEAKER_LEFT_GAIN		"\\\\Preset\\InPeq_L\\SV\\Band_5_Gain"
#define NODE_PEQ_SPEAKER_RIGHT_GAIN		"\\\\Preset\\InPeq_L\\SV\\Band_5_Gain"
#define NODE_PEQ_SPEAKER_CENTER_GAIN	"\\\\Preset\\InPeq_L\\SV\\Band_5_Gain"
#define NODE_PEQ_SPEAKER_LFE_GAIN		"\\\\Preset\\InPeq_L\\SV\\Band_5_Gain"
#define NODE_PEQ_SPEAKER_LS_GAIN		"\\\\Preset\\InPeq_L\\SV\\Band_5_Gain"
#define NODE_PEQ_SPEAKER_RS_GAIN		"\\\\Preset\\InPeq_L\\SV\\Band_5_Gain"
#define NODE_PEQ_SPEAKER_BSL_GAIN		"\\\\Preset\\InPeq_L\\SV\\Band_5_Gain"
#define NODE_PEQ_SPEAKER_BSR_GAIN		"\\\\Preset\\InPeq_L\\SV\\Band_5_Gain"

#define NODE_SW_GEN_CENTER				"\\\\Preset\\SourceRouter\\SV\\SignalCenterMode"
#define	NODE_LFE_INVERSE				"\\\\Preset\\InputGains\\SV\\Channel_4_Polarity\\r"	

class DeviceSocket : public QObject
{
	Q_OBJECT

public:
	explicit DeviceSocket(QHostAddress localAddress, QHostAddress deviceAddress);
	~DeviceSocket();
	bool setConnection();

	/* Get Model Name */
	bool getDeviceFWVersion(QString & version, int timeoutInSecond);
	bool getDSPFWVersion(QString & version, int timeoutInSecond);
	bool getFrontPanelFWVersion(QString & version, int timeoutInSecond);
	bool getA5FWVersion(QString & version, int timeoutInSecond);
	bool getA5ModelVersion(QString & version, int timeoutInSecond);

	/* Get Hall Name */
	bool getHallName(QString & hallName, int timeoutInSecond);

	/* Get Theater Name */
	bool getTheaterName(QString & theaterName, int timeoutInSecond);

	/* Set output speaker: 5.1 channel or 7.1 channel */
	void setOuputChannalType(OUTPUT_CHANNEL_TYPE channelNumber);
	
	/* Set theater Name */
	void setTheaterName(QString theaterName);

	/* set hall name */
	void setHallName(QString theaterName);

	/* sub output volume */
	void subOutputVolume();

	void subInputVolume();

	void unsubInputVolume();

	void subUSBStatus();

	void subRTAMeter();
	void unsubRTAMeter();

	/* set IP Address of the device */
	void setDeviceNetworkAddress(NETWORK_SETTING setting);

	/* get device network address */
	bool getDeviceNetworkAddr(NETWORK_SETTING &networkSetting, int timeoutInSecond);

	void setMasterVolume(float volume);  /*  volume range from 0 - 10, not dB */

	void setMasterVolumePreset(float volume);	/* volume range from 0 - 10, net dB */

//	void setMasterOutputMuteFlag(bool muteFlag); /* true: mute the speaker;  false: unmute the speaker (Normal Mode) */
	void setFadeInOutMuteFlag(bool muteFlag); 

	void setInputSource(INPUT_SOURCE inputSource);

	void sendHeartBeat();	/* We should send heart beat every 1 second */
	void endHeartBeat(void);		/* Notify the CPi2000 that heart beat is over */

	void sendBye(void);

	void setFadeIn(float value);
	void setFadeOut(float value);

	void setDigitalVolume(float volume);
	void setAnalogVolume(float volume);
	void setMusicVolume(float volume);
	void setMicVolume(float volume);

	void setLengthDistance(float lengthInMeter);
	void setWidthDistance(float widthInMeter);
	void setSurroundDelay(int ms);

	QHostAddress getLocalAddr();
	QHostAddress getDeviceAddr();

	void setLoadDetect(UINT32 flag);

	void setAnalogChannel(int channel[8]);
	void setDigitalChannel(int channel[8]);

	void setDigitalMute(bool flag);
	void setAnalogMute(bool flag);
	void setMicMute(bool flag);
	void setMusicMute(bool flag);

	void setMusicDelayEnable(bool flag);
	void setMicDelayEnable(bool flag);
	void setAnalogDelayEnable(bool flag);
	void setDigitalDelayEnable(bool flag);

	void setMusicDelay(float delay);
	void setMicDelay(float delay);
	void setAnalogDelay(float delay);
	void setDigitalDelay(float delay);

	void setLFEInverse(bool flag);
	void setPinkNoise(bool channelFlag[8]);

	void setMicChannel(MIC_CHANNEL channel);
	void setMusicChannel(int channel);
	void enablePhantom(bool flag);

	void setSpeakerMuteFlag(int index, bool flag);

	void setSpeakerVolume(int index, float value);

	void setGEQ(int speakerIndex, int index, float value);

	void setGEQBypassFlag(int speakerIndex, bool bypassFlag);

	void setPEQBypassFlag(int speakerIndex, bool bypassFlag);

	void saveDB(void);

	void loadSpeakerFile(void);	/* Notify A5 to copy speaker.db to /ftp/home/speaker.db */

	void saveSpeakerFile(void); /* Notify A5 to save speaker.db from /ftp/home/speaker.db */

	void saveLogFile(void);

	void setNormalString(QString keyString, QString value);

	void setPEQGain(int currentGroupIndex, int currentEQIndex, float value);

	void setPEQFrequence(int currentGroupIndex, int currentEQIndex, float value);

	void setPEQEnable(int currentSpeakerIndex, int m_EQIndex, bool enableflag);

	void setPEQ_Q(int currentGroupIndex, int currentEQIndex, float value);

	void setPEQSlope(int currentGroupIndex, int currentEQIndex, float value);

	void SaveDeviceFile(QString remoteDeviceFile);

	void loadDeviceFile(QString remoteDeviceFile);
	void loadUSBFile(QString usbDeviceFile);

	void resetDevice();
	void restoreFactorySetting();

	QString getNewFileName();

	void setLogOn();
	void setLogOff();
	void clearLog();

	bool getUSBFileList(QStringList &fileNameList);
	bool getUSBFileName(int fileIndex, QString& fileName);
	bool getUSBFileNumber(int &fileCount);
	bool getUSBNewFileName(QString & newFileName);
	void saveUSBFile(QString usbFile);
	bool getSerialNumber(QString &serialNumber);
	bool getMasterVolume(float & masterVolume);

	void getDeviceTime();
	void setDeviceTime(const QDate &date, const QTime &time);

	bool getRecvFlag();		/* Used for connection deection */

	QString getValueByNode(QString node, int waitTimeInSecond);

	void startSignal();

	void stopSignal();

	void setSignalMode(SIGNAL_MODE);

	void getSPLValue();

	void setLowShelfGain(int channelIndex, double gain);
	void setLowShelfFreq(int channelIndex, double freq);
	void setLowShelfSlope(int channelIndex, double slope);
	void setHighShelfGain(int channelIndex, double gain);
	void setHighShelfFreq(int channelIndex, double freq);
	void setHighShelfSlope(int channelIndex, double slope);	
	void setSWBellFreq(int index, double freq);
	void setSWBellCut(int index, double gain);
	void setSWBellQ(int index, double Q);
	void setSWCenterGen(bool flag);

public slots:
	void readyRead();

protected:
	void onDispatchData(QString strLine);
	void writeToBuffer(const QString & node, const QString &value);		/* For some message, We should not send the same message within 500ms */
	void writeToSocket(const char *lpszBuffer);

protected:
	QTcpSocket *m_pSocket;
	QHostAddress m_localAddress;
	QHostAddress m_deviceAddress;
	bool m_bConnectedFlag;
	QTextStream *m_pTextStream;

	/* Wait Model Version */
	bool m_commandFlag[CMD_TOTAL_COUNT];
	QStringList m_strCommandList[CMD_TOTAL_COUNT];

	bool m_recvFlag;
	UINT32 m_sendBufferTimer;	/* We should not send the same message within 500ms */
	QStringList m_nodeList, m_valueList; /* it is used to store the SV and value which is sent by the function: writeToBuffer */
};
#endif
