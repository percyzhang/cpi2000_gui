#ifndef DIGITAL_INPUT_SETTING_WIDGET_H
#define DIGITAL_INPUT_SETTING_WIDGET_H

#include <QWidget>
#include "MarkButton.h"
#include <QLabel>
#include <QComboBox>
#include "VolumeChartWidget.h"

typedef struct
{
	QString m_channelName;
	int m_channelNumber;
	int m_currentVolume;
} InputChannel;




class DigitalInputSettingWidget : public QWidget
{
    Q_OBJECT
public:
	DigitalInputSettingWidget(QWidget *pParent);
	void refreshChannelNo();
	void getChannel(int channel[8]);
	void setChannel(int channel[8]);
	void retranslateUi();

protected:
	void paintEvent(QPaintEvent *event);
	void changeEvent(QEvent *e);
	void resizeEvent(QResizeEvent * /* event */);
	void setUndoState();

	void onComboCurrentChanged(int comboIndex, int channelIndex);

signals:
	void applyChannel();

protected slots:
//	void onTimerOut();
	void onCombo0CurrentChanged(int index);
	void onCombo1CurrentChanged(int index);
	void onCombo2CurrentChanged(int index);
	void onCombo3CurrentChanged(int index);
	void onCombo4CurrentChanged(int index);
	void onCombo5CurrentChanged(int index);
	void onCombo6CurrentChanged(int index);
	void onCombo7CurrentChanged(int index);
	void onClickUndoButton();
	void onClickApplyButton();
	void onClickPresetButton1();
	void onClickPresetButton2();
	void onClickPresetButton3();

protected:	
	MarkButton *m_pButton1, *m_pButton2, *m_pButton3;
	QPushButton *m_pUndoButton, *m_pApplyButton;

	QLabel *m_pLabel[8];
	QComboBox *m_pChannelCombo[8];
	int m_channelNo[8];		/* L / R / C / LFE / Ls / Rs / Bsl / Bsr */
	int m_undoChannelNo[8];

	/* L / R / C / LFE / Ls / Rs / Bsl / Bsr */
//	VolumeChartWidget *m_pVolumeChart[8];

	InputChannel *m_pInputChannel;
};
#endif
