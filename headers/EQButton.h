#ifndef EQBUTTON_H
#define EQBUTTON_H

#include "QPushButton"

class EQButton : public QPushButton
{
    Q_OBJECT

public:
	EQButton(QString &buttonText, QWidget *pParent);
    Q_PROPERTY(bool enableFlag READ getEnableFlag WRITE setEnableFlag)
    Q_PROPERTY(bool selectFlag READ getSelectFlag WRITE setSelectFlag)
	void enable(bool flag);
	void focus(bool flag);

signals:
    void doubleClick();

protected:
	void setEnableFlag(bool flag);
    bool getEnableFlag() const;
	void setSelectFlag(bool selectFlag);
	bool getSelectFlag() const;

protected:
	void paintEvent(QPaintEvent *event);
	void mouseDoubleClickEvent(QMouseEvent *);

protected:
	bool m_enableFlag;
	bool m_selectFlag;

	QWidget * m_parent;
};


#endif
