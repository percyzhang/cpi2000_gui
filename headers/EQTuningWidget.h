#ifndef EQ_TUNING_WIDGET_H
#define EQ_TUNING_WIDGET_H

#include <QWidget>
#include <QSlider>
#include <QGroupBox>
#include <QPushButton>
#include <QSpinBox>
#include <QComboBox>
#include <QButtonGroup>
#include "MarkButton.h"
#include "HeadFrame.h"
#include "GEQWidget.h"
#include <QRadioButton>
#include "ButtonSlider.h"
#include <QContextMenuEvent>
#include <QMenu>
#include <QAction>
#include "VolumeGroup.h"
#include <QRadioButton>
#include <QCheckBox>

class EQTuningWidget : public QWidget
{
	Q_OBJECT

public:
	EQTuningWidget(QWidget *pParent);
	void refresh();
	void refreshChannelLevel();

	void refreshGEQGroup();
	void refreshGEQGroup_SW();

	void refreshPEQ();
	void refreshPEQ_Normal();
	void refreshPEQ_SW();

	void refreshGEQ();
	void retranslateUi();

private:
	void resizeEvent(QResizeEvent * /* event */);
	void refreshTuningType();
	void refreshActiveChannel();
	void toggleCurrentChannel(int index, bool flag);
	void setAutoAssistIndex(int index);
	void autoAssist(int index);
	void onEQAssistStoped();

private slots:
	void contextMenuEvent(QContextMenuEvent *event);

	void onCurrentChannelToggled_0(bool flag);
	void onCurrentChannelToggled_1(bool flag);
	void onCurrentChannelToggled_2(bool flag);
	void onCurrentChannelToggled_3(bool flag);
	void onCurrentChannelToggled_4(bool flag);
	void onCurrentChannelToggled_5(bool flag);
	void onCurrentChannelToggled_6(bool flag);
	void onCurrentChannelToggled_7(bool flag);
	void onTimer_200ms(void);
	void onRefresh() { refresh(); }
	void onChangeVolume(float);
	void onToggleMuteButton(bool);
	void onSignalModeSelected(int index);

	void onLShelfGainSliderChanged(int gain_10x);
	void onHShelfGainSliderChanged(int gain_10x);
	void onLFreqChanged(double freq);
	void onLGainChanged(double gain);
	void onLSlopeChanged(double slope);
	void onHFreqChanged(double freq);
	void onHGainChanged(double gain);
	void onHSlopeChanged(double slope);

	void onSWPEQGainSliderChanged1(int gain_10x);
	void onPEQCutChanged1(double value);
	void onPEQFreqChanged1(double freq);
	void onPEQQChanged1(double Q);

	void onSWPEQGainSliderChanged2(int gain_10x);
	void onPEQCutChanged2(double value);
	void onPEQFreqChanged2(double freq);
	void onPEQQChanged2(double Q);

	void onEQAssistClicked();
	void onEQFlattenClicked();
	void onEQCopyClicked();
	void onEQPasteClicked();

private:
//	MarkButton *m_pGEQTuningButton, *m_pPEQTuningButton;
//	HeadFrame *m_pActiveChannelWidget;
//	ActiveChannelWidget *m_pActiveChannelWidget;
//	GEQWidget *m_pGEQWidget;
//	PEQWidget *m_pPEQWidget;
//	bool m_geqActiveFlag;	/* true: GEQ Tuning is active, false: PEQ Turing is active */
//	QRadioButton m_pSpeakerRadioButton[8];  	/* L / C / R / LFE / Ls / Rs / Bsl / Bsr */
	QGroupBox *m_pSignalModeGroup;
		QComboBox *m_pSignalModeCombo;
		QLabel *m_pReminderLabel; /* To stop RTA setting, you need to set signal mode to 'OFF'. */  /* Not used any more -- Percy */
		QLabel *m_pSignalLabel;

	QGroupBox *m_pActiveChannelGroup;
		QRadioButton *m_pRadioButton[8];
//		QLabel *m_pChannelLabel[8];
		QLabel *m_pCurrentChannelLabel;

	QGroupBox *m_pChannelLevelGroup;
		VolumeGroup *m_pVolumeGroup;
		QLabel *m_pChannelLevelLabel;

	QGroupBox *m_pSubwooferPEQGroup1;
		QLabel *m_pSWPEQGainLabel1;
		ButtonSlider *m_pSWPEQGainSlider1;

		QLabel *m_pTypeLabel1, *m_pGainLabel1, *m_pFreqLabel1, *m_pQLabel1, *m_pSWPEQ1Label;
		QComboBox *m_pBellCombo1;
		QDoubleSpinBox *m_pPEQCutSpin1, *m_pPEQFreqSpin1, *m_pQSpin1;


	QGroupBox *m_pSubwooferPEQGroup2;
		QLabel *m_pSWPEQGainLabel2;
		ButtonSlider *m_pSWPEQGainSlider2;

		QLabel *m_pTypeLabel2, *m_pGainLabel2, *m_pFreqLabel2, *m_pQLabel2, *m_pSWPEQ2Label;
		QComboBox *m_pBellCombo2;
		QDoubleSpinBox *m_pPEQCutSpin2, *m_pPEQFreqSpin2, *m_pQSpin2;
//		QDoubleSpinBox *m_p

	QGroupBox *m_pPEQGroup;	/* Low-shelf and High-Shelf */
		QGroupBox *m_pLShGroup;
			QLabel *m_pLGreqLabel, *m_pLGainLabel, *m_pLSlopeLabel, *m_pLGainSliderLabel, *m_pLShelfLabel;
			QDoubleSpinBox *m_pLFreqSpin, *m_pLGainSpin, *m_pLSlopSpin;
			ButtonSlider *m_pLShelfGainSlider;

		QGroupBox *m_pHShGroup;
			QLabel *m_pHGreqLabel, *m_pHGainLabel, *m_pHSlopeLabel, *m_pHGainSliderLabel, *m_pHShelfLabel;
			QDoubleSpinBox *m_pHFreqSpin, *m_pHGainSpin, *m_pHSlopSpin;
			ButtonSlider *m_pHShelfGainSlider;

	QGroupBox *m_pGEQGroup;
		GEQWidget *m_pGEQWidget;

	bool m_slotEnableFlag;

	QTimer *m_pTimer;
	int m_autoAssistIndex;		/* Normally auto assist should be executed for 10 times in 10 seconds.  -1 means no auto assist is executed */
	int m_EQCopyIndex;	/* For copy EQ and paste EQ */

	QMenu *m_pPopupMenu;
	QAction *m_pCopyAction, *m_pPasteAction;
};


#endif
