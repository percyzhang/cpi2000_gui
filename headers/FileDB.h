#ifndef FILE_DB_H
#define FILE_DB_H

#include <QSqlDatabase>
#include <QString>
#include <QDate>
#include "Types.h"

class FileDB
{
public:
	FileDB() {}
	void setPresetsDB(QString fileName) { m_presetDBName = fileName; };
	QString getPresetsSetting(QString FileName, QString nameID);

protected:
	QString m_presetDBName;
};


#endif
