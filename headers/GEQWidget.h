#ifndef GEQ_WIDGET_H
#define GEQ_WIDGET_H

#include <QWidget>
#include "ButtonSlider.h"
#include <QLabel>
#include <MarkButton.h>
#include <QLineEdit>
#include <QGroupBox>
#include <QRadioButton>
#include "CPi2000Setting.h"
#include "ValueWidget.h"

#define FULL_MODE_GAIN  53.5
#define X_CURVE_GAIN    36.0

class GraphWidget: public QFrame
{
    Q_OBJECT

public:
//	void setGain(int maxGain, int gainStep) { m_gainMax = maxGain; m_gainStep = gainStep; } 
	GraphWidget(QWidget *parent);
	void setValue(int i, float value);
	void setRTAMode(RTA_MODE rtaMode);
	void setAverageValue(float averageValue);
//    ~EQWidget();

protected:
	void setFullStandard();
	void setSWLevelStandard();
	void resizeEvent(QResizeEvent * /* event */);
//	void paintEvent(QPaintEvent *event);
	ValueWidget *m_valueWidget[30];
	RTA_MODE m_RTAMode;
	float m_fullStandardDB;
};

class MarkWidget:public QWidget
{
    Q_OBJECT
public:

	MarkWidget(QWidget *parent);
	void setRTAMode(RTA_MODE mode);
	void refresh();

protected:
	void paintEvent(QPaintEvent *event);
	void paintMark(QPainter & painter, QString strMark, float percent, QColor color);
	void paintLevel(QPainter & painter, float percent, QColor color);

protected:
	RTA_MODE m_RTAMode;
};

class GEQSlider : public QSlider
{
public:
	GEQSlider(QWidget *pParent);
protected:
	void keyPressEvent(QKeyEvent *event);
};

class GEQWidget : public QFrame
{
    Q_OBJECT
public:
	GEQWidget(QWidget *parent);
	void refreshGEQ();
	void refreshRTAMode();
	void retranslateUi();

protected slots:
	void onRTAChanged();
//	void onTimerOut();
	void onGEQSliderChanged_0(int slider) { onGEQSliderChanged(0, slider); }
	void onGEQSliderChanged_1(int slider) { onGEQSliderChanged(1, slider); }
	void onGEQSliderChanged_2(int slider) { onGEQSliderChanged(2, slider); }
	void onGEQSliderChanged_3(int slider) { onGEQSliderChanged(3, slider); }
	void onGEQSliderChanged_4(int slider) { onGEQSliderChanged(4, slider); }
	void onGEQSliderChanged_5(int slider) { onGEQSliderChanged(5, slider); }
	void onGEQSliderChanged_6(int slider) { onGEQSliderChanged(6, slider); }
	void onGEQSliderChanged_7(int slider) { onGEQSliderChanged(7, slider); }
	void onGEQSliderChanged_8(int slider) { onGEQSliderChanged(8, slider); }
	void onGEQSliderChanged_9(int slider) { onGEQSliderChanged(9, slider); }
	void onGEQSliderChanged_10(int slider) { onGEQSliderChanged(10, slider); }
	void onGEQSliderChanged_11(int slider) { onGEQSliderChanged(11, slider); }
	void onGEQSliderChanged_12(int slider) { onGEQSliderChanged(12, slider); }
	void onGEQSliderChanged_13(int slider) { onGEQSliderChanged(13, slider); }
	void onGEQSliderChanged_14(int slider) { onGEQSliderChanged(14, slider); }
	void onGEQSliderChanged_15(int slider) { onGEQSliderChanged(15, slider); }
	void onGEQSliderChanged_16(int slider) { onGEQSliderChanged(16, slider); }
	void onGEQSliderChanged_17(int slider) { onGEQSliderChanged(17, slider); }
	void onGEQSliderChanged_18(int slider) { onGEQSliderChanged(18, slider); }
	void onGEQSliderChanged_19(int slider) { onGEQSliderChanged(19, slider); }
	void onGEQSliderChanged_20(int slider) { onGEQSliderChanged(20, slider); }
	void onGEQSliderChanged_21(int slider) { onGEQSliderChanged(21, slider); }
	void onGEQSliderChanged_22(int slider) { onGEQSliderChanged(22, slider); }
	void onGEQSliderChanged_23(int slider) { onGEQSliderChanged(23, slider); }
	void onGEQSliderChanged_24(int slider) { onGEQSliderChanged(24, slider); }
	void onGEQSliderChanged_25(int slider) { onGEQSliderChanged(25, slider); }
	void onGEQSliderChanged_26(int slider) { onGEQSliderChanged(26, slider); }
	void onFullModeChanged(bool flag);

	
										   
protected:
	void resizeEvent(QResizeEvent * /* event */);
	void onGEQSliderChanged(int index, int slider);
	void keyPressEvent(QKeyEvent *event);
//	void refreshBypassButton();
//	void refreshGEQ(int index);

protected:
	QLabel *m_pFreqLabel[27];
//	ButtonSlider *m_pGEQSlider[27];
	GEQSlider  *m_pGEQSlider[27];
	QFrame *m_pSliderLine[28];
	QLabel *m_pFreqLabel_fix[30];
	QFrame *m_pHorizontalLine;

//	QLabel *m_pMarkLabel_0L, *m_pMarkLabel_6L, *m_pMarkLabel__6L, *m_pMarkLabel_0R, *m_pMarkLabel_6R, *m_pMarkLabel__6R;
//	QLabel *m_pBandEQLabel;
//	QPushButton *m_pFlattenButton;
//	MarkButton *m_pBypassButton;
//	QLineEdit *m_pLineEdit[27];
	bool m_slotEnableFlag;
	QFrame *m_pPaintFrame;
	QFrame *m_pSliderFrame;
	GraphWidget *m_pGraphWidget;

	QGroupBox *m_pRTARangeGroup;
		QRadioButton *m_pFullRadio;
		QRadioButton *m_pXCurveRadio;
		QLabel *m_pModeLabel;

	MarkWidget *m_pMarkFrame;

public:
	QGroupBox *m_pEQAssistGroup;
		QPushButton *m_pAssistButton, *m_pFlattenButton, *m_pCopyButton, *m_pPasteButton;
		QLabel *m_pGEQLabel;
};


#endif
