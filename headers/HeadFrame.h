#ifndef HEAD_FRAME_H
#define HEAD_FRAME_H

#include <QFrame>
#include <QLabel>
#include <QEvent>

class HeadFrame : public QFrame
{
public:
	HeadFrame(QString title, QWidget *pParent, QString pngName = "");
	void retranslateUi(QString title);

protected:
	void resizeEvent(QResizeEvent * /* event */);
	void changeEvent(QEvent *e);

protected:
	QLabel *m_pTitleLabel, *m_pIconLabel;
};
#endif
