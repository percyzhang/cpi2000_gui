#ifndef INPUT_SETTING_WIDGET_H
#define INPUT_SETTING_WIDGET_H

#include <QWidget>
#include <QSlider>
#include <QGroupBox>
#include <QPushButton>
#include <QSpinBox>
#include <QComboBox>
#include <QRadioButton>
//#include <RadioButton.h>
#include <QButtonGroup>
#include "HeadFrame.h"
#include "ButtonSlider.h"
#include <QLabel>
#include "MarkButton.h"
#include <QDoubleSpinBox>
#include "VolumeChartWidget.h"
#include "DigitalInputSettingWidget.h"
#include "CPi2000Setting.h"

class InputSettingWidget : public QWidget
{
	Q_OBJECT

public:
	InputSettingWidget(QWidget *pParent);
	void setInputSource(INPUT_SOURCE inputSource);
	void refreshInputSource();
	void refresh();
	void refreshInputVolume();
	void retranslateUi();

private:
	void resizeEvent(QResizeEvent * /* event */);
//	void paintEvent(QPaintEvent *event);
	void mousePressEvent(QMouseEvent *event);

	void refreshDigitalDelayEnableButton();
	void refreshMicDelayEnableButton();
	void refreshAnalogDelayEnableButton();
	void refreshMusicDelayEnableButton();

	void refreshAnalogVolume();
	void refreshDigitalVolume();
	void refreshMicVolume();
	void refreshMusicVolume();

private slots:
	void onClickDigitalMuteButton(void);
	void onClickMicMuteButton(void);
	void onClickAnalogMuteButton(void);
	void onClickMusicMuteButton(void);

	void onClickDigitalEnableDelayButton();
	void onClickMicEnableDelayButton();
	void onClickAnalogEnableDelayButton();
	void onClickMusicEnableDelayButton();

	void onClickPhantomButton();
	void onApplyAnalogChannel();
	void onApplyDigitalChannel();

	void onDigitalVolumeSliderChanged(int);
	void onAnalogVolumeSliderChanged(int);
	void onMicVolumeSliderChanged(int);
	void onMusicVolumeSliderChanged(int);

	void onDigitalVolumeSpinBoxChanged(double);
	void onAnalogVolumeSpinBoxChanged(double);
	void onMicVolumeSpinBoxChanged(double);
	void onMusicVolumeSpinBoxChanged(double);

	void onMusicDelaySpinBoxChanged(int);
	void onMicDelaySpinBoxChanged(int);
	void onAnalogDelaySpinBoxChanged(int);
	void onDigitalDelaySpinBoxChanged(int);

	void onMicCenterRadioToggled(bool);
	void onMusicCenterRadioToggled(bool);

private:
	HeadFrame *m_pDigitalFrame, *m_pAnalogFrame, *m_pMicFrame, *m_pMusicFrame;
	ButtonSlider *m_pDigitalVolumeSlider, *m_pAnalogVolumeSlider, *m_pMicVolumeSlider, *m_pMusicVolumeSlider;
	QLabel *m_pDigitalVolumeLabel, *m_pAnalogVolumeLabel, *m_pMicVolumeLabel, *m_pMusicVolumeLabel;
	MarkButton *m_pDigitalMuteButton, *m_pAnalogMuteButton, *m_pMicMuteButton, *m_pMusicMuteButton;
	QDoubleSpinBox *m_pDigitalVolumeSpinBox, *m_pAnalogVolumeSpinBox, *m_pMicVolumeSpinBox, *m_pMusicVolumeSpinBox;
	QLineEdit *m_pDigitalVolumeEdit, *m_pAnalogVolumeEdit, *m_pMicVolumeEdit, *m_pMusicVolumeEdit;

	VolumeChartWidget *m_pDigitalVolumeChart, *m_pAnalogVolumeChart, *m_pMicVolumeChart, *m_pMusicVolumeChart;
	DigitalInputSettingWidget *m_pDigitalInputSettingWidget, *m_pAnalogInputSettingWidget;
	QLabel *m_pDigitalDelayLabel, *m_pAnalogDelayLabel, *m_pMusicDelayLabel, *m_pMicDelayLabel;
	QSpinBox *m_pDigitalDelaySpinBox, *m_pAnalogDelaySpinBox, *m_pMicDelaySpinBox, *m_pMusicDelaySpinBox;

	MarkButton *m_pDigitalEnableButton, *m_pAnalogEnableButton, *m_pMicEnableButton, *m_pMusicEnableButton;

	QLabel *m_pMicChannelLabel, *m_pMicPhantomLabel;
	QRadioButton *m_pMicCenterRadio, *m_pMicSurroundRadio;
	MarkButton *m_pMicPhantomButton;

	QLabel *m_pMusicChannelLabel;
	QRadioButton *m_pMusicAssignRadio[4];
	bool m_slotEnableFlag;
};


#endif
