#ifndef LOG_WIDGET_H
#define LOG_WIDGET_H

#include <QWidget>
#include <QPixmap>
#include <QStylePainter>
#include <QTableWidget>
#include <QDate>
#include <QTime>
#include <QPushButton>
#include "CPi2000Setting.h"
#include <QCheckBox>

class LogWidget : public QWidget
{
    Q_OBJECT
public:
    LogWidget(QWidget *parent);
	void refreshLog();
	void refreshButtonStatus(bool connectFlag);
	void clearLog();
	void retranslateUi();

signals:

protected slots:
	void onRefreshClicked();
	void onSaveClicked();
	void onClearClicked();
	void onInfoClicked();
	void onWarningClicked();
	void onErrorClicked();
	void onCriticalClicked();

protected:
	void resizeEvent(QResizeEvent * /* event */);
	void paintEvent(QPaintEvent *event);

	int readNewLog(Log *pLog);

protected:
	QTableWidget *m_pLogTable;
	QPushButton *m_pRefreshButton, *m_pSaveButton, *m_pClearButton;
	QCheckBox *m_pInfoBox, *m_pWarningBox, *m_pErrorBox, *m_pCriticalBox;
};


#endif
