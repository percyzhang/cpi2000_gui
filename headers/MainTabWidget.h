#ifndef MainTabWidget_H
#define MainTabWidget_H

#include <QTabWidget>

class MainTabWidget : public QTabWidget
{
	Q_OBJECT

public:
	MainTabWidget(QWidget *pParent);
	QColor GetColor(int x, int y);

public slots:
	void onTabChanged(int nIndex);

private:
//	void paintEvent(QPaintEvent *event);

	int	m_eqLength;					// length for EQ Button
	int m_roundRadius;				// round radius of the frame
	int m_frameSize;				// frame shadow size
	int m_tabRightOffset;			// right offset of the tabBar
	int m_slop;						// slop of the Tab
	int m_tabRoundRadius;			// Round Radius of Tab
	int m_tabHeight, m_tabWidth;	// height and width of the tab
};
















#endif
