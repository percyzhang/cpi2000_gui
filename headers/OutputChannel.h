#ifndef OUTPUT_CHANNEL_H
#define OUTPUT_CHANNEL_H

#include <QObject>
#include <QColor>
#include "types.h"

#define MIN_Q 0.1
#define MAX_Q 16.0

#define MIN_SLOPE 3.0
#define MAX_SLOPE 15.0


#define MIN_FIR_FREQ  400.0

/*
	Must match the following definition:

	enum filterType { PEAKING, EQ_L_SHELF, EQ_H_SHELF, EQ_NOTCH, EQ_LPF, EQ_HPF, EQ_BPF ,EQ_APF};
*/

class EQData
//	: public QObject
{
public:
	EQData();
	EQData(EQType type, qreal frequency, qreal Q, qreal gain, PF_IIR_Type pfType, bool enableFlag);
	EQData(EQData & curve);
	void dBCalc(int pointNum, qreal *pHorizontal, qreal *pVertical);
	qreal dBCalc(qreal freq);
	bool isInclude(qreal freq, qreal mag);
	void setType(EQType type);
	EQType getType();
	void setFrequency(qreal frequency);
	void setFrequency(QString frequency);
	qreal getFrequency();
	void setQ(qreal Q);
	qreal getQ();
	void setGain(qreal gain);
	qreal getGain();
	void enable(bool flag);
	bool getEnableFlag();
	qreal getFreqForQ(int index);
	qreal calcLeftQ(qreal leftFreq);  /* Only valid for PEQ */
	qreal calcRightQ(qreal rightFreq);	/* Only valid for PEQ */
	void setPFIIRType(PF_IIR_Type type) { m_PFIIRType = type; }
	PF_IIR_Type getPFIIRType() { return m_PFIIRType; }
	void setSlope(qreal slope) { m_slope = slope; }
	qreal getSlope()  { return m_slope; }
	void setPFType(PF_TYPE pfType) { m_pfType = pfType; }
	PF_TYPE getPFType() { return m_pfType; }
	int getFIRTab()	{ return m_FIRtab; }
	void setFIRTab(int tab) { m_FIRtab = tab; } 
	void setUserDefinedFIR(QString userDefinedFIR);
	QString getUserDefinedFIR() { return m_userDefinedFIR;}
protected:
	QString m_userDefinedFIR;
	EQType m_type;
	qreal m_frequency, m_gain;	/* Valid for Bell, L-Shelf and H-Shelf */
	qreal m_Q;			/* Q for Bell */ 
	qreal m_slope;		/* Slope for L-shelf and H-Shelf */
	PF_TYPE m_pfType;					/* Only valid for HPF and LPF : IIR and FIR */ 
	PF_IIR_Type m_PFIIRType;	/* Only valid for HPF and LPF - IIR */
	int	m_FIRtab;					/* Only valid for HPF and LPF - FIR */
	bool m_enableFlag;
};


class OutputChannel
{
public:
	OutputChannel();

	EQData * getLShelf()  { return &m_lShelf; }
	EQData * getHShelf()  { return &m_hShelf; }

	void setGEQ(int geqIndex, float gain) {  m_GEQ[geqIndex] = gain; }
	float getGEQ(int geqIndex) {  return m_GEQ[geqIndex]; }

	void setRoomLevelGain(float gain) { m_roomLevelGain = gain;  }
	float getRoomLevelGain(void)	{ return m_roomLevelGain; }

	void setRoomLevelMute(bool muteFlag) { m_roomLevelMute = muteFlag;  }
	bool getRoomLevelMute() { return m_roomLevelMute; }

	void flattenGEQ();

public:
	float m_GEQ[27];

	EQData m_lShelf, m_hShelf;	
	float m_roomLevelGain;
	bool m_roomLevelMute;
};

#endif