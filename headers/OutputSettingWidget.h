#ifndef OUTPUT_SETTING_WIDGET_H
#define OUTPUT_SETTING_WIDGET_H

#include <QWidget>
#include <QSlider>
#include <QGroupBox>
#include <QPushButton>
#include <QSpinBox>
#include <QComboBox>
#include <QFrame>
#include <QButtonGroup>
#include "OutputTabWidget.h"
#include "RoomLevelWidget.h"
#include "EQTuningWidget.h"

class OutputSettingWidget : public QWidget
{
	Q_OBJECT

public:
	OutputSettingWidget(QWidget *pParent);
	void refresh();
	int getCurrentTabIndex() { return m_pOutputTabWidget->currentIndex(); }
	void refreshMusicVolume();
	void retranslateUi();

private:
	void resizeEvent(QResizeEvent * /* event */);
//	void onTabChanged(int nIndex);

private slots:
	void onCurrentChanged(int currentIndex);
	void onSignalModeChanged(int index);


private:
	MarkButton *m_pGEQTuningButton, *m_pPEQTuningButton;
	OutputTabWidget *m_pOutputTabWidget;
	RoomLevelWidget *m_pRoomLevelWidget;
	EQTuningWidget *m_pEQTuningWidget;
};


#endif
