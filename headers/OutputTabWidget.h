#ifndef OUTPUT_TAB_BAR_H
#define OUTPUT_TAB_BAR_H

#include <QWidget>
#include <QSlider>
#include <QGroupBox>
#include <QPushButton>
#include <QSpinBox>
#include <QComboBox>
//#include <RadioButton.h>
#include <QButtonGroup>

class OutputTabWidget : public QTabWidget
{
	Q_OBJECT

public:
	OutputTabWidget(QWidget *pParent);

private:
//	void resizeEvent(QResizeEvent * /* event */);
	void paintEvent(QPaintEvent *event);

private slots:
	void onTabChanged(int nIndex);

private:
	int	m_eqLength;					// length for EQ Button
	int m_roundRadius;				// round radius of the frame
	int m_frameSize;				// frame shadow size
	int m_tabRightOffset;			// right offset of the tabBar
	int m_slop;						// slop of the Tab
	int m_tabRoundRadius;			// Round Radius of Tab
	int m_tabHeight, m_tabWidth;	// height and width of the tab
};


#endif
