#ifndef OVERVIEW_WIDGET_H
#define OVERVIEW_WIDGET_H

#include <QWidget>
#include <QSlider>
#include <QGroupBox>
#include <QPushButton>
#include <QSpinBox>
#include <QComboBox>
#include "BackgroundWidget.h"
#include <QButtonGroup>
#include <QLabel>
#include "RadioButton.h"
#include <QHostAddress>
#include "VolumeChartWidget.h"
#include "SpeakerAndAmplifierWidget.h"
#include "CPi2000Setting.h"
#include "MarkButton.h"

class OverviewWidget : public QWidget
{
	Q_OBJECT

public:
	OverviewWidget(QWidget *pParent);

	void refresh();
	void refreshMasterVolume();
	void refreshInputSource();
	void refreshMasterMuteFlag();
	void refreshNetwork(NETWORK_SETTING networkSetting);
	void refreshSpeaker();
	void retranslateUi();
	bool isAnythingChanged();
	bool isApplyEnabled();

protected:
	void setIPAddress(QHostAddress address);
	void setSubMask(QHostAddress address);
	void setDefaultGateWay(QHostAddress address);
	void setDHCPEnable(bool flag);

private:
	void resizeEvent(QResizeEvent * /* event */);
	void refreshApplyButton();

public slots:
		void onClickApplyButton();

private slots:
	void onClickDigitalButton();
	void onClickMusicButton();
	void onClickMicButton();
	void onClickAnalogButton();
	void onClickMuteButton();
	void onLRSpeakerChanged();
	void onSpeakerAndAmplifierSettingChanged();
	void onTheaterChanged(const QString &);
	void onHallChanged(const QString &);
	void onServerChanged(const QString &);
	void onProjectorChanged(const QString &);

private:
	QLabel *m_pTheaterLabel, *m_pHallLabel, *m_pDHCPLabel, *m_pIPAddress, *m_pSubnetMaskLabel,
			*m_pGatewayLabel, *m_pServerLabel,
			*m_pProjectorLabel, *m_pCPi2000SNLabel,  *m_pCPi2000SNValue, // *m_pCXM2000SNValue, *m_pCXM2000SNLabel,
			*m_pIPDot1, *m_pIPDot2, *m_pIPDot3, *m_pSubnetMaskDot1, *m_pSubnetMaskDot2, *m_pSubnetMaskDot3,
			*m_pGatewayDot1, *m_pGatewayDot2, *m_pGatewayDot3, *m_pSpeakerAndAmplifierLabel, *m_pDHCPResultLabel;
		
	QLineEdit *m_pTheaterEdit, *m_pHallEdit, *m_pIPEdit1, *m_pIPEdit2, *m_pIPEdit3, *m_pIPEdit4, 
			*m_pSubnetMaskEdit1, *m_pSubnetMaskEdit2, *m_pSubnetMaskEdit3, *m_pSubnetMaskEdit4, 
			*m_pGatewayEdit1, *m_pGatewayEdit2, *m_pGatewayEdit3, *m_pGatewayEdit4, *m_pServerEdit,
			*m_pProjectorEdit;

	QRadioButton *m_pDHCPEnableButton, *m_pDHCPDisableButton;

	SpeakerAndAmplifierWidget *m_pSpeakerAndAmplifierWidget;
	BackgroundWidget *m_pDeviceImageWidget;
	MarkButton *m_pMuteButton, *m_pMicButton, *m_pMusicButton, *m_pDigitalButton, *m_pAnalogButton, *m_pMenuButton;

//	VolumeChartWidget *m_pVolumeWidget;
	QLabel	*m_pVolumeLabel, *m_pDeviceHallLabel;
	QLabel *m_pReminderLabel;

	QPushButton *m_pApplyButton;
};


#endif
