#ifndef PANEL_WIDGET_H
#define PANEL_WIDGET_H

#include "VolumeWidget.h"
#include <QLabel>
#include <QTimer>
#include "ButtonSlider.h"
#include "MarkButton.h"
#include "backgroundWidget.h"

class PanelWidget : public QWidget
{
    Q_OBJECT
public:
    PanelWidget(QWidget *parent);

signals:
	void onSetPercent(int percent);

protected slots:
	void onTimer();
	void onClickMuteButton();

protected:
	void resizeEvent(QResizeEvent * /* event */);

protected:
	QLabel *m_pInputLabel;
	VolumeWidget *m_pVolumeWidget;
	QTimer *m_pTimer;
	ButtonSlider *m_pGeneralVolume;
	MarkButton *m_pMuteButton;
	BackgroundWidget *m_pDolbyWidget;
};


#endif
