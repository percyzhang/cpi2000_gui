#ifndef PRESETS_DB_H
#define PRESETS_DB_H

#include <QSqlDatabase>
#include <QString>
#include "types.h"
#include <SpeakerData.h>
#include "CPi2000Setting.h"

class PresetsDB
{
public:
	PresetsDB(void);
    void init(void);
	~PresetsDB(void);

	void setPresetDBFileName(QString presetDBFileName) { m_presetDBFileName = presetDBFileName; }
	QString getJSON(QString name);
	bool setJSON(const QString &filed, const QString &content);
	void setSpeakerDBFileName(QString speakerDBFileName) { m_strSpeakerDBFileName = speakerDBFileName; }
	bool getSpeakerList(QStringList &speakerList);
	bool addSpeaker(QString speakerName, QString speakerDescription);
	bool addSpeaker(QString speakerName, SpeakerData *pSpeakerData);
	bool deleteSpeaker(QString speakerName);
	bool getSpeaker(QString speakerName, QString &description);
	bool getSpeaker(QString speakerName, SpeakerData &speakerData);
	bool modifySpeaker(QString speakerName, QString description);
	bool modifySpeaker(QString speakerName, SpeakerData *pSpeakerData);

	bool getSpeaker(SPEAKER_CHANNEL speakerChannel, SpeakerData &speakerData);

protected:	
	QString m_presetDBFileName, m_strSpeakerDBFileName;
	QSqlDatabase m_database;
};

extern PresetsDB g_database;


#endif
