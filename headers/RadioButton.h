#ifndef RADIOBUTTON_H
#define RADIOBUTTON_H

#include "QRadioButton"

class RadioButton : public QRadioButton
{
    Q_OBJECT
public:
	RadioButton(QWidget *pParent);
	RadioButton(const QString &text, QWidget *parent=0);

protected:
	void paintEvent(QPaintEvent *event);
};


#endif
