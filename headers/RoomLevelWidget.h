#ifndef ROOM_LEVEL_WIDGET_H
#define ROOM_LEVEL_WIDGET_H

#include <QWidget>
#include <QSlider>
#include <QGroupBox>
#include <QPushButton>
#include <QSpinBox>
#include <QComboBox>
#include <QButtonGroup>
#include <MarkButton.h>
#include <QLabel>
#include <QLineEdit>
#include "VolumeGroup.h"
#include <QCheckBox>
#include "ValueWidget.h"

class RoomLevelWidget : public QWidget
{
	Q_OBJECT

public:
	RoomLevelWidget(QWidget *pParent);
	void refreshPinkNoise();
	void refresh();
	void refreshLabel();
	void refreshSPL();
	void retranslateUi();

private:
	void toggleMuteButton(int index, bool flag);
	void onChangeVolume(int index, float value);
	void resizeEvent(QResizeEvent * /* event */);
	void keyPressEvent(QKeyEvent *event);
//	void paintEvent(QPaintEvent *event);

private slots:
	void onClickPinkNoise_0() { onClickPinkNoise(0); }
	void onClickPinkNoise_1() { onClickPinkNoise(1); }
	void onClickPinkNoise_2() { onClickPinkNoise(2); }
	void onClickPinkNoise_3() { onClickPinkNoise(3); }
	void onClickPinkNoise_4() { onClickPinkNoise(4); }
	void onClickPinkNoise_5() { onClickPinkNoise(5); }
	void onClickPinkNoise_6() { onClickPinkNoise(6); }
	void onClickPinkNoise_7() { onClickPinkNoise(7); }

	void toggleMuteButton_0(bool flag) { toggleMuteButton(0, flag); }
	void toggleMuteButton_1(bool flag) { toggleMuteButton(1, flag); }
	void toggleMuteButton_2(bool flag) { toggleMuteButton(2, flag); }
	void toggleMuteButton_3(bool flag) { toggleMuteButton(3, flag); }
	void toggleMuteButton_4(bool flag) { toggleMuteButton(4, flag); }
	void toggleMuteButton_5(bool flag) { toggleMuteButton(5, flag); }
	void toggleMuteButton_6(bool flag) { toggleMuteButton(6, flag); }
	void toggleMuteButton_7(bool flag) { toggleMuteButton(7, flag); }

	void onChangeVolume_0(float value) { onChangeVolume(0, value); }
	void onChangeVolume_1(float value) { onChangeVolume(1, value); }
	void onChangeVolume_2(float value) { onChangeVolume(2, value); }
	void onChangeVolume_3(float value) { onChangeVolume(3, value); }
	void onChangeVolume_4(float value) { onChangeVolume(4, value); }
	void onChangeVolume_5(float value) { onChangeVolume(5, value); }
	void onChangeVolume_6(float value) { onChangeVolume(6, value); }
	void onChangeVolume_7(float value) { onChangeVolume(7, value); }

	void onSignalModeSelected(int mode);
	void onClacChanged(double);
	void onClickRotate(void);
	void onTimer_200ms();
	void onSPLChanged();
		
	void onRefresh() { refresh(); }

private:
	void onClickPinkNoise(int index);

private:

	QGroupBox *m_pSignalModeGroup;
		QComboBox *m_pSignalModeCombo;
		QLabel *m_pReminderLabel; /* To stop RTA setting, you need to set signal mode to 'OFF'. */  /* Not used any more -- Percy */
		QLabel *m_pSignalLabel;

	QGroupBox *m_pCalibrationGroup;
		QLabel *m_pMeterLabel, *m_pValueLabel;
		QDoubleSpinBox *m_pCalSpin; 
		QLabel *m_pLineLabel;
		CalWidget *m_pCalWidget;
//		ValueWidget *m_pCalWidget;
		QLabel *m_pMeterValue;
		QLabel *m_pCalLabel;

	QGroupBox *m_pOutputChannelGroup;


	MarkButton *m_pPinkNoiseButton[8];
	QLabel *m_pSpeakerLabel, *m_pAmpLabel;
	QLabel *m_pSpeakerName[8], *m_pAmpName[8], *m_pOutputLabel;	

	VolumeGroup *m_pVolumeGroup[8];
	QCheckBox *m_pRotateCheck;

	bool m_slotEnableFlag;

	QTimer *m_timer;
};


#endif
