#ifndef SPEAKER_AND_AMPLIFIER_WIDGET_H
#define SPEAKER_AND_AMPLIFIER_WIDGET_H

#include <QWidget>
#include <QComboBox>
#include <QLabel>
#include <QComboBox>
#include <QLineEdit>
#include <QDialog>
#include <QPushButton>
#include <QCheckBox>
#include "SpeakerData.h"

class SpeakerTypeDialog : public QDialog
{
    Q_OBJECT
public:
    SpeakerTypeDialog(QWidget *parent);

protected slots:
	void onFileButtonClicked();

protected:
	void resizeEvent(QResizeEvent * /* event */);

	QLabel *m_pSpeakerNameLabel, *m_pSpeakerEQLabel, *m_pFileLabel;
	QLineEdit *m_pSpeakerNameEdit, *m_pFileEdit;

	QPushButton *m_pOKButton, *m_pCancelButton, *m_pFileButton;
	QCheckBox *m_pEQBox;
};

class SpeakerAndAmplifierWidget : public QWidget
{
    Q_OBJECT
public:
    SpeakerAndAmplifierWidget(QWidget *parent);
	void refresh();
	void retranslateUi();
	QString getSpeakerName(int i);
	QString getAmplifierName(int i) { return m_pAmplifierEdit[i]->text(); }
	void setLRTypeMatch(SPEAKER_TYPE left, SPEAKER_TYPE right);

signals:
//	void onSetPercent(int percent);
	void changeLRSpeaker();
	void changeSpeakerAndAmplifier();

protected:
	void refreshAvailableSpeaker();
	void refreshSpeakerAndAmplifierName();

protected slots:
//	void onTimer();
//	void onClickMuteButton();
	void onSpeakerChanged_0(int index);
	void onSpeakerChanged_1(int index);
	void onSpeakerChanged_2(int index);
	void onSpeakerChanged_3(int index);
	void onSpeakerChanged_4(int index);
	void onSpeakerChanged_5(int index);
	void onSpeakerChanged_6(int index);
	void onSpeakerChanged_7(int index);
	void onAmplifierChanged(const QString &);

protected:
	void resizeEvent(QResizeEvent * /* event */);
	void paintEvent(QPaintEvent *event);
	void initUpdate();

	void onSpeakerChanged(int comboIndex, int selectIndex);

protected:
	QLabel *m_pSpeakerNameLabel[8], *m_pAmplifierNameLabel[8];
	QComboBox *m_pComboBox[8];
	QLineEdit *m_pAmplifierEdit[8];
	/* L / C / R / LFE / Ls / Rs / Bsl / Bsr */
	QLabel *m_pWarningInfo;

	bool m_slotEnableFlag;
};


#endif
