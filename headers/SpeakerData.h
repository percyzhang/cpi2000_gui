#ifndef SPEAKER_DATA_H
#define SPEAKER_DATA_H

#include <QWidget>
#include "commonLib.h"
#include "OutputChannel.h"

typedef enum
{
	SPEAKER_TYPE_PASSIVE = 0,
	SPEAKER_TYPE_BIAMP,
	SPEAKER_TYPE_TRIAMP,
	SPEAKER_TYPE_SURROUND,
	SPEAKER_TYPE_SUBWOOFER,
} SPEAKER_TYPE;

typedef struct
{
	bool		m_enableFlag;
	bool		m_autoFlag;
	float		m_threshold;		/* unit 0.1 dB */
	float		m_attackTimeInMs;	/* unit 0.05ms  */
	float		m_holdTimeInMs;		/* unit 0.05 ms */
	int			m_release;			/* unit 10 dB/s */
} LIMITER;

typedef struct
{
public:
	qreal setPFFrequency(int eqIndex, qreal freq);		/* Set Frequency for HPF and LPF */

	LIMITER			m_limiter;				/* Valid for all */
	bool			m_polarityNormalFlag;	/* Valid for all */
	float			m_delayInMs;			/* only valid for bi-amp /passive / tri-amp , unit: 0.05ms */ 
	float			m_gain;					/* Valid for all */
	EQData			m_EQData[11];			/* index 0 - 7: PEQ;  Index 8: High Path Filter; Index 9: Low Path Filter; Index 10: User defined FIR */

	/* The following is only used by GUI */
	bool			m_enableAll;			/* Enable all EQ */

	/* The following is only used speaker.db */
	bool			m_firEnableFlag;		/* only valid for bi-amp /passive / tri-amp */
	QString			m_strFIR;				/* only valid for bi-amp /passive / tri-amp */
	bool			m_PEQEnableFlag;		/* For EQData[0 - 7] */
	bool			m_HPF_IIR_EnableFlag;	/* For EQData[8]  */
	bool			m_LPF_IIR_EnableFlag;	/* For EQData[9]  */
} ChannelData;

class SpeakerData
{
public:
	SpeakerData();
	QByteArray writeToJSON();
	bool readFromJSON(const QString &json);
	bool writeToJSON(QString &description);
	void dump();
	void setSpeakerName(QString speakerName);
	QString getSpeakerName();

protected:

	void setSpeakerType(SPEAKER_TYPE speakerType);
	int getChannelCount();

	void writeJSON_common(QJsonObject & mainObject);
	void writeJSON_Triamp(QJsonObject & mainObject);
	void writeJSON_Passive(QJsonObject & mainObject);	
	void writeJSON_Biamp(QJsonObject & mainObject);
	void writeJSON_Subwoofer(QJsonObject & mainObject);	
	void writeJSON_Surround(QJsonObject & mainObject);
	void preWriteJSON(QJsonObject & mainObject);

	bool readJSON_common(QJsonObject & mainObject);
	bool readJSON_Triamp(QJsonObject & mainObject);
	bool readJSON_Passive(QJsonObject & mainObject);	
	bool readJSON_Biamp(QJsonObject & mainObject);
	bool readJSON_Subwoofer(QJsonObject & mainObject);	
	bool readJSON_Surround(QJsonObject & mainObject);
	void readJSON_patch(QJsonObject & mainObject);

public:
	QString			m_speakerName;
	SPEAKER_TYPE	m_speakerType;
	bool			m_readable;
	bool			m_writable;

	ChannelData		m_channelData[3];
	QStringList		m_channelNameList;		/* Only valid for bi-amp ("LF", "MF") and ti-amp ("LF", "MF" and "HF") */
	int				m_channelCount;			/*  bi-amp: 2; tri-amp: 3; others: 1 */ 
	QString			m_password;
	QString			m_strVersion, m_strVendor;
};

QString XOverToFIR(int tab0, double frequency0, int tab1, double frequency1);
#endif
