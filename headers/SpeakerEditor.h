#ifndef SPEAKER_EDITOR_H
#define SPEAKER_EDITOR_H

#include <QDialog>
#include <QLabel>
#include <QListWidget>
#include <QPushButton>
#include <QLineEdit>
#include "SpeakerData.h"
#include <QComboBox>
#include <QCheckBox>
#include <MarkButton.h>
#include "EQWidget.h"
#include <QGroupBox>
#include <QDoubleSpinBox>
#include "ButtonSlider.h"
#include "EQButton.h"
#include <QRadioButton>
#include <QTextEdit>
#include "CPi2000Setting.h"
#include <QScrollArea>

class PasswordDlg: public QDialog
{
    Q_OBJECT

public:
    PasswordDlg(QWidget *parent);
	void setPassword(QString password);
	QString getPassword();

protected slots:
	void onClickOk();
	void onClickCancel();
	void onClickEnable();
	void onTextChanged(const QString &);

protected:
	void resizeEvent(QResizeEvent * /* event */);
	void refresh(void);
	void refreshButtonStatus();

protected:
	QString m_password;

	QCheckBox *m_pEnableBox;
	QLabel *m_pPasswordLabel1, *m_pPasswordLabel2, *m_pDescription;
	QLineEdit *m_pPasswordEdit1, *m_pPasswordEdit2;

	QPushButton *m_pOKButton, *m_pCancelButton;
};

class SpeakerEditor;

class SpeakerEQWidget: public EQWidget
{
    Q_OBJECT
public:
	SpeakerEQWidget(SpeakerEditor *m_SpeakerEditor, QWidget *parent = 0);

	virtual int getEQCount();
	virtual int getCurrentEQIndex();
	virtual EQData *getEQ(int index);
	virtual EQData *getCurrentEQ();
	virtual bool getEnableAllFlag();
	virtual void clearFinalCurve();

	SpeakerEditor *m_pSpeakerEditor;
};

class SpeakerEditor : public QDialog
{
    Q_OBJECT
public:
    SpeakerEditor(QWidget *parent);
	~SpeakerEditor();
	void setSpeakerData(SpeakerData *pSpeakerData);
	SpeakerData *getSpeakerData() { return &m_speakerData; }
	int getEQCount();
	int getCurrentEQIndex();
	EQData * getCurrentEQ();
	EQData * getEQ(int index);
	ChannelData *getCurrentChannelData();
	void setApplyChannel(bool channelFlag[8]);

	void enableApplyButton();

signals:
	void changeLimiterEnable(int currentEQIndex, bool currentEnableFlag);
	void changeLimiterAutoFlag(int currentEQIndex, bool currentAutoFlag);
	void changeLimiterRelease(int currentEQIndex, int currentRelease);
	void changeLimiterAttack(int currentEQIndex, double currentAttack);
	void changeLimiterHold(int currentEQIndex, double currentHold);
	void changeLimiterThreshold(int currentEQIndex, double currentThreshold);
	void changeFIRContent(int currentEQIndex, QString currentFIRContent);
	void changeFIREnable(int currentEQIndex, bool currentFIREnableFlag);
	void changeGeneralGain(int currentEQIndex, double currentGeneralGain);
	void changeGeneralDelay(int currentEQIndex, double currentGeneralDelayInMS);
	void changeGeneralPolarity(int currentEQIndex, bool currentPolarityNormalFlag);
	void applyToDevice(SpeakerData *pSpeakerData);	/* When the user click the button "Save to Device" */

protected slots:
	void onWritableClicked();
	void onReadableClicked();
	void onClickSaveButton();
	void onClickLoadButton();
	void onClickPasswordButton();
	void onClickEQTab_0();
	void onClickEQTab_1();
	void onClickEQTab_2();
	void onClickLimiterEnableButton();
	void onClickLimiterAuto();
	void onLimiterReleaseChanged(int value);
	void onLimiterAttackChanged(double value);
	void onClickCancelButton();
	void onLimiterHoldChanged(double value);
	void onLimiterThresholdChanged(double value);
	void onClickFIREnable();
	void onClickFIRDefault();
	void onClickFIRPaste();
	void onGeneralGainChanged(double value);
	void onGeneralDelayChanged(double value);
	void onClickGeneralPolarity();

	void onClickEQIndex1();
	void onClickEQIndex2();
	void onClickEQIndex3();
	void onClickEQIndex4();
	void onClickEQIndex5();
	void onClickEQIndex6();
	void onClickEQIndex7();
	void onClickEQIndex8();
	void onClickEQIndex9();
	void onClickEQIndex10();
	void onClickEQIndex11();

	void onDblClickEQIndex1();
	void onDblClickEQIndex2();
	void onDblClickEQIndex3();
	void onDblClickEQIndex4();
	void onDblClickEQIndex5();
	void onDblClickEQIndex6();
	void onDblClickEQIndex7();
	void onDblClickEQIndex8();
	void onDblClickEQIndex9();
	void onDblClickEQIndex10();
	void onDblClickEQIndex11();

	void onClickApplyButton();
	void onGainChanged(double value);
	void onFreqChanged(double value);
	void onFreqChanged_PF(double);
	void onGainSliderChanged(int);
	void onQChanged(double value);
	void onClickAllPEQEnable();
	void onClickPFEnable();
	void onClickPEQDelete();
	void onPEQTypeChanged(int);
	void onClickPEQEnable();

	void onPEQSlopeChanged(double slop);
	void onSetPFFreq(qreal freq);	/* This message is sent from EQWidget */
	void onCurrentEQChanged();	/* This message is sent from EQWidget */
	void onCurrentEQIndexChanged(int i);	/* This message is sent from EQWidget */
	void onPFTypeToggled(bool checked);
	void onPFSlopeChanged(int slope);
	void onTabChanged(int tab);
	void onPFSliderChanged(int value);
	void onSignalModeSelected(int mode);

	void onClickChannelButton0();
	void onClickChannelButton1();
	void onClickChannelButton2();
	void onClickChannelButton3();

protected:
	void resizeEvent(QResizeEvent * /* event */);
	void refresh(void);
	void refreshReadableButton(); /* refresh readable button and password button */
	void refreshLimiter();
//	void refreshFIR();
	void refreshEQGroup();
	void onClickEQIndex(int eqIndex);
	void onDblClickEQIndex(int eqIndex);
	void refreshEQIndexButton();

	EQData * getHPF();
	EQData * getLPF();
	EQData * getFIR();

	void refreshPEQGroup();
	void refreshPFGroup();
	void refreshFIRGroup();

	void getSelectedChannel(bool channelFlag[8]);
	void onClickChannelButton(int i);

protected:
	QWidget *m_pWidget;
	QScrollArea *m_pScrollArea;
//	QListWidget *m_pSpeakerListWidget;
//	QStringList m_speakerList;
//	QPushButton *m_pNewButton, *m_pDeleteButton, *m_pModifyButton, *m_pCancelButton;

//	QLabel *m_pDescriptionLabel;

//	QString m_selectedSpeakerName; // The file name that selected by user. if null, means the user select to save as new file.

	QPushButton *m_pDefaultButton;

	SpeakerData m_speakerData;

	QLabel *m_pSpeakerNameLabel, *m_speakerTypeLabel;
	QLineEdit *m_pSpeakerNameEdit;
	QComboBox *m_pSpeakerTypeCombo;
	QCheckBox *m_pWritableCheck;

	MarkButton *m_pEQTabButton[3];
	QFrame *m_pChannelFrame, *m_pBottomFrame;

	EQButton *m_pEQButton[11];

	MarkButton *m_pPEQEnableButton, *m_pHPFEnableButton, *m_pLPFEnableButton;

	SpeakerEQWidget *m_pEQWidget;

	QPushButton *m_pLoadButton, *m_pSaveButton, *m_pApplyButton, *m_pCancelButton;

	QGroupBox *m_pPEQGroupBox, *m_pPFGroupBox, *m_pFIRGroupBox, *m_pGeneralGroupBox, *m_pLimiterGroupBox;
	QLabel *m_pFIRLabel;
	QPushButton *m_pFIRDefaultButton, *m_pFIRPasteButton;
	QTextEdit *m_pFIREdit;
	MarkButton *m_pFirEnableButton;

	QLabel *m_TypeLabel, *m_pSlopeLabel, *m_pFrequencyLabel, *m_pQLabel, *m_pGainLabel, *m_pGainSliderLabel;
	ButtonSlider *m_pGainSlider;
	MarkButton *m_pEnableButton_PEQ, *m_pEnableButton_PF;
	QComboBox *m_pTypeCombo, *m_pSlopeCombo;
	QDoubleSpinBox *m_pSlopeBox;
	QPushButton *m_pDeleteButton;
	
	QDoubleSpinBox  *m_pFrequencySpin, *m_pQSpin, *m_pGainSpin;

//	QLabel *m_pPolarityLabel;
	QLabel *m_pDelayLabel, *m_pGainLabel2;
	QDoubleSpinBox *m_pGainSpin2;
	QDoubleSpinBox *m_pDelaySpin;
	MarkButton *m_pPolarityButton;

	QLabel *m_pThresholdLabel, *m_pAttackLabel, *m_pReleaseLabel, *m_pHoldLabel;
	MarkButton *m_pLimiterEnableButton;
	QCheckBox *m_pLimiterAutoCheck;
	QDoubleSpinBox *m_pThresholdSpin, *m_pAttackSpin, *m_pHoldSpin;
	QSpinBox *m_pReleaseSpin;
	QLabel *m_pPEQIndexLabel, *m_pCrossOverEQIndexLabel;
	bool m_slotEnableFlag;

	QGroupBox *m_pGeneratorGroup;
	QLabel *m_pSignalModeLabel, *m_pChannelLabel;
	QComboBox *m_pSignalModeCombo;
	MarkButton *m_pChannelButton[4];


	/* The following control is for HPF and LPF */
	QGroupBox *m_pTypeGroup;
	QRadioButton *m_pIIRRadio, *m_pFIRRadio;
	QLabel *m_pPFSlopeLabel, *m_pTabLabel, *m_pPFFrequencyLabel, *m_pPFFrequencyLabel1;
	QComboBox *m_pSlopCombo_PF;
	QSpinBox *m_pTabSpin;
	QDoubleSpinBox *m_pFreqSpin_PF;
	ButtonSlider *m_pPFSlider;


	int m_currentEQTab;	/* 0 - 2 */
	int m_currentEQIndex[3];
	int m_currentPEQIndex[3]; /* 0 - 7 */
	int m_currentCrossoverIndex[3]; /* 8 - 10 */

	int m_channelCount; /* 1 - 4 */
	SPEAKER_CHANNEL m_applyChannel[4];	/* We apply the speaker at most 4 channel */
};


#endif
