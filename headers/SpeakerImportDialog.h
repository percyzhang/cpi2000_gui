#ifndef SPEAKER_IMPORT_DIALOG_H
#define SPEAKER_IMPORT_DIALOG_H

#include <QDialog>
#include <QLabel>
#include <QListWidget>
#include <QPushButton>
#include <QLineEdit>
#include <QCheckBox>
#include <QRadioButton>
#include <QGroupBox>
#include <QComboBox>
#include "SpeakerData.h"


class SpeakerImportDialog : public QDialog
{
    Q_OBJECT
public:
    SpeakerImportDialog(QWidget *parent);
	void setSpeakerDB(QString localSpeakerDB, QString importFileName) {  m_locapSpeakerDB = localSpeakerDB;   m_importFileName = importFileName; }
	
signals:

protected slots:
	void onCancel();
	void onImport();

protected:
	void resizeEvent(QResizeEvent * /* event */);
	void refresh(void);

protected:

	QLabel *m_pSpeakerList;
	QPushButton *m_pImportButton, *m_pCancelButton;

	QString m_locapSpeakerDB, m_importFileName;
};


#endif
