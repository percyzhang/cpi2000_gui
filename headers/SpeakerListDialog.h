#ifndef SPEAKER_LIST_DIALOG_H
#define SPEAKER_LIST_DIALOG_H

#include <QDialog>
#include <QLabel>
#include <QListWidget>
#include <QPushButton>
#include <QLineEdit>
#include <QCheckBox>
#include <QRadioButton>
#include <QGroupBox>
#include <QComboBox>
#include "SpeakerData.h"

typedef enum
{
	SPEAKER_OPERATION_CANCEL = 0,
	SPEAKER_OPERATION_NEW,
	SPEAKER_OPERATION_DELETE,
	SPEAKER_OPERATION_MODIFY,
} SPEAKER_OPERATION;

typedef struct
{
	QString m_speakerName;
	SPEAKER_OPERATION m_operation;
	SPEAKER_TYPE m_speakerType;
	bool m_speakerChannelApplied[8];
} SpeakerDialogResult;

typedef struct
{
	bool m_newSpeakerFlag;
	bool m_hideFlag;
	QString m_speakerName;
	SPEAKER_TYPE m_speakerType;
	bool m_channel_L, m_channel_R, m_channel_C, m_channel_SW, m_channel_LS, m_channel_RS, m_channel_BLS, m_channel_BRS;
	bool m_readable, m_writable;
} SpeakerListData;

class SpeakerListDialog : public QDialog
{
    Q_OBJECT
public:
    SpeakerListDialog(QWidget *parent);
	void setSpeakerList(QStringList speakerList);
	SpeakerDialogResult *getOperation() { return &m_result; }
	
signals:

protected slots:
	void onCancel();
	void onDelete();
	void onClickEdit();
	void onSpeakerChanged(int currentRow);
	void onSpeakerTypeChanged(int index);
	void onNewSpeakerClicked();
	void onRefreshButton();
	void onClickLeftChannel();
	void onClickRightChannel();
	void onSpeakerNameChanged(const QString &);
	void onHideClicked();

	void onClickC();	
	void onClickSW();	
	void onClickLS();	
	void onClickRS();	
	void onClickBLS();	
	void onClickBRS();	

protected:
	void resizeEvent(QResizeEvent * /* event */);
	void refresh(void);
	void refreshButton(void);
	void refreshSpeaker(void);

protected:
	QCheckBox *m_pNewSpeakerCheck;

	QListWidget *m_pSpeakerListWidget;
	QStringList m_speakerList;

	QLabel *m_pDescriptionLabel, *m_pSpeakerNameLabel;
	QLineEdit *m_pSpeakerNameEdit;

	QString m_selectedSpeakerName; // The file name that selected by user. if null, means the user select to save as new file.

	QLabel *m_pSpeakerTypeLabel, *m_pExistLabel;
	QComboBox *m_pSpeakerTypeCombo;

	QGroupBox *m_pApplyGroup;
	QCheckBox *m_pLeft, *m_pRight, *m_pCenter, *m_pLS, *m_pRS, *m_pBLS, *m_pBRS, *m_pSW;

	QGroupBox *m_pPriorityGroup;
	QCheckBox *m_pReadableRadio, *m_pWritableRadio;

	QPushButton *m_pDeleteButton, *m_pEditButton, *m_pCancelButton;
	QCheckBox *m_pHideCheck;

	SpeakerDialogResult m_result;

	QString m_password;	// password if set, and "" if password is not set.

	SpeakerListData m_speakerListData;
	bool m_slotEnableFlag;
};


#endif
