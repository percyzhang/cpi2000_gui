#ifndef USB_FILE_DIALOG_H
#define USB_FILE_DIALOG_H

#include <QDialog>
#include <QLabel>
#include <QListWidget>
#include <QPushButton>
#include <QLineEdit>
#include <QCheckBox>

class USBFileDialog : public QDialog
{
    Q_OBJECT
public:
    USBFileDialog(QWidget *parent);
	void setToSaveMode(QStringList &fileNameList, QString newFileName);
	void setToLoadMode(QStringList &fileNameList);
	QString getSelectedFileName() { return m_selectedFileName; }

signals:

protected slots:
	void fileSelected(QListWidgetItem *);
	void onCancel();
	void onLoadSave();
	void onFileNameChanged(const QString &text);
	void onNewFileClicked();

protected:
	void resizeEvent(QResizeEvent * /* event */);
	void refresh();

protected:
	QCheckBox *m_pNewFileCheckBox;
	QLineEdit *m_pFileNameEdit;
	QLabel *m_pDescriptionLabel, *m_pFileNameLabel, *m_pExplainLabel;
	QListWidget *pUSBFileList;
	QPushButton *m_pLoadButton, *m_pCancelButton;
	bool m_saveMode;	// true: save mode;  false: load mode;

	QStringList m_usbFileList;
	QString m_NewFileName;	// Used for Save to New File.
	QString m_selectedFileName; // The file name that selected by user. if null, means the user select to save as new file.
};


#endif
