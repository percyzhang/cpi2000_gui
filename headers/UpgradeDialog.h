#ifndef UPGARDE_DIALOG_H
#define UPGARDE_DIALOG_H

#include <QPixmap>
#include <QDialog>
#include <QLabel>
#include <QRadioButton>
#include <QPushButton>
#include <QLineEdit>
#include "CPi2000Setting.h"
#include "BackgroundWidget.h"
#include <QGroupBox>

class TimeAdjustDialog : public QDialog
{
    Q_OBJECT
public:
    TimeAdjustDialog(QWidget *parent);

signals:

protected slots:
	void onTimerOut();
	void onSyncTime();

protected:
	void resizeEvent(QResizeEvent * /* event */);

protected:
	QLabel *m_pCurrentTimeLabel;
	QLabel *m_pDeviceTimeLabel;
	QLineEdit *m_pCurrentTimeEdit;
	QLineEdit *m_pDeviceTimeEdit;
	QPushButton *m_pSetButton, *m_pSyncButton, *m_pCancelButton;
	QTimer *m_pTimer;

	QDateTime m_deviceTime, m_hostTime;

};

typedef enum
{
	UPGRADE_OPEATION = 0,
	DOWNGRADE_OPEATION = 1,
} UPDATE_OPERATION;

class UpgradeDialog : public QDialog
{
    Q_OBJECT
public:
    UpgradeDialog(QWidget *parent);
	void setReminder(QString reminder);
	void setUpgradeFlag(bool flag) { m_upgradeFlag = flag; }
	bool getUpgradeFlag(void) { return m_upgradeFlag; }

	void setUpgradeResult(bool result) { m_upgradeResultFlag = result; }
	bool getUpgradeResult() { return m_upgradeResultFlag; }
	void setOperation(UPDATE_OPERATION operaton);

signals:

protected slots:
	void onClickPage1Yes();

protected:
	void resizeEvent(QResizeEvent * /* event */);

protected:
	QLabel *m_pVerisonLabel;
	QLabel *m_pRemainderLabel;
	BackgroundWidget *m_pWaitingWidget;
	bool m_upgradeFlag;			// upgrade or not
	bool m_upgradeResultFlag;   // if upgrade, success (true) or fail(false)
	UPDATE_OPERATION m_operation;
	QPushButton *m_pPage1_Yes, *m_pPage1_Cancel;
};


class IPAddressDialog : public QDialog
{
    Q_OBJECT
public:
	IPAddressDialog(QWidget *parent);
	NETWORK_SETTING getNetworkSetting() { return m_networkSetting; }

protected slots:
	void applyIPAddress();
	void onDHCPButtonToggle(bool flag);

protected:

	void resizeEvent(QResizeEvent * /* event */);
	void setIPAddress(QHostAddress address);
	void setSubMask(QHostAddress address);
	void setDHCPEnable(bool flag);
	void refreshNetwork(NETWORK_SETTING networkSetting);
	void setDefaultGateWay(QHostAddress address);

	QRadioButton *m_pDHCPEnableButton, *m_pDHCPDisableButton;
	QLineEdit *m_pIPEdit1, *m_pIPEdit2, *m_pIPEdit3, *m_pIPEdit4, 
			*m_pSubnetMaskEdit1, *m_pSubnetMaskEdit2, *m_pSubnetMaskEdit3, *m_pSubnetMaskEdit4, 
			*m_pGatewayEdit1, *m_pGatewayEdit2, *m_pGatewayEdit3, *m_pGatewayEdit4;

	QLabel  *m_pDHCPLabel, *m_pIPAddress, *m_pSubnetMaskLabel,
			*m_pGatewayLabel, *m_pIPDot1, *m_pIPDot2, *m_pIPDot3, *m_pSubnetMaskDot1, *m_pSubnetMaskDot2, *m_pSubnetMaskDot3,
			*m_pGatewayDot1, *m_pGatewayDot2, *m_pGatewayDot3;

	QPushButton *m_applyButton, *m_cancelButton;

	NETWORK_SETTING m_networkSetting;
//	QGroupBox *m_pGroupBox;
};

#endif

