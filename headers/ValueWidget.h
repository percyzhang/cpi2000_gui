#pragma once

#include <QWidget>
#include <QColor>
#include <QTimer>

typedef struct
{
	float m_value1;
	float m_scale1;		/* Range : 0 - 100,  80 means 80% */
	float m_value2;
	float m_scale2;		/* Range : 0 - 100,  80 means 80% */
} ScaleValue;

class ValueWidget: public QWidget
{
    Q_OBJECT

public:
	ValueWidget(QWidget *pWidget);
	void setBackgroudColor(QColor backgroundColor) { m_backgroundColor = backgroundColor; }

	/*	
		setScale(-60.0, 10, 0.0, 90) 
			means:	when the value = -60.0dB, it will reach at the 10% of the widget height.
					when the value = -0.0dB, it will reach at the 90% of the widget height.
	*/
	void setScale(float value1, float scale1, float value2, float scale2);
	void setValue(float currentValue);
	void setSolidColor(QColor color) { m_solidColor = color; }
	void refresh();
	void setExtraGain(float currentValue);
	void setStandard(float standValue,QColor color = QColor(0xf0, 0xf0, 0xf0)) { m_standValue = standValue;  m_standColor = color; }

protected:
	QColor getColor(float value);
	void paintEvent(QPaintEvent *event);

protected:
	ScaleValue m_scaleValue;

	QColor m_backgroundColor, m_standColor, m_solidColor;

	float m_oldValue, m_newValue;
	float m_standValue;		/* draw with white color */
	float m_extraGain;		/* For full mode, it should be +53.5dB.  for X-Curve Mode, it should be 36dB */
};



class CalWidget: public QWidget
{
    Q_OBJECT

public:
	CalWidget(QWidget *pWidget);
	void setBackgroudColor(QColor backgroundColor) { m_backgroundColor = backgroundColor; }
	void setFrontColor(QColor color1, QColor color2, QColor QColor3, float leftDB, float rightDB);
	/*	
		setScale(-60.0, 10, 0.0, 90) 
			means:	when the value = -60.0dB, it will reach at the 10% of the widget height.
					when the value = -0.0dB, it will reach at the 90% of the widget height.
	*/
	void setScale(float value1, float scale1, float value2, float scale2);
	void setValue(float currentValue);
	void refresh();
//	void setExtraGain(float currentValue);

protected:
	void paintEvent(QPaintEvent *event);

private slots:
	void onTimer_100ms(void);

protected:
	ScaleValue m_scaleValue;
	QColor m_backgroundColor;
	float m_oldValue, m_newValue, m_zeroValue;
	float m_extraGain;

	QColor m_color1, m_color2, m_color3;
	float m_leftCal, m_rightCal;
	int m_leftPercent, m_rightPercent;
	QTimer *m_pTimer;
};


