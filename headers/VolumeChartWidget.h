#ifndef VOLUME_CHART_WIDGET_H
#define VOLUME_CHART_WIDGET_H

#include <QWidget>
#include <QTimer>
#include <QStringList>

#define MAX_VOLUME_COUNT	10
#define MAX_VOLUME_VALUE	20			

class VolumeChartWidget : public QWidget
{
    Q_OBJECT
public:
	VolumeChartWidget(QWidget *pParent);
	virtual QColor getVolumeValueColor(int value);
	void setVolumeData(float *pVolumeValue, QStringList voluemeNameList, int count);
	void refresh();

protected:
	void paintEvent(QPaintEvent *event);
	void changeEvent(QEvent *e);

protected slots:
	void onTimerOut();
	void onOutputVolumeChanged();

protected:	
	float *m_pDBValue;
	int m_volumeCount, m_volumeValue[MAX_VOLUME_COUNT]; /* 0 <= m_volumeValue[i] < MAX_VOLUME_VALUE  */
	QTimer *m_pTimer;
	QStringList m_volumeNameList;
};
#endif
