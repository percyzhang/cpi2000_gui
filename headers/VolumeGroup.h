#ifndef VOLUME_GROUP_H
#define VOLUME_GROUP_H

#include "ButtonSlider.h"
#include "MarkButton.h"
#include <QDoubleSpinBox>

class VolumeGroup : public QWidget
{
    Q_OBJECT
public:
	VolumeGroup(QWidget *parent);
	void setVolume(float volume);
	void setMuteFlag(bool flag);
	ButtonSlider *getVolumeSlider() { return m_pVolumeSlider; }

signals:
	void toggleMuteButton(bool);
	void changeVolume(float);

protected slots:
//	void onTimerOut();
	void onClickMuteButton();
	void onVolumeSliderChanged(int volume);
	void onVolumeSpinBoxChanged(double volume);

protected:
	void refresh();
	void paintEvent(QPaintEvent * event);
	void resizeEvent(QResizeEvent * /* event */);
	ButtonSlider *m_pVolumeSlider;
	MarkButton *m_pMuteButton; 
	QDoubleSpinBox *m_pVolumeSpinBox;

	float m_volume;
	bool m_slotEnableFlag;
};


#endif
