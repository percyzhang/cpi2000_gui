#ifndef VOLUMESLIDER_H
#define VOLUMESLIDER_H

#include "ButtonSlider.h"

class VolumeSlider : public ButtonSlider
{
public:
	VolumeSlider(QWidget *pParent, Qt::Orientation orientation =  Qt::Vertical);
	void setVolumePreset(int value);
};


#endif
