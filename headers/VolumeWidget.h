#ifndef VOLUME_WIDGET_H
#define VOLUME_WIDGET_H

#include <QWidget>
#include <QPixmap>
#include <QStylePainter>

class VolumeWidget : public QWidget
{
    Q_OBJECT
public:
    VolumeWidget(QWidget *parent);
	void setOriginPosition(int x, int y);
	void setXMarkString(QStringList &xStringList);
	void setYMarkString(QStringList &yStringList);
	void setVolumeStep(int min, int max, int totalStep);
	void setVolume(int *aVolumeValue);
	void refreshValue();
signals:

protected slots:

protected:
	void resizeEvent(QResizeEvent * /* event */);
	void paintEvent(QPaintEvent *event);
	void updatePixmap();
	void drawVolume(QStylePainter &painter, int index, int step);

protected:
	int m_originXPosition, m_originYPositon, m_xAxialRight, m_yAxialTop;
	QStringList m_xStringList, m_yStringList;
	int m_minVolume, m_maxVolume, m_totalStep;
	int m_volumeValue[20];
	int m_volumeCount;

	QPixmap m_pixmap;
	int m_updateType;	// 0: Update completely;  1: only update the green volume value, not the axial.
};


#endif
