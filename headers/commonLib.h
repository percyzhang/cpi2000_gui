
#ifndef _COMMON_LIB_H_
#define _COMMON_LIB_H_

#include "types.h"
#include <QString>
#include <QJsonObject>

#define REGULAR_EXPRESSION_INTEGER	"(-?\\d+)"
#define REGULAR_EXPRESSION_FLOAT	"(-?\\d+(\\.\\d+)?)"

#define ERROR_OUTPUT(string) qDebug() << QString("Fatal Error at: %1.%2 - %3").arg(__FILE__).arg(__LINE__).arg(string)

#define BYTE0(value) ((value) & 0xFF)
#define BYTE1(value) (((value) >> 8) & 0xFF)
#define BYTE2(value) (((value) >> 16) & 0xFF)
#define BYTE3(value) (((value) >> 24) & 0xFF)

#define BIT(value, bit) (((value) >> (bit)) & 1)

/* All the functions here MUST support multi-thread !!! */

void FormatString(char lpszBuffer[501], BYTE * pucMsg, int len);

void timerLoad(unsigned int *timerTicks);

unsigned int timerExpired(unsigned int *timerTicks, unsigned int tickCount, unsigned int reload);

BYTE calcChecksum (BYTE* pucData, int iCount);

void replaceChar(char *string, char *subString, char newChar);

int devideString(char *string, int position[20]);

void stringToChar(char *pDest, QString srcString);

UINT32 htonl(UINT32 data);

int readInt(QString valueString);

float readFloat(QString valueString);

typedef struct
{
	int		m_value;
	QString	m_explain;
} EXPLAIN_TABLE;

typedef struct
{
	int			m_stringID;
	QString		m_JSONKey;
	void		*m_pValue;
	EXPLAIN_TABLE	*m_pExplainTable;
	int			m_tableCount;
} JSON_STRING_TABLE;



extern QString readObject(QJsonObject &json, QString keyList);

extern QString readObject(QJsonObject &json, QStringList& list, int startIndex);

extern bool modifyObject(QJsonObject &json, QString keyString, QString &valueString);

//extern bool modifyObject(QJsonObject &json, QStringList& list, int startIndex, QString& valueString);
extern void modifyJsonValue(QJsonDocument& doc, const QString& path, const QJsonValue& newValue);


int getTableValue(EXPLAIN_TABLE *table, QString string);
QString getTableString(EXPLAIN_TABLE *table, int value);
int getTableCount(EXPLAIN_TABLE *table);

EXPLAIN_TABLE g_boolTable[], g_inputSourceTable[], g_speakerTypeTable[], g_micChannelTable[], g_LFEInverseTable[], g_AudioOutputTable[],
	g_polarityTable[], g_PEQName[], g_onOffTable[], g_yesNoTable[], g_eqTypeTable[], g_PFIIRTypeTable[];


// =============================================================
void msSleep(int ms);

#define LOG_SOCKET_MESSAGE  0		/* bit 0 is used for the switch to trace SOCKET message */
extern UINT32 g_debug;
extern bool g_debugEnable;

void setBit(UINT32 &value, int bit);
void clearBit(UINT32 &value, int bit);

#endif


