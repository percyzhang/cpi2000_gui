#pragma once
class conv
{
public:
	conv(void);
	~conv(void);
	// output: convolution result
	// coef1,coef2: input vector 1,2
	// N1,N2: input vector length 1,2
	void convolution(double *output,double *coef1,int N1,double *coef2, int N2);
};

