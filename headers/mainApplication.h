#ifndef MAIN_APPLICATION_H
#define MAIN_APPLICATION_H

#include <QApplication>
#include <QFile>
#include <QDebug>
#include "types.h"
#include "deviceSearchingDialog.h"
#include <CPi2000Setting.h>
#include <QNetworkReply>
#include <private/qzipreader_p.h>
#include <private/qzipwriter_p.h>
#include "LogWidget.h"
#include "CPi2000Setting.h"
#include "WaittingWidget.h"

#define CPi2000_GUI_VERSION "1.0.4.3"

#define CPI2000_MAIN_VERSION_MASK "1.0.4"
#define CPi2000_NEW_FW_VERSION  "1.0.4.2"

//#define CROSSROAD_CONSOLE_DEBUG



//#define _SOCKET_DEBUG
//#define LOG_FILE


#ifdef LOG_FILE
extern QTextStream g_log;
extern QFile g_logFile;
#define LOG 		g_log <<endl << QDate::currentDate().toString(Qt::ISODate) + " " + QTime::currentTime().toString() + " "
#define WARNING     g_log <<endl << QDate::currentDate().toString(Qt::ISODate) + " " + QTime::currentTime().toString() + " Warning: "
#define FATAL_ERROR     g_log << endl << QDate::currentDate().toString(Qt::ISODate) + " " + QTime::currentTime().toString() + " Error: "
#else
#define LOG 		qDebug() << QDate::currentDate().toString(Qt::ISODate) + " " + QTime::currentTime().toString() + " "
#define WARNING     qDebug() << QDate::currentDate().toString(Qt::ISODate) + " " + QTime::currentTime().toString() + " Warning: "
#define FATAL_ERROR     qDebug() << QDate::currentDate().toString(Qt::ISODate) + " " + QTime::currentTime().toString() + " Error: "
#endif

#define REMOTE_DEFAULT_DEVICE_FILE "SAVE_DEFAULT.dev"

class DebugSetting
{
public:
	DebugSetting() { m_bNeedCheckSendPacketCheckSum = m_bNeedCheckSendPacketLength = m_bNeedLogSentPacket = m_bNeedLogRecvedPacket = m_bNeedLogSentMsg = m_bNeedLogRecvedMsg = m_bNeedLogRecvedDebugMsg = false; m_bShakeHandEnable = false; m_saveToLockPreset = false; }

	bool needLogSentPacket() { return m_bNeedLogSentPacket; }
	void setLogSentPacketFlag (bool flag) { m_bNeedLogSentPacket = flag; }

	bool needLogRecvedPacket() { return m_bNeedLogRecvedPacket; }
	void setLogRecvedPacketFlag (bool flag) { m_bNeedLogRecvedPacket = flag; }

	bool needLogSentMsg() { return m_bNeedLogSentMsg; }
	void setLogSentMsgFlag (bool flag) { m_bNeedLogSentMsg = flag; }

	bool needLogRecvedMsg() { return m_bNeedLogRecvedMsg; }
	void setLogRecvedMsgFlag (bool flag) { m_bNeedLogRecvedMsg = flag; }

	bool needLogRecvedDebugMsg() { return m_bNeedLogRecvedDebugMsg; }
	void setLogRecvedDebugMsgFlag (bool flag) { m_bNeedLogRecvedDebugMsg = flag; }

	bool getShakeHandFlag() { return m_bShakeHandEnable; }
	void setShakeHandFlag (bool flag) { m_bShakeHandEnable = flag; }

	void setLockSaveFlag(bool flag) { m_saveToLockPreset = flag; }
	bool getLockSaveFlag() { return m_saveToLockPreset; }

protected:
	bool m_bNeedCheckSendPacketCheckSum, m_bNeedCheckSendPacketLength;
	bool m_bNeedLogSentPacket, m_bNeedLogRecvedPacket;
	bool m_bNeedLogSentMsg, m_bNeedLogRecvedMsg, m_bNeedLogRecvedDebugMsg;
	bool m_bShakeHandEnable;
	bool m_saveToLockPreset;	// Normally we can not save to locked preset. But this flag can allow us to save to locked preset.
};

typedef enum
{
	DEVICE_DISCONNECTED = 0,
	DEVICE_SEARCHING,
	DEVICE_CONNECTING,
	DEVICE_USB_CONNECTED,
	DEVICE_NETWORK_CONNECTED,
} DEVICE_CONNECT_STATUS;

class MainApplication : public QApplication
{
    Q_OBJECT

public:	
	MainApplication(int argc, char *argv[]);
	~MainApplication();
	void msleep(int ms);
	DebugSetting* getDebugSetting() { return &m_debugSetting; }
	DEVICE_CONNECT_STATUS getConnectStatus() { return m_deviceConnecteStatus; }
	void setConnectStatus(DEVICE_CONNECT_STATUS status);
	DeviceSocket *getDeviceConnection();

	bool connectDevice(QHostAddress localAddress, QHostAddress peerAddress);
	void disconnectDevice();
	bool getFtpFile(QString strRemoteFileName, QString strLocalFileName);
	bool unzipFileToFolder(QString zipFileName, QString folder);
	bool zipFolderToFile(QString zipFileName, QString folder);
	bool zipFilesToFile(QString zipFileName, QStringList fileNameList);
//	void onRecvMasterVolume(QString outputVolume);
	bool putFtpFile(QString strLocalFileName, QString strRemoteFileName);
	void getNewLog();
	void clearLog();
	static QString getTempSpeakerDBFileName();
	static QString getTempPresetDBFileName();
	static QString getTempDevFileName();

signals:
	void deviceConnected();
	void deviceDisconnected();
	void outputVolumeChanged();
	void masterVolumeChanged();

	void setSPLValue();		/* When receive SPL */
	void setRTAValue();		/* When receive RTA */

	void refreshRoolLevel();
	void refreshEQTuning();

	void setSignalMode(int mode);

private slots:
	void downloadFinished(QNetworkReply*);
	void uploadFinished(QNetworkReply*);

protected:
	DebugSetting m_debugSetting;
	volatile DEVICE_CONNECT_STATUS m_deviceConnecteStatus;
	CPi2000Data m_pDeviceData;
	DeviceSocket *m_pDeviceSocket;

	bool m_ftpSuccess;
	bool m_ftpFinished;
	QString m_ftpErrorInfo;
	QByteArray m_ftpData;
};


extern MainApplication *g_pApp;
extern QTextStream g_log;
extern QFile g_logFile;


#endif // MAINWIDGET_H
