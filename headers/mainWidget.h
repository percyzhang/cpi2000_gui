#ifndef MAIN_WIDGET_H
#define MAIN_WIDGET_H

#include <QWidget>
#include <QFrame>
#include <QPushButton>
#include <QButtonGroup>
#include <QTranslator>
#include <QCloseEvent>
#include <QRadioButton>
#include <QLabel>
#include <QLineEdit>
#include <QTimer>
#include <QPaintEvent>
#include "BackgroundWidget.h"
#include "broadcastCommunication.h"
#include <QMenuBar>
#include "MainTabWidget.h"
#include "OverviewWidget.h"
#include "BasicSettingWidget.h"
#include "InputSettingWidget.h"
#include "OutputSettingWidget.h"
#include "MarkButton.h"
#include "PanelWidget.h"
//#include "BottomWidget.h"
#include "VolumeChartWidget.h"
#include "LogWidget.h"
#include <QShortcut>
#include "QScrollArea.h"


#define PRESET_DB_FILE "Presets.db"
#define SPEAKER_DB_FILE "speaker.db"

#define CONFIG_DB "/cdi3/Software/Firmware/Model/Build/embBuild/Model/release/storage/config.db"

class MainWidget : public QWidget
{
    Q_OBJECT

public:
    MainWidget(QWidget *parent = 0);
    ~MainWidget();
	void retranslateUi();


	void refreshMasterVolume();
	void refreshInputSource();
	void refreshMasterMuteFlag();
	void refreshUSBFileMenu();
	void sendResizeEvent();
	void setLoadDetectionName(SPEAKER_CHANNEL channel, SPEAKER_TYPE type);
	void changeLoadDetection();
	
	void refreshOutputVolume();
	void refreshInputVolume();

	void startSignalGenerator();
	void stopSignalGenerator();
	void refreshSpeakerSetting();
signals:
//    void onRecvLongParamMsg(APP_MESSAGE * pAppMsg);
//	void onRecvParamMsg(APP_MESSAGE * pAppMsg);
//	void quitApp();	/* When the main window is about to quit, this signal will be sent to notify other widget to be closed */

protected slots:
	void onOverViewClicked();
	void onBasicSettingClicked();
	void onInputSettingClicked();
	void onOutputSettingClicked();
	void onClickChineseButton();
	void onClickEnglishButton();
	void onClickConnectButton();
	void onDeviceConnected();
	void onDeviceDisconnected();
	void on1STimerOut();
	void onUpgradeFirmware();
	void onRestoreFactory();
	void onAdjustTime();
	void onSpeakerEditor();
	void onSpeakerImport();
	void onSetIPAddress();
	void onZoom();
	void onOption();
	void about();
	void onSaveDeviceFile();
	void onSaveUSBFile();
	void onOpenDeviceFile();
	void onLoadUSBFile();
	void onLogClicked();
	void SaveSpeakerToDevice(SpeakerData *pSpeakerData);
	void onMenuShortcutActivated();

protected:
	void createMenuBar();
	void resizeEvent(QResizeEvent * /* event */);
	void closeEvent(QCloseEvent *event);
	void onTabChanged(int nIndex);
	void refreshConnectedButton();
	void refresh();
	bool loadFromDeviceFile(QString strDeviceFile);
	void showEvent(QShowEvent * event);

protected:
	QFrame *m_pTopFrame, *m_pBottomFrame;
	QButtonGroup *m_pLanguageButtonGroup;
	QPushButton *m_pEnglishButton, *m_pChineseButton;
	QFrame *m_pLine;
	int m_currentTabIndex;	/* 0: Overview,  1: Basic Setting,  2: Input Setting,  3: Output Setting */
	QTranslator *m_pTranslator;
	MarkButton *m_pConnectButton; 
	int m_recvCount;	// the number of the received message. 
	QTimer *m_pTimer1S, *m_pIdle1ms;
	BackgroundWidget *m_pLogoWidget;
	VolumeChartWidget *m_pVolumeChartWidget;
	QLabel	*m_pOutputLevelLabel;

	QMenuBar *m_pMenuBar;
	QMenu *m_pMenu1, *m_pMenu2, *m_pMenuOpen, *m_pMenuSave;
	QAction *m_pActionLoadDeviceFile, *m_pActionLoadFromUSB, *m_pActionSaveDeviceFile, *m_pActionUpgrade, *m_pSetIPAddress, *m_pActionAdjustTime, *m_pActionSpeakerEditor, *m_pActionRestore, *m_pActionImportSpeaker,
		*m_pActionSaveToUSB, *m_pActionExit, *m_pActionAbout, *m_pActionZoom, *m_pActionOption;

	QPushButton *m_pOverviewButton, *m_pBasicSettingButton, *m_pInputSettingButton, *m_pOutputSettingButton, *m_pLogButton;
	QFrame *m_pTabFrame;
	OverviewWidget *m_pOverviewWidget;
	BasicSettingWidget * m_pBasicSettingWidget;
	InputSettingWidget *m_pInputSettingWidget;
	OutputSettingWidget *m_pOutputSettingWidget;
	LogWidget *m_pLogWidget;
	PanelWidget *m_pPanelWidget;
//	BottomWidget *m_pBottomWidget;

	QShortcut *m_pMenuShortcut;
	bool m_debugActive;

	QScrollArea *m_pScrollArea;
};

extern MainWidget *g_pMainWidget;

#endif // MAINWIDGET_H
