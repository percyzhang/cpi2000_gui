#ifndef NETWORK_CONNECT_H
#define NETWORK_CONNECT_H

#include "connect.h"
#include <QTcpSocket>

class NetworkConnect : public Connect
{
	Q_OBJECT
public:
	NetworkConnect();
	~NetworkConnect();
	
	/* To find the device, no matter it is connected or disconnected */
	bool findDevice();

	// Open the device using the VID and PID.
//	int USBConnect(int vid, int pid);
//	int NetworkConnect(UINT32 ipAddr, UINT32 port);
	bool connectDevice(UINT32 ipAddr, UINT32 port);
    void disconnect();

	/* 
	Function Description:
		Send a translate layer packet (64 bytes + ReportID (Fix Byte 0)) to MCU.

	Return Value:
		ERR_NONE	- Send Successfully.
		other value - Send Failed. 
	*/
//	int USBSendPacket(BYTE data[65]);	
	bool sendData(BYTE *pBuffer, int nLen);

	/*
	readPacket: get a packet from translate layer. The max size of the packet should be 64 bytes.

	Return Value:  
		true: get a valid packet from translate layer.
		false: no valid packet is returned.

	pBuffer	- valid only return value is true, which means a valid translate packet start from Magic 1
	*/
//	bool USBreadPacket(BYTE pBuffer[64]);
	int recvData(BYTE *pBuffer, int nMaxLen);

protected slots:
	void onNetworkDisconnected();
	void onNetworkConnected();

protected:
	QTcpSocket *m_pSocket;
	bool m_connectFlag;
};

#endif // CONNECT_H
