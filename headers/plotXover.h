

#pragma once
#include "CBiquad.h"
#include "mathDefines.h"




enum eFilterTypeSOS
{		kLowPass1,
		kLowPass2,
		kHighPass1,
		kHighPass2,
		kLowPassNSOS,
		kHighPassNSOS,
		kLowPassBesselNSOS,
		kHighPassBesselNSOS,
        kNumFilterTypesSOS
};

	enum eSlopeType
	{
		//BS= Bessel , BW=Butterworth ,LR=Linkwitz-Riley	
	    BS_6,
	    BS_12,
		BS_18,
	    BS_24,
	    BS_30,
	    BS_36,
	    BS_42,
	    BS_48,
	    BW_6,
	    BW_12,
		BW_18,
	    BW_24,
	    BW_30,
	    BW_36,
	    BW_42,
	    BW_48,
	    LR_12,
	    LR_24,
		LR_36,
	    LR_48,
	    kNumSlopeTypes
	};
class plotXover
{
public:


	plotXover(void);
	~plotXover(void);
	void setLPFParameter(bool enable, int slope, double freq);
	void setHPFParameter(bool enable, int slope, double freq);
	void plotCurve(double *x, double *y, long pointslength);
private:
	static const sBiquadCoefs _biquadBypassCoefs;
	sBiquadCoefs biquad[4];

};

