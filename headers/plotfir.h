
#pragma once
#include <math.h>
#include "mathDefines.h"


enum eFilterBandType
{		
    LOWPASS,
    HIGHPASS,
    BANDPASS,
    BANDSTOP,
	ALLPASS
};

/*enum eWindowType
{	
    RETANGULAR = 1,
    TAPERED_RETANGULAR,
	TRIANGULAR,
    HANNING,
    HAMMING,
    BLACKMAN,
    KAISER
};*/

enum eWindow{
	wtNONE,
	wtKAISER,
	wtSINC,
	wtSINE
};

class plotFir
{
public:


	plotFir(void);
	~plotFir(void);
	void setFirParameter(bool enable, int N, int filterType, double fc, double BW);
	void plotCurve(double *x, double *y, long pointslength);
	void plotCurve(double *coef,int N,double *x, double *y, long pointslength);
	void getCoef(double *coef);
private:
	int nTap;
	double FirCoefs[1024];
	void RectWinFIR(double *FirCoeff, int NumTaps, int PassType, double OmegaC, double BW);
	void FIRFilterWindow(double *FIRCoeff, int N, int WindowType, double Beta);
	inline double Sinc(double x){
		if(x > -1.0E-5 && x < 1.0E-5)return(1.0);
		return(sin(x)/x);
	}


};

