#-------------------------------------------------
#
# Project created by QtCreator 2015-11-25T14:46:51
#
#-------------------------------------------------

QT       += core gui network gui-private sql

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = cpi2000
TEMPLATE = app
INCLUDEPATH += .

CONFIG += static 

#Arrange output folders
CONFIG(debug, debug|release){
    BUILDDIR = debug
} else {
    BUILDDIR = release
}
OBJECTS_DIR = $$PWD/../out/$$BUILDDIR/obj
UI_DIR = $$PWD/../out/$$BUILDDIR/ui
MOC_DIR = $$PWD/../out/$$BUILDDIR/moc
RCC_DIR = $$PWD/../out/$$BUILDDIR/rcc
DESTDIR = $$PWD/../out/$$BUILDDIR/bin
INCLUDEPATH += $$PWD/../headers
RESOURCES += $$PWD/../resources/cpi2000.qrc
RC_FILE += 	$$PWD/../resources/cpi2000.rc
DEFINES += _CRT_SECURE_NO_WARNINGS 

# Header file
HEADERS +=  $$PWD/../headers/Types.h \
            $$PWD/../headers/mainWidget.h \
            $$PWD/../headers/mainApplication.h \
            $$PWD/../headers/commonLib.h \
			$$PWD/../headers/backgroundWidget.h \
			$$PWD/../headers/broadcastCommunication.h \
			$$PWD/../headers/deviceSearchingDialog.h \
			$$PWD/../headers/MainTabWidget.h \			
			$$PWD/../headers/OverviewWidget.h \			
			$$PWD/../headers/BasicSettingWidget.h \			
			$$PWD/../headers/InputSettingWidget.h \			
			$$PWD/../headers/OutputSettingWidget.h \			
			$$PWD/../headers/MarkButton.h \				
			$$PWD/../headers/PanelWidget.h \			
			$$PWD/../headers/VolumeWidget.h \
			$$PWD/../headers/ButtonSlider.h \
			$$PWD/../headers/RoomLevelWidget.h \
			$$PWD/../headers/EQTuningWidget.h \
			$$PWD/../headers/OutputTabWidget.h \
			$$PWD/../headers/RadioButton.h \
			$$PWD/../headers/HeadFrame.h \
			$$PWD/../headers/VolumeChartWidget.h \
			$$PWD/../headers/DigitalInputSettingWidget.h \
			$$PWD/../headers/SpeakerAndAmplifierWidget.h \
			$$PWD/../headers/CPi2000Setting.h \
			$$PWD/../headers/VolumeGroup.h \
			$$PWD/../headers/DeviceSocket.h \
			$$PWD/../headers/GEQWidget.h \
			$$PWD/../headers/FileDB.h \
			$$PWD/../headers/EQWidget.h \
			$$PWD/../headers/OutputChannel.h \
			$$PWD/../headers/UpgradeDialog.h \
			$$PWD/../headers/PresetsDB.h \
			$$PWD/../headers/plotBiquad.h \	
			$$PWD/../headers/USBFileDialog.h \	
			$$PWD/../headers/LogWidget.h \	
			$$PWD/../headers/SpeakerListDialog.h \	
			$$PWD/../headers/SpeakerEditor.h \	
			$$PWD/../headers/SpeakerData.h \	
			$$PWD/../headers/plotXover.h \	
			$$PWD/../headers/CBiquad.h \
			$$PWD/../headers/BesselAnalogLUT.h \
			$$PWD/../headers/EQButton.h \
			$$PWD/../headers/plotfir.h \
			$$PWD/../headers/conv.h \			
			$$PWD/../headers/SpeakerImportDialog.h \			
			$$PWD/../headers/AdaptiveTuning.h \			
			$$PWD/../headers/ValueWidget.h \			
			$$PWD/../headers/VolumeSlider.h \			
			$$PWD/../headers/IniSetting.h \			
			$$PWD/../headers/ZoomDlg.h \			
			$$PWD/../headers/simpleQtLogger.h \			
			$$PWD/../headers/qtelnetperso.h \	
			$$PWD/../headers/WaittingWidget.h \	
			
			
SOURCES +=  $$PWD/../sources/main.cpp \
	    $$PWD/../sources/mainWidget.cpp \
            $$PWD/../sources/MainApplication.cpp \
            $$PWD/../sources/commonLib.cpp \
			$$PWD/../sources/backgroundWidget.cpp \
			$$PWD/../sources/broadcastCommunication.cpp \
			$$PWD/../sources/deviceSearchingDialog.cpp \
			$$PWD/../sources/MainTabWidget.cpp \
			$$PWD/../sources/OverviewWidget.cpp \
			$$PWD/../sources/BasicSettingWidget.cpp \
			$$PWD/../sources/InputSettingWidget.cpp \
			$$PWD/../sources/OutputSettingWidget.cpp \
			$$PWD/../sources/MarkButton.cpp \
			$$PWD/../sources/PanelWidget.cpp \
			$$PWD/../sources/VolumeWidget.cpp \
			$$PWD/../sources/ButtonSlider.cpp \
			$$PWD/../sources/RoomLevelWidget.cpp \
			$$PWD/../sources/EQTuningWidget.cpp \
			$$PWD/../sources/OutputTabWidget.cpp \
			$$PWD/../sources/RadioButton.cpp \
			$$PWD/../sources/HeadFrame.cpp \
			$$PWD/../sources/VolumeChartWidget.cpp \
			$$PWD/../sources/DigitalInputSettingWidget.cpp \
			$$PWD/../sources/SpeakerAndAmplifierWidget.cpp \
			$$PWD/../sources/CPi2000Setting.cpp \
			$$PWD/../sources/VolumeGroup.cpp \
			$$PWD/../sources/DeviceSocket.cpp \
			$$PWD/../sources/GEQWidget.cpp \
			$$PWD/../sources/FileDB.cpp \
			$$PWD/../sources/EQWidget.cpp \
			$$PWD/../sources/OutputChannel.cpp \
			$$PWD/../sources/UpgradeDialog.cpp \
			$$PWD/../sources/PresetsDB.cpp \
			$$PWD/../sources/plotBiquad.cpp \
			$$PWD/../sources/USBFileDialog.cpp \
			$$PWD/../sources/LogWidget.cpp \
			$$PWD/../sources/SpeakerListDialog.cpp \
			$$PWD/../sources/SpeakerEditor.cpp \
			$$PWD/../sources/SpeakerData.cpp \
			$$PWD/../sources/CBiquad.cpp \
			$$PWD/../sources/plotXover.cpp \
			$$PWD/../sources/EQButton.cpp \
			$$PWD/../sources/plotfir.cpp \
			$$PWD/../sources/conv.cpp \			
			$$PWD/../sources/SpeakerImportDialog.cpp \			
			$$PWD/../sources/AdaptiveTuning.cpp \			
			$$PWD/../sources/ValueWidget.cpp \			
			$$PWD/../sources/VolumeSlider.cpp \			
			$$PWD/../sources/IniSetting.cpp \			
			$$PWD/../sources/ZoomDlg.cpp \			
			$$PWD/../sources/simpleQtLogger.cpp \			
			$$PWD/../sources/qtelnetperso.cpp \			
			$$PWD/../sources/WaittingWidget.cpp \			


TRANSLATIONS = $$PWD/../resources/cpi2000_en.ts $$PWD/../resources/cpi2000_zh.ts

