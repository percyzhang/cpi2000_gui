<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en_US">
<context>
    <name>BasicSettingWidget</name>
    <message>
        <location filename="../sources/BasicSettingWidget.cpp" line="14"/>
        <location filename="../sources/BasicSettingWidget.cpp" line="839"/>
        <source>   Surround Delay</source>
        <translation>   Surround Delay</translation>
    </message>
    <message>
        <source>   Load Detection</source>
        <translation type="vanished">   Load Detection</translation>
    </message>
    <message>
        <location filename="../sources/BasicSettingWidget.cpp" line="18"/>
        <location filename="../sources/BasicSettingWidget.cpp" line="841"/>
        <source>   Main Audio Setting</source>
        <translation>   Main Audio Setting</translation>
    </message>
    <message>
        <location filename="../sources/BasicSettingWidget.cpp" line="20"/>
        <location filename="../sources/BasicSettingWidget.cpp" line="842"/>
        <source>Master Volume</source>
        <translation>Master Volume</translation>
    </message>
    <message>
        <location filename="../sources/BasicSettingWidget.cpp" line="28"/>
        <location filename="../sources/BasicSettingWidget.cpp" line="843"/>
        <source>Fade In:</source>
        <translation>Fade In:</translation>
    </message>
    <message>
        <location filename="../sources/BasicSettingWidget.cpp" line="31"/>
        <location filename="../sources/BasicSettingWidget.cpp" line="844"/>
        <source>Fade Out:</source>
        <translation>Fade Out:</translation>
    </message>
    <message>
        <location filename="../sources/BasicSettingWidget.cpp" line="43"/>
        <location filename="../sources/BasicSettingWidget.cpp" line="845"/>
        <source>Sync to preset</source>
        <translation>Sync to preset</translation>
    </message>
    <message>
        <source>Distance from screen to rear wall of the theater</source>
        <translation type="vanished">Distance from screen to rear wall of the theater</translation>
    </message>
    <message>
        <location filename="../sources/BasicSettingWidget.cpp" line="84"/>
        <location filename="../sources/BasicSettingWidget.cpp" line="848"/>
        <source>Average distance from screen to rear wall of the theater</source>
        <translation>Average distance from screen to rear wall of the theater</translation>
    </message>
    <message>
        <location filename="../sources/BasicSettingWidget.cpp" line="86"/>
        <location filename="../sources/BasicSettingWidget.cpp" line="849"/>
        <source>Average distance between left and right surround speakers</source>
        <translation>Average distance between left and right surround speakers</translation>
    </message>
    <message>
        <location filename="../sources/BasicSettingWidget.cpp" line="88"/>
        <location filename="../sources/BasicSettingWidget.cpp" line="90"/>
        <location filename="../sources/BasicSettingWidget.cpp" line="139"/>
        <location filename="../sources/BasicSettingWidget.cpp" line="373"/>
        <location filename="../sources/BasicSettingWidget.cpp" line="374"/>
        <location filename="../sources/BasicSettingWidget.cpp" line="850"/>
        <location filename="../sources/BasicSettingWidget.cpp" line="851"/>
        <location filename="../sources/BasicSettingWidget.cpp" line="853"/>
        <source>Meters</source>
        <translation>Meters</translation>
    </message>
    <message>
        <location filename="../sources/BasicSettingWidget.cpp" line="108"/>
        <location filename="../sources/BasicSettingWidget.cpp" line="858"/>
        <source>calculate</source>
        <translation>calculate</translation>
    </message>
    <message>
        <location filename="../sources/BasicSettingWidget.cpp" line="111"/>
        <location filename="../sources/BasicSettingWidget.cpp" line="857"/>
        <source>Delay time:</source>
        <translation>Delay time:</translation>
    </message>
    <message>
        <location filename="../sources/BasicSettingWidget.cpp" line="135"/>
        <location filename="../sources/BasicSettingWidget.cpp" line="351"/>
        <location filename="../sources/BasicSettingWidget.cpp" line="352"/>
        <location filename="../sources/BasicSettingWidget.cpp" line="852"/>
        <source>Feet</source>
        <translation>Feet</translation>
    </message>
    <message>
        <source>Note: Only Crown XLC2500 and XLC2800 support Load Detection!</source>
        <translation type="vanished">Note: Only Crown XLC2500 and XLC2800 support Load Detection!</translation>
    </message>
    <message>
        <location filename="../sources/BasicSettingWidget.cpp" line="16"/>
        <location filename="../sources/BasicSettingWidget.cpp" line="840"/>
        <source>   Fault Detection</source>
        <translation>   Fault Detection</translation>
    </message>
    <message>
        <source>Note: Only Crown XLC2500 and XLC2800 support Fault Detection!</source>
        <translation type="vanished">Note: Only Crown XLC2500 and XLC2800 support Fault Detection!</translation>
    </message>
    <message>
        <location filename="../sources/BasicSettingWidget.cpp" line="164"/>
        <location filename="../sources/BasicSettingWidget.cpp" line="854"/>
        <source>Remind: Only Crown XLC2500 and XLC2800 support Fault Detection!</source>
        <translation>Remind: Only Crown XLC2500 and XLC2800 support Fault Detection!</translation>
    </message>
    <message>
        <source>LS/RS1</source>
        <translation type="vanished">LS/RS1</translation>
    </message>
    <message>
        <source>LS/RS2</source>
        <translation type="vanished">LS/RS2</translation>
    </message>
    <message>
        <source>LEFT1</source>
        <translation type="vanished">LEFT1</translation>
    </message>
    <message>
        <source>LEFT2</source>
        <translation type="vanished">LEFT2</translation>
    </message>
    <message>
        <source>CENTER1</source>
        <translation type="vanished">CENTER1</translation>
    </message>
    <message>
        <source>CENTER2</source>
        <translation type="vanished">CENTER2</translation>
    </message>
    <message>
        <source>RIGHT1</source>
        <translation type="vanished">RIGHT1</translation>
    </message>
    <message>
        <source>Bls/Brs1</source>
        <translation type="vanished">Bls/Brs1</translation>
    </message>
    <message>
        <source>SW1</source>
        <translation type="vanished">SW1</translation>
    </message>
    <message>
        <source>SW2</source>
        <translation type="vanished">SW2</translation>
    </message>
    <message>
        <location filename="../sources/BasicSettingWidget.cpp" line="168"/>
        <location filename="../sources/BasicSettingWidget.cpp" line="855"/>
        <source>5.1 System</source>
        <translation>5.1 System</translation>
    </message>
    <message>
        <location filename="../sources/BasicSettingWidget.cpp" line="172"/>
        <location filename="../sources/BasicSettingWidget.cpp" line="856"/>
        <source>7.1 System</source>
        <translation>7.1 System</translation>
    </message>
</context>
<context>
    <name>DeviceSearchingDialog</name>
    <message>
        <location filename="../sources/deviceSearchingDialog.cpp" line="20"/>
        <source>Available Device List</source>
        <translation>Available Device List</translation>
    </message>
    <message>
        <location filename="../sources/deviceSearchingDialog.cpp" line="24"/>
        <source>Device Name</source>
        <translation>Device Name</translation>
    </message>
    <message>
        <location filename="../sources/deviceSearchingDialog.cpp" line="24"/>
        <source>Device Address</source>
        <translation>Device Address</translation>
    </message>
    <message>
        <location filename="../sources/deviceSearchingDialog.cpp" line="24"/>
        <source>Firmware</source>
        <translation>Firmware</translation>
    </message>
    <message>
        <location filename="../sources/deviceSearchingDialog.cpp" line="46"/>
        <source>Scan</source>
        <translation>Scan</translation>
    </message>
    <message>
        <location filename="../sources/deviceSearchingDialog.cpp" line="49"/>
        <source>Connect</source>
        <translation>Connect</translation>
    </message>
    <message>
        <location filename="../sources/deviceSearchingDialog.cpp" line="52"/>
        <source>Cancel</source>
        <translation>Cancel</translation>
    </message>
    <message>
        <location filename="../sources/deviceSearchingDialog.cpp" line="65"/>
        <source>Connect Device</source>
        <translation>Connect Device</translation>
    </message>
    <message>
        <location filename="../sources/deviceSearchingDialog.cpp" line="216"/>
        <source>Add device...</source>
        <translation>Add device...</translation>
    </message>
    <message>
        <location filename="../sources/deviceSearchingDialog.cpp" line="217"/>
        <source>CPi2000 IP Address:</source>
        <translation>CPi2000 IP Address:</translation>
    </message>
    <message>
        <location filename="../sources/deviceSearchingDialog.cpp" line="226"/>
        <location filename="../sources/deviceSearchingDialog.cpp" line="232"/>
        <location filename="../sources/deviceSearchingDialog.cpp" line="238"/>
        <source>Invalid Input</source>
        <translation>Invalid Input</translation>
    </message>
    <message>
        <location filename="../sources/deviceSearchingDialog.cpp" line="226"/>
        <location filename="../sources/deviceSearchingDialog.cpp" line="232"/>
        <location filename="../sources/deviceSearchingDialog.cpp" line="238"/>
        <source>What you input is not a valid IP Address</source>
        <translation>What you input is not a valid IP Address</translation>
    </message>
    <message>
        <location filename="../sources/deviceSearchingDialog.cpp" line="226"/>
        <location filename="../sources/deviceSearchingDialog.cpp" line="232"/>
        <location filename="../sources/deviceSearchingDialog.cpp" line="238"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="../sources/deviceSearchingDialog.cpp" line="245"/>
        <source>Can&apos;t find the CPi2000</source>
        <translation>Can&apos;t find the CPi2000</translation>
    </message>
    <message>
        <location filename="../sources/deviceSearchingDialog.cpp" line="245"/>
        <source>Failed to connect with the CPi2000 with the IP address: %1, please check the IP address again!</source>
        <translation>Failed to connect with the CPi2000 with the IP address: %1, please check the IP address again!</translation>
    </message>
</context>
<context>
    <name>DigitalInputSettingWidget</name>
    <message>
        <location filename="../sources/DigitalInputSettingWidget.cpp" line="42"/>
        <location filename="../sources/DigitalInputSettingWidget.cpp" line="382"/>
        <source>Undo</source>
        <translation>Undo</translation>
    </message>
    <message>
        <location filename="../sources/DigitalInputSettingWidget.cpp" line="45"/>
        <location filename="../sources/DigitalInputSettingWidget.cpp" line="383"/>
        <source>Apply</source>
        <translation>Apply</translation>
    </message>
</context>
<context>
    <name>EQTuningWidget</name>
    <message>
        <location filename="../sources/EQTuningWidget.cpp" line="46"/>
        <location filename="../sources/EQTuningWidget.cpp" line="1549"/>
        <source>Signal Mode</source>
        <translation>Signal Mode</translation>
    </message>
    <message>
        <location filename="../sources/EQTuningWidget.cpp" line="42"/>
        <location filename="../sources/EQTuningWidget.cpp" line="1550"/>
        <source>To stop RTA setting, you need to set signal mode to &apos;OFF&apos;.</source>
        <translation>To stop RTA setting, you need to set signal mode to &apos;OFF&apos;.</translation>
    </message>
    <message>
        <source>Active Channel</source>
        <translation type="vanished">Active Channel</translation>
    </message>
    <message>
        <location filename="../sources/EQTuningWidget.cpp" line="83"/>
        <location filename="../sources/EQTuningWidget.cpp" line="1566"/>
        <source>Active Channel Level</source>
        <translation>Active Channel Level</translation>
    </message>
    <message>
        <source>Graphic EQ</source>
        <translation type="vanished">Graphic EQ</translation>
    </message>
    <message>
        <source>EQ Assist</source>
        <translation type="vanished">EQ Assist</translation>
    </message>
    <message>
        <source>Flatten</source>
        <translation type="vanished">Flatten</translation>
    </message>
    <message>
        <location filename="../sources/EQTuningWidget.cpp" line="361"/>
        <location filename="../sources/EQTuningWidget.cpp" line="389"/>
        <source>Copy EQ</source>
        <translation>Copy EQ</translation>
    </message>
    <message>
        <location filename="../sources/EQTuningWidget.cpp" line="362"/>
        <location filename="../sources/EQTuningWidget.cpp" line="390"/>
        <location filename="../sources/EQTuningWidget.cpp" line="788"/>
        <source>Paste EQ</source>
        <translation>Paste EQ</translation>
    </message>
    <message>
        <location filename="../sources/EQTuningWidget.cpp" line="400"/>
        <location filename="../sources/EQTuningWidget.cpp" line="797"/>
        <source>Paste EQ (From %1)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../sources/EQTuningWidget.cpp" line="1565"/>
        <source>Subwoofer PEQ1</source>
        <translation>Subwoofer PEQ1</translation>
    </message>
    <message>
        <location filename="../sources/EQTuningWidget.cpp" line="1567"/>
        <source>Subwoofer PEQ2</source>
        <translation>Subwoofer PEQ2</translation>
    </message>
    <message>
        <source>Subwoofer PEQ</source>
        <translation type="vanished">Subwoofer PEQ</translation>
    </message>
    <message>
        <location filename="../sources/EQTuningWidget.cpp" line="95"/>
        <location filename="../sources/EQTuningWidget.cpp" line="159"/>
        <location filename="../sources/EQTuningWidget.cpp" line="1555"/>
        <location filename="../sources/EQTuningWidget.cpp" line="1568"/>
        <source>Cut</source>
        <translation>Cut</translation>
    </message>
    <message>
        <location filename="../sources/EQTuningWidget.cpp" line="117"/>
        <location filename="../sources/EQTuningWidget.cpp" line="182"/>
        <location filename="../sources/EQTuningWidget.cpp" line="1556"/>
        <location filename="../sources/EQTuningWidget.cpp" line="1569"/>
        <source>Type:</source>
        <translation>Type:</translation>
    </message>
    <message>
        <location filename="../sources/EQTuningWidget.cpp" line="120"/>
        <location filename="../sources/EQTuningWidget.cpp" line="185"/>
        <location filename="../sources/EQTuningWidget.cpp" line="1557"/>
        <location filename="../sources/EQTuningWidget.cpp" line="1570"/>
        <source>Cut:</source>
        <translation>Cut:</translation>
    </message>
    <message>
        <location filename="../sources/EQTuningWidget.cpp" line="123"/>
        <location filename="../sources/EQTuningWidget.cpp" line="188"/>
        <location filename="../sources/EQTuningWidget.cpp" line="309"/>
        <location filename="../sources/EQTuningWidget.cpp" line="1558"/>
        <location filename="../sources/EQTuningWidget.cpp" line="1571"/>
        <location filename="../sources/EQTuningWidget.cpp" line="1581"/>
        <location filename="../sources/EQTuningWidget.cpp" line="1588"/>
        <source>Frequency:</source>
        <translation>Frequency:</translation>
    </message>
    <message>
        <location filename="../sources/EQTuningWidget.cpp" line="126"/>
        <location filename="../sources/EQTuningWidget.cpp" line="191"/>
        <location filename="../sources/EQTuningWidget.cpp" line="1559"/>
        <location filename="../sources/EQTuningWidget.cpp" line="1572"/>
        <source>Q:</source>
        <translation>Q:</translation>
    </message>
    <message>
        <location filename="../sources/EQTuningWidget.cpp" line="130"/>
        <location filename="../sources/EQTuningWidget.cpp" line="195"/>
        <location filename="../sources/EQTuningWidget.cpp" line="1562"/>
        <location filename="../sources/EQTuningWidget.cpp" line="1575"/>
        <source>Bell</source>
        <translation>Bell</translation>
    </message>
    <message>
        <source>Paste GEQ (From Channel: %1)</source>
        <translation type="vanished">Paste GEQ (From Channel: %1)</translation>
    </message>
    <message>
        <source>Subwoofer Configuration</source>
        <translation type="vanished">Subwoofer Configuration</translation>
    </message>
    <message>
        <source>Note: The Center level must be determined by aligning the center channel before the SW channel can be aligned!</source>
        <translation type="vanished">Note: The Center level must be determined by aligning the center channel before the SW channel can be aligned!</translation>
    </message>
    <message>
        <source>Note: The Center level must be determined by aligning the center channel before the LFE channel can be aligned!</source>
        <translation type="vanished">Note: The Center level must be determined by aligning the center channel before the LFE channel can be aligned!</translation>
    </message>
    <message>
        <source>Center Channel Gen</source>
        <translation type="vanished">Center Channel Gen</translation>
    </message>
    <message>
        <source>Inverse</source>
        <translation type="vanished">Inverse</translation>
    </message>
    <message>
        <location filename="../sources/EQTuningWidget.cpp" line="1578"/>
        <source>Active Channel EQ</source>
        <translation>Active Channel EQ</translation>
    </message>
    <message>
        <location filename="../sources/EQTuningWidget.cpp" line="244"/>
        <location filename="../sources/EQTuningWidget.cpp" line="1586"/>
        <source>Low Shelf Filter</source>
        <translation>Low Shelf Filter</translation>
    </message>
    <message>
        <location filename="../sources/EQTuningWidget.cpp" line="251"/>
        <source>Frequency</source>
        <translation>Frequency</translation>
    </message>
    <message>
        <location filename="../sources/EQTuningWidget.cpp" line="248"/>
        <location filename="../sources/EQTuningWidget.cpp" line="255"/>
        <location filename="../sources/EQTuningWidget.cpp" line="306"/>
        <location filename="../sources/EQTuningWidget.cpp" line="1580"/>
        <location filename="../sources/EQTuningWidget.cpp" line="1585"/>
        <source>Gain</source>
        <translation>Gain</translation>
    </message>
    <message>
        <location filename="../sources/EQTuningWidget.cpp" line="259"/>
        <source>Slope</source>
        <translation>Slope</translation>
    </message>
    <message>
        <location filename="../sources/EQTuningWidget.cpp" line="313"/>
        <location filename="../sources/EQTuningWidget.cpp" line="1582"/>
        <location filename="../sources/EQTuningWidget.cpp" line="1589"/>
        <source>Gain:</source>
        <translation>Gain:</translation>
    </message>
    <message>
        <location filename="../sources/EQTuningWidget.cpp" line="317"/>
        <location filename="../sources/EQTuningWidget.cpp" line="1583"/>
        <location filename="../sources/EQTuningWidget.cpp" line="1590"/>
        <source>Slope:</source>
        <translation>Slope:</translation>
    </message>
    <message>
        <source>Copy GEQ</source>
        <translation type="vanished">Copy GEQ</translation>
    </message>
    <message>
        <source>Paste GEQ</source>
        <translation type="vanished">Paste GEQ</translation>
    </message>
    <message>
        <source>Paste GEQ From Channel: %1</source>
        <translation type="obsolete">Paste GEQ From Channel: %1</translation>
    </message>
    <message>
        <location filename="../sources/EQTuningWidget.cpp" line="1587"/>
        <source>High Shelf Filter</source>
        <translation>High Shelf Filter</translation>
    </message>
    <message>
        <source>1/3 Octave Graphic EQ</source>
        <translation type="vanished">1/3 Octave Graphic EQ</translation>
    </message>
</context>
<context>
    <name>GEQWidget</name>
    <message>
        <source>RTA Range</source>
        <translation type="vanished">RTA Range</translation>
    </message>
    <message>
        <location filename="../sources/GEQWidget.cpp" line="406"/>
        <location filename="../sources/GEQWidget.cpp" line="754"/>
        <source>1/3 Octave Graphic EQ</source>
        <translation>1/3 Octave Graphic EQ</translation>
    </message>
    <message>
        <location filename="../sources/GEQWidget.cpp" line="410"/>
        <source>Full</source>
        <translation>Full</translation>
    </message>
    <message>
        <location filename="../sources/GEQWidget.cpp" line="414"/>
        <source>X-Curve</source>
        <translation>X-Curve</translation>
    </message>
    <message>
        <location filename="../sources/GEQWidget.cpp" line="506"/>
        <location filename="../sources/GEQWidget.cpp" line="749"/>
        <source>EQ Assist</source>
        <translation>EQ Assist</translation>
    </message>
    <message>
        <location filename="../sources/GEQWidget.cpp" line="507"/>
        <location filename="../sources/GEQWidget.cpp" line="750"/>
        <source>Flatten</source>
        <translation>Flatten</translation>
    </message>
    <message>
        <location filename="../sources/GEQWidget.cpp" line="508"/>
        <location filename="../sources/GEQWidget.cpp" line="751"/>
        <source>Copy EQ</source>
        <translation>Copy EQ</translation>
    </message>
    <message>
        <location filename="../sources/GEQWidget.cpp" line="509"/>
        <location filename="../sources/GEQWidget.cpp" line="752"/>
        <source>Paste EQ</source>
        <translation>Paste EQ</translation>
    </message>
    <message>
        <source>Paste GEQ</source>
        <translation type="vanished">Paste GEQ</translation>
    </message>
    <message>
        <location filename="../sources/GEQWidget.cpp" line="510"/>
        <location filename="../sources/GEQWidget.cpp" line="753"/>
        <source>Graphic EQ</source>
        <translation>Graphic EQ</translation>
    </message>
    <message>
        <source>Copy GEQ</source>
        <translation type="vanished">Copy GEQ</translation>
    </message>
</context>
<context>
    <name>IPAddressDialog</name>
    <message>
        <location filename="../sources/UpgradeDialog.cpp" line="185"/>
        <source>Set CPi2000 device IP Address</source>
        <translation>Set CPi2000 device IP Address</translation>
    </message>
    <message>
        <location filename="../sources/UpgradeDialog.cpp" line="201"/>
        <source>Enable</source>
        <translation>Enable</translation>
    </message>
    <message>
        <location filename="../sources/UpgradeDialog.cpp" line="205"/>
        <source>Disable</source>
        <translation>Disable</translation>
    </message>
    <message>
        <location filename="../sources/UpgradeDialog.cpp" line="213"/>
        <source>DHCP:</source>
        <translation>DHCP:</translation>
    </message>
    <message>
        <location filename="../sources/UpgradeDialog.cpp" line="217"/>
        <source>IP Address:</source>
        <translation>IP Address:</translation>
    </message>
    <message>
        <location filename="../sources/UpgradeDialog.cpp" line="233"/>
        <source>Subnet Mask:</source>
        <translation>Subnet Mask:</translation>
    </message>
    <message>
        <location filename="../sources/UpgradeDialog.cpp" line="250"/>
        <source>Default Gateway:</source>
        <translation>Default Gateway:</translation>
    </message>
    <message>
        <location filename="../sources/UpgradeDialog.cpp" line="266"/>
        <source>Apply</source>
        <translation>Apply</translation>
    </message>
    <message>
        <location filename="../sources/UpgradeDialog.cpp" line="270"/>
        <source>Cancel</source>
        <translation>Cancel</translation>
    </message>
    <message>
        <location filename="../sources/UpgradeDialog.cpp" line="348"/>
        <source>IP Address</source>
        <translation>IP Address</translation>
    </message>
    <message>
        <location filename="../sources/UpgradeDialog.cpp" line="348"/>
        <source>Gateway</source>
        <translation>Gateway</translation>
    </message>
    <message>
        <location filename="../sources/UpgradeDialog.cpp" line="348"/>
        <source>Subnet mask</source>
        <translation>Subnet mask</translation>
    </message>
    <message>
        <location filename="../sources/UpgradeDialog.cpp" line="353"/>
        <location filename="../sources/UpgradeDialog.cpp" line="386"/>
        <location filename="../sources/UpgradeDialog.cpp" line="393"/>
        <location filename="../sources/UpgradeDialog.cpp" line="403"/>
        <location filename="../sources/UpgradeDialog.cpp" line="410"/>
        <source>Format Error</source>
        <translation>Format Error</translation>
    </message>
    <message>
        <location filename="../sources/UpgradeDialog.cpp" line="353"/>
        <source>Please input valid %1!</source>
        <translation>Please input valid %1!</translation>
    </message>
    <message>
        <location filename="../sources/UpgradeDialog.cpp" line="386"/>
        <source>Subnet mask error: %1 is not valie subnet mask!</source>
        <translation>Subnet mask error: %1 is not valie subnet mask!</translation>
    </message>
    <message>
        <location filename="../sources/UpgradeDialog.cpp" line="393"/>
        <source>Gateway should be in the same subnet with IP address!</source>
        <translation>Gateway should be in the same subnet with IP address!</translation>
    </message>
    <message>
        <location filename="../sources/UpgradeDialog.cpp" line="403"/>
        <source>IP Address should not be broadcast address!</source>
        <translation>IP Address should not be broadcast address!</translation>
    </message>
    <message>
        <location filename="../sources/UpgradeDialog.cpp" line="410"/>
        <source>Gateway should not be broadcast address!</source>
        <translation>Gateway should not be broadcast address!</translation>
    </message>
</context>
<context>
    <name>InputSettingWidget</name>
    <message>
        <location filename="../sources/InputSettingWidget.cpp" line="15"/>
        <location filename="../sources/InputSettingWidget.cpp" line="1232"/>
        <source>      DIGITAL</source>
        <translation>      DIGITAL</translation>
    </message>
    <message>
        <location filename="../sources/InputSettingWidget.cpp" line="18"/>
        <location filename="../sources/InputSettingWidget.cpp" line="1233"/>
        <source>      ANALOG</source>
        <translation>      ANALOG</translation>
    </message>
    <message>
        <location filename="../sources/InputSettingWidget.cpp" line="21"/>
        <location filename="../sources/InputSettingWidget.cpp" line="1234"/>
        <source>      MIC</source>
        <translation>      MIC</translation>
    </message>
    <message>
        <location filename="../sources/InputSettingWidget.cpp" line="24"/>
        <location filename="../sources/InputSettingWidget.cpp" line="1235"/>
        <source>      MUSIC(NON-SYNC)</source>
        <translation>      MUSIC(NON-SYNC)</translation>
    </message>
    <message>
        <location filename="../sources/InputSettingWidget.cpp" line="57"/>
        <location filename="../sources/InputSettingWidget.cpp" line="131"/>
        <location filename="../sources/InputSettingWidget.cpp" line="205"/>
        <location filename="../sources/InputSettingWidget.cpp" line="295"/>
        <location filename="../sources/InputSettingWidget.cpp" line="1236"/>
        <location filename="../sources/InputSettingWidget.cpp" line="1252"/>
        <location filename="../sources/InputSettingWidget.cpp" line="1254"/>
        <location filename="../sources/InputSettingWidget.cpp" line="1260"/>
        <source>Input Gain</source>
        <translation>Input Gain</translation>
    </message>
    <message>
        <location filename="../sources/InputSettingWidget.cpp" line="88"/>
        <location filename="../sources/InputSettingWidget.cpp" line="162"/>
        <location filename="../sources/InputSettingWidget.cpp" line="232"/>
        <location filename="../sources/InputSettingWidget.cpp" line="322"/>
        <location filename="../sources/InputSettingWidget.cpp" line="1237"/>
        <location filename="../sources/InputSettingWidget.cpp" line="1253"/>
        <location filename="../sources/InputSettingWidget.cpp" line="1255"/>
        <location filename="../sources/InputSettingWidget.cpp" line="1261"/>
        <source>Global Audio Delay</source>
        <translation>Global Audio Delay</translation>
    </message>
    <message>
        <location filename="../sources/InputSettingWidget.cpp" line="248"/>
        <location filename="../sources/InputSettingWidget.cpp" line="339"/>
        <location filename="../sources/InputSettingWidget.cpp" line="1256"/>
        <location filename="../sources/InputSettingWidget.cpp" line="1257"/>
        <source>Channel Assignment</source>
        <translation>Channel Assignment</translation>
    </message>
    <message>
        <location filename="../sources/InputSettingWidget.cpp" line="251"/>
        <location filename="../sources/InputSettingWidget.cpp" line="1264"/>
        <source>Center</source>
        <translation>Center</translation>
    </message>
    <message>
        <location filename="../sources/InputSettingWidget.cpp" line="256"/>
        <location filename="../sources/InputSettingWidget.cpp" line="1258"/>
        <source>Surrounds</source>
        <translation>Surrounds</translation>
    </message>
    <message>
        <location filename="../sources/InputSettingWidget.cpp" line="262"/>
        <location filename="../sources/InputSettingWidget.cpp" line="1259"/>
        <source>Phantom Power</source>
        <translation>Phantom Power</translation>
    </message>
    <message>
        <location filename="../sources/InputSettingWidget.cpp" line="542"/>
        <source>Digital</source>
        <translation>Digital</translation>
    </message>
    <message>
        <location filename="../sources/InputSettingWidget.cpp" line="549"/>
        <source>Analog</source>
        <translation>Analog</translation>
    </message>
    <message>
        <location filename="../sources/InputSettingWidget.cpp" line="556"/>
        <source>Mic</source>
        <translation>Mic</translation>
    </message>
    <message>
        <location filename="../sources/InputSettingWidget.cpp" line="563"/>
        <source>Music</source>
        <translation>Music</translation>
    </message>
    <message>
        <location filename="../sources/InputSettingWidget.cpp" line="569"/>
        <source>Change Input Source</source>
        <translation>Change Input Source</translation>
    </message>
    <message>
        <location filename="../sources/InputSettingWidget.cpp" line="569"/>
        <source>Do you want to change input source to: </source>
        <translation>Do you want to change input source to: </translation>
    </message>
    <message>
        <location filename="../sources/InputSettingWidget.cpp" line="573"/>
        <source>Yes</source>
        <translation>Yes</translation>
    </message>
    <message>
        <location filename="../sources/InputSettingWidget.cpp" line="574"/>
        <source>No</source>
        <translation>No</translation>
    </message>
    <message>
        <location filename="../sources/InputSettingWidget.cpp" line="959"/>
        <location filename="../sources/InputSettingWidget.cpp" line="980"/>
        <location filename="../sources/InputSettingWidget.cpp" line="1001"/>
        <location filename="../sources/InputSettingWidget.cpp" line="1022"/>
        <location filename="../sources/InputSettingWidget.cpp" line="1244"/>
        <source>Enabled</source>
        <translation>Enabled</translation>
    </message>
    <message>
        <location filename="../sources/InputSettingWidget.cpp" line="965"/>
        <location filename="../sources/InputSettingWidget.cpp" line="986"/>
        <location filename="../sources/InputSettingWidget.cpp" line="1007"/>
        <location filename="../sources/InputSettingWidget.cpp" line="1028"/>
        <location filename="../sources/InputSettingWidget.cpp" line="1248"/>
        <source>Enable</source>
        <translation>Enable</translation>
    </message>
</context>
<context>
    <name>LogWidget</name>
    <message>
        <location filename="../sources/LogWidget.cpp" line="11"/>
        <location filename="../sources/LogWidget.cpp" line="304"/>
        <source>Date</source>
        <translation>Date</translation>
    </message>
    <message>
        <location filename="../sources/LogWidget.cpp" line="11"/>
        <location filename="../sources/LogWidget.cpp" line="304"/>
        <source>Time</source>
        <translation>Time</translation>
    </message>
    <message>
        <location filename="../sources/LogWidget.cpp" line="11"/>
        <location filename="../sources/LogWidget.cpp" line="304"/>
        <source>Level</source>
        <translation>Level</translation>
    </message>
    <message>
        <location filename="../sources/LogWidget.cpp" line="11"/>
        <location filename="../sources/LogWidget.cpp" line="304"/>
        <source>Content</source>
        <translation>Content</translation>
    </message>
    <message>
        <location filename="../sources/LogWidget.cpp" line="25"/>
        <location filename="../sources/LogWidget.cpp" line="307"/>
        <source>Refresh Log</source>
        <translation>Refresh Log</translation>
    </message>
    <message>
        <location filename="../sources/LogWidget.cpp" line="28"/>
        <location filename="../sources/LogWidget.cpp" line="308"/>
        <source>Save Log</source>
        <translation>Save Log</translation>
    </message>
    <message>
        <location filename="../sources/LogWidget.cpp" line="31"/>
        <location filename="../sources/LogWidget.cpp" line="309"/>
        <source>Clear Log</source>
        <translation>Clear Log</translation>
    </message>
    <message>
        <location filename="../sources/LogWidget.cpp" line="35"/>
        <location filename="../sources/LogWidget.cpp" line="310"/>
        <source>Information</source>
        <translation>Information</translation>
    </message>
    <message>
        <location filename="../sources/LogWidget.cpp" line="41"/>
        <location filename="../sources/LogWidget.cpp" line="311"/>
        <source>Warning</source>
        <translation>Warning</translation>
    </message>
    <message>
        <location filename="../sources/LogWidget.cpp" line="47"/>
        <location filename="../sources/LogWidget.cpp" line="312"/>
        <source>Error</source>
        <translation>Error</translation>
    </message>
    <message>
        <location filename="../sources/LogWidget.cpp" line="53"/>
        <location filename="../sources/LogWidget.cpp" line="313"/>
        <source>Critical</source>
        <translation>Critical</translation>
    </message>
    <message>
        <location filename="../sources/LogWidget.cpp" line="202"/>
        <source>Save Log File</source>
        <translation>Save Log File</translation>
    </message>
    <message>
        <location filename="../sources/LogWidget.cpp" line="216"/>
        <source>Save File Error</source>
        <translation>Save File Error</translation>
    </message>
    <message>
        <location filename="../sources/LogWidget.cpp" line="217"/>
        <source>Cannot save the device settings to file: %1.</source>
        <translation>Cannot save the device settings to file: %1.</translation>
    </message>
    <message>
        <location filename="../sources/LogWidget.cpp" line="218"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
</context>
<context>
    <name>MainApplication</name>
    <message>
        <location filename="../sources/MainApplication.cpp" line="163"/>
        <source>Read file error</source>
        <translation>Read file error</translation>
    </message>
    <message>
        <location filename="../sources/MainApplication.cpp" line="163"/>
        <source>Can&apos;t read the file:%1</source>
        <translation>Can&apos;t read the file:%1</translation>
    </message>
    <message>
        <location filename="../sources/MainApplication.cpp" line="428"/>
        <source>Warning</source>
        <translation>Warning</translation>
    </message>
    <message>
        <location filename="../sources/MainApplication.cpp" line="428"/>
        <source>Failed to get the log file: %1</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>MainWidget</name>
    <message>
        <source>Connect</source>
        <translation type="vanished">Connect</translation>
    </message>
    <message>
        <location filename="../sources/mainWidget.cpp" line="42"/>
        <location filename="../sources/mainWidget.cpp" line="1900"/>
        <source>OverView</source>
        <translation>OverView</translation>
    </message>
    <message>
        <location filename="../sources/mainWidget.cpp" line="47"/>
        <location filename="../sources/mainWidget.cpp" line="1901"/>
        <source>Basic Setting</source>
        <translation>Basic Setting</translation>
    </message>
    <message>
        <location filename="../sources/mainWidget.cpp" line="52"/>
        <location filename="../sources/mainWidget.cpp" line="1902"/>
        <source>Input Setting</source>
        <translation>Input Setting</translation>
    </message>
    <message>
        <location filename="../sources/mainWidget.cpp" line="57"/>
        <location filename="../sources/mainWidget.cpp" line="1903"/>
        <source>Output Setting</source>
        <translation>Output Setting</translation>
    </message>
    <message>
        <location filename="../sources/mainWidget.cpp" line="62"/>
        <location filename="../sources/mainWidget.cpp" line="1904"/>
        <source>Log Page</source>
        <translation>Log Page</translation>
    </message>
    <message>
        <location filename="../sources/mainWidget.cpp" line="90"/>
        <location filename="../sources/mainWidget.cpp" line="942"/>
        <location filename="../sources/mainWidget.cpp" line="1935"/>
        <source>Offline</source>
        <translation>Offline</translation>
    </message>
    <message>
        <location filename="../sources/mainWidget.cpp" line="103"/>
        <source>CPi2000 - Cinema processor</source>
        <translation>CPi2000 - Cinema processor</translation>
    </message>
    <message>
        <location filename="../sources/mainWidget.cpp" line="134"/>
        <location filename="../sources/mainWidget.cpp" line="1927"/>
        <source>Output Level</source>
        <translation>Output Level</translation>
    </message>
    <message>
        <location filename="../sources/mainWidget.cpp" line="230"/>
        <source>You just did some changes on this page. Do you want to apply these changes to the device before you leave this page?</source>
        <translation>You just did some changes on this page. Do you want to apply these changes to the device before you leave this page?</translation>
    </message>
    <message>
        <location filename="../sources/mainWidget.cpp" line="230"/>
        <source>Apply</source>
        <translation>Apply</translation>
    </message>
    <message>
        <location filename="../sources/mainWidget.cpp" line="230"/>
        <location filename="../sources/mainWidget.cpp" line="254"/>
        <source>Discard</source>
        <translation>Discard</translation>
    </message>
    <message>
        <location filename="../sources/mainWidget.cpp" line="254"/>
        <source>You are about to leave this page. Do you want to discard the changes?</source>
        <translation>You are about to leave this page. Do you want to discard the changes?</translation>
    </message>
    <message>
        <location filename="../sources/mainWidget.cpp" line="611"/>
        <source>Connecting...</source>
        <translation>Connecting...</translation>
    </message>
    <message>
        <location filename="../sources/mainWidget.cpp" line="661"/>
        <source>The device&apos;s firmware version is %1. It can be upgraded to %2. However it can still work even you don&apos;t upgrade the firmware.</source>
        <translation>The device&apos;s firmware version is %1. It can be upgraded to %2. However it can still work even you don&apos;t upgrade the firmware.</translation>
    </message>
    <message>
        <source>There is new version of GUI for this device&apos;s firmware. Please upgrade the GUI application ASAP.</source>
        <translation type="obsolete">There is new version of GUI application for this device&apos;s firmware. Please upgrade the GUI application ASAP.</translation>
    </message>
    <message>
        <location filename="../sources/mainWidget.cpp" line="682"/>
        <source>Waring</source>
        <translation>Waring</translation>
    </message>
    <message>
        <source>There is new version of GUI application for this device&apos;s firmware. Please upgrade the GUI application first.</source>
        <translation type="obsolete">There is new version of GUI application for this device&apos;s firmware. Please upgrade the GUI application first.</translation>
    </message>
    <message>
        <location filename="../sources/mainWidget.cpp" line="690"/>
        <source>The device&apos;s firmware version is %1, and it can&apos;t match current GUI application. It must be upgraded to %2 first.</source>
        <translation>The device&apos;s firmware version is %1, and it can&apos;t match current GUI application. It must be upgraded to %2 first.</translation>
    </message>
    <message>
        <location filename="../sources/mainWidget.cpp" line="947"/>
        <location filename="../sources/mainWidget.cpp" line="1938"/>
        <source>Connected</source>
        <translation>Connected</translation>
    </message>
    <message>
        <location filename="../sources/mainWidget.cpp" line="1003"/>
        <source>Connection broken</source>
        <translation>Connection broken</translation>
    </message>
    <message>
        <location filename="../sources/mainWidget.cpp" line="1003"/>
        <source>The CPi2000 is disconnected. Please check the device status!</source>
        <translation>The CPi2000 is disconnected. Please check the device status!</translation>
    </message>
    <message>
        <location filename="../sources/mainWidget.cpp" line="1020"/>
        <location filename="../sources/mainWidget.cpp" line="1026"/>
        <location filename="../sources/mainWidget.cpp" line="1908"/>
        <location filename="../sources/mainWidget.cpp" line="1910"/>
        <source>Device File...</source>
        <translation>Device File...</translation>
    </message>
    <message>
        <location filename="../sources/mainWidget.cpp" line="1023"/>
        <location filename="../sources/mainWidget.cpp" line="1909"/>
        <source>from USB...</source>
        <translation>from USB...</translation>
    </message>
    <message>
        <location filename="../sources/mainWidget.cpp" line="1029"/>
        <location filename="../sources/mainWidget.cpp" line="1911"/>
        <source>to USB...</source>
        <translation>to USB...</translation>
    </message>
    <message>
        <location filename="../sources/mainWidget.cpp" line="1032"/>
        <location filename="../sources/mainWidget.cpp" line="1912"/>
        <source>Exit</source>
        <translation>Exit</translation>
    </message>
    <message>
        <location filename="../sources/mainWidget.cpp" line="1038"/>
        <location filename="../sources/mainWidget.cpp" line="1907"/>
        <location filename="../sources/mainWidget.cpp" line="1913"/>
        <source>File</source>
        <translation>File</translation>
    </message>
    <message>
        <location filename="../sources/mainWidget.cpp" line="1040"/>
        <location filename="../sources/mainWidget.cpp" line="1914"/>
        <source>Load</source>
        <translation>Load</translation>
    </message>
    <message>
        <location filename="../sources/mainWidget.cpp" line="1045"/>
        <location filename="../sources/mainWidget.cpp" line="1915"/>
        <source>Save</source>
        <translation>Save</translation>
    </message>
    <message>
        <location filename="../sources/mainWidget.cpp" line="1051"/>
        <location filename="../sources/mainWidget.cpp" line="1916"/>
        <source>Speaker Editor...</source>
        <translation>Speaker Editor...</translation>
    </message>
    <message>
        <location filename="../sources/mainWidget.cpp" line="1054"/>
        <location filename="../sources/mainWidget.cpp" line="1917"/>
        <source>Import Speaker...</source>
        <translation>Import Speaker...</translation>
    </message>
    <message>
        <location filename="../sources/mainWidget.cpp" line="1057"/>
        <location filename="../sources/mainWidget.cpp" line="1918"/>
        <source>Calibrate Time...</source>
        <translation>Calibrate Time...</translation>
    </message>
    <message>
        <location filename="../sources/mainWidget.cpp" line="1060"/>
        <location filename="../sources/mainWidget.cpp" line="1919"/>
        <source>Upgrade Firmware...</source>
        <translation>Upgrade Firmware...</translation>
    </message>
    <message>
        <location filename="../sources/mainWidget.cpp" line="1064"/>
        <location filename="../sources/mainWidget.cpp" line="1920"/>
        <source>Restore Factory Settings...</source>
        <translation>Restore Factory Settings...</translation>
    </message>
    <message>
        <location filename="../sources/mainWidget.cpp" line="1067"/>
        <location filename="../sources/mainWidget.cpp" line="1921"/>
        <source>Set IP Address...</source>
        <translation>Set IP Address...</translation>
    </message>
    <message>
        <location filename="../sources/mainWidget.cpp" line="1070"/>
        <location filename="../sources/mainWidget.cpp" line="1924"/>
        <source>Zoom...</source>
        <translation>Zoom...</translation>
    </message>
    <message>
        <location filename="../sources/mainWidget.cpp" line="1073"/>
        <location filename="../sources/mainWidget.cpp" line="1925"/>
        <source>Debug Option...</source>
        <translation>Debug Option...</translation>
    </message>
    <message>
        <location filename="../sources/mainWidget.cpp" line="1076"/>
        <location filename="../sources/mainWidget.cpp" line="1922"/>
        <source>Tools</source>
        <translation>Tools</translation>
    </message>
    <message>
        <location filename="../sources/mainWidget.cpp" line="1089"/>
        <location filename="../sources/mainWidget.cpp" line="1923"/>
        <source>About</source>
        <translation>About</translation>
    </message>
    <message>
        <location filename="../sources/mainWidget.cpp" line="1102"/>
        <source>Build Time: %1 %2</source>
        <translation>Build Time: %1 %2</translation>
    </message>
    <message>
        <location filename="../sources/mainWidget.cpp" line="1130"/>
        <source>&lt;h2&gt;CPi2000 Application Version: %1&lt;/h2&gt;</source>
        <translation>&lt;h2&gt;CPi2000 Application Version: %1&lt;/h2&gt;</translation>
    </message>
    <message>
        <location filename="../sources/mainWidget.cpp" line="1131"/>
        <source>&lt;p&gt;CPi2000 Device Firmware Version: %1&lt;/p&gt;</source>
        <translation>&lt;p&gt;CPi2000 Device Firmware Version: %1&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../sources/mainWidget.cpp" line="1132"/>
        <source>&lt;p&gt;CPi2000 A5 Firmware Version: %1&lt;/p&gt;</source>
        <translation>&lt;p&gt;CPi2000 A5 Firmware Version: %1&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../sources/mainWidget.cpp" line="1133"/>
        <source>&lt;p&gt;CPi2000 A5 Software Version: %1&lt;/p&gt;</source>
        <translation>&lt;p&gt;CPi2000 A5 Software Version: %1&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../sources/mainWidget.cpp" line="1134"/>
        <source>&lt;p&gt;CPi2000 DSP Firmware Version: %1&lt;/p&gt;</source>
        <translation>&lt;p&gt;CPi2000 DSP Firmware Version: %1&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../sources/mainWidget.cpp" line="1135"/>
        <source>&lt;p&gt;CPi2000 Front Panel Firmware Version: %1&lt;/p&gt;</source>
        <translation>&lt;p&gt;CPi2000 Front Panel Firmware Version: %1&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../sources/mainWidget.cpp" line="1516"/>
        <source>Current CPi2000 device firmware version is %1, and it will be downgraded to %2. Please backup your device settings before downgrading! </source>
        <translation>Current CPi2000 device firmware version is %1, and it will be downgraded to %2. Please backup your device settings before downgrading! </translation>
    </message>
    <message>
        <location filename="../sources/mainWidget.cpp" line="1521"/>
        <source>Information</source>
        <translation>Information</translation>
    </message>
    <message>
        <location filename="../sources/mainWidget.cpp" line="1521"/>
        <source>Current CPi2000 device firmware matches this GUI application well, and no upgrading is needed!</source>
        <translation>Current CPi2000 device firmware matches this GUI application well, and no upgrading is needed!</translation>
    </message>
    <message>
        <source>&lt;p&gt;Copyright &amp;copy; 2017 Harman Inc.This is the GUI application that used to set the parameters for the device of CPi2000.&lt;/p&gt;</source>
        <translation type="vanished">&lt;p&gt;Copyright &amp;copy; 2017 Harman Inc.This is the GUI application that used to set the parameters for the device of CPi2000.&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../sources/mainWidget.cpp" line="1136"/>
        <source>&lt;p&gt;Copyright &amp;copy; 2017 Harman Inc.&lt;/p&gt;</source>
        <translation>&lt;p&gt;Copyright &amp;copy; 2017 Harman Inc.&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../sources/mainWidget.cpp" line="1142"/>
        <source>About CPi2000</source>
        <translation>About CPi2000</translation>
    </message>
    <message>
        <location filename="../sources/mainWidget.cpp" line="1142"/>
        <location filename="../sources/mainWidget.cpp" line="1381"/>
        <location filename="../sources/mainWidget.cpp" line="1667"/>
        <location filename="../sources/mainWidget.cpp" line="1699"/>
        <location filename="../sources/mainWidget.cpp" line="1782"/>
        <location filename="../sources/mainWidget.cpp" line="1880"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="../sources/mainWidget.cpp" line="230"/>
        <location filename="../sources/mainWidget.cpp" line="254"/>
        <location filename="../sources/mainWidget.cpp" line="1278"/>
        <location filename="../sources/mainWidget.cpp" line="1317"/>
        <location filename="../sources/mainWidget.cpp" line="1381"/>
        <location filename="../sources/mainWidget.cpp" line="1385"/>
        <location filename="../sources/mainWidget.cpp" line="1409"/>
        <location filename="../sources/mainWidget.cpp" line="1468"/>
        <location filename="../sources/mainWidget.cpp" line="1490"/>
        <location filename="../sources/mainWidget.cpp" line="1855"/>
        <source>Warning</source>
        <translation>Warning</translation>
    </message>
    <message>
        <location filename="../sources/mainWidget.cpp" line="1278"/>
        <location filename="../sources/mainWidget.cpp" line="1317"/>
        <source>Can&apos;t find the speaker: %1!</source>
        <translation>Can&apos;t find the speaker: %1!</translation>
    </message>
    <message>
        <location filename="../sources/mainWidget.cpp" line="1306"/>
        <source>System Error</source>
        <translation>System Error</translation>
    </message>
    <message>
        <location filename="../sources/mainWidget.cpp" line="1306"/>
        <source>Can&apos;t find the file name: %1!</source>
        <translation>Can&apos;t find the file name: %1!</translation>
    </message>
    <message>
        <location filename="../sources/mainWidget.cpp" line="1381"/>
        <source>The selected speaker:%1 is being used by CPi2000 now, and it can&apos;t be deleted from the device!</source>
        <translation>The selected speaker:%1 is being used by CPi2000 now, and it can&apos;t be deleted from the device!</translation>
    </message>
    <message>
        <location filename="../sources/mainWidget.cpp" line="1385"/>
        <source>Do you want to delete the speaker: %1 from the speaker presets?</source>
        <translation>Do you want to delete the speaker: %1 from the speaker presets?</translation>
    </message>
    <message>
        <location filename="../sources/mainWidget.cpp" line="1385"/>
        <location filename="../sources/mainWidget.cpp" line="1468"/>
        <source>Yes</source>
        <translation>Yes</translation>
    </message>
    <message>
        <location filename="../sources/mainWidget.cpp" line="1385"/>
        <location filename="../sources/mainWidget.cpp" line="1468"/>
        <source>No</source>
        <translation>No</translation>
    </message>
    <message>
        <location filename="../sources/mainWidget.cpp" line="1409"/>
        <source>Can&apos;t find the speaker: %1 in the speaker presets!</source>
        <translation>Can&apos;t find the speaker: %1 in the speaker presets!</translation>
    </message>
    <message>
        <location filename="../sources/mainWidget.cpp" line="682"/>
        <location filename="../sources/mainWidget.cpp" line="1409"/>
        <source>Ok</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="../sources/mainWidget.cpp" line="1490"/>
        <location filename="../sources/mainWidget.cpp" line="1855"/>
        <source>Can&apos;t connect to CPi2000 device, please check the network connection!</source>
        <translation>Can&apos;t connect to CPi2000 device, please check the network connection!</translation>
    </message>
    <message>
        <location filename="../sources/mainWidget.cpp" line="1468"/>
        <source>Do you want to restore the factory setting? All user data will be lost if you restore the factory setting!</source>
        <translation>Do you want to restore the factory setting? All user data will be lost if you restore the factory setting!</translation>
    </message>
    <message>
        <location filename="../sources/mainWidget.cpp" line="1482"/>
        <source>Restore factory Setting</source>
        <translation>Restore factory Setting</translation>
    </message>
    <message>
        <location filename="../sources/mainWidget.cpp" line="1482"/>
        <source>The factory setting is restored, please re-connect the CPi2000 with new IP Address</source>
        <translation>The factory setting is restored, please re-connect the CPi2000 with new IP Address</translation>
    </message>
    <message>
        <location filename="../sources/mainWidget.cpp" line="1511"/>
        <source>Current CPi2000 device firmware version is %1, and it will be upgraded to %2. Please backup your device settings before upgrading! </source>
        <translation>Current CPi2000 device firmware version is %1, and it will be upgraded to %2. Please backup your device settings before upgrading! </translation>
    </message>
    <message>
        <source>&lt;h2&gt;CPi2000 Firmware Available&lt;/h2&gt;</source>
        <translation type="vanished">&lt;h2&gt;CPi2000 Firmware Available&lt;/h2&gt;</translation>
    </message>
    <message>
        <source>&lt;p&gt;Do you want to upgrade the CPi2000 firmware?&lt;/p&gt;</source>
        <translation type="vanished">&lt;p&gt;Do you want to upgrade the CPi2000 firmware?&lt;/p&gt;</translation>
    </message>
    <message>
        <source>Upgrade CPi2000</source>
        <translation type="vanished">Upgrade CPi2000</translation>
    </message>
    <message>
        <location filename="../sources/mainWidget.cpp" line="230"/>
        <location filename="../sources/mainWidget.cpp" line="254"/>
        <source>Cancel</source>
        <translation>Cancel</translation>
    </message>
    <message>
        <location filename="../sources/mainWidget.cpp" line="681"/>
        <source>There is new version of GUI application for this device&apos;s firmware. Please upgrade the GUI application ASAP.</source>
        <translation>There is new version of GUI application for this device&apos;s firmware. Please upgrade the GUI application ASAP.</translation>
    </message>
    <message>
        <source>&lt;h2&gt;CPi2000 Firmware is up to date&lt;/h2&gt;</source>
        <translation type="vanished">&lt;h2&gt;CPi2000 Firmware is up to date&lt;/h2&gt;</translation>
    </message>
    <message>
        <source>&lt;p&gt;The firmware is up to date, and no firmware is available to upgrade this device! &lt;/p&gt;</source>
        <translation type="vanished">&lt;p&gt;The firmware is up to date, and no firmware is available to upgrade this device! &lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../sources/mainWidget.cpp" line="1575"/>
        <source>Set IP Address</source>
        <translation>Set IP Address</translation>
    </message>
    <message>
        <location filename="../sources/mainWidget.cpp" line="1575"/>
        <source>IP Address has been set, please re-connect the CPi2000 with new IP Address</source>
        <translation>IP Address has been set, please re-connect the CPi2000 with new IP Address</translation>
    </message>
    <message>
        <location filename="../sources/mainWidget.cpp" line="1608"/>
        <location filename="../sources/mainWidget.cpp" line="1648"/>
        <source>Getting USB File Name failed!</source>
        <translation>Getting USB File Name failed!</translation>
    </message>
    <message>
        <location filename="../sources/mainWidget.cpp" line="1614"/>
        <source>Getting USB New File Name failed!</source>
        <translation>Getting USB New File Name failed!</translation>
    </message>
    <message>
        <location filename="../sources/mainWidget.cpp" line="1630"/>
        <source>File is saved successfully</source>
        <translation>File is saved successfully</translation>
    </message>
    <message>
        <location filename="../sources/mainWidget.cpp" line="1630"/>
        <source>Current setting is saved in USB as:%1</source>
        <translation>Current setting is saved in USB as:%1</translation>
    </message>
    <message>
        <location filename="../sources/mainWidget.cpp" line="1634"/>
        <source>Save file failed: No CPi2000 is connected!</source>
        <translation>Save file failed: No CPi2000 is connected!</translation>
    </message>
    <message>
        <location filename="../sources/mainWidget.cpp" line="1667"/>
        <source>USB File is loaded successfully</source>
        <translation>USB File is loaded successfully</translation>
    </message>
    <message>
        <location filename="../sources/mainWidget.cpp" line="1667"/>
        <location filename="../sources/mainWidget.cpp" line="1782"/>
        <source>The device will reboot to reload the device setting. Please re-connect the CPi2000 after the device&apos;s rebooting</source>
        <translation>The device will reboot to reload the device setting. Please re-connect the CPi2000 after the device&apos;s rebooting</translation>
    </message>
    <message>
        <location filename="../sources/mainWidget.cpp" line="1687"/>
        <location filename="../sources/mainWidget.cpp" line="1750"/>
        <source>Device File(*.dev);; All files (*.*)</source>
        <translation>Device File(*.dev);; All files (*.*)</translation>
    </message>
    <message>
        <location filename="../sources/mainWidget.cpp" line="1697"/>
        <source>Save File Error</source>
        <translation>Save File Error</translation>
    </message>
    <message>
        <location filename="../sources/mainWidget.cpp" line="1698"/>
        <source>Cannot save the device settings to file: %1.</source>
        <translation>Cannot save the device settings to file: %1.</translation>
    </message>
    <message>
        <source>Can&apos;t save the device file: No CPi2000 device is connected!</source>
        <translation type="vanished">Can&apos;t save the device file: No CPi2000 device is connected!</translation>
    </message>
    <message>
        <source>Can&apos;t load the device file to CPi2000: No CPi2000 device is connected!</source>
        <translation type="vanished">Can&apos;t load the device file to CPi2000: No CPi2000 device is connected!</translation>
    </message>
    <message>
        <location filename="../sources/mainWidget.cpp" line="1775"/>
        <source>Loading Device File Failed</source>
        <translation>Loading Device File Failed</translation>
    </message>
    <message>
        <location filename="../sources/mainWidget.cpp" line="1775"/>
        <source>Can&apos;t load CPi2000 device file!</source>
        <translation>Can&apos;t load CPi2000 device file!</translation>
    </message>
    <message>
        <location filename="../sources/mainWidget.cpp" line="1782"/>
        <source>Device File is loaded successfully</source>
        <translation>Device File is loaded successfully</translation>
    </message>
    <message>
        <location filename="../sources/mainWidget.cpp" line="1871"/>
        <source>Speaker File(*.db);; All files (*.*)</source>
        <translation>Speaker File(*.db);; All files (*.*)</translation>
    </message>
    <message>
        <location filename="../sources/mainWidget.cpp" line="1880"/>
        <source>Invalid Speakers File</source>
        <translation>Invalid Speakers File</translation>
    </message>
    <message>
        <location filename="../sources/mainWidget.cpp" line="1880"/>
        <source>Failed to import speakers. We will finish it later.</source>
        <translation>Failed to import speakers. We will finish it later.</translation>
    </message>
</context>
<context>
    <name>OptionDlg</name>
    <message>
        <location filename="../sources/ZoomDlg.cpp" line="135"/>
        <source>Ok</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="../sources/ZoomDlg.cpp" line="137"/>
        <source>Enable Debug</source>
        <translation>Enable Debug</translation>
    </message>
    <message>
        <location filename="../sources/ZoomDlg.cpp" line="151"/>
        <source>Debug Option</source>
        <translation>Debug Option</translation>
    </message>
</context>
<context>
    <name>OutputSettingWidget</name>
    <message>
        <location filename="../sources/OutputSettingWidget.cpp" line="79"/>
        <source>Room Level</source>
        <translation>Room Level</translation>
    </message>
    <message>
        <location filename="../sources/OutputSettingWidget.cpp" line="80"/>
        <source>EQ Tuning</source>
        <translation>EQ Tuning</translation>
    </message>
</context>
<context>
    <name>OverviewWidget</name>
    <message>
        <location filename="../sources/OverviewWidget.cpp" line="56"/>
        <location filename="../sources/OverviewWidget.cpp" line="765"/>
        <source>Theater:</source>
        <translation>Theater:</translation>
    </message>
    <message>
        <location filename="../sources/OverviewWidget.cpp" line="60"/>
        <location filename="../sources/OverviewWidget.cpp" line="766"/>
        <source>Hall:</source>
        <translation>Hall:</translation>
    </message>
    <message>
        <location filename="../sources/OverviewWidget.cpp" line="64"/>
        <location filename="../sources/OverviewWidget.cpp" line="782"/>
        <source>&lt;p&gt;Reminder: The Hall name can only contain the captial (A-Z), digital Number (0 - 9), &apos;-&apos; and &apos;_&apos;.&lt;/p&gt;</source>
        <translation>&lt;p&gt;Reminder: The Hall name can only contain the captial (A-Z), digital Number (0 - 9), &apos;-&apos; and &apos;_&apos;.&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../sources/OverviewWidget.cpp" line="69"/>
        <location filename="../sources/OverviewWidget.cpp" line="767"/>
        <source>DHCP:</source>
        <translation>DHCP:</translation>
    </message>
    <message>
        <location filename="../sources/OverviewWidget.cpp" line="73"/>
        <location filename="../sources/OverviewWidget.cpp" line="768"/>
        <source>IP Address:</source>
        <translation>IP Address:</translation>
    </message>
    <message>
        <location filename="../sources/OverviewWidget.cpp" line="89"/>
        <location filename="../sources/OverviewWidget.cpp" line="769"/>
        <source>Subnet Mask:</source>
        <translation>Subnet Mask:</translation>
    </message>
    <message>
        <location filename="../sources/OverviewWidget.cpp" line="106"/>
        <location filename="../sources/OverviewWidget.cpp" line="770"/>
        <source>Default Gateway:</source>
        <translation>Default Gateway:</translation>
    </message>
    <message>
        <location filename="../sources/OverviewWidget.cpp" line="122"/>
        <location filename="../sources/OverviewWidget.cpp" line="771"/>
        <source>Server:</source>
        <translation>Server:</translation>
    </message>
    <message>
        <location filename="../sources/OverviewWidget.cpp" line="126"/>
        <location filename="../sources/OverviewWidget.cpp" line="772"/>
        <source>Projector:</source>
        <translation>Projector:</translation>
    </message>
    <message>
        <location filename="../sources/OverviewWidget.cpp" line="130"/>
        <location filename="../sources/OverviewWidget.cpp" line="773"/>
        <source>CPi2000 S/N:</source>
        <translation>CPi2000 S/N:</translation>
    </message>
    <message>
        <location filename="../sources/OverviewWidget.cpp" line="134"/>
        <source>Not Available</source>
        <translation>Not Available</translation>
    </message>
    <message>
        <location filename="../sources/OverviewWidget.cpp" line="139"/>
        <source>CXM2000 S/N:</source>
        <translation>CXM2000 S/N:</translation>
    </message>
    <message>
        <location filename="../sources/OverviewWidget.cpp" line="143"/>
        <source>Unknown (Not Available)</source>
        <translation>Unknown (Not Available)</translation>
    </message>
    <message>
        <location filename="../sources/OverviewWidget.cpp" line="147"/>
        <location filename="../sources/OverviewWidget.cpp" line="774"/>
        <source>Speaker &amp; Amplifier:</source>
        <translation>Speaker &amp; Amplifier:</translation>
    </message>
    <message>
        <location filename="../sources/OverviewWidget.cpp" line="206"/>
        <location filename="../sources/OverviewWidget.cpp" line="585"/>
        <location filename="../sources/OverviewWidget.cpp" line="775"/>
        <source>Apply</source>
        <translation>Apply</translation>
    </message>
    <message>
        <location filename="../sources/OverviewWidget.cpp" line="369"/>
        <source>Disabled</source>
        <translation>Disabled</translation>
    </message>
    <message>
        <location filename="../sources/OverviewWidget.cpp" line="369"/>
        <source>Enabled</source>
        <translation>Enabled</translation>
    </message>
    <message>
        <location filename="../sources/OverviewWidget.cpp" line="491"/>
        <source>Invalid Theater Name</source>
        <translation>Invalid Theater Name</translation>
    </message>
    <message>
        <location filename="../sources/OverviewWidget.cpp" line="498"/>
        <source>Invalid Hall Name</source>
        <translation>Invalid Hall Name</translation>
    </message>
    <message>
        <location filename="../sources/OverviewWidget.cpp" line="502"/>
        <source>Applying...</source>
        <translation>Applying...</translation>
    </message>
</context>
<context>
    <name>PasswordDlg</name>
    <message>
        <location filename="../sources/SpeakerEditor.cpp" line="19"/>
        <source>Enable Password Protection</source>
        <translation>Enable Password Protection</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerEditor.cpp" line="22"/>
        <source>Enter Password:</source>
        <translation>Enter Password:</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerEditor.cpp" line="25"/>
        <source>Confirm Password:</source>
        <translation>Confirm Password:</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerEditor.cpp" line="34"/>
        <source>The password must contains at least 6 characters.
The password can only contain the following characters:
the captial letter(A-Z), the lowercase letter(a-z), 
digital number (0 - 9), &apos;-&apos; and &apos;_&apos;.</source>
        <translation>The password must contains at least 6 characters.
The password can only contain the following characters:
the captial letter(A-Z), the lowercase letter(a-z), 
digital number (0 - 9), &apos;-&apos; and &apos;_&apos;.</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerEditor.cpp" line="36"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerEditor.cpp" line="39"/>
        <source>Cancel</source>
        <translation>Cancel</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <source>CPi2000 Warning</source>
        <translation type="vanished">CPi2000 Warning</translation>
    </message>
    <message>
        <location filename="../sources/main.cpp" line="53"/>
        <source>Another CPi2000 Application is already running now. You can&apos;t start more than one instance of the application!</source>
        <translation>Another CPi2000 Application is already running now. You can&apos;t start more than one instance of the application!</translation>
    </message>
    <message>
        <location filename="../sources/main.cpp" line="53"/>
        <source>Warning</source>
        <translation>Warning</translation>
    </message>
    <message>
        <location filename="../sources/main.cpp" line="54"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
</context>
<context>
    <name>RoomLevelWidget</name>
    <message>
        <source>Hall 1</source>
        <translation type="vanished">Hall 1</translation>
    </message>
    <message>
        <location filename="../sources/RoomLevelWidget.cpp" line="43"/>
        <location filename="../sources/RoomLevelWidget.cpp" line="681"/>
        <source>Signal Mode</source>
        <translation>Signal Mode</translation>
    </message>
    <message>
        <location filename="../sources/RoomLevelWidget.cpp" line="39"/>
        <location filename="../sources/RoomLevelWidget.cpp" line="682"/>
        <source>To stop RTA setting, you need to set signal mode to &apos;OFF&apos;.</source>
        <translation>To stop RTA setting, you need to set signal mode to &apos;OFF&apos;.</translation>
    </message>
    <message>
        <location filename="../sources/RoomLevelWidget.cpp" line="48"/>
        <location filename="../sources/RoomLevelWidget.cpp" line="62"/>
        <location filename="../sources/RoomLevelWidget.cpp" line="684"/>
        <source>Internal SPL Meter Calibration</source>
        <translation>Internal SPL Meter Calibration</translation>
    </message>
    <message>
        <source>Internal meter uncalibrated</source>
        <translation type="vanished">Internal meter uncalibrated</translation>
    </message>
    <message>
        <location filename="../sources/RoomLevelWidget.cpp" line="65"/>
        <location filename="../sources/RoomLevelWidget.cpp" line="685"/>
        <source>Measured Value</source>
        <translation>Measured Value</translation>
    </message>
    <message>
        <location filename="../sources/RoomLevelWidget.cpp" line="82"/>
        <location filename="../sources/RoomLevelWidget.cpp" line="94"/>
        <source>0.0 dB</source>
        <translation>0.0 dB</translation>
    </message>
    <message>
        <location filename="../sources/RoomLevelWidget.cpp" line="86"/>
        <location filename="../sources/RoomLevelWidget.cpp" line="686"/>
        <source>Target Mic Level</source>
        <translation>Target Mic Level</translation>
    </message>
    <message>
        <source>Internal SPL Meter</source>
        <translation type="vanished">Internal SPL Meter</translation>
    </message>
    <message>
        <location filename="../sources/RoomLevelWidget.cpp" line="97"/>
        <location filename="../sources/RoomLevelWidget.cpp" line="485"/>
        <location filename="../sources/RoomLevelWidget.cpp" line="687"/>
        <source>uncal</source>
        <translation>uncal</translation>
    </message>
    <message>
        <location filename="../sources/RoomLevelWidget.cpp" line="105"/>
        <location filename="../sources/RoomLevelWidget.cpp" line="688"/>
        <source>Output Channel Level</source>
        <translation>Output Channel Level</translation>
    </message>
    <message>
        <location filename="../sources/RoomLevelWidget.cpp" line="110"/>
        <location filename="../sources/RoomLevelWidget.cpp" line="691"/>
        <source>Left</source>
        <translation>Left</translation>
    </message>
    <message>
        <location filename="../sources/RoomLevelWidget.cpp" line="110"/>
        <location filename="../sources/RoomLevelWidget.cpp" line="691"/>
        <source>Right</source>
        <translation>Right</translation>
    </message>
    <message>
        <location filename="../sources/RoomLevelWidget.cpp" line="110"/>
        <location filename="../sources/RoomLevelWidget.cpp" line="691"/>
        <source>Center</source>
        <translation>Center</translation>
    </message>
    <message>
        <location filename="../sources/RoomLevelWidget.cpp" line="110"/>
        <location filename="../sources/RoomLevelWidget.cpp" line="691"/>
        <source>Subwoofer</source>
        <translation>Subwoofer</translation>
    </message>
    <message>
        <location filename="../sources/RoomLevelWidget.cpp" line="110"/>
        <location filename="../sources/RoomLevelWidget.cpp" line="691"/>
        <source>Left Surround</source>
        <translation>Left Surround</translation>
    </message>
    <message>
        <location filename="../sources/RoomLevelWidget.cpp" line="110"/>
        <location filename="../sources/RoomLevelWidget.cpp" line="691"/>
        <source>Right Surround</source>
        <translation>Right Surround</translation>
    </message>
    <message>
        <location filename="../sources/RoomLevelWidget.cpp" line="110"/>
        <location filename="../sources/RoomLevelWidget.cpp" line="691"/>
        <source>Back Left Surround</source>
        <translation>Back Left Surround</translation>
    </message>
    <message>
        <location filename="../sources/RoomLevelWidget.cpp" line="110"/>
        <location filename="../sources/RoomLevelWidget.cpp" line="691"/>
        <source>Back Right Surround</source>
        <translation>Back Right Surround</translation>
    </message>
    <message>
        <location filename="../sources/RoomLevelWidget.cpp" line="166"/>
        <location filename="../sources/RoomLevelWidget.cpp" line="696"/>
        <source>Speaker</source>
        <translation>Speaker</translation>
    </message>
    <message>
        <location filename="../sources/RoomLevelWidget.cpp" line="170"/>
        <location filename="../sources/RoomLevelWidget.cpp" line="697"/>
        <source>Amp</source>
        <translation>Amp</translation>
    </message>
    <message>
        <location filename="../sources/RoomLevelWidget.cpp" line="174"/>
        <location filename="../sources/RoomLevelWidget.cpp" line="698"/>
        <source>Rotate</source>
        <translation>Rotate</translation>
    </message>
    <message>
        <source>Internal meter calibrated</source>
        <translation type="vanished">Internal meter calibrated</translation>
    </message>
    <message>
        <location filename="../sources/RoomLevelWidget.cpp" line="495"/>
        <source>(target 85 dBC)</source>
        <translation>(target 85 dBC)</translation>
    </message>
    <message>
        <location filename="../sources/RoomLevelWidget.cpp" line="499"/>
        <source>(target 82 dBC)</source>
        <translation>(target 82 dBC)</translation>
    </message>
</context>
<context>
    <name>SpeakerAndAmplifierWidget</name>
    <message>
        <location filename="../sources/SpeakerAndAmplifierWidget.cpp" line="85"/>
        <location filename="../sources/SpeakerAndAmplifierWidget.cpp" line="400"/>
        <source>Speaker:</source>
        <translation>Speaker:</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerAndAmplifierWidget.cpp" line="89"/>
        <location filename="../sources/SpeakerAndAmplifierWidget.cpp" line="401"/>
        <source>Amplifier:</source>
        <translation>Amplifier:</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerAndAmplifierWidget.cpp" line="149"/>
        <source>Full-range</source>
        <translation>Full-range</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerAndAmplifierWidget.cpp" line="149"/>
        <source>Two-way</source>
        <translation>Two-way</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerAndAmplifierWidget.cpp" line="149"/>
        <source>Three-way</source>
        <translation>Three-way</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerAndAmplifierWidget.cpp" line="160"/>
        <source>Warning: The type of the Left Speaker is %1,
 and the right is %2;
They are not the same type!</source>
        <translation>Warning: The type of the Left Speaker is %1,
 and the right is %2;
They are not the same type!</translation>
    </message>
    <message>
        <source>The type of the Left Speaker is %1,
 and the right is %2;
They are not the same type!</source>
        <translation type="vanished">The type of the Left Speaker is %1,
 and the right is %2;
They are not the same type!</translation>
    </message>
    <message>
        <source>The type of the Left Speaker is %1, and the right is %2;
They are not the same type!</source>
        <translation type="vanished">The type of the Left Speaker is %1, and the right is %2;
They are not the same type!</translation>
    </message>
    <message>
        <source>Passive</source>
        <translation type="vanished">Passive</translation>
    </message>
    <message>
        <source>Bi-Amp</source>
        <translation type="vanished">Bi-Amp</translation>
    </message>
    <message>
        <source>Tri-Amp</source>
        <translation type="vanished">Tri-Amp</translation>
    </message>
    <message>
        <source>The type of the Left Speaker is %1; The type of the right Speaker is %2;
They are not the same type!</source>
        <translation type="vanished">The type of the Left Speaker is %1; The type of the right Speaker is %2;
They are not the same type!</translation>
    </message>
</context>
<context>
    <name>SpeakerEditor</name>
    <message>
        <location filename="../sources/SpeakerEditor.cpp" line="224"/>
        <source>Speaker Name:</source>
        <translation>Speaker Name:</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerEditor.cpp" line="227"/>
        <source>Speaker Type:</source>
        <translation>Speaker Type:</translation>
    </message>
    <message>
        <source>Passive</source>
        <translation type="vanished">Passive</translation>
    </message>
    <message>
        <source>Bi-Amp</source>
        <translation type="vanished">Bi-Amp</translation>
    </message>
    <message>
        <source>Tri-Amp</source>
        <translation type="vanished">Tri-Amp</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerEditor.cpp" line="235"/>
        <source>Full-range</source>
        <translation>Full-range</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerEditor.cpp" line="236"/>
        <source>Two-way</source>
        <translation>Two-way</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerEditor.cpp" line="237"/>
        <source>Three-way</source>
        <translation>Three-way</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerEditor.cpp" line="238"/>
        <source>Surround</source>
        <translation>Surround</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerEditor.cpp" line="239"/>
        <source>Subwoofer</source>
        <translation>Subwoofer</translation>
    </message>
    <message>
        <source>Readable</source>
        <translation type="vanished">Readable</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerEditor.cpp" line="244"/>
        <source>Generator:</source>
        <translation>Generator:</translation>
    </message>
    <message>
        <source>Apply Channel:</source>
        <translation type="vanished">Apply Channel:</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerEditor.cpp" line="247"/>
        <source>Assign to:</source>
        <translation>Assign to:</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerEditor.cpp" line="289"/>
        <source>Writable</source>
        <translation>Writable</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerEditor.cpp" line="294"/>
        <location filename="../sources/SpeakerEditor.cpp" line="1179"/>
        <source>Password Enabled</source>
        <translation>Password Enabled</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerEditor.cpp" line="302"/>
        <location filename="../sources/SpeakerEditor.cpp" line="971"/>
        <location filename="../sources/SpeakerEditor.cpp" line="981"/>
        <source>LF</source>
        <translation>LF</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerEditor.cpp" line="307"/>
        <location filename="../sources/SpeakerEditor.cpp" line="971"/>
        <source>MF</source>
        <translation>MF</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerEditor.cpp" line="312"/>
        <location filename="../sources/SpeakerEditor.cpp" line="971"/>
        <location filename="../sources/SpeakerEditor.cpp" line="981"/>
        <source>HF</source>
        <translation>HF</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerEditor.cpp" line="331"/>
        <source>Load from file</source>
        <translation>Load from file</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerEditor.cpp" line="334"/>
        <source>Save to file</source>
        <translation>Save to file</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerEditor.cpp" line="337"/>
        <source>Apply</source>
        <translation>Apply</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerEditor.cpp" line="341"/>
        <source>Exit</source>
        <translation>Exit</translation>
    </message>
    <message>
        <source>EQ Enabled</source>
        <translation type="vanished">EQ Enabled</translation>
    </message>
    <message>
        <source>EQ Index:</source>
        <translation type="vanished">EQ Index:</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerEditor.cpp" line="203"/>
        <source>Speaker Editor</source>
        <translation>Speaker Editor</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerEditor.cpp" line="348"/>
        <location filename="../sources/SpeakerEditor.cpp" line="1043"/>
        <location filename="../sources/SpeakerEditor.cpp" line="2158"/>
        <source>PEQ Enabled</source>
        <translation>PEQ Enabled</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerEditor.cpp" line="353"/>
        <source>Parametric EQ:</source>
        <translation>Parametric EQ:</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerEditor.cpp" line="357"/>
        <source>Crossover EQ:</source>
        <translation>Crossover EQ:</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerEditor.cpp" line="392"/>
        <source>Band Pass Filter Equalizer</source>
        <translation>Band Pass Filter Equalizer</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerEditor.cpp" line="395"/>
        <location filename="../sources/SpeakerEditor.cpp" line="480"/>
        <source>Type:</source>
        <translation>Type:</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerEditor.cpp" line="399"/>
        <source>IIR</source>
        <translation>IIR</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerEditor.cpp" line="400"/>
        <source>FIR</source>
        <translation>FIR</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerEditor.cpp" line="407"/>
        <location filename="../sources/SpeakerEditor.cpp" line="483"/>
        <source>Slope:</source>
        <translation>Slope:</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerEditor.cpp" line="410"/>
        <source>Tab:</source>
        <translation>Tab:</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerEditor.cpp" line="413"/>
        <location filename="../sources/SpeakerEditor.cpp" line="486"/>
        <source>Frequency:</source>
        <translation>Frequency:</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerEditor.cpp" line="416"/>
        <source>Frequency</source>
        <translation>Frequency</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerEditor.cpp" line="427"/>
        <location filename="../sources/SpeakerEditor.cpp" line="492"/>
        <location filename="../sources/SpeakerEditor.cpp" line="1116"/>
        <location filename="../sources/SpeakerEditor.cpp" line="1671"/>
        <location filename="../sources/SpeakerEditor.cpp" line="1756"/>
        <location filename="../sources/SpeakerEditor.cpp" line="1906"/>
        <source>Enabled</source>
        <translation>Enabled</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerEditor.cpp" line="455"/>
        <source>FIR Equalizer</source>
        <translation>FIR Equalizer</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerEditor.cpp" line="458"/>
        <source>384 tabs FIR:</source>
        <translation>384 tabs FIR:</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerEditor.cpp" line="461"/>
        <source>Enable FIR</source>
        <translation>Enable FIR</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerEditor.cpp" line="465"/>
        <source>Set to Default</source>
        <translation>Set to Default</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerEditor.cpp" line="468"/>
        <source>Paste from Clipboard</source>
        <translation>Paste from Clipboard</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerEditor.cpp" line="477"/>
        <source>PEQ Equalizer</source>
        <translation>PEQ Equalizer</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerEditor.cpp" line="489"/>
        <source>Q:</source>
        <translation>Q:</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerEditor.cpp" line="498"/>
        <source>Bell</source>
        <translation>Bell</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerEditor.cpp" line="499"/>
        <source>Low Shelf</source>
        <translation>Low Shelf</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerEditor.cpp" line="500"/>
        <source>High shelf</source>
        <translation>High shelf</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerEditor.cpp" line="503"/>
        <source>Delete</source>
        <translation>Delete</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerEditor.cpp" line="515"/>
        <location filename="../sources/SpeakerEditor.cpp" line="561"/>
        <source>Gain:</source>
        <translation>Gain:</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerEditor.cpp" line="536"/>
        <source>Gain</source>
        <translation>Gain</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerEditor.cpp" line="558"/>
        <source>General setting</source>
        <translation>General setting</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerEditor.cpp" line="564"/>
        <source>Delay:</source>
        <translation>Delay:</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerEditor.cpp" line="584"/>
        <location filename="../sources/SpeakerEditor.cpp" line="1021"/>
        <location filename="../sources/SpeakerEditor.cpp" line="1542"/>
        <source>Polarity (+)</source>
        <translation>Polarity (+)</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerEditor.cpp" line="604"/>
        <location filename="../sources/SpeakerEditor.cpp" line="1103"/>
        <source> Enable </source>
        <translation> Enable </translation>
    </message>
    <message>
        <location filename="../sources/SpeakerEditor.cpp" line="1025"/>
        <location filename="../sources/SpeakerEditor.cpp" line="1546"/>
        <source>Polarity (-)</source>
        <translation>Polarity (-)</translation>
    </message>
    <message>
        <source>Polarity:</source>
        <translation type="vanished">Polarity:</translation>
    </message>
    <message>
        <source>Normal</source>
        <translation type="vanished">Normal</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerEditor.cpp" line="589"/>
        <source>Output limiter</source>
        <translation>Output limiter</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerEditor.cpp" line="592"/>
        <source>Threshold:</source>
        <translation>Threshold:</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerEditor.cpp" line="595"/>
        <source>Attack time:</source>
        <translation>Attack time:</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerEditor.cpp" line="598"/>
        <source>Release:</source>
        <translation>Release:</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerEditor.cpp" line="601"/>
        <source>Hold:</source>
        <translation>Hold:</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerEditor.cpp" line="1490"/>
        <location filename="../sources/SpeakerEditor.cpp" line="1499"/>
        <source>Invalid FIR Data, please check the data in the clipboard!</source>
        <translation>Invalid FIR Data, please check the data in the clipboard!</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerEditor.cpp" line="1675"/>
        <location filename="../sources/SpeakerEditor.cpp" line="1760"/>
        <location filename="../sources/SpeakerEditor.cpp" line="1910"/>
        <source>Enable</source>
        <translation>Enable</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerEditor.cpp" line="608"/>
        <source>Auto</source>
        <translation>Auto</translation>
    </message>
    <message>
        <source>Inverted</source>
        <translation type="vanished">Inverted</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerEditor.cpp" line="1047"/>
        <location filename="../sources/SpeakerEditor.cpp" line="2162"/>
        <source>Enable All</source>
        <translation>Enable All</translation>
    </message>
    <message>
        <source>Disabled</source>
        <translation type="vanished">Disabled</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerEditor.cpp" line="1175"/>
        <source>Password Disabled</source>
        <translation>Password Disabled</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerEditor.cpp" line="1194"/>
        <location filename="../sources/SpeakerEditor.cpp" line="1241"/>
        <source>Speaker File(*.spk);; All files (*.*)</source>
        <translation>Speaker File(*.spk);; All files (*.*)</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerEditor.cpp" line="1205"/>
        <location filename="../sources/SpeakerEditor.cpp" line="1252"/>
        <source>Save File Error</source>
        <translation>Save File Error</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerEditor.cpp" line="1206"/>
        <location filename="../sources/SpeakerEditor.cpp" line="1253"/>
        <source>Cannot save the speaker settings to file: %1.</source>
        <translation>Cannot save the speaker settings to file: %1.</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerEditor.cpp" line="1207"/>
        <location filename="../sources/SpeakerEditor.cpp" line="1254"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerEditor.cpp" line="1157"/>
        <location filename="../sources/SpeakerEditor.cpp" line="1219"/>
        <location filename="../sources/SpeakerEditor.cpp" line="1490"/>
        <location filename="../sources/SpeakerEditor.cpp" line="1499"/>
        <location filename="../sources/SpeakerEditor.cpp" line="1513"/>
        <location filename="../sources/SpeakerEditor.cpp" line="1523"/>
        <source>Warning</source>
        <translation>Warning</translation>
    </message>
    <message>
        <source>Signal Mode:</source>
        <translation type="vanished">Signal Mode</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerEditor.cpp" line="1157"/>
        <source>The speaker you are editing is set to unwritable! If it takes effect, you won&apos;t modify the settings of this speaker any more!</source>
        <translation>The speaker you are editing is set to unwritable! If it takes effect, you won&apos;t modify the settings of this speaker any more!</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerEditor.cpp" line="1224"/>
        <source>Loading failed</source>
        <translation>Loading failed</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerEditor.cpp" line="1224"/>
        <source>The speaker type in the loaded file is %1, and you can&apos;t apply it at current output channel!</source>
        <translation>The speaker type in the loaded file is %1, and you can&apos;t apply it at current output channel!</translation>
    </message>
    <message>
        <source>Invalid FIR Data, please check the data at index %1: %2!</source>
        <translation type="vanished">Invalid FIR Data, please check the data at index %1: %2!</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerEditor.cpp" line="1513"/>
        <source>Invalid FIR Data, please check it again!</source>
        <translation>Invalid FIR Data, please check it again!</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerEditor.cpp" line="1523"/>
        <source>Invalid FIR Data: CPi2000 can only support those FIR which tab is not bigger than 384!</source>
        <translation>Invalid FIR Data: CPi2000 can only support those FIR which tab is not bigger than 384!</translation>
    </message>
</context>
<context>
    <name>SpeakerImportDialog</name>
    <message>
        <location filename="../sources/SpeakerImportDialog.cpp" line="10"/>
        <source>Speaker List:</source>
        <translation>Speaker List:</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerImportDialog.cpp" line="16"/>
        <source>New Speaker</source>
        <translation>New Speaker</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerImportDialog.cpp" line="20"/>
        <source>Apply channel:</source>
        <translation>Apply channel:</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerImportDialog.cpp" line="21"/>
        <source>Left</source>
        <translation>Left</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerImportDialog.cpp" line="22"/>
        <source>Right</source>
        <translation>Right</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerImportDialog.cpp" line="23"/>
        <source>Center</source>
        <translation>Center</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerImportDialog.cpp" line="24"/>
        <source>Left Surround</source>
        <translation>Left Surround</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerImportDialog.cpp" line="25"/>
        <source>Right Surround</source>
        <translation>Right Surround</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerImportDialog.cpp" line="26"/>
        <source>Back Left Surround</source>
        <translation>Back Left Surround</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerImportDialog.cpp" line="27"/>
        <source>Back Right Surround</source>
        <translation>Back Right Surround</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerImportDialog.cpp" line="28"/>
        <location filename="../sources/SpeakerImportDialog.cpp" line="70"/>
        <source>Subwoofer</source>
        <translation>Subwoofer</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerImportDialog.cpp" line="48"/>
        <source>Delete</source>
        <translation>Delete</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerImportDialog.cpp" line="51"/>
        <source>Modify</source>
        <translation>Modify</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerImportDialog.cpp" line="54"/>
        <source>Cancel</source>
        <translation>Cancel</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerImportDialog.cpp" line="57"/>
        <source>Speaker name:</source>
        <translation>Speaker name:</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerImportDialog.cpp" line="63"/>
        <source>Speaker type</source>
        <translation>Speaker type</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerImportDialog.cpp" line="66"/>
        <source>Passive</source>
        <translation>Passive</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerImportDialog.cpp" line="67"/>
        <source>Bi-Amp</source>
        <translation>Bi-Amp</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerImportDialog.cpp" line="68"/>
        <source>Tri-Amp</source>
        <translation>Tri-Amp</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerImportDialog.cpp" line="69"/>
        <source>Surround</source>
        <translation>Surround</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerImportDialog.cpp" line="74"/>
        <source>Property</source>
        <translation>Property</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerImportDialog.cpp" line="75"/>
        <source>Readable</source>
        <translation>Readable</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerImportDialog.cpp" line="76"/>
        <source>Writable</source>
        <translation>Writable</translation>
    </message>
</context>
<context>
    <name>SpeakerListDialog</name>
    <message>
        <location filename="../sources/SpeakerListDialog.cpp" line="11"/>
        <source>CPi2000 Speaker</source>
        <translation>CPi2000 Speaker</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerListDialog.cpp" line="12"/>
        <source>Speaker List:</source>
        <translation>Speaker List:</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerListDialog.cpp" line="13"/>
        <source>Invalid Speaker Name</source>
        <translation>Invalid Speaker Name</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerListDialog.cpp" line="20"/>
        <source>New Speaker</source>
        <translation>New Speaker</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerListDialog.cpp" line="24"/>
        <source>Apply channel:</source>
        <translation>Apply channel:</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerListDialog.cpp" line="25"/>
        <source>Left</source>
        <translation>Left</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerListDialog.cpp" line="26"/>
        <source>Right</source>
        <translation>Right</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerListDialog.cpp" line="27"/>
        <source>Center</source>
        <translation>Center</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerListDialog.cpp" line="28"/>
        <source>Left Surround</source>
        <translation>Left Surround</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerListDialog.cpp" line="29"/>
        <source>Right Surround</source>
        <translation>Right Surround</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerListDialog.cpp" line="30"/>
        <source>Back Left Surround</source>
        <translation>Back Left Surround</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerListDialog.cpp" line="31"/>
        <source>Back Right Surround</source>
        <translation>Back Right Surround</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerListDialog.cpp" line="32"/>
        <location filename="../sources/SpeakerListDialog.cpp" line="74"/>
        <source>Subwoofer</source>
        <translation>Subwoofer</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerListDialog.cpp" line="52"/>
        <source>Delete</source>
        <translation>Delete</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerListDialog.cpp" line="55"/>
        <location filename="../sources/SpeakerListDialog.cpp" line="564"/>
        <location filename="../sources/SpeakerListDialog.cpp" line="613"/>
        <source>Modify</source>
        <translation>Modify</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerListDialog.cpp" line="58"/>
        <source>Cancel</source>
        <translation>Cancel</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerListDialog.cpp" line="61"/>
        <source>Speaker name:</source>
        <translation>Speaker name:</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerListDialog.cpp" line="67"/>
        <source>Speaker type</source>
        <translation>Speaker type</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerListDialog.cpp" line="70"/>
        <source>Full-range</source>
        <translation>Full-range</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerListDialog.cpp" line="71"/>
        <source>Two-way</source>
        <translation>Two-way</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerListDialog.cpp" line="72"/>
        <source>Three-way</source>
        <translation>Three-way</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerListDialog.cpp" line="88"/>
        <source>Hide Unwritable</source>
        <translation>Hide Unwritable</translation>
    </message>
    <message>
        <source>Passive</source>
        <translation type="vanished">Passive</translation>
    </message>
    <message>
        <source>Bi-Amp</source>
        <translation type="vanished">Bi-Amp</translation>
    </message>
    <message>
        <source>Tri-Amp</source>
        <translation type="vanished">Tri-Amp</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerListDialog.cpp" line="73"/>
        <source>Surround</source>
        <translation>Surround</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerListDialog.cpp" line="78"/>
        <source>Property</source>
        <translation>Property</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerListDialog.cpp" line="79"/>
        <source>Readable</source>
        <translation>Readable</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerListDialog.cpp" line="80"/>
        <source>Writable</source>
        <translation>Writable</translation>
    </message>
    <message>
        <source>Hide Uneditable</source>
        <translation type="vanished">Hide Uneditable</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerListDialog.cpp" line="204"/>
        <source>Password protection</source>
        <translation>Password protection</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerListDialog.cpp" line="204"/>
        <source>Please Enter Password:</source>
        <translation>Please Enter Password:</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerListDialog.cpp" line="211"/>
        <source>Warning</source>
        <translation>Warning</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerListDialog.cpp" line="211"/>
        <source>Invalid password! You can&apos;t edit this speaker&apos;s setting</source>
        <translation>Invalid password! You can&apos;t edit this speaker&apos;s setting</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerListDialog.cpp" line="560"/>
        <location filename="../sources/SpeakerListDialog.cpp" line="609"/>
        <source>New</source>
        <translation>New</translation>
    </message>
</context>
<context>
    <name>SpeakerTypeDialog</name>
    <message>
        <location filename="../sources/SpeakerAndAmplifierWidget.cpp" line="9"/>
        <source>Speaker Name:</source>
        <translation>Speaker Name:</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerAndAmplifierWidget.cpp" line="10"/>
        <source>Speaker EQ:</source>
        <translation>Speaker EQ:</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerAndAmplifierWidget.cpp" line="11"/>
        <source>EQ File:</source>
        <translation>EQ File:</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerAndAmplifierWidget.cpp" line="18"/>
        <location filename="../sources/SpeakerAndAmplifierWidget.cpp" line="51"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerAndAmplifierWidget.cpp" line="20"/>
        <source>Cancel</source>
        <translation>Cancel</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerAndAmplifierWidget.cpp" line="25"/>
        <source>Enable</source>
        <translation>Enable</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerAndAmplifierWidget.cpp" line="28"/>
        <source>Speaker Type</source>
        <translation>Speaker Type</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerAndAmplifierWidget.cpp" line="39"/>
        <source>Speaker EQ File(*.eq);; All files (*.*)</source>
        <translation>Speaker EQ File(*.eq);; All files (*.*)</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerAndAmplifierWidget.cpp" line="50"/>
        <source>Open File Error</source>
        <translation>Open File Error</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerAndAmplifierWidget.cpp" line="51"/>
        <source>Cannot open the speaker EQ file: %1.</source>
        <translation>Cannot open the speaker EQ file: %1.</translation>
    </message>
</context>
<context>
    <name>TimeAdjustDialog</name>
    <message>
        <location filename="../sources/UpgradeDialog.cpp" line="489"/>
        <source>Calibrate Time</source>
        <translation>Calibrate Time</translation>
    </message>
    <message>
        <location filename="../sources/UpgradeDialog.cpp" line="490"/>
        <source>PC Host Time:</source>
        <translation>PC Host Time:</translation>
    </message>
    <message>
        <location filename="../sources/UpgradeDialog.cpp" line="494"/>
        <source>Device Time:</source>
        <translation>Device Time:</translation>
    </message>
    <message>
        <location filename="../sources/UpgradeDialog.cpp" line="506"/>
        <source>Sync Time</source>
        <translation>Sync Time</translation>
    </message>
    <message>
        <location filename="../sources/UpgradeDialog.cpp" line="509"/>
        <source>Exit</source>
        <translation>Exit</translation>
    </message>
    <message>
        <location filename="../sources/UpgradeDialog.cpp" line="577"/>
        <source>Warning</source>
        <translation>Warning</translation>
    </message>
    <message>
        <location filename="../sources/UpgradeDialog.cpp" line="577"/>
        <source>Can&apos;t connect to CPi2000 device, please check the network connection!</source>
        <translation>Can&apos;t connect to CPi2000 device, please check the network connection!</translation>
    </message>
</context>
<context>
    <name>USBFileDialog</name>
    <message>
        <source>USB File List</source>
        <translation type="vanished">USB File List</translation>
    </message>
    <message>
        <location filename="../sources/USBFileDialog.cpp" line="9"/>
        <source>USB Disk on CPi2000</source>
        <translation>USB Disk on CPi2000</translation>
    </message>
    <message>
        <location filename="../sources/USBFileDialog.cpp" line="11"/>
        <source>File Name:</source>
        <translation>File Name:</translation>
    </message>
    <message>
        <location filename="../sources/USBFileDialog.cpp" line="13"/>
        <source>File name can only contain the following characters: 
the captial (A-Z), digital Number (0 - 9), &apos;-&apos; and &apos;_&apos;</source>
        <translation>
          File name can only contain the following characters:
          the captial (A-Z), digital Number (0 - 9), &apos;-&apos; and &apos;_&apos;</translation>
    </message>
    <message>
        <location filename="../sources/USBFileDialog.cpp" line="28"/>
        <source>Cancel</source>
        <translation>Cancel</translation>
    </message>
    <message>
        <location filename="../sources/USBFileDialog.cpp" line="31"/>
        <source>New File</source>
        <translation>New File</translation>
    </message>
    <message>
        <location filename="../sources/USBFileDialog.cpp" line="96"/>
        <source>Save as</source>
        <translation>Save as</translation>
    </message>
    <message>
        <location filename="../sources/USBFileDialog.cpp" line="97"/>
        <source>Save to USB File on CPi2000</source>
        <translation>Save to USB File on CPi2000</translation>
    </message>
    <message>
        <location filename="../sources/USBFileDialog.cpp" line="102"/>
        <source>Load USB file on CPi2000</source>
        <translation>Load USB file on CPi2000</translation>
    </message>
    <message>
        <location filename="../sources/USBFileDialog.cpp" line="103"/>
        <source>Load from USB device on CPi2000</source>
        <translation>Load from USB device on CPi2000</translation>
    </message>
    <message>
        <source>Save to USB File</source>
        <translation type="vanished">Save to USB File</translation>
    </message>
    <message>
        <location filename="../sources/USBFileDialog.cpp" line="98"/>
        <source>Save</source>
        <translation>Save</translation>
    </message>
    <message>
        <source>Load USB file</source>
        <translation type="vanished">Load USB file</translation>
    </message>
    <message>
        <source>Load from USB File</source>
        <translation type="vanished">Load from USB File</translation>
    </message>
    <message>
        <location filename="../sources/USBFileDialog.cpp" line="104"/>
        <source>Load</source>
        <translation>Load</translation>
    </message>
    <message>
        <location filename="../sources/USBFileDialog.cpp" line="149"/>
        <source>Save as New</source>
        <translation>Save as New</translation>
    </message>
    <message>
        <location filename="../sources/USBFileDialog.cpp" line="155"/>
        <source>Replace</source>
        <translation>Replace</translation>
    </message>
</context>
<context>
    <name>UpgradeDialog</name>
    <message>
        <source>Do you want to start upgrading process?</source>
        <translation type="vanished">Do you want to start upgrading process?</translation>
    </message>
    <message>
        <location filename="../sources/UpgradeDialog.cpp" line="20"/>
        <location filename="../sources/UpgradeDialog.cpp" line="38"/>
        <source>Do you want to start the upgrading process?</source>
        <translation>Do you want to start the upgrading process?</translation>
    </message>
    <message>
        <location filename="../sources/UpgradeDialog.cpp" line="24"/>
        <source>Do you want to start the downgrading process?</source>
        <translation>Do you want to start the downgrading process?</translation>
    </message>
    <message>
        <location filename="../sources/UpgradeDialog.cpp" line="45"/>
        <source>Yes</source>
        <translation>Yes</translation>
    </message>
    <message>
        <location filename="../sources/UpgradeDialog.cpp" line="49"/>
        <source>No</source>
        <translation>No</translation>
    </message>
    <message>
        <location filename="../sources/UpgradeDialog.cpp" line="85"/>
        <source>Warning</source>
        <translation>Warning</translation>
    </message>
    <message>
        <location filename="../sources/UpgradeDialog.cpp" line="85"/>
        <source>Can&apos;t connect to CPi2000 device, please check the network connection!</source>
        <translation>Can&apos;t connect to CPi2000 device, please check the network connection!</translation>
    </message>
    <message>
        <location filename="../sources/UpgradeDialog.cpp" line="98"/>
        <source>Upgrading...</source>
        <translation>Upgrading...</translation>
    </message>
    <message>
        <location filename="../sources/UpgradeDialog.cpp" line="102"/>
        <source>Downgrading...</source>
        <translation>Downgrading...</translation>
    </message>
    <message>
        <location filename="../sources/UpgradeDialog.cpp" line="158"/>
        <source>Network Abnormal</source>
        <translation>Network Abnormal</translation>
    </message>
    <message>
        <source>Loading Device File Failed</source>
        <translation type="vanished">Loading Device File Failed</translation>
    </message>
    <message>
        <location filename="../sources/UpgradeDialog.cpp" line="158"/>
        <source>Failed to transfer CPi2000 device file!</source>
        <translation>Failed to transfer CPi2000 device file!</translation>
    </message>
    <message>
        <source>Network Error</source>
        <translation type="obsolete">Network Error</translation>
    </message>
    <message>
        <location filename="../sources/UpgradeDialog.cpp" line="169"/>
        <source>Firmware is transfered to CPi2000 device, please re-connect the CPi2000 after its upgrading.</source>
        <translation>Firmware is transfered to CPi2000 device, please re-connect the CPi2000 after its upgrading.</translation>
    </message>
    <message>
        <location filename="../sources/UpgradeDialog.cpp" line="173"/>
        <source>Firmware is transfered to CPi2000 device, please re-connect the CPi2000 after its downgrading.</source>
        <translation>Firmware is transfered to CPi2000 device, please re-connect the CPi2000 after its downgrading.</translation>
    </message>
    <message>
        <location filename="../sources/UpgradeDialog.cpp" line="178"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
</context>
<context>
    <name>ZoomDlg</name>
    <message>
        <location filename="../sources/ZoomDlg.cpp" line="10"/>
        <source>Enable Scale</source>
        <translation>Enable Scale</translation>
    </message>
    <message>
        <location filename="../sources/ZoomDlg.cpp" line="14"/>
        <source>Scale Factor:</source>
        <translation>Scale Factor:</translation>
    </message>
    <message>
        <location filename="../sources/ZoomDlg.cpp" line="38"/>
        <source>Apply</source>
        <translation>Apply</translation>
    </message>
    <message>
        <location filename="../sources/ZoomDlg.cpp" line="41"/>
        <source>Cancel</source>
        <translation>Cancel</translation>
    </message>
    <message>
        <location filename="../sources/ZoomDlg.cpp" line="121"/>
        <source>Warning</source>
        <translation>Warning</translation>
    </message>
    <message>
        <location filename="../sources/ZoomDlg.cpp" line="121"/>
        <source>You are changing the display scale settings, and this may lead to abnormal display of the GUI application. If the display of GUI application is abnormal, please delete the file: %1 manually.
Do you want to continue?</source>
        <translation>You are changing the display scale settings, and this may lead to abnormal display of the GUI application. If the display of GUI application is abnormal, please delete the file: %1 manually.
Do you want to continue?</translation>
    </message>
    <message>
        <location filename="../sources/ZoomDlg.cpp" line="127"/>
        <source>Information</source>
        <translation>Information</translation>
    </message>
    <message>
        <location filename="../sources/ZoomDlg.cpp" line="127"/>
        <source>The scale settings will take effect atftr you restart the GUI applicaiton.</source>
        <translation>The scale settings will take effect atftr you restart the GUI applicaiton.</translation>
    </message>
</context>
</TS>
