<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_CN">
<context>
    <name>BasicSettingWidget</name>
    <message>
        <location filename="../sources/BasicSettingWidget.cpp" line="14"/>
        <location filename="../sources/BasicSettingWidget.cpp" line="839"/>
        <source>   Surround Delay</source>
        <translation>   环绕延迟</translation>
    </message>
    <message>
        <source>   Load Detection</source>
        <translation type="vanished">   负载检测</translation>
    </message>
    <message>
        <location filename="../sources/BasicSettingWidget.cpp" line="18"/>
        <location filename="../sources/BasicSettingWidget.cpp" line="841"/>
        <source>   Main Audio Setting</source>
        <translation>   主音频设置</translation>
    </message>
    <message>
        <location filename="../sources/BasicSettingWidget.cpp" line="20"/>
        <location filename="../sources/BasicSettingWidget.cpp" line="842"/>
        <source>Master Volume</source>
        <translation>主音量</translation>
    </message>
    <message>
        <location filename="../sources/BasicSettingWidget.cpp" line="28"/>
        <location filename="../sources/BasicSettingWidget.cpp" line="843"/>
        <source>Fade In:</source>
        <translation>淡入:</translation>
    </message>
    <message>
        <location filename="../sources/BasicSettingWidget.cpp" line="31"/>
        <location filename="../sources/BasicSettingWidget.cpp" line="844"/>
        <source>Fade Out:</source>
        <translation>淡出:</translation>
    </message>
    <message>
        <location filename="../sources/BasicSettingWidget.cpp" line="43"/>
        <location filename="../sources/BasicSettingWidget.cpp" line="845"/>
        <source>Sync to preset</source>
        <translation>同步到预设值</translation>
    </message>
    <message>
        <location filename="../sources/BasicSettingWidget.cpp" line="84"/>
        <location filename="../sources/BasicSettingWidget.cpp" line="848"/>
        <source>Average distance from screen to rear wall of the theater</source>
        <translation>影院前后平均距离</translation>
    </message>
    <message>
        <location filename="../sources/BasicSettingWidget.cpp" line="86"/>
        <location filename="../sources/BasicSettingWidget.cpp" line="849"/>
        <source>Average distance between left and right surround speakers</source>
        <translation>影院左右平均距离</translation>
    </message>
    <message>
        <location filename="../sources/BasicSettingWidget.cpp" line="88"/>
        <location filename="../sources/BasicSettingWidget.cpp" line="90"/>
        <location filename="../sources/BasicSettingWidget.cpp" line="139"/>
        <location filename="../sources/BasicSettingWidget.cpp" line="373"/>
        <location filename="../sources/BasicSettingWidget.cpp" line="374"/>
        <location filename="../sources/BasicSettingWidget.cpp" line="850"/>
        <location filename="../sources/BasicSettingWidget.cpp" line="851"/>
        <location filename="../sources/BasicSettingWidget.cpp" line="853"/>
        <source>Meters</source>
        <translation>米</translation>
    </message>
    <message>
        <location filename="../sources/BasicSettingWidget.cpp" line="108"/>
        <location filename="../sources/BasicSettingWidget.cpp" line="858"/>
        <source>calculate</source>
        <translation>计算</translation>
    </message>
    <message>
        <location filename="../sources/BasicSettingWidget.cpp" line="111"/>
        <location filename="../sources/BasicSettingWidget.cpp" line="857"/>
        <source>Delay time:</source>
        <translation>延时时长:</translation>
    </message>
    <message>
        <location filename="../sources/BasicSettingWidget.cpp" line="135"/>
        <location filename="../sources/BasicSettingWidget.cpp" line="351"/>
        <location filename="../sources/BasicSettingWidget.cpp" line="352"/>
        <location filename="../sources/BasicSettingWidget.cpp" line="852"/>
        <source>Feet</source>
        <translation>英尺</translation>
    </message>
    <message>
        <location filename="../sources/BasicSettingWidget.cpp" line="16"/>
        <location filename="../sources/BasicSettingWidget.cpp" line="840"/>
        <source>   Fault Detection</source>
        <translation>   故障检测</translation>
    </message>
    <message>
        <source>Note: Only Crown XLC2500 and XLC2800 support Fault Detection!</source>
        <translation type="vanished">注意:仅Crown XLC2500与Crown XLC 2800支持故障检测！</translation>
    </message>
    <message>
        <location filename="../sources/BasicSettingWidget.cpp" line="164"/>
        <location filename="../sources/BasicSettingWidget.cpp" line="854"/>
        <source>Remind: Only Crown XLC2500 and XLC2800 support Fault Detection!</source>
        <translation>注意:仅Crown XLC2500与Crown XLC 2800支持故障检测！</translation>
    </message>
    <message>
        <source>LS/RS1</source>
        <translation type="vanished">LS/RS1</translation>
    </message>
    <message>
        <source>LS/RS2</source>
        <translation type="vanished">LS/RS2</translation>
    </message>
    <message>
        <source>LEFT1</source>
        <translation type="vanished">LEFT1</translation>
    </message>
    <message>
        <source>LEFT2</source>
        <translation type="vanished">LEFT2</translation>
    </message>
    <message>
        <source>CENTER1</source>
        <translation type="vanished">CENTER1</translation>
    </message>
    <message>
        <source>CENTER2</source>
        <translation type="vanished">CENTER2</translation>
    </message>
    <message>
        <source>RIGHT1</source>
        <translation type="vanished">RIGHT1</translation>
    </message>
    <message>
        <source>Bls/Brs1</source>
        <translation type="vanished">Bls/Brs1</translation>
    </message>
    <message>
        <source>SW1</source>
        <translation type="vanished">SW1</translation>
    </message>
    <message>
        <source>SW2</source>
        <translation type="vanished">SW2</translation>
    </message>
    <message>
        <location filename="../sources/BasicSettingWidget.cpp" line="168"/>
        <location filename="../sources/BasicSettingWidget.cpp" line="855"/>
        <source>5.1 System</source>
        <translation>5.1声道</translation>
    </message>
    <message>
        <location filename="../sources/BasicSettingWidget.cpp" line="172"/>
        <location filename="../sources/BasicSettingWidget.cpp" line="856"/>
        <source>7.1 System</source>
        <translation>7.1声道</translation>
    </message>
</context>
<context>
    <name>DeviceSearchingDialog</name>
    <message>
        <location filename="../sources/deviceSearchingDialog.cpp" line="20"/>
        <source>Available Device List</source>
        <translation>设备列表</translation>
    </message>
    <message>
        <location filename="../sources/deviceSearchingDialog.cpp" line="24"/>
        <source>Device Name</source>
        <translation>设备名</translation>
    </message>
    <message>
        <location filename="../sources/deviceSearchingDialog.cpp" line="24"/>
        <source>Device Address</source>
        <translation>设备地址</translation>
    </message>
    <message>
        <location filename="../sources/deviceSearchingDialog.cpp" line="24"/>
        <source>Firmware</source>
        <translation>固件</translation>
    </message>
    <message>
        <location filename="../sources/deviceSearchingDialog.cpp" line="46"/>
        <source>Scan</source>
        <translation>扫描</translation>
    </message>
    <message>
        <location filename="../sources/deviceSearchingDialog.cpp" line="49"/>
        <source>Connect</source>
        <translation>连接</translation>
    </message>
    <message>
        <location filename="../sources/deviceSearchingDialog.cpp" line="52"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../sources/deviceSearchingDialog.cpp" line="65"/>
        <source>Connect Device</source>
        <translation>连接设备</translation>
    </message>
    <message>
        <location filename="../sources/deviceSearchingDialog.cpp" line="216"/>
        <source>Add device...</source>
        <translation>添加设备...</translation>
    </message>
    <message>
        <location filename="../sources/deviceSearchingDialog.cpp" line="217"/>
        <source>CPi2000 IP Address:</source>
        <translation>CPi2000的IP地址:</translation>
    </message>
    <message>
        <location filename="../sources/deviceSearchingDialog.cpp" line="226"/>
        <location filename="../sources/deviceSearchingDialog.cpp" line="232"/>
        <location filename="../sources/deviceSearchingDialog.cpp" line="238"/>
        <source>Invalid Input</source>
        <translation>无效输入</translation>
    </message>
    <message>
        <location filename="../sources/deviceSearchingDialog.cpp" line="226"/>
        <location filename="../sources/deviceSearchingDialog.cpp" line="232"/>
        <location filename="../sources/deviceSearchingDialog.cpp" line="238"/>
        <source>What you input is not a valid IP Address</source>
        <translation>你输入的IP地址无效！</translation>
    </message>
    <message>
        <location filename="../sources/deviceSearchingDialog.cpp" line="226"/>
        <location filename="../sources/deviceSearchingDialog.cpp" line="232"/>
        <location filename="../sources/deviceSearchingDialog.cpp" line="238"/>
        <source>OK</source>
        <translation>确定</translation>
    </message>
    <message>
        <location filename="../sources/deviceSearchingDialog.cpp" line="245"/>
        <source>Can&apos;t find the CPi2000</source>
        <translation>无法找到CPi2000</translation>
    </message>
    <message>
        <location filename="../sources/deviceSearchingDialog.cpp" line="245"/>
        <source>Failed to connect with the CPi2000 with the IP address: %1, please check the IP address again!</source>
        <translation>无法连接IP地址为:%1的CPi2000设备，请重新检查IP地址！</translation>
    </message>
</context>
<context>
    <name>DigitalInputSettingWidget</name>
    <message>
        <location filename="../sources/DigitalInputSettingWidget.cpp" line="42"/>
        <location filename="../sources/DigitalInputSettingWidget.cpp" line="382"/>
        <source>Undo</source>
        <translation>撤销</translation>
    </message>
    <message>
        <location filename="../sources/DigitalInputSettingWidget.cpp" line="45"/>
        <location filename="../sources/DigitalInputSettingWidget.cpp" line="383"/>
        <source>Apply</source>
        <translation>应用</translation>
    </message>
</context>
<context>
    <name>EQTuningWidget</name>
    <message>
        <location filename="../sources/EQTuningWidget.cpp" line="46"/>
        <location filename="../sources/EQTuningWidget.cpp" line="1549"/>
        <source>Signal Mode</source>
        <translation>信号模式</translation>
    </message>
    <message>
        <location filename="../sources/EQTuningWidget.cpp" line="42"/>
        <location filename="../sources/EQTuningWidget.cpp" line="1550"/>
        <source>To stop RTA setting, you need to set signal mode to &apos;OFF&apos;.</source>
        <translation>请将信号模式设置为“OFF”以结束RTA测试！</translation>
    </message>
    <message>
        <source>Active Channel</source>
        <translation type="vanished">工作通道</translation>
    </message>
    <message>
        <location filename="../sources/EQTuningWidget.cpp" line="83"/>
        <location filename="../sources/EQTuningWidget.cpp" line="1566"/>
        <source>Active Channel Level</source>
        <translation>工作通道音量</translation>
    </message>
    <message>
        <source>Graphic EQ</source>
        <translation type="vanished">图示均衡器</translation>
    </message>
    <message>
        <source>EQ Assist</source>
        <translation type="vanished">均衡器助手</translation>
    </message>
    <message>
        <source>Flatten</source>
        <translation type="vanished">拉平</translation>
    </message>
    <message>
        <location filename="../sources/EQTuningWidget.cpp" line="361"/>
        <location filename="../sources/EQTuningWidget.cpp" line="389"/>
        <source>Copy EQ</source>
        <translation>复制均衡器</translation>
    </message>
    <message>
        <location filename="../sources/EQTuningWidget.cpp" line="362"/>
        <location filename="../sources/EQTuningWidget.cpp" line="390"/>
        <location filename="../sources/EQTuningWidget.cpp" line="788"/>
        <source>Paste EQ</source>
        <translation>粘贴均衡器</translation>
    </message>
    <message>
        <location filename="../sources/EQTuningWidget.cpp" line="1565"/>
        <source>Subwoofer PEQ1</source>
        <translation>超低音参数均衡器1</translation>
    </message>
    <message>
        <location filename="../sources/EQTuningWidget.cpp" line="1567"/>
        <source>Subwoofer PEQ2</source>
        <translation>超低音参数均衡器2</translation>
    </message>
    <message>
        <source>Subwoofer PEQ</source>
        <translation type="vanished">超低音参数均衡器</translation>
    </message>
    <message>
        <location filename="../sources/EQTuningWidget.cpp" line="95"/>
        <location filename="../sources/EQTuningWidget.cpp" line="159"/>
        <location filename="../sources/EQTuningWidget.cpp" line="1555"/>
        <location filename="../sources/EQTuningWidget.cpp" line="1568"/>
        <source>Cut</source>
        <translation>剪切</translation>
    </message>
    <message>
        <location filename="../sources/EQTuningWidget.cpp" line="117"/>
        <location filename="../sources/EQTuningWidget.cpp" line="182"/>
        <location filename="../sources/EQTuningWidget.cpp" line="1556"/>
        <location filename="../sources/EQTuningWidget.cpp" line="1569"/>
        <source>Type:</source>
        <translation>类型:</translation>
    </message>
    <message>
        <location filename="../sources/EQTuningWidget.cpp" line="120"/>
        <location filename="../sources/EQTuningWidget.cpp" line="185"/>
        <location filename="../sources/EQTuningWidget.cpp" line="1557"/>
        <location filename="../sources/EQTuningWidget.cpp" line="1570"/>
        <source>Cut:</source>
        <translation>剪切:</translation>
    </message>
    <message>
        <location filename="../sources/EQTuningWidget.cpp" line="123"/>
        <location filename="../sources/EQTuningWidget.cpp" line="188"/>
        <location filename="../sources/EQTuningWidget.cpp" line="309"/>
        <location filename="../sources/EQTuningWidget.cpp" line="1558"/>
        <location filename="../sources/EQTuningWidget.cpp" line="1571"/>
        <location filename="../sources/EQTuningWidget.cpp" line="1581"/>
        <location filename="../sources/EQTuningWidget.cpp" line="1588"/>
        <source>Frequency:</source>
        <translation>频率:</translation>
    </message>
    <message>
        <location filename="../sources/EQTuningWidget.cpp" line="126"/>
        <location filename="../sources/EQTuningWidget.cpp" line="191"/>
        <location filename="../sources/EQTuningWidget.cpp" line="1559"/>
        <location filename="../sources/EQTuningWidget.cpp" line="1572"/>
        <source>Q:</source>
        <translation>Q:</translation>
    </message>
    <message>
        <location filename="../sources/EQTuningWidget.cpp" line="130"/>
        <location filename="../sources/EQTuningWidget.cpp" line="195"/>
        <location filename="../sources/EQTuningWidget.cpp" line="1562"/>
        <location filename="../sources/EQTuningWidget.cpp" line="1575"/>
        <source>Bell</source>
        <translation>钟形</translation>
    </message>
    <message>
        <source>Paste GEQ (From Channel: %1)</source>
        <translation type="vanished">粘贴图示均衡器(来自%1)</translation>
    </message>
    <message>
        <source>Subwoofer Configuration</source>
        <translation type="vanished">超低音配置</translation>
    </message>
    <message>
        <source>Note: The Center level must be determined by aligning the center channel before the SW channel can be aligned!</source>
        <translation type="vanished">注意:在校正超低音通道音量前，请确保中置通道音量已完成校正！</translation>
    </message>
    <message>
        <source>Note: The Center level must be determined by aligning the center channel before the LFE channel can be aligned!</source>
        <translation type="vanished">注意:在校正超低音之前，请先通过调整中置通道来决定中置级别！</translation>
    </message>
    <message>
        <source>Center Channel Gen</source>
        <translation type="vanished">中置通道信号发生器</translation>
    </message>
    <message>
        <source>Inverse</source>
        <translation type="vanished">反转</translation>
    </message>
    <message>
        <location filename="../sources/EQTuningWidget.cpp" line="1578"/>
        <source>Active Channel EQ</source>
        <translation>工作通道均衡器</translation>
    </message>
    <message>
        <location filename="../sources/EQTuningWidget.cpp" line="244"/>
        <location filename="../sources/EQTuningWidget.cpp" line="1586"/>
        <source>Low Shelf Filter</source>
        <translation>低架滤波器</translation>
    </message>
    <message>
        <location filename="../sources/EQTuningWidget.cpp" line="251"/>
        <source>Frequency</source>
        <translation>频率</translation>
    </message>
    <message>
        <location filename="../sources/EQTuningWidget.cpp" line="248"/>
        <location filename="../sources/EQTuningWidget.cpp" line="255"/>
        <location filename="../sources/EQTuningWidget.cpp" line="306"/>
        <location filename="../sources/EQTuningWidget.cpp" line="1580"/>
        <location filename="../sources/EQTuningWidget.cpp" line="1585"/>
        <source>Gain</source>
        <translation>增益</translation>
    </message>
    <message>
        <location filename="../sources/EQTuningWidget.cpp" line="259"/>
        <source>Slope</source>
        <translation>斜率</translation>
    </message>
    <message>
        <location filename="../sources/EQTuningWidget.cpp" line="313"/>
        <location filename="../sources/EQTuningWidget.cpp" line="1582"/>
        <location filename="../sources/EQTuningWidget.cpp" line="1589"/>
        <source>Gain:</source>
        <translation>增益:</translation>
    </message>
    <message>
        <location filename="../sources/EQTuningWidget.cpp" line="317"/>
        <location filename="../sources/EQTuningWidget.cpp" line="1583"/>
        <location filename="../sources/EQTuningWidget.cpp" line="1590"/>
        <source>Slope:</source>
        <translation>斜率:</translation>
    </message>
    <message>
        <source>Copy GEQ</source>
        <translation type="vanished">复制图示均衡器</translation>
    </message>
    <message>
        <source>Paste GEQ</source>
        <translation type="vanished">粘贴图示均衡器</translation>
    </message>
    <message>
        <source>Paste GEQ From Channel: %1</source>
        <translation type="obsolete">粘贴图示均衡器l: %1</translation>
    </message>
    <message>
        <location filename="../sources/EQTuningWidget.cpp" line="1587"/>
        <source>High Shelf Filter</source>
        <translation>高架滤波器</translation>
    </message>
    <message>
        <source>1/3 Octave Graphic EQ</source>
        <translation type="vanished">1/3倍频程图示均衡器</translation>
    </message>
    <message>
        <location filename="../sources/EQTuningWidget.cpp" line="400"/>
        <location filename="../sources/EQTuningWidget.cpp" line="797"/>
        <source>Paste EQ (From %1)</source>
        <translation>粘贴均衡器(来自%1)</translation>
    </message>
</context>
<context>
    <name>GEQWidget</name>
    <message>
        <source>RTA Range</source>
        <translation type="vanished">RTA范围</translation>
    </message>
    <message>
        <location filename="../sources/GEQWidget.cpp" line="406"/>
        <location filename="../sources/GEQWidget.cpp" line="754"/>
        <source>1/3 Octave Graphic EQ</source>
        <translation>1/3倍频程图示均衡器</translation>
    </message>
    <message>
        <location filename="../sources/GEQWidget.cpp" line="410"/>
        <source>Full</source>
        <translation>Full</translation>
    </message>
    <message>
        <location filename="../sources/GEQWidget.cpp" line="414"/>
        <source>X-Curve</source>
        <translation>X-Curve</translation>
    </message>
    <message>
        <location filename="../sources/GEQWidget.cpp" line="506"/>
        <location filename="../sources/GEQWidget.cpp" line="749"/>
        <source>EQ Assist</source>
        <translation>均衡器助手</translation>
    </message>
    <message>
        <location filename="../sources/GEQWidget.cpp" line="507"/>
        <location filename="../sources/GEQWidget.cpp" line="750"/>
        <source>Flatten</source>
        <translation>拉平</translation>
    </message>
    <message>
        <location filename="../sources/GEQWidget.cpp" line="508"/>
        <location filename="../sources/GEQWidget.cpp" line="751"/>
        <source>Copy EQ</source>
        <translation>复制均衡器</translation>
    </message>
    <message>
        <location filename="../sources/GEQWidget.cpp" line="509"/>
        <location filename="../sources/GEQWidget.cpp" line="752"/>
        <source>Paste EQ</source>
        <translation>粘贴均衡器</translation>
    </message>
    <message>
        <source>Paste GEQ</source>
        <translation type="vanished">粘贴图示均衡器</translation>
    </message>
    <message>
        <location filename="../sources/GEQWidget.cpp" line="510"/>
        <location filename="../sources/GEQWidget.cpp" line="753"/>
        <source>Graphic EQ</source>
        <translation>图示均衡器</translation>
    </message>
    <message>
        <source>Copy GEQ</source>
        <translation type="vanished">复制图示均衡器</translation>
    </message>
</context>
<context>
    <name>IPAddressDialog</name>
    <message>
        <location filename="../sources/UpgradeDialog.cpp" line="185"/>
        <source>Set CPi2000 device IP Address</source>
        <translation>设置CPi2000的IP地址</translation>
    </message>
    <message>
        <location filename="../sources/UpgradeDialog.cpp" line="201"/>
        <source>Enable</source>
        <translation>启用</translation>
    </message>
    <message>
        <location filename="../sources/UpgradeDialog.cpp" line="205"/>
        <source>Disable</source>
        <translation>禁用</translation>
    </message>
    <message>
        <location filename="../sources/UpgradeDialog.cpp" line="213"/>
        <source>DHCP:</source>
        <translation>DHCP:</translation>
    </message>
    <message>
        <location filename="../sources/UpgradeDialog.cpp" line="217"/>
        <source>IP Address:</source>
        <translation>IP地址:</translation>
    </message>
    <message>
        <location filename="../sources/UpgradeDialog.cpp" line="233"/>
        <source>Subnet Mask:</source>
        <translation>子网掩码:</translation>
    </message>
    <message>
        <location filename="../sources/UpgradeDialog.cpp" line="250"/>
        <source>Default Gateway:</source>
        <translation>默认网关:</translation>
    </message>
    <message>
        <location filename="../sources/UpgradeDialog.cpp" line="266"/>
        <source>Apply</source>
        <translation>应用</translation>
    </message>
    <message>
        <location filename="../sources/UpgradeDialog.cpp" line="270"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../sources/UpgradeDialog.cpp" line="348"/>
        <source>IP Address</source>
        <translation>IP地址</translation>
    </message>
    <message>
        <location filename="../sources/UpgradeDialog.cpp" line="348"/>
        <source>Gateway</source>
        <translation>网关</translation>
    </message>
    <message>
        <location filename="../sources/UpgradeDialog.cpp" line="348"/>
        <source>Subnet mask</source>
        <translation>子网掩码</translation>
    </message>
    <message>
        <location filename="../sources/UpgradeDialog.cpp" line="353"/>
        <location filename="../sources/UpgradeDialog.cpp" line="386"/>
        <location filename="../sources/UpgradeDialog.cpp" line="393"/>
        <location filename="../sources/UpgradeDialog.cpp" line="403"/>
        <location filename="../sources/UpgradeDialog.cpp" line="410"/>
        <source>Format Error</source>
        <translation>格式错误</translation>
    </message>
    <message>
        <location filename="../sources/UpgradeDialog.cpp" line="353"/>
        <source>Please input valid %1!</source>
        <translation>请输入正确的 %1!</translation>
    </message>
    <message>
        <location filename="../sources/UpgradeDialog.cpp" line="386"/>
        <source>Subnet mask error: %1 is not valie subnet mask!</source>
        <translation>子网掩码错误：%1不是一个合法的子网掩码！</translation>
    </message>
    <message>
        <location filename="../sources/UpgradeDialog.cpp" line="393"/>
        <source>Gateway should be in the same subnet with IP address!</source>
        <translation>网关和IP地址应该在同一个子网内！</translation>
    </message>
    <message>
        <location filename="../sources/UpgradeDialog.cpp" line="403"/>
        <source>IP Address should not be broadcast address!</source>
        <translation>IP地址不应该被设为广播地址！</translation>
    </message>
    <message>
        <location filename="../sources/UpgradeDialog.cpp" line="410"/>
        <source>Gateway should not be broadcast address!</source>
        <translation>网管不应该被设为广播地址！</translation>
    </message>
</context>
<context>
    <name>InputSettingWidget</name>
    <message>
        <location filename="../sources/InputSettingWidget.cpp" line="15"/>
        <location filename="../sources/InputSettingWidget.cpp" line="1232"/>
        <source>      DIGITAL</source>
        <translation>      数字</translation>
    </message>
    <message>
        <location filename="../sources/InputSettingWidget.cpp" line="18"/>
        <location filename="../sources/InputSettingWidget.cpp" line="1233"/>
        <source>      ANALOG</source>
        <translation>      模拟</translation>
    </message>
    <message>
        <location filename="../sources/InputSettingWidget.cpp" line="21"/>
        <location filename="../sources/InputSettingWidget.cpp" line="1234"/>
        <source>      MIC</source>
        <translation>      麦克风</translation>
    </message>
    <message>
        <location filename="../sources/InputSettingWidget.cpp" line="24"/>
        <location filename="../sources/InputSettingWidget.cpp" line="1235"/>
        <source>      MUSIC(NON-SYNC)</source>
        <translation>      音乐（非同步）</translation>
    </message>
    <message>
        <location filename="../sources/InputSettingWidget.cpp" line="57"/>
        <location filename="../sources/InputSettingWidget.cpp" line="131"/>
        <location filename="../sources/InputSettingWidget.cpp" line="205"/>
        <location filename="../sources/InputSettingWidget.cpp" line="295"/>
        <location filename="../sources/InputSettingWidget.cpp" line="1236"/>
        <location filename="../sources/InputSettingWidget.cpp" line="1252"/>
        <location filename="../sources/InputSettingWidget.cpp" line="1254"/>
        <location filename="../sources/InputSettingWidget.cpp" line="1260"/>
        <source>Input Gain</source>
        <translation>输入增益</translation>
    </message>
    <message>
        <location filename="../sources/InputSettingWidget.cpp" line="88"/>
        <location filename="../sources/InputSettingWidget.cpp" line="162"/>
        <location filename="../sources/InputSettingWidget.cpp" line="232"/>
        <location filename="../sources/InputSettingWidget.cpp" line="322"/>
        <location filename="../sources/InputSettingWidget.cpp" line="1237"/>
        <location filename="../sources/InputSettingWidget.cpp" line="1253"/>
        <location filename="../sources/InputSettingWidget.cpp" line="1255"/>
        <location filename="../sources/InputSettingWidget.cpp" line="1261"/>
        <source>Global Audio Delay</source>
        <translation>全局音频延迟</translation>
    </message>
    <message>
        <location filename="../sources/InputSettingWidget.cpp" line="248"/>
        <location filename="../sources/InputSettingWidget.cpp" line="339"/>
        <location filename="../sources/InputSettingWidget.cpp" line="1256"/>
        <location filename="../sources/InputSettingWidget.cpp" line="1257"/>
        <source>Channel Assignment</source>
        <translation>通道分配</translation>
    </message>
    <message>
        <location filename="../sources/InputSettingWidget.cpp" line="251"/>
        <location filename="../sources/InputSettingWidget.cpp" line="1264"/>
        <source>Center</source>
        <translation>中置</translation>
    </message>
    <message>
        <location filename="../sources/InputSettingWidget.cpp" line="256"/>
        <location filename="../sources/InputSettingWidget.cpp" line="1258"/>
        <source>Surrounds</source>
        <translation>环绕</translation>
    </message>
    <message>
        <location filename="../sources/InputSettingWidget.cpp" line="262"/>
        <location filename="../sources/InputSettingWidget.cpp" line="1259"/>
        <source>Phantom Power</source>
        <translation>幻象电源</translation>
    </message>
    <message>
        <location filename="../sources/InputSettingWidget.cpp" line="542"/>
        <source>Digital</source>
        <translation>数字</translation>
    </message>
    <message>
        <location filename="../sources/InputSettingWidget.cpp" line="549"/>
        <source>Analog</source>
        <translation>模拟</translation>
    </message>
    <message>
        <location filename="../sources/InputSettingWidget.cpp" line="556"/>
        <source>Mic</source>
        <translation>麦克风</translation>
    </message>
    <message>
        <location filename="../sources/InputSettingWidget.cpp" line="563"/>
        <source>Music</source>
        <translation>音乐</translation>
    </message>
    <message>
        <location filename="../sources/InputSettingWidget.cpp" line="569"/>
        <source>Change Input Source</source>
        <translation>更改输入源</translation>
    </message>
    <message>
        <location filename="../sources/InputSettingWidget.cpp" line="569"/>
        <source>Do you want to change input source to: </source>
        <translation>您是否想将输入源更改为:</translation>
    </message>
    <message>
        <location filename="../sources/InputSettingWidget.cpp" line="573"/>
        <source>Yes</source>
        <translation>是</translation>
    </message>
    <message>
        <location filename="../sources/InputSettingWidget.cpp" line="574"/>
        <source>No</source>
        <translation>否</translation>
    </message>
    <message>
        <location filename="../sources/InputSettingWidget.cpp" line="959"/>
        <location filename="../sources/InputSettingWidget.cpp" line="980"/>
        <location filename="../sources/InputSettingWidget.cpp" line="1001"/>
        <location filename="../sources/InputSettingWidget.cpp" line="1022"/>
        <location filename="../sources/InputSettingWidget.cpp" line="1244"/>
        <source>Enabled</source>
        <translation>已启用</translation>
    </message>
    <message>
        <location filename="../sources/InputSettingWidget.cpp" line="965"/>
        <location filename="../sources/InputSettingWidget.cpp" line="986"/>
        <location filename="../sources/InputSettingWidget.cpp" line="1007"/>
        <location filename="../sources/InputSettingWidget.cpp" line="1028"/>
        <location filename="../sources/InputSettingWidget.cpp" line="1248"/>
        <source>Enable</source>
        <translation>已禁用</translation>
    </message>
    <message>
        <source>Disabled</source>
        <translation type="vanished">已禁用</translation>
    </message>
</context>
<context>
    <name>LogWidget</name>
    <message>
        <location filename="../sources/LogWidget.cpp" line="11"/>
        <location filename="../sources/LogWidget.cpp" line="304"/>
        <source>Date</source>
        <translation>日期</translation>
    </message>
    <message>
        <location filename="../sources/LogWidget.cpp" line="11"/>
        <location filename="../sources/LogWidget.cpp" line="304"/>
        <source>Time</source>
        <translation>时间</translation>
    </message>
    <message>
        <location filename="../sources/LogWidget.cpp" line="11"/>
        <location filename="../sources/LogWidget.cpp" line="304"/>
        <source>Level</source>
        <translation>级别</translation>
    </message>
    <message>
        <location filename="../sources/LogWidget.cpp" line="11"/>
        <location filename="../sources/LogWidget.cpp" line="304"/>
        <source>Content</source>
        <translation>内容</translation>
    </message>
    <message>
        <location filename="../sources/LogWidget.cpp" line="25"/>
        <location filename="../sources/LogWidget.cpp" line="307"/>
        <source>Refresh Log</source>
        <translation>刷新日志</translation>
    </message>
    <message>
        <location filename="../sources/LogWidget.cpp" line="28"/>
        <location filename="../sources/LogWidget.cpp" line="308"/>
        <source>Save Log</source>
        <translation>保存日志</translation>
    </message>
    <message>
        <location filename="../sources/LogWidget.cpp" line="31"/>
        <location filename="../sources/LogWidget.cpp" line="309"/>
        <source>Clear Log</source>
        <translation>清除日志</translation>
    </message>
    <message>
        <location filename="../sources/LogWidget.cpp" line="35"/>
        <location filename="../sources/LogWidget.cpp" line="310"/>
        <source>Information</source>
        <translation>信息</translation>
    </message>
    <message>
        <location filename="../sources/LogWidget.cpp" line="41"/>
        <location filename="../sources/LogWidget.cpp" line="311"/>
        <source>Warning</source>
        <translation>警告</translation>
    </message>
    <message>
        <location filename="../sources/LogWidget.cpp" line="47"/>
        <location filename="../sources/LogWidget.cpp" line="312"/>
        <source>Error</source>
        <translation>错误</translation>
    </message>
    <message>
        <location filename="../sources/LogWidget.cpp" line="53"/>
        <location filename="../sources/LogWidget.cpp" line="313"/>
        <source>Critical</source>
        <translation>故障</translation>
    </message>
    <message>
        <location filename="../sources/LogWidget.cpp" line="202"/>
        <source>Save Log File</source>
        <translation>保存日志文件</translation>
    </message>
    <message>
        <location filename="../sources/LogWidget.cpp" line="216"/>
        <source>Save File Error</source>
        <translation>保存文件错误</translation>
    </message>
    <message>
        <location filename="../sources/LogWidget.cpp" line="217"/>
        <source>Cannot save the device settings to file: %1.</source>
        <translation>无法将设备参数保存到文件: %1</translation>
    </message>
    <message>
        <location filename="../sources/LogWidget.cpp" line="218"/>
        <source>OK</source>
        <translation>确定</translation>
    </message>
</context>
<context>
    <name>MainApplication</name>
    <message>
        <location filename="../sources/MainApplication.cpp" line="163"/>
        <source>Read file error</source>
        <translation>读取文件错误</translation>
    </message>
    <message>
        <location filename="../sources/MainApplication.cpp" line="163"/>
        <source>Can&apos;t read the file:%1</source>
        <translation>无法读取文件: %1</translation>
    </message>
    <message>
        <location filename="../sources/MainApplication.cpp" line="428"/>
        <source>Warning</source>
        <translation>警告</translation>
    </message>
    <message>
        <location filename="../sources/MainApplication.cpp" line="428"/>
        <source>Failed to get the log file: %1</source>
        <translation>无法获取日志文件: %1</translation>
    </message>
</context>
<context>
    <name>MainWidget</name>
    <message>
        <source>Connect</source>
        <translation type="vanished">连接</translation>
    </message>
    <message>
        <location filename="../sources/mainWidget.cpp" line="42"/>
        <location filename="../sources/mainWidget.cpp" line="1900"/>
        <source>OverView</source>
        <translation>系统概览</translation>
    </message>
    <message>
        <location filename="../sources/mainWidget.cpp" line="47"/>
        <location filename="../sources/mainWidget.cpp" line="1901"/>
        <source>Basic Setting</source>
        <translation>基本设置</translation>
    </message>
    <message>
        <location filename="../sources/mainWidget.cpp" line="52"/>
        <location filename="../sources/mainWidget.cpp" line="1902"/>
        <source>Input Setting</source>
        <translation>输入设置</translation>
    </message>
    <message>
        <location filename="../sources/mainWidget.cpp" line="57"/>
        <location filename="../sources/mainWidget.cpp" line="1903"/>
        <source>Output Setting</source>
        <translation>输出设置</translation>
    </message>
    <message>
        <location filename="../sources/mainWidget.cpp" line="62"/>
        <location filename="../sources/mainWidget.cpp" line="1904"/>
        <source>Log Page</source>
        <translation>设备日志</translation>
    </message>
    <message>
        <location filename="../sources/mainWidget.cpp" line="90"/>
        <location filename="../sources/mainWidget.cpp" line="942"/>
        <location filename="../sources/mainWidget.cpp" line="1935"/>
        <source>Offline</source>
        <translation>已断开</translation>
    </message>
    <message>
        <location filename="../sources/mainWidget.cpp" line="103"/>
        <source>CPi2000 - Cinema processor</source>
        <translation>CPi2000 - 影院处理器</translation>
    </message>
    <message>
        <location filename="../sources/mainWidget.cpp" line="134"/>
        <location filename="../sources/mainWidget.cpp" line="1927"/>
        <source>Output Level</source>
        <translation>输出音量</translation>
    </message>
    <message>
        <location filename="../sources/mainWidget.cpp" line="230"/>
        <source>You just did some changes on this page. Do you want to apply these changes to the device before you leave this page?</source>
        <translation>你在本页上做了些修改。在离开本页面前是否需要将这些改动应用到设备上？</translation>
    </message>
    <message>
        <location filename="../sources/mainWidget.cpp" line="230"/>
        <source>Apply</source>
        <translation>应用</translation>
    </message>
    <message>
        <location filename="../sources/mainWidget.cpp" line="230"/>
        <location filename="../sources/mainWidget.cpp" line="254"/>
        <source>Discard</source>
        <translation>放弃改动</translation>
    </message>
    <message>
        <location filename="../sources/mainWidget.cpp" line="254"/>
        <source>You are about to leave this page. Do you want to discard the changes?</source>
        <translation>你将离开本页面，你是否要放弃你所做的改动？</translation>
    </message>
    <message>
        <location filename="../sources/mainWidget.cpp" line="611"/>
        <source>Connecting...</source>
        <translation>正在连接...</translation>
    </message>
    <message>
        <location filename="../sources/mainWidget.cpp" line="661"/>
        <source>The device&apos;s firmware version is %1. It can be upgraded to %2. However it can still work even you don&apos;t upgrade the firmware.</source>
        <translation>设备的固件版本为%1,可以升级到%2.当然该设备即使不升级也能工作.</translation>
    </message>
    <message>
        <source>There is new version of GUI for this device&apos;s firmware. Please upgrade the GUI application ASAP.</source>
        <translation type="vanished">已发布新版本的GUI应用程序，它可以更好地匹配该CPi2000设备。请尽快升级GUI应用程序。</translation>
    </message>
    <message>
        <location filename="../sources/mainWidget.cpp" line="682"/>
        <source>Waring</source>
        <translation>警告</translation>
    </message>
    <message>
        <source>There is new version of GUI application for this device&apos;s firmware. Please upgrade the GUI application first.</source>
        <translation type="obsolete">已发布新版本的GUI应用程序，它可以更好地匹配该CPi2000设备。请先升级GUI应用程序。</translation>
    </message>
    <message>
        <location filename="../sources/mainWidget.cpp" line="690"/>
        <source>The device&apos;s firmware version is %1, and it can&apos;t match current GUI application. It must be upgraded to %2 first.</source>
        <translation>设备的固件版本为%1,该版本固件无法与当前的GUI应用程序匹配。请先将设备的固件升级到%2.</translation>
    </message>
    <message>
        <location filename="../sources/mainWidget.cpp" line="947"/>
        <location filename="../sources/mainWidget.cpp" line="1938"/>
        <source>Connected</source>
        <translation>已连接</translation>
    </message>
    <message>
        <location filename="../sources/mainWidget.cpp" line="1003"/>
        <source>Connection broken</source>
        <translation>连接断开</translation>
    </message>
    <message>
        <location filename="../sources/mainWidget.cpp" line="1003"/>
        <source>The CPi2000 is disconnected. Please check the device status!</source>
        <translation>CPi2000未连接，请检查设备状态！</translation>
    </message>
    <message>
        <location filename="../sources/mainWidget.cpp" line="1020"/>
        <location filename="../sources/mainWidget.cpp" line="1026"/>
        <location filename="../sources/mainWidget.cpp" line="1908"/>
        <location filename="../sources/mainWidget.cpp" line="1910"/>
        <source>Device File...</source>
        <translation>设备文件...</translation>
    </message>
    <message>
        <location filename="../sources/mainWidget.cpp" line="1023"/>
        <location filename="../sources/mainWidget.cpp" line="1909"/>
        <source>from USB...</source>
        <translation>由USB导入...</translation>
    </message>
    <message>
        <location filename="../sources/mainWidget.cpp" line="1029"/>
        <location filename="../sources/mainWidget.cpp" line="1911"/>
        <source>to USB...</source>
        <translation>导出至USB...</translation>
    </message>
    <message>
        <location filename="../sources/mainWidget.cpp" line="1032"/>
        <location filename="../sources/mainWidget.cpp" line="1912"/>
        <source>Exit</source>
        <translation>退出</translation>
    </message>
    <message>
        <location filename="../sources/mainWidget.cpp" line="1038"/>
        <location filename="../sources/mainWidget.cpp" line="1907"/>
        <location filename="../sources/mainWidget.cpp" line="1913"/>
        <source>File</source>
        <translation>文件</translation>
    </message>
    <message>
        <location filename="../sources/mainWidget.cpp" line="1040"/>
        <location filename="../sources/mainWidget.cpp" line="1914"/>
        <source>Load</source>
        <translation>加载</translation>
    </message>
    <message>
        <location filename="../sources/mainWidget.cpp" line="1045"/>
        <location filename="../sources/mainWidget.cpp" line="1915"/>
        <source>Save</source>
        <translation>保存</translation>
    </message>
    <message>
        <location filename="../sources/mainWidget.cpp" line="1051"/>
        <location filename="../sources/mainWidget.cpp" line="1916"/>
        <source>Speaker Editor...</source>
        <translation>音箱编辑器...</translation>
    </message>
    <message>
        <location filename="../sources/mainWidget.cpp" line="1054"/>
        <location filename="../sources/mainWidget.cpp" line="1917"/>
        <source>Import Speaker...</source>
        <translation>导入音箱...</translation>
    </message>
    <message>
        <location filename="../sources/mainWidget.cpp" line="1057"/>
        <location filename="../sources/mainWidget.cpp" line="1918"/>
        <source>Calibrate Time...</source>
        <translation>校准时间...</translation>
    </message>
    <message>
        <location filename="../sources/mainWidget.cpp" line="1060"/>
        <location filename="../sources/mainWidget.cpp" line="1919"/>
        <source>Upgrade Firmware...</source>
        <translation>升级固件...</translation>
    </message>
    <message>
        <location filename="../sources/mainWidget.cpp" line="1064"/>
        <location filename="../sources/mainWidget.cpp" line="1920"/>
        <source>Restore Factory Settings...</source>
        <translation>恢复出厂设置...</translation>
    </message>
    <message>
        <location filename="../sources/mainWidget.cpp" line="1067"/>
        <location filename="../sources/mainWidget.cpp" line="1921"/>
        <source>Set IP Address...</source>
        <translation>设置IP地址...</translation>
    </message>
    <message>
        <location filename="../sources/mainWidget.cpp" line="1070"/>
        <location filename="../sources/mainWidget.cpp" line="1924"/>
        <source>Zoom...</source>
        <translation>窗口缩放...</translation>
    </message>
    <message>
        <location filename="../sources/mainWidget.cpp" line="1073"/>
        <location filename="../sources/mainWidget.cpp" line="1925"/>
        <source>Debug Option...</source>
        <translation>调试选项...</translation>
    </message>
    <message>
        <location filename="../sources/mainWidget.cpp" line="1076"/>
        <location filename="../sources/mainWidget.cpp" line="1922"/>
        <source>Tools</source>
        <translation>工具</translation>
    </message>
    <message>
        <location filename="../sources/mainWidget.cpp" line="1089"/>
        <location filename="../sources/mainWidget.cpp" line="1923"/>
        <source>About</source>
        <translation>关于</translation>
    </message>
    <message>
        <location filename="../sources/mainWidget.cpp" line="1102"/>
        <source>Build Time: %1 %2</source>
        <translation>编译时间:%1 %2</translation>
    </message>
    <message>
        <location filename="../sources/mainWidget.cpp" line="1130"/>
        <source>&lt;h2&gt;CPi2000 Application Version: %1&lt;/h2&gt;</source>
        <translation>&lt;h2&gt;CPi2000 应用程序版本:%1&lt;/h2&gt;</translation>
    </message>
    <message>
        <location filename="../sources/mainWidget.cpp" line="1131"/>
        <source>&lt;p&gt;CPi2000 Device Firmware Version: %1&lt;/p&gt;</source>
        <translation>&lt;p&gt;CPi2000 固件版本:%1&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../sources/mainWidget.cpp" line="1132"/>
        <source>&lt;p&gt;CPi2000 A5 Firmware Version: %1&lt;/p&gt;</source>
        <translation>&lt;p&gt;CPi2000 A5固件版本:%1&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../sources/mainWidget.cpp" line="1133"/>
        <source>&lt;p&gt;CPi2000 A5 Software Version: %1&lt;/p&gt;</source>
        <translation>&lt;p&gt;CPi2000 A5软件版本:%1&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../sources/mainWidget.cpp" line="1134"/>
        <source>&lt;p&gt;CPi2000 DSP Firmware Version: %1&lt;/p&gt;</source>
        <translation>&lt;p&gt;CPi2000 DSP固件版本:%1&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../sources/mainWidget.cpp" line="1135"/>
        <source>&lt;p&gt;CPi2000 Front Panel Firmware Version: %1&lt;/p&gt;</source>
        <translation>&lt;p&gt;CPi2000前面板固件版本:%1&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../sources/mainWidget.cpp" line="1516"/>
        <source>Current CPi2000 device firmware version is %1, and it will be downgraded to %2. Please backup your device settings before downgrading! </source>
        <translation>当前的CPi2000固件版本为%1,该固件将降级至%2。请在降级前保存好设备设置！</translation>
    </message>
    <message>
        <location filename="../sources/mainWidget.cpp" line="1521"/>
        <source>Information</source>
        <translation>信息</translation>
    </message>
    <message>
        <location filename="../sources/mainWidget.cpp" line="1521"/>
        <source>Current CPi2000 device firmware matches this GUI application well, and no upgrading is needed!</source>
        <translation>当前CPi2000的固件版本和GUI应用程序完全匹配，无需升级！</translation>
    </message>
    <message>
        <source>&lt;p&gt;Copyright &amp;copy; 2017 Harman Inc.This is the GUI application that used to set the parameters for the device of CPi2000.&lt;/p&gt;</source>
        <translation type="vanished">&lt;p&gt;Copyright &amp;copy; 2017 Harman Inc.&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../sources/mainWidget.cpp" line="1136"/>
        <source>&lt;p&gt;Copyright &amp;copy; 2017 Harman Inc.&lt;/p&gt;</source>
        <translation>&lt;p&gt;Copyright &amp;copy; 2017 Harman Inc.&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../sources/mainWidget.cpp" line="1142"/>
        <source>About CPi2000</source>
        <translation>关于CPi2000</translation>
    </message>
    <message>
        <location filename="../sources/mainWidget.cpp" line="1142"/>
        <location filename="../sources/mainWidget.cpp" line="1381"/>
        <location filename="../sources/mainWidget.cpp" line="1667"/>
        <location filename="../sources/mainWidget.cpp" line="1699"/>
        <location filename="../sources/mainWidget.cpp" line="1782"/>
        <location filename="../sources/mainWidget.cpp" line="1880"/>
        <source>OK</source>
        <translation>确定</translation>
    </message>
    <message>
        <location filename="../sources/mainWidget.cpp" line="230"/>
        <location filename="../sources/mainWidget.cpp" line="254"/>
        <location filename="../sources/mainWidget.cpp" line="1278"/>
        <location filename="../sources/mainWidget.cpp" line="1317"/>
        <location filename="../sources/mainWidget.cpp" line="1381"/>
        <location filename="../sources/mainWidget.cpp" line="1385"/>
        <location filename="../sources/mainWidget.cpp" line="1409"/>
        <location filename="../sources/mainWidget.cpp" line="1468"/>
        <location filename="../sources/mainWidget.cpp" line="1490"/>
        <location filename="../sources/mainWidget.cpp" line="1855"/>
        <source>Warning</source>
        <translation>警告</translation>
    </message>
    <message>
        <location filename="../sources/mainWidget.cpp" line="1278"/>
        <location filename="../sources/mainWidget.cpp" line="1317"/>
        <source>Can&apos;t find the speaker: %1!</source>
        <translation>无法找到音箱: %1!</translation>
    </message>
    <message>
        <location filename="../sources/mainWidget.cpp" line="1306"/>
        <source>System Error</source>
        <translation>系统错误</translation>
    </message>
    <message>
        <location filename="../sources/mainWidget.cpp" line="1306"/>
        <source>Can&apos;t find the file name: %1!</source>
        <translation>无法找到文件名: %1</translation>
    </message>
    <message>
        <location filename="../sources/mainWidget.cpp" line="1381"/>
        <source>The selected speaker:%1 is being used by CPi2000 now, and it can&apos;t be deleted from the device!</source>
        <translation>当前所选音箱: %1正在使用，无法从设备中删除。</translation>
    </message>
    <message>
        <location filename="../sources/mainWidget.cpp" line="1385"/>
        <source>Do you want to delete the speaker: %1 from the speaker presets?</source>
        <translation>请确认是否从音箱预置中删除音箱: %1。</translation>
    </message>
    <message>
        <location filename="../sources/mainWidget.cpp" line="1385"/>
        <location filename="../sources/mainWidget.cpp" line="1468"/>
        <source>Yes</source>
        <translation>是</translation>
    </message>
    <message>
        <location filename="../sources/mainWidget.cpp" line="1385"/>
        <location filename="../sources/mainWidget.cpp" line="1468"/>
        <source>No</source>
        <translation>否</translation>
    </message>
    <message>
        <location filename="../sources/mainWidget.cpp" line="1409"/>
        <source>Can&apos;t find the speaker: %1 in the speaker presets!</source>
        <translation>无法在音箱预置中找到音箱: %1！</translation>
    </message>
    <message>
        <location filename="../sources/mainWidget.cpp" line="682"/>
        <location filename="../sources/mainWidget.cpp" line="1409"/>
        <source>Ok</source>
        <translation>确定</translation>
    </message>
    <message>
        <location filename="../sources/mainWidget.cpp" line="1490"/>
        <location filename="../sources/mainWidget.cpp" line="1855"/>
        <source>Can&apos;t connect to CPi2000 device, please check the network connection!</source>
        <translation>无法连接CPi2000，请检查网络连接！</translation>
    </message>
    <message>
        <location filename="../sources/mainWidget.cpp" line="1468"/>
        <source>Do you want to restore the factory setting? All user data will be lost if you restore the factory setting!</source>
        <translation>您想恢复出厂设置吗？如果恢复出厂设置，所有的用户数据将丢失！</translation>
    </message>
    <message>
        <location filename="../sources/mainWidget.cpp" line="1482"/>
        <source>Restore factory Setting</source>
        <translation>恢复工厂设置</translation>
    </message>
    <message>
        <location filename="../sources/mainWidget.cpp" line="1482"/>
        <source>The factory setting is restored, please re-connect the CPi2000 with new IP Address</source>
        <translation>已恢复至出厂设置，请重新连接CPi2000</translation>
    </message>
    <message>
        <location filename="../sources/mainWidget.cpp" line="1511"/>
        <source>Current CPi2000 device firmware version is %1, and it will be upgraded to %2. Please backup your device settings before upgrading! </source>
        <translation>当前的CPi2000固件版本为%1,该固件将升级至%2。请在升级前保存好设备设置！</translation>
    </message>
    <message>
        <source>&lt;h2&gt;CPi2000 Firmware Available&lt;/h2&gt;</source>
        <translation type="vanished">&lt;h2&gt;CPi2000新固件版本&lt;/h2&gt;</translation>
    </message>
    <message>
        <source>&lt;p&gt;Do you want to upgrade the CPi2000 firmware?&lt;/p&gt;</source>
        <translation type="vanished">&lt;p&gt;您想升级CPi2000设备固件吗?&lt;/p&gt;</translation>
    </message>
    <message>
        <source>Upgrade CPi2000</source>
        <translation type="vanished">升级CPi2000</translation>
    </message>
    <message>
        <location filename="../sources/mainWidget.cpp" line="230"/>
        <location filename="../sources/mainWidget.cpp" line="254"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../sources/mainWidget.cpp" line="681"/>
        <source>There is new version of GUI application for this device&apos;s firmware. Please upgrade the GUI application ASAP.</source>
        <translation>已发布新版本的GUI应用程序，它可以更好地匹配该CPi2000设备。请尽快升级GUI应用程序。</translation>
    </message>
    <message>
        <source>&lt;h2&gt;CPi2000 Firmware is up to date&lt;/h2&gt;</source>
        <translation type="vanished">&lt;h2&gt;CPi2000固件版本已经是最新版本&lt;/h2&gt;</translation>
    </message>
    <message>
        <source>&lt;p&gt;The firmware is up to date, and no firmware is available to upgrade this device! &lt;/p&gt;</source>
        <translation type="vanished">&lt;p&gt;该固件版本已经是最新版本! &lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../sources/mainWidget.cpp" line="1575"/>
        <source>Set IP Address</source>
        <translation>设置IP地址</translation>
    </message>
    <message>
        <location filename="../sources/mainWidget.cpp" line="1575"/>
        <source>IP Address has been set, please re-connect the CPi2000 with new IP Address</source>
        <translation>IP地址已被设置，请使用新的IP地址重新连接CPi2000。</translation>
    </message>
    <message>
        <location filename="../sources/mainWidget.cpp" line="1608"/>
        <location filename="../sources/mainWidget.cpp" line="1648"/>
        <source>Getting USB File Name failed!</source>
        <translation>获取USB文件名失败！</translation>
    </message>
    <message>
        <location filename="../sources/mainWidget.cpp" line="1614"/>
        <source>Getting USB New File Name failed!</source>
        <translation>获取USB新文件名失败！</translation>
    </message>
    <message>
        <location filename="../sources/mainWidget.cpp" line="1630"/>
        <source>File is saved successfully</source>
        <translation>保存文件成功</translation>
    </message>
    <message>
        <location filename="../sources/mainWidget.cpp" line="1630"/>
        <source>Current setting is saved in USB as:%1</source>
        <translation>当前设置已保存为USB文件:%1</translation>
    </message>
    <message>
        <location filename="../sources/mainWidget.cpp" line="1634"/>
        <source>Save file failed: No CPi2000 is connected!</source>
        <translation>保存文件失败:没有连接CPi2000设备！</translation>
    </message>
    <message>
        <location filename="../sources/mainWidget.cpp" line="1667"/>
        <source>USB File is loaded successfully</source>
        <translation>加载USB文件成功</translation>
    </message>
    <message>
        <location filename="../sources/mainWidget.cpp" line="1667"/>
        <location filename="../sources/mainWidget.cpp" line="1782"/>
        <source>The device will reboot to reload the device setting. Please re-connect the CPi2000 after the device&apos;s rebooting</source>
        <translation>需要重启设备以使设置生效。请在设备重启后重新连接CPi2000</translation>
    </message>
    <message>
        <location filename="../sources/mainWidget.cpp" line="1687"/>
        <location filename="../sources/mainWidget.cpp" line="1750"/>
        <source>Device File(*.dev);; All files (*.*)</source>
        <translation>设备文件(*.dev);; 所有文件 (*.*)</translation>
    </message>
    <message>
        <location filename="../sources/mainWidget.cpp" line="1697"/>
        <source>Save File Error</source>
        <translation>保存文件错误</translation>
    </message>
    <message>
        <location filename="../sources/mainWidget.cpp" line="1698"/>
        <source>Cannot save the device settings to file: %1.</source>
        <translation>无法将设备配置保存到文件:%1</translation>
    </message>
    <message>
        <source>Can&apos;t save the device file: No CPi2000 device is connected!</source>
        <translation type="vanished">保存文件失败:没有连接CPi2000设备！</translation>
    </message>
    <message>
        <source>Can&apos;t load the device file to CPi2000: No CPi2000 device is connected!</source>
        <translation type="vanished">加载文件失败:没有连接CPi2000设备！</translation>
    </message>
    <message>
        <location filename="../sources/mainWidget.cpp" line="1775"/>
        <source>Loading Device File Failed</source>
        <translation>加载设备文件失败</translation>
    </message>
    <message>
        <location filename="../sources/mainWidget.cpp" line="1775"/>
        <source>Can&apos;t load CPi2000 device file!</source>
        <translation>无法加载CPi2000设备文件！</translation>
    </message>
    <message>
        <location filename="../sources/mainWidget.cpp" line="1782"/>
        <source>Device File is loaded successfully</source>
        <translation>加载设备文件成功</translation>
    </message>
    <message>
        <location filename="../sources/mainWidget.cpp" line="1871"/>
        <source>Speaker File(*.db);; All files (*.*)</source>
        <translation>音箱文件(*.db);; 所有文件(*.*)</translation>
    </message>
    <message>
        <location filename="../sources/mainWidget.cpp" line="1880"/>
        <source>Invalid Speakers File</source>
        <translation>无效音箱文件</translation>
    </message>
    <message>
        <location filename="../sources/mainWidget.cpp" line="1880"/>
        <source>Failed to import speakers. We will finish it later.</source>
        <translation>无法导入音箱。我们将尽快完成该功能。</translation>
    </message>
</context>
<context>
    <name>OptionDlg</name>
    <message>
        <location filename="../sources/ZoomDlg.cpp" line="135"/>
        <source>Ok</source>
        <translation>确定</translation>
    </message>
    <message>
        <location filename="../sources/ZoomDlg.cpp" line="137"/>
        <source>Enable Debug</source>
        <translation>允许调试</translation>
    </message>
    <message>
        <location filename="../sources/ZoomDlg.cpp" line="151"/>
        <source>Debug Option</source>
        <translation>调试选项</translation>
    </message>
</context>
<context>
    <name>OutputSettingWidget</name>
    <message>
        <location filename="../sources/OutputSettingWidget.cpp" line="79"/>
        <source>Room Level</source>
        <translation>房间声场校准</translation>
    </message>
    <message>
        <location filename="../sources/OutputSettingWidget.cpp" line="80"/>
        <source>EQ Tuning</source>
        <translation>均衡器设置</translation>
    </message>
</context>
<context>
    <name>OverviewWidget</name>
    <message>
        <location filename="../sources/OverviewWidget.cpp" line="56"/>
        <location filename="../sources/OverviewWidget.cpp" line="765"/>
        <source>Theater:</source>
        <translation>影院:</translation>
    </message>
    <message>
        <location filename="../sources/OverviewWidget.cpp" line="60"/>
        <location filename="../sources/OverviewWidget.cpp" line="766"/>
        <source>Hall:</source>
        <translation>影厅:</translation>
    </message>
    <message>
        <location filename="../sources/OverviewWidget.cpp" line="64"/>
        <location filename="../sources/OverviewWidget.cpp" line="782"/>
        <source>&lt;p&gt;Reminder: The Hall name can only contain the captial (A-Z), digital Number (0 - 9), &apos;-&apos; and &apos;_&apos;.&lt;/p&gt;</source>
        <translation>&lt;p&gt;提示: 影厅的名字只能包含大写字母(A-Z),数字(0-9),&apos;-&apos;以及&apos;_&apos;.&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../sources/OverviewWidget.cpp" line="69"/>
        <location filename="../sources/OverviewWidget.cpp" line="767"/>
        <source>DHCP:</source>
        <translation>DHCP:</translation>
    </message>
    <message>
        <location filename="../sources/OverviewWidget.cpp" line="73"/>
        <location filename="../sources/OverviewWidget.cpp" line="768"/>
        <source>IP Address:</source>
        <translation>IP地址:</translation>
    </message>
    <message>
        <location filename="../sources/OverviewWidget.cpp" line="89"/>
        <location filename="../sources/OverviewWidget.cpp" line="769"/>
        <source>Subnet Mask:</source>
        <translation>子网掩码:</translation>
    </message>
    <message>
        <location filename="../sources/OverviewWidget.cpp" line="106"/>
        <location filename="../sources/OverviewWidget.cpp" line="770"/>
        <source>Default Gateway:</source>
        <translation>默认网关:</translation>
    </message>
    <message>
        <location filename="../sources/OverviewWidget.cpp" line="122"/>
        <location filename="../sources/OverviewWidget.cpp" line="771"/>
        <source>Server:</source>
        <translation>服务器:</translation>
    </message>
    <message>
        <location filename="../sources/OverviewWidget.cpp" line="126"/>
        <location filename="../sources/OverviewWidget.cpp" line="772"/>
        <source>Projector:</source>
        <translation>放映机:</translation>
    </message>
    <message>
        <location filename="../sources/OverviewWidget.cpp" line="130"/>
        <location filename="../sources/OverviewWidget.cpp" line="773"/>
        <source>CPi2000 S/N:</source>
        <translation>CPi2000序列号:</translation>
    </message>
    <message>
        <location filename="../sources/OverviewWidget.cpp" line="134"/>
        <source>Not Available</source>
        <translation>无法获取</translation>
    </message>
    <message>
        <location filename="../sources/OverviewWidget.cpp" line="139"/>
        <source>CXM2000 S/N:</source>
        <translation>CXM2000序列号:</translation>
    </message>
    <message>
        <location filename="../sources/OverviewWidget.cpp" line="143"/>
        <source>Unknown (Not Available)</source>
        <translation>未知</translation>
    </message>
    <message>
        <location filename="../sources/OverviewWidget.cpp" line="147"/>
        <location filename="../sources/OverviewWidget.cpp" line="774"/>
        <source>Speaker &amp; Amplifier:</source>
        <translation>音箱与功放</translation>
    </message>
    <message>
        <location filename="../sources/OverviewWidget.cpp" line="206"/>
        <location filename="../sources/OverviewWidget.cpp" line="585"/>
        <location filename="../sources/OverviewWidget.cpp" line="775"/>
        <source>Apply</source>
        <translation>应用</translation>
    </message>
    <message>
        <location filename="../sources/OverviewWidget.cpp" line="369"/>
        <source>Disabled</source>
        <translation>已禁用</translation>
    </message>
    <message>
        <location filename="../sources/OverviewWidget.cpp" line="369"/>
        <source>Enabled</source>
        <translation>已启用</translation>
    </message>
    <message>
        <location filename="../sources/OverviewWidget.cpp" line="491"/>
        <source>Invalid Theater Name</source>
        <translation>无效影院名</translation>
    </message>
    <message>
        <location filename="../sources/OverviewWidget.cpp" line="498"/>
        <source>Invalid Hall Name</source>
        <translation>无效影厅名</translation>
    </message>
    <message>
        <location filename="../sources/OverviewWidget.cpp" line="502"/>
        <source>Applying...</source>
        <translation>正在应用</translation>
    </message>
</context>
<context>
    <name>PasswordDlg</name>
    <message>
        <location filename="../sources/SpeakerEditor.cpp" line="19"/>
        <source>Enable Password Protection</source>
        <translation>开启密码保护</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerEditor.cpp" line="22"/>
        <source>Enter Password:</source>
        <translation>输入密码:</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerEditor.cpp" line="25"/>
        <source>Confirm Password:</source>
        <translation>确认密码:</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerEditor.cpp" line="34"/>
        <source>The password must contains at least 6 characters.
The password can only contain the following characters:
the captial letter(A-Z), the lowercase letter(a-z), 
digital number (0 - 9), &apos;-&apos; and &apos;_&apos;.</source>
        <translation>密码至少6位。
密码只能由下列数字或字母组成:
大写字母(A-Z)，小写字母(a-z)， 
数字(0-9)，&apos;-&apos;以及&apos;_&apos;.</translation>
    </message>
    <message>
        <source>The password must contains at least 6 characters.
The password can only contain the following characters:
the captial letter(A-Z), the lowercase letter(a-z), 
digital number (0-9), &apos;-&apos; and &apos;_&apos;.</source>
        <translation type="vanished">密码至少6位。
密码只能由下列数字或字母组成:
大写字母(A-Z)，小写字母(a-z)，
数字(0-9)，&apos;-&apos; 以及 &apos;_&apos;。</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerEditor.cpp" line="36"/>
        <source>OK</source>
        <translation>确定</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerEditor.cpp" line="39"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <source>CPi2000 Warning</source>
        <translation type="vanished">CPi2000 警告</translation>
    </message>
    <message>
        <location filename="../sources/main.cpp" line="53"/>
        <source>Another CPi2000 Application is already running now. You can&apos;t start more than one instance of the application!</source>
        <translation>CPi2000应用程序启动失败。您已启动了一个CPi2000应用程序!</translation>
    </message>
    <message>
        <location filename="../sources/main.cpp" line="53"/>
        <source>Warning</source>
        <translation>警告</translation>
    </message>
    <message>
        <location filename="../sources/main.cpp" line="54"/>
        <source>OK</source>
        <translation>确定</translation>
    </message>
</context>
<context>
    <name>RoomLevelWidget</name>
    <message>
        <source>Hall 1</source>
        <translation type="vanished">影厅1</translation>
    </message>
    <message>
        <location filename="../sources/RoomLevelWidget.cpp" line="43"/>
        <location filename="../sources/RoomLevelWidget.cpp" line="681"/>
        <source>Signal Mode</source>
        <translation>信号模式</translation>
    </message>
    <message>
        <location filename="../sources/RoomLevelWidget.cpp" line="39"/>
        <location filename="../sources/RoomLevelWidget.cpp" line="682"/>
        <source>To stop RTA setting, you need to set signal mode to &apos;OFF&apos;.</source>
        <translation>请将信号模式设置为“OFF”以结束RTA测试！</translation>
    </message>
    <message>
        <location filename="../sources/RoomLevelWidget.cpp" line="48"/>
        <location filename="../sources/RoomLevelWidget.cpp" line="62"/>
        <location filename="../sources/RoomLevelWidget.cpp" line="684"/>
        <source>Internal SPL Meter Calibration</source>
        <translation>内部声压计校正</translation>
    </message>
    <message>
        <source>Internal meter uncalibrated</source>
        <translation type="vanished">未校正</translation>
    </message>
    <message>
        <location filename="../sources/RoomLevelWidget.cpp" line="65"/>
        <location filename="../sources/RoomLevelWidget.cpp" line="685"/>
        <source>Measured Value</source>
        <translation>测得值</translation>
    </message>
    <message>
        <location filename="../sources/RoomLevelWidget.cpp" line="82"/>
        <location filename="../sources/RoomLevelWidget.cpp" line="94"/>
        <source>0.0 dB</source>
        <translation>0.0 dB</translation>
    </message>
    <message>
        <location filename="../sources/RoomLevelWidget.cpp" line="86"/>
        <location filename="../sources/RoomLevelWidget.cpp" line="686"/>
        <source>Target Mic Level</source>
        <translation>目标麦克风音量</translation>
    </message>
    <message>
        <source>Internal SPL Meter</source>
        <translation type="vanished">内部声压计</translation>
    </message>
    <message>
        <location filename="../sources/RoomLevelWidget.cpp" line="97"/>
        <location filename="../sources/RoomLevelWidget.cpp" line="485"/>
        <location filename="../sources/RoomLevelWidget.cpp" line="687"/>
        <source>uncal</source>
        <translation>未校正</translation>
    </message>
    <message>
        <location filename="../sources/RoomLevelWidget.cpp" line="105"/>
        <location filename="../sources/RoomLevelWidget.cpp" line="688"/>
        <source>Output Channel Level</source>
        <translation>输出通道音量</translation>
    </message>
    <message>
        <location filename="../sources/RoomLevelWidget.cpp" line="110"/>
        <location filename="../sources/RoomLevelWidget.cpp" line="691"/>
        <source>Left</source>
        <translation>左</translation>
    </message>
    <message>
        <location filename="../sources/RoomLevelWidget.cpp" line="110"/>
        <location filename="../sources/RoomLevelWidget.cpp" line="691"/>
        <source>Right</source>
        <translation>右</translation>
    </message>
    <message>
        <location filename="../sources/RoomLevelWidget.cpp" line="110"/>
        <location filename="../sources/RoomLevelWidget.cpp" line="691"/>
        <source>Center</source>
        <translation>中置</translation>
    </message>
    <message>
        <location filename="../sources/RoomLevelWidget.cpp" line="110"/>
        <location filename="../sources/RoomLevelWidget.cpp" line="691"/>
        <source>Subwoofer</source>
        <translation>超低音</translation>
    </message>
    <message>
        <location filename="../sources/RoomLevelWidget.cpp" line="110"/>
        <location filename="../sources/RoomLevelWidget.cpp" line="691"/>
        <source>Left Surround</source>
        <translation>左环绕</translation>
    </message>
    <message>
        <location filename="../sources/RoomLevelWidget.cpp" line="110"/>
        <location filename="../sources/RoomLevelWidget.cpp" line="691"/>
        <source>Right Surround</source>
        <translation>右环绕</translation>
    </message>
    <message>
        <location filename="../sources/RoomLevelWidget.cpp" line="110"/>
        <location filename="../sources/RoomLevelWidget.cpp" line="691"/>
        <source>Back Left Surround</source>
        <translation>左后环绕</translation>
    </message>
    <message>
        <location filename="../sources/RoomLevelWidget.cpp" line="110"/>
        <location filename="../sources/RoomLevelWidget.cpp" line="691"/>
        <source>Back Right Surround</source>
        <translation>右后环绕</translation>
    </message>
    <message>
        <location filename="../sources/RoomLevelWidget.cpp" line="166"/>
        <location filename="../sources/RoomLevelWidget.cpp" line="696"/>
        <source>Speaker</source>
        <translation>音箱</translation>
    </message>
    <message>
        <location filename="../sources/RoomLevelWidget.cpp" line="170"/>
        <location filename="../sources/RoomLevelWidget.cpp" line="697"/>
        <source>Amp</source>
        <translation>功放</translation>
    </message>
    <message>
        <location filename="../sources/RoomLevelWidget.cpp" line="174"/>
        <location filename="../sources/RoomLevelWidget.cpp" line="698"/>
        <source>Rotate</source>
        <translation>自动循环</translation>
    </message>
    <message>
        <source>Internal meter calibrated</source>
        <translation type="vanished">内部声压计已校正</translation>
    </message>
    <message>
        <location filename="../sources/RoomLevelWidget.cpp" line="495"/>
        <source>(target 85 dBC)</source>
        <translation>(目标 85 dBC)</translation>
    </message>
    <message>
        <location filename="../sources/RoomLevelWidget.cpp" line="499"/>
        <source>(target 82 dBC)</source>
        <translation>(目标 82 dBC)</translation>
    </message>
</context>
<context>
    <name>SpeakerAndAmplifierWidget</name>
    <message>
        <location filename="../sources/SpeakerAndAmplifierWidget.cpp" line="85"/>
        <location filename="../sources/SpeakerAndAmplifierWidget.cpp" line="400"/>
        <source>Speaker:</source>
        <translation>音箱:</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerAndAmplifierWidget.cpp" line="89"/>
        <location filename="../sources/SpeakerAndAmplifierWidget.cpp" line="401"/>
        <source>Amplifier:</source>
        <translation>功放:</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerAndAmplifierWidget.cpp" line="149"/>
        <source>Full-range</source>
        <translation>全频</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerAndAmplifierWidget.cpp" line="149"/>
        <source>Two-way</source>
        <translation>二分频</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerAndAmplifierWidget.cpp" line="149"/>
        <source>Three-way</source>
        <translation>三分频</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerAndAmplifierWidget.cpp" line="160"/>
        <source>Warning: The type of the Left Speaker is %1,
 and the right is %2;
They are not the same type!</source>
        <translation>警告:左音箱类型是%1;右音箱类型是%2;
两者的类型不一致!</translation>
    </message>
    <message>
        <source>The type of the Left Speaker is %1,
 and the right is %2;
They are not the same type!</source>
        <translation type="vanished">左音箱类型是%1, 右音箱类型是%2;
两者的类型不一致!</translation>
    </message>
    <message>
        <source>The type of the Left Speaker is %1, and the right is %2;
They are not the same type!</source>
        <translation type="vanished">左音箱类型是%1;右音箱类型是%2;
两者的类型不一致!</translation>
    </message>
    <message>
        <source>Passive</source>
        <translation type="vanished">全频</translation>
    </message>
    <message>
        <source>Bi-Amp</source>
        <translation type="vanished">二分频</translation>
    </message>
    <message>
        <source>Tri-Amp</source>
        <translation type="vanished">三分频</translation>
    </message>
    <message>
        <source>The type of the Left Speaker is %1; The type of the right Speaker is %2;
They are not the same type!</source>
        <translation type="vanished">左音箱类型是%1;右音箱类型是%2;
两者的类型不一致!</translation>
    </message>
</context>
<context>
    <name>SpeakerEditor</name>
    <message>
        <location filename="../sources/SpeakerEditor.cpp" line="224"/>
        <source>Speaker Name:</source>
        <translation>音箱名:</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerEditor.cpp" line="227"/>
        <source>Speaker Type:</source>
        <translation>音箱类型:</translation>
    </message>
    <message>
        <source>Passive</source>
        <translation type="vanished">全频</translation>
    </message>
    <message>
        <source>Bi-Amp</source>
        <translation type="vanished">二分频</translation>
    </message>
    <message>
        <source>Tri-Amp</source>
        <translation type="vanished">三分频</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerEditor.cpp" line="235"/>
        <source>Full-range</source>
        <translation>全频</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerEditor.cpp" line="236"/>
        <source>Two-way</source>
        <translation>二分频</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerEditor.cpp" line="237"/>
        <source>Three-way</source>
        <translation>三分频</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerEditor.cpp" line="238"/>
        <source>Surround</source>
        <translation>环绕</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerEditor.cpp" line="239"/>
        <source>Subwoofer</source>
        <translation>超低音</translation>
    </message>
    <message>
        <source>Readable</source>
        <translation type="vanished">可读</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerEditor.cpp" line="244"/>
        <source>Generator:</source>
        <translation>信号发生器:</translation>
    </message>
    <message>
        <source>Apply Channel:</source>
        <translation type="vanished">应用于通道:</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerEditor.cpp" line="247"/>
        <source>Assign to:</source>
        <translation>分配到:</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerEditor.cpp" line="289"/>
        <source>Writable</source>
        <translation>可写</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerEditor.cpp" line="294"/>
        <location filename="../sources/SpeakerEditor.cpp" line="1179"/>
        <source>Password Enabled</source>
        <translation>已设置密码</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerEditor.cpp" line="302"/>
        <location filename="../sources/SpeakerEditor.cpp" line="971"/>
        <location filename="../sources/SpeakerEditor.cpp" line="981"/>
        <source>LF</source>
        <translation>LF</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerEditor.cpp" line="307"/>
        <location filename="../sources/SpeakerEditor.cpp" line="971"/>
        <source>MF</source>
        <translation>MF</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerEditor.cpp" line="312"/>
        <location filename="../sources/SpeakerEditor.cpp" line="971"/>
        <location filename="../sources/SpeakerEditor.cpp" line="981"/>
        <source>HF</source>
        <translation>HF</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerEditor.cpp" line="331"/>
        <source>Load from file</source>
        <translation>从文件加载</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerEditor.cpp" line="334"/>
        <source>Save to file</source>
        <translation>保存到文件</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerEditor.cpp" line="337"/>
        <source>Apply</source>
        <translation>应用</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerEditor.cpp" line="341"/>
        <source>Exit</source>
        <translation>退出</translation>
    </message>
    <message>
        <source>EQ Enabled</source>
        <translation type="vanished">已启用均衡器</translation>
    </message>
    <message>
        <source>EQ Index:</source>
        <translation type="vanished">均衡器选择:</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerEditor.cpp" line="203"/>
        <source>Speaker Editor</source>
        <translation>音箱编辑器</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerEditor.cpp" line="348"/>
        <location filename="../sources/SpeakerEditor.cpp" line="1043"/>
        <location filename="../sources/SpeakerEditor.cpp" line="2158"/>
        <source>PEQ Enabled</source>
        <translation>已启用</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerEditor.cpp" line="353"/>
        <source>Parametric EQ:</source>
        <translation>参数均衡器</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerEditor.cpp" line="357"/>
        <source>Crossover EQ:</source>
        <translation>分频器:</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerEditor.cpp" line="392"/>
        <source>Band Pass Filter Equalizer</source>
        <translation>带通滤波器均衡器</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerEditor.cpp" line="395"/>
        <location filename="../sources/SpeakerEditor.cpp" line="480"/>
        <source>Type:</source>
        <translation>类型:</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerEditor.cpp" line="399"/>
        <source>IIR</source>
        <translation>IIR</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerEditor.cpp" line="400"/>
        <source>FIR</source>
        <translation>FIR</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerEditor.cpp" line="407"/>
        <location filename="../sources/SpeakerEditor.cpp" line="483"/>
        <source>Slope:</source>
        <translation>斜率:</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerEditor.cpp" line="410"/>
        <source>Tab:</source>
        <translation>阶数:</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerEditor.cpp" line="413"/>
        <location filename="../sources/SpeakerEditor.cpp" line="486"/>
        <source>Frequency:</source>
        <translation>频率:</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerEditor.cpp" line="416"/>
        <source>Frequency</source>
        <translation>频率</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerEditor.cpp" line="427"/>
        <location filename="../sources/SpeakerEditor.cpp" line="492"/>
        <location filename="../sources/SpeakerEditor.cpp" line="1116"/>
        <location filename="../sources/SpeakerEditor.cpp" line="1671"/>
        <location filename="../sources/SpeakerEditor.cpp" line="1756"/>
        <location filename="../sources/SpeakerEditor.cpp" line="1906"/>
        <source>Enabled</source>
        <translation>已启用</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerEditor.cpp" line="455"/>
        <source>FIR Equalizer</source>
        <translation>FIR滤波器</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerEditor.cpp" line="458"/>
        <source>384 tabs FIR:</source>
        <translation>384阶FIR</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerEditor.cpp" line="461"/>
        <source>Enable FIR</source>
        <translation>启用FIR</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerEditor.cpp" line="465"/>
        <source>Set to Default</source>
        <translation>恢复默认值</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerEditor.cpp" line="468"/>
        <source>Paste from Clipboard</source>
        <translation>从剪切板粘贴</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerEditor.cpp" line="477"/>
        <source>PEQ Equalizer</source>
        <translation>参数均衡器</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerEditor.cpp" line="489"/>
        <source>Q:</source>
        <translation>Q:</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerEditor.cpp" line="498"/>
        <source>Bell</source>
        <translation>钟形</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerEditor.cpp" line="499"/>
        <source>Low Shelf</source>
        <translation>低架</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerEditor.cpp" line="500"/>
        <source>High shelf</source>
        <translation>高架</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerEditor.cpp" line="503"/>
        <source>Delete</source>
        <translation>删除</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerEditor.cpp" line="515"/>
        <location filename="../sources/SpeakerEditor.cpp" line="561"/>
        <source>Gain:</source>
        <translation>增益:</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerEditor.cpp" line="536"/>
        <source>Gain</source>
        <translation>增益</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerEditor.cpp" line="558"/>
        <source>General setting</source>
        <translation>通用设置</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerEditor.cpp" line="564"/>
        <source>Delay:</source>
        <translation>延迟:</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerEditor.cpp" line="584"/>
        <location filename="../sources/SpeakerEditor.cpp" line="1021"/>
        <location filename="../sources/SpeakerEditor.cpp" line="1542"/>
        <source>Polarity (+)</source>
        <translation>极性 (+)</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerEditor.cpp" line="604"/>
        <location filename="../sources/SpeakerEditor.cpp" line="1103"/>
        <source> Enable </source>
        <translation>已关闭</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerEditor.cpp" line="1025"/>
        <location filename="../sources/SpeakerEditor.cpp" line="1546"/>
        <source>Polarity (-)</source>
        <translation>极性 (-)</translation>
    </message>
    <message>
        <source>Polarity:</source>
        <translation type="vanished">极性:</translation>
    </message>
    <message>
        <source>Normal</source>
        <translation type="vanished">正常</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerEditor.cpp" line="589"/>
        <source>Output limiter</source>
        <translation>输出限幅器</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerEditor.cpp" line="592"/>
        <source>Threshold:</source>
        <translation>门限值:</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerEditor.cpp" line="595"/>
        <source>Attack time:</source>
        <translation>启动时间:</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerEditor.cpp" line="598"/>
        <source>Release:</source>
        <translation>释放时间:</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerEditor.cpp" line="601"/>
        <source>Hold:</source>
        <translation>保持时间:</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerEditor.cpp" line="1490"/>
        <location filename="../sources/SpeakerEditor.cpp" line="1499"/>
        <source>Invalid FIR Data, please check the data in the clipboard!</source>
        <translation>无效FIR数据，请检查剪贴板中的内容！</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerEditor.cpp" line="1675"/>
        <location filename="../sources/SpeakerEditor.cpp" line="1760"/>
        <location filename="../sources/SpeakerEditor.cpp" line="1910"/>
        <source>Enable</source>
        <translation>已旁通</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerEditor.cpp" line="608"/>
        <source>Auto</source>
        <translation>自动</translation>
    </message>
    <message>
        <source>Inverted</source>
        <translation type="vanished">反转</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerEditor.cpp" line="1047"/>
        <location filename="../sources/SpeakerEditor.cpp" line="2162"/>
        <source>Enable All</source>
        <translation>已旁通</translation>
    </message>
    <message>
        <source>Disabled</source>
        <translation type="vanished">已禁用</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerEditor.cpp" line="1175"/>
        <source>Password Disabled</source>
        <translation>未设置密码</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerEditor.cpp" line="1194"/>
        <location filename="../sources/SpeakerEditor.cpp" line="1241"/>
        <source>Speaker File(*.spk);; All files (*.*)</source>
        <translation>音箱文件(*.spk);;所有文件(*.*)</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerEditor.cpp" line="1205"/>
        <location filename="../sources/SpeakerEditor.cpp" line="1252"/>
        <source>Save File Error</source>
        <translation>保存文件错误</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerEditor.cpp" line="1206"/>
        <location filename="../sources/SpeakerEditor.cpp" line="1253"/>
        <source>Cannot save the speaker settings to file: %1.</source>
        <translation>无法将音箱设置保存到文件:%1中。</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerEditor.cpp" line="1207"/>
        <location filename="../sources/SpeakerEditor.cpp" line="1254"/>
        <source>OK</source>
        <translation>确定</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerEditor.cpp" line="1157"/>
        <location filename="../sources/SpeakerEditor.cpp" line="1219"/>
        <location filename="../sources/SpeakerEditor.cpp" line="1490"/>
        <location filename="../sources/SpeakerEditor.cpp" line="1499"/>
        <location filename="../sources/SpeakerEditor.cpp" line="1513"/>
        <location filename="../sources/SpeakerEditor.cpp" line="1523"/>
        <source>Warning</source>
        <translation>警告</translation>
    </message>
    <message>
        <source>Signal Mode:</source>
        <translation type="vanished">信号模式</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerEditor.cpp" line="1157"/>
        <source>The speaker you are editing is set to unwritable! If it takes effect, you won&apos;t modify the settings of this speaker any more!</source>
        <translation>本音箱将被设置为写保护！一旦该设置生效，该音箱的所有参数都会被固定而且无法修改！</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerEditor.cpp" line="1224"/>
        <source>Loading failed</source>
        <translation>加载失败</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerEditor.cpp" line="1224"/>
        <source>The speaker type in the loaded file is %1, and you can&apos;t apply it at current output channel!</source>
        <translation>您所加载的音箱类型为%1，无法应用于当前输出通道。</translation>
    </message>
    <message>
        <source>Invalid FIR Data, please check the data at index %1: %2!</source>
        <translation type="vanished">无效FIR数据，请检查索引号为:%1的数据:%2！</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerEditor.cpp" line="1513"/>
        <source>Invalid FIR Data, please check it again!</source>
        <translation>无效FIR数据，请重新检查数据！</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerEditor.cpp" line="1523"/>
        <source>Invalid FIR Data: CPi2000 can only support those FIR which tab is not bigger than 384!</source>
        <translation>无效FIR数据:CPi2000仅支持阶数不超过384的FIR！</translation>
    </message>
</context>
<context>
    <name>SpeakerImportDialog</name>
    <message>
        <location filename="../sources/SpeakerImportDialog.cpp" line="10"/>
        <source>Speaker List:</source>
        <translation>音箱列表:</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerImportDialog.cpp" line="16"/>
        <source>New Speaker</source>
        <translation>新音箱</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerImportDialog.cpp" line="20"/>
        <source>Apply channel:</source>
        <translation>应用通道:</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerImportDialog.cpp" line="21"/>
        <source>Left</source>
        <translation>左</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerImportDialog.cpp" line="22"/>
        <source>Right</source>
        <translation>右</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerImportDialog.cpp" line="23"/>
        <source>Center</source>
        <translation>中置</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerImportDialog.cpp" line="24"/>
        <source>Left Surround</source>
        <translation>左环绕</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerImportDialog.cpp" line="25"/>
        <source>Right Surround</source>
        <translation>右环绕</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerImportDialog.cpp" line="26"/>
        <source>Back Left Surround</source>
        <translation>左后环绕</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerImportDialog.cpp" line="27"/>
        <source>Back Right Surround</source>
        <translation>右后环绕</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerImportDialog.cpp" line="28"/>
        <location filename="../sources/SpeakerImportDialog.cpp" line="70"/>
        <source>Subwoofer</source>
        <translation>超低音</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerImportDialog.cpp" line="48"/>
        <source>Delete</source>
        <translation>删除</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerImportDialog.cpp" line="51"/>
        <source>Modify</source>
        <translation>修改</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerImportDialog.cpp" line="54"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerImportDialog.cpp" line="57"/>
        <source>Speaker name:</source>
        <translation>音箱名称:</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerImportDialog.cpp" line="63"/>
        <source>Speaker type</source>
        <translation>音箱类型</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerImportDialog.cpp" line="66"/>
        <source>Passive</source>
        <translation>全频</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerImportDialog.cpp" line="67"/>
        <source>Bi-Amp</source>
        <translation>二分频</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerImportDialog.cpp" line="68"/>
        <source>Tri-Amp</source>
        <translation>三分频</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerImportDialog.cpp" line="69"/>
        <source>Surround</source>
        <translation>环绕</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerImportDialog.cpp" line="74"/>
        <source>Property</source>
        <translation>属性</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerImportDialog.cpp" line="75"/>
        <source>Readable</source>
        <translation>可读</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerImportDialog.cpp" line="76"/>
        <source>Writable</source>
        <translation>可写</translation>
    </message>
</context>
<context>
    <name>SpeakerListDialog</name>
    <message>
        <location filename="../sources/SpeakerListDialog.cpp" line="11"/>
        <source>CPi2000 Speaker</source>
        <translation>CPi2000音箱</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerListDialog.cpp" line="12"/>
        <source>Speaker List:</source>
        <translation>音箱列表:</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerListDialog.cpp" line="13"/>
        <source>Invalid Speaker Name</source>
        <translation>音箱名不正确</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerListDialog.cpp" line="20"/>
        <source>New Speaker</source>
        <translation>新建音箱</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerListDialog.cpp" line="24"/>
        <source>Apply channel:</source>
        <translation>应用通道:</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerListDialog.cpp" line="25"/>
        <source>Left</source>
        <translation>左</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerListDialog.cpp" line="26"/>
        <source>Right</source>
        <translation>右</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerListDialog.cpp" line="27"/>
        <source>Center</source>
        <translation>中置</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerListDialog.cpp" line="28"/>
        <source>Left Surround</source>
        <translation>左环绕</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerListDialog.cpp" line="29"/>
        <source>Right Surround</source>
        <translation>右环绕</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerListDialog.cpp" line="30"/>
        <source>Back Left Surround</source>
        <translation>左后环绕</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerListDialog.cpp" line="31"/>
        <source>Back Right Surround</source>
        <translation>右后环绕</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerListDialog.cpp" line="32"/>
        <location filename="../sources/SpeakerListDialog.cpp" line="74"/>
        <source>Subwoofer</source>
        <translation>超低音</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerListDialog.cpp" line="52"/>
        <source>Delete</source>
        <translation>删除</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerListDialog.cpp" line="55"/>
        <location filename="../sources/SpeakerListDialog.cpp" line="564"/>
        <location filename="../sources/SpeakerListDialog.cpp" line="613"/>
        <source>Modify</source>
        <translation>修改</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerListDialog.cpp" line="58"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerListDialog.cpp" line="61"/>
        <source>Speaker name:</source>
        <translation>音箱名称:</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerListDialog.cpp" line="67"/>
        <source>Speaker type</source>
        <translation>音箱类型</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerListDialog.cpp" line="70"/>
        <source>Full-range</source>
        <translation>全频</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerListDialog.cpp" line="71"/>
        <source>Two-way</source>
        <translation>二分频</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerListDialog.cpp" line="72"/>
        <source>Three-way</source>
        <translation>三分频</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerListDialog.cpp" line="88"/>
        <source>Hide Unwritable</source>
        <translation>隐藏无法修改的音箱</translation>
    </message>
    <message>
        <source>Passive</source>
        <translation type="vanished">全频</translation>
    </message>
    <message>
        <source>Bi-Amp</source>
        <translation type="vanished">二分频</translation>
    </message>
    <message>
        <source>Tri-Amp</source>
        <translation type="vanished">三分频</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerListDialog.cpp" line="73"/>
        <source>Surround</source>
        <translation>环绕</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerListDialog.cpp" line="78"/>
        <source>Property</source>
        <translation>属性</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerListDialog.cpp" line="79"/>
        <source>Readable</source>
        <translation>可读</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerListDialog.cpp" line="80"/>
        <source>Writable</source>
        <translation>可写</translation>
    </message>
    <message>
        <source>Hide Uneditable</source>
        <translation type="vanished">隐藏无法修改的音箱</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerListDialog.cpp" line="204"/>
        <source>Password protection</source>
        <translation>密码保护</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerListDialog.cpp" line="204"/>
        <source>Please Enter Password:</source>
        <translation>请输入密码:</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerListDialog.cpp" line="211"/>
        <source>Warning</source>
        <translation>警告</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerListDialog.cpp" line="211"/>
        <source>Invalid password! You can&apos;t edit this speaker&apos;s setting</source>
        <translation>无效密码！您无法编辑当前音箱的参数</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerListDialog.cpp" line="560"/>
        <location filename="../sources/SpeakerListDialog.cpp" line="609"/>
        <source>New</source>
        <translation>新建</translation>
    </message>
</context>
<context>
    <name>SpeakerTypeDialog</name>
    <message>
        <location filename="../sources/SpeakerAndAmplifierWidget.cpp" line="9"/>
        <source>Speaker Name:</source>
        <translation>音箱名:</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerAndAmplifierWidget.cpp" line="10"/>
        <source>Speaker EQ:</source>
        <translation>音箱均衡器:</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerAndAmplifierWidget.cpp" line="11"/>
        <source>EQ File:</source>
        <translation>均衡器文件:</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerAndAmplifierWidget.cpp" line="18"/>
        <location filename="../sources/SpeakerAndAmplifierWidget.cpp" line="51"/>
        <source>OK</source>
        <translation>确定</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerAndAmplifierWidget.cpp" line="20"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerAndAmplifierWidget.cpp" line="25"/>
        <source>Enable</source>
        <translation>已旁通</translation>
    </message>
    <message>
        <source>Bypassed</source>
        <translation type="vanished">已旁通:</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerAndAmplifierWidget.cpp" line="28"/>
        <source>Speaker Type</source>
        <translation>音箱类型</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerAndAmplifierWidget.cpp" line="39"/>
        <source>Speaker EQ File(*.eq);; All files (*.*)</source>
        <translation>音箱均衡器文件(*.eq);; 所有文件 (*.*)</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerAndAmplifierWidget.cpp" line="50"/>
        <source>Open File Error</source>
        <translation>打开文件错误</translation>
    </message>
    <message>
        <location filename="../sources/SpeakerAndAmplifierWidget.cpp" line="51"/>
        <source>Cannot open the speaker EQ file: %1.</source>
        <translation>无法打开音箱均衡器文件: %1.</translation>
    </message>
</context>
<context>
    <name>TimeAdjustDialog</name>
    <message>
        <location filename="../sources/UpgradeDialog.cpp" line="489"/>
        <source>Calibrate Time</source>
        <translation>校准时间</translation>
    </message>
    <message>
        <location filename="../sources/UpgradeDialog.cpp" line="490"/>
        <source>PC Host Time:</source>
        <translation>PC主机时间:</translation>
    </message>
    <message>
        <location filename="../sources/UpgradeDialog.cpp" line="494"/>
        <source>Device Time:</source>
        <translation>设备时间:</translation>
    </message>
    <message>
        <location filename="../sources/UpgradeDialog.cpp" line="506"/>
        <source>Sync Time</source>
        <translation>同步时间</translation>
    </message>
    <message>
        <location filename="../sources/UpgradeDialog.cpp" line="509"/>
        <source>Exit</source>
        <translation>退出</translation>
    </message>
    <message>
        <location filename="../sources/UpgradeDialog.cpp" line="577"/>
        <source>Warning</source>
        <translation>警告</translation>
    </message>
    <message>
        <location filename="../sources/UpgradeDialog.cpp" line="577"/>
        <source>Can&apos;t connect to CPi2000 device, please check the network connection!</source>
        <translation>无法连接CPi2000，请检查网络连接！</translation>
    </message>
</context>
<context>
    <name>USBFileDialog</name>
    <message>
        <source>USB File List</source>
        <translation type="vanished">USB文件列表</translation>
    </message>
    <message>
        <location filename="../sources/USBFileDialog.cpp" line="11"/>
        <source>File Name:</source>
        <translation>文件名:</translation>
    </message>
    <message>
        <source>File name can only contain the following characters: 
the captial (A-Z), digital Number (0-9), &apos;-&apos; and &apos;_&apos;.</source>
        <translation type="vanished">文件名只能包含下列字符组合:
大写字母 (A-Z)，数字 (0-9)，&apos;-&apos; 以及 &apos;_&apos;。</translation>
    </message>
    <message>
        <location filename="../sources/USBFileDialog.cpp" line="9"/>
        <source>USB Disk on CPi2000</source>
        <translation>CPi2000上的U盘</translation>
    </message>
    <message>
        <location filename="../sources/USBFileDialog.cpp" line="13"/>
        <source>File name can only contain the following characters: 
the captial (A-Z), digital Number (0 - 9), &apos;-&apos; and &apos;_&apos;</source>
        <translation>文件名只能包含下列字符: 
大写字母 (A-Z)，数字 (0-9)， &apos;-&apos;以及&apos;_&apos;</translation>
    </message>
    <message>
        <location filename="../sources/USBFileDialog.cpp" line="28"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../sources/USBFileDialog.cpp" line="31"/>
        <source>New File</source>
        <translation>新建文件</translation>
    </message>
    <message>
        <location filename="../sources/USBFileDialog.cpp" line="96"/>
        <source>Save as</source>
        <translation>另存为</translation>
    </message>
    <message>
        <location filename="../sources/USBFileDialog.cpp" line="97"/>
        <source>Save to USB File on CPi2000</source>
        <translation>保存到CPi2000上的U盘</translation>
    </message>
    <message>
        <location filename="../sources/USBFileDialog.cpp" line="102"/>
        <source>Load USB file on CPi2000</source>
        <translation>从CPi2000上的U盘加载</translation>
    </message>
    <message>
        <location filename="../sources/USBFileDialog.cpp" line="103"/>
        <source>Load from USB device on CPi2000</source>
        <translation>从CPi2000上的U盘加载</translation>
    </message>
    <message>
        <source>Save to USB File</source>
        <translation type="vanished">保存到USB文件</translation>
    </message>
    <message>
        <location filename="../sources/USBFileDialog.cpp" line="98"/>
        <source>Save</source>
        <translation>保存</translation>
    </message>
    <message>
        <source>Load USB file</source>
        <translation type="vanished">加载USB文件</translation>
    </message>
    <message>
        <source>Load from USB File</source>
        <translation type="vanished">从USB文件加载</translation>
    </message>
    <message>
        <location filename="../sources/USBFileDialog.cpp" line="104"/>
        <source>Load</source>
        <translation>加载</translation>
    </message>
    <message>
        <location filename="../sources/USBFileDialog.cpp" line="149"/>
        <source>Save as New</source>
        <translation>保存为新文件</translation>
    </message>
    <message>
        <location filename="../sources/USBFileDialog.cpp" line="155"/>
        <source>Replace</source>
        <translation>替换</translation>
    </message>
</context>
<context>
    <name>UpgradeDialog</name>
    <message>
        <source>Do you want to start upgrading process?</source>
        <translation type="vanished">您想开始升级吗？</translation>
    </message>
    <message>
        <location filename="../sources/UpgradeDialog.cpp" line="20"/>
        <location filename="../sources/UpgradeDialog.cpp" line="38"/>
        <source>Do you want to start the upgrading process?</source>
        <translation>您想开始升级吗？</translation>
    </message>
    <message>
        <location filename="../sources/UpgradeDialog.cpp" line="24"/>
        <source>Do you want to start the downgrading process?</source>
        <translation>您想开始降级吗？</translation>
    </message>
    <message>
        <location filename="../sources/UpgradeDialog.cpp" line="45"/>
        <source>Yes</source>
        <translation>是</translation>
    </message>
    <message>
        <location filename="../sources/UpgradeDialog.cpp" line="49"/>
        <source>No</source>
        <translation>否</translation>
    </message>
    <message>
        <location filename="../sources/UpgradeDialog.cpp" line="85"/>
        <source>Warning</source>
        <translation>警告</translation>
    </message>
    <message>
        <location filename="../sources/UpgradeDialog.cpp" line="85"/>
        <source>Can&apos;t connect to CPi2000 device, please check the network connection!</source>
        <translation>无法连接CPi2000设备，请检查网络连接！</translation>
    </message>
    <message>
        <location filename="../sources/UpgradeDialog.cpp" line="98"/>
        <source>Upgrading...</source>
        <translation>正在升级...</translation>
    </message>
    <message>
        <location filename="../sources/UpgradeDialog.cpp" line="102"/>
        <source>Downgrading...</source>
        <translation>正在降级...</translation>
    </message>
    <message>
        <location filename="../sources/UpgradeDialog.cpp" line="158"/>
        <source>Network Abnormal</source>
        <translation>网络异常</translation>
    </message>
    <message>
        <source>Loading Device File Failed</source>
        <translation type="vanished">加载设备文件失败</translation>
    </message>
    <message>
        <location filename="../sources/UpgradeDialog.cpp" line="158"/>
        <source>Failed to transfer CPi2000 device file!</source>
        <translation>无法传输CPi2000升级文件！</translation>
    </message>
    <message>
        <location filename="../sources/UpgradeDialog.cpp" line="169"/>
        <source>Firmware is transfered to CPi2000 device, please re-connect the CPi2000 after its upgrading.</source>
        <translation>固件已准备就绪。升级完成后请重新连接CPi2000设备。</translation>
    </message>
    <message>
        <location filename="../sources/UpgradeDialog.cpp" line="173"/>
        <source>Firmware is transfered to CPi2000 device, please re-connect the CPi2000 after its downgrading.</source>
        <translation>固件已准备就绪。降级完成后请重新连接CPi2000设备。</translation>
    </message>
    <message>
        <location filename="../sources/UpgradeDialog.cpp" line="178"/>
        <source>OK</source>
        <translation>确定</translation>
    </message>
</context>
<context>
    <name>ZoomDlg</name>
    <message>
        <location filename="../sources/ZoomDlg.cpp" line="10"/>
        <source>Enable Scale</source>
        <translation>允许缩放</translation>
    </message>
    <message>
        <location filename="../sources/ZoomDlg.cpp" line="14"/>
        <source>Scale Factor:</source>
        <translation>缩放系数</translation>
    </message>
    <message>
        <location filename="../sources/ZoomDlg.cpp" line="38"/>
        <source>Apply</source>
        <translation>应用</translation>
    </message>
    <message>
        <location filename="../sources/ZoomDlg.cpp" line="41"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../sources/ZoomDlg.cpp" line="121"/>
        <source>Warning</source>
        <translation>警告</translation>
    </message>
    <message>
        <location filename="../sources/ZoomDlg.cpp" line="121"/>
        <source>You are changing the display scale settings, and this may lead to abnormal display of the GUI application. If the display of GUI application is abnormal, please delete the file: %1 manually.
Do you want to continue?</source>
        <translation>你正在改变缩放设置,但这些参数可能会导致GUI应用程序显示不正常.如果显示不正常，请手动删除缩放配置文件: %1.
你想继续吗？</translation>
    </message>
    <message>
        <location filename="../sources/ZoomDlg.cpp" line="127"/>
        <source>Information</source>
        <translation>信息</translation>
    </message>
    <message>
        <location filename="../sources/ZoomDlg.cpp" line="127"/>
        <source>The scale settings will take effect atftr you restart the GUI applicaiton.</source>
        <translation>缩放设置将会在下次重启GUI应用程序时生效。</translation>
    </message>
</context>
</TS>
