#include "AdaptiveTuning.h"
#include "math.h"

const int iter=20;
AdaptiveTuning::AdaptiveTuning(void)
{
	for(int i=0;i<200;i++){
		errtst[i]=0;
	}
}


AdaptiveTuning::~AdaptiveTuning(void)
{

}

double AdaptiveTuning::dBtoLinear(double linVal){
	
	return pow(10,linVal*0.05);
}

double AdaptiveTuning::LineartodB(double dBVal){
	
	return 20*log10(dBVal);
}

double AdaptiveTuning::getRef(double *meterInput,int startIdx,int count){
	double sum=0;
	for(int i=startIdx;i<startIdx+count;i++){
		sum+=meterInput[i];
	}

	return sum/count;
}

/**************************************

meterInput: the 1/3 octave band meter value array input
targetCurve: the 1/3 ocatve target curve array input 
result: the Geq Gain result that calculated by the algorithm
mu: the step size of the adaptive filter , normally will set to 0.5~1 ,
iter: number of the iteration , normally set to 20~100
limit: limit the tuned gain result to +6dB to -6dB
**************************************/
void AdaptiveTuning::calculateAutomatedGain(double *meterInput,double *targetCurve,double *result, double mu,int iter,bool limit){
	double RefMeter;
	double normalizedMeterVal[30];
	double y_mag;
	double *y,*err,*lastMeterinput;

	y= new double[iter];
	err= new double[iter];
	lastMeterinput =new double[iter];

	RefMeter=getRef(meterInput,4,17); //50~2K Hz in 1/3 octave 

	//normalize the meterInput based on the ref meter value
	for(int i=0;i<NumOfRTABands;i++){
		normalizedMeterVal[i]=meterInput[i]-RefMeter;
		//convert to linear
		normalizedMeterVal[i]=dBtoLinear(normalizedMeterVal[i]);
		targetCurve[i]=dBtoLinear(targetCurve[i]);
	}
	

	for(int i=0;i<NumOfRTABands;i++){
		for(int n=0; n<iter; n++){
			y[n]=normalizedMeterVal[i]*result[i];
			y_mag=fabs(y[n]);
			err[n]=targetCurve[i]-y_mag;
			lastMeterinput[n]=normalizedMeterVal[i];
			result[i] = result[i] + mu*err[n];
			errtst[n]=err[n];
		}

	}
				
	//convert to dB	, and round to x.y	
	for(int i=0;i<NumOfRTABands;i++){

		result[i]=LineartodB(result[i]);

		if(limit){
			if(result[i]>=6.0) result[i]=6.0;
			if(result[i]<=-6.0) result[i]=-6.0;
		}
			
		
	}




}