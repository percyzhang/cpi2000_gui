#include "BasicSettingWidget.h"
#include <QStylePainter>
#include "mainwidget.h"
#include <QDebug>
#include "mainApplication.h"


BasicSettingWidget::BasicSettingWidget(QWidget *parent)
    : QWidget(parent)
{
	m_pMasterVolumeFrame = new QFrame(this);
 	m_pMasterVolumeFrame->setObjectName(QStringLiteral("m_pMasterVolumeFrame"));

	m_pSurroundDelayFrame = new HeadFrame(tr("   Surround Delay"), this);

	m_pLoadDetectionFrame = new HeadFrame(tr("   Fault Detection"), this);

	m_pMainOutputFrame = new HeadFrame(tr("   Main Audio Setting"), this);

	m_pMasterVolumeLabel = new QLabel(tr("Master Volume"), m_pMasterVolumeFrame);
	m_pMasterVolumeLabel->setObjectName(QStringLiteral("BlackFont14"));
	m_pMasterVolumeLabel->setAlignment(Qt::AlignCenter );

//	m_pMuteDurationLabel = new QLabel(tr("Mute Duration:"), m_pMasterVolumeFrame);
//	m_pMuteDurationLabel->setObjectName(QStringLiteral("m_pMasterVolumeLabel"));
//	m_pMuteDurationLabel->setAlignment(Qt::AlignLeft );

	m_pFadeInLabel = new QLabel(tr("Fade In:"), m_pMasterVolumeFrame);
	m_pFadeInLabel->setAlignment(Qt::AlignLeft | Qt::AlignVCenter);

	m_pFadeOutLabel = new QLabel(tr("Fade Out:"), m_pMasterVolumeFrame);
	m_pFadeOutLabel->setAlignment(Qt::AlignLeft | Qt::AlignVCenter);

	/* Child Widget of m_pMasterVolumeFrame */
	m_pMasterVolumeSpinBox = new QDoubleSpinBox(m_pMasterVolumeFrame);
	m_pMasterVolumeSpinBox->setRange(0.0, 10.0);
	m_pMasterVolumeSpinBox->setSingleStep(0.1);
	m_pMasterVolumeSpinBox->setDecimals(1);
	m_pMasterVolumeSpinBox->setSuffix("");
	connect(m_pMasterVolumeSpinBox, SIGNAL(valueChanged(double)), this, SLOT(onMasterVolumeSpinBoxChanged(double)));

	/* Sync to preset */
	m_pSyncCheckBox = new QCheckBox(tr("Sync to preset"), m_pMasterVolumeFrame);
	connect(m_pSyncCheckBox, SIGNAL(clicked()), this, SLOT(onSyncClicked()));	

	m_pFadeInSpinBox = new QDoubleSpinBox(m_pMasterVolumeFrame);
	m_pFadeInSpinBox->setRange(0.0, 10.0);
	m_pFadeInSpinBox->setSingleStep(0.1);
	m_pFadeInSpinBox->setDecimals(1);
	m_pFadeInSpinBox->setSuffix("  Sec");
	connect(m_pFadeInSpinBox, SIGNAL(valueChanged(double)), this, SLOT(onFadeInSpinBoxChanged(double)));

	m_pFadeOutSpinBox = new QDoubleSpinBox(m_pMasterVolumeFrame);
	m_pFadeOutSpinBox->setRange(0.0, 10.0);
	m_pFadeOutSpinBox->setSingleStep(0.1);
	m_pFadeOutSpinBox->setDecimals(1);
	m_pFadeOutSpinBox->setSuffix("  Sec");
	connect(m_pFadeOutSpinBox, SIGNAL(valueChanged(double)), this, SLOT(onFadeOutSpinBoxChanged(double)));

	m_pMuteButton = new MarkButton(QString(""), m_pMasterVolumeFrame);
    m_pMuteButton->setObjectName(QStringLiteral("muteButton"));
	connect(m_pMuteButton, SIGNAL(clicked()), this, SLOT(onClickMuteButton()));
	m_pMuteButton->mark(false);

	m_pMasterVolume = new VolumeSlider(m_pMasterVolumeFrame);
	m_pMasterVolume->setTextColor(QColor(0x10, 0x10, 0x10));
	m_pMasterVolume->setMarkColor(QColor(0x10, 0x10, 0x10));
	m_pMasterVolume->addExtraLongMark(70, "7.0", true, QColor(13, 91, 124), QColor(13, 91, 124), 3);
	m_pMasterVolume->addExtraShortMark(0, " 0", false);
	m_pMasterVolume->addExtraShortMark(100, " 10", false);
	QStringList sliderStringList;
	sliderStringList << " 0" << " 10";
	m_pMasterVolume->setMark(0, 10, sliderStringList);
	m_pMasterVolume->setMarkLength(15, 22);
	m_pMasterVolume->setMarkSide(true, true);
	m_pMasterVolume->setRange(0, 100);
	m_pMasterVolume->setGrooveHeight(30);
	m_pMasterVolume->setSingleStep(1);
	m_pMasterVolume->setPageStep(1);
	m_pMasterVolume->setValue(70);
	connect(m_pMasterVolume, SIGNAL(valueChanged(int)), this, SLOT(onMasterVolumeSliderChanged(int)));

	/* Child Widget of m_pSurroundDelayFrame */
	m_pTotalDistanceLabel = new QLabel(tr("Average distance from screen to rear wall of the theater"), m_pSurroundDelayFrame);
	m_pTotalDistanceLabel->setObjectName(QStringLiteral("WhiteFont14"));
	m_pAverageDistanceLabel = new QLabel(tr("Average distance between left and right surround speakers"), m_pSurroundDelayFrame);
	m_pAverageDistanceLabel->setObjectName(QStringLiteral("WhiteFont14"));
	m_pMeterOrFeetLabel1 = new QLabel(tr("Meters"), m_pSurroundDelayFrame);
	m_pMeterOrFeetLabel1->setObjectName(QStringLiteral("WhiteFont14"));
	m_pMeterOrFeetLabel2 = new QLabel(tr("Meters"), m_pSurroundDelayFrame);
	m_pMeterOrFeetLabel2->setObjectName(QStringLiteral("WhiteFont14"));

	m_pHorizonLineTop = new QFrame(m_pSurroundDelayFrame);
	m_pHorizonLineTop->setObjectName(QStringLiteral("m_pDelayLine"));
	m_pHorizonLineTop->setFrameShape(QFrame::HLine);

	m_pHorizonLineBottom = new QFrame(m_pSurroundDelayFrame);
	m_pHorizonLineBottom->setObjectName(QStringLiteral("m_pDelayLine"));
	m_pHorizonLineBottom->setFrameShape(QFrame::HLine);

	m_pVerticalLine = new QFrame(m_pSurroundDelayFrame);
	m_pVerticalLine->setObjectName(QStringLiteral("m_pDelayLine"));
	m_pVerticalLine->setFrameShape(QFrame::VLine);


	m_pCalcButton = new QPushButton("", m_pSurroundDelayFrame);
	m_pCalcButton->setObjectName(QStringLiteral("m_pCalcButton"));
	m_pCalcButton->setToolTip(tr("calculate")); 
	connect(m_pCalcButton, SIGNAL(clicked()), this, SLOT(onClickCalcButton()));	

	m_pSurroundExLabel = new QLabel(tr("Delay time:"), m_pSurroundDelayFrame);
	m_pSurroundExLabel->setObjectName(QStringLiteral("WhiteFont14"));

	m_pSurroundDelaySpinBox = new QSpinBox(m_pSurroundDelayFrame);
	m_pSurroundDelaySpinBox->setRange(0, 200);
	m_pSurroundDelaySpinBox->setSingleStep(1);
	m_pSurroundDelaySpinBox->setSuffix(" ms");
	connect(m_pSurroundDelaySpinBox, SIGNAL(valueChanged(int)), this, SLOT(onSurroundDelaySpinBoxChanged(int)));

	m_pLengthDistanceSpinBox = new QDoubleSpinBox(m_pSurroundDelayFrame);
	m_pLengthDistanceSpinBox->setRange(0.0, 100.0);
	m_pLengthDistanceSpinBox->setSingleStep(0.1);
	m_pLengthDistanceSpinBox->setDecimals(1);
	m_pLengthDistanceSpinBox->setValue(10.0);
	connect(m_pLengthDistanceSpinBox, SIGNAL(valueChanged(double)), this, SLOT(onLengthDistanceSpinBoxChanged(double)));


	m_pWidthDistanceSpinBox = new QDoubleSpinBox(m_pSurroundDelayFrame);
	m_pWidthDistanceSpinBox->setRange(0, 100.0);
	m_pWidthDistanceSpinBox->setSingleStep(0.1);
	m_pWidthDistanceSpinBox->setDecimals(1);
	m_pWidthDistanceSpinBox->setValue(10.0);
	connect(m_pWidthDistanceSpinBox, SIGNAL(valueChanged(double)), this, SLOT(onWidthDistanceSpinBoxChanged(double)));

	m_pFeetRadioButton = new QRadioButton(tr("Feet"), m_pSurroundDelayFrame);
	m_pFeetRadioButton->setObjectName(QStringLiteral("WhiteFont14"));
	connect(m_pFeetRadioButton, SIGNAL(clicked()), this, SLOT(onFeetButtonClicked()));	

	m_pMeterRadioButton = new QRadioButton(tr("Meters"), m_pSurroundDelayFrame);
	m_pMeterRadioButton->setObjectName(QStringLiteral("WhiteFont14"));
	connect(m_pMeterRadioButton, SIGNAL(toggled(bool)), this, SLOT(onSDToggled(bool)));
//	connect(m_pMeterRadioButton, SIGNAL(clicked()), this, SLOT(onMeterButtonClicked()));	
	m_pMeterRadioButton->setChecked(true);
	m_distanceType = DISTANCE_TYPE_METER;

//	m_pApplyButton = new QPushButton(tr("Apply"), m_pSurroundDelayFrame);
//	connect(m_pApplyButton, SIGNAL(clicked()), this, SLOT(onApplyButtonClicked()));	

	QString radioText[] = { 
		"LEFT 1-CH1", "LEFT 1-CH2", "L/R 2-CH1", "L/R 2-CH2", 
		"CENTER 1-CH1", "CENTER 1-CH2", "CENTER 2-CH1", "CENTER 2-CH2", 
		"RIGHT 1-CH1", "RIGHT 1-CH2", "LS/RS 1-CH1 (LS)", "LS/RS 1-CH2 (RS)", 
		"LS/RS 2-CH1 (LS)", "LS/RS 2-CH2 (RS)", "BLS/BRS-CH1 (BLS)", "BLS/BRS-CH2 (BRS)", 
		"SW 1-CH1 (SW)", "SW 1-CH2 (SW)", "SW 2-CH1 (SW)", "SW 2-CH2 (SW)", 
	};

	for (int i = 0; i < 20; i++)
	{
		m_pLoadDetectMaskCheck[i] = new QCheckBox(radioText[i], m_pLoadDetectionFrame);
		m_pLoadDetectMaskCheck[i]->setObjectName(QStringLiteral("CheckBox_WhiteFont14"));
		connect(m_pLoadDetectMaskCheck[i],	SIGNAL(clicked()), this, SLOT(onLoadDetectionChanged()));
	}
	m_pLoadDetectMaskCheck[7]->hide();
	m_pNoteLabel = new QLabel(tr("Remind: Only Crown XLC2500 and XLC2800 support Fault Detection!"), m_pLoadDetectionFrame);
	m_pNoteLabel->setObjectName("WhiteFont16");

	/* Child Widget of m_pMainOutputFrame */
	m_p51System = new QRadioButton(tr("5.1 System"), m_pMainOutputFrame);
	m_p51System->setObjectName(QStringLiteral("WhiteFont14"));
	connect(m_p51System, SIGNAL(toggled(bool)), this, SLOT(on51ChannelChanged(bool)));

	m_p71System = new QRadioButton(tr("7.1 System"), m_pMainOutputFrame);
	m_p71System->setObjectName(QStringLiteral("WhiteFont14"));
//	connect(m_p71System, SIGNAL(toggled(bool)), this, SLOT(on71ChannelChanged(bool)));

	/* Child Widget of Power on Mode */
//	m_pLastLoadRadio = new QRadioButton(tr("Last Setting"), m_pPowerOnModeFrame);
//	m_pLastLoadRadio->setObjectName(QStringLiteral("WhiteFont14"));
//	connect(m_pLastLoadRadio, SIGNAL(clicked()), this, SLOT(onLastLoadButtonClicked()));	

//	m_pAnalogRadio = new QRadioButton(tr("Analog Input"), m_pPowerOnModeFrame);
//	m_pAnalogRadio->setObjectName(QStringLiteral("WhiteFont14"));
//	connect(m_pAnalogRadio, SIGNAL(clicked()), this, SLOT(onPowerOnAnalogButtonClicked()));	

//	m_pDigitalRadio = new QRadioButton(tr("Digital Input"), m_pPowerOnModeFrame);
//	m_pDigitalRadio->setObjectName(QStringLiteral("WhiteFont14"));
//	connect(m_pDigitalRadio, SIGNAL(clicked()), this, SLOT(onPowerOnDigitalButtonClicked()));	

//	m_pMicRadio = new QRadioButton(tr("Mic"), m_pPowerOnModeFrame);
//	m_pMicRadio->setObjectName(QStringLiteral("WhiteFont14"));
//	connect(m_pMicRadio, SIGNAL(clicked()), this, SLOT(onPowerOnMicButtonClicked()));	

//	m_pMusicRadio = new QRadioButton(tr("Music"), m_pPowerOnModeFrame);
//	m_pMusicRadio->setObjectName(QStringLiteral("WhiteFont14"));
//	connect(m_pMusicRadio, SIGNAL(clicked()), this, SLOT(onPowerOnMusicButtonClicked()));	

	m_slotEnableFlag = true;

	refresh();
}

#include "PresetsDB.h"

void BasicSettingWidget::refreshLoadDetection()
{
	/* We can save to the data now */
	CPi2000Data *pData = getCPi2000Data();
	QString speakerLeft = pData->m_speakerName[0];
	QString speakerCenter = pData->m_speakerName[2];

	SpeakerData speakerData;
	if (g_database.getSpeaker(speakerLeft, speakerData) == true)
	{
		setLoadDetectionName(SPEAKER_CHANNEL_L, speakerData.m_speakerType);
	}

	if (g_database.getSpeaker(speakerCenter, speakerData) == true)
	{
		setLoadDetectionName(SPEAKER_CHANNEL_CENTER, speakerData.m_speakerType);
	}
}

void BasicSettingWidget::setLoadDetectionName(SPEAKER_CHANNEL channel, SPEAKER_TYPE type)
{
	QString triAmpLR[] = {"LEFT 1-CH1 (Left MF)", "LEFT 1-CH2 (Left HF)", "L/R 2-CH1 (Left LF)", "L/R 2-CH2 (Right LF)", "RIGHT 1-CH1 (Right MF)", "RIGHT 1-CH2 (Right HF)"};
	bool triAmpLRFlag[] = { true, true, true, true, true, true };

	QString biAmpLR[] = {"LEFT 1-CH1 (Left LF)", "LEFT 1-CH2 (Left HF)", "L/R 2-CH1", "L/R 2-CH2", "RIGHT 1-CH1 (Right LF)", "RIGHT 1-CH2 (Right HF)"};
	bool biAmpLRFlag[] = { true, true, false, false, true, true };

	QString passiveLR[] = {"LEFT 1-CH1 (Left)", "LEFT 1-CH2 (Right)", "L/R 2-CH1", "L/R 2-CH2", "RIGHT 1-CH1", "RIGHT 1-CH2"};
	bool passLRFlag[] = { true, true, false, false, false, false };

	QString *pString;
	bool *pFlag;

	switch (channel)
	{
	case SPEAKER_CHANNEL_L:
	case SPEAKER_CHANNEL_R:
		switch(type)
		{
		case SPEAKER_TYPE_PASSIVE:
			pString = passiveLR;
			pFlag = passLRFlag;
			break;

		case SPEAKER_TYPE_BIAMP:
			pString = biAmpLR;
			pFlag = biAmpLRFlag;
			break;

		case SPEAKER_TYPE_TRIAMP:
			pString = triAmpLR;
			pFlag = triAmpLRFlag;
			break;
		}

		m_pLoadDetectMaskCheck[0]->setText(pString[0]);
		m_pLoadDetectMaskCheck[0]->setEnabled(pFlag[0]);
		m_pLoadDetectMaskCheck[1]->setText(pString[1]);
		m_pLoadDetectMaskCheck[1]->setEnabled(pFlag[1]);
		m_pLoadDetectMaskCheck[2]->setText(pString[2]);
		m_pLoadDetectMaskCheck[2]->setEnabled(pFlag[2]);
		m_pLoadDetectMaskCheck[3]->setText(pString[3]);
		m_pLoadDetectMaskCheck[3]->setEnabled(pFlag[3]);
		m_pLoadDetectMaskCheck[8]->setText(pString[4]);
		m_pLoadDetectMaskCheck[8]->setEnabled(pFlag[4]);
		m_pLoadDetectMaskCheck[9]->setText(pString[5]);
		m_pLoadDetectMaskCheck[9]->setEnabled(pFlag[5]);

		break;
	case SPEAKER_CHANNEL_CENTER:
		switch(type)
		{
		case SPEAKER_TYPE_PASSIVE:
			m_pLoadDetectMaskCheck[4]->setEnabled(true);
			m_pLoadDetectMaskCheck[4]->setText("CENTER 1-CH1 (Center)");

			m_pLoadDetectMaskCheck[5]->setEnabled(false);
			m_pLoadDetectMaskCheck[5]->setText("CENTER 1-CH2");

			m_pLoadDetectMaskCheck[6]->setEnabled(false);
			m_pLoadDetectMaskCheck[6]->setText("CENTER 2-CH1");

			break;

		case SPEAKER_TYPE_TRIAMP:
			m_pLoadDetectMaskCheck[4]->setEnabled(true);
			m_pLoadDetectMaskCheck[4]->setText("CENTER 1-CH1 (Center MF)");

			m_pLoadDetectMaskCheck[5]->setEnabled(true);
			m_pLoadDetectMaskCheck[5]->setText("CENTER 1-CH2 (Center HF)");

			m_pLoadDetectMaskCheck[6]->setEnabled(true);
			m_pLoadDetectMaskCheck[6]->setText("CENTER 2-CH1 (Center LF)");
			
			break;

		case SPEAKER_TYPE_BIAMP:
			m_pLoadDetectMaskCheck[4]->setEnabled(true);
			m_pLoadDetectMaskCheck[4]->setText("CENTER 1-CH1 (Center LF)");

			m_pLoadDetectMaskCheck[5]->setEnabled(true);
			m_pLoadDetectMaskCheck[5]->setText("CENTER 1-CH2 (Center HF)");

			m_pLoadDetectMaskCheck[6]->setEnabled(false);
			m_pLoadDetectMaskCheck[6]->setText("CENTER 2-CH1");

			break;
		}
		break;
	case SPEAKER_CHANNEL_LS:
		break;

	case SPEAKER_CHANNEL_RS:
		break;

	case SPEAKER_CHANNEL_BLS:
		break;

	case SPEAKER_CHANNEL_BRS:
		break;


	case SPEAKER_CHANNEL_SW:
		break;
	}
}

void BasicSettingWidget::onSDToggled(bool meterFlag)
{
	if (meterFlag)
	{
		clickMeterButton();
	}
	else
	{
		clickFeetButton();
	}
}

void BasicSettingWidget::clickFeetButton()
{
	bool oldSlotEnableFlag = m_slotEnableFlag;
	m_slotEnableFlag = false;

	float distanceL = m_pLengthDistanceSpinBox->value();
	float distanceW = m_pWidthDistanceSpinBox->value();

	m_pMeterOrFeetLabel1->setText(tr("Feet"));
	m_pMeterOrFeetLabel2->setText(tr("Feet"));
	m_pLengthDistanceSpinBox->setRange(0.0, MAX_DISTANCE * 3.2808);
	m_pWidthDistanceSpinBox->setRange(0.0, MAX_DISTANCE * 3.2808);

	m_pLengthDistanceSpinBox->setValue(distanceL * 3.2808);
	m_pWidthDistanceSpinBox->setValue(distanceW * 3.2808);

	m_distanceType = DISTANCE_TYPE_FEET;


	m_slotEnableFlag = oldSlotEnableFlag;
}

void BasicSettingWidget::clickMeterButton()
{
	bool oldSlotEnableFlag = m_slotEnableFlag;
	m_slotEnableFlag = false;

	float distanceL = m_pLengthDistanceSpinBox->value();
	float distanceW = m_pWidthDistanceSpinBox->value();

	m_pMeterOrFeetLabel1->setText(tr("Meters"));
	m_pMeterOrFeetLabel2->setText(tr("Meters"));

	m_pLengthDistanceSpinBox->setRange(0.0, MAX_DISTANCE);
	m_pWidthDistanceSpinBox->setRange(0.0, MAX_DISTANCE);

	m_pLengthDistanceSpinBox->setValue(distanceL / 3.2808);
	m_pWidthDistanceSpinBox->setValue(distanceW / 3.2808);

	m_distanceType = DISTANCE_TYPE_METER;

	m_slotEnableFlag = oldSlotEnableFlag;
}

void BasicSettingWidget::resizeEvent(QResizeEvent * /*event*/)
{
	int xMargin = 30, yMargin = 20;

	int masterVolumnWidth = width() / 6;
	int masterVolumnHeight = height() - 2 * yMargin;

	m_pMasterVolumeFrame->setGeometry(xMargin, yMargin, masterVolumnWidth, masterVolumnHeight);
	m_pMasterVolumeLabel->setGeometry(0, 0, masterVolumnWidth, masterVolumnHeight / 10);
	m_pMasterVolume->setGeometry(masterVolumnWidth / 5,   masterVolumnHeight / 10, masterVolumnWidth * 3 / 5, masterVolumnHeight * 0.7 - 40);
	m_pMasterVolumeSpinBox->setGeometry(masterVolumnWidth / 2 - 45, masterVolumnHeight * 8 / 10 - 30, 90, 20);
	m_pSyncCheckBox->setGeometry(masterVolumnWidth / 2 - 45, masterVolumnHeight * 8 / 10 - 5, 150, 25);

	m_pMuteButton->setGeometry(masterVolumnWidth / 2 - 45 - 40, masterVolumnHeight * 9 / 10 - 5 , 26, 25);

	m_pFadeInLabel->setGeometry(masterVolumnWidth / 2 - 45, masterVolumnHeight * 9 / 10 - 38, 100, 20);
	m_pFadeInSpinBox->setGeometry(masterVolumnWidth / 2 - 45, masterVolumnHeight * 9 / 10 - 20, 90, 20);

	m_pFadeOutLabel->setGeometry(masterVolumnWidth / 2 - 45, masterVolumnHeight * 9 / 10 + 2, 100, 20);
	m_pFadeOutSpinBox->setGeometry(masterVolumnWidth / 2 - 45, masterVolumnHeight * 9 / 10 + 20, 90, 20);

	int left = xMargin * 2 + masterVolumnWidth;
	int surroundDelayWidth = width() - left - xMargin;
	int unitHeight = (height() - 4 * yMargin) / (5 + 3 + 2);
	m_pSurroundDelayFrame->setGeometry(left, yMargin, surroundDelayWidth, unitHeight * 3.5);
	m_pLoadDetectionFrame->setGeometry(left, 2 * yMargin + unitHeight * 3.5, surroundDelayWidth, unitHeight * 4.5);
	m_pMainOutputFrame->setGeometry(left, 3 * yMargin + 8 * unitHeight, surroundDelayWidth, unitHeight * 2); 
//	m_pPowerOnModeFrame->setGeometry(left, 4 * yMargin + 10 * unitHeight, surroundDelayWidth, unitHeight * 4); 

	/* Child Widget of m_pSurroundDelayFrame */
	int surroundDelayHeight = unitHeight * 3.5;
	int labelWidth = surroundDelayWidth * 2 / 10 + 200;
	int alignCenter = surroundDelayWidth * 5.5 / 10;
	m_pTotalDistanceLabel->setGeometry(alignCenter - labelWidth, surroundDelayHeight * 7.5 / 15, labelWidth, 17); 
	m_pAverageDistanceLabel->setGeometry(alignCenter - labelWidth, surroundDelayHeight * 11.5 / 15, labelWidth, 17); 

	m_pLengthDistanceSpinBox->setGeometry(alignCenter + 20, surroundDelayHeight * 7.5 / 15, 70, 19);
	m_pWidthDistanceSpinBox->setGeometry(alignCenter + 20, surroundDelayHeight * 11.5 / 15, 70, 19);

	m_pMeterOrFeetLabel1->setGeometry(alignCenter + 100, surroundDelayHeight * 7.5 / 15, 50, 17);
	m_pMeterOrFeetLabel2->setGeometry(alignCenter + 100, surroundDelayHeight * 11.5 / 15, 50, 17);

	m_pFeetRadioButton->setGeometry(alignCenter - 130, surroundDelayHeight * 4/ 15, 70, 20);
	m_pMeterRadioButton->setGeometry(alignCenter - 50, surroundDelayHeight * 4 / 15, 70, 20);
	m_pMeterRadioButton->setChecked(true);


	m_pHorizonLineTop->setGeometry(alignCenter + 150, surroundDelayHeight * 7.5 / 15 + 8, surroundDelayWidth * 7.2 /10 + 30 + 12 - alignCenter - 150, 1);
	m_pHorizonLineBottom->setGeometry(alignCenter + 150, surroundDelayHeight * 11.5 / 15 + 8, surroundDelayWidth * 7.2 /10 + 30 + 12 - alignCenter - 150, 1);
	m_pVerticalLine->setGeometry(surroundDelayWidth * 7.2 /10 + 30 + 11, surroundDelayHeight * 7.5 / 15 + 8, 2, surroundDelayHeight * 4 / 15 + 1);

	m_pCalcButton->setGeometry(surroundDelayWidth * 7.2 /10 + 31, surroundDelayHeight * 9.5 / 15, 24, 22);
	m_pSurroundExLabel->setGeometry(surroundDelayWidth * 7.7 /10 + 30, surroundDelayHeight * 9.5 / 15 - 23, 300, 20);
	m_pSurroundDelaySpinBox->setGeometry(surroundDelayWidth * 7.7 /10 + 30, surroundDelayHeight * 9.5 / 15, 80, 20);

//	m_pApplyButton->setGeometry(surroundDelayWidth * 4 / 5, surroundDelayHeight - 45, 100, 30);

	/* Child Widget of m_pLoadDetectionFrame */
	int loadDetectHeight = unitHeight * 4.5;

	int xPosition[5];
	xPosition[0] = surroundDelayWidth / 5 * 0.5 - 60;
	xPosition[1] = surroundDelayWidth / 5 * 1.5 - 70;
	xPosition[2] = surroundDelayWidth / 5 * 2.5 - 50;
	xPosition[3] = surroundDelayWidth / 5 * 3.5 - 50;
	xPosition[4] = surroundDelayWidth / 5 * 4.5 - 50;

	if (xPosition[0] < 0)
	{
		int value = xPosition[0];
		for (int i = 0; i < 5; i++)
		{
			xPosition[i] -= value;
		}
	}

	int index = 0;
	for (int x = 1; x <= 5; x++)
	{
		for (int y = 1; y <= 4; y++, index++)
		{
			m_pLoadDetectMaskCheck[index]->setGeometry(xPosition[x - 1], loadDetectHeight / 6 * y, 190, 25);
		}
	}
//	m_pNoteLabel->setGeometry(surroundDelayWidth / 5.5 - 90, loadDetectHeight - 40, 700, 20);
	m_pNoteLabel->setGeometry(xPosition[0], loadDetectHeight - 40, 700, 20);

#if 0
	m_pSW1CheckBox->setGeometry(surroundDelayWidth / 3 - 100, loadDetectHeight / 3, 200, 20);
	m_pSW2CheckBox->setGeometry(surroundDelayWidth / 3 - 100, loadDetectHeight * 2 / 3, 200, 20);

	m_pRS1CheckBox->setGeometry(surroundDelayWidth * 2 / 3 - 100, loadDetectHeight / 3, 200, 20);
	m_pRS2CheckBox->setGeometry(surroundDelayWidth * 2 / 3 - 100, loadDetectHeight * 2 / 3, 200, 20);
#endif
	/* Child Widget of m_pMainOutputFrame */
	int mainOutputHeight = unitHeight * 2;
	m_p51System->setGeometry(surroundDelayWidth / 3 - 100, mainOutputHeight / 2, 200, 20);
	m_p71System->setGeometry(surroundDelayWidth * 2 / 3 - 100, mainOutputHeight / 2, 200, 20);

	/* Child Widget of Power on Mode */
//	int powerOnModeHeight = unitHeight * 4;
//	m_pLastLoadRadio->setGeometry(surroundDelayWidth / 3 - 100, powerOnModeHeight / 4, 200, 20);
//	m_pDigitalRadio->setGeometry(surroundDelayWidth / 3 - 100, powerOnModeHeight / 2, 200, 20);
//	m_pMicRadio->setGeometry(surroundDelayWidth * 2 / 3 - 100, powerOnModeHeight / 2, 200, 20);
//	m_pAnalogRadio->setGeometry(surroundDelayWidth / 3 - 100, powerOnModeHeight * 3 / 4, 200, 20);
//	m_pMusicRadio->setGeometry(surroundDelayWidth * 2/ 3 - 100, powerOnModeHeight * 3 / 4, 200, 20);
}

void BasicSettingWidget::onClickMuteButton()
{
	bool flag = !m_pMuteButton->getMarkFlag();

	CPi2000Data *pData = getCPi2000Data();
	pData->setMasterOutputMuteFlag(flag);

	if (g_pApp->getConnectStatus() == DEVICE_NETWORK_CONNECTED)
	{
		g_pApp->getDeviceConnection()->setFadeInOutMuteFlag(flag);
	}
	g_pMainWidget->refreshMasterMuteFlag();
}

void BasicSettingWidget::on51ChannelChanged(bool flag)
{
	qDebug() << "on51ChannelChanged, flag = " << flag;
	
	OUTPUT_CHANNEL_TYPE channelNumber;
	if (flag == true)
	{
		channelNumber = SPEAKER_5_1;
	}
	else
	{
		channelNumber = SPEAKER_7_1;
	}
	getCPi2000Data()->setSpeakerNumber(channelNumber);	

	DeviceSocket *pDeviceSocket = g_pApp->getDeviceConnection();
	if (pDeviceSocket != nullptr)
	{
		pDeviceSocket->setOuputChannalType(channelNumber);
	}
}

void BasicSettingWidget::setSyncChecked(bool flag)
{
	m_pSyncCheckBox->setChecked(flag);
}

void BasicSettingWidget::refreshMasterVolumePreset()
{
	CPi2000Data *pData = getCPi2000Data();
	float masterVolumePreset = pData->getMasterVolumePreset();

	bool oldFlag = m_slotEnableFlag; 
	m_slotEnableFlag = false;

	m_pMasterVolume->setVolumePreset(masterVolumePreset * 10);
	m_pMasterVolume->update();

	m_slotEnableFlag = oldFlag;
}

void BasicSettingWidget::refreshMasterVolume()
{
	CPi2000Data *pData = getCPi2000Data();
	float masterVolume = pData->getMasterVolume();

	bool oldFlag = m_slotEnableFlag; 
	m_slotEnableFlag = false;

	m_pMasterVolume->setValue(masterVolume * 10);
	m_pMasterVolumeSpinBox->setValue(masterVolume);

	m_slotEnableFlag = oldFlag;
}

void BasicSettingWidget::refresh()
{

	CPi2000Data *pData = getCPi2000Data();

	bool oldSlotEnableFlag = m_slotEnableFlag;
	m_slotEnableFlag = false;

	OUTPUT_CHANNEL_TYPE speakerNumber = pData->getSpeakerNumber();
	if (speakerNumber == SPEAKER_7_1)
	{
		/* 7.1 channel */
		m_p71System->setChecked(true);
	}
	else 
	{
		/* 5.1 channel */
		m_p51System->setChecked(true);
	}
	refreshMasterVolume();
	refreshMasterVolumePreset();
	refreshMasterMuteFlag();

	m_pFadeInSpinBox->setValue(pData->getFadeIn());
	m_pFadeOutSpinBox->setValue(pData->getFadeOut());

	m_pMeterRadioButton->setChecked(true);
	m_pLengthDistanceSpinBox->setValue(pData->getLengthDistance());
	m_pWidthDistanceSpinBox->setValue(pData->getWidthDistance());
	m_pSurroundDelaySpinBox->setValue(pData->getSurroundDelay());

	int mask = getCPi2000Data()->getDetectionMask();
	for (int i = 0; i < 19; i++)
	{
		int index = i;
		if (i >= 7)
		{
			index += 1;
		}
		if (mask & (1 << i))
		{
			qDebug() << i;
			m_pLoadDetectMaskCheck[index]->setChecked(true);
		}
		else
		{
			m_pLoadDetectMaskCheck[index]->setChecked(false);
		}
	}

	m_slotEnableFlag = oldSlotEnableFlag;
}

void BasicSettingWidget::onMasterVolumeSliderChanged(int value)
{
	if (m_slotEnableFlag == false)
	{
		return;
	}

	CPi2000Data *pData = getCPi2000Data();
	float volume = (float)value / 10;
	pData->setMasterVolume(volume);

	if (m_pSyncCheckBox->isChecked())
	{
		pData->setMasterVolumePreset(volume);
		refreshMasterVolumePreset();
	}

	if (g_pApp->getConnectStatus() == DEVICE_NETWORK_CONNECTED)
	{
		g_pApp->getDeviceConnection()->setMasterVolume(volume);
		if (m_pSyncCheckBox->isChecked())
		{
			g_pApp->getDeviceConnection()->setMasterVolumePreset(volume);
		}
	}

	g_pMainWidget->refreshMasterVolume();
}

void BasicSettingWidget::refreshMasterMuteFlag()
{
	CPi2000Data *pData = getCPi2000Data();

	bool muteFlag = pData->getMasterOutputMuteFlag();
	m_pMuteButton->mark(muteFlag);
}

void BasicSettingWidget::onMasterVolumeSpinBoxChanged(double value)
{
	if (m_slotEnableFlag == false)
	{
		return;
	}
	qDebug() << "onMasterVolumeSpinBoxChanged" << value;
	CPi2000Data *pData = getCPi2000Data();
	float volume = value;
	pData->setMasterVolume(volume);

	if (g_pApp->getConnectStatus() == DEVICE_NETWORK_CONNECTED)
	{
		g_pApp->getDeviceConnection()->setMasterVolume(volume);
	}

	g_pMainWidget->refreshMasterVolume();
}

void BasicSettingWidget::onFadeInSpinBoxChanged(double value)
{
	if (m_slotEnableFlag == false)
	{
		return;
	}
	qDebug() << "onFadeInSpinBoxChanged" << value;
	CPi2000Data *pData = getCPi2000Data();
	float fadeInSecond = value;
	pData->setFadeIn(fadeInSecond);

	if (g_pApp->getConnectStatus() == DEVICE_NETWORK_CONNECTED)
	{
		g_pApp->getDeviceConnection()->setFadeIn(fadeInSecond);
	}
}



void BasicSettingWidget::onFadeOutSpinBoxChanged(double value)
{
	if (m_slotEnableFlag == false)
	{
		return;
	}
	qDebug() << "onFadeOutSpinBoxChanged" << value;
	CPi2000Data *pData = getCPi2000Data();
	float fadeOutSecond = value;
	pData->setFadeOut(fadeOutSecond);

	if (g_pApp->getConnectStatus() == DEVICE_NETWORK_CONNECTED)
	{
		g_pApp->getDeviceConnection()->setFadeOut(fadeOutSecond);
	}
}

void BasicSettingWidget::onClickCalcButton()
{
	CPi2000Data *pData = getCPi2000Data();
	double l = pData->getLengthDistance();
	double w = pData->getWidthDistance();

	float delayTime = l * 1.9321 + 0.0476 - 7.279 / 5 * w;
	if (delayTime >= 200.0)
	{
		delayTime=200.0;
	}
	if (delayTime <= 0)
	{
		delayTime=0.0;
	}

	m_pSurroundDelaySpinBox->setValue(qRound(delayTime));
}

void BasicSettingWidget::onSurroundDelaySpinBoxChanged(int value)
{
	if (m_slotEnableFlag == false)
	{
		return;
	}

	CPi2000Data *pData = getCPi2000Data();
	pData->setSurroundDelay(value);
	
	qDebug() << QString("set surroundDelay = %1 ms").arg(value);

	if (g_pApp->getConnectStatus() == DEVICE_NETWORK_CONNECTED)
	{
		g_pApp->getDeviceConnection()->setSurroundDelay(value);
	}
}

void BasicSettingWidget::onLengthDistanceSpinBoxChanged(double /*value*/)
{
	if (m_slotEnableFlag == false)
	{
		return;
	}

	QString unit;
	float lengthInMeter;
	if (m_distanceType == DISTANCE_TYPE_METER)
	{
		unit = QString("meter");
		lengthInMeter = m_pLengthDistanceSpinBox->value();
	}
	else
	{
		unit = QString("feet");
		lengthInMeter = (float)qRound(m_pLengthDistanceSpinBox->value() / 3.2808 * 10) / 10;
	}

	CPi2000Data *pData = getCPi2000Data();
	pData->setLengthDistance(lengthInMeter);
	
	qDebug() << QString("set lengthDistanceSpinBox = %1 (%2), = %3 meters").arg(m_pLengthDistanceSpinBox->value()).arg(unit).arg(lengthInMeter);

	if (g_pApp->getConnectStatus() == DEVICE_NETWORK_CONNECTED)
	{
		g_pApp->getDeviceConnection()->setLengthDistance(lengthInMeter);
	}
}

void BasicSettingWidget::onWidthDistanceSpinBoxChanged(double /*value */)
{
	if (m_slotEnableFlag == false)
	{
		return;
	}

	QString unit;
	float widthInMeter;
	if (m_distanceType == DISTANCE_TYPE_METER)
	{
		unit = QString("meter");
		widthInMeter = m_pWidthDistanceSpinBox->value();
	}
	else
	{
		unit = QString("feet");
		widthInMeter = (float)qRound(m_pWidthDistanceSpinBox->value() / 3.2808 * 10) / 10;
	}

	CPi2000Data *pData = getCPi2000Data();
	pData->setWidthDistance(widthInMeter);
	
	qDebug() << QString("set widthDistanceInValue = %1 (%2), = %3 meters").arg(m_pWidthDistanceSpinBox->value()).arg(unit).arg(widthInMeter);	

	if (g_pApp->getConnectStatus() == DEVICE_NETWORK_CONNECTED)
	{
		g_pApp->getDeviceConnection()->setWidthDistance(widthInMeter);
	}

}

void BasicSettingWidget::onLoadDetectionChanged()
{
	UINT32 detectionMask = 0;

	int bitIndex = 0;
	for (int i = 0; i < 20; i++)
	{
		if (i == 7)
		{
			continue;
		}

		if ((m_pLoadDetectMaskCheck[i]->isEnabled() == true) && (m_pLoadDetectMaskCheck[i]->isChecked() == true))
		{
			detectionMask = detectionMask | (1 << bitIndex);
		}
		bitIndex++;
	}

	qDebug() << "detectionMask = " << detectionMask;

	getCPi2000Data()->setDetectionMask(detectionMask);
	if (g_pApp->getConnectStatus() == DEVICE_NETWORK_CONNECTED)
	{
		g_pApp->getDeviceConnection()->setLoadDetect(detectionMask);
	}	
}

void BasicSettingWidget::retranslateUi()
{
	m_pSurroundDelayFrame->retranslateUi(tr("   Surround Delay"));
	m_pLoadDetectionFrame->retranslateUi(tr("   Fault Detection"));
	m_pMainOutputFrame->retranslateUi(tr("   Main Audio Setting"));
	m_pMasterVolumeLabel->setText(tr("Master Volume"));
	m_pFadeInLabel->setText(tr("Fade In:"));
	m_pFadeOutLabel->setText(tr("Fade Out:"));
	m_pSyncCheckBox->setText(tr("Sync to preset"));

	/* Child Widget of m_pSurroundDelayFrame */
	m_pTotalDistanceLabel->setText(tr("Average distance from screen to rear wall of the theater"));
	m_pAverageDistanceLabel->setText(tr("Average distance between left and right surround speakers"));
	m_pMeterOrFeetLabel1->setText(tr("Meters"));
	m_pMeterOrFeetLabel2->setText(tr("Meters"));
	m_pFeetRadioButton->setText(tr("Feet"));
	m_pMeterRadioButton->setText(tr("Meters"));
	m_pNoteLabel->setText(tr("Remind: Only Crown XLC2500 and XLC2800 support Fault Detection!"));
	m_p51System->setText(tr("5.1 System"));
	m_p71System->setText(tr("7.1 System"));
	m_pSurroundExLabel->setText(tr("Delay time:"));
	m_pCalcButton->setToolTip(tr("calculate"));
}

void BasicSettingWidget::onSyncClicked()
{
	if (m_slotEnableFlag == false)
	{
		return;
	}

	CPi2000Data *pData = getCPi2000Data();
	bool syncFlag = m_pSyncCheckBox->isChecked();
//	pData->setSyncMasterVolumePreset(syncFlag);

	if (syncFlag)
	{
		float masterVolume = pData->getMasterVolume();
		pData->setMasterVolumePreset(masterVolume);
		refreshMasterVolumePreset();

		if (g_pApp->getConnectStatus() == DEVICE_NETWORK_CONNECTED)
		{
			g_pApp->getDeviceConnection()->setMasterVolumePreset(masterVolume);
		}
	}
}
