//=============================================================================
//  HMG Confidential - Subject to Non-Disclosure Restriction  
//    
//  Copyright [yearCreated] by Harman International Industries, Inc.  
//  This is an unpublished work protected by Harman International Industries
//  as a trade secret.  All Rights Reserved                                          
// 
// All information contained herein is the sole property of Harman Music Group, 
// Inc., and may not be used, disclosed or reproduced, in whole or in part, in 
// any media, without prior written permission from Harman Music Group Inc.
// No liability is accepted for errors or omissions.
//
// PROJECT:                
//
// SYSTEM DEPENDENCIES:    
//
// FILE NAME:              FILE_NAME
//
// DESCRIPTION:        	   
//
// WRITTEN BY: 			   AUTHOR    (c) CREATE_DATE
//
// Rev.     Date        Author          Description.
// -------  ----------  --------------  ---------------------------------------
// 
//=============================================================================

#include "CBiquad.h"

const sBiquadCoefs CBiquad::_bypassCoefs = {1,0,0,0,0};


