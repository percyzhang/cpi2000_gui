#include "CPi2000Setting.h"
#include <QFile>
#include "PresetsDB.h"
#include <QJsonObject>
#include "DeviceSocket.h"
#include "mainApplication.h"
#include "commonLib.h"
#include "simpleQtLogger.h"

CPi2000Settings	g_CPi2000Setting;
CPi2000Data g_CPi2000Data;
LogManager g_logManager;

void dump(Log *pLog)
{
	qDebug() << pLog->m_date.toString(Qt::ISODate) << pLog->m_time.toString() << pLog->m_content;
}

LogManager* getLogManager()
{
	return (&g_logManager);
}

LogManager::LogManager()
{
	m_head = 0;
	m_tail = 0;
	m_filterMask = 0xf;
}

int LogManager::getCount()
{
	if (m_head >= m_tail)
	{
		return (m_head - m_tail);
	}
	else
	{
		return (m_head + MAX_LOG_COUNT - m_tail);
	}
}

Log * LogManager::getLog(int i)
{
	Log *pLog;
	if ((m_tail + i) >= MAX_LOG_COUNT)
	{
		pLog = &m_pLog[m_tail + i - MAX_LOG_COUNT];
	}
	else
	{
		pLog = &m_pLog[m_tail + i];		
	}

	return (pLog);
}

void LogManager:: addLog(Log *pLog)
{
	m_pLog[m_head].m_content = pLog->m_content;
	m_pLog[m_head].m_date = pLog->m_date;
	m_pLog[m_head].m_level = pLog->m_level;
	m_pLog[m_head].m_time = pLog->m_time;
//	dump(m_pLog + m_head);

	m_head++;
	if (m_head == MAX_LOG_COUNT)
	{
		m_head = 0;
	}

	if (m_tail == m_head)
	{
		m_tail++;
		if (m_tail == MAX_LOG_COUNT)
		{
			m_tail = 0;
		}
	}
}

void LogManager::clearAll()
{
	m_head = 0;
	m_tail = 0;
}

void LogManager::addLogFile(QString fileName)
{
	QFile file(fileName);
	if (file.open(QIODevice::ReadOnly | QIODevice::Text) == false)
	{
		return;
	}

	Log theLog;
	QStringList levelList;
	levelList << "info" << "warning" << "error" << "critical";

	QTextStream in(&file);
	while (!in.atEnd())
	{
		QString line = in.readLine(); //read one line at a time

		/* Let's explain the Log as following: 
			[2017-06-19 12:06:09.120] [harman_logger] [info] set \\Preset\MasterVol\SV\Gain\r to -30.4
		*/
		QRegExp rx("\\[([^\\]]*)\\] \\[([^\\]]*)\\] \\[([^\\]]*)\\] (.*)");
		if (rx.indexIn(line) == -1)
		{
			qDebug() << "Invalid Log: " << line;
			continue;
		}
		
		QString dateTime = rx.cap(1);
		QRegExp dateRx("(\\d+)-(\\d+)-(\\d+) (\\d+):(\\d+):(\\d+)\\.(\\d+)");
		if (dateRx.indexIn(dateTime) == -1)
		{
			qDebug() << "Invalid data format: " << dateTime;
			continue;
		}
		theLog.m_date = QDate(dateRx.cap(1).toInt(), dateRx.cap(2).toInt(), dateRx.cap(3).toInt());
		theLog.m_time = QTime(dateRx.cap(4).toInt(), dateRx.cap(5).toInt(), dateRx.cap(6).toInt(), dateRx.cap(7).toInt());

		QString level = rx.cap(3);
		int i;
		for (i = 0; i < 4; i++)
		{
			if (level == levelList.at(i))
			{
				theLog.m_level = (LOG_WARNING_LEVEL)i;
				break;
			}
		}
		if (i == 4)
		{
			continue;
		}

		theLog.m_content = rx.cap(4);
		addLog(&theLog);
	}
}


void LogManager::filter(int mask, LogManager &newLogManager)
{
	newLogManager.clearAll();

	int count = getCount();
	for (int i = 0; i < count; i++)
	{
		Log *pLog = getLog(i);
		int logLevel = (int)pLog->m_level;
		if ((1 << logLevel) & mask)
		{
			newLogManager.addLog(pLog);
		}
	}
}

float volumeToDB(float volume)
{
	float maxDB = 0; // max volume= 10.0, db = 0dB
	float maxVolume = 10.0;

	float defaultDB = -80;	// default volume= 0, db = -80dB
	float defaultVolume = 0;

	return (defaultDB + (volume - defaultVolume) *(maxDB - defaultDB) / (maxVolume - defaultVolume));
}


float DBToVolume(float DB)
{
	float maxDB = 0; // max volume= 10.0, db = 0dB
	float maxVolume = 10.0;

	float defaultDB = -80;	// default volume= 0, db = -80dB
	float defaultVolume = 0;

	float ret = (defaultVolume + (DB - defaultDB) * (maxVolume - defaultVolume) / (maxDB - defaultDB));
	return ((float)qRound(ret * 10) / 10);
}

float masterVolumeToDB(float volume)
{
	float maxDB, minDB, maxVolume, minVolume;

	if (volume < 4)
	{
		maxDB = -10;
		maxVolume = 4;
		minDB = -90;
		minVolume = 0;
	}
	else
	{
		maxDB = 10;
		maxVolume = 10;
		minDB = -10;
		minVolume = 4;
	}
	return (minDB + (volume - minVolume) *(maxDB - minDB) / (maxVolume - minVolume));
}


float DBToMasterVolume(float DB)
{
	float maxDB, minDB, maxVolume, minVolume;

	if (DB < -10)
	{
		maxDB = -10;
		maxVolume = 4;
		minDB = -90;
		minVolume = 0;
	}
	else
	{
		maxDB = 10;
		maxVolume = 10;
		minDB = -10;
		minVolume = 4;
	}

	float ret = (minVolume + (DB - minDB) * (maxVolume - minVolume) / (maxDB - minDB));
	return ((float)qRound(ret * 10) / 10);
}

RTAData::RTAData()
{
	init();
}

void RTAData::init()
{
	m_signalMode = SIGNAL_OFF;
	m_measuredValue = 0;
	m_pinkNoiseIndex_EQTurning = 0;

	for (int i = 0; i < 8; i++)
	{
		m_pinkNoise_RoomLevel[i] = false;
	}

	m_splValue = -60.0;
	m_addValue = 0;

	m_rotateFlag = false;
	m_RTAMode = RTA_FULL;
}

void RTAData::setRoomLevelPinkNoise(bool roomLevelPinkNoise[8])
{
	for (int i = 0; i < 8; i++)
	{
		m_pinkNoise_RoomLevel[i] = roomLevelPinkNoise[i];
	}
}

void RTAData::getRoomLevelPinkNoise(bool * roomLevelPinkNoise)
{
	for (int i = 0; i < 8; i++)
	{
		roomLevelPinkNoise[i] = m_pinkNoise_RoomLevel[i];
	}
}

void RTAData::setRTAValue(float RTAValue[30])
{
	for (int i = 0; i < 30; i++)
	{
		m_RTAValue[i] = RTAValue[i];
	}
}

float *RTAData::getRTAValue()
{
	return (m_RTAValue);
}


CPi2000Settings::CPi2000Settings(QObject *parent) :
    QObject(parent)
{
}

CPi2000Data *getCPi2000Data()
{
	return (&g_CPi2000Data);
}


CPi2000Data::CPi2000Data()
{
	for (int i = 0; i < 8; i++)
	{
		m_outputDB[i] = -80;
		m_inputDB[i] = -80;
		m_AnalogChannel[i] = i;	
		m_DigitalChannel[i] = i;
	}

	m_strTheaterName = "MCL";
	m_strHallName = "VIP-HALL-1";
	m_serverName = "VIP1_Server_1022";
	m_projectorName = "VIP1_Projector_1022";

	m_speakerName[0] = QString("Passive-Bypass");
	m_speakerName[1] = QString("Passive-Bypass");
	m_speakerName[2] = QString("Passive-Bypass");
	m_speakerName[3] = QString("Subwoofer-Bypass");
	m_speakerName[4] = QString("Surround-Bypass");
	m_speakerName[5] = QString("Surround-Bypass");
	m_speakerName[6] = QString("Surround-Bypass");
	m_speakerName[7] = QString("Surround-Bypass");
	for (int i = 0; i < 8; i++)
	{
		m_amplifierName[i] = QString("Amplifier_%1").arg(i);
	}
	m_strSerialNumber = QString("Not available");

	m_maxInputDB = -10;
	m_speakerNumber = SPEAKER_7_1;	// output: 7.1 channel

	m_masterVolumePreset = 7.0;
	m_masterVolume = 7.0;
	m_masterOutputMuteFlag = false;

	m_inputSource = INPUT_SOURCE_DIGITAL;
	m_hallLength = 10.0;
	m_hallWidth = 10.0;
	m_surroundDelay = 0;

	m_detectionMask = 0;
	m_RTACenterLevel = -90.0;
	m_RTAAverageLevel = -90.0;

	m_networkSetting.m_dhcpEnable = true;
	m_networkSetting.m_ipAddress = QHostAddress("192.168.1.105");
	m_networkSetting.m_networkMask = QHostAddress("255.255.0.0");
	m_networkSetting.m_ipGateway = QHostAddress("192.168.1.1");

	m_currentOutputChannel = 0;

//	m_swConfiguration.m_bPLEnableFlag = false;
	m_swConfiguration.m_centerGenFlag = false;
	m_swConfiguration.m_inverseFlag = false;

	m_usbConnectStatus = false;
}


bool CPi2000Data::loadFromJSON(QString fileName)
{

	QString strDigitalChannel, strAnalogChannel;
	float masterVolumePresetInDB, digitalVolumeInDB, musicVolumeInDB, micVolumeInDB, analogVolumeInDB;
	float widthInMeter, lengthInMeter;
	int fadeIn, fadeOut, inputSource, speakerNumber, micChannel, LFEInverse = 0;
	float swBellQ1, swBellQ2;	/* Q for Subwoofer Bell  */
//	QString swLPFType;	/* "Low Pass" or "Flat" */
	QString swPolarity; /* "Normal" or "Inverted" */
	bool centerGen = false;

	const JSON_STRING_TABLE stringTable[] = 
	{
		/* For TAB 0: Overview */
		{ JSON_ENUM,			"Preset/SourceRouter/InputSource"	, &inputSource,			g_inputSourceTable, },
		{ JSON_STRING,			"Node/.rootSVs/Theater"				, &m_strTheaterName,  nullptr, 0	},
		{ JSON_STRING,			"Node/.rootSVs/Hall"				, &m_strHallName,  nullptr, 0	},
		{ JSON_STRING,			"Node/.rootSVs/Server"				, &m_serverName,  nullptr, 0	},
		{ JSON_STRING,			"Node/.rootSVs/Projector"			, &m_projectorName,  nullptr, 0	},
		{ JSON_STRING,			"Node/.rootSVs/SpeakerSettingL"		, &m_speakerName[0],  nullptr, 0   },
		{ JSON_STRING,			"Node/.rootSVs/SpeakerSettingR"		, &m_speakerName[1],  nullptr, 0	},
		{ JSON_STRING,			"Node/.rootSVs/SpeakerSettingC"		, &m_speakerName[2],  nullptr, 0	},
		{ JSON_STRING,			"Node/.rootSVs/SpeakerSettingLFE"	, &m_speakerName[3],  nullptr, 0	},
		{ JSON_STRING,			"Node/.rootSVs/SpeakerSettingLS"	, &m_speakerName[4],  nullptr, 0	},
		{ JSON_STRING,			"Node/.rootSVs/SpeakerSettingRS"	, &m_speakerName[5],  nullptr, 0	},
		{ JSON_STRING,			"Node/.rootSVs/SpeakerSettingBLS"	, &m_speakerName[6],  nullptr, 0	},
		{ JSON_STRING,			"Node/.rootSVs/SpeakerSettingBRS"	, &m_speakerName[7],  nullptr, 0   },
		{ JSON_STRING,			"Node/.rootSVs/Amplifier_L"			, &m_amplifierName[0],  nullptr, 0	},
		{ JSON_STRING,			"Node/.rootSVs/Amplifier_R"			, &m_amplifierName[1],  nullptr, 0	},
		{ JSON_STRING,			"Node/.rootSVs/Amplifier_C"			, &m_amplifierName[2],  nullptr, 0	},
		{ JSON_STRING,			"Node/.rootSVs/Amplifier_LFE"		, &m_amplifierName[3],  nullptr, 0	},
		{ JSON_STRING,			"Node/.rootSVs/Amplifier_LS"		, &m_amplifierName[4],  nullptr, 0	},
		{ JSON_STRING,			"Node/.rootSVs/Amplifier_RS"		, &m_amplifierName[5],  nullptr, 0	},
		{ JSON_STRING,			"Node/.rootSVs/Amplifier_BLS"		, &m_amplifierName[6],  nullptr, 0   },
		{ JSON_STRING,			"Node/.rootSVs/Amplifier_BRS"		, &m_amplifierName[7],  nullptr, 0   },
		/* End of TAB 0: Overview */

		/* For TAB 1: Basic Setting */
		{ JSON_FLOAT,			"Preset/MasterVol/GainPreset"				, &masterVolumePresetInDB ,  nullptr, 0   },
		{ JSON_BOOL,			"Preset/Fader/Mute"							, &m_masterOutputMuteFlag ,  nullptr, 0   },
		{ JSON_INT,				"Preset/Fader/FadeIn"						, &fadeIn  ,  nullptr, 0   },
		{ JSON_INT,				"Preset/Fader/FadeOut"						, &fadeOut  ,  nullptr, 0   },
		{ JSON_ENUM,			"Node/.rootSVs/AudioInputType"				, &speakerNumber  , g_AudioOutputTable },				
		{ JSON_INT,				"Node/.rootSVs/FaultMask"					, &m_detectionMask ,  nullptr, 0   },
		{ JSON_FLOAT,			"Node/.rootSVs/DistanceL"					, &lengthInMeter  ,  nullptr, 0   },		
		{ JSON_FLOAT,			"Node/.rootSVs/DistanceW"					, &widthInMeter  ,  nullptr, 0   },		

		/* For TAB2: Input Setting */
		{ JSON_FLOAT,			"Preset/SourceRouter/DigitalGain"		, &digitalVolumeInDB  ,  nullptr, 0   },		
		{ JSON_FLOAT,			"Preset/SourceRouter/AnalogGain"		, &analogVolumeInDB ,  nullptr, 0   },		
		{ JSON_FLOAT,			"Preset/SourceRouter/MicGain"			, &micVolumeInDB  ,  nullptr, 0   },		
		{ JSON_FLOAT,			"Preset/SourceRouter/MusicGain"			, &musicVolumeInDB ,  nullptr, 0   },	

		{ JSON_INT,				"Preset/InDelay/Channel_5_Amount"		, &m_surroundDelay ,  nullptr, 0   },	

		{ JSON_BOOL,			"Preset/SourceRouter/DigitalMute"		, &m_digitalMute,  nullptr, 0   },	
		{ JSON_BOOL,			"Preset/SourceRouter/AnalogMute"		, &m_analogMute ,  nullptr, 0   },		
		{ JSON_BOOL,			"Preset/SourceRouter/MicMute"			, &m_micMute  ,  nullptr, 0   },		
		{ JSON_BOOL,			"Preset/SourceRouter/MusicMute"			, &m_musicMute  ,  nullptr, 0   },

		{ JSON_BOOL,			"Preset/SourceRouter/DigitalDelay"		, &m_digitalDelayEnable  ,  nullptr, 0   },	
		{ JSON_BOOL,			"Preset/SourceRouter/AnalogDelay"		, &m_analogDelayEnable ,  nullptr, 0   },		
		{ JSON_BOOL,			"Preset/SourceRouter/MicDelay"			, &m_micDelayEnable  ,  nullptr, 0   },		
		{ JSON_BOOL,			"Preset/SourceRouter/MusicDelay"		, &m_musicDelayEnable ,  nullptr, 0   },	

		{ JSON_INT,				"Preset/SourceRouter/DigitalDelayAmount", &m_digitalDelay ,  nullptr, 0   },		
		{ JSON_INT,				"Preset/SourceRouter/AnalogDelayAmount"	, &m_analogDelay  ,  nullptr, 0   },	
		{ JSON_INT,				"Preset/SourceRouter/MicDelayAmount"	, &m_micDelay  ,  nullptr, 0   },		
		{ JSON_INT,				"Preset/SourceRouter/MusicDelayAmount"	, &m_musicDelay  ,  nullptr, 0   },		

		{ JSON_ENUM,			"Preset/SourceRouter/MicChannelAssign"	, &micChannel ,  g_micChannelTable},
		{ JSON_INT,				"Preset/SourceRouter/MusicChannelAssign", &m_musicChannel ,  nullptr, 0},

		{ JSON_BOOL,			"Preset/SourceRouter/PhantomPower"		, &m_phantomEnable  ,  nullptr, 0   },
		{ JSON_STRING,			"Preset/SourceRouter/DigitalChannel"	, &strDigitalChannel,  nullptr, 0   },
		{ JSON_STRING,			"Preset/SourceRouter/AnalogChannel"		, &strAnalogChannel ,  nullptr, 0   },

		/* For TAB3: Output Setting - Rool Level */
		{ JSON_BOOL,			"Preset/InputGains/Channel_1_Mute"		, &m_pOutputChannel[0].m_roomLevelMute  ,  nullptr, 0   },
		{ JSON_BOOL,			"Preset/InputGains/Channel_2_Mute"		, &m_pOutputChannel[1].m_roomLevelMute  ,  nullptr, 0   },
		{ JSON_BOOL,			"Preset/InputGains/Channel_3_Mute"		, &m_pOutputChannel[2].m_roomLevelMute  ,  nullptr, 0   },
		{ JSON_BOOL,			"Preset/InputGains/Channel_4_Mute"		, &m_pOutputChannel[3].m_roomLevelMute  ,  nullptr, 0   },
		{ JSON_BOOL,			"Preset/InputGains/Channel_5_Mute"		, &m_pOutputChannel[4].m_roomLevelMute  ,  nullptr, 0   },
		{ JSON_BOOL,			"Preset/InputGains/Channel_6_Mute"		, &m_pOutputChannel[5].m_roomLevelMute  ,  nullptr, 0   },
		{ JSON_BOOL,			"Preset/InputGains/Channel_7_Mute"		, &m_pOutputChannel[6].m_roomLevelMute  ,  nullptr, 0   },
		{ JSON_BOOL,			"Preset/InputGains/Channel_8_Mute"		, &m_pOutputChannel[7].m_roomLevelMute  ,  nullptr, 0   },
																												
		{ JSON_FLOAT,			"Preset/InputGains/Channel_1_Gain"		, &m_pOutputChannel[0].m_roomLevelGain  ,  nullptr, 0   },	
		{ JSON_FLOAT,			"Preset/InputGains/Channel_2_Gain"		, &m_pOutputChannel[1].m_roomLevelGain  ,  nullptr, 0   },
		{ JSON_FLOAT,			"Preset/InputGains/Channel_3_Gain"		, &m_pOutputChannel[2].m_roomLevelGain  ,  nullptr, 0   },
		{ JSON_FLOAT,			"Preset/InputGains/Channel_4_Gain"		, &m_pOutputChannel[3].m_roomLevelGain  ,  nullptr, 0   },
		{ JSON_FLOAT,			"Preset/InputGains/Channel_5_Gain"		, &m_pOutputChannel[4].m_roomLevelGain  ,  nullptr, 0   },
		{ JSON_FLOAT,			"Preset/InputGains/Channel_6_Gain"		, &m_pOutputChannel[5].m_roomLevelGain  ,  nullptr, 0   },
		{ JSON_FLOAT,			"Preset/InputGains/Channel_7_Gain"		, &m_pOutputChannel[6].m_roomLevelGain  ,  nullptr, 0   },
		{ JSON_FLOAT,			"Preset/InputGains/Channel_8_Gain"		, &m_pOutputChannel[7].m_roomLevelGain  ,  nullptr, 0   },

		{ JSON_ENUM,			"Preset/InputGains/Channel_4_Polarity"	, &LFEInverse  ,  g_LFEInverseTable   },	
		{ JSON_FLOAT,			"Preset/InPeq/Channel_4_Band_1_Q"		, &swBellQ1, nullptr, 0   },
		{ JSON_FLOAT,			"Preset/InPeq/Channel_4_Band_2_Q"		, &swBellQ2, nullptr, 0   },
		{ JSON_BOOL,			"Preset/SourceRouter/SignalCenterMode"	, &centerGen  ,  nullptr, 0   },
	};

    QFile loadFile(fileName);
	bool *pBoolValue;
	float *pFloatValue;
	int *pIntValue;
	QString *pStringValue;
	QRegExp rx_float, rx_int;
	QString JSONKey;
	QString value;

	rx_float.setPattern(REGULAR_EXPRESSION_FLOAT); 
	rx_int.setPattern(REGULAR_EXPRESSION_INTEGER); 

	g_database.setPresetDBFileName(fileName);
	QString content = g_database.getJSON("0");

    if (content.length() == 0)
	{
		L_ERROR(QString("content of %1 is NULL").arg(fileName));
		return (false);
	}

	QJsonDocument loadDoc(QJsonDocument::fromJson(content.toUtf8()));
	QJsonObject mainObject = loadDoc.object();
//    qDebug() << readObject(mainObject, "/Preset/InDelay/Amount");
	for (int i = 0; i < sizeof(stringTable) / sizeof(JSON_STRING_TABLE); i++)
	{
		QString value = readObject(mainObject, stringTable[i].m_JSONKey);
		if (value == QString("NULL"))
		{
			value = "";
		}
		
		if (stringTable[i].m_stringID == JSON_STRING)
		{
			pStringValue = (QString *)(stringTable[i].m_pValue);
			*pStringValue = value;
			qDebug() << i << value << " = " << *pStringValue;		
		}
		else if (stringTable[i].m_stringID == JSON_BOOL)
		{
			pBoolValue = (bool *)(stringTable[i].m_pValue);
			/* "On" or "Off" */
			if (value == "On")
			{
				*pBoolValue = true;
				qDebug() << i << value << "true";				
			}
			else if (value == "Off")
			{
				*pBoolValue = false;
				qDebug() << i << value << "false";				
			}
			else
			{
				qDebug() << i << value << "Invalid Bool Value";				
			}
		}
		else if (stringTable[i].m_stringID == JSON_INT)
		{
			pIntValue = (int *)(stringTable[i].m_pValue);
			// if match all, it should be : rx.setPattern("(^-?\\d+$)"); 
			// Here we just need to match the first digital munber.
			if (rx_int.indexIn(value) != -1) 
			{
				*pIntValue = rx_int.cap(1).toInt();
				qDebug() << i << value << *pIntValue;				
			}
			else
			{
				qDebug() << i << value << "Invalid Int Value";							
			}
		}
		else if (stringTable[i].m_stringID == JSON_FLOAT)
		{
			pFloatValue = (float *)(stringTable[i].m_pValue);
			if (rx_float.indexIn(value) != -1) 
			{
				*pFloatValue = rx_float.cap(1).toFloat();
				qDebug() << i << value << *pFloatValue;				
			}
			else
			{
				qDebug() << i << value << "Invalid Float Value";							
			}
		}
		else if (stringTable[i].m_stringID == JSON_ENUM)
		{
			pIntValue = (int *)(stringTable[i].m_pValue);
			*pIntValue = getTableValue(stringTable[i].m_pExplainTable, value);
			qDebug() << i << value << " = " << *pIntValue;						
		}
		else
		{
			qDebug() << "Invalid JSON_ID: " << i << "  = " << stringTable[i].m_stringID;			
		}
	}

	m_hallLength = (float)(qRound(lengthInMeter * 10)) / 10;
	m_hallWidth = (float)(qRound(widthInMeter * 10)) / 10;

	m_inputSource = (INPUT_SOURCE)inputSource;
	m_speakerNumber = (OUTPUT_CHANNEL_TYPE)speakerNumber;
	m_micChannel = (MIC_CHANNEL)micChannel;
	m_swConfiguration.m_inverseFlag = (bool)LFEInverse;
	m_swConfiguration.m_centerGenFlag = centerGen;

	m_pOutputChannel[3].m_lShelf.setQ(swBellQ1);  
	m_pOutputChannel[3].m_hShelf.setQ(swBellQ2);  

	/* TAB1: Basic Setting */
	/* Master Volume */
	float masterVolumePreset = DBToMasterVolume(masterVolumePresetInDB);
	setMasterVolumePreset(masterVolumePreset);
	setMasterVolume(masterVolumePreset);	/* set the temp master volume */

	/* Fade in & Fade out */
	m_fadeIn = (float)fadeIn / 1000;
	m_fadeOut = (float)fadeOut / 1000;

	/* TAB2: Input Setting */
//	float digitalVolume = DBToVolume(digitalVolumeInDB);
	setDigitalVolume(digitalVolumeInDB);
//	float analogVolume = DBToVolume(analogVolumeInDB);
	setAnalogVolume(analogVolumeInDB);
//	float micVolume = DBToVolume(micVolumeInDB);
	setMicVolume(micVolumeInDB);
//	float musicVolume = DBToVolume(musicVolumeInDB);
	setMusicVolume(musicVolumeInDB);

	qDebug() << strDigitalChannel;
	QStringList digitalChannelList = strDigitalChannel.split(" ");
	qDebug() << digitalChannelList;
	for (int i = 0; i < 8; i++)
	{
		m_DigitalChannel[i] = digitalChannelList.at(i).toInt();
	}
	
	QStringList analogChannelList = strAnalogChannel.split(" ");
	for (int i = 0; i < 8; i++)
	{
		m_AnalogChannel[i] = analogChannelList.at(i).toInt();
	}

	/* TAB3: Output Setting - Room Level */
	setEQTuningPinkNoise(0);

	bool channalPinkNoise[] = {false, false, false, false, false, false, false, false };
	setRoomLevelPinkNoise(channalPinkNoise);

	/* Let's load GEQ Here */
	QStringList GEQspeakerList;
	GEQspeakerList << "InGeq_L" << "InGeq_R" << "InGeq_C" << "InGeq_LFE" << "InGeq_LS" << "InGeq_RS" << "InGeq_BLS" << "InGeq_BRS";

	QStringList freqList;
	freqList	<< "40 Hz" << "50 Hz" << "63 Hz" 
				<< "80 Hz" << "100 Hz" << "125 Hz" 
				<< "160 Hz" << "200 Hz" << "250 Hz"
				<< "315 Hz" << "400 Hz" << "500 Hz" 
				<< "630 Hz"	<< "800 Hz" << "1.0 kHz" 
				<< "1.25 kHz" << "1.6 kHz" << "2.0 kHz" 
				<< "2.5 kHz" << "3.15 kHz" << "4.0 kHz" 
				<< "5.0 kHz" << "6.3 kHz" << "8.0 kHz"
				<< "10.0 kHz" << "12.5 kHz" << "16.0 kHz";

	/* We will get float value here... */
	for (int speakerIndex = 0; speakerIndex < 8; speakerIndex++)
	{
		qDebug() << "***************************";
		
		if (speakerIndex == 3)
		{
			continue;
		}

		/* Get GEQ Value */
		for (int freqIndex = 0; freqIndex < 27; freqIndex++)
		{
			JSONKey = QString("Preset/%1/%2").arg(GEQspeakerList.at(speakerIndex)).arg(freqList.at(freqIndex));
			value = readObject(mainObject, JSONKey);
			if (rx_float.indexIn(value) != -1) 
			{
				setGEQ(speakerIndex, freqIndex, rx_float.cap(1).toFloat());
				qDebug() << JSONKey << value << rx_float.cap(1).toFloat();
			}
		}

		/* Get PEQ- Low Shelf Filter Value */
		EQData *pEQ = m_pOutputChannel[speakerIndex].getLShelf();
		
		JSONKey = QString("Preset/InPeq/Channel_%1_Band_1_Frequency").arg(speakerIndex + 1);
		QString strFreq = readObject(mainObject, JSONKey);
		pEQ->setFrequency(strFreq);

		JSONKey = QString("Preset/InPeq/Channel_%1_Band_1_Gain").arg(speakerIndex + 1);
		QString strGain = readObject(mainObject, JSONKey);
		pEQ->setGain(readFloat(strGain));
		
		JSONKey = QString("Preset/InPeq/Channel_%1_Band_1_Slope").arg(speakerIndex + 1);
		QString strSlope = readObject(mainObject, JSONKey);
		pEQ->setSlope(readFloat(strSlope));

		qDebug() << strFreq << strGain << strSlope;

		/* Get PEQ- High Shelf Filter Value */
		pEQ = m_pOutputChannel[speakerIndex].getHShelf();
		
		JSONKey = QString("Preset/InPeq/Channel_%1_Band_2_Frequency").arg(speakerIndex + 1);
		strFreq = readObject(mainObject, JSONKey);
		pEQ->setFrequency(strFreq);

		JSONKey = QString("Preset/InPeq/Channel_%1_Band_2_Gain").arg(speakerIndex + 1);
		strGain = readObject(mainObject, JSONKey);
		pEQ->setGain(readFloat(strGain));
		
		JSONKey = QString("Preset/InPeq/Channel_%1_Band_2_Slope").arg(speakerIndex + 1);
		strSlope = readObject(mainObject, JSONKey);
		pEQ->setSlope(readFloat(strSlope));

		qDebug() << strFreq << strGain << strSlope;

	}
	
	RTAData *pRTA = getRTA();
	pRTA->init();

    return true;
}


bool CPi2000Data::saveToJSON(QString presetDBFileName) 
{
	QString localPresetFileName = MainApplication::getTempPresetDBFileName();
	QFile localPresetFile(localPresetFileName);
	if (localPresetFile.open(QIODevice::ReadOnly) == false)
	{
		QFile::copy(QString(":/binaries/Presets.db"), localPresetFileName);
		QFile::setPermissions(localPresetFileName, QFileDevice::WriteOwner | QFileDevice::WriteUser | QFileDevice::WriteGroup | QFileDevice::WriteOther);
	}
	else
	{
		localPresetFile.close();
	}

	g_database.setPresetDBFileName(localPresetFileName);
	QString content = g_database.getJSON("0");
	qDebug() << content.length();
	qDebug() << content;

    if (content.length() == 0)
	{
		return (false);
	}

	QJsonDocument loadDoc(QJsonDocument::fromJson(content.toUtf8()));
	QJsonObject mainObject = loadDoc.object();

	QString debugKey = QString("Preset/SourceRouter/MicChannelAssign");
	qDebug() << readObject(mainObject, debugKey);

	struct JSON_KEY_STRING
	{
		QString m_strKeyList;
		QString m_valueString;
	}  stringTable[] = 
	{
		/* For TAB 0: Overview */
		{ "Preset/SourceRouter/InputSource"	, getTableString(g_inputSourceTable, m_inputSource) },
		{ "Node/.rootSVs/Theater"			, m_strTheaterName},
		{ "Node/.rootSVs/Hall"				, m_strHallName},
		{ "Node/.rootSVs/Server"			, m_serverName},
		{ "Node/.rootSVs/Projector"			, m_projectorName},
		{ "Node/.rootSVs/SpeakerSettingL"	, m_speakerName[0]},
		{ "Node/.rootSVs/SpeakerSettingR"	, m_speakerName[1]},
		{ "Node/.rootSVs/SpeakerSettingC"	, m_speakerName[2]},
		{ "Node/.rootSVs/SpeakerSettingLFE"	, m_speakerName[3]},
		{ "Node/.rootSVs/SpeakerSettingLS"	, m_speakerName[4]},
		{ "Node/.rootSVs/SpeakerSettingRS"	, m_speakerName[5]},
		{ "Node/.rootSVs/SpeakerSettingBLS"	, m_speakerName[6]},
		{ "Node/.rootSVs/SpeakerSettingBRS"	, m_speakerName[7]},
		{ "Node/.rootSVs/Amplifier_L"		, m_amplifierName[0]},
		{ "Node/.rootSVs/Amplifier_R"		, m_amplifierName[1]},
		{ "Node/.rootSVs/Amplifier_C"		, m_amplifierName[2]},
		{ "Node/.rootSVs/Amplifier_LFE"		, m_amplifierName[3]},
		{ "Node/.rootSVs/Amplifier_LS"		, m_amplifierName[4]},
		{ "Node/.rootSVs/Amplifier_RS"		, m_amplifierName[5]},
		{ "Node/.rootSVs/Amplifier_BLS"		, m_amplifierName[6]},
		{ "Node/.rootSVs/Amplifier_BRS"		, m_amplifierName[7]},

		/* For TAB 1: Basic Setting */
		{ "Preset/MasterVol/GainPreset"		, QString::number(masterVolumeToDB(getMasterVolumePreset()), 'f', 1) + QString("dB")},
//		{ "Preset/MasterVol/Gain"			, QString::number(masterVolumeToDB(getMasterVolume()), 'f', 1) + QString("dB")},
		{ "Preset/Fader/Mute"				, getTableString(g_boolTable, m_masterOutputMuteFlag)},
		{ "Preset/Fader/FadeIn"				, QString::number(qRound(m_fadeIn * 1000), 10) },
		{ "Preset/Fader/FadeOut"			, QString::number(qRound(m_fadeOut * 1000), 10) },
		{ "Node/.rootSVs/AudioInputType"	, getTableString(g_AudioOutputTable, (int)m_speakerNumber)},		
		{ "Node/.rootSVs/FaultMask"			, QString::number(m_detectionMask, 10) },
		{ "Node/.rootSVs/DistanceL"			, QString::number(m_hallLength, 'f', 1) },
		{ "Node/.rootSVs/DistanceW"			, QString::number(m_hallWidth, 'f', 1) },

		{ "Preset/InDelay/Channel_5_Amount"			, QString("%1ms/%2ft/%3m").arg(m_surroundDelay).arg(1.127 * m_surroundDelay).arg(0.343 * m_surroundDelay) },
		{ "Preset/InDelay/Channel_6_Amount"			, QString("%1ms/%2ft/%3m").arg(m_surroundDelay).arg(1.127 * m_surroundDelay).arg(0.343 * m_surroundDelay) },
		{ "Preset/InDelay/Channel_7_Amount"			, QString("%1ms/%2ft/%3m").arg(m_surroundDelay).arg(1.127 * m_surroundDelay).arg(0.343 * m_surroundDelay) },
		{ "Preset/InDelay/Channel_8_Amount"			, QString("%1ms/%2ft/%3m").arg(m_surroundDelay).arg(1.127 * m_surroundDelay).arg(0.343 * m_surroundDelay) },

		/* For TAB2: Input Setting */
		{ "Preset/SourceRouter/DigitalGain"	, QString::number(getDigitalVolume(), 'f', 1) + QString("dB")},
		{ "Preset/SourceRouter/AnalogGain"	, QString::number(getAnalogVolume(), 'f', 1) + QString("dB")},	
		{ "Preset/SourceRouter/MicGain"		, QString::number(getMicVolume(), 'f', 1) + QString("dB")},	
		{ "Preset/SourceRouter/MusicGain"	, QString::number(getMusicVolume(), 'f', 1) + QString("dB")},	

		{ "Preset/SourceRouter/DigitalMute"	, getTableString(g_boolTable, m_digitalMute)},
		{ "Preset/SourceRouter/AnalogMute"	, getTableString(g_boolTable, m_analogMute)},
		{ "Preset/SourceRouter/MicMute"		, getTableString(g_boolTable, m_micMute)},
		{ "Preset/SourceRouter/MusicMute"	, getTableString(g_boolTable, m_musicMute)},

		{ "Preset/SourceRouter/DigitalDelay", getTableString(g_boolTable, m_digitalDelayEnable)},
		{ "Preset/SourceRouter/AnalogDelay"	, getTableString(g_boolTable, m_analogDelayEnable)},
		{ "Preset/SourceRouter/MicDelay"	, getTableString(g_boolTable, m_micDelayEnable)},
		{ "Preset/SourceRouter/MusicDelay"	, getTableString(g_boolTable, m_musicDelayEnable)},

		{ "Preset/SourceRouter/DigitalDelayAmount", QString::number(m_digitalDelay, 10) + "ms/" + QString::number(m_digitalDelay * 1.127, 'f', 2) + "ft/" +  QString::number(m_digitalDelay * 0.3432, 'f', 2) + "m" },
		{ "Preset/SourceRouter/AnalogDelayAmount",	QString::number(m_analogDelay, 10) + "ms/" + QString::number(m_analogDelay * 1.127, 'f', 2) + "ft/" +  QString::number(m_analogDelay * 0.3432, 'f', 2) + "m" },
		{ "Preset/SourceRouter/MicDelayAmount",		QString::number(m_micDelay, 10) + "ms/" + QString::number(m_micDelay * 1.127, 'f', 2) + "ft/" +  QString::number(m_micDelay * 0.3432, 'f', 2) + "m" },
		{ "Preset/SourceRouter/MusicDelayAmount",	QString::number(m_musicDelay, 10) + "ms/" + QString::number(m_musicDelay * 1.127, 'f', 2) + "ft/" +  QString::number(m_musicDelay * 0.3432, 'f', 2) + "m" },

		{ "Preset/SourceRouter/MicChannelAssign",	getTableString(g_micChannelTable, m_micChannel)},
		{ "Preset/SourceRouter/MusicChannelAssign", QString::number(m_musicChannel, 10) }, 
		{ "Preset/SourceRouter/PhantomPower",		getTableString(g_boolTable, m_phantomEnable)},
		{ "Preset/SourceRouter/DigitalChannel",		QString("%1 %2 %3 %4 %5 %6 %7 %8").arg(m_DigitalChannel[0]).arg(m_DigitalChannel[1]).arg(m_DigitalChannel[2]).arg(m_DigitalChannel[3]).arg(m_DigitalChannel[4]).arg(m_DigitalChannel[5]).arg(m_DigitalChannel[6]).arg(m_DigitalChannel[7]) },
		{ "Preset/SourceRouter/AnalogChannel",		QString("%1 %2 %3 %4 %5 %6 %7 %8").arg(m_AnalogChannel[0]).arg(m_AnalogChannel[1]).arg(m_AnalogChannel[2]).arg(m_AnalogChannel[3]).arg(m_AnalogChannel[4]).arg(m_AnalogChannel[5]).arg(m_AnalogChannel[6]).arg(m_AnalogChannel[7]) },

		/* For TAB3: Output Setting - Rool Level */
		{ "Preset/InputGains/Channel_1_Mute",	getTableString(g_boolTable, m_pOutputChannel[0].m_roomLevelMute)},
		{ "Preset/InputGains/Channel_2_Mute",	getTableString(g_boolTable, m_pOutputChannel[1].m_roomLevelMute)},
		{ "Preset/InputGains/Channel_3_Mute",	getTableString(g_boolTable, m_pOutputChannel[2].m_roomLevelMute)},
		{ "Preset/InputGains/Channel_4_Mute",	getTableString(g_boolTable, m_pOutputChannel[3].m_roomLevelMute)},
		{ "Preset/InputGains/Channel_5_Mute",	getTableString(g_boolTable, m_pOutputChannel[4].m_roomLevelMute)},
		{ "Preset/InputGains/Channel_6_Mute",	getTableString(g_boolTable, m_pOutputChannel[5].m_roomLevelMute)},
		{ "Preset/InputGains/Channel_7_Mute",	getTableString(g_boolTable, m_pOutputChannel[6].m_roomLevelMute)},
		{ "Preset/InputGains/Channel_8_Mute",	getTableString(g_boolTable, m_pOutputChannel[7].m_roomLevelMute)},

		{ "Preset/InputGains/Channel_1_Gain",	QString::number(m_pOutputChannel[0].m_roomLevelGain, 'f', 1) + QString("dB")},
		{ "Preset/InputGains/Channel_2_Gain",	QString::number(m_pOutputChannel[1].m_roomLevelGain, 'f', 1) + QString("dB")},
		{ "Preset/InputGains/Channel_3_Gain",	QString::number(m_pOutputChannel[2].m_roomLevelGain, 'f', 1) + QString("dB")},
		{ "Preset/InputGains/Channel_4_Gain",	QString::number(m_pOutputChannel[3].m_roomLevelGain, 'f', 1) + QString("dB")},
		{ "Preset/InputGains/Channel_5_Gain",	QString::number(m_pOutputChannel[4].m_roomLevelGain, 'f', 1) + QString("dB")},
		{ "Preset/InputGains/Channel_6_Gain",	QString::number(m_pOutputChannel[5].m_roomLevelGain, 'f', 1) + QString("dB")},
		{ "Preset/InputGains/Channel_7_Gain",	QString::number(m_pOutputChannel[6].m_roomLevelGain, 'f', 1) + QString("dB")},
		{ "Preset/InputGains/Channel_8_Gain",	QString::number(m_pOutputChannel[7].m_roomLevelGain, 'f', 1) + QString("dB")},

		{ "Preset/InputGains/Channel_4_Polarity",	getTableString(g_LFEInverseTable, m_swConfiguration.m_inverseFlag)},	
		{ "Preset/InPeq/Channel_4_Band_1_Q",	QString::number(m_pOutputChannel[3].m_lShelf.getQ(), 'f', 1)},
		{ "Preset/InPeq/Channel_4_Band_2_Q",	QString::number(m_pOutputChannel[3].m_hShelf.getQ(), 'f', 1)},
		{ "Preset/SourceRouter/SignalCenterMode",	getTableString(g_boolTable, m_swConfiguration.m_centerGenFlag)},	
	};

	for (int i = 0; i < sizeof(stringTable) / sizeof(JSON_KEY_STRING); i++)
	{
		modifyJsonValue(loadDoc, stringTable[i].m_strKeyList, stringTable[i].m_valueString);
	}

	/* Let's load GEQ Here */
	{
		QStringList GEQspeakerList;
		GEQspeakerList << "InGeq_L" << "InGeq_R" << "InGeq_C" << "InGeq_LFE" << "InGeq_LS" << "InGeq_RS" << "InGeq_BLS" << "InGeq_BRS";

		QStringList freqList;
		freqList	<< "40 Hz" << "50 Hz" << "63 Hz" 
					<< "80 Hz" << "100 Hz" << "125 Hz" 
					<< "160 Hz" << "200 Hz" << "250 Hz"
					<< "315 Hz" << "400 Hz" << "500 Hz" 
					<< "630 Hz"	<< "800 Hz" << "1.0 kHz" 
					<< "1.25 kHz" << "1.6 kHz" << "2.0 kHz" 
					<< "2.5 kHz" << "3.15 kHz" << "4.0 kHz" 
					<< "5.0 kHz" << "6.3 kHz" << "8.0 kHz"
					<< "10.0 kHz" << "12.5 kHz" << "16.0 kHz";

		/* We will get float value here... */
		for (int speakerIndex = 0; speakerIndex < 8; speakerIndex++)
		{
			QString JSONKey;
			qDebug() << "***************************";

			if (speakerIndex == 3)
			{
				continue;
			}

			/* Get GEQ Value */
			for (int freqIndex = 0; freqIndex < 27; freqIndex++)
			{
				JSONKey = QString("Preset/%1/%2").arg(GEQspeakerList.at(speakerIndex)).arg(freqList.at(freqIndex));
				qDebug() << JSONKey;
				modifyJsonValue(loadDoc, JSONKey, QString::number(getGEQ(speakerIndex, freqIndex), 'f', 1) + QString("dB"));
			}

			/* Get PEQ- Low Shelf Filter Value */
			EQData *pEQ = m_pOutputChannel[speakerIndex].getLShelf();

			JSONKey = QString("Preset/InPeq/Channel_%1_Band_1_Frequency").arg(speakerIndex + 1);
			modifyJsonValue(loadDoc, JSONKey, formatFreq(pEQ->getFrequency()));

			JSONKey = QString("Preset/InPeq/Channel_%1_Band_1_Gain").arg(speakerIndex + 1);
			modifyJsonValue(loadDoc, JSONKey, formatGain(pEQ->getGain()));
		
			JSONKey = QString("Preset/InPeq/Channel_%1_Band_1_Slope").arg(speakerIndex + 1);
			modifyJsonValue(loadDoc, JSONKey, QString::number(pEQ->getSlope(), 'f', 2));

			/* Get PEQ- High Shelf Filter Value */
			pEQ = m_pOutputChannel[speakerIndex].getHShelf();
		
			JSONKey = QString("Preset/InPeq/Channel_%1_Band_2_Frequency").arg(speakerIndex + 1);
			modifyJsonValue(loadDoc, JSONKey, formatFreq(pEQ->getFrequency()));

			JSONKey = QString("Preset/InPeq/Channel_%1_Band_2_Gain").arg(speakerIndex + 1);
			modifyJsonValue(loadDoc, JSONKey, formatGain(pEQ->getGain()));
		
			JSONKey = QString("Preset/InPeq/Channel_%1_Band_2_Slope").arg(speakerIndex + 1);
			modifyJsonValue(loadDoc, JSONKey, QString::number(pEQ->getSlope(), 'f', 2));
		}
	}

	mainObject = loadDoc.object();
	QByteArray byte_array = QJsonDocument(mainObject).toJson();

	qDebug() << QString(byte_array).length();

	bool ret = g_database.setJSON(QString("0"), QString(byte_array));
	if (ret == true)
	{
		QFile::copy(localPresetFileName, presetDBFileName);
	}

	return (ret);
}

void CPi2000Data::setOutputDB(float outputDB[8])
{
	for (int i = 0; i < 8; i++)
	{
		m_outputDB[i] = outputDB[i];
	}
}

void CPi2000Data::setInputDB(float inputDB[8])
{
	m_maxInputDB = -90;
	for (int i = 0; i < 8; i++)
	{
		m_inputDB[i] = inputDB[i];
		if (m_maxInputDB < inputDB[i])
		{
			m_maxInputDB = inputDB[i];
		}
	}
}

void CPi2000Data::clearSpeaker()
{
	speakerList_LCR.clear();
	speakerList_subwoofer.clear();
	speakerList_surround.clear();
}

void CPi2000Data::addSpeaker(QString strSpeakerName, SPEAKER_TYPE speakerType)
{
	if ((speakerType == SPEAKER_TYPE_PASSIVE) || (speakerType == SPEAKER_TYPE_BIAMP) || (speakerType == SPEAKER_TYPE_TRIAMP))
	{
		speakerList_LCR << strSpeakerName;
	}
	else if (speakerType == SPEAKER_TYPE_SURROUND)
	{
		speakerList_surround << strSpeakerName;	
	}
	else if (speakerType == SPEAKER_TYPE_SUBWOOFER)
	{
		speakerList_subwoofer << strSpeakerName;	
	}
}

QStringList CPi2000Data::getAvailableSpeakerName(SPEAKER_CHANNEL speakerType)
{
	switch (speakerType)
	{
	case SPEAKER_CHANNEL_L:
	case SPEAKER_CHANNEL_R:
	case SPEAKER_CHANNEL_CENTER:
		return (speakerList_LCR);
	case SPEAKER_CHANNEL_LS:
	case SPEAKER_CHANNEL_RS:
	case SPEAKER_CHANNEL_BLS:
	case SPEAKER_CHANNEL_BRS:
		return (speakerList_surround);
	case SPEAKER_CHANNEL_SW:
		return (speakerList_subwoofer);
	default:
		return (QStringList());
	}
}

void CPi2000Data::setAnalogChannel(int channel[8])
{
	for (int i = 0; i < 8; i++)
	{
		m_AnalogChannel[i] = channel[i];
	}
}

void CPi2000Data::setDigitalChannel(int channel[8])
{
	for (int i = 0; i < 8; i++)
	{
		m_DigitalChannel[i] = channel[i];
	}
}

QString formatFreq(qreal freq)
{
	if (freq < 1000)
	{
		return (QString("%1Hz").arg((int)freq));
	}
	else
	{
		return (QString::number(freq / 1000, 'f', 2) + QString("kHz"));
	}
}

QString formatGain(qreal gain)
{
	return (QString::number(gain, 'f', 1) + QString("dB"));
}