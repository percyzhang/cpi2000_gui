#include "DeviceSocket.h"
#include <qdebug.h>
#include "mainApplication.h"
#include "mainWidget.h"
#include "CPi2000Setting.h"
#include "commonLib.h"
#include "simpleQtLogger.h"

#define LOCAL_PORT	10050
#define DEVICE_PORT	19272

#define SEND_INTERVAL_IN_MS  500

DeviceSocket::DeviceSocket(QHostAddress localAddress, QHostAddress deviceAddress)
    : QObject(nullptr)
{
    m_pSocket = new QTcpSocket(this);
	m_localAddress = localAddress;
	m_deviceAddress = deviceAddress;
	m_bConnectedFlag = false;
	m_pTextStream = nullptr;

	m_recvFlag = false;
	timerLoad(&m_sendBufferTimer);
}

DeviceSocket::~DeviceSocket()
{
    m_pSocket->disconnect();
	if (m_bConnectedFlag)
	{
		m_pSocket->disconnectFromHost();
		delete m_pTextStream;
	}
	delete m_pSocket;
}

QHostAddress DeviceSocket::getLocalAddr()
{
	return (m_localAddress);
}

QHostAddress DeviceSocket::getDeviceAddr()
{
	return (m_deviceAddress);
}

bool DeviceSocket::setConnection()
{
	int portStep = g_CPi2000Setting.value("currentSocketStep", 0).toInt();

	int count;
	for (count = 0; count < 50; count++)
	{
		portStep++;
		if (portStep > 5000)
		{
			portStep = 0;
		}
		g_CPi2000Setting.setValue("currentSocketStep", portStep);

		if (m_pSocket->bind(m_localAddress, LOCAL_PORT + portStep))
		{
			break;
		}
	}
	if (count == 50)
	{
		L_ERROR("Bind local port failed!");
		return (false);
	}

    // this is not blocking call
    m_pSocket->connectToHost(m_deviceAddress, DEVICE_PORT);
	L_INFO("SOCKET CONNECTING... " + m_deviceAddress.toString());

    // we need to wait...
    if(!m_pSocket->waitForConnected(3000))
    {
		L_WARN("Socket failed to connected to host:" + m_deviceAddress.toString());
		return (false);
    }

    connect(m_pSocket, SIGNAL(readyRead()),this, SLOT(readyRead()));
	m_bConnectedFlag = true;
	m_pTextStream = new QTextStream(m_pSocket);

	/* wait for connecttion as administrator */
	m_commandFlag[CMD_WAIT_LOGIN] = true;
	for (int i = 0; i < 200; i++)
	{
		g_pApp->msleep(10);
		if (m_commandFlag[CMD_WAIT_LOGIN] == false)
		{
			return (true);
		}
	}
	L_WARN(QString("Failed to login SOCKET %1 as administrator.").arg(m_deviceAddress.toString()));
	return (false);
}

void DeviceSocket::readyRead()
{
    while(m_pTextStream->device()->canReadLine())
    {
        QString dataFromTCP = m_pTextStream->readLine();
		onDispatchData(dataFromTCP);

		m_recvFlag = true;
    }
}

typedef void ( *DispatchCrossRoadConsoleFunc)(DeviceSocket *m_pSocket, QStringList cmdList); 

typedef struct
{
	QString							m_strParam0;
	QString							m_strParam1;
	int								m_commandType;
	DispatchCrossRoadConsoleFunc	m_pFunc;
} DISPATCH_FUNC_PARAM3;

void onRecvOutputVolume(DeviceSocket *m_pSocket, QStringList strList);
void onRecvInputVolume(DeviceSocket *m_pSocket, QStringList strList);
//void onRecvMasterVolume(DeviceSocket *m_pSocket, QStringList strList);
void onRecvUSBStatus(DeviceSocket *m_pSocket, QStringList strList);
void onGetSystemTime(DeviceSocket *m_pSocket, QStringList strList);
void onRecvSPLValue(DeviceSocket *m_pSocket, QStringList strList);
void onRecvRTAValue(DeviceSocket *m_pSocket, QStringList strList);



DISPATCH_FUNC_PARAM3 s_param3[] = {
	{ "",		"",						CMD_RESERVED,				nullptr				},
	{ "get",	NODE_THEATER_NAME,		CMD_GET_THEATER_NAME,		nullptr				},
	{ "get",	NODE_HALL_NAME,			CMD_GET_HALL_NAME,			nullptr				},
	{ "get",	NODE_NETWORK_INFO,		CMD_GET_NETWORK_INFO,		nullptr				},
	{ "set",	NODE_OUTPUT_VOLUME,		CMD_NULL,					onRecvOutputVolume	},
	{ "subr",	NODE_OUTPUT_VOLUME,		CMD_NULL,					onRecvOutputVolume	},
	{ "set",	NODE_INPUT_VOLUME,		CMD_NULL,					onRecvInputVolume	},
	{ "subr",	NODE_INPUT_VOLUME,		CMD_NULL,					onRecvInputVolume	},
	{ "set",	NODE_USB_STATUS,		CMD_NULL,					onRecvUSBStatus		},
	{ "subr",	NODE_USB_STATUS,		CMD_NULL,					onRecvUSBStatus		},
	{ "get",	NODE_USB_FILE_NUMBER,	CMD_GET_USB_FILE_NUMBER,	nullptr				},
	{ "get",	NODE_USB_FILE_NAME,		CMD_GET_USB_FILE_NAME,		nullptr				},
	{ "get",	NODE_USB_NEW_FILE_NAME,	CMD_GET_USB_NEW_FILE_NAME,	nullptr				},

	{ "get",	NODE_SPL_VALUE,			CMD_NULL,					onRecvSPLValue		},

	{ "get",	NODE_DEVICE_FIRMWARE_VERSION,	CMD_GET_DEVICE_FIRMWARE_VERSION,	nullptr				},
	{ "get",	NODE_FT_FIRMWARE_VERSION,		CMD_GET_FT_FIRMWARE_VERSION,		nullptr				},
	{ "get",	NODE_A5_MODEL_VERSION,			CMD_GET_A5_MODEL_VERSION,			nullptr				},
	{ "get",	NODE_A5_FIRMWARE_VERSION,		CMD_GET_A5_FIRMWARE_VERSION,		nullptr				},
	{ "get",	NODE_DSP_FIRMWARE_VERSION,		CMD_GET_DSP_FIRMWARE_VERSION,		nullptr				},
	{ "get",	NODE_SERIAL_NUMBER,				CMD_GET_SERIAL_NUMBER,				nullptr				},
	{ "get",	NODE_MASTER_VOLUME,				CMD_GET_MASTER_VOLUME,				nullptr				},
	{ "get",	NODE_SYSTEM_TIME,				CMD_NULL,							onGetSystemTime		},

	{ "set",	NODE_RTA_METER,					CMD_NULL,			onRecvRTAValue		}

};

QStringList splitCommand(QString strLine)
{
	QString line = QString(" ") + strLine;
	QStringList list;

	int pos = 0;
	QRegExp rx("^\\s+([^\"\\s]+)|\\s+\"([^\"]*)\"");
	QRegExp rx1("^\\s+([^\"\\s]+)");
	QRegExp rx2("^\\s+\"([^\"]*)\"");
	while ((pos = rx.indexIn(line, pos)) != -1) 
	{
		QString subString = rx.cap(0);
//		qDebug() << "subString" << subString;
		QString matchString;
		if (rx1.indexIn(subString, 0) != -1)
		{
			matchString = rx1.cap(1);
		}
		else if (rx2.indexIn(subString, 0) != -1)
		{
			matchString = rx2.cap(1);
		}
		else
		{
			L_ERROR("splitCommand Error: " + strLine);
		}

		list << matchString;
//		qDebug() << matchString;

		pos += rx.matchedLength();
	}
	return (list);
}

void DeviceSocket::onDispatchData(QString strLine)
{
#ifdef _SOCKET_DEBUG
    qDebug() << "Recv data from Socket: << " << strLine.toStdString().c_str();
#endif

	/* Add shake hand here */

	if (BIT(g_debug, LOG_SOCKET_MESSAGE))
	{
		L_DEBUG("Recv from device " + getDeviceAddr().toString() + " :" + strLine);
	}

	if(strLine == "HiQnet Console")
    {
        QString command = QString("connect administrator administrator\n");
        writeToSocket(command.toStdString().c_str());
		return;
    }
	else if (strLine == "connect logged in as administrator")
	{
		m_commandFlag[CMD_WAIT_LOGIN] = false;
		return;
	}

	QStringList cmdList = splitCommand(strLine);

	/* Dispatch "error" info */
	if (cmdList.length() >= 1)
	{
		QString cmd = cmdList[0];
		if (cmd == QString("error"))
		{
			L_WARN(QString("Recv error information from device %1: %2").arg(m_deviceAddress.toString()).arg(strLine));

			if (cmdList.length() == 2)
			{
				for (int i = 0; i < sizeof(s_param3) / sizeof(DISPATCH_FUNC_PARAM3); i++)
				{
					int cmdType = s_param3[i].m_commandType;
					if ((cmdList[1].trimmed() == s_param3[i].m_strParam1) && (cmdType > 0) && (cmdType < CMD_TOTAL_COUNT))
					{
						m_commandFlag[cmdType] = false;
						m_strCommandList[cmdType].clear();
						L_WARN("Error at: " +  s_param3[i].m_strParam1);
						return;
					}
				}
			}
		}
	}

	if (cmdList.length() == 3)
	{
		QString param1 = cmdList[1];
#ifdef _SOCKET_DEBUG
		qDebug() << "Parameter Number 2 " << cmdList[0] << ", " << param1.trimmed();
#endif
//		qDebug(param2.trimmed();

		for (int i = 0; i < sizeof(s_param3) / sizeof(DISPATCH_FUNC_PARAM3); i++)
		{
//			qDebug() << cmdList[0] << ", " << param1.trimmed();
//			qDebug() << s_param3[i].m_strParam0 << ", " << s_param3[i].m_strParam1;

			if ((cmdList[0] == s_param3[i].m_strParam0) && (param1.trimmed() == s_param3[i].m_strParam1))
			{
				int cmdType = s_param3[i].m_commandType;

				if ((cmdType > 0) && (cmdType < CMD_TOTAL_COUNT))
				{
					m_commandFlag[cmdType] = false;
					m_strCommandList[cmdType] = cmdList;
				}
				else if ((cmdType == CMD_NULL) && (s_param3[i].m_pFunc != nullptr))
				{
					s_param3[i].m_pFunc(this, cmdList);
				}
				return;
			}
		}
	}
	else
	{
//		QString cmd = cmdList[1];
		qDebug() << "********************************** Unknown Command ***********************************";
		qDebug() << strLine;
		qDebug() <<"";
	}
}

bool DeviceSocket::getTheaterName(QString & theaterName, int timeoutInSecond)
{
    QString command = QString("get \"%1\"\n").arg(NODE_THEATER_NAME);

	m_commandFlag[CMD_GET_THEATER_NAME] = true;
	writeToSocket(command.toStdString().c_str());

	for (int i = 0; i < timeoutInSecond * 100; i++)
	{
		g_pApp->msleep(10);
		if (m_commandFlag[CMD_GET_THEATER_NAME] == false)
		{
			QStringList cmdList = m_strCommandList[CMD_GET_THEATER_NAME];
			if (cmdList.length() != 3)
			{
				return (false);
			}
			theaterName = cmdList[2];
			if (theaterName == QString("NULL"))
			{
				theaterName = "UNKNOWN";
			}
			return (true);
		}
	}
	return (false);
}


bool DeviceSocket::getHallName(QString & hallName, int timeoutInSecond)
{
    QString command = QString("get \"%1\"\n").arg(NODE_HALL_NAME);
	m_commandFlag[CMD_GET_HALL_NAME] = true;
	writeToSocket(command.toStdString().c_str());

	for (int i = 0; i < timeoutInSecond * 100; i++)
	{
		g_pApp->msleep(10);
		if (m_commandFlag[CMD_GET_HALL_NAME] == false)
		{
			QStringList cmdList = m_strCommandList[CMD_GET_HALL_NAME];
			if (cmdList.length() != 3)
			{
				return (false);
			}
			hallName = cmdList[2];
			if (hallName == QString("NULL"))
			{
				hallName = "UNKNOWN";
			}
			return (true);
		}
	}
	return (false);
}

bool DeviceSocket::getDeviceFWVersion(QString & version, int timeoutInSecond)
{
    QString command = QString("get \"%1\"\n").arg(NODE_DEVICE_FIRMWARE_VERSION);
	m_commandFlag[CMD_GET_DEVICE_FIRMWARE_VERSION] = true;
	writeToSocket(command.toStdString().c_str());

	for (int i = 0; i < timeoutInSecond * 100; i++)
	{
		g_pApp->msleep(10);
		if (m_commandFlag[CMD_GET_DEVICE_FIRMWARE_VERSION] == false)
		{
			QStringList cmdList = m_strCommandList[CMD_GET_DEVICE_FIRMWARE_VERSION];
			if (cmdList.length() != 3)
			{
				return (false);
			}
			version = cmdList[2];
			return (true);
		}
	}
	return (false);
}

bool DeviceSocket::getDSPFWVersion(QString & version, int timeoutInSecond)
{
    QString command = QString("get \"%1\"\n").arg(NODE_DSP_FIRMWARE_VERSION);
	m_commandFlag[CMD_GET_DSP_FIRMWARE_VERSION] = true;
	writeToSocket(command.toStdString().c_str());

	for (int i = 0; i < timeoutInSecond * 100; i++)
	{
		g_pApp->msleep(10);
		if (m_commandFlag[CMD_GET_DSP_FIRMWARE_VERSION] == false)
		{
			QStringList cmdList = m_strCommandList[CMD_GET_DSP_FIRMWARE_VERSION];
			if (cmdList.length() != 3)
			{
				return (false);
			}

			version = cmdList[2];
			return (true);
		}
	}
	return (false);
}

bool DeviceSocket::getFrontPanelFWVersion(QString & version, int timeoutInSecond)
{
    QString command = QString("get \"%1\"\n").arg(NODE_FT_FIRMWARE_VERSION);
	m_commandFlag[CMD_GET_FT_FIRMWARE_VERSION] = true;
	writeToSocket(command.toStdString().c_str());

	for (int i = 0; i < timeoutInSecond * 100; i++)
	{
		g_pApp->msleep(10);
		if (m_commandFlag[CMD_GET_FT_FIRMWARE_VERSION] == false)
		{
			QStringList cmdList = m_strCommandList[CMD_GET_FT_FIRMWARE_VERSION];
			if (cmdList.length() != 3)
			{
				return (false);
			}

			version = cmdList[2];
			return (true);
		}
	}
	return (false);
}

bool DeviceSocket::getA5FWVersion(QString & version, int timeoutInSecond)
{
    QString command = QString("get \"%1\"\n").arg(NODE_A5_FIRMWARE_VERSION);
	m_commandFlag[CMD_GET_A5_FIRMWARE_VERSION] = true;
	writeToSocket(command.toStdString().c_str());

	for (int i = 0; i < timeoutInSecond * 100; i++)
	{
		g_pApp->msleep(10);
		if (m_commandFlag[CMD_GET_A5_FIRMWARE_VERSION] == false)
		{
			QStringList cmdList = m_strCommandList[CMD_GET_A5_FIRMWARE_VERSION];
			if (cmdList.length() != 3)
			{
				return (false);
			}
			version = cmdList[2];
			return (true);
		}
	}
	return (false);
}

bool DeviceSocket::getA5ModelVersion(QString & version, int timeoutInSecond)
{
    QString command = QString("get \"%1\"\n").arg(NODE_A5_MODEL_VERSION);
	m_commandFlag[CMD_GET_A5_MODEL_VERSION] = true;
	writeToSocket(command.toStdString().c_str());

	for (int i = 0; i < timeoutInSecond * 100; i++)
	{
		g_pApp->msleep(10);
		if (m_commandFlag[CMD_GET_A5_MODEL_VERSION] == false)
		{
			QStringList cmdList = m_strCommandList[CMD_GET_A5_MODEL_VERSION];
			if (cmdList.length() != 3)
			{
				return (false);
			}
			version = cmdList[2];
			return (true);
		}
	}
	return (false);
}

#if 0
void onRecvMasterVolume(DeviceSocket *m_pSocket, QStringList strList)
{
	QString outputVolume = strList[2];
	g_pApp->onRecvMasterVolume(outputVolume);
}
#endif

void onRecvOutputVolume(DeviceSocket * /*m_pSocket */, QStringList strList)
{
	QString outputVolume = strList[2];
	float outputDB[8];

//	qDebug() << outputVolume;
	QStringList paramList = outputVolume.split(" ");
	if (paramList.count() != 8)
	{
		return;
	}
	for (int i = 0; i < 8; i++)
	{
		bool ok;
		float dbValue = paramList[i].toFloat(&ok);
		if (ok == true)
		{
			outputDB[i] = dbValue;		
		}
		else 
		{
			return;
		}
	}

	CPi2000Data *pData = getCPi2000Data();
	pData->setOutputDB(outputDB);

	g_pMainWidget->refreshOutputVolume();
}

void onRecvUSBStatus(DeviceSocket * /*m_pSocket*/, QStringList strList)
{
	QString inputVolume = strList[2];

	int status = inputVolume.toInt();
	CPi2000Data *pData = getCPi2000Data();

	pData->setUSBStatus((bool)status);
	qDebug() << "USB Connect Flag = " << inputVolume << " = " << pData->getUSBStatus(); 

	g_pMainWidget->refreshUSBFileMenu();
}

void onRecvSPLValue(DeviceSocket * /*pSocket */, QStringList strList)
{
	QString inputVolume = strList[2];

	float splValue = readFloat(inputVolume);
	CPi2000Data *pData = getCPi2000Data();
	RTAData *pRTA = pData->getRTA();

	pRTA->setSPLValue(splValue);
	emit g_pApp->setSPLValue();
}

#define MAX_RTA_SAMPLE 5
static float s_oldRTAValue[MAX_RTA_SAMPLE][30];
static int s_RTAIndex = -1;  /* Normally it should be from 0 to MAX_RTA_SAMPLE - 1 */

void onRecvRTAValue(DeviceSocket * /*pSocket*/, QStringList strList)
{
	int i, index;

	QString inputVolume = strList[2];

	QStringList meterList = inputVolume.split(" ", QString::SkipEmptyParts);

	CPi2000Data *pData = getCPi2000Data();
	RTAData *pRTA = pData->getRTA();

	float rtaValue[30];
	for (i = 0; i < 30; i++)
	{
		rtaValue[i] = meterList.at(i).toFloat();
	}

	if (s_RTAIndex == -1)
	{
		/* init the old value */
		s_RTAIndex = 0;
		for (index = 0; index < MAX_RTA_SAMPLE; index++)
		{
			for (i = 0; i < 30; i++)
			{
				s_oldRTAValue[index][i] = -90;
			}
		}
	}

	for (i = 0; i < 30; i++)
	{
		s_oldRTAValue[s_RTAIndex][i] = rtaValue[i];
		float totalCount = 0.0;
		for (index = 0; index < MAX_RTA_SAMPLE; index++)
		{
			totalCount += s_oldRTAValue[index][i];
		}
		rtaValue[i] = totalCount / MAX_RTA_SAMPLE;
	}
	s_RTAIndex++;
	if (s_RTAIndex >= MAX_RTA_SAMPLE)
	{
		s_RTAIndex = 0;
	}
	pRTA->setRTAValue(rtaValue);
	emit g_pApp->setRTAValue();
}
void onRecvInputVolume(DeviceSocket * /*m_pSocket*/, QStringList strList)
{
	QString inputVolume = strList[2];
	float inputDB[8];

	qDebug() << inputVolume;
	QStringList paramList = inputVolume.split(" ");
	if (paramList.count() != 8)
	{
		return;
	}
	for (int i = 0; i < 8; i++)
	{
		bool ok;
		float dbValue = paramList[i].toFloat(&ok);
		if (ok == true)
		{
			inputDB[i] = dbValue;		
		}
		else 
		{
			return;
		}
	}

	CPi2000Data *pData = getCPi2000Data();
	pData->setInputDB(inputDB);
	g_pMainWidget->refreshInputVolume();
}

void DeviceSocket::setOuputChannalType(OUTPUT_CHANNEL_TYPE channelType)
{
	int type = (int)channelType;

    QString command = QString("set \"%1\" %2\n").arg(NODE_OUTPUT_CHANNEL_TYPE).arg(type);
//	m_bWaitTheaterName = true;
	writeToSocket(command.toStdString().c_str());
	qDebug() << command.toStdString().c_str();
}

/* Set theater Name */
void DeviceSocket::setTheaterName(QString theaterName)
{
    QString command = QString("set \"%1\" \"%2\"\n").arg(NODE_THEATER_NAME).arg(theaterName);
//	m_bWaitTheaterName = true;
	writeToSocket(command.toStdString().c_str());
	qDebug() << command.toStdString().c_str();
}

bool DeviceSocket::getDeviceNetworkAddr(NETWORK_SETTING &networkSetting, int timeoutInSecond)
{
	bool ret = false;
    QString command = QString("get \"%1\"\n").arg(NODE_NETWORK_INFO);
	m_commandFlag[CMD_GET_NETWORK_INFO] = true;
	writeToSocket(command.toStdString().c_str());

	for (int i = 0; i < timeoutInSecond * 100; i++)
	{
		g_pApp->msleep(10);
		if (m_commandFlag[CMD_GET_NETWORK_INFO] == false)
		{
			QStringList cmdList = m_strCommandList[CMD_GET_NETWORK_INFO];
			if (cmdList.length() != 3)
			{
				return (false);
			}

			QString response = cmdList[2];
			//qDebug() << cmdList[2];
			//{'Gateway':'192.168.1.1','IPAddress':'192.168.1.107','IPType':'AutoIP','MAC':'d2:04:7e:5f:ec:b0','NodeAddress':'1','SubnetMask':'255.255.255.0'}

				int pos = 0;
			//	QRegExp rx("\s+([^\"\s]+)|\s+\"(.*)\"" );
				QRegExp rx("'([^']+)'");
				QStringList wordList;
				while ((pos = rx.indexIn(response, pos)) != -1) 
				{
					QString matchString = rx.cap(1);
					qDebug() << matchString;
					wordList << matchString;
					pos += rx.matchedLength();
				}

				for (int i = 0; i < wordList.length(); i+=2)
				{
					QString key = wordList.at(i);
					if (key == QString("Gateway"))
					{
						networkSetting.m_ipGateway = wordList.at(i + 1);
					}
					else if (key == QString("IPAddress"))
					{
						networkSetting.m_ipAddress = wordList.at(i + 1);
					}
					else if (key == QString("SubnetMask"))
					{
						networkSetting.m_networkMask = wordList.at(i + 1);
					}
					else if (key == QString("IPType"))
					{
						QString value = wordList.at(i + 1);
						if (value == "AutoIP")
						{
							ret = true;
							networkSetting.m_dhcpEnable = true;
						}
						else if (value == "Fixed")
						{
							ret = true;
							networkSetting.m_dhcpEnable = false;
						}
					}

				}
			return (ret);
		}
	}
	return (ret);
}

/* set IP Address */
void DeviceSocket::setDeviceNetworkAddress(NETWORK_SETTING setting)
{
	QString ipType;
	if (setting.m_dhcpEnable == true)
	{
		ipType = QString("AutoIP");
	}
	else
	{
		ipType = QString("Fixed");
	}
    QString command = QString("set \"%1\" \"{'NodeAddress':'1','IPType':'%2','IPAddress':'%3','SubnetMask':'%4','Gateway':'%5'}\"")
		.arg(NODE_NETWORK_INFO).arg(ipType).arg(setting.m_ipAddress.toString()).arg(setting.m_networkMask.toString()).arg(setting.m_ipGateway.toString());

	writeToSocket(command.toStdString().c_str());
	qDebug() << command.toStdString().c_str();
}

/* set hall name */
void DeviceSocket::setHallName(QString hallName)
{
    QString command = QString("set \"%1\" \"%2\"\n").arg(NODE_HALL_NAME).arg(hallName);
//	m_bWaitTheaterName = true;
	writeToSocket(command.toStdString().c_str());
	qDebug() << command.toStdString().c_str();
}

/* set master volume 
	volume range from 0 - 10, not dB 
*/
void DeviceSocket::setMasterVolume(float volume)
{
	float dbValue = masterVolumeToDB(volume);
#if 0
    QString command = QString("set \"%1\" \"%2\"\n").arg(NODE_MASTER_VOLUME).arg(dbValue);
//	m_bWaitTheaterName = true;
	writeToSocket(command.toStdString().c_str());
	qDebug() << command.toStdString().c_str();
#else
	writeToBuffer(QString(NODE_MASTER_VOLUME), QString::number(dbValue, 'f', 1));
#endif
}

void DeviceSocket::setMasterVolumePreset(float volume)
{
	float dbValue = masterVolumeToDB(volume);
#if 0
    QString command = QString("set \"%1\" \"%2\"\n").arg(NODE_MASTER_VOLUME).arg(dbValue);
//	m_bWaitTheaterName = true;
	writeToSocket(command.toStdString().c_str());
	qDebug() << command.toStdString().c_str();
#else
	writeToBuffer(QString(NODE_MASTER_VOLUME_PRESET), QString::number(dbValue, 'f', 1));
#endif
}

#if 0
void DeviceSocket::setMasterOutputMuteFlag(bool flag)
{
    QString command = QString("set \"%1\" \"%2\"\n").arg(NODE_MASTER_MUTE).arg((int)flag);
//	m_bWaitTheaterName = true;
	writeToSocket(command.toStdString().c_str());
	qDebug() << command.toStdString().c_str();
}
#endif

void DeviceSocket::setFadeInOutMuteFlag(bool flag)
{
    QString command = QString("set \"%1\" \"%2\"\n").arg(NODE_FADE_INOUT_MUTE).arg((int)flag);
//	m_bWaitTheaterName = true;
	writeToSocket(command.toStdString().c_str());
	qDebug() << command.toStdString().c_str();
}

void DeviceSocket::setInputSource(INPUT_SOURCE inputSource)
{
    QString command = QString("set \"%1\" \"%2\"\n").arg(NODE_INPUT_SOURCE).arg((int)inputSource);
	writeToSocket(command.toStdString().c_str());
	qDebug() << command.toStdString().c_str();
}

void DeviceSocket::setAnalogChannel(int channel[8])
{
    QString command = QString("set \"%1\" \"%2 %3 %4 %5 %6 %7 %8 %9\"\n").arg(NODE_ANALOG_CHANNEL).arg(channel[0]).
		arg(channel[1]).arg(channel[2]).arg(channel[3]).arg(channel[4]).arg(channel[5]).arg(channel[6]).arg(channel[7]);
	writeToSocket(command.toStdString().c_str());
	qDebug() << command.toStdString().c_str();
}

void DeviceSocket::setDigitalChannel(int channel[8])
{
    QString command = QString("set \"%1\" \"%2 %3 %4 %5 %6 %7 %8 %9\"\n").arg(NODE_DIGITAL_CHANNEL).arg(channel[0]).
		arg(channel[1]).arg(channel[2]).arg(channel[3]).arg(channel[4]).arg(channel[5]).arg(channel[6]).arg(channel[7]);
	writeToSocket(command.toStdString().c_str());
	qDebug() << command.toStdString().c_str();
}

void DeviceSocket::setDigitalMute(bool flag)
{
    QString command = QString("set \"%1\" \"%2\"\n").arg(NODE_DIGITAL_MUTE).arg((int)flag);
	writeToSocket(command.toStdString().c_str());
	qDebug() << command.toStdString().c_str();
}

void DeviceSocket::setAnalogMute(bool flag)
{
    QString command = QString("set \"%1\" \"%2\"\n").arg(NODE_ANALOG_MUTE).arg((int)flag);
	writeToSocket(command.toStdString().c_str());
	qDebug() << command.toStdString().c_str();
}

void DeviceSocket::setMicMute(bool flag)
{
    QString command = QString("set \"%1\" \"%2\"\n").arg(NODE_MIC_MUTE).arg((int)flag);
	writeToSocket(command.toStdString().c_str());
	qDebug() << command.toStdString().c_str();
}

void DeviceSocket::setMusicMute(bool flag)
{
    QString command = QString("set \"%1\" \"%2\"\n").arg(NODE_MUSIC_MUTE).arg((int)flag);
	writeToSocket(command.toStdString().c_str());
	qDebug() << command.toStdString().c_str();
}

void DeviceSocket::subRTAMeter()
{
//#ifndef CROSSROAD_CONSOLE_DEBUG
//	"get \"\\\\Preset\\PeakMeters\\SV\\OutChArray\\r\"\r\n";
	QString command = QString("sub \"%1\"\r\n").arg(NODE_RTA_METER);
	writeToSocket(command.toStdString().c_str());
	qDebug() << command.toStdString().c_str();
//#endif
}

void DeviceSocket::unsubRTAMeter()
{
	QString command = QString("unsub \"%1\"\r\n").arg(NODE_RTA_METER);
	writeToSocket(command.toStdString().c_str());
	qDebug() << command.toStdString().c_str();
}

/* Sub volume */
void DeviceSocket::subOutputVolume()
{
#ifndef CROSSROAD_CONSOLE_DEBUG
//	"get \"\\\\Preset\\PeakMeters\\SV\\OutChArray\\r\"\r\n";
	QString command = QString("sub \"%1\"\r\n").arg(NODE_OUTPUT_VOLUME);
	writeToSocket(command.toStdString().c_str());
	qDebug() << command.toStdString().c_str();
#endif
}

void DeviceSocket::subUSBStatus()
{
	QString command = QString("sub \"%1\"\r\n").arg(NODE_USB_STATUS);
	writeToSocket(command.toStdString().c_str());
	qDebug() << command.toStdString().c_str();
}

/* Sub input volume */
void DeviceSocket::subInputVolume()
{
#ifndef CROSSROAD_CONSOLE_DEBUG
//	"get \"\\\\Preset\\PeakMeters\\SV\\OutChArray\\r\"\r\n";
	QString command = QString("sub \"%1\"\r\n").arg(NODE_INPUT_VOLUME);
	writeToSocket(command.toStdString().c_str());
	qDebug() << command.toStdString().c_str();
#endif
}

/* Sub input volume */
void DeviceSocket::unsubInputVolume()
{
//	"get \"\\\\Preset\\PeakMeters\\SV\\OutChArray\\r\"\r\n";
    QString command = QString("unsub \"%1\"\r\n").arg(NODE_INPUT_VOLUME);
	writeToSocket(command.toStdString().c_str());
	qDebug() << command.toStdString().c_str();
}

void DeviceSocket::sendBye()
{
    QString command = QString("exit\r\n").arg(NODE_INPUT_VOLUME);
	writeToSocket(command.toStdString().c_str());
	qDebug() << command.toStdString().c_str();
}

void DeviceSocket::sendHeartBeat()
{
#ifndef CROSSROAD_CONSOLE_DEBUG
	static int s_value = 0;

	s_value++;
	if (s_value > 100)
	{
		s_value = 0;
	}
#if 0
    QString command = QString("set \"%1\" \"%2\"\n").arg(NODE_HEART_BEAT).arg(s_value);
	writeToSocket(command.toStdString().c_str());
	qDebug() << command.toStdString().c_str();
#else
	writeToBuffer(QString(NODE_HEART_BEAT), QString::number(s_value)); 
#endif
#endif
}

bool DeviceSocket::getRecvFlag()
{
	bool ret = m_recvFlag;

	m_recvFlag = false;
	return (ret);
}

void DeviceSocket::endHeartBeat()
{
	static int s_value = 0;

	s_value++;
	if (s_value > 100)
	{
		s_value = 0;
	}
    QString command = QString("set \"%1\" \"0\"\n").arg(NODE_PC_CONNECTION);
	writeToSocket(command.toStdString().c_str());
	qDebug() << command.toStdString().c_str();
}


void DeviceSocket::setFadeIn(float value)
{
    QString command = QString("set \"%1\" \"%2\"\n").arg(NODE_FADE_IN).arg(qRound(value * 10) * 100);
//	m_bWaitTheaterName = true;
	writeToSocket(command.toStdString().c_str());
	qDebug() << command.toStdString().c_str();
}

void DeviceSocket::setFadeOut(float value)
{
    QString command = QString("set \"%1\" \"%2\"\n").arg(NODE_FADE_OUT).arg(qRound(value * 10) * 100);
//	m_bWaitTheaterName = true;
	writeToSocket(command.toStdString().c_str());
	qDebug() << command.toStdString().c_str();
}

void DeviceSocket::setDigitalVolume(float volume)
{
//	float dbValue = volumeToDB(volume);

#if 0
    QString command = QString("set \"%1\" \"%2\"\n").arg(NODE_DIGITAL_VOLUME).arg(dbValue);
//	m_bWaitTheaterName = true;
	writeToSocket(command.toStdString().c_str());
	qDebug() << command.toStdString().c_str();
#else
	writeToBuffer(QString(NODE_DIGITAL_VOLUME), QString::number((double)volume, 'f', 1)); 
#endif
}

void DeviceSocket::setAnalogVolume(float volume)
{
//	float dbValue = volumeToDB(volume);
#if 0
    QString command = QString("set \"%1\" \"%2\"\n").arg(NODE_ANALOG_VOLUME).arg(dbValue);
//	m_bWaitTheaterName = true;
	writeToSocket(command.toStdString().c_str());
	qDebug() << command.toStdString().c_str();
#else
	writeToBuffer(QString(NODE_ANALOG_VOLUME), QString::number((double)volume, 'f', 1)); 
#endif
}

void DeviceSocket::setMusicVolume(float volume)
{
//	float dbValue = volumeToDB(volume);
#if 0
	QString command = QString("set \"%1\" \"%2\"\n").arg(NODE_MUSIC_VOLUME).arg(dbValue);
//	m_bWaitTheaterName = true;
	writeToSocket(command.toStdString().c_str());
	qDebug() << command.toStdString().c_str();
#else
	writeToBuffer(QString(NODE_MUSIC_VOLUME), QString::number((double)volume, 'f', 1)); 
#endif
}

void DeviceSocket::setMicVolume(float volume)
{
//	float dbValue = volumeToDB(volume);
#if 0
	QString command = QString("set \"%1\" \"%2\"\n").arg(NODE_MIC_VOLUME).arg(dbValue);
//	m_bWaitTheaterName = true;
	writeToSocket(command.toStdString().c_str());
	qDebug() << command.toStdString().c_str();
#else
	writeToBuffer(QString(NODE_MIC_VOLUME), QString::number((double)volume, 'f', 1)); 
#endif
}

void DeviceSocket::setSurroundDelay(int delayInMS)
{
	writeToBuffer(QString(NODE_SURROUND_DELAY_5), QString("%1ms").arg(delayInMS)); 
	writeToBuffer(QString(NODE_SURROUND_DELAY_6), QString("%1ms").arg(delayInMS));  
	writeToBuffer(QString(NODE_SURROUND_DELAY_7), QString("%1ms").arg(delayInMS));  
	writeToBuffer(QString(NODE_SURROUND_DELAY_8), QString("%1ms").arg(delayInMS));  
}

void DeviceSocket::setLengthDistance(float lengthInMeter)
{
#if 0
    QString command = QString("set \"%1\" \"%2\"\n").arg(NODE_HALL_LENGTH).arg(lengthInMeter);
	writeToSocket(command.toStdString().c_str());
	qDebug() << command.toStdString().c_str();
#else
	writeToBuffer(QString(NODE_HALL_LENGTH), QString::number((double)lengthInMeter, 'f', 1)); 
#endif
}

void DeviceSocket::setWidthDistance(float widthInMeter)
{
#if 0
    QString command = QString("set \"%1\" \"%2\"\n").arg(NODE_HALL_WIDTH).arg(widthInMeter);
	writeToSocket(command.toStdString().c_str());
	qDebug() << command.toStdString().c_str();
#else
	writeToBuffer(QString(NODE_HALL_WIDTH), QString::number((double)widthInMeter, 'f', 1)); 
#endif
}

void DeviceSocket::setLoadDetect(UINT32 detectValue)
{
    QString command = QString("set \"%1\" \"%2\"\n").arg(NODE_LOAD_DETECT).arg(detectValue);
//	m_bWaitTheaterName = true;
	writeToSocket(command.toStdString().c_str());
	qDebug() << command.toStdString().c_str();
}

void DeviceSocket::setMusicDelayEnable(bool flag)
{
    QString command = QString("set \"%1\" \"%2\"\n").arg(NODE_MUSIC_DELAY_ENABLE).arg((int)flag);
//	m_bWaitTheaterName = true;
	writeToSocket(command.toStdString().c_str());
	qDebug() << command.toStdString().c_str();
}

void DeviceSocket::setMicDelayEnable(bool flag)
{
    QString command = QString("set \"%1\" \"%2\"\n").arg(NODE_MIC_DELAY_ENABLE).arg((int)flag);
//	m_bWaitTheaterName = true;
	writeToSocket(command.toStdString().c_str());
	qDebug() << command.toStdString().c_str();
}

void DeviceSocket::setAnalogDelayEnable(bool flag)
{
    QString command = QString("set \"%1\" \"%2\"\n").arg(NODE_ANALOG_DELAY_ENABLE).arg((int)flag);
//	m_bWaitTheaterName = true;
	writeToSocket(command.toStdString().c_str());
	qDebug() << command.toStdString().c_str();
}

void DeviceSocket::setDigitalDelayEnable(bool flag)
{
    QString command = QString("set \"%1\" \"%2\"\n").arg(NODE_DIGITAL_DELAY_ENABLE).arg((int)flag);
//	m_bWaitTheaterName = true;
	writeToSocket(command.toStdString().c_str());
	qDebug() << command.toStdString().c_str();
}

void DeviceSocket::setMusicDelay(float delay)
{
	/* 1ms = 100, tested by Percy */
    QString command = QString("set \"%1\" \"%2\"\n").arg(NODE_MUSIC_DELAY).arg(delay * 100);
//	m_bWaitTheaterName = true;
	writeToSocket(command.toStdString().c_str());
	qDebug() << command.toStdString().c_str();
}

void DeviceSocket::setMicDelay(float delay)
{
	/* 1ms = 100, tested by Percy */
    QString command = QString("set \"%1\" \"%2\"\n").arg(NODE_MIC_DELAY).arg(delay * 100);
//	m_bWaitTheaterName = true;
	writeToSocket(command.toStdString().c_str());
	qDebug() << command.toStdString().c_str();
}

void DeviceSocket::setAnalogDelay(float delay)
{
	/* 1ms = 100, tested by Percy */
    QString command = QString("set \"%1\" \"%2\"\n").arg(NODE_ANALOG_DELAY).arg(delay * 100);
//	m_bWaitTheaterName = true;
	writeToSocket(command.toStdString().c_str());
	qDebug() << command.toStdString().c_str();
}

void DeviceSocket::setDigitalDelay(float delay)
{
	/* 1ms = 100, tested by Percy */
    QString command = QString("set \"%1\" \"%2\"\n").arg(NODE_DIGITAL_DELAY).arg(delay * 100);
//	m_bWaitTheaterName = true;
	writeToSocket(command.toStdString().c_str());
	qDebug() << command.toStdString().c_str();
}

void DeviceSocket::setLFEInverse(bool flag)
{
    QString command = QString("set \"%1\" \"%2\"\n").arg(NODE_LFE_INVERSE).arg((int)flag);
//	m_bWaitTheaterName = true;
	writeToSocket(command.toStdString().c_str());
	qDebug() << command.toStdString().c_str();
}

void DeviceSocket::setPinkNoise(bool channelFlag[8])
{
    QString command = QString("set \"%1\" \"%2 %3 %4 %5 %6 %7 %8 %9\"\n").arg(NODE_PINKNOISE_LIST)
		.arg((int)channelFlag[0]).arg((int)channelFlag[1]).arg((int)channelFlag[2]).arg((int)channelFlag[3])
		.arg((int)channelFlag[4]).arg((int)channelFlag[5]).arg((int)channelFlag[6]).arg((int)channelFlag[7]);

	writeToSocket(command.toStdString().c_str());
	qDebug() << command.toStdString().c_str();
}


void DeviceSocket::setMicChannel(MIC_CHANNEL channel)
{
    QString command = QString("set \"%1\" \"%2\"\n").arg(NODE_MIC_CHANNEL).arg((int)channel);
//	m_bWaitTheaterName = true;
	writeToSocket(command.toStdString().c_str());
	qDebug() << command.toStdString().c_str();
}

void DeviceSocket::setMusicChannel(int channel)
{
    QString command = QString("set \"%1\" \"%2\"\n").arg(NODE_MUSIC_CHANNEL).arg((int)channel);
//	m_bWaitTheaterName = true;
	writeToSocket(command.toStdString().c_str());
	qDebug() << command.toStdString().c_str();
}

void DeviceSocket::enablePhantom(bool flag)
{
    QString command = QString("set \"%1\" \"%2\"\n").arg(NODE_PHANTOM_ENABLE).arg((int)flag);
	writeToSocket(command.toStdString().c_str());
	qDebug() << command.toStdString().c_str();
}

void DeviceSocket::setSpeakerMuteFlag(int index, bool flag)
{
	QStringList speakerMuteList;
	speakerMuteList << NODE_MUTE_SPEAKER_LEFT << NODE_MUTE_SPEAKER_RIGHT << NODE_MUTE_SPEAKER_CENTER << NODE_MUTE_SPEAKER_LFE << 
		NODE_MUTE_SPEAKER_LS << NODE_MUTE_SPEAKER_RS << NODE_MUTE_SPEAKER_BSL << NODE_MUTE_SPEAKER_BSR;

	QString command = QString("set \"%1\" \"%2\"\n").arg(speakerMuteList.at(index)).arg((int)flag);
//	m_bWaitTheaterName = true;
	writeToSocket(command.toStdString().c_str());
	qDebug() << command.toStdString().c_str();
}

void DeviceSocket::setSpeakerVolume(int index, float value)
{
	QStringList speakerVolumeList;
	speakerVolumeList << NODE_VOLUME_SPEAKER_LEFT << NODE_VOLUME_SPEAKER_RIGHT << NODE_VOLUME_SPEAKER_CENTER << NODE_VOLUME_SPEAKER_LFE << 
		NODE_VOLUME_SPEAKER_LS << NODE_VOLUME_SPEAKER_RS << NODE_VOLUME_SPEAKER_BSL << NODE_VOLUME_SPEAKER_BSR;

#if 0
	QString command = QString("set \"%1\" \"%2\"\n").arg(speakerVolumeList.at(index)).arg(value);
//	m_bWaitTheaterName = true;
	writeToSocket(command.toStdString().c_str());
	qDebug() << command.toStdString().c_str();
#else
	writeToBuffer(QString(speakerVolumeList.at(index)), QString::number(value, 'f', 1));
#endif
}

void DeviceSocket::setGEQ(int speakerIndex, int index, float value)
{
	QStringList speakerList;
	speakerList << "InGeq_L" << "InGeq_R" << "InGeq_C" << "InGeq_LFE" << "InGeq_LS" << "InGeq_RS" << "InGeq_BLS" << "InGeq_BRS";

	QStringList freqList;
	freqList	<< "40 Hz" << "50 Hz" << "63 Hz" 
				<< "80 Hz" << "100 Hz" << "125 Hz" 
				<< "160 Hz" << "200 Hz" << "250 Hz"
				<< "315 Hz" << "400 Hz" << "500 Hz" 
				<< "630 Hz"	<< "800 Hz" << "1.0 kHz" 
				<< "1.25 kHz" << "1.6 kHz" << "2.0 kHz" 
				<< "2.5 kHz" << "3.15 kHz" << "4.0 kHz" 
				<< "5.0 kHz" << "6.3 kHz" << "8.0 kHz"
				<< "10.0 kHz" << "12.5 kHz" << "16.0 kHz";

	QString strNode = QString("\\\\Preset\\%1\\SV\\%2\\r").arg(speakerList.at(speakerIndex)).arg(freqList.at(index));
	QString strValue = QString::number(value, 'f', 1);

	writeToBuffer(strNode, strValue);
}


void DeviceSocket::loadSpeakerFile()
{
    QString command[] = {
		QString("{\"method\":\"SaveFile\",\"params\":[\"PC/speaker.db\"],\"id\":\"someID\"}\n"),
	};

	for (int i = 0; i < sizeof(command) / sizeof(QString); i++)
	{
		writeToSocket(command[i].toStdString().c_str());
		qDebug() << command[i].toStdString().c_str();
	}
}

void DeviceSocket::saveSpeakerFile()
{
    QString command[] = {
		QString("{\"method\":\"SaveFile\",\"params\":[\"PC/speaker.db\"],\"id\":\"someID\"}\n"),
	};

	for (int i = 0; i < sizeof(command) / sizeof(QString); i++)
	{
		writeToSocket(command[i].toStdString().c_str());
		qDebug() << command[i].toStdString().c_str();
	}
}



void DeviceSocket::saveDB(void)
{
    QString command[] = {
		QString("{\"method\":\"Store\",\"params\":[\"0\",\"Preset Name\"],\"id\":\"fwID_store1\"}\n"),
		QString("{\"method\":\"GetFile\",\"params\":[\"PC/Presets.db\"],\"id\":\"someID\"}\n"),
		QString("{\"method\":\"GetFile\",\"params\":[\"PC/speaker.db\"],\"id\":\"someID\"}\n"),
	};

	for (int i = 0; i < sizeof(command) / sizeof(QString); i++)
	{
		writeToSocket(command[i].toStdString().c_str());
		qDebug() << command[i].toStdString().c_str();
	}
}

void DeviceSocket::saveLogFile(void)
{
    QString command1 = QString("{\"method\":\"GetFile\",\"params\":[\"PC/mylogfile.1\"],\"id\":\"someID\"}\n");
    QString command2 = QString("{\"method\":\"GetFile\",\"params\":[\"PC/mylogfile\"],\"id\":\"someID\"}\n");

//	m_bWaitTheaterName = true;
	writeToSocket(command1.toStdString().c_str());
	qDebug() << command1.toStdString().c_str();

	writeToSocket(command2.toStdString().c_str());
	qDebug() << command2.toStdString().c_str();
}


void DeviceSocket::setNormalString(QString keyString, QString value)
{
    QString command = QString("set \"%1\" \"%2\"\n").arg(keyString).arg(value);
//	m_bWaitTheaterName = true;
	writeToSocket(command.toStdString().c_str());
	qDebug() << command.toStdString().c_str();
}


void DeviceSocket::setPEQGain(int currentGroupIndex, int currentEQIndex, float value)
{
	float roundValue = float(qRound(10 * value)) / 10;

	int bandIndex[] = {1, 5, 2, 3, 4};
	QString speakerIndex[] = {"InPeq_L", "InPeq_R", "InPeq_C", "InPeq_LFE", "InPeq_LS", "InPeq_RS", "InPeq_BLS", "InPeq_BRS" };
	QString NodePEQGain = QString("\\\\Preset\\%1\\SV\\Band_%2_Gain\\r").arg(speakerIndex[currentGroupIndex]).arg(bandIndex[currentEQIndex]);


	QString command = QString("set \"%1\" \"%2\"\n").arg(NodePEQGain).arg(roundValue);
	writeToSocket(command.toStdString().c_str());
	qDebug() << command.toStdString().c_str();
}

void DeviceSocket::setPEQFrequence(int currentGroupIndex, int currentEQIndex, float value)
{
	float roundValue = float(qRound(10 * value)) / 10;
	int bandIndex[] = {1, 5, 2, 3, 4};
	QString speakerIndex[] = {"InPeq_L", "InPeq_R", "InPeq_C", "InPeq_LFE", "InPeq_LS", "InPeq_RS", "InPeq_BLS", "InPeq_BRS" };
	QString NodePEQGain = QString("\\\\Preset\\%1\\SV\\Band_%2_Frequency\\r").arg(speakerIndex[currentGroupIndex]).arg(bandIndex[currentEQIndex]);

	QString command = QString("set \"%1\" \"%2\"\n").arg(NodePEQGain).arg(roundValue);
	writeToSocket(command.toStdString().c_str());
	qDebug() << command.toStdString().c_str();
}

void DeviceSocket::setPEQSlope(int currentGroupIndex, int currentEQIndex, float value)
{
	int bandIndex[] = {1, 5, 2, 3, 4};
	QString speakerIndex[] = {"InPeq_L", "InPeq_R", "InPeq_C", "InPeq_LFE", "InPeq_LS", "InPeq_RS", "InPeq_BLS", "InPeq_BRS" };
	QString NodePEQGain = QString("\\\\Preset\\%1\\SV\\Band_%2_Slope\\r").arg(speakerIndex[currentGroupIndex]).arg(bandIndex[currentEQIndex]);

	QString command = QString("set \"%1\" \"%2\"\n").arg(NodePEQGain).arg(value);
	writeToSocket(command.toStdString().c_str());
	qDebug() << command.toStdString().c_str();
}

void DeviceSocket::setPEQ_Q(int currentGroupIndex, int currentEQIndex, float value)
{
	int bandIndex[] = {1, 5, 2, 3, 4};
	QString speakerIndex[] = {"InPeq_L", "InPeq_R", "InPeq_C", "InPeq_LFE", "InPeq_LS", "InPeq_RS", "InPeq_BLS", "InPeq_BRS" };
	QString NodePEQGain = QString("\\\\Preset\\%1\\SV\\Band_%2_Q\\r").arg(speakerIndex[currentGroupIndex]).arg(bandIndex[currentEQIndex]);

	QString command = QString("set \"%1\" \"%2\"\n").arg(NodePEQGain).arg(value);
	writeToSocket(command.toStdString().c_str());
	qDebug() << command.toStdString().c_str();
}


void DeviceSocket::setPEQEnable(int currentSpeakerIndex, int m_EQIndex, bool enableflag)
{
	int bandIndex[] = {1, 5, 2, 3, 4};
	QString speakerIndex[] = {"InPeq_L", "InPeq_R", "InPeq_C", "InPeq_LFE", "InPeq_LS", "InPeq_RS", "InPeq_BLS", "InPeq_BRS" };
	QString NodePEQGain = QString("\\\\Preset\\%1\\SV\\Band_%2_Enable\\r").arg(speakerIndex[currentSpeakerIndex]).arg(bandIndex[m_EQIndex]);

	QString command = QString("set \"%1\" \"%2\"\n").arg(NodePEQGain).arg((int)enableflag);
	writeToSocket(command.toStdString().c_str());
	qDebug() << command.toStdString().c_str();
}


void DeviceSocket::setPEQBypassFlag(int currentSpeakerIndex, bool bypassFlag)
{
	QString speakerIndex[] = {"InPeq_L", "InPeq_R", "InPeq_C", "InPeq_LFE", "InPeq_LS", "InPeq_RS", "InPeq_BLS", "InPeq_BRS" };
	QString NodePEQGain = QString("\\\\Preset\\%1\\SV\\ParametricEQ\\r").arg(speakerIndex[currentSpeakerIndex]);

	QString command = QString("set \"%1\" \"%2\"\n").arg(NodePEQGain).arg((int)!bypassFlag);
	writeToSocket(command.toStdString().c_str());
	qDebug() << command.toStdString().c_str();
}

void DeviceSocket::setGEQBypassFlag(int currentSpeakerIndex, bool bypassFlag)
{
	QString speakerIndex[] = {"InGeq_L", "InGeq_R", "InGeq_C", "InGeq_LFE", "InGeq_LS", "InGeq_RS", "InGeq_BLS", "InGeq_BRS" };
	QString NodePEQGain = QString("\\\\Preset\\%1\\SV\\GraphicEQ\\r").arg(speakerIndex[currentSpeakerIndex]);

	QString command = QString("set \"%1\" \"%2\"\n").arg(NodePEQGain).arg((int)!bypassFlag);
	writeToSocket(command.toStdString().c_str());
	qDebug() << command.toStdString().c_str();
}

void DeviceSocket::SaveDeviceFile(QString remoteDeviceFile)
{
	QString command = QString("{\"method\":\"GetDB\",\"params\":[\"PC/%1\"],\"id\":\"someID\"}\n").arg(remoteDeviceFile);
	writeToSocket(command.toStdString().c_str());
	qDebug() << command.toStdString().c_str();
	g_pApp->msleep(100);
}

void DeviceSocket::loadDeviceFile(QString remoteDeviceFile)
{
	QString command = QString("{\"method\":\"LoadDB\",\"params\":[\"PC/%1\"],\"id\":\"someID\"}\n").arg(remoteDeviceFile);
	writeToSocket(command.toStdString().c_str());
	qDebug() << command.toStdString().c_str();
	g_pApp->msleep(100);
}

void DeviceSocket::loadUSBFile(QString usbDeviceFile)
{
	QString command = QString("{\"method\":\"LoadDB\",\"params\":[\"USB/%1\"],\"id\":\"someID\"}\n").arg(usbDeviceFile);
	writeToSocket(command.toStdString().c_str());
	qDebug() << command.toStdString().c_str();
	g_pApp->msleep(100);
}

void DeviceSocket::resetDevice()
{
	QString command = QString("set \\\\Node\\SV\\Reboot Yes\n");
	writeToSocket(command.toStdString().c_str());
	qDebug() << command.toStdString().c_str();
}

void DeviceSocket::restoreFactorySetting()
{
    QString command = QString("{\"method\":\"FactoryRestore\",\"params\":[\"\"],\"id\":\"someID\"}\n");
	writeToSocket(command.toStdString().c_str());
	qDebug() << command.toStdString().c_str();
}

void DeviceSocket::setLogOn()
{
	QString command = "log on\n";	
	writeToSocket(command.toStdString().c_str());
	qDebug() << command.toStdString().c_str();
}

void DeviceSocket::setLogOff()
{
	QString command = "log off\n";	
	writeToSocket(command.toStdString().c_str());
	qDebug() << command.toStdString().c_str();
}

void DeviceSocket::clearLog()
{
	QString command = "log clear\n";	
	writeToSocket(command.toStdString().c_str());
	qDebug() << command.toStdString().c_str();
}

bool DeviceSocket::getUSBFileNumber(int &fileCount)
{
	int timeoutInSecond = 3;
	bool flag = false;

	QString command = QString("get \"%1\"\n").arg(NODE_USB_FILE_NUMBER);
	m_commandFlag[CMD_GET_USB_FILE_NUMBER] = true;
	writeToSocket(command.toStdString().c_str());
	qDebug() << command.toStdString().c_str();

	for (int i = 0; i < timeoutInSecond * 100; i++)
	{
		g_pApp->msleep(10);
		if (m_commandFlag[CMD_GET_USB_FILE_NUMBER] == false)
		{
			QStringList cmdList = m_strCommandList[CMD_GET_USB_FILE_NUMBER];
			if (cmdList.length() != 3)
			{
				return (false);
			}

			fileCount = cmdList[2].toInt(&flag);
			break;
		}
	}
	return (flag);
}

bool DeviceSocket::getUSBFileName(int fileIndex, QString& fileName)
{
	int timeoutInSecond = 3;
	bool flag = false;

	QString command = QString("set \"%1\" \"%2\"\n").arg(NODE_USB_FILE_INDEX).arg(fileIndex);
	m_commandFlag[CMD_GET_USB_FILE_NUMBER] = true;
	writeToSocket(command.toStdString().c_str());
	qDebug() << command.toStdString().c_str();

	g_pApp->msleep(1);

	command = QString("get \"%1\"\n").arg(NODE_USB_FILE_NAME);
	m_commandFlag[CMD_GET_USB_FILE_NAME] = true;
	writeToSocket(command.toStdString().c_str());
	qDebug() << command.toStdString().c_str();

	for (int i = 0; i < timeoutInSecond * 100; i++)
	{
		g_pApp->msleep(10);
		if (m_commandFlag[CMD_GET_USB_FILE_NAME] == false)
		{
			QStringList cmdList = m_strCommandList[CMD_GET_USB_FILE_NAME];
			if (cmdList.length() != 3)
			{
				return (false);
			}

			QString fileField = cmdList[2];
			int index = fileField.indexOf(':');
			fileName = fileField.mid(index + 1, -1);
			qDebug() << fileName;
			flag = true;
			break;
		}
	}
	return (flag);
}

bool DeviceSocket::getUSBFileList(QStringList &fileNameList)
{
//	bool flag = false;
	int fileCount = 0;

	fileNameList.clear();

	if (getUSBFileNumber(fileCount) == false)
	{
		qDebug() << "get USB File number failed.";
		return (false);
	}

	for (int fileIndex = 0; fileIndex < fileCount; fileIndex++)
	{
		QString fileName;
		if (getUSBFileName(fileIndex, fileName) == false)
		{
			qDebug() << "get USB File name failed!, index =" << fileIndex;
			return (false);
		}
		fileNameList << fileName;
	}

	return (true);
}

bool DeviceSocket::getUSBNewFileName(QString& fileName)
{
	int timeoutInSecond = 3;
	bool flag = false;

	QString command = QString("get \"%1\"\n").arg(NODE_USB_NEW_FILE_NAME);
	m_commandFlag[CMD_GET_USB_NEW_FILE_NAME] = true;
	writeToSocket(command.toStdString().c_str());
	qDebug() << command.toStdString().c_str();

	for (int i = 0; i < timeoutInSecond * 100; i++)
	{
		g_pApp->msleep(10);
		if (m_commandFlag[CMD_GET_USB_NEW_FILE_NAME] == false)
		{
			QStringList cmdList = m_strCommandList[CMD_GET_USB_NEW_FILE_NAME];
			if (cmdList.length() != 3)
			{
				return (false);
			}

			fileName = cmdList[2];
			flag = true;
			break;
		}
	}
	return (flag);
}

void DeviceSocket::saveUSBFile(QString usbFile)
{
	QString command = QString("{\"method\":\"GetDB\",\"params\":[\"USB/%1\"],\"id\":\"someID\"}\n").arg(usbFile);
	writeToSocket(command.toStdString().c_str());
	qDebug() << command.toStdString().c_str();
}

bool DeviceSocket::getMasterVolume(float &masterVolume)
{
	int timeoutInSecond = 3;
	bool flag = false;

	QString command = QString("get \"%1\"\n").arg(NODE_MASTER_VOLUME);
	m_commandFlag[CMD_GET_MASTER_VOLUME] = true;
	writeToSocket(command.toStdString().c_str());
	qDebug() << command.toStdString().c_str();

	for (int i = 0; i < timeoutInSecond * 100; i++)
	{
		g_pApp->msleep(10);
		if (m_commandFlag[CMD_GET_MASTER_VOLUME] == false)
		{
			QStringList cmdList = m_strCommandList[CMD_GET_MASTER_VOLUME];
			if (cmdList.length() != 3)
			{
				return (false);
			}

			masterVolume = cmdList[2].toFloat();
			flag = true;
			break;
		}
	}
	return (flag);	
}

bool DeviceSocket::getSerialNumber(QString &serialNumber)
{
	int timeoutInSecond = 3;
	bool flag = false;

	QString command = QString("get \"%1\"\n").arg(NODE_SERIAL_NUMBER);
	m_commandFlag[CMD_GET_SERIAL_NUMBER] = true;
	writeToSocket(command.toStdString().c_str());
	qDebug() << command.toStdString().c_str();

	for (int i = 0; i < timeoutInSecond * 100; i++)
	{
		g_pApp->msleep(10);
		if (m_commandFlag[CMD_GET_SERIAL_NUMBER] == false)
		{
			QStringList cmdList = m_strCommandList[CMD_GET_SERIAL_NUMBER];
			if (cmdList.length() != 3)
			{
				return (false);
			}

			serialNumber = cmdList[2];
			flag = true;
			break;
		}
	}
	return (flag);	
}

void DeviceSocket::getDeviceTime()
{
	QString command = QString("get \"%1\"\n").arg(NODE_SYSTEM_TIME);
	m_commandFlag[CMD_GET_SYSTEM_TIME] = true;
	writeToSocket(command.toStdString().c_str());
	qDebug() << command.toStdString().c_str();
}

void DeviceSocket::setDeviceTime(const QDate &date, const QTime &time)
{
	QString command = QString("set \"%1\" \"%2:%3:%4:%5:%6:%7\"\n").arg(NODE_SYSTEM_TIME).arg(date.year()).arg(date.month()).arg(date.day()).arg(time.hour()).arg(time.minute()).arg(time.second());
	m_commandFlag[CMD_GET_SYSTEM_TIME] = true;
	writeToSocket(command.toStdString().c_str());
	qDebug() << command.toStdString().c_str();
}


void onGetSystemTime(DeviceSocket * /*m_pSocket*/, QStringList strList)
{
	QString systemTime = strList[2];
	QDate deviceDate;
	QTime deviceTime;

	QRegExp rx("(\\d+):(\\d+):(\\d+):(\\d+):(\\d+):(\\d+)"); 
	bool flag = false;
	CPi2000Data *pData = getCPi2000Data();
	flag = rx.exactMatch(systemTime);
	if (flag == true)
	{
		deviceDate.setDate(rx.cap(1).toInt(), rx.cap(2).toInt(), rx.cap(3).toInt());
		deviceTime = QTime(rx.cap(4).toInt(), rx.cap(5).toInt(), rx.cap(6).toInt());
		pData->setDeviceTime(&deviceDate, &deviceTime);
	}
}

void DeviceSocket::writeToBuffer(const QString & node, const QString &value)
{
	int i;
	QString command;

	for (i = 0; i < m_nodeList.length(); i++)
	{
		if (m_nodeList.at(i) == node)
		{
			m_valueList[i] = value;
			break;
		}
	}

	if (i == m_nodeList.length())
	{
		m_nodeList << node;
		m_valueList << value;
//		qDebug() << i << ":" << m_nodeList.length() << "add node" << node << "= " << value;
	}

	if (timerExpired(&m_sendBufferTimer, SEND_INTERVAL_IN_MS, true))
	{
		for (i = 0; i < m_nodeList.length(); i++)
		{
			command = QString("set \"%1\" \"%2\"\n").arg(m_nodeList.at(i)).arg(m_valueList.at(i));
			writeToSocket(command.toStdString().c_str());
#ifdef _SOCKET_DEBUG
			qDebug() << command.toStdString().c_str();
#endif
		}
		m_nodeList.clear();
		m_valueList.clear();

	}
}

QString DeviceSocket::getValueByNode(QString node, int timeoutInSecond)
{
    QString command = QString("get \"%1\"\n").arg(node);
	QString ret;

	s_param3[0].m_strParam0 = "get";
	s_param3[0].m_strParam1 = node;

	m_commandFlag[CMD_RESERVED] = true;
	writeToSocket(command.toStdString().c_str());

	for (int i = 0; i < timeoutInSecond * 100; i++)
	{
		g_pApp->msleep(10);
		if (m_commandFlag[CMD_RESERVED] == false)
		{
			QStringList cmdList = m_strCommandList[CMD_RESERVED];
			if (cmdList.length() != 3)
			{
				return (false);
			}

			ret = cmdList[2];
			break;
		}
	}

	s_param3[0].m_strParam0 = "";
	s_param3[0].m_strParam1 = "";
	return (ret);
}

void DeviceSocket::startSignal()
{
	QString command = QString("set \"%1\" \"On\"\n").arg(NODE_SIGNAL_MODE);
	writeToSocket(command.toStdString().c_str());
	qDebug() << command.toStdString().c_str();
}

void DeviceSocket::stopSignal()
{
	QString command = QString("set \"%1\" \"Off\"\n").arg(NODE_SIGNAL_MODE);
	writeToSocket(command.toStdString().c_str());
	qDebug() << command.toStdString().c_str();
}

void DeviceSocket::getSPLValue()
{
	QString command = QString("asyncget \"%1\"\n").arg(NODE_SPL_VALUE);
	writeToSocket(command.toStdString().c_str());
	qDebug() << command.toStdString().c_str();
}

void DeviceSocket::setLowShelfGain(int channelIndex, double gain)
{
	writeToBuffer(QString("\\\\Preset\\InPeq\\SV\\Channel_%1_Band_1_Gain").arg(channelIndex + 1), QString::number(gain, 'f', 1));

//	QString command = QString("set \"\\\\Preset\\InPeq\\SV\\Channel_%1_Band_1_Gain\" \"%2dB\"\n").arg(channelIndex + 1).arg(QString::number(gain, 'f', 1));
//	writeToSocket(command.toStdString().c_str());
//	qDebug() << command.toStdString().c_str();
}

void DeviceSocket::setLowShelfFreq(int channelIndex, double freq)
{
	writeToBuffer(QString("\\\\Preset\\InPeq\\SV\\Channel_%1_Band_1_Frequency").arg(channelIndex + 1), QString::number(freq, 'f', 1));

//	QString command = QString("set \"\\\\Preset\\InPeq\\SV\\Channel_%1_Band_1_Frequency\" \"%2Hz\"\n").arg(channelIndex + 1).arg(QString::number(freq, 'f', 1));
//	writeToSocket(command.toStdString().c_str());
//	qDebug() << command.toStdString().c_str();
}

void DeviceSocket::setLowShelfSlope(int channelIndex, double slope)
{
	writeToBuffer(QString("\\\\Preset\\InPeq\\SV\\Channel_%1_Band_1_Slope").arg(channelIndex + 1), QString::number(slope, 'f', 3));

//	QString command = QString("set \"\\\\Preset\\InPeq\\SV\\Channel_%1_Band_1_Slope\" \"%2\"\n").arg(channelIndex + 1).arg(QString::number(slope, 'f', 3));
//	writeToSocket(command.toStdString().c_str());
//	qDebug() << command.toStdString().c_str();
}

void DeviceSocket::setHighShelfGain(int channelIndex, double gain)
{
	writeToBuffer(QString("\\\\Preset\\InPeq\\SV\\Channel_%1_Band_2_Gain").arg(channelIndex + 1), QString::number(gain, 'f', 1));
//	QString command = QString("set \"\\\\Preset\\InPeq\\SV\\Channel_%1_Band_2_Gain\" \"%2dB\"\n").arg(channelIndex + 1).arg(QString::number(gain, 'f', 1));
//	writeToSocket(command.toStdString().c_str());
//	qDebug() << command.toStdString().c_str();
}

void DeviceSocket::setHighShelfFreq(int channelIndex, double freq)
{
	writeToBuffer(QString("\\\\Preset\\InPeq\\SV\\Channel_%1_Band_2_Frequency").arg(channelIndex + 1), QString::number(freq, 'f', 1));
//	QString command = QString("set \"\\\\Preset\\InPeq\\SV\\Channel_%1_Band_2_Frequency\" \"%2Hz\"\n").arg(channelIndex + 1).arg(QString::number(freq, 'f', 1));
//	writeToSocket(command.toStdString().c_str());
//	qDebug() << command.toStdString().c_str();
}

void DeviceSocket::setHighShelfSlope(int channelIndex, double slope)
{
	writeToBuffer(QString("\\\\Preset\\InPeq\\SV\\Channel_%1_Band_2_Slope").arg(channelIndex + 1), QString::number(slope, 'f', 3));
//	QString command = QString("set \"\\\\Preset\\InPeq\\SV\\Channel_%1_Band_2_Slope\" \"%2\"\n").arg(channelIndex + 1).arg(QString::number(slope, 'f', 3));
//	writeToSocket(command.toStdString().c_str());
//	qDebug() << command.toStdString().c_str();
}

void DeviceSocket::setSWBellFreq(int index, double freq)
{
	if (index == 1)
	{
		writeToBuffer(QString("\\\\Preset\\InPeq\\SV\\Channel_4_Band_1_Frequency"), QString::number(freq, 'f', 1));
	}
	else if (index == 2)
	{
		writeToBuffer(QString("\\\\Preset\\InPeq\\SV\\Channel_4_Band_2_Frequency"), QString::number(freq, 'f', 1));
	}
//	QString command = QString("set \"\\\\Preset\\InPeq\\SV\\Channel_4_Band_2_Frequency\" \"%2Hz\"\n").arg(QString::number(freq, 'f', 1));
//	writeToSocket(command.toStdString().c_str());
//	qDebug() << command.toStdString().c_str();
}

void DeviceSocket::setSWBellCut(int index, double gain)
{
	if (index == 1)
	{
		writeToBuffer(QString("\\\\Preset\\InPeq\\SV\\Channel_4_Band_1_Gain"), QString::number(gain, 'f', 1));
	}
	else if (index == 2)
	{
		writeToBuffer(QString("\\\\Preset\\InPeq\\SV\\Channel_4_Band_2_Gain"), QString::number(gain, 'f', 1));
	}
//	QString command = QString("set \"\\\\Preset\\InPeq\\SV\\Channel_4_Band_2_Gain\" \"%1dB\"\n").arg(QString::number(gain, 'f', 1));
//	writeToSocket(command.toStdString().c_str());
//	qDebug() << command.toStdString().c_str();
}

void DeviceSocket::setSWBellQ(int index, double Q)
{
	if (index == 1)
	{
		writeToBuffer(QString("\\\\Preset\\InPeq\\SV\\Channel_4_Band_1_Q"), QString::number(Q, 'f', 1));
	}
	else if (index == 2)
	{
		writeToBuffer(QString("\\\\Preset\\InPeq\\SV\\Channel_4_Band_2_Q"), QString::number(Q, 'f', 1));
	}
//	QString command = QString("set \"\\\\Preset\\InPeq\\SV\\Channel_4_Band_2_Q\" \"%1\"\n").arg(QString::number(Q, 'f', 1));
//	writeToSocket(command.toStdString().c_str());
//	qDebug() << command.toStdString().c_str();
}

void DeviceSocket::setSignalMode(SIGNAL_MODE mode)
{
	QString command1;
	QString command2;
	switch (mode)
	{
	case SIGNAL_OFF:
		command1 = QString("set \"%1\" \"off\"\n").arg(NODE_SIGNAL_TYPE);
		break;
	case SIGNAL_100Hz:
		command1 = QString("set \"%1\" \"Sine\"\n").arg(NODE_SIGNAL_TYPE);
		command2 = QString("set \"%1\" \"100Hz\"\n").arg(NODE_SIGNAL_FREQUENCY);
		break;
	case SIGNAL_1kHz:
		command1 = QString("set \"%1\" \"Sine\"\n").arg(NODE_SIGNAL_TYPE);
		command2 = QString("set \"%1\" \"1kHz\"\n").arg(NODE_SIGNAL_FREQUENCY);
		break;
	case SIGNAL_10kHz:
		command1 = QString("set \"%1\" \"Sine\"\n").arg(NODE_SIGNAL_TYPE);
		command2 = QString("set \"%1\" \"10kHz\"\n").arg(NODE_SIGNAL_FREQUENCY);
		break;
	case SIGNAL_PINK_NOISE:
		command1 = QString("set \"%1\" \"Noise\"\n").arg(NODE_SIGNAL_TYPE);
		break;

	}

	writeToSocket(command1.toStdString().c_str());
	qDebug() << command1.toStdString().c_str();

	if (!command2.isEmpty())
	{
		writeToSocket(command2.toStdString().c_str());
		qDebug() << command2.toStdString().c_str();
	}
}

void DeviceSocket::setSWCenterGen(bool flag)
{
	QString command;
	if (flag)
	{
		command = QString("set \"%1\" \"On\"\n").arg(NODE_SW_GEN_CENTER);
	}
	else
	{
		command = QString("set \"%1\" \"Off\"\n").arg(NODE_SW_GEN_CENTER);
	}
	writeToSocket(command.toStdString().c_str());
	qDebug() << command.toStdString().c_str();
}


void DeviceSocket::writeToSocket(const char *lpszBuffer)
{
	if (BIT(g_debug, LOG_SOCKET_MESSAGE))
	{
		L_DEBUG(QString("Send to device %1 : %2").arg(getDeviceAddr().toString()).arg(lpszBuffer));
	}
	m_pSocket->write(lpszBuffer);
}
