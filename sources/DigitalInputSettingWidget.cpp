#include "DigitalInputSettingWidget.h"
#include <QPainter>
#include <QGlobal.h>
#include <QEvent>
#include "CPi2000Setting.h"

static const int s_defaultChannelNo1[8] = {0, 1, 2, 3, 4, 5, 6, 7};
static const int s_defaultChannelNo2[8] = {0, 2, 1, 5, 3, 4, 6, 7};
static const int s_defaultChannelNo3[8] = {0, 4, 2, 5, 1, 3, 6, 7};


/* L / C / R / LFE / LS / RS / Bls / Brs */
InputChannel g_inputChannel[8] = 
{
	{ QString("L"), 1, 3 },
	{ QString("R"), 2, 3 },
	{ QString("C"), 3, 3 },
	{ QString("Sw"), 4, 3 },
	{ QString("Ls"), 5, 3 },
	{ QString("Rs"), 6, 3 },
	{ QString("Bls"), 7, 3 },
	{ QString("Brs"), 8, 3 },
};

DigitalInputSettingWidget::DigitalInputSettingWidget(QWidget *pParent)
    : QWidget(pParent)
{
	m_pInputChannel = g_inputChannel;

	m_pButton1 = new MarkButton(QString("L/R C/Sw Ls/Rs"), this);
	m_pButton1->setObjectName(QStringLiteral("InputPresetButton"));
	connect(m_pButton1, SIGNAL(clicked()), this, SLOT(onClickPresetButton1()));

	m_pButton2 = new MarkButton(QString("L/C R/Ls Rs/Sw"), this);
	m_pButton2->setObjectName(QStringLiteral("InputPresetButton"));
	connect(m_pButton2, SIGNAL(clicked()), this, SLOT(onClickPresetButton2()));

	m_pButton3 = new MarkButton(QString("L/Ls C/Rs R/Sw"), this);
	m_pButton3->setObjectName(QStringLiteral("InputPresetButton"));
	connect(m_pButton3, SIGNAL(clicked()), this, SLOT(onClickPresetButton3()));
	
	m_pUndoButton = new QPushButton(tr("Undo"), this);
	connect(m_pUndoButton, SIGNAL(clicked()), this, SLOT(onClickUndoButton()));

	m_pApplyButton = new QPushButton(tr("Apply"), this);
	connect(m_pApplyButton, SIGNAL(clicked()), this, SLOT(onClickApplyButton()));

	QStringList digitalVolumeStringList;
	digitalVolumeStringList << "";
	for (int i = 0; i < 8; i++)
	{
		m_pLabel[i] = new QLabel(m_pInputChannel[i].m_channelName, this);
		m_pLabel[i]->setObjectName(QStringLiteral("BlackFont14"));
		m_pChannelCombo[i] = new QComboBox(this);
		for (int index = 0; index < 8; index++)
		{
			m_pChannelCombo[i]->addItem(QString("CH %1").arg(index + 1));
		}
		m_pChannelCombo[i]->setCurrentIndex(i);

#if 0
		m_pVolumeChart[i] = new VolumeChartWidget(this);
		float *inputDB = getCPi2000Data()->getInputDB();
		m_pVolumeChart[i]->setVolumeData(inputDB + i, digitalVolumeStringList, 1);
		m_pVolumeChart[i]->hide();
#endif
	}

	for (int i = 0; i < 8; i++)
	{
		m_channelNo[i] = i;
	}
	setUndoState();

	connect(m_pChannelCombo[0], SIGNAL(currentIndexChanged(int)), this, SLOT(onCombo0CurrentChanged(int)));
	connect(m_pChannelCombo[1], SIGNAL(currentIndexChanged(int)), this, SLOT(onCombo1CurrentChanged(int)));
	connect(m_pChannelCombo[2], SIGNAL(currentIndexChanged(int)), this, SLOT(onCombo2CurrentChanged(int)));
	connect(m_pChannelCombo[3], SIGNAL(currentIndexChanged(int)), this, SLOT(onCombo3CurrentChanged(int)));
	connect(m_pChannelCombo[4], SIGNAL(currentIndexChanged(int)), this, SLOT(onCombo4CurrentChanged(int)));
	connect(m_pChannelCombo[5], SIGNAL(currentIndexChanged(int)), this, SLOT(onCombo5CurrentChanged(int)));
	connect(m_pChannelCombo[6], SIGNAL(currentIndexChanged(int)), this, SLOT(onCombo6CurrentChanged(int)));
	connect(m_pChannelCombo[7], SIGNAL(currentIndexChanged(int)), this, SLOT(onCombo7CurrentChanged(int)));
	refreshChannelNo();
}



void DigitalInputSettingWidget::changeEvent(QEvent *e)
{
    if(e->type() == QEvent::EnabledChange)
    {
        if(isEnabled())
        {
//			show();
        }
        else
        {
//			hide();
        }
    }
}


void DigitalInputSettingWidget::paintEvent(QPaintEvent * /*event*/)
{
	QPainter painter(this);
	painter.setPen(QPen(QColor(10, 10, 10), 1));

	int topLeftX = width() / 8;
	int topRightX = width() * 9 / 10 - 40 - topLeftX;
	int bottomRightX = width() * 9 / 10 - 40;

	QPoint aPoint[5] = {QPoint(topLeftX, 1), QPoint(topRightX, 1), QPoint(bottomRightX, height() - 2), QPoint(0, height() - 2), QPoint(topLeftX, 1) }; 
	for (int i = 0; i < 4; i++)
	{
		painter.drawLine(aPoint[i], aPoint[i + 1]);
	}

#if 0
	int topWidth = topRightX - topLeftX;
	int unitWidth = (topRightX - topLeftX) / 4;

	QPoint point[8] = 
	{
		QPoint(topLeftX + topWidth / 16, 0),
		QPoint(topLeftX + topWidth * 6 / 16, 0),
		QPoint(topLeftX + topWidth * 11 / 16, 0),
		QPoint(topLeftX + topWidth / 2, height() / 4),
		QPoint(topLeftX , height() / 2),
		QPoint(topLeftX + topWidth * 3 / 4, height() / 2),
		QPoint(bottomRightX / 4, height() * 3/ 4),
		QPoint(bottomRightX * 5/ 8, height() * 3 / 4),
	};

	for (int i = 0; i < 8; i++)
	{
//		painter.drawRect(point[i].x(), point[i].y(), unitWidth, 40);
	}
#endif
}

void DigitalInputSettingWidget::resizeEvent(QResizeEvent * /* event */)
{
	int w = width();
	int h = height();

	int buttonLeft = w * 9 / 10 - 35;
	int buttonWidth = w / 10 + 40;
	int buttonTop = h / 20;
	int buttonHeight = h / 20 + 15;
	int buttonMargin = h / 40 + 5;

	m_pButton1->setGeometry(buttonLeft, buttonTop + 0 *(buttonHeight + buttonMargin), buttonWidth, buttonHeight);
	m_pButton2->setGeometry(buttonLeft, buttonTop + 1 *(buttonHeight + buttonMargin), buttonWidth, buttonHeight);
	m_pButton3->setGeometry(buttonLeft, buttonTop + 2 *(buttonHeight + buttonMargin), buttonWidth, buttonHeight);

	buttonTop = 21 * h / 30;
	m_pUndoButton->setGeometry(buttonLeft, buttonTop + 0 * (buttonHeight + buttonMargin), buttonWidth, buttonHeight);
	m_pApplyButton->setGeometry(buttonLeft, buttonTop + 1 * (buttonHeight + buttonMargin), buttonWidth, buttonHeight);

	int topLeftX = width() / 8;
	int topRightX = width() * 9 / 10 - 40 - topLeftX;
	int bottomRightX = width() * 9 / 10 - 40;

	int topWidth = topRightX - topLeftX;
	int unitWidth = (topRightX - topLeftX) / 4;
	QPoint point[8] = 
	{
		QPoint(topLeftX + topWidth / 16, 0),
		QPoint(topLeftX + topWidth * 11 / 16, 0),
		QPoint(topLeftX + topWidth * 6 / 16, 0),
		QPoint(topLeftX + topWidth / 2, height() / 4),
		QPoint(topLeftX , height() / 2),
		QPoint(topLeftX + topWidth * 3 / 4, height() / 2),
		QPoint(bottomRightX / 4, height() * 3/ 4),
		QPoint(bottomRightX * 5/ 8, height() * 3 / 4),
	};

	for (int i = 0; i < 8; i++)
	{
		m_pLabel[i]->setGeometry(point[i].x() + 10, point[i].y() + height() / 20 - 5, unitWidth / 3 + 30, 20);
		m_pChannelCombo[i]->setGeometry(point[i].x(), point[i].y() + height() / 20 + 20 - 5, unitWidth / 6 + 60, 20); 
//		m_pVolumeChart[i]->setGeometry(point[i].x() + unitWidth / 6 + 60, point[i].y() + height() / 20 - 12, unitWidth / 4 + 5, 70);  
	}
}

void DigitalInputSettingWidget::setUndoState()
{
	for (int i = 0; i < 8; i++)
	{
		m_undoChannelNo[i] = m_channelNo[i];
	}
}

void DigitalInputSettingWidget::getChannel(int channel[8])
{
	for (int i = 0; i < 8; i++)
	{
		channel[i] = m_channelNo[i];
	}
}

void DigitalInputSettingWidget::setChannel(int channel[8])
{
	for (int i = 0; i < 8; i++)
	{
		m_channelNo[i] = channel[i];
	}
	refreshChannelNo();
}

void DigitalInputSettingWidget::refreshChannelNo()
{
	bool validFlag = true;
	for (int i = 0; i < 8; i++)
	{
		bool repeatFlag = false;
		for (int j = 0; j < 8; j++)
		{
			if (i == j)
			{
				continue;
			}
			if (m_channelNo[i] == m_channelNo[j])
			{
				repeatFlag = true;
				validFlag = false;
				break;
			}
		}

		m_pChannelCombo[i]->setCurrentIndex(m_channelNo[i]);
		QPalette palette = m_pChannelCombo[i]->palette();
		palette.setColor(QPalette::Text, repeatFlag ? Qt::red : Qt::black);
		m_pChannelCombo[i]->setPalette(palette);
	}

	m_pApplyButton->setEnabled(validFlag);

	bool sameFlag = true;
	for (int i = 0; i < 8; i++)
	{
		if (m_undoChannelNo[i] != m_channelNo[i])
		{
			sameFlag = false;
			break;
		}
	}
	m_pUndoButton->setEnabled(!sameFlag);

	sameFlag = true;
	for (int i = 0; i < 8; i++)
	{
		if (s_defaultChannelNo1[i] != m_channelNo[i])
		{
			sameFlag = false;
			break;
		}
	}
	m_pButton1->mark(sameFlag);

	sameFlag = true;
	for (int i = 0; i < 8; i++)
	{
		if (s_defaultChannelNo2[i] != m_channelNo[i])
		{
			sameFlag = false;
			break;
		}
	}
	m_pButton2->mark(sameFlag);

	sameFlag = true;
	for (int i = 0; i < 8; i++)
	{
		if (s_defaultChannelNo3[i] != m_channelNo[i])
		{
			sameFlag = false;
			break;
		}
	}
	m_pButton3->mark(sameFlag);

}

void DigitalInputSettingWidget::onCombo0CurrentChanged(int index)
{
	onComboCurrentChanged(0, index);
}

void DigitalInputSettingWidget::onCombo1CurrentChanged(int index)
{
	onComboCurrentChanged(1, index);
}

void DigitalInputSettingWidget::onCombo2CurrentChanged(int index)
{
	onComboCurrentChanged(2, index);
}

void DigitalInputSettingWidget::onCombo3CurrentChanged(int index)
{
	onComboCurrentChanged(3, index);
}

void DigitalInputSettingWidget::onCombo4CurrentChanged(int index)
{
	onComboCurrentChanged(4, index);
}

void DigitalInputSettingWidget::onCombo5CurrentChanged(int index)
{
	onComboCurrentChanged(5, index);
}

void DigitalInputSettingWidget::onCombo6CurrentChanged(int index)
{
	onComboCurrentChanged(6, index);
}

void DigitalInputSettingWidget::onCombo7CurrentChanged(int index)
{
	onComboCurrentChanged(7, index);
}

void DigitalInputSettingWidget::onComboCurrentChanged(int comboIndex, int channelIndex)
{
	if ((comboIndex < 8) && (channelIndex < 8))
	{
		m_channelNo[comboIndex] = channelIndex;
	}
	refreshChannelNo();
}

void DigitalInputSettingWidget::onClickUndoButton()
{
	for (int i = 0; i < 8; i++)
	{
		m_channelNo[i] = m_undoChannelNo[i];
	}
	refreshChannelNo();
}

void DigitalInputSettingWidget::onClickApplyButton()
{
	setUndoState();
	refreshChannelNo();

	// emit Apply
	emit applyChannel();
}

void DigitalInputSettingWidget::onClickPresetButton1()
{
	for (int i = 0; i < 8; i++)
	{
		m_channelNo[i] = s_defaultChannelNo1[i];
	}
	refreshChannelNo();
}

void DigitalInputSettingWidget::onClickPresetButton2()
{
	for (int i = 0; i < 8; i++)
	{
		m_channelNo[i] = s_defaultChannelNo2[i];
	}
	refreshChannelNo();
}

void DigitalInputSettingWidget::onClickPresetButton3()
{
	for (int i = 0; i < 8; i++)
	{
		m_channelNo[i] = s_defaultChannelNo3[i];
	}
	refreshChannelNo();
}

void DigitalInputSettingWidget::retranslateUi()
{
	m_pUndoButton->setText(tr("Undo"));
	m_pApplyButton->setText(tr("Apply"));
}