#include "EQButton.h"
#include <QPainter>
#include <QVariant>
#include <QMouseEvent>

EQButton::EQButton(QString &text, QWidget *parent)
    : QPushButton(text, parent)
{
	m_enableFlag = false;
	m_parent = parent;
}

void EQButton::enable(bool flag)
{
	m_enableFlag = flag;
	setProperty("enableFlag", flag);
}

void EQButton::focus(bool flag)
{
	m_selectFlag = flag;
	setProperty("selectFlag", flag);
}


void EQButton::setEnableFlag(bool flag)
{
	m_enableFlag = flag;
}

bool EQButton::getEnableFlag() const
{
	return (m_enableFlag);
}

void EQButton::setSelectFlag(bool selectFlag)
{
	m_selectFlag = selectFlag;
}

bool EQButton::getSelectFlag() const
{
	return (m_selectFlag);
}

void EQButton::paintEvent(QPaintEvent *event)
{
	QPixmap background(":LEDGreen.png");

	if (m_selectFlag == true)
	{
		if (m_enableFlag == true)
		{
			background = QPixmap(":EQButton-enable-focus.png");
		}
		else
		{
			background = QPixmap(":EQButton-disable-focus.png");			
		}
	}
	else
	{
		if (m_enableFlag == true)
		{
			background = QPixmap(":EQButton-enable-unfocus.png");		
		}
		else
		{
			background = QPixmap(":EQButton-disable-unfocus.png");		
		}
	}

	QPainter painter(this);
//	painter.drawPixmap(0, 0, background);
	painter.drawPixmap(0, 0, background.scaled(size()));
	painter.setPen(QPen(QColor(0, 0, 0), 1));
	painter.drawRect(0, 0, width() - 1, height() - 1);
	QPushButton::paintEvent(event);
}

void EQButton::mouseDoubleClickEvent(QMouseEvent *)
{
	emit doubleClick();
}
