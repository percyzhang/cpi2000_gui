#include "EQTuningWidget.h"
#include <QPainter>
#include <QDebug>
#include "CPi2000Setting.h"
#include "mainApplication.h"
#include "mainWidget.h"
#include "AdaptiveTuning.h"
#include "WaittingWidget.h"

EQTuningWidget::EQTuningWidget(QWidget *parent)
    : QWidget(parent)
{
	m_EQCopyIndex = -1;

	m_pSignalModeGroup = new QGroupBox(tr(""), this);
	m_pSignalModeGroup->setObjectName(QStringLiteral("SpeakerGroupBox"));
	{
		m_pSignalModeCombo = new QComboBox(m_pSignalModeGroup);

		QString signal[] = {"OFF", "100 Hz", "1 kHz", "10 kHz",  "Pink Noise" };
		/*
				set to 100Hz:
				set "\\Preset\SignalGenerator\SV\SineFrequency"   "100Hz"
				set "\\Preset\SignalGenerator\SV\SignalType"   "Sine"

				set to 1kHz:
				set "\\Preset\SignalGenerator\SV\SineFrequency"   "1000Hz"
				set "\\Preset\SignalGenerator\SV\SignalType"   "Sine"

				set to Pink:
				set "\\Preset\SignalGenerator\SV\SignalType"   "Noise"

				set to Off:
				set "\\Preset\SignalGenerator\SV\SignalType"   "Off"
		*/
		for (int i = 0; i < 5; i++)
		{
			m_pSignalModeCombo->addItem(signal[i]);
		}
		connect(m_pSignalModeCombo, SIGNAL(currentIndexChanged(int)), this, SLOT(onSignalModeSelected(int)));
//		connect(g_pApp, SIGNAL(setSignalMode(int)), this, SLOT(onSignalModeSelected(int)));

		m_pReminderLabel = new QLabel(tr("To stop RTA setting, you need to set signal mode to 'OFF'."),  m_pSignalModeGroup);
		m_pReminderLabel->setWordWrap(true);
		m_pReminderLabel->setObjectName("BrownFont12");

		m_pSignalLabel = new QLabel(tr("Signal Mode"), m_pSignalModeGroup);
		m_pSignalLabel->setObjectName("BlackFont12");
		m_pSignalLabel->setAlignment(Qt::AlignLeft | Qt::AlignTop);
	}

	m_pActiveChannelGroup = new QGroupBox(tr(""), this);
	m_pActiveChannelGroup->setObjectName(QStringLiteral("ActiveChannel"));
	{
		QString channelName[8] = { "L", "R", "C", "Sw", "Ls", "Rs", "Bls", "Brs" };

		m_pCurrentChannelLabel = new QLabel(m_pActiveChannelGroup);
		m_pCurrentChannelLabel->setAlignment(Qt::AlignCenter);
		m_pCurrentChannelLabel->setObjectName("CurrentChannel");
//		m_pCurrentChannelLabel->setObjectName("WhiteFont18");
		for (int i = 0; i < 8; i++)
		{
//			m_pChannelLabel[i] = new QLabel(channelName[i], m_pActiveChannelGroup);
//			m_pChannelLabel[i]->setAlignment(Qt::AlignLeft | Qt::AlignVCenter);
//			m_pChannelLabel[i]->setObjectName("WhiteFont12");

			m_pRadioButton[i] = new QRadioButton(channelName[i], m_pActiveChannelGroup);
			m_pRadioButton[i]->setObjectName("WhiteFont12");
		}

		connect(m_pRadioButton[0], SIGNAL(toggled(bool)), this, SLOT(onCurrentChannelToggled_0(bool)));
		connect(m_pRadioButton[1], SIGNAL(toggled(bool)), this, SLOT(onCurrentChannelToggled_1(bool)));
		connect(m_pRadioButton[2], SIGNAL(toggled(bool)), this, SLOT(onCurrentChannelToggled_2(bool)));
		connect(m_pRadioButton[3], SIGNAL(toggled(bool)), this, SLOT(onCurrentChannelToggled_3(bool)));
		connect(m_pRadioButton[4], SIGNAL(toggled(bool)), this, SLOT(onCurrentChannelToggled_4(bool)));
		connect(m_pRadioButton[5], SIGNAL(toggled(bool)), this, SLOT(onCurrentChannelToggled_5(bool)));
		connect(m_pRadioButton[6], SIGNAL(toggled(bool)), this, SLOT(onCurrentChannelToggled_6(bool)));
		connect(m_pRadioButton[7], SIGNAL(toggled(bool)), this, SLOT(onCurrentChannelToggled_7(bool)));
	}

	m_pChannelLevelGroup = new QGroupBox(tr(""), this);
	m_pChannelLevelGroup->setObjectName(QStringLiteral("SpeakerGroupBox"));
	{
		m_pChannelLevelLabel = new QLabel(tr("Active Channel Level"), m_pChannelLevelGroup);
		m_pChannelLevelLabel->setAlignment(Qt::AlignCenter);
		m_pChannelLevelLabel->setObjectName("BlackFont12");

		m_pVolumeGroup = new VolumeGroup(m_pChannelLevelGroup);
		connect(m_pVolumeGroup, SIGNAL(changeVolume(float)), this, SLOT(onChangeVolume(float)));
		connect(m_pVolumeGroup, SIGNAL(toggleMuteButton(bool)), this, SLOT(onToggleMuteButton(bool)));
	}

	m_pSubwooferPEQGroup1 = new QGroupBox(tr(""), this);
	m_pSubwooferPEQGroup1->setObjectName(QStringLiteral("SpeakerGroupBox"));
	{
		m_pSWPEQGainLabel1 = new QLabel(tr("Cut"), m_pSubwooferPEQGroup1);
		m_pSWPEQGainLabel1->setAlignment(Qt::AlignCenter);

		m_pSWPEQ1Label = new QLabel(tr(""), m_pSubwooferPEQGroup1);
		m_pSWPEQ1Label->setAlignment(Qt::AlignCenter);
		m_pSWPEQ1Label->setObjectName("BlackFont12");

		m_pSWPEQGainSlider1 = new ButtonSlider(m_pSubwooferPEQGroup1);
		m_pSWPEQGainSlider1->setTextColor(QColor(0x10, 0x10, 0x10));
		m_pSWPEQGainSlider1->setMarkColor(QColor(0x10, 0x10, 0x10));
		QStringList analogStringList;
		analogStringList << "-12.6dB" << "0dB"<<"1" << "2";
		m_pSWPEQGainSlider1->setMark(2, 4, analogStringList);
		m_pSWPEQGainSlider1->setMarkLength(5, 7);
		m_pSWPEQGainSlider1->setMarkSide(true, true);
		m_pSWPEQGainSlider1->setRange(-126, 0);
		m_pSWPEQGainSlider1->setGrooveHeight(30);
		m_pSWPEQGainSlider1->setSingleStep(1);
		m_pSWPEQGainSlider1->setPageStep(1);
		m_pSWPEQGainSlider1->setValue(0);
		connect(m_pSWPEQGainSlider1, SIGNAL(valueChanged(int)), this, SLOT(onSWPEQGainSliderChanged1(int)));

		m_pTypeLabel1 = new QLabel(tr("Type:"), m_pSubwooferPEQGroup1);
		m_pTypeLabel1->setAlignment(Qt::AlignRight);

		m_pGainLabel1 = new QLabel(tr("Cut:"), m_pSubwooferPEQGroup1);
		m_pGainLabel1->setAlignment(Qt::AlignRight);

		m_pFreqLabel1 = new QLabel(tr("Frequency:"), m_pSubwooferPEQGroup1);
		m_pFreqLabel1->setAlignment(Qt::AlignRight);

		m_pQLabel1 = new QLabel(tr("Q:"), m_pSubwooferPEQGroup1);
		m_pQLabel1->setAlignment(Qt::AlignRight);

		m_pBellCombo1 = new QComboBox(m_pSubwooferPEQGroup1);
		m_pBellCombo1->addItem(tr("Bell"));
		m_pBellCombo1->setCurrentIndex(0);
		m_pBellCombo1->setEnabled(false);
//		m_pBellEdit->setAlignment(Qt::AlignLeft);

		m_pPEQCutSpin1 = new QDoubleSpinBox(m_pSubwooferPEQGroup1);
		m_pPEQCutSpin1->setRange(-12.6, 0);
		m_pPEQCutSpin1->setSingleStep(0.1);
		m_pPEQCutSpin1->setDecimals(1);
		m_pPEQCutSpin1->setSuffix(" dB");
		connect(m_pPEQCutSpin1, SIGNAL(valueChanged(double)), this, SLOT(onPEQCutChanged1(double)));

		m_pPEQFreqSpin1 = new QDoubleSpinBox(m_pSubwooferPEQGroup1);
		m_pPEQFreqSpin1->setRange(25.0, 125.0);
		m_pPEQFreqSpin1->setSingleStep(0.1);
		m_pPEQFreqSpin1->setDecimals(1);
		m_pPEQFreqSpin1->setSuffix(" Hz");
		connect(m_pPEQFreqSpin1, SIGNAL(valueChanged(double)), this, SLOT(onPEQFreqChanged1(double)));

		m_pQSpin1 = new QDoubleSpinBox(m_pSubwooferPEQGroup1);
		m_pQSpin1->setRange(0.1, 4.0);
		m_pQSpin1->setSingleStep(0.1);
		m_pQSpin1->setDecimals(1);
		connect(m_pQSpin1, SIGNAL(valueChanged(double)), this, SLOT(onPEQQChanged1(double)));
	}

	m_pSubwooferPEQGroup2 = new QGroupBox(tr(""), this);
	m_pSubwooferPEQGroup2->setObjectName(QStringLiteral("SpeakerGroupBox"));
	{
		m_pSWPEQGainLabel2 = new QLabel(tr("Cut"), m_pSubwooferPEQGroup2);
		m_pSWPEQGainLabel2->setAlignment(Qt::AlignCenter);

		m_pSWPEQ2Label = new QLabel(tr(""), m_pSubwooferPEQGroup2);
		m_pSWPEQ2Label->setAlignment(Qt::AlignCenter);
		m_pSWPEQ2Label->setObjectName("BlackFont12");


		m_pSWPEQGainSlider2 = new ButtonSlider(m_pSubwooferPEQGroup2);
		m_pSWPEQGainSlider2->setTextColor(QColor(0x10, 0x10, 0x10));
		m_pSWPEQGainSlider2->setMarkColor(QColor(0x10, 0x10, 0x10));
		QStringList analogStringList;
		analogStringList << "-12.6dB" << "0dB"<<"1" << "2";
		m_pSWPEQGainSlider2->setMark(2, 4, analogStringList);
		m_pSWPEQGainSlider2->setMarkLength(5, 7);
		m_pSWPEQGainSlider2->setMarkSide(true, true);
		m_pSWPEQGainSlider2->setRange(-126, 0);
		m_pSWPEQGainSlider2->setGrooveHeight(30);
		m_pSWPEQGainSlider2->setSingleStep(1);
		m_pSWPEQGainSlider2->setPageStep(1);
		m_pSWPEQGainSlider2->setValue(0);
		connect(m_pSWPEQGainSlider2, SIGNAL(valueChanged(int)), this, SLOT(onSWPEQGainSliderChanged2(int)));

		m_pTypeLabel2 = new QLabel(tr("Type:"), m_pSubwooferPEQGroup2);
		m_pTypeLabel2->setAlignment(Qt::AlignRight);

		m_pGainLabel2 = new QLabel(tr("Cut:"), m_pSubwooferPEQGroup2);
		m_pGainLabel2->setAlignment(Qt::AlignRight);

		m_pFreqLabel2 = new QLabel(tr("Frequency:"), m_pSubwooferPEQGroup2);
		m_pFreqLabel2->setAlignment(Qt::AlignRight);

		m_pQLabel2 = new QLabel(tr("Q:"), m_pSubwooferPEQGroup2);
		m_pQLabel2->setAlignment(Qt::AlignRight);

		m_pBellCombo2 = new QComboBox(m_pSubwooferPEQGroup2);
		m_pBellCombo2->addItem(tr("Bell"));
		m_pBellCombo2->setCurrentIndex(0);
		m_pBellCombo2->setEnabled(false);
//		m_pBellEdit->setAlignment(Qt::AlignLeft);

		m_pPEQCutSpin2 = new QDoubleSpinBox(m_pSubwooferPEQGroup2);
		m_pPEQCutSpin2->setRange(-12.6, 0);
		m_pPEQCutSpin2->setSingleStep(0.1);
		m_pPEQCutSpin2->setDecimals(1);
		m_pPEQCutSpin2->setSuffix(" dB");
		connect(m_pPEQCutSpin2, SIGNAL(valueChanged(double)), this, SLOT(onPEQCutChanged2(double)));

		m_pPEQFreqSpin2 = new QDoubleSpinBox(m_pSubwooferPEQGroup2);
		m_pPEQFreqSpin2->setRange(25.0, 125.0);
		m_pPEQFreqSpin2->setSingleStep(0.1);
		m_pPEQFreqSpin2->setDecimals(1);
		m_pPEQFreqSpin2->setSuffix(" Hz");
		connect(m_pPEQFreqSpin2, SIGNAL(valueChanged(double)), this, SLOT(onPEQFreqChanged2(double)));

		m_pQSpin2 = new QDoubleSpinBox(m_pSubwooferPEQGroup2);
		m_pQSpin2->setRange(0.1, 4.0);
		m_pQSpin2->setSingleStep(0.1);
		m_pQSpin2->setDecimals(1);
		connect(m_pQSpin2, SIGNAL(valueChanged(double)), this, SLOT(onPEQQChanged2(double)));
	}


	m_pPEQGroup = new QGroupBox(tr(""), this);
	m_pPEQGroup->setObjectName(QStringLiteral("SpeakerGroupBox"));
	{
		m_pLShGroup = new QGroupBox(tr(""), m_pPEQGroup);
		m_pLShGroup->setObjectName(QStringLiteral("SpeakerGroupBox"));
		{
			m_pLShelfGainSlider = new ButtonSlider(m_pLShGroup);
			m_pLShelfGainSlider->setTextColor(QColor(0x10, 0x10, 0x10));
			m_pLShelfGainSlider->setMarkColor(QColor(0x10, 0x10, 0x10));
			QStringList analogStringList;
			analogStringList << "-15dB" << "0dB" << "15dB";
			m_pLShelfGainSlider->setMark(3, 3, analogStringList);
			m_pLShelfGainSlider->setMarkLength(5, 7);
			m_pLShelfGainSlider->setMarkSide(true, true);
			m_pLShelfGainSlider->setRange(-150, 150);
			m_pLShelfGainSlider->setGrooveHeight(30);
			m_pLShelfGainSlider->setSingleStep(1);
			m_pLShelfGainSlider->setPageStep(1);
			m_pLShelfGainSlider->setValue(0);
			connect(m_pLShelfGainSlider, SIGNAL(valueChanged(int)), this, SLOT(onLShelfGainSliderChanged(int)));

			
			m_pLShelfLabel = new QLabel(tr("Low Shelf Filter"), m_pLShGroup);
			m_pLShelfLabel->setAlignment(Qt::AlignCenter);
			m_pLShelfLabel->setObjectName("BlackFont12");

			m_pLGainSliderLabel = new QLabel(tr("Gain"), m_pLShGroup);
			m_pLGainSliderLabel->setAlignment(Qt::AlignCenter);

			m_pLGreqLabel = new QLabel(tr("Frequency"), m_pLShGroup);
			m_pLGreqLabel->setObjectName("BlackFont12");
			m_pLGreqLabel->setAlignment(Qt::AlignRight | Qt::AlignVCenter);

			m_pLGainLabel = new QLabel(tr("Gain"), m_pLShGroup);
			m_pLGainLabel->setObjectName("BlackFont12");
			m_pLGainLabel->setAlignment(Qt::AlignRight | Qt::AlignVCenter);

			m_pLSlopeLabel = new QLabel(tr("Slope"), m_pLShGroup);
			m_pLSlopeLabel->setObjectName("BlackFont12");
			m_pLSlopeLabel->setAlignment(Qt::AlignRight | Qt::AlignVCenter);

			m_pLFreqSpin = new QDoubleSpinBox(m_pLShGroup);
			m_pLFreqSpin->setRange(MIN_EQ_FREQUENCY, MAX_EQ_FREQUENCY);
			m_pLFreqSpin->setDecimals(1);
			m_pLFreqSpin->setSuffix(" Hz");
			connect(m_pLFreqSpin, SIGNAL(valueChanged(double)), this, SLOT(onLFreqChanged(double)));

			m_pLGainSpin = new QDoubleSpinBox(m_pLShGroup);
			m_pLGainSpin->setRange(-15.0, 15.0);
			m_pLGainSpin->setSingleStep(0.1);
			m_pLGainSpin->setDecimals(1);
			m_pLGainSpin->setSuffix(" dB");
			connect(m_pLGainSpin, SIGNAL(valueChanged(double)), this, SLOT(onLGainChanged(double)));

			m_pLSlopSpin = new QDoubleSpinBox(m_pLShGroup);
			m_pLSlopSpin->setSuffix(" dB/Oct");
			m_pLSlopSpin->setRange(MIN_SLOPE, MAX_SLOPE);
			m_pLSlopSpin->setSingleStep(0.01);
			m_pLSlopSpin->setDecimals(2);
			connect(m_pLSlopSpin, SIGNAL(valueChanged(double)), this, SLOT(onLSlopeChanged(double)));
		}

		m_pHShGroup = new QGroupBox(tr(""), m_pPEQGroup);
		m_pHShGroup->setObjectName(QStringLiteral("SpeakerGroupBox"));
		{
			m_pHShelfLabel = new QLabel(tr(""), m_pHShGroup);
			m_pHShelfLabel->setAlignment(Qt::AlignCenter);
			m_pHShelfLabel->setObjectName("BlackFont12");

			m_pHShelfGainSlider = new ButtonSlider(m_pHShGroup);
			m_pHShelfGainSlider->setTextColor(QColor(0x10, 0x10, 0x10));
			m_pHShelfGainSlider->setMarkColor(QColor(0x10, 0x10, 0x10));
			QStringList analogStringList;
			analogStringList << "-15dB" << "0dB" << "15dB";
			m_pHShelfGainSlider->setMark(3, 3, analogStringList);
			m_pHShelfGainSlider->setMarkLength(5, 7);
			m_pHShelfGainSlider->setMarkSide(true, true);
			m_pHShelfGainSlider->setRange(-150, 150);
			m_pHShelfGainSlider->setGrooveHeight(30);
			m_pHShelfGainSlider->setSingleStep(1);
			m_pHShelfGainSlider->setPageStep(1);
			m_pHShelfGainSlider->setValue(0);
			connect(m_pHShelfGainSlider, SIGNAL(valueChanged(int)), this, SLOT(onHShelfGainSliderChanged(int)));

			m_pHGainSliderLabel = new QLabel(tr("Gain"), m_pHShGroup);
			m_pHGainSliderLabel->setAlignment(Qt::AlignCenter);

			m_pHGreqLabel = new QLabel(tr("Frequency:"), m_pHShGroup);
			m_pHGreqLabel->setObjectName("BlackFont12");
			m_pHGreqLabel->setAlignment(Qt::AlignRight | Qt::AlignVCenter);

			m_pHGainLabel = new QLabel(tr("Gain:"), m_pHShGroup);
			m_pHGainLabel->setObjectName("BlackFont12");
			m_pHGainLabel->setAlignment(Qt::AlignRight | Qt::AlignVCenter);

			m_pHSlopeLabel = new QLabel(tr("Slope:"), m_pHShGroup);
			m_pHSlopeLabel->setObjectName("BlackFont12");
			m_pHSlopeLabel->setAlignment(Qt::AlignRight | Qt::AlignVCenter);

			m_pHFreqSpin = new QDoubleSpinBox(m_pHShGroup);
			m_pHFreqSpin->setRange(MIN_EQ_FREQUENCY, MAX_EQ_FREQUENCY);
			m_pHFreqSpin->setDecimals(1);
			m_pHFreqSpin->setSuffix(" Hz");
			connect(m_pHFreqSpin, SIGNAL(valueChanged(double)), this, SLOT(onHFreqChanged(double)));

			m_pHGainSpin = new QDoubleSpinBox(m_pHShGroup);
			m_pHGainSpin->setRange(-15.0, 15.0);
			m_pHGainSpin->setSingleStep(0.1);
			m_pHGainSpin->setDecimals(1);
			m_pHGainSpin->setSuffix(" dB");
			connect(m_pHGainSpin, SIGNAL(valueChanged(double)), this, SLOT(onHGainChanged(double)));

			m_pHSlopSpin = new QDoubleSpinBox(m_pHShGroup);
			m_pHSlopSpin->setSuffix(" dB/Oct");
			m_pHSlopSpin->setRange(MIN_SLOPE, MAX_SLOPE);
			m_pHSlopSpin->setSingleStep(0.01);
			m_pHSlopSpin->setDecimals(2);
			connect(m_pHSlopSpin, SIGNAL(valueChanged(double)), this, SLOT(onHSlopeChanged(double)));		
		}
	}

	m_pGEQGroup = new QGroupBox(tr(""), this);
	m_pGEQGroup->setObjectName(QStringLiteral("m_pGEQGroup"));
	{
		m_pGEQWidget = new GEQWidget(m_pGEQGroup);
		connect(m_pGEQWidget->m_pAssistButton, SIGNAL(clicked()), this, SLOT(onEQAssistClicked()));	
		connect(m_pGEQWidget->m_pFlattenButton, SIGNAL(clicked()), this, SLOT(onEQFlattenClicked()));	
		connect(m_pGEQWidget->m_pCopyButton, SIGNAL(clicked()), this, SLOT(onEQCopyClicked()));	
		connect(m_pGEQWidget->m_pPasteButton, SIGNAL(clicked()), this, SLOT(onEQPasteClicked()));	
	}

	m_slotEnableFlag = true;
	m_autoAssistIndex = -1;

	m_pTimer = new QTimer(this);
    connect(m_pTimer, SIGNAL(timeout()), this, SLOT(onTimer_200ms()));
	m_pTimer->start(2000);

	m_pPopupMenu = new QMenu(this);
	m_pCopyAction = new QAction(tr("Copy EQ"), this);  
	m_pPasteAction = new QAction(tr("Paste EQ"), this);
	connect(m_pCopyAction, SIGNAL(triggered()), this, SLOT(onEQCopyClicked()));
	connect(m_pPasteAction, SIGNAL(triggered()), this, SLOT(onEQPasteClicked()));	
	m_pPopupMenu->addAction(m_pCopyAction);
	m_pPopupMenu->addAction(m_pPasteAction);

	refresh();
}

void EQTuningWidget::contextMenuEvent(QContextMenuEvent *event)
{
	CPi2000Data *pData = getCPi2000Data();
	int currentChannelIndex = pData->getCurrentOutputChannelIndex();
	if (currentChannelIndex == 3)
	{
		return;
	}

	QRect groupRect = m_pActiveChannelGroup->geometry();
	QPoint clickPoint = event->pos();
	QRect rect = m_pRadioButton[currentChannelIndex]->geometry();
	
	QRect newRect = rect.translated(groupRect.left(), groupRect.top());

	if (newRect.contains(clickPoint))
	{
//		bool pasteEnable = false;
		m_pCopyAction->setText(tr("Copy EQ"));
		m_pPasteAction->setText(tr("Paste EQ"));
		m_pPasteAction->setEnabled(false);

		if (m_EQCopyIndex != -1)
		{
			if (m_EQCopyIndex != currentChannelIndex)
			{
				QStringList speakerList;
				speakerList << "L" << "R" << "C" << "Sw" << "Ls" << "Rs" << "Bls" << "Brs";

				m_pPasteAction->setText(QString(tr("Paste EQ (From %1)").arg(speakerList.at(m_EQCopyIndex))));
				m_pPasteAction->setEnabled(true);
			}
		}
		m_pPopupMenu->exec(event->globalPos());
	}
}

void EQTuningWidget::resizeEvent(QResizeEvent * /*event*/)
{
	int h1 = 100;
	int h2 = 100 + height() / 10;
	int h3 = height() - h1 - h2;

	int w = width() / 5;
	m_pSignalModeGroup->setGeometry(0, 0, w, h1);
	{
		m_pSignalLabel->setGeometry(w / 2 - 60, h1 / 2 - 30, 120, 20);
		m_pSignalModeCombo->setGeometry(w / 2 - 60, h1 / 2 - 10, 120, 25);
		m_pReminderLabel->setGeometry(w / 2 - 90,  h1 / 2 - 0, 180, 50);
	}

	m_pActiveChannelGroup->setGeometry(w, 0, w * 3, h1);
	{
		int labelPosx[] = { 25, 55, 40, 50, 10, 70, 20, 60 };
		int labelPosy[] = { 10, 10, 10, 50, 50, 50, 78, 78 };

		int offsetx[] = {-15, -15, -15, -15, -15, -15, -15, -15 };
		int offsety[] = {0, 0, 0, 0, 0, 0, 0, 0 };
		int unitWidth = w * 3;
		for (int i = 0; i < 8; i++)
		{
//			m_pChannelLabel[i]->setGeometry(unitWidth * labelPosx[i] / 100 - 10, labelPosy[i] * h1 / 100, 30, 20);
			m_pRadioButton[i]->setGeometry(unitWidth * labelPosx[i] / 100 + offsetx[i] - 22 , labelPosy[i] * h1 / 100 + offsety[i], 100, 20);		
		}

		m_pCurrentChannelLabel->setGeometry(unitWidth - 160, 10, 120, 70);
	}
	m_pChannelLevelGroup->setGeometry(w * 4, 0, width() - 4 * w, h1 + h2);
	{
		int volumeWidth = 160;
		m_pChannelLevelLabel->setGeometry(0, 3, width() - 4 * w, 20);
		m_pVolumeGroup->setGeometry((w - volumeWidth) / 2, 5, volumeWidth, h1 + h2 - 5);
	}

#if 0

	{
		int w = width() / 3 - 48;
		int h = height() - PEQHeight - 80;
		m_pEQAssistGroup->setGeometry(leftMargin * 3 + width() / 3, topMargin, w, h);

		int buttonWidth = 120;
		int buttonHeight = 30;
		int margin = (w - buttonWidth * 2) / 3;

		int left = margin + buttonWidth  / 2;
		int right = 2 * margin + buttonWidth * 3 / 2;
		int bottom = h / 3;
		int top = h * 2 / 3;


		m_pAssistButton->setGeometry(left - buttonWidth / 2, top - buttonHeight / 2, buttonWidth, buttonHeight);
		m_pFlattenButton->setGeometry(right - buttonWidth / 2, top - buttonHeight / 2, buttonWidth, buttonHeight);
		m_pCopyButton->setGeometry(left - buttonWidth / 2, bottom - buttonHeight / 2, buttonWidth, buttonHeight);
		m_pPasteButton->setGeometry(right - buttonWidth / 2, bottom - buttonHeight / 2, buttonWidth, buttonHeight);

	}
#endif
	{
//	QGroupBox *m_pGEQGroup;	/* Low-shelf and High-Shelf */
		m_pPEQGroup->setGeometry(0, h1, w * 4, h2);

		m_pLShGroup->setGeometry(0, 0, w * 2, h2);
		{
			int spinWidth = 100;
			int unitHeight = h2;
//			int left = w * 2 / 3;
			int marginHigh = (unitHeight - 20 * 4 - 10) / 4;
			int topHeigh = marginHigh + 24;

			int leftMargin = w / 3;
			m_pLShelfLabel->setGeometry(w, topHeigh - (marginHigh + 15),  120, 20);

			m_pLGainSliderLabel->setGeometry(leftMargin, 10, w / 2, 20);
			m_pLShelfGainSlider->setGeometry(leftMargin, 30, w / 2, h2 - 40);	

			m_pLGainLabel->setGeometry(w - 60, topHeigh,  spinWidth, 20);
			m_pLGainSpin->setGeometry(w + 45, topHeigh,  spinWidth, 20);

			m_pLGreqLabel->setGeometry(w - 60, topHeigh + (marginHigh + 24),  spinWidth, 20);
			m_pLFreqSpin->setGeometry(w + 45, topHeigh + (marginHigh + 24),  spinWidth, 20);

			m_pLSlopeLabel->setGeometry(w - 60, topHeigh + (marginHigh + 24) * 2,  spinWidth, 20);	
			m_pLSlopSpin->setGeometry(w + 45, topHeigh + (marginHigh + 24) * 2,  spinWidth, 20);
		}

		m_pHShGroup->setGeometry(w * 2, 0, w * 2, h2);
		{
			int spinWidth = 100;
			int unitHeight = h2;
//			int left = w * 2 / 3;
			int marginHigh = (unitHeight - 20 * 4 - 10) / 4;
			int topHeigh = marginHigh + 24;
			
			int leftMargin = w / 3;
			m_pHShelfLabel->setGeometry(w, topHeigh - (marginHigh + 15),  120, 20);

			m_pHGainSliderLabel->setGeometry(leftMargin, 10, w / 2, 20);
			m_pHShelfGainSlider->setGeometry(leftMargin, 30, w / 2, h2 - 40);	

			m_pHGainLabel->setGeometry(w - 60, topHeigh,  spinWidth, 20);
			m_pHGainSpin->setGeometry(w + 45, topHeigh,  spinWidth, 20);

			m_pHGreqLabel->setGeometry(w - 60, topHeigh + (marginHigh + 24),  spinWidth, 20);
			m_pHFreqSpin->setGeometry(w + 45, topHeigh + (marginHigh + 24),  spinWidth, 20);

			m_pHSlopeLabel->setGeometry(w - 60, topHeigh + (marginHigh + 24) * 2,  spinWidth, 20);	
			m_pHSlopSpin->setGeometry(w + 45, topHeigh + (marginHigh + 24) * 2,  spinWidth, 20);
		}
	}
#if 0
		{
			int w = width() / 3 - 48;
			int h = height() - PEQHeight - 80;
	//		m_pSubwooferGroup->setGeometry(leftMargin * 3 + width() / 3, topMargin, w, h);

//			m_pCenterBox->setGeometry(w / 2 - 55 , h / 10 + w / 10, 140, 20);
//			m_pInverseBox->setGeometry(w / 2 - 55, (h / 10 + w / 10) * 1.9, 140, 20);

//			m_pInfoLabel->setGeometry(20, 80, w - 40, h - 90);
		}
#endif
		m_pSubwooferPEQGroup1->setGeometry(0, h1, w * 2, h2);
		{
			int leftMargin = w / 3;
//			int spinWidth = 100;
			int unitHeight = h2;
//			int left = w * 2 / 3;
			int marginHigh = (unitHeight - 20 * 4 - 10) / 4;
			int topHeigh = marginHigh + 24;
			m_pSWPEQ1Label->setGeometry(w, topHeigh - (marginHigh + 15),  120, 20);

			m_pSWPEQGainLabel1->setGeometry(leftMargin, 10, w / 2, 20);
			m_pSWPEQGainSlider1->setGeometry(leftMargin, 30, w / 2, h2 - 40);	

			m_pTypeLabel1->setGeometry(w - 60, h2 * 5 / 20, 100, 20);
			m_pGainLabel1->setGeometry(w - 60, h2 * 9 / 20, 100, 20);
			m_pFreqLabel1->setGeometry(w - 60, h2 * 13 / 20, 100, 20);
			m_pQLabel1->setGeometry(w - 60, h2 * 17 / 20, 100, 20);

			m_pBellCombo1->setGeometry(w + 45, h2 * 5 / 20 - 2, 100, 20);
			m_pPEQCutSpin1->setGeometry(w + 45, h2 * 9 / 20 - 2, 100, 20);
			m_pPEQFreqSpin1->setGeometry(w + 45, h2  * 13/ 20 - 2, 100, 20);
			m_pQSpin1->setGeometry(w + 45, h2 * 17/ 20 - 2, 100, 20);
		}

		m_pSubwooferPEQGroup2->setGeometry(w * 2, h1, w * 2, h2);
		{
			int leftMargin = w / 3;
//			int spinWidth = 100;
			int unitHeight = h2;
//			int left = w * 2 / 3;
			int marginHigh = (unitHeight - 20 * 4 - 10) / 4;
			int topHeigh = marginHigh + 24;

			m_pSWPEQ2Label->setGeometry(w, topHeigh - (marginHigh + 15),  120, 20);

			m_pSWPEQGainLabel2->setGeometry(leftMargin, 10, w / 2, 20);
			m_pSWPEQGainSlider2->setGeometry(leftMargin, 30, w / 2, h2 - 40);	

			m_pTypeLabel2->setGeometry(w - 60, h2 * 5/ 20, 100, 20);
			m_pGainLabel2->setGeometry(w - 60, h2 * 8.67 / 20, 100, 20);
			m_pFreqLabel2->setGeometry(w - 60, h2 * 12.33 / 20, 100, 20);
			m_pQLabel2->setGeometry(w - 60, h2 * 16 / 20, 100, 20);

			m_pBellCombo2->setGeometry(w + 45, h2 * 5/ 20 - 2, 80, 20);
			m_pPEQCutSpin2->setGeometry(w + 45, h2 * 8.67 / 20 - 2, 80, 20);
			m_pPEQFreqSpin2->setGeometry(w + 45, h2  * 12.33/ 20 - 2, 80, 20);
			m_pQSpin2->setGeometry(w + 45, h2 * 16/ 20 - 2, 80, 20);
		}


	{
//		int leftMargin = 2;
		m_pGEQGroup->setGeometry(0, h1 + h2, width(), h3);
		m_pGEQWidget->setGeometry(0, 0, width(), h3 - 2);
	}
}


void EQTuningWidget::refreshActiveChannel()
{
	bool oldSlotEnableFlag = m_slotEnableFlag;
	m_slotEnableFlag = false;

	CPi2000Data *pData = getCPi2000Data();
	int currentChannelIndex = pData->getCurrentOutputChannelIndex();
	for (int i = 0; i < 8; i++)
	{
		m_pRadioButton[i]->setChecked(currentChannelIndex == i); 
	}

	QString channelName[8] = { "L", "R", "C", "Sw", "Ls", "Rs", "Bls", "Brs" };
	m_pCurrentChannelLabel->setText(channelName[currentChannelIndex]);

	m_slotEnableFlag = oldSlotEnableFlag;
}

void EQTuningWidget::refresh()
{
	bool oldSlotEnableFlag = m_slotEnableFlag;
	m_slotEnableFlag = false;

	CPi2000Data *pData = getCPi2000Data();
	RTAData *pRTA = pData->getRTA();
	if (m_pSignalModeCombo->currentIndex() != (int)pRTA->getSignalMode())
	{
		m_pSignalModeCombo->setCurrentIndex((int)pRTA->getSignalMode());
	}
	if (pRTA->getSignalMode() == SIGNAL_OFF)
	{
		m_pReminderLabel->hide();
	}
	else
	{
//		m_pReminderLabel->show();
	}

	refreshActiveChannel();

	refreshChannelLevel();

	refreshGEQGroup();

	refreshPEQ();

	refreshGEQ();

	m_slotEnableFlag = oldSlotEnableFlag;
}

void EQTuningWidget::refreshGEQ()
{
	m_pGEQWidget->refreshGEQ();
}

void EQTuningWidget::refreshPEQ()
{
	bool oldSlotEnableFlag = m_slotEnableFlag;
	m_slotEnableFlag = false;


	CPi2000Data *pData = getCPi2000Data();
	int currentChannelIndex = pData->getCurrentOutputChannelIndex();

	if (currentChannelIndex == 3)
	{
		m_pSubwooferPEQGroup2->show();
		m_pPEQGroup->hide();

		refreshPEQ_SW();
	}
	else
	{
		m_pSubwooferPEQGroup2->hide();
		m_pPEQGroup->show();

		refreshPEQ_Normal();
	}

	m_slotEnableFlag = oldSlotEnableFlag;
}

void EQTuningWidget::refreshPEQ_SW()
{
	bool oldSlotEnableFlag = m_slotEnableFlag;
	m_slotEnableFlag = false;

	CPi2000Data *pData = getCPi2000Data();
	OutputChannel *pChannel = pData->getCurrentOutputChannel();

	EQData *pEQ = pChannel->getHShelf();	/* We will use m_hShelf as PEQ */
	m_pPEQCutSpin2->setValue(pEQ->getGain());
	m_pPEQFreqSpin2->setValue(pEQ->getFrequency());
	m_pQSpin2->setValue(pEQ->getQ());
	m_pSWPEQGainSlider2->setValue(qRound(pEQ->getGain() * 10));

	m_slotEnableFlag = oldSlotEnableFlag;
}

void EQTuningWidget::refreshPEQ_Normal()
{
	bool oldSlotEnableFlag = m_slotEnableFlag;
	m_slotEnableFlag = false;

	CPi2000Data *pData = getCPi2000Data();
	OutputChannel *pChannel = pData->getCurrentOutputChannel();

	EQData *pEQ = pChannel->getLShelf();

	qreal freq = pEQ->getFrequency();
	m_pLFreqSpin->setValue(freq);
	if (freq <= 200)
	{
		m_pLFreqSpin->setSingleStep(0.1);
	}
	else if (freq <= 2000)
	{
		m_pLFreqSpin->setSingleStep(1);
	}
	else
	{
		m_pLFreqSpin->setSingleStep(10);
	}


	m_pLShelfGainSlider->setValue(qRound(pEQ->getGain() * 10));
	m_pLGainSpin->setValue(pEQ->getGain());
	m_pLSlopSpin->setValue(pEQ->getSlope());

	pEQ = pChannel->getHShelf();
	freq = pEQ->getFrequency();
	m_pHFreqSpin->setValue(freq);
	if (freq <= 200)
	{
		m_pHFreqSpin->setSingleStep(0.1);
	}
	else if (freq <= 2000)
	{
		m_pHFreqSpin->setSingleStep(1);
	}
	else
	{
		m_pHFreqSpin->setSingleStep(10);
	}

	m_pHShelfGainSlider->setValue(qRound(pEQ->getGain() * 10));
	m_pHGainSpin->setValue(pEQ->getGain());
	m_pHSlopSpin->setValue(pEQ->getSlope());

	m_slotEnableFlag = oldSlotEnableFlag;
}

void EQTuningWidget::refreshGEQGroup_SW()
{
	bool oldSlotEnableFlag = m_slotEnableFlag;
	m_slotEnableFlag = false;

	CPi2000Data *pData = getCPi2000Data();
	OutputChannel *pChannel = pData->getCurrentOutputChannel();

	EQData *pEQ = pChannel->getLShelf();	/* We will use m_lShelf as PEQ1 */
	m_pPEQCutSpin1->setValue(pEQ->getGain());
	m_pPEQFreqSpin1->setValue(pEQ->getFrequency());
	m_pQSpin1->setValue(pEQ->getQ());
	m_pSWPEQGainSlider1->setValue(qRound(pEQ->getGain() * 10));

	m_slotEnableFlag = oldSlotEnableFlag;
}

void EQTuningWidget::refreshGEQGroup()
{
	bool oldSlotEnableFlag = m_slotEnableFlag;
	m_slotEnableFlag = false;


	CPi2000Data *pData = getCPi2000Data();
	int currentChannelIndex = pData->getCurrentOutputChannelIndex();

	if (currentChannelIndex == 3)
	{
		m_pSubwooferPEQGroup1->show();
		m_pGEQWidget->m_pEQAssistGroup->hide();

		refreshGEQGroup_SW();
	}
	else
	{
		m_pSubwooferPEQGroup1->hide();
		m_pGEQWidget->m_pEQAssistGroup->show();

		QStringList speakerList;
		speakerList << "L" << "R" << "C" << "Sw" << "Ls" << "Rs" << "Bls" << "Brs";

		m_pGEQWidget->m_pPasteButton->setToolTip("");
		if (m_EQCopyIndex == -1)
		{
			m_pGEQWidget->m_pPasteButton->setText(tr("Paste EQ"));
			m_pGEQWidget->m_pPasteButton->setEnabled(false);
		}
		else
		{
//			m_pGEQWidget->m_pPasteButton->setText(QString(tr("Paste EQ (From %1)").arg(speakerList.at(m_EQCopyIndex))));
			m_pGEQWidget->m_pPasteButton->setEnabled(m_EQCopyIndex != currentChannelIndex);
			if (m_EQCopyIndex != currentChannelIndex)
			{
				m_pGEQWidget->m_pPasteButton->setToolTip(QString(tr("Paste EQ (From %1)").arg(speakerList.at(m_EQCopyIndex))));
			}
		}
	}

	m_slotEnableFlag = oldSlotEnableFlag;
}

void EQTuningWidget::refreshChannelLevel()
{
	bool oldSlotEnableFlag = m_slotEnableFlag;
	m_slotEnableFlag = false;

	CPi2000Data *pData = getCPi2000Data();
//	int currentChannelIndex = pData->getCurrentOutputChannelIndex();

	OutputChannel *pChannel = pData->getCurrentOutputChannel();

	m_pVolumeGroup->setVolume(pChannel->getRoomLevelGain());
	m_pVolumeGroup->setMuteFlag(pChannel->getRoomLevelMute());

	m_slotEnableFlag = oldSlotEnableFlag;
}

void EQTuningWidget::onCurrentChannelToggled_0(bool flag) { toggleCurrentChannel(0, flag); }
void EQTuningWidget::onCurrentChannelToggled_1(bool flag) { toggleCurrentChannel(1, flag); }
void EQTuningWidget::onCurrentChannelToggled_2(bool flag) { toggleCurrentChannel(2, flag); }
void EQTuningWidget::onCurrentChannelToggled_3(bool flag) { toggleCurrentChannel(3, flag); }
void EQTuningWidget::onCurrentChannelToggled_4(bool flag) { toggleCurrentChannel(4, flag); }
void EQTuningWidget::onCurrentChannelToggled_5(bool flag) { toggleCurrentChannel(5, flag); }
void EQTuningWidget::onCurrentChannelToggled_6(bool flag) { toggleCurrentChannel(6, flag); }
void EQTuningWidget::onCurrentChannelToggled_7(bool flag) { toggleCurrentChannel(7, flag); }

void EQTuningWidget::onChangeVolume(float gain)
{
	if (m_slotEnableFlag == false)
	{
		return;
	}

	CPi2000Data *pData = getCPi2000Data();
	OutputChannel *pOutputChannel = pData->getCurrentOutputChannel();
	int index = pData->getCurrentOutputChannelIndex();
	pOutputChannel->setRoomLevelGain(gain);
	
	if (g_pApp->getConnectStatus() == DEVICE_NETWORK_CONNECTED)
	{
		g_pApp->getDeviceConnection()->setSpeakerVolume(index, gain);
	}	
}

void EQTuningWidget::onToggleMuteButton(bool flag)
{
	if (m_slotEnableFlag == false)
	{
		return;
	}

	CPi2000Data *pData = getCPi2000Data();
	OutputChannel *pOutputChannel = pData->getCurrentOutputChannel();
	int index = pData->getCurrentOutputChannelIndex();
	pOutputChannel->setRoomLevelMute(flag);
	
	if (g_pApp->getConnectStatus() == DEVICE_NETWORK_CONNECTED)
	{
		g_pApp->getDeviceConnection()->setSpeakerMuteFlag(index, flag);
	}	
}

void EQTuningWidget::toggleCurrentChannel(int index, bool flag)
{
	if (m_slotEnableFlag == false)
	{
		return;
	}

	if (flag == true)
	{
		CPi2000Data *pData = getCPi2000Data();
		pData->setCurrentOutputChannelIndex(index);

		RTAData *pRTA = pData->getRTA();
		if (pRTA->getRotate() == true)
		{
			pRTA->setRotate(false);
		}

		bool channelFlag[8] = {false, false, false, false, false, false, false, false};
		channelFlag[index] = true;
		pRTA->setEQTuningPinkNoise(index);
		pRTA->setRoomLevelPinkNoise(channelFlag);
		if (g_pApp->getConnectStatus() == DEVICE_NETWORK_CONNECTED)
		{
			g_pApp->getDeviceConnection()->setPinkNoise(channelFlag);
		}
		refresh();
	}
}

void EQTuningWidget::onTimer_200ms(void)
{
	if (m_autoAssistIndex >= 0)
	{
		autoAssist(m_autoAssistIndex);
		m_autoAssistIndex++;
	
		if (m_autoAssistIndex >= 20)
		{
			m_autoAssistIndex = -1;
			onEQAssistStoped();
		}
	}
#if 0
	static int i = 0;

	i++;
	if (i == 5)
	{
		i = 0;
		CPi2000Data *pData = getCPi2000Data();
		RTAData *pRTA = pData->getRTA();

		/* 		---get RTA meter value---
					asyncget "\\Preset\RTA\SV\40 Hz"
					asyncget "\\Preset\RTA\SV\50 Hz"
					...
					asyncget "\\Preset\RTA\SV\1.0 kHz"
					...
					asyncget "\\Preset\RTA\SV\16.0 kHz"
					asyncget "\\Preset\RTA\SV\20.0 kHz"
		*/

		if (pRTA->getSignalMode() != SIGNAL_OFF)
		{
			if (g_pApp->getConnectStatus() == DEVICE_NETWORK_CONNECTED)
			{
				g_pApp->getDeviceConnection()->getRTAMeter();
			}
		}
	}
#endif
}

void EQTuningWidget::onSignalModeSelected(int currentMode)
{
	if (m_slotEnableFlag == false)
	{
		return;
	}

	emit g_pApp->setSignalMode(currentMode);

#if 0
	DeviceSocket *pSocket = g_pApp->getDeviceConnection();

	CPi2000Data *pData = getCPi2000Data();
	RTAData *pRTA = pData->getRTA();
	SIGNAL_MODE oldMode = pRTA->getSignalMode();
	pRTA->setSignalMode((SIGNAL_MODE)currentMode);

	int currentOutputIndex = pData->getCurrentOutputChannelIndex();

	if (pRTA->getSignalMode() == SIGNAL_OFF)
	{
		m_pReminderLabel->hide();
		pData->setRTAAverageLevel(-90.0);
		pData->setRTACenterLevel(-90.0);

		float currentValue[30]; 
		for (int i = 0; i < 30; i++)
		{
			currentValue[i] = -90.0;
		}
		pRTA->setRTAValue(currentValue);
		emit g_pApp->setRTAValue();
		m_pGEQWidget->update();
	}
	else
	{
//		m_pReminderLabel->show();
	}

	if ((oldMode == SIGNAL_OFF) && (currentMode != SIGNAL_OFF))
	{
		pRTA->setMeasuredValue(0.0);
		pRTA->setRotate(false);
		pRTA->setPinkNoise(currentOutputIndex);

		// Start RTA
		if (g_pApp->getConnectStatus() == DEVICE_NETWORK_CONNECTED)
		{
			pSocket->startSignal();
			pSocket->setPinkNoise(currentOutputIndex);
		} 

		g_pMainWidget->startSignalGenerator();
	}
	else if ((oldMode != SIGNAL_OFF) && (currentMode == SIGNAL_OFF))
	{
		/* Change Input source to Digital if current is Mic */
		if (pData->getInputSource() == INPUT_SOURCE_MIC)
		{
			pData->setInputSource(INPUT_SOURCE_DIGITAL);
			if (g_pApp->getConnectStatus() == DEVICE_NETWORK_CONNECTED)
			{
				g_pApp->getDeviceConnection()->setInputSource(INPUT_SOURCE_DIGITAL);
			}
			g_pMainWidget->refreshInputSource();
		}

		pRTA->setMeasuredValue(0.0);
		pRTA->setRotate(false);
		pRTA->setPinkNoise(-1);
		pRTA->setSPLValue(-60.0);
		pRTA->setMeasuredValue(0.0);
		pRTA->setAddValue(0.0);

		// Stop RTA
		if (g_pApp->getConnectStatus() == DEVICE_NETWORK_CONNECTED)
		{
//			pSocket->setFadeInOutMuteFlag(true);
			pSocket->setPinkNoise(-1);
			pSocket->stopSignal();

//			g_pApp->msleep(5000);
//			pSocket->setFadeInOutMuteFlag(false);
		}
		g_pMainWidget->stopSignalGenerator();
	}

	if (g_pApp->getConnectStatus() == DEVICE_NETWORK_CONNECTED)
	{
//		if (currentMode != SIGNAL_OFF)
		{
			pSocket->setSignalMode((SIGNAL_MODE)currentMode);
		}
	}
	refresh();
#endif
}

void EQTuningWidget::onLFreqChanged(double freq)
{
	if (m_slotEnableFlag == false)
	{
		return;
	}

	DeviceSocket *pSocket = g_pApp->getDeviceConnection();

	CPi2000Data *pData = getCPi2000Data();
	int channelIndex = pData->getCurrentOutputChannelIndex();
	OutputChannel *pCurrentChannel = pData->getCurrentOutputChannel();
	EQData *pEQ = pCurrentChannel->getLShelf();
	pEQ->setFrequency(freq);

	if (g_pApp->getConnectStatus() == DEVICE_NETWORK_CONNECTED)
	{
		pSocket->setLowShelfFreq(channelIndex, freq);
	}
}

void EQTuningWidget::onLGainChanged(double gain)
{
	if (m_slotEnableFlag == false)
	{
		return;
	}

	DeviceSocket *pSocket = g_pApp->getDeviceConnection();

	CPi2000Data *pData = getCPi2000Data();
	int channelIndex = pData->getCurrentOutputChannelIndex();
	OutputChannel *pCurrentChannel = pData->getCurrentOutputChannel();
	EQData *pEQ = pCurrentChannel->getLShelf();
	pEQ->setGain(gain);

	if (g_pApp->getConnectStatus() == DEVICE_NETWORK_CONNECTED)
	{
		pSocket->setLowShelfGain(channelIndex, gain);
	}
	refreshPEQ_Normal();
}

void EQTuningWidget::onLSlopeChanged(double slope)
{
	if (m_slotEnableFlag == false)
	{
		return;
	}

	DeviceSocket *pSocket = g_pApp->getDeviceConnection();

	CPi2000Data *pData = getCPi2000Data();
	int channelIndex = pData->getCurrentOutputChannelIndex();
	OutputChannel *pCurrentChannel = pData->getCurrentOutputChannel();
	EQData *pEQ = pCurrentChannel->getLShelf();
	pEQ->setSlope(slope);

	if (g_pApp->getConnectStatus() == DEVICE_NETWORK_CONNECTED)
	{
		pSocket->setLowShelfSlope(channelIndex, slope);
	}
}

void EQTuningWidget::onHFreqChanged(double freq)
{
	if (m_slotEnableFlag == false)
	{
		return;
	}

	DeviceSocket *pSocket = g_pApp->getDeviceConnection();

	CPi2000Data *pData = getCPi2000Data();
	int channelIndex = pData->getCurrentOutputChannelIndex();
	OutputChannel *pCurrentChannel = pData->getCurrentOutputChannel();
	EQData *pEQ = pCurrentChannel->getHShelf();
	pEQ->setFrequency(freq);

	if (g_pApp->getConnectStatus() == DEVICE_NETWORK_CONNECTED)
	{
		pSocket->setHighShelfFreq(channelIndex, freq);
	}
}

void EQTuningWidget::onHGainChanged(double gain)
{
	if (m_slotEnableFlag == false)
	{
		return;
	}

	DeviceSocket *pSocket = g_pApp->getDeviceConnection();

	CPi2000Data *pData = getCPi2000Data();
	int channelIndex = pData->getCurrentOutputChannelIndex();
	OutputChannel *pCurrentChannel = pData->getCurrentOutputChannel();
	EQData *pEQ = pCurrentChannel->getHShelf();
	pEQ->setGain(gain);

	if (g_pApp->getConnectStatus() == DEVICE_NETWORK_CONNECTED)
	{
		pSocket->setHighShelfGain(channelIndex, gain);
	}
	refreshPEQ_Normal();
}

void EQTuningWidget::onHSlopeChanged(double slope)
{
	if (m_slotEnableFlag == false)
	{
		return;
	}

	DeviceSocket *pSocket = g_pApp->getDeviceConnection();

	CPi2000Data *pData = getCPi2000Data();
	int channelIndex = pData->getCurrentOutputChannelIndex();
	OutputChannel *pCurrentChannel = pData->getCurrentOutputChannel();
	EQData *pEQ = pCurrentChannel->getHShelf();
	pEQ->setSlope(slope);

	if (g_pApp->getConnectStatus() == DEVICE_NETWORK_CONNECTED)
	{
		pSocket->setHighShelfSlope(channelIndex, slope);
	}
}

void EQTuningWidget::onEQAssistClicked()
{
	m_pGEQWidget->m_pAssistButton->setEnabled(false);
	g_waittingWidget.startWaitting(this);
	setAutoAssistIndex(0);
}

void EQTuningWidget::onEQAssistStoped()
{
	m_pGEQWidget->m_pAssistButton->setEnabled(true);
	g_waittingWidget.stopWaitting();
}

void EQTuningWidget::setAutoAssistIndex(int index)
{
	m_autoAssistIndex = index;
}

#define MAX_DB_PER_STEP 0.5
void EQTuningWidget::autoAssist(int adjustIndex)
{
	double meterVal[30];
	double target[30]={-4,-3,-2,-1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,-1,-2,-3,-4,-5,-6,-7,-9,-11};
	double geqGain[30];
	int index;

	qDebug() << "auto Assist = " << m_autoAssistIndex;

	CPi2000Data *pData = getCPi2000Data();
//	OutputChannel *pChannel = pData->getCurrentOutputChannel();
	int currentOutputChannelIndex = pData->getCurrentOutputChannelIndex();
	RTAData *pRTA = pData->getRTA();
	float *pRTAMeter = pRTA->getRTAValue();

	for (index = 0; index < 30; index++)
	{
		geqGain[index] = 0;
		meterVal[index] = (double)pRTAMeter[index];
	}

	//actual algorithm example:
	AdaptiveTuning test;
	test.calculateAutomatedGain(meterVal,target,geqGain,0.5, 100,true);

	for (index = 0; index < 27; index++)
	{
#if 0
		float gain = pData->getGEQ(currentOutputChannelIndex, index);
		gain += geqGain[3 + index];
#elif 0
		float gain = geqGain[3 + index];
#else
		float oldGain = pData->getGEQ(currentOutputChannelIndex, index);
		float gain = geqGain[3 + index];
		if (adjustIndex != 0)	/* The first time, we will adjust the GEQ without any limitation */
		{
			if (gain > oldGain)
			{
				if (gain > oldGain + MAX_DB_PER_STEP)
				{
					gain = oldGain + MAX_DB_PER_STEP;
				}
			}
			else if (gain < oldGain)
			{
				if (gain < oldGain - MAX_DB_PER_STEP)
				{
					gain = oldGain - MAX_DB_PER_STEP;
				}
			}
		}
#endif
		if (gain > 12)
		{
			gain = 12;
		}
		if (gain < -12)
		{
			gain = -12;
		}
		pData->setGEQ(currentOutputChannelIndex, index, gain);

		if (g_pApp->getConnectStatus() == DEVICE_NETWORK_CONNECTED)
		{
			g_pApp->getDeviceConnection()->setGEQ(currentOutputChannelIndex, index, gain);
			g_pApp->msleep(10);
		}
	}

	m_pGEQWidget->refreshGEQ();
}

void EQTuningWidget::onEQFlattenClicked()
{
	if (m_slotEnableFlag == false)
	{
		return;
	}

	CPi2000Data *pData = getCPi2000Data();
	OutputChannel *pChannel = pData->getCurrentOutputChannel();
	int currentOutputChannelIndex = pData->getCurrentOutputChannelIndex();

	g_waittingWidget.startWaitting(this);

	for (int index = 0; index < 27; index++)
	{
		pChannel->setGEQ(index, 0.0);

		if (g_pApp->getConnectStatus() == DEVICE_NETWORK_CONNECTED)
		{
			g_pApp->getDeviceConnection()->setGEQ(currentOutputChannelIndex, index, 0.0);
			g_pApp->msleep(50);
		}
	}
	m_pGEQWidget->refreshGEQ();

	g_waittingWidget.stopWaitting();
}

void EQTuningWidget::onEQCopyClicked()
{
	CPi2000Data *pData = getCPi2000Data();
	int currentOutputChannelIndex = pData->getCurrentOutputChannelIndex();

	m_EQCopyIndex = currentOutputChannelIndex;
	refreshGEQGroup();
}

void EQTuningWidget::onEQPasteClicked()
{
	if (m_slotEnableFlag == false)
	{
		return;
	}

	CPi2000Data *pData = getCPi2000Data();
	OutputChannel *pChannel = pData->getCurrentOutputChannel();
	int currentOutputChannelIndex = pData->getCurrentOutputChannelIndex();

	g_waittingWidget.startWaitting(this);

	for (int index = 0; index < 27; index++)
	{
		float gain = pData->getGEQ(m_EQCopyIndex, index);
		pChannel->setGEQ(index, gain);

		if (g_pApp->getConnectStatus() == DEVICE_NETWORK_CONNECTED)
		{
			g_pApp->getDeviceConnection()->setGEQ(currentOutputChannelIndex, index, gain);
			g_pApp->msleep(50);
		}
	}
	m_pGEQWidget->refreshGEQ();

	g_waittingWidget.stopWaitting();
}

void EQTuningWidget::onSWPEQGainSliderChanged2(int gain_10x)
{
	if (m_slotEnableFlag == false)
	{
		return;
	}

	CPi2000Data *pData = getCPi2000Data();
	OutputChannel *pChannel = pData->getCurrentOutputChannel();

	EQData *pEQ = pChannel->getHShelf();	/* We will use m_hShelf as PEQ */
	pEQ->setGain((float)gain_10x / 10);

	DeviceSocket *pSocket = g_pApp->getDeviceConnection();
	if (g_pApp->getConnectStatus() == DEVICE_NETWORK_CONNECTED)
	{
		pSocket->setSWBellCut(2, (float)gain_10x / 10);
	}
	refreshPEQ_SW();
}

void EQTuningWidget::onPEQCutChanged2(double gain)
{
	if (m_slotEnableFlag == false)
	{
		return;
	}

	CPi2000Data *pData = getCPi2000Data();
	OutputChannel *pChannel = pData->getCurrentOutputChannel();

	EQData *pEQ = pChannel->getHShelf();	/* We will use m_hShelf as PEQ */
	pEQ->setGain(gain);

	DeviceSocket *pSocket = g_pApp->getDeviceConnection();
	if (g_pApp->getConnectStatus() == DEVICE_NETWORK_CONNECTED)
	{
		pSocket->setSWBellCut(2, gain);
	}
	refreshPEQ_SW();

}

void EQTuningWidget::onPEQFreqChanged2(double freq)
{
	if (m_slotEnableFlag == false)
	{
		return;
	}

	CPi2000Data *pData = getCPi2000Data();
	OutputChannel *pChannel = pData->getCurrentOutputChannel();

	EQData *pEQ = pChannel->getHShelf();	/* We will use m_hShelf as PEQ */
	pEQ->setFrequency(freq);

	DeviceSocket *pSocket = g_pApp->getDeviceConnection();
	if (g_pApp->getConnectStatus() == DEVICE_NETWORK_CONNECTED)
	{
		pSocket->setSWBellFreq(2, freq);
	}
}

void EQTuningWidget::onPEQQChanged2(double Q)
{
	if (m_slotEnableFlag == false)
	{
		return;
	}

	CPi2000Data *pData = getCPi2000Data();
	OutputChannel *pChannel = pData->getCurrentOutputChannel();

	EQData *pEQ = pChannel->getHShelf();	/* We will use m_hShelf as PEQ */
	pEQ->setQ(Q);

	DeviceSocket *pSocket = g_pApp->getDeviceConnection();
	if (g_pApp->getConnectStatus() == DEVICE_NETWORK_CONNECTED)
	{
		pSocket->setSWBellQ(2, Q);
	}
}

void EQTuningWidget::onLShelfGainSliderChanged(int gain_10x)
{
	if (m_slotEnableFlag == false)
	{
		return;
	}

	CPi2000Data *pData = getCPi2000Data();

	int channelIndex = pData->getCurrentOutputChannelIndex();
	OutputChannel *pCurrentChannel = pData->getCurrentOutputChannel();
	EQData *pEQ = pCurrentChannel->getLShelf();
	pEQ->setGain((float)gain_10x / 10);

	DeviceSocket *pSocket = g_pApp->getDeviceConnection();
	if (g_pApp->getConnectStatus() == DEVICE_NETWORK_CONNECTED)
	{
		pSocket->setLowShelfGain(channelIndex, (float)gain_10x / 10);
	}
	refreshPEQ_Normal();
}

void EQTuningWidget::onHShelfGainSliderChanged(int gain_10x)
{
	if (m_slotEnableFlag == false)
	{
		return;
	}

	CPi2000Data *pData = getCPi2000Data();

	int channelIndex = pData->getCurrentOutputChannelIndex();
	OutputChannel *pCurrentChannel = pData->getCurrentOutputChannel();
	EQData *pEQ = pCurrentChannel->getHShelf();
	pEQ->setGain((float)gain_10x / 10);

	DeviceSocket *pSocket = g_pApp->getDeviceConnection();
	if (g_pApp->getConnectStatus() == DEVICE_NETWORK_CONNECTED)
	{
		pSocket->setHighShelfGain(channelIndex, (float)gain_10x / 10);
	}
	refreshPEQ_Normal();
}

void EQTuningWidget::onSWPEQGainSliderChanged1(int gain_10x)
{
	if (m_slotEnableFlag == false)
	{
		return;
	}

	CPi2000Data *pData = getCPi2000Data();
	OutputChannel *pChannel = pData->getCurrentOutputChannel();

	EQData *pEQ = pChannel->getLShelf();	/* We will use m_hShelf as PEQ */
	pEQ->setGain((float)gain_10x / 10);

	DeviceSocket *pSocket = g_pApp->getDeviceConnection();
	if (g_pApp->getConnectStatus() == DEVICE_NETWORK_CONNECTED)
	{
		pSocket->setSWBellCut(1, (float)gain_10x / 10);
	}
	refreshGEQGroup_SW();
}

void EQTuningWidget::onPEQCutChanged1(double gain)
{
	if (m_slotEnableFlag == false)
	{
		return;
	}

	CPi2000Data *pData = getCPi2000Data();
	OutputChannel *pChannel = pData->getCurrentOutputChannel();

	EQData *pEQ = pChannel->getLShelf();	/* We will use m_hShelf as PEQ */
	pEQ->setGain(gain);

	DeviceSocket *pSocket = g_pApp->getDeviceConnection();
	if (g_pApp->getConnectStatus() == DEVICE_NETWORK_CONNECTED)
	{
		pSocket->setSWBellCut(1, gain);
	}
	refreshGEQGroup_SW();

}

void EQTuningWidget::onPEQFreqChanged1(double freq)
{
	if (m_slotEnableFlag == false)
	{
		return;
	}

	CPi2000Data *pData = getCPi2000Data();
	OutputChannel *pChannel = pData->getCurrentOutputChannel();

	EQData *pEQ = pChannel->getLShelf();	/* We will use m_hShelf as PEQ */
	pEQ->setFrequency(freq);

	DeviceSocket *pSocket = g_pApp->getDeviceConnection();
	if (g_pApp->getConnectStatus() == DEVICE_NETWORK_CONNECTED)
	{
		pSocket->setSWBellFreq(1, freq);
	}
}

void EQTuningWidget::onPEQQChanged1(double Q)
{
	if (m_slotEnableFlag == false)
	{
		return;
	}

	CPi2000Data *pData = getCPi2000Data();
	OutputChannel *pChannel = pData->getCurrentOutputChannel();

	EQData *pEQ = pChannel->getLShelf();	/* We will use m_hShelf as PEQ */
	pEQ->setQ(Q);

	DeviceSocket *pSocket = g_pApp->getDeviceConnection();
	if (g_pApp->getConnectStatus() == DEVICE_NETWORK_CONNECTED)
	{
		pSocket->setSWBellQ(1, Q);
	}
}


void EQTuningWidget::retranslateUi()
{
	m_pSignalModeGroup->setTitle(tr(""));
	m_pSignalLabel->setText(tr("Signal Mode"));
	m_pReminderLabel->setText(tr("To stop RTA setting, you need to set signal mode to 'OFF'."));
	m_pActiveChannelGroup->setTitle(tr(""));
	m_pChannelLevelGroup->setTitle(tr(""));

	m_pSubwooferPEQGroup1->setTitle(tr(""));
	m_pSWPEQGainLabel1->setText(tr("Cut"));
	m_pTypeLabel1->setText(tr("Type:"));
	m_pGainLabel1->setText(tr("Cut:"));
	m_pFreqLabel1->setText(tr("Frequency:"));
	m_pQLabel1->setText(tr("Q:"));

	m_pBellCombo1->removeItem(0);
	m_pBellCombo1->addItem(tr("Bell"));
	m_pBellCombo1->setCurrentIndex(0);

	m_pSWPEQ1Label->setText(tr("Subwoofer PEQ1"));
	m_pChannelLevelLabel->setText(tr("Active Channel Level"));
	m_pSWPEQ2Label->setText(tr("Subwoofer PEQ2"));
	m_pSWPEQGainLabel2->setText(tr("Cut"));
	m_pTypeLabel2->setText(tr("Type:"));
	m_pGainLabel2->setText(tr("Cut:"));
	m_pFreqLabel2->setText(tr("Frequency:"));
	m_pQLabel2->setText(tr("Q:"));

	m_pBellCombo2->removeItem(0);
	m_pBellCombo2->addItem(tr("Bell"));
	m_pBellCombo2->setCurrentIndex(0);

	m_pPEQGroup->setTitle(tr("Active Channel EQ"));
	m_pLShGroup->setTitle(tr(""));
	m_pLGainSliderLabel->setText(tr("Gain"));
	m_pLGreqLabel->setText(tr("Frequency:"));
	m_pLGainLabel->setText(tr("Gain:"));
	m_pLSlopeLabel->setText(tr("Slope:"));

	m_pHGainSliderLabel->setText(tr("Gain"));
	m_pLShelfLabel->setText(tr("Low Shelf Filter"));
	m_pHShelfLabel->setText(tr("High Shelf Filter"));
	m_pHGreqLabel->setText(tr("Frequency:"));
	m_pHGainLabel->setText(tr("Gain:"));
	m_pHSlopeLabel->setText(tr("Slope:"));
	m_pGEQGroup->setTitle(tr(""));
	m_pGEQWidget->retranslateUi();
}
