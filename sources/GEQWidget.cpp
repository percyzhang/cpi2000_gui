#include "GEQWidget.h"
#include <QPainter>
#include "CPi2000Setting.h"
#include "mainApplication.h"
#include <QKeyEvent>

MarkWidget::MarkWidget(QWidget *parent) : QWidget(parent)
{
	m_RTAMode = RTA_FULL;
}

void MarkWidget::setRTAMode(RTA_MODE mode)
{
	m_RTAMode = mode;
	update();
}

void MarkWidget::paintEvent(QPaintEvent * /* event */ )
{
	QPainter painter(this);
	CPi2000Data *pData = getCPi2000Data();
	float centerLevel = pData->getRTACenterLevel();
	float averageLevel = pData->getRTAAverageLevel();
	int currentIndex = pData->getCurrentOutputChannelIndex();

	if (m_RTAMode == RTA_FULL)
	{
		float value1 = 0, percent1 = 0;
		float value2 = 55, percent2 = 100;

		float dbValue[] = {50, 40, 30, 20, 10, 0};
		QString strDBValue[] = {"+50 dB", "+40 dB", "+30 dB", "+20 dB", "+10 dB", "0 dB" };
		for (int i = 0; i < sizeof(dbValue) / sizeof(float); i++)
//		for (int i = 0; i < 1; i++)
		{
			float percent = percent1 + (dbValue[i] - value1) * (percent2 - percent1) / (value2 - value1);
			paintMark(painter, strDBValue[i], percent, QColor(0xff, 0xff, 0xff));
		}


		centerLevel += FULL_MODE_GAIN;
		if (currentIndex == 3)  /* For SW */
		{
			if (centerLevel < 0)
			{
			    centerLevel = 0;
			}
			centerLevel += 10;
		}
		centerLevel = qRound(centerLevel);
		float percent = percent1 + (centerLevel - value1) * (percent2 - percent1) / (value2 - value1);
		if (percent < 0)
		{
			percent = 0;
		}
		else if (percent > 100)
		{
			percent = 100;
		}
		paintLevel(painter, percent, QColor(0, 255, 0));
	}
	else // X-CURVE mode
	{
		if (currentIndex == 3)
		{
			/* Subwoofer Speaker */
			float value1 = 0, percent1 = 0;
			float value2 = 20, percent2 = 100;

			float dbValue[] = {20, 15, 10, 5, 0};
			QString strDBValue[] = {"+20 dB", "+15 dB", "+10 dB", "+5 dB", "0 dB"};
			for (int i = 0; i < sizeof(dbValue) / sizeof(float); i++)
			{
				float percent = percent1 + (dbValue[i] - value1) * (percent2 - percent1) / (value2 - value1);
				paintMark(painter, strDBValue[i], percent, QColor(0xff, 0xff, 0xff));
			}

			centerLevel = 10;
			float percent = percent1 + (centerLevel - value1) * (percent2 - percent1) / (value2 - value1);
			paintLevel(painter, percent, QColor(0, 255, 0));
		}

		else
		{
			float value1 = -15, percent1 = 0;
			float value2 = 5, percent2 = 100;

			float dbValue[] = {5, 0, -5, -10, -15};
			QString strDBValue[] = {"+5 dB", "0 dB", "-5 dB", "-10 dB", "-15 dB" };
			for (int i = 0; i < sizeof(dbValue) / sizeof(float); i++)
			{
				float percent = percent1 + (dbValue[i] - value1) * (percent2 - percent1) / (value2 - value1);
				paintMark(painter, strDBValue[i], percent, QColor(0xff, 0xff, 0xff));
			}

			if (currentIndex == 2)
			{
				/* Center Speaker */
				centerLevel += X_CURVE_GAIN;
				if (centerLevel > 0)
				{
					centerLevel = 0;
				}

				centerLevel = qRound(centerLevel);
				float percent = percent1 + (centerLevel - value1) * (percent2 - percent1) / (value2 - value1);
				if (percent < 0)
				{
					percent = 0;
				}
	//			else if (percent > 100)
	//			{
	//				percent = 100;
	//			}
				paintLevel(painter, percent, QColor(0, 255, 0));
			}
			else
			{
				/* Other Speaker */
				centerLevel += X_CURVE_GAIN;
				averageLevel += X_CURVE_GAIN;

				if (averageLevel > 0)
				{
					centerLevel -= averageLevel;
				}

				centerLevel = qRound(centerLevel);
				float percent = percent1 + (centerLevel - value1) * (percent2 - percent1) / (value2 - value1);
				if (percent < 0)
				{
					percent = 0;
				}
	//			else if (percent > 100)
	//			{
	//				percent = 100;
	//			}
				paintLevel(painter, percent, QColor(0, 255, 0));
			
			}
		}
	}
}

void MarkWidget::paintLevel(QPainter & painter, float percent, QColor color)
{
	if ((percent < 0) || (percent > 100))
	{
		return;
	}

	float h = qRound(percent * height() / 100);
	if (h == 0)
	{
		h = 1;
	}

	painter.setPen(QPen(color, 1));
//	painter.setPen(QPen(QColor(0x0, 0xf0, 0x0), 1));
	painter.drawLine(10, height() - h, width() - 3, height() - h);

	int textTop;
	if (h > 20)
	{
		textTop = h - 2;
	}
	else
	{
		textTop = h + 15;
	}
	int left = 20;
	painter.drawText(QRect(10, height() - textTop, width() - left - 3, 15), Qt::AlignLeft, "C");
}

void MarkWidget::paintMark(QPainter & painter, QString strMark, float percent, QColor color)
{
	if ((percent < 0) || (percent > 100))
	{
		return;
	}

	float h = qRound(percent * height() / 100);
	if (h == 0)
	{
		h = 1;
	}

	painter.setPen(QPen(QColor(0x3e, 0x84, 0x84), 1));
	painter.drawLine(10, height() - h, width() - 3, height() - h);
	painter.setPen(QPen(color, 1));
	int textTop;
	if (h > 20)
	{
		textTop = h - 2;
	}
	else
	{
		textTop = h + 15;
	}
	int left = 20;
	painter.drawText(QRect(20, height() - textTop, width() - left - 3, 15), Qt::AlignRight, strMark);
}



void GraphWidget::setValue(int i, float value)
{
	if (i < 30)
	{
		m_valueWidget[i]->setValue(value);
		m_valueWidget[i]->refresh();
	}
}

GraphWidget::GraphWidget(QWidget *parent)  : QFrame (parent)
{
	for (int i = 0; i < 30; i++)
	{
		m_valueWidget[i] = new ValueWidget(this);
		m_valueWidget[i]->setValue(-100);
		m_valueWidget[i]->setScale(-100, 10, 0, 90);
		m_valueWidget[i]->setSolidColor(QColor(185, 30 + 5 * i, 30 + i));
	}

	m_fullStandardDB = 16.0;
	m_RTAMode = RTA_FULL;
	setRTAMode(RTA_FULL);
}

void GraphWidget::setAverageValue(float averageValue)
{
	if (m_RTAMode == RTA_FULL)
	{
		bool updateFlag = false;
		averageValue += FULL_MODE_GAIN;

		if (averageValue < 16)
		{
			averageValue = 16;
		}

		if (fabs(averageValue - m_fullStandardDB) >= 0.5) 
		{
			updateFlag = true;
		}

		if ((averageValue == 16) && ( m_fullStandardDB != 16))
		{
			updateFlag = true;
		}

		if (updateFlag == true)
		{
			m_fullStandardDB = averageValue;
			setFullStandard();
			update();	
		}
	}
}

void GraphWidget::setFullStandard()
{
//	float fullStand = 16;
	//		HZ		   20  25  31  40  50 
	float diff[30] = { -4,	-3,	-2,	-1,	0, 
						0,	0,	0,	0,	0, 
						0,	0,	0,	0,	0, 
						0,	0,	0,	0,	0, 
						0,	-1,	-2,	-3,	-4, 
						-5,	-6,	-7,	-9,	-11 };

	if (m_RTAMode == RTA_FULL)
	{
		for (int i = 0; i < 30; i++)
		{
			m_valueWidget[i]->setStandard(m_fullStandardDB + diff[i]);
		}
	}
}

void GraphWidget::setSWLevelStandard()
{
	CPi2000Data *pData = getCPi2000Data();
	float standard = pData->getRTACenterLevel() + FULL_MODE_GAIN + 10;
	if (standard < 10)
	{
		standard = 10;
	}
	if (m_RTAMode == RTA_FULL)
	{
		for (int i = 0; i < 30; i++)
		{
			m_valueWidget[i]->setStandard(standard, QColor(0x0, 0xF0, 0x00));
		}
	}
	else
	{
		for (int i = 0; i < 30; i++)
		{
			m_valueWidget[i]->setStandard(10, QColor(0x0, 0xF0, 0x0));
		}
	}
}

void GraphWidget::setRTAMode(RTA_MODE rtaMode)
{
	m_RTAMode = rtaMode;
	CPi2000Data *pData = getCPi2000Data();
	int currentChannel = pData->getCurrentOutputChannelIndex();

//	float fullStand = 16;
	float xcurveStand = 0;
	//		HZ		   20  25  31  40  50 
	float diff[30] = { -4,	-3,	-2,	-1,	0, 
						0,	0,	0,	0,	0, 
						0,	0,	0,	0,	0, 
						0,	0,	0,	0,	0, 
						0,	-1,	-2,	-3,	-4, 
						-5,	-6,	-7,	-9,	-11 };

	if (rtaMode == RTA_FULL)
	{
		for (int i = 0; i < 30; i++)
		{
			m_valueWidget[i]->setScale(0.0, 0, 55, 100);
			m_valueWidget[i]->setExtraGain(FULL_MODE_GAIN);
//			m_valueWidget[i]->setStandard(fullStand + diff[i]);
		}

		if (currentChannel == 3)
		{
			setSWLevelStandard();
		}
		else
		{
			setFullStandard();
		}
	}
	else
	{
		if (currentChannel == 3)
		{
			// for SW
			for (int i = 0; i < 30; i++)
			{
				m_valueWidget[i]->setScale(0, 0, 20, 100);
				m_valueWidget[i]->setExtraGain(-pData->getRTACenterLevel());
				m_valueWidget[i]->setStandard(10, QColor(0x0, 0xF0, 0x0));
			}
		}
		else
		{
			for (int i = 0; i < 30; i++)
			{
				m_valueWidget[i]->setScale(-15, 0, 5, 100);
				m_valueWidget[i]->setExtraGain(X_CURVE_GAIN);
				m_valueWidget[i]->setStandard(xcurveStand + diff[i]);
			}
		}
	}
	update();
}


void GraphWidget::resizeEvent(QResizeEvent * /* event */)
{
	float unitWidth = (float)width() / 30;

	for (int i = 0; i < 30; i++)
	{
		int w = qRound(unitWidth * (i + 1)) - qRound(unitWidth * i) - 1;
		if (i == 29)
		{
			w--;
		}
		m_valueWidget[i]->setGeometry(qRound(unitWidth * i) + 1, 0, w, height() - 1);
	}
}

#if 0
void GraphWidget::paintEvent(QPaintEvent *event)
{
	QPainter painter(this);

	float unitWidth = (float)width() / 30;
	painter.fillRect(0, 0, width(), height() - 1, QColor(50, 50, 50));
	qDebug() << "fillRect " << unitWidth * 30;

	painter.setPen(QPen(QColor(0xf0, 0xf0, 0xf0), 1));
	for (int i = 1; i <= 30; i++)
	{
		painter.drawLine(unitWidth * i, 0, qRound(unitWidth * i), height() - 1);
		qDebug() << "drawLine " << qRound(unitWidth * i);
	}
}
#endif

GEQWidget::GEQWidget(QWidget *parent)	: QFrame(parent)
{
//	setObjectName(QStringLiteral("m_pGEQWidget"));
	m_slotEnableFlag = true;

	m_pRTARangeGroup = new QGroupBox(tr(""), this);
	m_pRTARangeGroup->setObjectName(QStringLiteral("RTAGroup"));
	{
		m_pModeLabel = new QLabel(tr("1/3 Octave Graphic EQ"), m_pRTARangeGroup);
		m_pModeLabel->setAlignment(Qt::AlignLeft | Qt::AlignTop);
		m_pModeLabel->setObjectName("WhiteFont12");

		m_pFullRadio = new QRadioButton(tr("Full"), m_pRTARangeGroup);
		m_pFullRadio->setObjectName(QStringLiteral("WhiteFont14"));
		connect(m_pFullRadio, SIGNAL(toggled(bool)), this, SLOT(onFullModeChanged(bool)));

		m_pXCurveRadio = new QRadioButton(tr("X-Curve"), m_pRTARangeGroup);
		m_pXCurveRadio->setObjectName(QStringLiteral("WhiteFont14"));
	}

	m_pMarkFrame = new MarkWidget(this);

	m_pPaintFrame = new QFrame(this);
	m_pPaintFrame->setObjectName(QStringLiteral("PaintFrame"));

	m_pGraphWidget = new GraphWidget(m_pPaintFrame);
	m_pGraphWidget->setObjectName("lightBlueBackground");

	m_pSliderFrame = new QFrame(this);
	m_pSliderFrame->setObjectName("m_pSliderFrame");

	QString freq[] = {  "20", "25", "31",
						"40", "50", "63", 
						"80", "100", "125",
						"160", "200", "250",
						"315", "400", "500",
						"630", "800", "1k",
						"1.25k", "1.6k", "2k",
						"2.5k", "3.15k", "4k",
						"5k", "6.3k", "8k",
						"10k", "12.5k", "16k"};
	for (int i = 0; i < 30; i++)
	{
		m_pFreqLabel_fix[i] = new QLabel(freq[i], m_pPaintFrame);
		m_pFreqLabel_fix[i]->setAlignment(Qt::AlignCenter);
		m_pFreqLabel_fix[i]->setObjectName("WhiteFont10");
	}

	for (int i = 0; i < 27; i++)
	{
		m_pGEQSlider[i] = new GEQSlider(m_pSliderFrame);
		m_pGEQSlider[i]->setObjectName("LittleSlider");

		m_pGEQSlider[i]->setRange(-120, 120);
		m_pGEQSlider[i]->setSingleStep(1);
		m_pGEQSlider[i]->setPageStep(1);
		m_pGEQSlider[i]->setValue(0);


		m_pFreqLabel[i] = new QLabel("0.0", m_pSliderFrame);
		m_pFreqLabel[i]->setAlignment(Qt::AlignCenter);
		m_pFreqLabel[i]->setObjectName("WhiteFont10");
	}

	for (int i = 0; i < 28; i++)
	{
		m_pSliderLine[i] = new QFrame(m_pSliderFrame);
		m_pSliderLine[i]->setObjectName(QStringLiteral("m_pLineFrame"));
		m_pSliderLine[i]->setFrameShape(QFrame::VLine);
	}

	m_pHorizontalLine = new QFrame(m_pSliderFrame);
	m_pHorizontalLine->setObjectName(QStringLiteral("m_pLineFrame"));
	m_pHorizontalLine->setFrameShape(QFrame::HLine);

	connect(m_pGEQSlider[0], SIGNAL(valueChanged(int)), this, SLOT(onGEQSliderChanged_0(int)));
	connect(m_pGEQSlider[1], SIGNAL(valueChanged(int)), this, SLOT(onGEQSliderChanged_1(int)));
	connect(m_pGEQSlider[2], SIGNAL(valueChanged(int)), this, SLOT(onGEQSliderChanged_2(int)));
	connect(m_pGEQSlider[3], SIGNAL(valueChanged(int)), this, SLOT(onGEQSliderChanged_3(int)));
	connect(m_pGEQSlider[4], SIGNAL(valueChanged(int)), this, SLOT(onGEQSliderChanged_4(int)));
	connect(m_pGEQSlider[5], SIGNAL(valueChanged(int)), this, SLOT(onGEQSliderChanged_5(int)));
	connect(m_pGEQSlider[6], SIGNAL(valueChanged(int)), this, SLOT(onGEQSliderChanged_6(int)));
	connect(m_pGEQSlider[7], SIGNAL(valueChanged(int)), this, SLOT(onGEQSliderChanged_7(int)));
	connect(m_pGEQSlider[8], SIGNAL(valueChanged(int)), this, SLOT(onGEQSliderChanged_8(int)));
	connect(m_pGEQSlider[9], SIGNAL(valueChanged(int)), this, SLOT(onGEQSliderChanged_9(int)));
	connect(m_pGEQSlider[10], SIGNAL(valueChanged(int)), this, SLOT(onGEQSliderChanged_10(int)));
	connect(m_pGEQSlider[11], SIGNAL(valueChanged(int)), this, SLOT(onGEQSliderChanged_11(int)));
	connect(m_pGEQSlider[12], SIGNAL(valueChanged(int)), this, SLOT(onGEQSliderChanged_12(int)));
	connect(m_pGEQSlider[13], SIGNAL(valueChanged(int)), this, SLOT(onGEQSliderChanged_13(int)));
	connect(m_pGEQSlider[14], SIGNAL(valueChanged(int)), this, SLOT(onGEQSliderChanged_14(int)));
	connect(m_pGEQSlider[15], SIGNAL(valueChanged(int)), this, SLOT(onGEQSliderChanged_15(int)));
	connect(m_pGEQSlider[16], SIGNAL(valueChanged(int)), this, SLOT(onGEQSliderChanged_16(int)));
	connect(m_pGEQSlider[17], SIGNAL(valueChanged(int)), this, SLOT(onGEQSliderChanged_17(int)));
	connect(m_pGEQSlider[18], SIGNAL(valueChanged(int)), this, SLOT(onGEQSliderChanged_18(int)));
	connect(m_pGEQSlider[19], SIGNAL(valueChanged(int)), this, SLOT(onGEQSliderChanged_19(int)));
	connect(m_pGEQSlider[20], SIGNAL(valueChanged(int)), this, SLOT(onGEQSliderChanged_20(int)));
	connect(m_pGEQSlider[21], SIGNAL(valueChanged(int)), this, SLOT(onGEQSliderChanged_21(int)));
	connect(m_pGEQSlider[22], SIGNAL(valueChanged(int)), this, SLOT(onGEQSliderChanged_22(int)));
	connect(m_pGEQSlider[23], SIGNAL(valueChanged(int)), this, SLOT(onGEQSliderChanged_23(int)));
	connect(m_pGEQSlider[24], SIGNAL(valueChanged(int)), this, SLOT(onGEQSliderChanged_24(int)));
	connect(m_pGEQSlider[25], SIGNAL(valueChanged(int)), this, SLOT(onGEQSliderChanged_25(int)));
	connect(m_pGEQSlider[26], SIGNAL(valueChanged(int)), this, SLOT(onGEQSliderChanged_26(int)));

	connect(g_pApp, SIGNAL(setRTAValue()), this, SLOT(onRTAChanged()));

	m_pEQAssistGroup = new QGroupBox(tr(""), this);
	m_pEQAssistGroup->setObjectName(QStringLiteral("RTAGroup"));
	{
		m_pAssistButton = new QPushButton(tr("EQ Assist"), m_pEQAssistGroup);
		m_pFlattenButton = new QPushButton(tr("Flatten"), m_pEQAssistGroup);
		m_pCopyButton = new QPushButton(tr("Copy EQ"), m_pEQAssistGroup);
		m_pPasteButton = new QPushButton(tr("Paste EQ"), m_pEQAssistGroup);	
		m_pGEQLabel = new QLabel(tr("Graphic EQ"), m_pEQAssistGroup);
		m_pGEQLabel->setAlignment(Qt::AlignLeft | Qt::AlignTop);
		m_pGEQLabel->setObjectName("WhiteFont12");
	}
}

void GEQWidget::onRTAChanged()
{
	CPi2000Data *pData = getCPi2000Data();
	RTAData *pRTA = pData->getRTA();

	float *pRTAMeter = pRTA->getRTAValue();
	float totalMeter = 0;
	int currentOutputIndex = pData->getCurrentOutputChannelIndex();

	for (int i = 0; i < 30; i++)
	{
		if (pRTA->getSignalMode() == SIGNAL_OFF)
		{
			pRTAMeter[i] = -90.0;
		}

		if ((i >= 4) && (i <= 20))
		{
			totalMeter += pRTAMeter[i];
		}
	}

	float averageValue = totalMeter / 17;
	if (currentOutputIndex != 3)
	{
		m_pGraphWidget->setAverageValue(averageValue);
	}

	int diff = 0;
	if ((pRTA->getRTAMode() == RTA_X_CURVE) && (currentOutputIndex == 2) && (averageValue + X_CURVE_GAIN > 0))
	{
		diff = averageValue + X_CURVE_GAIN;
	}
	for (int i = 0; i < 30; i++)
	{
		if ((pRTA->getRTAMode() == RTA_X_CURVE) && (currentOutputIndex == 3))
		{
			/* For Subwoofer at X_Curve Mode */
			m_pGraphWidget->setValue(i, pRTAMeter[i]);
		}
		else
		{
			m_pGraphWidget->setValue(i, pRTAMeter[i] - diff);
		}
	}

	pData->setRTAAverageLevel(averageValue);
	if (currentOutputIndex == 2)
	{
		pData->setRTACenterLevel(averageValue);
		m_pMarkFrame->update();
	}
	else if (pRTA->getRTAMode() == RTA_X_CURVE)
	{
		m_pMarkFrame->update();
	}
}

void GEQWidget::onGEQSliderChanged(int index, int slider)
{
	if (m_slotEnableFlag == false)
	{
		return;
	}

	CPi2000Data *pData = getCPi2000Data();
	OutputChannel *pChannel = pData->getCurrentOutputChannel();
	int currentOutputChannelIndex = pData->getCurrentOutputChannelIndex();

	float gain = (float)slider / 10;
	pChannel->setGEQ(index, (float)slider / 10);
	m_pFreqLabel[index]->setText(QString::number(gain, 'f', 1));

	m_pGEQSlider[index]->setFocus();

	if (g_pApp->getConnectStatus() == DEVICE_NETWORK_CONNECTED)
	{
		g_pApp->getDeviceConnection()->setGEQ(currentOutputChannelIndex, index, gain);
	}
}

void GEQWidget::resizeEvent(QResizeEvent * /* event */)
{

//	int leftMargin = 10;
//	int sliderTop = 60;
//	int bottomMargin = height() / 20;
//	m_pBandEQLabel->setGeometry(leftMargin, 0, 300, 30);

//	m_pFrame->setGeometry(leftMargin, 30, width() - 2 * leftMargin, height() - 30 - 5);

//	int frameWidth = width() - 2 * leftMargin - 100;
//	int frameHeight = height() - 30 - 5;

	int paintLeft = 60;
	int paintWidth = width() - paintLeft - 2;
	int paintHeight = height() * 2 / 3 - 30;

	m_pPaintFrame->setGeometry(paintLeft, 0, paintWidth, paintHeight);
	float unitWidth = (float)paintWidth / 30;
	for (int i = 0; i < 30; i++)
	{
		m_pFreqLabel_fix[i]->setGeometry(unitWidth * i, paintHeight - 15, unitWidth, 15);
	}
	m_pGraphWidget->setGeometry(0, 0, paintWidth, paintHeight - 15);

	int slideLeft = paintLeft + paintWidth / 10;
//	int slideTop = 0;
	float slideWidth = (float)paintWidth * 9/ 10;
	int slideHeight = height() - paintHeight;
	m_pSliderFrame->setGeometry(slideLeft, paintHeight, slideWidth, slideHeight);


//	float unitWidth = slideWidth / 27;
	int unitHeight = slideHeight - 20;
	int unitTop = 15;

	for (int i = 0; i < 27; i++)
	{
		m_pFreqLabel[i]->setGeometry(i * unitWidth, unitHeight + 5, unitWidth, unitTop);
		m_pGEQSlider[i]->setGeometry(i * unitWidth, 5, unitWidth, unitHeight);
	}
	
	for (int i = 0; i < 28; i++)
	{
		int left;
		if (i != 27)
		{
			left = qRound(i * unitWidth) - 1;
		}
		else
		{
			left = (int)(i * unitWidth) - 1;
		}
		if (left < 0)
		{
			left = 0;
		}
		m_pSliderLine[i]->setGeometry(left, 10, 1, slideHeight - 1);
	}

	m_pHorizontalLine->setGeometry(0, slideHeight - 1, slideWidth - 1, 1);

	{
		int modeWidth = slideLeft;
		int modeHeight = slideHeight * 2 / 5;
		m_pRTARangeGroup->setGeometry(0, paintHeight + slideHeight * 0.2 / 5, modeWidth, slideHeight * 1.8 / 5);
		{
			m_pModeLabel->setGeometry(modeWidth / 20, modeHeight / 3 - 10, 150, 20);
			m_pFullRadio->setGeometry(modeWidth * 2 / 10 - 15, modeHeight * 2 / 3 - 10, 80, 20);
			m_pXCurveRadio->setGeometry(modeWidth * 6 / 10 - 20, modeHeight * 2 / 3 - 10, 80, 20);
		}

		int GEQButtonHeight = slideHeight * 2.9 / 5;
		m_pEQAssistGroup->setGeometry(0, paintHeight + slideHeight - GEQButtonHeight, modeWidth, GEQButtonHeight);
		{
			int buttonWidth = 60 + modeWidth / 8;
			m_pGEQLabel->setGeometry(modeWidth / 20, GEQButtonHeight / 20, 100, 20);
			m_pCopyButton->setGeometry(modeWidth / 4 - buttonWidth / 2, GEQButtonHeight / 3, buttonWidth, 25);
			m_pPasteButton->setGeometry(modeWidth / 4 * 3 - buttonWidth / 2, GEQButtonHeight / 3, buttonWidth, 25);
			m_pAssistButton->setGeometry(modeWidth / 4 - buttonWidth / 2, GEQButtonHeight / 3 * 2, buttonWidth, 25);
			m_pFlattenButton->setGeometry(modeWidth / 4 * 3 - buttonWidth / 2, GEQButtonHeight / 3 * 2, buttonWidth, 25);
		}
	}

	m_pMarkFrame->setGeometry(0, 0, paintLeft, paintHeight - 15);
}

void GEQWidget::refreshGEQ()
{
	bool oldSlotEnableFlag = m_slotEnableFlag;
	m_slotEnableFlag = false;

	CPi2000Data *pData = getCPi2000Data();
	OutputChannel *pChannel = pData->getCurrentOutputChannel();
	int currentOutputIndex = pData->getCurrentOutputChannelIndex();
//	RTAData *pRTA = pData->getRTA();

	for (int i = 0; i < 27; i++)
	{
		m_pGEQSlider[i]->setEnabled(currentOutputIndex != 3);
		m_pFreqLabel[i]->setEnabled(currentOutputIndex != 3);
		m_pGEQSlider[i]->setValue(qRound(pChannel->getGEQ(i) * 10));
		m_pFreqLabel[i]->setText(QString::number(pChannel->getGEQ(i), 'f', 1));
	}

	refreshRTAMode();

	m_slotEnableFlag = oldSlotEnableFlag;
}

void GEQWidget::refreshRTAMode()
{
	bool oldSlotEnableFlag = m_slotEnableFlag;
	m_slotEnableFlag = false;

	CPi2000Data *pData = getCPi2000Data();
	RTAData *pRTA = pData->getRTA();

	m_pGraphWidget->setRTAMode(pRTA->getRTAMode());
	m_pMarkFrame->setRTAMode(pRTA->getRTAMode());
	if (pRTA->getRTAMode() == RTA_FULL)
	{
		m_pFullRadio->setChecked(true);
	}
	else
	{
		m_pXCurveRadio->setChecked(true);
	}

	m_slotEnableFlag = oldSlotEnableFlag;
}

void GEQWidget::onFullModeChanged(bool flag)
{
	CPi2000Data *pData = getCPi2000Data();
	RTAData *pRTA = pData->getRTA();

	if (flag)
	{
		pRTA->setRTAMode(RTA_FULL);
	}
	else
	{
		pRTA->setRTAMode(RTA_X_CURVE);
	}

	refreshRTAMode();
}

void GEQWidget::retranslateUi()
{
	m_pRTARangeGroup->setTitle(tr(""));
	m_pAssistButton->setText(tr("EQ Assist"));
	m_pFlattenButton->setText(tr("Flatten"));
	m_pCopyButton->setText(tr("Copy EQ"));
	m_pPasteButton->setText(tr("Paste EQ"));	
	m_pGEQLabel->setText(tr("Graphic EQ"));
	m_pModeLabel->setText(tr("1/3 Octave Graphic EQ"));
}


GEQSlider::GEQSlider(QWidget *pParent) 
	:QSlider(pParent)
{

}

void GEQSlider::keyPressEvent(QKeyEvent *event)
{
    switch (event->key())
    {
        case Qt::Key_Left:
		case Qt::Key_Right:
			event->ignore();
			return;
	}
	QSlider::keyPressEvent(event);
}

void GEQWidget::keyPressEvent(QKeyEvent *event)
{
	int nextFocus = -1;
    switch (event->key())
    {
		case Qt::Key_Left:
		case Qt::Key_Right:
			{
				for (int i = 0; i < 27; i++)
				{
					if (m_pGEQSlider[i]->hasFocus() == true)
					{
						if (event->key() == Qt::Key_Right)
						{
							nextFocus = i + 1;
							if (nextFocus == 27)
							{
								nextFocus = 0;
							}
							m_pGEQSlider[nextFocus]->setFocus();
						}
						else
						{
							nextFocus = i - 1;
							if (nextFocus == -1)
							{
								nextFocus = 26;
							}
							m_pGEQSlider[nextFocus]->setFocus();
						}
						break;
					}
				}
			}

			event->accept();
			return;
	}
	QWidget::keyPressEvent(event);
}
