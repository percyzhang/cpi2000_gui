#include "HeadFrame.h"
#include <qdebug.h>

HeadFrame::HeadFrame(QString strTitle, QWidget *pParent, QString pngName)
    : QFrame(pParent)
{
 	setObjectName(QStringLiteral("headFrame"));

	m_pTitleLabel = new	QLabel(strTitle, this);
	m_pTitleLabel->setObjectName(QStringLiteral("m_pTitleLabel"));

	if (!pngName.isEmpty())
	{
		m_pIconLabel = new QLabel(this);
		QString styleSheet = QString("border-image: url(:/%1);").arg(pngName);
		m_pIconLabel->setStyleSheet(styleSheet);
		m_pIconLabel->setGeometry(2, 2, 20, 20);
	}
	else
	{
		m_pIconLabel = nullptr;
	}
}

void HeadFrame::resizeEvent(QResizeEvent * /* event */)
{
	m_pTitleLabel->setGeometry(0, 0, width(), 24);
}

void HeadFrame::changeEvent(QEvent *e)
{
    if(e->type() == QEvent::EnabledChange)
    {
        if(isEnabled())
        {
		 	setObjectName(QStringLiteral("headFrame"));
			m_pTitleLabel->setObjectName(QStringLiteral("m_pTitleLabel"));
        }
        else
        {
		 	setObjectName(QStringLiteral("headFrameDisabled"));
			m_pTitleLabel->setObjectName(QStringLiteral("m_pTitleLabelDisabled"));
        }
		this->setStyleSheet("#headFrame { 	border-radius: 8; 	background-color: #6e6e6e; } "
							"#headFrameDisabled { 	border-radius: 8; 	background-color: #aeaeae; }");
//		m_pTitleLabel->setStyleSheet("");
    }
}

void HeadFrame::retranslateUi(QString title)
{
	m_pTitleLabel->setText(title);
}