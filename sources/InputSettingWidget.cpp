#include "InputSettingWidget.h"
#include <QStylePainter>
#include "mainwidget.h"
#include <qmessagebox.h>
#include "CPi2000Setting.h"
#include "MainApplication.h"

/* If the input gain is less or equal to -70dB, it will show "-Inf" in the GUI, and send -120dB to device  */
#define MIN_INPUT_GAIN	-70
#define MIN_INPUT_GAIN_TO_DEVICE  -120

InputSettingWidget::InputSettingWidget(QWidget *parent)
    : QWidget(parent)
{
	m_pDigitalFrame = new HeadFrame(tr("      DIGITAL"), this, "IconDigital.png");
//	m_pDigitalFrame->setEnabled(false);

	m_pAnalogFrame = new HeadFrame(tr("      ANALOG"), this, "IconAnalog.png");
//	m_pAnalogFrame->setEnabled(false);

	m_pMicFrame = new HeadFrame(tr("      MIC"), this, "IconMic.png");
//	m_pMicFrame->setEnabled(false);

	m_pMusicFrame = new HeadFrame(tr("      MUSIC(NON-SYNC)"), this, "IconMusic.png");
//	m_pMusicFrame->setEnabled(false);

//	setInputSource(INPUT_SOURCE_DIGITAL);

	/* The following component is for Digital Input */
	m_pDigitalVolumeSlider = new ButtonSlider(m_pDigitalFrame);
	m_pDigitalVolumeSlider->setTextColor(QColor(0x10, 0x10, 0x10));
	m_pDigitalVolumeSlider->setMarkColor(QColor(0x10, 0x10, 0x10));
	m_pDigitalVolumeSlider->addExtraLongMark(0, " 0dB", true);
	m_pDigitalVolumeSlider->addExtraLongMark(MIN_INPUT_GAIN, "-INF", false);
	QStringList sliderStringList;
	sliderStringList << "" << "" << "" << "" << "" << "" << "" << "" ;
	m_pDigitalVolumeSlider->setMark(2, 1, sliderStringList);
	{
		m_pDigitalVolumeSlider->addExtraShortMark(-10, "-10dB", false);
		m_pDigitalVolumeSlider->addExtraShortMark(-20, "-20dB", false);
		m_pDigitalVolumeSlider->addExtraShortMark(-30, "-30dB", false);
		m_pDigitalVolumeSlider->addExtraShortMark(-40, "-40dB", false);
		m_pDigitalVolumeSlider->addExtraShortMark(-50, "-50dB", false);
		m_pDigitalVolumeSlider->addExtraShortMark(-60, "-60dB", false);
	}

	m_pDigitalVolumeSlider->setMarkLength(7, 10);
	m_pDigitalVolumeSlider->setMarkSide(true, true);
	m_pDigitalVolumeSlider->setRange(MIN_INPUT_GAIN, 0);
	m_pDigitalVolumeSlider->setGrooveHeight(30);
	m_pDigitalVolumeSlider->setSingleStep(1);
	m_pDigitalVolumeSlider->setPageStep(1);
	m_pDigitalVolumeSlider->setValue(70);
	connect(m_pDigitalVolumeSlider, SIGNAL(valueChanged(int)), this, SLOT(onDigitalVolumeSliderChanged(int)));


	m_pDigitalVolumeLabel = new QLabel(tr("Input Gain"), m_pDigitalFrame);
	m_pDigitalVolumeLabel->setObjectName(QStringLiteral("BlackFont14"));
	m_pDigitalVolumeLabel->setAlignment(Qt::AlignCenter);

	m_pDigitalVolumeSpinBox = new QDoubleSpinBox(m_pDigitalFrame);
	m_pDigitalVolumeSpinBox->setRange(0.0, 10.0);
	m_pDigitalVolumeSpinBox->setSingleStep(0.1);
	m_pDigitalVolumeSpinBox->setDecimals(1);
	m_pDigitalVolumeSpinBox->setSuffix("");
	m_pDigitalVolumeSpinBox->setValue(7.0);
	connect(m_pDigitalVolumeSpinBox, SIGNAL(valueChanged(double)), this, SLOT(onDigitalVolumeSpinBoxChanged(double)));
	m_pDigitalVolumeSpinBox->hide();	/* replace by m_pDigitalVolumeEdit */

	m_pDigitalVolumeEdit = new QLineEdit(m_pDigitalFrame);
	m_pDigitalVolumeEdit->setReadOnly(true);
	m_pDigitalVolumeEdit->setAlignment(Qt::AlignCenter);

	m_pDigitalMuteButton = new MarkButton(QString(""), m_pDigitalFrame);
    m_pDigitalMuteButton->setObjectName(QStringLiteral("muteButton"));
	connect(m_pDigitalMuteButton, SIGNAL(clicked()), this, SLOT(onClickDigitalMuteButton()));
	m_pDigitalMuteButton->mark(false);

	m_pDigitalVolumeChart = new VolumeChartWidget(m_pDigitalFrame);
	QStringList digitalVolumeStringList;
	digitalVolumeStringList << "";
	float *maxInputDB = getCPi2000Data()->getMaxInputDB();
	m_pDigitalVolumeChart->setVolumeData(maxInputDB, digitalVolumeStringList, 1);

	m_pDigitalInputSettingWidget = new DigitalInputSettingWidget(m_pDigitalFrame);
	connect(m_pDigitalInputSettingWidget, SIGNAL(applyChannel()), this, SLOT(onApplyDigitalChannel()));

	m_pDigitalDelayLabel = new QLabel(tr("Global Audio Delay"), m_pDigitalFrame);
	m_pDigitalDelayLabel->setObjectName(QStringLiteral("BlackFont12"));
	m_pDigitalDelayLabel->setAlignment(Qt::AlignRight | Qt::AlignVCenter);

	m_pDigitalDelaySpinBox = new QSpinBox(m_pDigitalFrame);
	m_pDigitalDelaySpinBox->setRange(0, 1000);
	m_pDigitalDelaySpinBox->setSingleStep(1);
//	m_pDigitalDelaySpinBox->setDecimals(1);
	m_pDigitalDelaySpinBox->setSuffix(" ms");
	connect(m_pDigitalDelaySpinBox, SIGNAL(valueChanged(int)), this, SLOT(onDigitalDelaySpinBoxChanged(int)));


	m_pDigitalEnableButton = new MarkButton(QString("Enabled"), m_pDigitalFrame);
    m_pDigitalEnableButton->setObjectName(QStringLiteral("InputPresetButton"));
	connect(m_pDigitalEnableButton, SIGNAL(clicked()), this, SLOT(onClickDigitalEnableDelayButton()));
	m_pDigitalEnableButton->mark(true);

	/* The following component is for Analog Input */
	m_pAnalogVolumeSlider = new ButtonSlider(m_pAnalogFrame);
	m_pAnalogVolumeSlider->setTextColor(QColor(0x10, 0x10, 0x10));
	m_pAnalogVolumeSlider->setMarkColor(QColor(0x10, 0x10, 0x10));
	m_pAnalogVolumeSlider->addExtraLongMark(0, " 0dB", true);
	m_pAnalogVolumeSlider->addExtraLongMark(MIN_INPUT_GAIN, "-INF", false);
	m_pAnalogVolumeSlider->setMark(2, 1, sliderStringList);
	{
		m_pAnalogVolumeSlider->addExtraShortMark(-10, "-10dB", false);
		m_pAnalogVolumeSlider->addExtraShortMark(-20, "-20dB", false);
		m_pAnalogVolumeSlider->addExtraShortMark(-30, "-30dB", false);
		m_pAnalogVolumeSlider->addExtraShortMark(-40, "-40dB", false);
		m_pAnalogVolumeSlider->addExtraShortMark(-50, "-50dB", false);
		m_pAnalogVolumeSlider->addExtraShortMark(-60, "-60dB", false);
	}

	m_pAnalogVolumeSlider->setMarkLength(7, 10);
	m_pAnalogVolumeSlider->setMarkSide(true, true);
	m_pAnalogVolumeSlider->setRange(MIN_INPUT_GAIN, 0);
	m_pAnalogVolumeSlider->setGrooveHeight(30);
	m_pAnalogVolumeSlider->setSingleStep(1);
	m_pAnalogVolumeSlider->setPageStep(1);
	m_pAnalogVolumeSlider->setValue(70);
	connect(m_pAnalogVolumeSlider, SIGNAL(valueChanged(int)), this, SLOT(onAnalogVolumeSliderChanged(int)));


	m_pAnalogVolumeLabel = new QLabel(tr("Input Gain"), m_pAnalogFrame);
	m_pAnalogVolumeLabel->setObjectName(QStringLiteral("BlackFont14"));
	m_pAnalogVolumeLabel->setAlignment(Qt::AlignCenter);

	m_pAnalogVolumeSpinBox = new QDoubleSpinBox(m_pAnalogFrame);
	m_pAnalogVolumeSpinBox->setRange(0.0, 10.0);
	m_pAnalogVolumeSpinBox->setSingleStep(0.1);
	m_pAnalogVolumeSpinBox->setDecimals(1);
	m_pAnalogVolumeSpinBox->setSuffix("");
	m_pAnalogVolumeSpinBox->setValue(7.0);
	connect(m_pAnalogVolumeSpinBox, SIGNAL(valueChanged(double)), this, SLOT(onAnalogVolumeSpinBoxChanged(double)));
	m_pAnalogVolumeSpinBox->hide();		/* replace by m_pAnalogVolumeEdit */

	m_pAnalogVolumeEdit = new QLineEdit(m_pAnalogFrame);
	m_pAnalogVolumeEdit->setReadOnly(true);


	m_pAnalogMuteButton = new MarkButton(QString(""), m_pAnalogFrame);
    m_pAnalogMuteButton->setObjectName(QStringLiteral("muteButton"));
	connect(m_pAnalogMuteButton, SIGNAL(clicked()), this, SLOT(onClickAnalogMuteButton()));
	m_pAnalogMuteButton->mark(false);

	m_pAnalogVolumeChart = new VolumeChartWidget(m_pAnalogFrame);
	QStringList analogVolumeStringList;
	analogVolumeStringList << "";
	maxInputDB = getCPi2000Data()->getMaxInputDB();
	m_pAnalogVolumeChart->setVolumeData(maxInputDB, analogVolumeStringList, 1);

	m_pAnalogInputSettingWidget = new DigitalInputSettingWidget(m_pAnalogFrame);
	connect(m_pAnalogInputSettingWidget, SIGNAL(applyChannel()), this, SLOT(onApplyAnalogChannel()));

	m_pAnalogDelayLabel = new QLabel(tr("Global Audio Delay"), m_pAnalogFrame);
	m_pAnalogDelayLabel->setObjectName(QStringLiteral("BlackFont12"));
	m_pAnalogDelayLabel->setAlignment(Qt::AlignRight | Qt::AlignVCenter);

	m_pAnalogDelaySpinBox = new QSpinBox(m_pAnalogFrame);
	m_pAnalogDelaySpinBox->setRange(0, 1000);
	m_pAnalogDelaySpinBox->setSingleStep(1);
//	m_pAnalogDelaySpinBox->setDecimals(1);
	m_pAnalogDelaySpinBox->setSuffix(" ms");
	connect(m_pAnalogDelaySpinBox, SIGNAL(valueChanged(int)), this, SLOT(onAnalogDelaySpinBoxChanged(int)));


	m_pAnalogEnableButton = new MarkButton(QString("Enabled"), m_pAnalogFrame);
    m_pAnalogEnableButton->setObjectName(QStringLiteral("InputPresetButton"));
	connect(m_pAnalogEnableButton, SIGNAL(clicked()), this, SLOT(onClickAnalogEnableDelayButton()));
	m_pAnalogEnableButton->mark(true);

	/* The following component is for MIC Input */
	m_pMicVolumeSlider = new ButtonSlider(m_pMicFrame);
	m_pMicVolumeSlider->setTextColor(QColor(0x10, 0x10, 0x10));
	m_pMicVolumeSlider->setMarkColor(QColor(0x10, 0x10, 0x10));
	m_pMicVolumeSlider->addExtraLongMark(0, " 0dB", true);
	m_pMicVolumeSlider->addExtraLongMark(MIN_INPUT_GAIN, "-INF", false);
	m_pMicVolumeSlider->setMark(2, 1, sliderStringList);
	{
		m_pMicVolumeSlider->addExtraShortMark(-10, "-10dB", false);
		m_pMicVolumeSlider->addExtraShortMark(-20, "-20dB", false);
		m_pMicVolumeSlider->addExtraShortMark(-30, "-30dB", false);
		m_pMicVolumeSlider->addExtraShortMark(-40, "-40dB", false);
		m_pMicVolumeSlider->addExtraShortMark(-50, "-50dB", false);
		m_pMicVolumeSlider->addExtraShortMark(-60, "-60dB", false);
	}

	m_pMicVolumeSlider->setMarkLength(7, 10);
	m_pMicVolumeSlider->setMarkSide(true, true);
	m_pMicVolumeSlider->setRange(MIN_INPUT_GAIN, 0);
	m_pMicVolumeSlider->setGrooveHeight(30);
	m_pMicVolumeSlider->setSingleStep(1);
	m_pMicVolumeSlider->setPageStep(1);
	m_pMicVolumeSlider->setValue(70);
	connect(m_pMicVolumeSlider, SIGNAL(valueChanged(int)), this, SLOT(onMicVolumeSliderChanged(int)));


	m_pMicVolumeLabel = new QLabel(tr("Input Gain"), m_pMicFrame);
	m_pMicVolumeLabel->setObjectName(QStringLiteral("BlackFont14"));
	m_pMicVolumeLabel->setAlignment(Qt::AlignCenter);

	m_pMicVolumeSpinBox = new QDoubleSpinBox(m_pMicFrame);
	m_pMicVolumeSpinBox->setRange(0.0, 10.0);
	m_pMicVolumeSpinBox->setSingleStep(0.1);
	m_pMicVolumeSpinBox->setDecimals(1);
	m_pMicVolumeSpinBox->setSuffix("");
	m_pMicVolumeSpinBox->setValue(7.0);
	connect(m_pMicVolumeSpinBox, SIGNAL(valueChanged(double)), this, SLOT(onMicVolumeSpinBoxChanged(double)));
	m_pMicVolumeSpinBox->hide();		/* replace by m_pMicVolumeEdit */

	m_pMicVolumeEdit = new QLineEdit(m_pMicFrame);
	m_pMicVolumeEdit->setReadOnly(true);

	m_pMicMuteButton = new MarkButton(QString(""), m_pMicFrame);
    m_pMicMuteButton->setObjectName(QStringLiteral("muteButton"));
	connect(m_pMicMuteButton, SIGNAL(clicked()), this, SLOT(onClickMicMuteButton()));
	m_pMicMuteButton->mark(false);

	m_pMicVolumeChart = new VolumeChartWidget(m_pMicFrame);
	QStringList micVolumeStringList;
	micVolumeStringList << "";
	maxInputDB = getCPi2000Data()->getMaxInputDB();
	m_pMicVolumeChart->setVolumeData(maxInputDB, micVolumeStringList, 1);

	m_pMicDelayLabel = new QLabel(tr("Global Audio Delay"), m_pMicFrame);
	m_pMicDelayLabel->setObjectName(QStringLiteral("BlackFont12"));
	m_pMicDelayLabel->setAlignment(Qt::AlignRight | Qt::AlignVCenter);

	m_pMicDelaySpinBox = new QSpinBox(m_pMicFrame);
	m_pMicDelaySpinBox->setRange(0, 1000);
	m_pMicDelaySpinBox->setSingleStep(1);
//	m_pMicDelaySpinBox->setDecimals(1);
	m_pMicDelaySpinBox->setSuffix(" ms");
	connect(m_pMicDelaySpinBox, SIGNAL(valueChanged(int)), this, SLOT(onMicDelaySpinBoxChanged(int)));

	m_pMicEnableButton = new MarkButton(QString("Enabled"), m_pMicFrame);
    m_pMicEnableButton->setObjectName(QStringLiteral("InputPresetButton"));
	connect(m_pMicEnableButton, SIGNAL(clicked()), this, SLOT(onClickMicEnableDelayButton()));
	m_pMicEnableButton->mark(true);

	m_pMicChannelLabel = new QLabel(tr("Channel Assignment"), m_pMicFrame);
	m_pMicChannelLabel->setObjectName(QStringLiteral("BlackFont12"));
//	m_pMicChannelLabel->setAlignment(Qt::AlignRight | Qt::AlignVCenter);
	m_pMicCenterRadio = new QRadioButton(tr("Center"), m_pMicFrame);
	m_pMicCenterRadio->setObjectName(QStringLiteral("BlackFont12"));
	m_pMicCenterRadio->setChecked(true);
	connect(m_pMicCenterRadio, SIGNAL(toggled(bool)), this, SLOT(onMicCenterRadioToggled(bool)));

	m_pMicSurroundRadio = new QRadioButton(tr("Surrounds"), m_pMicFrame);
	m_pMicSurroundRadio->setObjectName(QStringLiteral("BlackFont12"));
//	connect(m_pMicSurroundRadio, SIGNAL(clicked()), this, SLOT(onClickPhantomButton()));

	

	m_pMicPhantomLabel = new QLabel(tr("Phantom Power"), m_pMicFrame);
	m_pMicPhantomLabel->setObjectName(QStringLiteral("BlackFont12"));
	m_pMicPhantomButton = new MarkButton("", m_pMicFrame);
    m_pMicPhantomButton->setObjectName(QStringLiteral("PhantomButton"));
	connect(m_pMicPhantomButton, SIGNAL(clicked()), this, SLOT(onClickPhantomButton()));
	m_pMicPhantomButton->mark(true);


	/* The following component is for Music Input */
	m_pMusicVolumeSlider = new ButtonSlider(m_pMusicFrame);
	m_pMusicVolumeSlider->setTextColor(QColor(0x10, 0x10, 0x10));
	m_pMusicVolumeSlider->setMarkColor(QColor(0x10, 0x10, 0x10));
	m_pMusicVolumeSlider->addExtraLongMark(0, " 0dB", true);
	m_pMusicVolumeSlider->addExtraLongMark(MIN_INPUT_GAIN, "-INF", false);
	m_pMusicVolumeSlider->setMark(2, 1, sliderStringList);
	{
		m_pMusicVolumeSlider->addExtraShortMark(-10, "-10dB", false);
		m_pMusicVolumeSlider->addExtraShortMark(-20, "-20dB", false);
		m_pMusicVolumeSlider->addExtraShortMark(-30, "-30dB", false);
		m_pMusicVolumeSlider->addExtraShortMark(-40, "-40dB", false);
		m_pMusicVolumeSlider->addExtraShortMark(-50, "-50dB", false);
		m_pMusicVolumeSlider->addExtraShortMark(-60, "-60dB", false);
	}

	m_pMusicVolumeSlider->setMarkLength(7, 10);
	m_pMusicVolumeSlider->setMarkSide(true, true);
	m_pMusicVolumeSlider->setRange(MIN_INPUT_GAIN, 0);
	m_pMusicVolumeSlider->setGrooveHeight(30);
	m_pMusicVolumeSlider->setSingleStep(1);
	m_pMusicVolumeSlider->setPageStep(1);
	m_pMusicVolumeSlider->setValue(70);
	connect(m_pMusicVolumeSlider, SIGNAL(valueChanged(int)), this, SLOT(onMusicVolumeSliderChanged(int)));

	m_pMusicVolumeLabel = new QLabel(tr("Input Gain"), m_pMusicFrame);
	m_pMusicVolumeLabel->setObjectName(QStringLiteral("BlackFont14"));
	m_pMusicVolumeLabel->setAlignment(Qt::AlignCenter);

	m_pMusicVolumeSpinBox = new QDoubleSpinBox(m_pMusicFrame);
	m_pMusicVolumeSpinBox->setRange(0.0, 10.0);
	m_pMusicVolumeSpinBox->setSingleStep(0.1);
	m_pMusicVolumeSpinBox->setDecimals(1);
	m_pMusicVolumeSpinBox->setSuffix("");
	m_pMusicVolumeSpinBox->setValue(7.0);
	connect(m_pMusicVolumeSpinBox, SIGNAL(valueChanged(double)), this, SLOT(onMusicVolumeSpinBoxChanged(double)));
	m_pMusicVolumeSpinBox->hide();		/* replace by m_pMusicVolumeEdit */

	m_pMusicVolumeEdit = new QLineEdit(m_pMusicFrame);
	m_pMusicVolumeEdit->setReadOnly(true);

	m_pMusicMuteButton = new MarkButton(QString(""), m_pMusicFrame);
    m_pMusicMuteButton->setObjectName(QStringLiteral("muteButton"));
	connect(m_pMusicMuteButton, SIGNAL(clicked()), this, SLOT(onClickMusicMuteButton()));
	m_pMusicMuteButton->mark(false);

	m_pMusicVolumeChart = new VolumeChartWidget(m_pMusicFrame);
	QStringList musicVolumeStringList;
	musicVolumeStringList << "";
	maxInputDB = getCPi2000Data()->getMaxInputDB();
	m_pMusicVolumeChart->setVolumeData(maxInputDB, musicVolumeStringList, 1);

	m_pMusicDelayLabel = new QLabel(tr("Global Audio Delay"), m_pMusicFrame);
	m_pMusicDelayLabel->setObjectName(QStringLiteral("BlackFont12"));
	m_pMusicDelayLabel->setAlignment(Qt::AlignRight | Qt::AlignVCenter);

	m_pMusicDelaySpinBox = new QSpinBox(m_pMusicFrame);
	m_pMusicDelaySpinBox->setRange(0, 1000);
	m_pMusicDelaySpinBox->setSingleStep(1);
//	m_pMusicDelaySpinBox->setDecimals(1);
	m_pMusicDelaySpinBox->setSuffix(" ms");
	connect(m_pMusicDelaySpinBox, SIGNAL(valueChanged(int)), this, SLOT(onMusicDelaySpinBoxChanged(int)));

	m_pMusicEnableButton = new MarkButton(QString("Enabled"), m_pMusicFrame);
    m_pMusicEnableButton->setObjectName(QStringLiteral("InputPresetButton"));
	connect(m_pMusicEnableButton, SIGNAL(clicked()), this, SLOT(onClickMusicEnableDelayButton()));
	m_pMusicEnableButton->mark(true);

	{
		m_pMusicChannelLabel = new QLabel(tr("Channel Assignment"), m_pMusicFrame);
		m_pMusicChannelLabel->setObjectName(QStringLiteral("BlackFont12"));

		QString modeString[] = {"L R", "Ls Rs Bls Brs", "Ls Rs Bls Brs Sw", "L R C Sw Ls Rs Bls Brs" };
		for (int i = 0; i < 4; i++)
		{
			m_pMusicAssignRadio[i] = new QRadioButton(modeString[i], m_pMusicFrame);
			m_pMusicAssignRadio[i]->setObjectName(QStringLiteral("BlackFont12"));
			connect(m_pMusicAssignRadio[i], SIGNAL(toggled(bool)), this, SLOT(onMusicCenterRadioToggled(bool)));
		}
	}

	m_slotEnableFlag = true;
	refreshInputSource();
}

void InputSettingWidget::resizeEvent(QResizeEvent * /*event*/)
{
	int margin = 10;

	int digitalWidth = (width() - margin * 2) * 2 / 3;
	int unitHeight = (height() - margin * 3) / 2;
	m_pDigitalFrame->setGeometry(margin, margin, digitalWidth - 2, unitHeight);

	int micWidth = (width() - margin * 2) / 3 - margin;
	m_pMicFrame->setGeometry(2 * margin + digitalWidth, margin, micWidth, unitHeight);

	m_pAnalogFrame->setGeometry(margin, 2 * margin + unitHeight, digitalWidth - 2, unitHeight);

	m_pMusicFrame->setGeometry(2 * margin + digitalWidth, 2 * margin + unitHeight, micWidth, unitHeight);

	/* The following component is for Digital Input */
	int sliderCenter = digitalWidth / 10 + 5;
	int sliderWidth = digitalWidth / 8;
	m_pDigitalVolumeSlider->setGeometry(sliderCenter - sliderWidth / 2, unitHeight / 5, sliderWidth, unitHeight * 3 / 5);
	m_pDigitalVolumeLabel->setGeometry(sliderCenter - sliderWidth / 2 - 30, unitHeight / 10 + 5, sliderWidth + 60, 20);
	int spinBoxWidth = 70;
	m_pDigitalVolumeSpinBox->setGeometry(sliderCenter - spinBoxWidth / 2, unitHeight * 9 / 10, spinBoxWidth, 20);
	m_pDigitalVolumeEdit->setGeometry(sliderCenter - spinBoxWidth / 2, unitHeight * 9 / 10, spinBoxWidth, 20);

	m_pDigitalMuteButton->setGeometry(sliderCenter - spinBoxWidth / 2 - 40, unitHeight * 9 / 10 - 2, 26, 25);
	m_pDigitalVolumeChart->setGeometry(sliderCenter + sliderWidth / 2, unitHeight / 2, 50, unitHeight / 3);

	m_pDigitalInputSettingWidget->setGeometry(sliderCenter + sliderWidth, 30, digitalWidth - sliderCenter - sliderWidth - 10, unitHeight * 8 / 10 - 10);

	int digitalStartPoint = digitalWidth / 3 - 100;
	m_pDigitalEnableButton->setGeometry(digitalStartPoint, unitHeight * 9 / 10 - 2, 60, 25);
	m_pDigitalDelayLabel->setGeometry(digitalStartPoint + 60, unitHeight * 9 / 10 - 2, 100, 25);
	m_pDigitalDelaySpinBox->setGeometry(digitalStartPoint + 165, unitHeight * 9 / 10, 60, 20);

	/* The following component is for Analog Input */
//	int analogSliderCenter = micWidth / 4;
//	int analogSliderWidth = micWidth / 3;
//	m_pAnalogVolumeSlider->setGeometry(analogSliderCenter - analogSliderWidth / 2, unitHeight / 5, analogSliderWidth, unitHeight * 3 / 5);
//	m_pAnalogVolumeLabel->setGeometry(analogSliderCenter - analogSliderWidth / 2 - 20, unitHeight / 10 + 5, analogSliderWidth + 40, 20);
	m_pAnalogVolumeSlider->setGeometry(sliderCenter - sliderWidth / 2, unitHeight / 5, sliderWidth, unitHeight * 3 / 5);
	m_pAnalogVolumeLabel->setGeometry(sliderCenter - sliderWidth / 2 - 30, unitHeight / 10 + 5, sliderWidth + 60, 20);
//	int spinBoxWidth = 70;
	m_pAnalogVolumeSpinBox->setGeometry(sliderCenter - spinBoxWidth / 2, unitHeight * 9 / 10, spinBoxWidth, 20);
	m_pAnalogVolumeEdit->setGeometry(sliderCenter - spinBoxWidth / 2, unitHeight * 9 / 10, spinBoxWidth, 20);
	m_pAnalogMuteButton->setGeometry(sliderCenter - spinBoxWidth / 2 - 40, unitHeight * 9 / 10 - 2, 26, 25);
	m_pAnalogVolumeChart->setGeometry(sliderCenter + sliderWidth / 2, unitHeight / 2, 50, unitHeight / 3);

	m_pAnalogInputSettingWidget->setGeometry(sliderCenter + sliderWidth, 30, digitalWidth - sliderCenter - sliderWidth - 10, unitHeight * 8 / 10 - 10);
	m_pAnalogEnableButton->setGeometry(digitalStartPoint, unitHeight * 9 / 10 - 2, 60, 25);
	m_pAnalogDelayLabel->setGeometry(digitalStartPoint + 60, unitHeight * 9 / 10 - 2, 100, 25);
	m_pAnalogDelaySpinBox->setGeometry(digitalStartPoint + 165, unitHeight * 9 / 10, 60, 20);

	/* The following component is for MIC Input */
	int micSliderCenter = micWidth / 5 + 10;
	int micSliderWidth = micWidth / 5 + 15;
	m_pMicVolumeSlider->setGeometry(micSliderCenter - micSliderWidth / 2, unitHeight / 5, micSliderWidth, unitHeight * 3 / 5);
	m_pMicVolumeLabel->setGeometry(micSliderCenter - micSliderWidth / 2 - 25, unitHeight / 10 + 5, micSliderWidth + 50, 20);
//	int spinBoxWidth = 70;
	m_pMicVolumeSpinBox->setGeometry(micSliderCenter - spinBoxWidth / 2, unitHeight * 9 / 10, spinBoxWidth, 20);
	m_pMicVolumeEdit->setGeometry(micSliderCenter - spinBoxWidth / 2, unitHeight * 9 / 10, spinBoxWidth, 20);
	m_pMicMuteButton->setGeometry(micSliderCenter - spinBoxWidth / 2 - 40, unitHeight * 9 / 10 - 2, 26, 25);
	m_pMicVolumeChart->setGeometry(micSliderCenter + micSliderWidth / 2 + 20, unitHeight / 2 , 50, unitHeight / 3);

	int micStartPoint = micSliderCenter + micSliderWidth - 40;
	m_pMicEnableButton->setGeometry(micStartPoint, unitHeight * 9 / 10 - 2, 60, 25);
	m_pMicDelayLabel->setGeometry(micStartPoint + 60, unitHeight * 9 / 10 - 2, 100, 25);
	m_pMicDelaySpinBox->setGeometry(micStartPoint + 165, unitHeight * 9 / 10, 60, 20);

	m_pMicChannelLabel->setGeometry(micWidth  * 7 / 12, unitHeight / 3, 120, 20);
	m_pMicCenterRadio->setGeometry(micWidth * 7 / 12, unitHeight / 3 + 25, 100, 20);
	m_pMicSurroundRadio->setGeometry(micWidth * 7 / 12, unitHeight / 3 + 50, 100, 20);

	m_pMicPhantomLabel->setGeometry(micWidth * 7 / 12, unitHeight  *7 / 12, 100, 20);
	m_pMicPhantomButton->setGeometry(micWidth * 7 / 12, unitHeight * 7/ 12 + 20, 35, 35);

	/* The following component is for MIC Input */
#if 0
	int musicSliderCenter = micWidth / 4;
	int musicSliderWidth = micWidth / 3;
	m_pMusicVolumeSlider->setGeometry(musicSliderCenter - musicSliderWidth / 2, unitHeight / 5, musicSliderWidth, unitHeight * 3 / 5);
	m_pMusicVolumeLabel->setGeometry(musicSliderCenter - musicSliderWidth / 2 - 20, unitHeight / 10 + 5, musicSliderWidth + 40, 20);
//	int spinBoxWidth = 70;
	m_pMusicVolumeSpinBox->setGeometry(musicSliderCenter - spinBoxWidth / 2, unitHeight * 9 / 10, spinBoxWidth, 20);
	m_pMusicMuteButton->setGeometry(musicSliderCenter - spinBoxWidth / 2 - 40, unitHeight * 9 / 10 - 2, 25, 25);
	m_pMusicVolumeChart->setGeometry(musicSliderCenter + musicSliderWidth / 2 + 20, unitHeight / 2 , 50, unitHeight / 3);
#endif
	int toLeft = 10;
	m_pMusicVolumeSlider->setGeometry(micSliderCenter - micSliderWidth / 2 - toLeft, unitHeight / 5, micSliderWidth, unitHeight * 3 / 5);
	m_pMusicVolumeLabel->setGeometry(micSliderCenter - micSliderWidth / 2 - 25 - toLeft, unitHeight / 10 + 5, micSliderWidth + 50, 20);
	m_pMusicVolumeSpinBox->setGeometry(micSliderCenter - spinBoxWidth / 2, unitHeight * 9 / 10, spinBoxWidth, 20);
	m_pMusicVolumeEdit->setGeometry(micSliderCenter - spinBoxWidth / 2, unitHeight * 9 / 10, spinBoxWidth, 20);
	m_pMusicMuteButton->setGeometry(micSliderCenter - spinBoxWidth / 2 - 40, unitHeight * 9 / 10 - 2, 26, 25);
	m_pMusicVolumeChart->setGeometry(micSliderCenter + micSliderWidth / 2 + 20 - toLeft, unitHeight / 2 , 50, unitHeight / 3);

	m_pMusicEnableButton->setGeometry(micStartPoint, unitHeight * 9 / 10 - 2, 60, 25);
	m_pMusicDelayLabel->setGeometry(micStartPoint + 60, unitHeight * 9 / 10 - 2, 100, 25);
	m_pMusicDelaySpinBox->setGeometry(micStartPoint + 165, unitHeight * 9 / 10, 60, 20);

	m_pMusicChannelLabel->setGeometry(micWidth  * 7 / 12 - toLeft, unitHeight / 3, 120, 20);
	for (int i = 0; i < 4; i++)
	{
		m_pMusicAssignRadio[i]->setGeometry(micWidth * 7 / 12 - toLeft, unitHeight / 3 + 25 + 25 * i, 200, 20);
	}
}

void InputSettingWidget::onClickPhantomButton()
{
	bool flag = !m_pMicPhantomButton->getMarkFlag();
	m_pMicPhantomButton->mark(flag);

	CPi2000Data *pData = getCPi2000Data();
	pData->enablePhantom(flag);

	if (g_pApp->getConnectStatus() == DEVICE_NETWORK_CONNECTED)
	{
		g_pApp->getDeviceConnection()->enablePhantom(flag);
	}
}

void InputSettingWidget::onClickDigitalMuteButton(void)
{
	bool flag = !m_pDigitalMuteButton->getMarkFlag();
	m_pDigitalMuteButton->mark(flag);

	CPi2000Data *pData = getCPi2000Data();
	pData->setDigitalMute(flag);

	if (g_pApp->getConnectStatus() == DEVICE_NETWORK_CONNECTED)
	{
		g_pApp->getDeviceConnection()->setDigitalMute(flag);
	}
}

void InputSettingWidget::onClickAnalogMuteButton(void)
{
	bool flag = !m_pAnalogMuteButton->getMarkFlag();
	m_pAnalogMuteButton->mark(flag);

	CPi2000Data *pData = getCPi2000Data();
	pData->setAnalogMute(flag);

	if (g_pApp->getConnectStatus() == DEVICE_NETWORK_CONNECTED)
	{
		g_pApp->getDeviceConnection()->setAnalogMute(flag);
	}
}

void InputSettingWidget::onClickMusicMuteButton(void)
{
	bool flag = !m_pMusicMuteButton->getMarkFlag();
	m_pMusicMuteButton->mark(flag);

	CPi2000Data *pData = getCPi2000Data();
	pData->setMusicMute(flag);

	if (g_pApp->getConnectStatus() == DEVICE_NETWORK_CONNECTED)
	{
		g_pApp->getDeviceConnection()->setMusicMute(flag);
	}
}

void InputSettingWidget::onClickMicMuteButton(void)
{
	bool flag = !m_pMicMuteButton->getMarkFlag();
	m_pMicMuteButton->mark(flag);

	CPi2000Data *pData = getCPi2000Data();
	pData->setMicMute(flag);

	if (g_pApp->getConnectStatus() == DEVICE_NETWORK_CONNECTED)
	{
		g_pApp->getDeviceConnection()->setMicMute(flag);
	}
}

void InputSettingWidget::mousePressEvent(QMouseEvent *event)
{
	if(event->type() ==  QEvent::MouseButtonPress )
	{
		QPoint pos = event->pos();
		INPUT_SOURCE inputSource = INPUT_SOURCE_NULL;
		QString inputSourceString = "";

		QRect rect = m_pDigitalFrame->geometry();
		if (rect.contains(pos) == true)
		{
			inputSource = INPUT_SOURCE_DIGITAL;
			inputSourceString = QString(tr("Digital"));
		}

		rect = m_pAnalogFrame->geometry();
		if (rect.contains(pos) == true)
		{
			inputSource = INPUT_SOURCE_ANALOG;
			inputSourceString = QString(tr("Analog"));
		}

		rect = m_pMicFrame->geometry();
		if (rect.contains(pos) == true)
		{
			inputSource = INPUT_SOURCE_MIC;
			inputSourceString = QString(tr("Mic"));
		}

		rect = m_pMusicFrame->geometry();
		if (rect.contains(pos) == true)
		{
			inputSource = INPUT_SOURCE_MUSIC;
			inputSourceString = QString(tr("Music"));
		}

		CPi2000Data *pData = getCPi2000Data();
		if ((inputSource != INPUT_SOURCE_NULL) && (inputSource != pData->getInputSource()))
		{
			QMessageBox messageBox(QMessageBox::Question, tr("Change Input Source"), tr("Do you want to change input source to: ") + inputSourceString + "?",
			            QMessageBox::Yes | QMessageBox::No,
						this);

			messageBox.setButtonText(QMessageBox::Yes, tr("Yes"));
			messageBox.setButtonText(QMessageBox::No, tr("No"));
			int resp = messageBox.exec();

//			int resp = QMessageBox::information(this, tr("Change Input Source"), tr("Do you want to change input source to: ") + inputSourceString + "?", tr("Yes"), tr("No"));
			if (resp == QMessageBox::Yes)
			{
				setInputSource(inputSource);
			}
		}
	}
}

void InputSettingWidget::setInputSource(INPUT_SOURCE inputSource)
{
	CPi2000Data *pData = getCPi2000Data();
	pData->setInputSource(inputSource);

	if (g_pApp->getConnectStatus() == DEVICE_NETWORK_CONNECTED)
	{
		g_pApp->getDeviceConnection()->setInputSource(inputSource);
	}

	g_pMainWidget->refreshInputSource();
}

void InputSettingWidget::refreshInputSource()
{
	CPi2000Data *pData = getCPi2000Data();
	INPUT_SOURCE inputSource = pData->getInputSource();

	m_pDigitalFrame->setEnabled(inputSource == INPUT_SOURCE_DIGITAL);
	m_pAnalogFrame->setEnabled(inputSource == INPUT_SOURCE_ANALOG);
	m_pMicFrame->setEnabled(inputSource == INPUT_SOURCE_MIC);
	m_pMusicFrame->setEnabled(inputSource == INPUT_SOURCE_MUSIC);
}

void InputSettingWidget::onApplyAnalogChannel()
{
	qDebug() << "onApplyAnalogChannel called here...";
	int aChannel[8];
	m_pAnalogInputSettingWidget->getChannel(aChannel);

	getCPi2000Data()->setAnalogChannel(aChannel);
	if (g_pApp->getConnectStatus() == DEVICE_NETWORK_CONNECTED)
	{
		g_pApp->getDeviceConnection()->setAnalogChannel(aChannel);
	}	
}

void InputSettingWidget::onApplyDigitalChannel()
{
	qDebug() << "onApplyDigitalChannel called here...";
	int aChannel[8];
	m_pDigitalInputSettingWidget->getChannel(aChannel);

	getCPi2000Data()->setAnalogChannel(aChannel);
	if (g_pApp->getConnectStatus() == DEVICE_NETWORK_CONNECTED)
	{
		g_pApp->getDeviceConnection()->setDigitalChannel(aChannel);
	}	
}

void InputSettingWidget::onDigitalVolumeSliderChanged(int value)
{
	if (m_slotEnableFlag == false)
	{
		return;
	}

	CPi2000Data *pData = getCPi2000Data();
	if (value <= MIN_INPUT_GAIN)
	{
		value = MIN_INPUT_GAIN_TO_DEVICE;
	}
	pData->setDigitalVolume(value);

	if (g_pApp->getConnectStatus() == DEVICE_NETWORK_CONNECTED)
	{
		g_pApp->getDeviceConnection()->setDigitalVolume(value);
	}

	refreshDigitalVolume();
}

void InputSettingWidget::onAnalogVolumeSliderChanged(int value)
{
	if (m_slotEnableFlag == false)
	{
		return;
	}

	CPi2000Data *pData = getCPi2000Data();
	if (value <= MIN_INPUT_GAIN)
	{
		value = MIN_INPUT_GAIN_TO_DEVICE;
	}
	pData->setAnalogVolume(value);

	if (g_pApp->getConnectStatus() == DEVICE_NETWORK_CONNECTED)
	{
		g_pApp->getDeviceConnection()->setAnalogVolume(value);
	}

	refreshAnalogVolume();
}

void InputSettingWidget::onMicVolumeSliderChanged(int value)
{
	if (m_slotEnableFlag == false)
	{
		return;
	}

	CPi2000Data *pData = getCPi2000Data();
	if (value <= MIN_INPUT_GAIN)
	{
		value = MIN_INPUT_GAIN_TO_DEVICE;
	}
	pData->setMicVolume(value);

	if (g_pApp->getConnectStatus() == DEVICE_NETWORK_CONNECTED)
	{
		g_pApp->getDeviceConnection()->setMicVolume(value);
	}

	refreshMicVolume();

}

void InputSettingWidget::onMusicVolumeSliderChanged(int value)
{
	if (m_slotEnableFlag == false)
	{
		return;
	}

	CPi2000Data *pData = getCPi2000Data();
	if (value <= MIN_INPUT_GAIN)
	{
		value = MIN_INPUT_GAIN_TO_DEVICE;
	}
	pData->setMusicVolume(value);

	if (g_pApp->getConnectStatus() == DEVICE_NETWORK_CONNECTED)
	{
		g_pApp->getDeviceConnection()->setMusicVolume(value);
	}

	refreshMusicVolume();
}

void InputSettingWidget::onDigitalVolumeSpinBoxChanged(double volume)
{
	if (m_slotEnableFlag == false)
	{
		return;
	}

	CPi2000Data *pData = getCPi2000Data();
	pData->setDigitalVolume(volume);

	if (g_pApp->getConnectStatus() == DEVICE_NETWORK_CONNECTED)
	{
		g_pApp->getDeviceConnection()->setDigitalVolume(volume);
	}

	refreshDigitalVolume();
}

void InputSettingWidget::onAnalogVolumeSpinBoxChanged(double volume)
{
	if (m_slotEnableFlag == false)
	{
		return;
	}

	CPi2000Data *pData = getCPi2000Data();
	pData->setAnalogVolume(volume);

	if (g_pApp->getConnectStatus() == DEVICE_NETWORK_CONNECTED)
	{
		g_pApp->getDeviceConnection()->setAnalogVolume(volume);
	}

	refreshAnalogVolume();
}

void InputSettingWidget::onMicVolumeSpinBoxChanged(double volume)
{
	if (m_slotEnableFlag == false)
	{
		return;
	}

	CPi2000Data *pData = getCPi2000Data();
	pData->setMicVolume(volume);

	if (g_pApp->getConnectStatus() == DEVICE_NETWORK_CONNECTED)
	{
		g_pApp->getDeviceConnection()->setMicVolume(volume);
	}

	refreshMicVolume();
}

void InputSettingWidget::onMusicVolumeSpinBoxChanged(double volume)
{
	if (m_slotEnableFlag == false)
	{
		return;
	}

	CPi2000Data *pData = getCPi2000Data();
	pData->setMusicVolume(volume);

	if (g_pApp->getConnectStatus() == DEVICE_NETWORK_CONNECTED)
	{
		g_pApp->getDeviceConnection()->setMusicVolume(volume);
	}

	refreshMusicVolume();
}


void InputSettingWidget::refreshAnalogVolume()
{
	CPi2000Data *pData = getCPi2000Data();
	float volume = pData->getAnalogVolume();

	bool oldFlag = m_slotEnableFlag; 
	m_slotEnableFlag = false;

	if (volume <= MIN_INPUT_GAIN)
	{
		m_pAnalogVolumeSlider->setValue(MIN_INPUT_GAIN);
		m_pAnalogVolumeSpinBox->setValue(MIN_INPUT_GAIN);
		m_pAnalogVolumeEdit->setText(QString("-INF"));
	}
	else
	{
		m_pAnalogVolumeSlider->setValue(volume);
		m_pAnalogVolumeSpinBox->setValue(volume);
		m_pAnalogVolumeEdit->setText(QString("%1 dB").arg(volume));
	}

	m_slotEnableFlag = oldFlag;
}

void InputSettingWidget::refreshDigitalVolume()
{
	CPi2000Data *pData = getCPi2000Data();
	int volume = qRound(pData->getDigitalVolume());

	bool oldFlag = m_slotEnableFlag; 
	m_slotEnableFlag = false;

	if (volume <= MIN_INPUT_GAIN)
	{
		m_pDigitalVolumeSlider->setValue(MIN_INPUT_GAIN);
		m_pDigitalVolumeSpinBox->setValue(MIN_INPUT_GAIN);
		m_pDigitalVolumeEdit->setText(QString("-INF"));
	}
	else
	{
		m_pDigitalVolumeSlider->setValue(volume);
		m_pDigitalVolumeSpinBox->setValue(volume);
		m_pDigitalVolumeEdit->setText(QString("%1 dB").arg(volume));
	}

	m_slotEnableFlag = oldFlag;
}

void InputSettingWidget::refreshMicVolume()
{
	CPi2000Data *pData = getCPi2000Data();
	float volume = pData->getMicVolume();

	bool oldFlag = m_slotEnableFlag; 
	m_slotEnableFlag = false;

	if (volume <= MIN_INPUT_GAIN)
	{
		m_pMicVolumeSlider->setValue(MIN_INPUT_GAIN);
		m_pMicVolumeSpinBox->setValue(MIN_INPUT_GAIN);
		m_pMicVolumeEdit->setText(QString("-INF"));
	}
	else
	{
		m_pMicVolumeSlider->setValue(volume);
		m_pMicVolumeSpinBox->setValue(volume);
		m_pMicVolumeEdit->setText(QString("%1 dB").arg(volume));
	}

	m_slotEnableFlag = oldFlag;
}

void InputSettingWidget::refreshMusicVolume()
{
	CPi2000Data *pData = getCPi2000Data();
	float volume = pData->getMusicVolume();

	bool oldFlag = m_slotEnableFlag; 
	m_slotEnableFlag = false;

	if (volume <= MIN_INPUT_GAIN)
	{
		m_pMusicVolumeSlider->setValue(MIN_INPUT_GAIN);
		m_pMusicVolumeSpinBox->setValue(MIN_INPUT_GAIN);
		m_pMusicVolumeEdit->setText(QString("-INF"));
	}
	else
	{
		m_pMusicVolumeSlider->setValue(volume);
		m_pMusicVolumeSpinBox->setValue(volume);
		m_pMusicVolumeEdit->setText(QString("%1 dB").arg(volume));
	}

	m_slotEnableFlag = oldFlag;
}

void InputSettingWidget::onClickMusicEnableDelayButton()
{
	bool flag = !m_pMusicEnableButton->getMarkFlag();

	CPi2000Data *pData = getCPi2000Data();
	pData->setMusicDelayEnable(flag);

	if (g_pApp->getConnectStatus() == DEVICE_NETWORK_CONNECTED)
	{
		g_pApp->getDeviceConnection()->setMusicDelayEnable(flag);
	}
	refreshMusicDelayEnableButton();
}

void InputSettingWidget::onClickMicEnableDelayButton()
{
	bool flag = !m_pMicEnableButton->getMarkFlag();

	CPi2000Data *pData = getCPi2000Data();
	pData->setMicDelayEnable(flag);

	if (g_pApp->getConnectStatus() == DEVICE_NETWORK_CONNECTED)
	{
		g_pApp->getDeviceConnection()->setMicDelayEnable(flag);
	}
	refreshMicDelayEnableButton();
}

void InputSettingWidget::onClickDigitalEnableDelayButton()
{
	bool flag = !m_pDigitalEnableButton->getMarkFlag();

	CPi2000Data *pData = getCPi2000Data();
	pData->setDigitalDelayEnable(flag);

	if (g_pApp->getConnectStatus() == DEVICE_NETWORK_CONNECTED)
	{
		g_pApp->getDeviceConnection()->setDigitalDelayEnable(flag);
	}
	refreshDigitalDelayEnableButton();
}

void InputSettingWidget::onClickAnalogEnableDelayButton()
{
	bool flag = !m_pAnalogEnableButton->getMarkFlag();

	CPi2000Data *pData = getCPi2000Data();
	pData->setAnalogDelayEnable(flag);

	if (g_pApp->getConnectStatus() == DEVICE_NETWORK_CONNECTED)
	{
		g_pApp->getDeviceConnection()->setAnalogDelayEnable(flag);
	}
	refreshAnalogDelayEnableButton();
}


void InputSettingWidget::refreshDigitalDelayEnableButton()
{
	CPi2000Data *pData = getCPi2000Data();
	bool flag = pData->getDigitalDelayEnable();

	m_pDigitalEnableButton->mark(flag);
	if (flag)
	{
		m_pDigitalEnableButton->setText(tr("Enabled"));
//		m_pDigitalDelayLabel->setEnabled(true);
		m_pDigitalDelaySpinBox->setEnabled(true);
	}
	else
	{
		m_pDigitalEnableButton->setText(tr("Enable"));	
//		m_pDigitalDelayLabel->setEnabled(false);
		m_pDigitalDelaySpinBox->setEnabled(false);
	}
}


void InputSettingWidget::refreshMicDelayEnableButton()
{
	CPi2000Data *pData = getCPi2000Data();
	bool flag = pData->getMicDelayEnable();

	m_pMicEnableButton->mark(flag);
	if (flag)
	{
		m_pMicEnableButton->setText(tr("Enabled"));
//		m_pMicDelayLabel->setEnabled(true);
		m_pMicDelaySpinBox->setEnabled(true);
	}
	else
	{
		m_pMicEnableButton->setText(tr("Enable"));	
//		m_pMicDelayLabel->setEnabled(false);
		m_pMicDelaySpinBox->setEnabled(false);
	}
}


void InputSettingWidget::refreshAnalogDelayEnableButton()
{
	CPi2000Data *pData = getCPi2000Data();
	bool flag = pData->getAnalogDelayEnable();

	m_pAnalogEnableButton->mark(flag);
	if (flag)
	{
		m_pAnalogEnableButton->setText(tr("Enabled"));
//		m_pAnalogDelayLabel->setEnabled(true);
		m_pAnalogDelaySpinBox->setEnabled(true);
	}
	else
	{
		m_pAnalogEnableButton->setText(tr("Enable"));	
//		m_pAnalogDelayLabel->setEnabled(false);
		m_pAnalogDelaySpinBox->setEnabled(false);
	}
}


void InputSettingWidget::refreshMusicDelayEnableButton()
{
	CPi2000Data *pData = getCPi2000Data();
	bool flag = pData->getMusicDelayEnable();

	m_pMusicEnableButton->mark(flag);
	if (flag)
	{
		m_pMusicEnableButton->setText(tr("Enabled"));
//		m_pMusicDelayLabel->setEnabled(true);
		m_pMusicDelaySpinBox->setEnabled(true);
	}
	else
	{
		m_pMusicEnableButton->setText(tr("Enable"));	
//		m_pMusicDelayLabel->setEnabled(false);
		m_pMusicDelaySpinBox->setEnabled(false);
	}
}

void InputSettingWidget::onMusicDelaySpinBoxChanged(int delayTime)
{
	if (m_slotEnableFlag == false)
	{
		return;
	}

	CPi2000Data *pData = getCPi2000Data();
	pData->setMusicDelay(delayTime);

	if (g_pApp->getConnectStatus() == DEVICE_NETWORK_CONNECTED)
	{
		g_pApp->getDeviceConnection()->setMusicDelay(delayTime);
	}
}

void InputSettingWidget::onMicDelaySpinBoxChanged(int delayTime)
{
	if (m_slotEnableFlag == false)
	{
		return;
	}

	CPi2000Data *pData = getCPi2000Data();
	pData->setMicDelay(delayTime);

	if (g_pApp->getConnectStatus() == DEVICE_NETWORK_CONNECTED)
	{
		g_pApp->getDeviceConnection()->setMicDelay(delayTime);
	}
}

void InputSettingWidget::onAnalogDelaySpinBoxChanged(int delayTime)
{
	if (m_slotEnableFlag == false)
	{
		return;
	}

	CPi2000Data *pData = getCPi2000Data();
	pData->setAnalogDelay(delayTime);

	if (g_pApp->getConnectStatus() == DEVICE_NETWORK_CONNECTED)
	{
		g_pApp->getDeviceConnection()->setAnalogDelay(delayTime);
	}
}

void InputSettingWidget::onDigitalDelaySpinBoxChanged(int delayTime)
{
	if (m_slotEnableFlag == false)
	{
		return;
	}

	CPi2000Data *pData = getCPi2000Data();
	pData->setDigitalDelay(delayTime);

	if (g_pApp->getConnectStatus() == DEVICE_NETWORK_CONNECTED)
	{
		g_pApp->getDeviceConnection()->setDigitalDelay(delayTime);
	}
}

void InputSettingWidget::onMicCenterRadioToggled(bool flag)
{
	if (m_slotEnableFlag == false)
	{
		return;
	}

	CPi2000Data *pData = getCPi2000Data();
	if (flag)
	{
		pData->setMicChannel(MIC_CHANNEL_CENTER);
	}
	else
	{
		pData->setMicChannel(MIC_CHANNEL_SURROUNDS);	
	}

	if (g_pApp->getConnectStatus() == DEVICE_NETWORK_CONNECTED)
	{
		if (flag)
		{
			g_pApp->getDeviceConnection()->setMicChannel(MIC_CHANNEL_CENTER);
		}
		else
		{
			g_pApp->getDeviceConnection()->setMicChannel(MIC_CHANNEL_SURROUNDS);
		}
	}
}

void InputSettingWidget::refresh()
{
	bool oldFlag = m_slotEnableFlag; 
	m_slotEnableFlag = false;

	CPi2000Data *pData = getCPi2000Data();

	refreshAnalogVolume();
	refreshDigitalVolume();
	refreshMicVolume();
	refreshMusicVolume();

	refreshInputSource();

	m_pDigitalMuteButton->mark(pData->getDigitalMute());
	m_pAnalogMuteButton->mark(pData->getAnalogMute());
	m_pMicMuteButton->mark(pData->getMicMute());
	m_pMusicMuteButton->mark(pData->getMusicMute());

	refreshDigitalDelayEnableButton();
	refreshMicDelayEnableButton();
	refreshAnalogDelayEnableButton();
	refreshMusicDelayEnableButton();

	m_pDigitalDelaySpinBox->setValue(pData->getDigitalDelay());
	m_pAnalogDelaySpinBox->setValue(pData->getAnalogDelay());
	m_pMicDelaySpinBox->setValue(pData->getMicDelay());
	m_pMusicDelaySpinBox->setValue(pData->getMusicDelay());

	if (pData->getMicChannel() == MIC_CHANNEL_CENTER)
	{
		m_pMicSurroundRadio->setChecked(false);
		m_pMicCenterRadio->setChecked(true);
	}
	else
	{
		m_pMicCenterRadio->setChecked(false);	
		m_pMicSurroundRadio->setChecked(true);
	}

	int musciChannelAssign = pData->getMusicChannel();
	for (int i = 0; i < 4; i++)
	{
		m_pMusicAssignRadio[i]->setChecked(musciChannelAssign == i);
	}

	m_pMicPhantomButton->mark(pData->getPhantomFlag());

	m_pDigitalInputSettingWidget->setChannel(pData->getDigitalChannel());
	m_pAnalogInputSettingWidget->setChannel(pData->getAnalogChannel());

	m_slotEnableFlag = oldFlag;
}

void InputSettingWidget::onMusicCenterRadioToggled(bool flag)
{
	if (m_slotEnableFlag == false)
		return;

	for (int i = 0; i < 4; i++)
	{
		if ((m_pMusicAssignRadio[i]->isChecked() == true) && (flag == true))
		{
			qDebug() << "onMusicCenterRadioToggled " << i << "flag = " << flag;
			CPi2000Data *pData = getCPi2000Data();
			pData->setMusicChannel(i);

			if (g_pApp->getConnectStatus() == DEVICE_NETWORK_CONNECTED)
			{
				g_pApp->getDeviceConnection()->setMusicChannel(i);
			}
			break;
		}
	}
}

void InputSettingWidget::refreshInputVolume()
{
	bool oldFlag = m_slotEnableFlag; 
	m_slotEnableFlag = false;

	CPi2000Data *pData = getCPi2000Data();
	if (pData->getInputSource() == INPUT_SOURCE_DIGITAL)
	{
		m_pDigitalVolumeChart->refresh();
	}
	else if (pData->getInputSource() == INPUT_SOURCE_MIC)
	{
		m_pMicVolumeChart->refresh();
	}
	else if (pData->getInputSource() == INPUT_SOURCE_ANALOG)
	{
		m_pAnalogVolumeChart->refresh();
	}
	else if (pData->getInputSource() == INPUT_SOURCE_MUSIC)
	{
		m_pMusicVolumeChart->refresh();
	}

	m_slotEnableFlag = oldFlag;
}

void InputSettingWidget::retranslateUi()
{
	m_pDigitalFrame->retranslateUi(tr("      DIGITAL"));
	m_pAnalogFrame->retranslateUi(tr("      ANALOG"));
	m_pMicFrame->retranslateUi(tr("      MIC"));
	m_pMusicFrame->retranslateUi(tr("      MUSIC(NON-SYNC)"));
	m_pDigitalVolumeLabel->setText(tr("Input Gain"));
	m_pDigitalDelayLabel->setText(tr("Global Audio Delay"));

	MarkButton *m_pEnableButton[] = {m_pDigitalEnableButton, m_pAnalogEnableButton, m_pMicEnableButton, m_pMusicEnableButton };
	for (int i = 0; i < 4; i++)
	{
		if (m_pEnableButton[i]->getMarkFlag() == true)
		{
			m_pEnableButton[i]->setText(tr("Enabled"));
		}
		else
		{
			m_pEnableButton[i]->setText(tr("Enable"));
		}
	}

	m_pAnalogVolumeLabel->setText(tr("Input Gain"));
	m_pAnalogDelayLabel->setText(tr("Global Audio Delay"));
	m_pMicVolumeLabel->setText(tr("Input Gain"));
	m_pMicDelayLabel->setText(tr("Global Audio Delay"));
	m_pMicChannelLabel->setText(tr("Channel Assignment"));
	m_pMusicChannelLabel->setText(tr("Channel Assignment"));
	m_pMicSurroundRadio->setText(tr("Surrounds"));
	m_pMicPhantomLabel->setText(tr("Phantom Power"));
	m_pMusicVolumeLabel->setText(tr("Input Gain"));
	m_pMusicDelayLabel->setText(tr("Global Audio Delay"));
	m_pDigitalInputSettingWidget->retranslateUi();
	m_pAnalogInputSettingWidget->retranslateUi();
	m_pMicCenterRadio->setText(tr("Center"));
}