#include "LogWidget.h"
#include <QPainter>
#include <QHeaderView>
#include "mainApplication.h"
#include "QFileDialog"
#include "QMessageBox"
#include "WaittingWidget.h"

LogWidget::LogWidget(QWidget *parent)	: QWidget(parent)
{
	QStringList labels;
    labels << tr("Date") << tr("Time") << tr("Level") << tr("Content");

	m_pLogTable = new QTableWidget(this);
	m_pLogTable->setObjectName(QStringLiteral("m_pLogTable"));
    m_pLogTable->setColumnCount(4);
    m_pLogTable->setHorizontalHeaderLabels(labels);
	m_pLogTable->setEditTriggers(QAbstractItemView::NoEditTriggers);
	m_pLogTable->setSelectionBehavior(QAbstractItemView::SelectRows);	
	m_pLogTable->setSelectionMode(QAbstractItemView::SingleSelection);

	QHeaderView *pHeaderView = m_pLogTable->horizontalHeader();
	pHeaderView->resizeSections(QHeaderView::Fixed);
	pHeaderView->setFixedHeight(40);

	m_pRefreshButton = new QPushButton(tr("Refresh Log"), this);
	connect(m_pRefreshButton, SIGNAL(clicked()), this, SLOT(onRefreshClicked()));	

	m_pSaveButton = new QPushButton(tr("Save Log"), this);
	connect(m_pSaveButton, SIGNAL(clicked()), this, SLOT(onSaveClicked()));	

	m_pClearButton = new QPushButton(tr("Clear Log"), this);
	connect(m_pClearButton, SIGNAL(clicked()), this, SLOT(onClearClicked()));	
	m_pClearButton->hide();

	m_pInfoBox = new QCheckBox(tr("Information"), this);
	m_pInfoBox->setCheckable(true);
	m_pInfoBox->setChecked(true);
	m_pInfoBox->setObjectName(QStringLiteral("WhiteFont12"));
	connect(m_pInfoBox, SIGNAL(clicked()), this, SLOT(onInfoClicked()));	

	m_pWarningBox = new QCheckBox(tr("Warning"), this);
	m_pWarningBox->setCheckable(true);
	m_pWarningBox->setChecked(true);
	m_pWarningBox->setObjectName(QStringLiteral("WhiteFont12"));
	connect(m_pWarningBox, SIGNAL(clicked()), this, SLOT(onWarningClicked()));	

	m_pErrorBox = new QCheckBox(tr("Error"), this);
	m_pErrorBox->setCheckable(true);
	m_pErrorBox->setChecked(true);
	m_pErrorBox->setObjectName(QStringLiteral("WhiteFont12"));
	connect(m_pErrorBox, SIGNAL(clicked()), this, SLOT(onErrorClicked()));	

	m_pCriticalBox = new QCheckBox(tr("Critical"), this);
	m_pCriticalBox->setCheckable(true);
	m_pCriticalBox->setChecked(true);
	m_pCriticalBox->setObjectName(QStringLiteral("WhiteFont12"));
	connect(m_pCriticalBox, SIGNAL(clicked()), this, SLOT(onCriticalClicked()));	

	refreshLog();
}

void LogWidget::refreshLog()
{
	LogManager s_filterLogManager;

	LogManager *pLogManager = getLogManager();
	int filterMask = 0;
	if (m_pInfoBox->isChecked())
	{
		filterMask += 1;
	}
	if (m_pWarningBox->isChecked())
	{
		filterMask += 2;
	}
	if (m_pErrorBox->isChecked())
	{
		filterMask += 4;
	}
	if (m_pCriticalBox->isChecked())
	{
		filterMask += 8;
	}

	pLogManager->filter(filterMask, s_filterLogManager);

	m_pLogTable->setRowCount(s_filterLogManager.getCount());

	QStringList levelList;
	levelList << "Info" << "Warning" << "Error" << "Critical";

	int totalCount = s_filterLogManager.getCount();
	for (int index = 0; index < totalCount; index++)
	{
		Log *pLog = s_filterLogManager.getLog(totalCount - 1 - index);

		m_pLogTable->setRowHeight(index, 40);

		QTableWidgetItem *pTemp = new QTableWidgetItem(pLog->m_date.toString(Qt::ISODate));
		pTemp->setTextAlignment(Qt::AlignCenter);
		m_pLogTable->setItem(index, 0, pTemp);

		pTemp = new QTableWidgetItem(pLog->m_time.toString("hh:mm:ss.zzz"));	
		pTemp->setTextAlignment(Qt::AlignCenter);
		m_pLogTable->setItem(index, 1, pTemp);

		int level = (int)pLog->m_level;
		pTemp = new QTableWidgetItem(levelList.at(level));
		pTemp->setTextAlignment(Qt::AlignCenter);
		m_pLogTable->setItem(index, 2, pTemp);

		pTemp = new QTableWidgetItem(pLog->m_content);
		pTemp->setTextAlignment(Qt::AlignCenter);
		m_pLogTable->setItem(index, 3, pTemp);
	}

	m_pSaveButton->setEnabled(s_filterLogManager.getCount() != 0);
}

void LogWidget::resizeEvent(QResizeEvent * /* event */)
{
	int leftMargin = 30;
	int bottomMargin = 50;

	int top = 40;

	m_pLogTable->setGeometry(leftMargin, top, width() - 2 * leftMargin, height() - top - bottomMargin);
	m_pClearButton->setGeometry(width() - 310, height() - 40, 80, 25);
	m_pRefreshButton->setGeometry(width() - 210, height() - 40, 80, 25);
	m_pSaveButton->setGeometry(width() - 110, height() - 40, 80, 25);

	int boxWidth = 100;
	m_pInfoBox->setGeometry(width() / 5 - boxWidth / 2, 10, boxWidth, 20);
	m_pWarningBox->setGeometry(width() * 2 / 5 - boxWidth / 2, 10, boxWidth, 20);
	m_pErrorBox->setGeometry(width() * 3 / 5 - boxWidth / 2, 10, boxWidth, 20);
	m_pCriticalBox->setGeometry(width() * 4 / 5 - boxWidth / 2, 10, boxWidth, 20);

	m_pLogTable->setColumnWidth(0, 100);
	m_pLogTable->setColumnWidth(1, 100);
	m_pLogTable->setColumnWidth(2, 100);
	m_pLogTable->setColumnWidth(3, width() - 310);
}

void LogWidget::paintEvent(QPaintEvent * /* event */)
{
#if 0
	QPainter painter(this);
	painter.setPen(QPen(QColor(0xff, 0x70, 0x70), 3));
	painter.drawRect(1, 1, width() - 2, height() - 2);
#endif
}

void LogWidget::onRefreshClicked()
{
	m_pClearButton->setEnabled(false);
	m_pRefreshButton->setEnabled(false);
	m_pSaveButton->setEnabled(false);

	m_pLogTable->setRowCount(0);

	g_waittingWidget.startWaitting(this);

	g_pApp->getNewLog();
	refreshLog();

	g_waittingWidget.stopWaitting();

	m_pClearButton->setEnabled(true);
	m_pRefreshButton->setEnabled(true);
}

void LogWidget::onSaveClicked()
{
	LogManager s_filterLogManager;

	LogManager *pLogManager = getLogManager();
	int filterMask = 0, index = 0;
	if (m_pInfoBox->isChecked())
	{
		filterMask += 1;
	}
	if (m_pWarningBox->isChecked())
	{
		filterMask += 2;
	}
	if (m_pErrorBox->isChecked())
	{
		filterMask += 4;
	}
	if (m_pCriticalBox->isChecked())
	{
		filterMask += 8;
	}

	pLogManager->filter(filterMask, s_filterLogManager);

	QString directory = ".";
	QString fileName = QFileDialog::getSaveFileName(
		this,
		tr("Save Log File"),
		directory,
		"Text File(*.txt);; All files (*.*)"
//		tr("Excel File(*.xlsx);; Text File(*.txt);; All files (*.*)")
	);

	if (fileName.isEmpty() == true)
	{
		return;
	}

	QFile file(fileName);
	if (!file.open(QIODevice::WriteOnly | QIODevice::Text)) 
	{
        QMessageBox::warning(this, tr("Save File Error"),
                             tr("Cannot save the device settings to file: %1.")
                             .arg(file.fileName()), tr("OK"));
        return;
    }
	QTextStream out(&file);

	QStringList levelList;
	levelList << "Info" << "Warning" << "Error" << "Critical";
	QFileInfo fileInfo(fileName);
	bool excelFlag = false;
	if (fileInfo.suffix() == QString("xlsx"))
	{
		excelFlag = true;
	}

	for (index = 0; index < s_filterLogManager.getCount(); index++)
	{
		Log *pLog = s_filterLogManager.getLog(index);
		if (excelFlag == true)
		{
//			out << "\"" << pLog->m_date.toString(Qt::ISODate) << "\"\n\"" << pLog->m_time.toString() << "\"\n\"" << pLog->m_content	<< "\"\n";
		}
		else
		{
			out << pLog->m_date.toString(Qt::ISODate) << "\t" << pLog->m_time.toString("hh:mm:ss.zzz") << "\t" << levelList.at((int)pLog->m_level) << "\t" << pLog->m_content	<< "\n";
		}
	}
	file.close();
}

void LogWidget::onClearClicked()
{
	m_pClearButton->setEnabled(false);
	m_pRefreshButton->setEnabled(false);
	m_pSaveButton->setEnabled(false);

	g_waittingWidget.startWaitting(this);
	g_pApp->clearLog();
	refreshLog();
	g_pApp->msleep(1000);

	g_waittingWidget.stopWaitting();
	m_pClearButton->setEnabled(true);
	m_pRefreshButton->setEnabled(true);
}

void LogWidget::clearLog()
{
	LogManager *pLogManager = getLogManager();
	pLogManager->clearAll();

	refreshLog();
}

void LogWidget::onInfoClicked()
{
	refreshLog();
}

void LogWidget::onWarningClicked()
{
	refreshLog();
}

void LogWidget::onErrorClicked()
{
	refreshLog();
}

void LogWidget::onCriticalClicked()
{
	refreshLog();
}

void LogWidget::refreshButtonStatus(bool connectFlag)
{
	m_pClearButton->setEnabled(connectFlag);
	m_pRefreshButton->setEnabled(connectFlag);
}

void LogWidget::retranslateUi()
{
	QStringList labels;
    labels << tr("Date") << tr("Time") << tr("Level") << tr("Content");
    m_pLogTable->setHorizontalHeaderLabels(labels);

	m_pRefreshButton->setText(tr("Refresh Log"));
	m_pSaveButton->setText(tr("Save Log"));
	m_pClearButton->setText(tr("Clear Log"));
	m_pInfoBox->setText(tr("Information"));
	m_pWarningBox->setText(tr("Warning"));
	m_pErrorBox->setText(tr("Error"));
	m_pCriticalBox->setText(tr("Critical"));
}