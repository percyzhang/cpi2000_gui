#include "MainTabWidget.h"
#include <QPainter>
#include <QtDebug>

MainTabWidget::MainTabWidget(QWidget *parent)
    : QTabWidget(parent)
{
	m_slop = 4;				//slop of the Tab
	m_tabRoundRadius = 6;	//Round Radius of Tab
	m_tabWidth = 57;		// width of the tab
	m_tabHeight = 35;		// height of the tab
	m_roundRadius = 10;     // round radius of the frame
	m_frameSize = 6;		//reserved for frame
	m_tabRightOffset = 16;	//right offset of the tabBar

	connect(this, SIGNAL(currentChanged(int)), this, SLOT(onTabChanged(int)));
}

void MainTabWidget::onTabChanged(int /* nIndex */)
{
/*
	EQFilterWidget *pWidget = (EQFilterWidget *)currentWidget();
	g_pApp->onTabChanged(pWidget);
	pWidget->onTabChanged();
*/
}

#if 0
void MainTabWidget::paintEvent(QPaintEvent * /*event */)
{
	int colorDark = 189; //This color is the darkest color of the TabWidget background;
	int colorLight = 219;  //This color is the lightest color of the TabWidget background;

	if (count() == 0)
	{
		return;
	}

//	EQFilterWidget *pCurrentWidget = (EQFilterWidget *)currentWidget();
//	m_eqLength = pCurrentWidget->getEQTabWidth();       // length for EQ Button


	QPainter painter(this);
	painter.setPen(QPen(QColor(10, 10, 10), 1));
	painter.setRenderHint(QPainter::HighQualityAntialiasing);

	{   //draw the frame
	    QPainterPath path;
		//	painter.setRenderHint(QPainter::Antialiasing);
		painter.setBrush(QColor(255, 255, 255, 150));

		path.moveTo(m_roundRadius, m_tabHeight);
		path.lineTo(width() - m_eqLength - m_frameSize - m_roundRadius, m_tabHeight);
		path.arcTo(width() - m_eqLength - m_frameSize - 2 * m_roundRadius,  m_tabHeight - 2 * m_roundRadius, 2 * m_roundRadius, 2 * m_roundRadius, 270, 90);
		path.lineTo(width() - m_eqLength - m_frameSize, m_roundRadius);
		path.arcTo(width() - m_eqLength - m_frameSize, 0, 2 * m_roundRadius, 2 * m_roundRadius, 180, -90);
		path.lineTo(width() - m_frameSize - m_roundRadius, 0);
		path.arcTo(width() - m_frameSize - 2 * m_roundRadius, 0, 2 * m_roundRadius, 2 * m_roundRadius, 90, -90);
		path.lineTo(width() - m_frameSize, height() - m_frameSize - m_roundRadius);
		path.arcTo(width() - m_frameSize - 2 * m_roundRadius, height() - m_frameSize - 2 * m_roundRadius, 2 * m_roundRadius, 2 * m_roundRadius, 0, -90);
		path.lineTo(m_roundRadius, height() - m_frameSize);
		path.arcTo(0, height() - m_frameSize - 2 * m_roundRadius, 2 * m_roundRadius, 2 * m_roundRadius, 270, -90);
		path.lineTo(0, m_tabHeight + m_roundRadius);
		path.arcTo(0, m_tabHeight, 2 * m_roundRadius, 2 * m_roundRadius, 180, -90);
		path.closeSubpath();
		painter.drawPath(path);
	}

   	/* Draw tabBar */
	{
		QLinearGradient linearGradient(QPointF(0, 0), QPointF(0, m_tabHeight));     
		linearGradient.setColorAt(0.0, QColor(203, 203, 203));         
		linearGradient.setColorAt(0.04, QColor(243, 243, 243));         
		linearGradient.setColorAt(0.25, QColor(190, 190, 190));
		linearGradient.setColorAt(0.18, QColor(190, 190, 190));
		linearGradient.setColorAt(0.7, QColor(190, 190, 190));
		linearGradient.setColorAt(0.96, QColor(247, 247, 247));
		linearGradient.setSpread(QGradient::PadSpread);  
		painter.setBrush(linearGradient); 

		for (int i = count() - 1; i >= 0; i--)
		{
			if (i == currentIndex())
			{
				continue;
			}
			QPainterPath path;
			path.moveTo(m_tabRightOffset + (i + 1) * m_tabWidth + m_slop, m_tabHeight);
			path.lineTo(m_tabRightOffset + (i + 1) * m_tabWidth - m_slop, m_tabRoundRadius);
			path.arcTo(m_tabRightOffset + (i + 1) * m_tabWidth - m_slop - 2 * m_tabRoundRadius, 0, 2 * m_tabRoundRadius, 2 * m_tabRoundRadius, 0, 90);
			path.lineTo(m_tabRightOffset + i * m_tabWidth + m_slop + m_tabRoundRadius, 0);
			path.arcTo(m_tabRightOffset + i * m_tabWidth + m_slop, 0, 2 * m_tabRoundRadius, 2 * m_tabRoundRadius, 90, 90);
			path.lineTo(m_tabRightOffset + i * m_tabWidth - m_slop, m_tabHeight);
			path.closeSubpath();
			painter.drawPath(path);
		}

		{
			/* Get the background color for current Tab */
			int x = (currentIndex() + 0.5) * m_tabWidth + m_tabRightOffset + 5;
			int unit = (width()+ 10) / 5;
			int xColor;
			x = x % (2 * unit);
			if (x <= unit)
			{
				xColor = colorDark + x * (colorLight - colorDark) / unit;
			}
			else
			{
				xColor = colorDark + (colorLight - colorDark) * (2 * unit - x) / unit;
			}

			/* Paint the current Tab */
			QPainterPath path;
			path.moveTo(m_tabRightOffset + (currentIndex() + 1) * m_tabWidth + m_slop, m_tabHeight);
			path.lineTo(m_tabRightOffset + (currentIndex() + 1) * m_tabWidth - m_slop, m_tabRoundRadius);
			path.arcTo(m_tabRightOffset + (currentIndex() + 1) * m_tabWidth - m_slop - 2 * m_tabRoundRadius, 0, 2 * m_tabRoundRadius, 2 * m_tabRoundRadius, 0, 90);
			path.lineTo(m_tabRightOffset + currentIndex() * m_tabWidth + m_slop + m_tabRoundRadius, 0);
			path.arcTo(m_tabRightOffset + currentIndex() * m_tabWidth + m_slop, 0, 2 * m_tabRoundRadius, 2 * m_tabRoundRadius, 90, 90);
			path.lineTo(m_tabRightOffset + currentIndex() * m_tabWidth - m_slop, m_tabHeight);
			path.closeSubpath();
			linearGradient.setColorAt(0.96, QColor(xColor, xColor, xColor));
			painter.setBrush(linearGradient); 
			painter.drawPath(path);
			painter.setPen(QColor(xColor, xColor, xColor));
			painter.drawLine(m_tabRightOffset + currentIndex() * m_tabWidth - m_slop, m_tabHeight, m_tabRightOffset + (currentIndex() + 1) * m_tabWidth + m_slop, m_tabHeight);

			/* Paint the shadow of the current Tab */
			QPainterPath tabShadowPath;
			int shadowLength = 4;
			int roundRadius = 7;
			qreal ratio = 0.08;
			tabShadowPath.moveTo(m_tabRightOffset + (currentIndex() + 1) * m_tabWidth + m_slop, m_tabHeight);
			tabShadowPath.lineTo(m_tabRightOffset + (currentIndex() + 1) * m_tabWidth + m_slop + shadowLength, m_tabHeight);
			tabShadowPath.lineTo(m_tabRightOffset + (currentIndex() + 1) * m_tabWidth - m_slop + shadowLength + (2 * m_slop) * ratio, m_tabHeight * ratio + roundRadius);
			tabShadowPath.arcTo(m_tabRightOffset + (currentIndex() + 1) * m_tabWidth - m_slop + shadowLength + (2 * m_slop) * ratio - 2 * roundRadius, m_tabHeight * ratio, 2 * roundRadius, 2 * m_roundRadius, 20, 70); 
//			tabShadowPath.lineTo(m_tabRightOffset + (currentIndex() + 1) * m_tabWidth - m_slop + (2 * m_slop) * ratio, m_tabHeight * ratio);
			tabShadowPath.lineTo(m_tabRightOffset + (currentIndex() + 1) * m_tabWidth - m_slop + (2 * m_slop) * ratio - shadowLength + 1, m_tabHeight * ratio);
			path.closeSubpath();
			painter.setBrush(QColor(0, 0, 0, 180));
			painter.fillPath(tabShadowPath, QBrush(QColor(0, 0, 0, 140)));
		}
	}

	{   //Draw the shadow
		QPainterPath pathShadow;
		pathShadow.moveTo(width() - m_frameSize, m_roundRadius - 4);
		pathShadow.lineTo(width() - m_frameSize, height() - m_frameSize - m_roundRadius);
		pathShadow.arcTo(width() - m_frameSize - 2 * m_roundRadius, height() - m_frameSize - 2 * m_roundRadius, 2 * m_roundRadius, 2 * m_roundRadius, 0, -90);
		pathShadow.lineTo(m_roundRadius, height() - m_frameSize);
		pathShadow.arcTo(m_roundRadius - m_frameSize, height() - 2 * m_roundRadius, 2 * m_roundRadius, 2 * m_roundRadius, 190, 80);
		pathShadow.lineTo(width() - m_roundRadius, height());
		pathShadow.arcTo(width() - 2 * (m_roundRadius + 2), height() - 2 * (m_roundRadius + 2), 2 * (m_roundRadius + 2), 2 * (m_roundRadius + 2), 270, 90);
		pathShadow.lineTo(width(), m_roundRadius + m_frameSize - 4);
		pathShadow.arcTo(width() - 2 * m_roundRadius, m_frameSize - 4, 2 * m_roundRadius, 2 * m_roundRadius, 0, 100);
		pathShadow.closeSubpath();
		painter.fillPath(pathShadow, QColor(0, 0, 0, 180));
	}
}

#endif



