#include <OutputChannel.h>
#include <math.h>
#include <QDebug>
#include "plotBiquad.h"
#include "commonLib.h"
#include "plotXover.h"
#include "plotfir.h"

qreal cot(qreal param)
{
	return (1 / tanf(param));
}

#if 1
/* calc curve for PEQ: Peaking / Low-Shelf / High Shelf */
void curve_calculation(int eq_type, qreal eq_gain, qreal eq_fc, qreal q_or_slope, int horizontal_number, qreal *horizontal, qreal *vertical)
{
	g_plotBiquad.setParameter(true, eq_type, eq_fc, eq_gain, q_or_slope);
	g_plotBiquad.plotCurve(horizontal, vertical, horizontal_number);
}

/* calc curve for IIR: HPF */
void calc_IIR_HPF(PF_IIR_Type slopeType, double freq, int pointNum, qreal *pHorizontal, qreal *pVertical)
{	
	plotXover testXoverHPF;
	testXoverHPF.setHPFParameter(true, int(slopeType), freq);
	testXoverHPF.plotCurve(pHorizontal, pVertical, pointNum);
}

/* calc curve for IIR: LPF */
void calc_IIR_LPF(PF_IIR_Type slopeType, double freq, int pointNum, qreal *pHorizontal, qreal *pVertical)
{
	plotXover testXoverLPF;
	testXoverLPF.setLPFParameter(true, int(slopeType), freq);
	testXoverLPF.plotCurve(pHorizontal, pVertical, pointNum);
}

/* calc curve for FIR: HPF */
void calc_FIR_HPF(int tab, double freq, int pointNum, qreal * pHorizontal, qreal * pVertical)
{
	plotFir Fir;
	Fir.setFirParameter(true, tab, HIGHPASS, freq, 0.1);
	Fir.plotCurve(pHorizontal, pVertical, pointNum);
}

/* calc curve for FIR: HPF */
void calc_FIR_LPF(int tab, double freq, int pointNum, qreal * pHorizontal, qreal * pVertical)
{
	plotFir Fir;
	Fir.setFirParameter(true, tab, LOWPASS, freq, 0.1);
	Fir.plotCurve(pHorizontal, pVertical, pointNum);
}

void calc_user_defined_FIR(QString fir, int pointNum, qreal * pHorizontal, qreal * pVertical)
{
	double coef[384];

	fir.replace(',', ' ');
	fir.replace("\r", " "); 
	fir.replace("\n", " "); 
	fir.replace(",", " "); 
	fir.replace("\t", " "); 
	QStringList firList = fir.split(QRegExp(" "), QString::SkipEmptyParts);

	int tab = firList.length();
	if (tab > 384)
	{
		qDebug() << "Invalid tab number: " << tab;
		tab = 384;
	}

	for (int i = 0; i < tab; i++)
	{
		coef[i] = firList.at(i).toDouble();
	}

    plotFir testFir;
	//arbitrary FR plot 
	testFir.plotCurve(coef, tab, pHorizontal,pVertical,pointNum);
}


/* calc curve for FIR HPF / LPF */

#else
void curve_calculation(int eq_type, float eq_gain, float eq_fc, float eq_q, int horizontal_number, qreal *horizontal, qreal *vertical)
{
	int i;
	qreal	fs=48000.0;
	qreal	pi=3.1415926535897932384626433832795;
	qreal	omega;
	qreal	denominator=1.0;
	qreal	v0,K;
	qreal	x1,y1,x2,y2;

	K=tan(pi*eq_fc/fs);
	if (eq_type==EQ_PEAKING)	//	for EQ_PEAKING
	{
		for(i=0;i<horizontal_number;i++)
		{
			vertical[i]=0;
		}
		if (eq_gain>0.0)
		{
			v0=pow(10,eq_gain/20); 
			denominator=1.0/(1+K/eq_q+K*K); 
			for(i=0;i<horizontal_number;i++)
			{
				omega=2*horizontal[i]*3.1415926535897932384626433832795/fs;
				y1=(1+v0*K/eq_q+K*K)*denominator+2*(K*K-1)*denominator*cos(omega)+(1-v0*K/eq_q+K*K)*denominator*cos(2*omega);
				y2=2*(K*K-1)*denominator*sin(omega)+(1-v0*K/eq_q+K*K)*denominator*sin(2*omega);
				x1=1+2*(K*K-1)*denominator*cos(omega)+(1-K/eq_q+K*K)*denominator*cos(2*omega);
				x2=2*(K*K-1)*denominator*sin(omega)+(1-K/eq_q+K*K)*denominator*sin(2*omega);
				vertical[i]=vertical[i]+10*log10((y1*y1+y2*y2)/(x1*x1+x2*x2));
			}
		}
		else
		{
			v0=pow(10,-eq_gain/20); 
			denominator=1.0/(1+v0*K/eq_q+K*K); 
			for(i=0;i<horizontal_number;i++)
			{
				omega=2*horizontal[i]*3.1415926535897932384626433832795/fs;
				y1=(1+K/eq_q+K*K)*denominator+2*(K*K-1)*denominator*cos(omega)+(1-K/eq_q+K*K)*denominator*cos(2*omega);
				y2=2*(K*K-1)*denominator*sin(omega)+(1-K/eq_q+K*K)*denominator*sin(2*omega);
				x1=1+2*(K*K-1)*denominator*cos(omega)+(1-v0*K/eq_q+K*K)*denominator*cos(2*omega);
				x2=2*(K*K-1)*denominator*sin(omega)+(1-v0*K/eq_q+K*K)*denominator*sin(2*omega);
				vertical[i]=vertical[i]+10*log10((y1*y1+y2*y2)/(x1*x1+x2*x2));
			}
		}

		
	}
	else if (eq_type==EQ_L_SHELF)	// for EQ_L_SHELF
	{
		for(i=0;i<horizontal_number;i++)
		{
			vertical[i]=0;
		}
		if (eq_gain>0.0)
		{
			v0=pow(10,eq_gain/20);
			denominator=1+1.4142135623730950488016887242097*K+K*K;
			for(i=0;i<horizontal_number;i++)
			{
				omega=2*horizontal[i]*3.1415926535897932384626433832795/fs;
				y1=(1+sqrt(2*v0)*K+v0*K*K)/denominator+2*(v0*K*K-1)/denominator*cos(omega)+(1-sqrt(2*v0)*K+v0*K*K)/denominator*cos(2*omega);
				y2=2*(v0*K*K-1)/denominator*sin(omega)+(1-sqrt(2*v0)*K+v0*K*K)/denominator*sin(2*omega);
				x1=1+2*(K*K-1)/denominator*cos(omega)+(1-1.4142135623730950488016887242097*K+K*K)/denominator*cos(2*omega);
				x2=2*(K*K-1)/denominator*sin(omega)+(1-1.4142135623730950488016887242097*K+K*K)/denominator*sin(2*omega);
				vertical[i]=vertical[i]+10*log10((y1*y1+y2*y2)/(x1*x1+x2*x2));
			}
		}
		else
		{
			v0=pow(10,-eq_gain/20);
			denominator=1.0/(1+sqrt(2*v0)*K+v0*K*K);
			for(i=0;i<horizontal_number;i++)
			{
				omega=2*horizontal[i]*3.1415926535897932384626433832795/fs;
				y1=(1+1.4142135623730950488016887242097*K+K*K)*denominator+2*(K*K-1)*denominator*cos(omega)+(1-1.4142135623730950488016887242097*K+K*K)*denominator*cos(2*omega);
				y2=2*(K*K-1)*denominator*sin(omega)+(1-1.4142135623730950488016887242097*K+K*K)*denominator*sin(2*omega);
				x1=1+2*(v0*K*K-1)*denominator*cos(omega)+(1-sqrt(2*v0)*K+v0*K*K)*denominator*cos(2*omega);
				x2=2*(v0*K*K-1)*denominator*sin(omega)+(1-sqrt(2*v0)*K+v0*K*K)*denominator*sin(2*omega);
				vertical[i]=vertical[i]+10*log10((y1*y1+y2*y2)/(x1*x1+x2*x2));
			}
		}
	}
	else if (eq_type==EQ_H_SHELF)	// for EQ_H_SHELF
	{
		for(i=0;i<horizontal_number;i++)
		{
			vertical[i]=0;
		}
		if (eq_gain>0.0)
		{
			v0=pow(10,eq_gain/20);
			denominator=1.0/(1+1.4142135623730950488016887242097*K+K*K);
			for(i=0;i<horizontal_number;i++)
			{
				omega=2*horizontal[i]*3.1415926535897932384626433832795/fs;
				y1=(v0+sqrt(2*v0)*K+K*K)*denominator+2*(K*K-v0)*denominator*cos(omega)+(v0-sqrt(2*v0)*K+K*K)*denominator*cos(2*omega);
				y2=2*(K*K-v0)*denominator*sin(omega)+(v0-sqrt(2*v0)*K+K*K)*denominator*sin(2*omega);
				x1=1+2*(K*K-1)*denominator*cos(omega)+(1-1.4142135623730950488016887242097*K+K*K)*denominator*cos(2*omega);
				x2=2*(K*K-1)*denominator*sin(omega)+(1-1.4142135623730950488016887242097*K+K*K)*denominator*sin(2*omega);
				vertical[i]=vertical[i]+10*log10((y1*y1+y2*y2)/(x1*x1+x2*x2));
			}
		}
		else
		{
			v0=pow(10,-eq_gain/20);
			denominator=v0+sqrt(2*v0)*K+K*K;
			for(i=0;i<horizontal_number;i++)
			{
				omega=2*horizontal[i]*3.1415926535897932384626433832795/fs;
				y1=(1+1.4142135623730950488016887242097*K+K*K)/denominator+2*(K*K-1)/denominator*cos(omega)+(1-1.4142135623730950488016887242097*K+K*K)/denominator*cos(2*omega);
				y2=2*(K*K-1)/denominator*sin(omega)+(1-1.4142135623730950488016887242097*K+K*K)/denominator*sin(2*omega);
				x1=1+2*(K*K/v0-1)/(1+sqrt(2/v0)*K+K*K/v0)*cos(omega)+(1-sqrt(2/v0)*K+K*K/v0)/(1+sqrt(2/v0)*K+K*K/v0)*cos(2*omega);
				x2=2*(K*K/v0-1)/(1+sqrt(2/v0)*K+K*K/v0)*sin(omega)+(1-sqrt(2/v0)*K+K*K/v0)/(1+sqrt(2/v0)*K+K*K/v0)*sin(2*omega);
				vertical[i]=vertical[i]+10*log10((y1*y1+y2*y2)/(x1*x1+x2*x2));
			}
		}
	}
#if 0
	else if (eq_type==EQ_LPF)	// for EQ_LPF
	{
		for(i=0;i<horizontal_number;i++)
		{
			vertical[i]=0;
		}
		if (eq_fc < MAX_EQ_FREQUENCY)
		{
			for(i=0;i<horizontal_number;i++)
			{
				omega=2*horizontal[i]*3.1415926535897932384626433832795/fs;
				beta=0.5*(1-sin(pi/4)*sin(2*pi*eq_fc/fs))/(1+sin(pi/4)*sin(2*pi*eq_fc/fs));
				gama=(0.5+beta)*cos(2*pi*eq_fc/fs);
				y1=(0.5+beta-gama)*0.5+(0.5+beta-gama)*cos(omega)+(0.5+beta-gama)*0.5*cos(2*omega);
				y2=(0.5+beta-gama)*sin(omega)+(0.5+beta-gama)*0.5*sin(2*omega);
				x1=1-2*gama*cos(omega)+2*beta*cos(2*omega);
				x2=-2*gama*sin(omega)+2*beta*sin(2*omega);
				vertical[i]=vertical[i]+10*log10((y1*y1+y2*y2)/(x1*x1+x2*x2));
			}
		}
	}
	else if (eq_type==EQ_HPF)	// for EQ_HPF
	{
		for(i=0;i<horizontal_number;i++)
		{
			vertical[i]=0;
		}
		if (eq_fc > MIN_EQ_FREQUENCY)
		{
			for(i=0;i<horizontal_number;i++)
			{
				omega=2*horizontal[i]*3.1415926535897932384626433832795/fs;
				beta=0.5*(1-sin(pi/4)*sin(2*pi*eq_fc/fs))/(1+sin(pi/4)*sin(2*pi*eq_fc/fs));
				gama=(0.5+beta)*cos(2*pi*eq_fc/fs);
				y1=(0.5+beta+gama)*0.5-(0.5+beta+gama)*cos(omega)+(0.5+beta+gama)*0.5*cos(2*omega);
				y2=-(0.5+beta+gama)*sin(omega)+(0.5+beta+gama)*0.5*sin(2*omega);
				x1=1-2*gama*cos(omega)+2*beta*cos(2*omega);
				x2=-2*gama*sin(omega)+2*beta*sin(2*omega);
				vertical[i]=vertical[i]+10*log10((y1*y1+y2*y2)/(x1*x1+x2*x2));
			}
		}
	}
	else if (eq_type==EQ_HPF_24)	// for HPF_24
	{
		for(i=0;i<horizontal_number;i++)
		{
			vertical[i]=0;
		}
		if (eq_fc > MIN_EQ_FREQUENCY)
		{
			///////////////////
			//for 24 sub highpass filter
			for(i=0;i<horizontal_number;i++)
			{
				omega=2*horizontal[i]*3.1415926535897932384626433832795/fs;
				gama=cot(pi*eq_fc/fs)*cot(pi*eq_fc/fs)/(1-2*cos(2.5*pi/4)*cot(pi*eq_fc/fs)+cot(pi*eq_fc/fs)*cot(pi*eq_fc/fs));
				y1=gama-2*gama*cos(omega)+gama*cos(2*omega);
				y2=-2*gama*sin(omega)+gama*sin(2*omega);
				gama=2*(1-cot(pi*eq_fc/fs)*cot(pi*eq_fc/fs))/(1-2*cos(2.5*pi/4)*cot(pi*eq_fc/fs)+cot(pi*eq_fc/fs)*cot(pi*eq_fc/fs));
				beta=(1+2*cos(2.5*pi/4)*cot(pi*eq_fc/fs)+cot(pi*eq_fc/fs)*cot(pi*eq_fc/fs))/(1-2*cos(2.5*pi/4)*cot(pi*eq_fc/fs)+cot(pi*eq_fc/fs)*cot(pi*eq_fc/fs));
				x1=1+gama*cos(omega)+beta*cos(2*omega);
				x2=gama*sin(omega)+beta*sin(2*omega);
				vertical[i]=vertical[i]+10*log10((y1*y1+y2*y2)/(x1*x1+x2*x2));
				gama=cot(pi*eq_fc/fs)*cot(pi*eq_fc/fs)/(1-2*cos(3.5*pi/4)*cot(pi*eq_fc/fs)+cot(pi*eq_fc/fs)*cot(pi*eq_fc/fs));
				y1=gama-2*gama*cos(omega)+gama*cos(2*omega);
				y2=-2*gama*sin(omega)+gama*sin(2*omega);
				gama=2*(1-cot(pi*eq_fc/fs)*cot(pi*eq_fc/fs))/(1-2*cos(3.5*pi/4)*cot(pi*eq_fc/fs)+cot(pi*eq_fc/fs)*cot(pi*eq_fc/fs));
				beta=(1+2*cos(3.5*pi/4)*cot(pi*eq_fc/fs)+cot(pi*eq_fc/fs)*cot(pi*eq_fc/fs))/(1-2*cos(3.5*pi/4)*cot(pi*eq_fc/fs)+cot(pi*eq_fc/fs)*cot(pi*eq_fc/fs));
				x1=1+gama*cos(omega)+beta*cos(2*omega);
				x2=gama*sin(omega)+beta*sin(2*omega);
				vertical[i]=vertical[i]+10*log10((y1*y1+y2*y2)/(x1*x1+x2*x2));
			}
		}
	}
	else if (eq_type == EQ_LPF_24)
	{
		for(i=0;i<horizontal_number;i++)
		{
			vertical[i]=0;
		}
		if (eq_fc < MAX_EQ_FREQUENCY)
		{
			///////////////////
			//for 24 sub highpass filter
			for(i=0;i<horizontal_number;i++)
			{
				omega=2*horizontal[i]*3.1415926535897932384626433832795/fs;
				gama=tan(pi*eq_fc/fs)*tan(pi*eq_fc/fs)/(1-2*cos(2.5*pi/4)*tan(pi*eq_fc/fs)+tan(pi*eq_fc/fs)*tan(pi*eq_fc/fs));
				y1=gama+2*gama*cos(omega)+gama*cos(2*omega);
				y2=2*gama*sin(omega)+gama*sin(2*omega);
				gama=-2*(1-tan(pi*eq_fc/fs)*tan(pi*eq_fc/fs))/(1-2*cos(2.5*pi/4)*tan(pi*eq_fc/fs)+tan(pi*eq_fc/fs)*tan(pi*eq_fc/fs));
				beta=(1+2*cos(2.5*pi/4)*tan(pi*eq_fc/fs)+tan(pi*eq_fc/fs)*tan(pi*eq_fc/fs))/(1-2*cos(2.5*pi/4)*tan(pi*eq_fc/fs)+tan(pi*eq_fc/fs)*tan(pi*eq_fc/fs));
				x1=1+gama*cos(omega)+beta*cos(2*omega);
				x2=gama*sin(omega)+beta*sin(2*omega);
				vertical[i]=vertical[i]+10*log10((y1*y1+y2*y2)/(x1*x1+x2*x2));
				gama=tan(pi*eq_fc/fs)*tan(pi*eq_fc/fs)/(1-2*cos(3.5*pi/4)*tan(pi*eq_fc/fs)+tan(pi*eq_fc/fs)*tan(pi*eq_fc/fs));
				y1=gama+2*gama*cos(omega)+gama*cos(2*omega);
				y2=2*gama*sin(omega)+gama*sin(2*omega);
				gama=-2*(1-tan(pi*eq_fc/fs)*tan(pi*eq_fc/fs))/(1-2*cos(3.5*pi/4)*tan(pi*eq_fc/fs)+tan(pi*eq_fc/fs)*tan(pi*eq_fc/fs));
				beta=(1+2*cos(3.5*pi/4)*tan(pi*eq_fc/fs)+tan(pi*eq_fc/fs)*tan(pi*eq_fc/fs))/(1-2*cos(3.5*pi/4)*tan(pi*eq_fc/fs)+tan(pi*eq_fc/fs)*tan(pi*eq_fc/fs));
				x1=1+gama*cos(omega)+beta*cos(2*omega);
				x2=gama*sin(omega)+beta*sin(2*omega);
				vertical[i]=vertical[i]+10*log10((y1*y1+y2*y2)/(x1*x1+x2*x2));
			}
		}
	
	}
#endif
}
#endif

EQData::EQData(EQData &curve)
{
	m_type = curve.m_type;
	m_frequency = curve.m_frequency;
	m_Q = curve.m_Q;
	m_slope = curve.m_slope;
	m_gain = curve.m_gain;
	m_PFIIRType = curve.m_PFIIRType;
	m_pfType = curve.m_pfType;
	m_enableFlag = curve.m_enableFlag;
	m_FIRtab = curve.m_FIRtab;
}

EQData::EQData()
{
	m_enableFlag = false;
	m_type = EQ_PEAKING;
	m_gain = 0;
	m_Q = 1;
	m_slope = 3;
	m_frequency = 1000;
	m_PFIIRType = PF_IIR_BS24;
	m_pfType = PF_IIR;
	m_FIRtab = 50;
}

void EQData::dBCalc(int pointNum, qreal *pHorizontal, qreal *pVertical)
{
	if (m_type == EQ_PEAKING)
	{
		curve_calculation(m_type, m_gain, m_frequency, m_Q, pointNum, pHorizontal, pVertical);
	}
	else if ((m_type == EQ_L_SHELF) || (m_type == EQ_H_SHELF))
	{
		curve_calculation(m_type, m_gain, m_frequency, m_slope, pointNum, pHorizontal, pVertical);
	}
	else if ((m_type == EQ_HPF) || (m_type == EQ_LPF))
	{
		if (getPFType() == PF_IIR)
		{
			if (m_type == EQ_HPF)
			{
				calc_IIR_HPF(getPFIIRType(), getFrequency(), pointNum, pHorizontal, pVertical);
			}
			else
			{
				calc_IIR_LPF(getPFIIRType(), getFrequency(), pointNum, pHorizontal, pVertical);
			}
		}
		else if (getPFType() == PF_FIR)
		{
			if (m_type == EQ_HPF)
			{
				calc_FIR_HPF(getFIRTab(), getFrequency(), pointNum, pHorizontal, pVertical);
			}
			else
			{
				calc_FIR_LPF(getFIRTab(), getFrequency(), pointNum, pHorizontal, pVertical);
			}
		}
	}
	else if (m_type == EQ_USER_DEFINED_FIR)
	{
		calc_user_defined_FIR(getUserDefinedFIR(), pointNum, pHorizontal, pVertical);
	}
}

qreal EQData::dBCalc(qreal freq)
{
	qreal result;

	if (m_type == EQ_PEAKING)
	{
		curve_calculation(m_type, m_gain, m_frequency, m_Q, 1, &freq, &result);
	}
	else if ((m_type == EQ_L_SHELF) || (m_type == EQ_H_SHELF))
	{
		curve_calculation(m_type, m_gain, m_frequency, m_slope, 1, &freq, &result);
	}
	return (result);
}

/*
qreal OutputChannel::getMagForFrequency()
{
	return dBCalc(m_frequency);
}
*/

bool EQData::isInclude(qreal freq, qreal mag)
{

	if ((m_gain >= 0) && (mag <= 0))
	{
		return (false);
	}

	if ((m_gain <= 0) && (mag >= 0))
	{
		return (false);
	}

	qreal maxM = dBCalc(freq);

	if (m_gain > 0) 
	{
		return (maxM >= mag);
	}
	else
	{
		return (maxM <= mag);
	}
}

void EQData::setUserDefinedFIR(QString userDefinedFIR)
{
	m_userDefinedFIR = userDefinedFIR;
}

void EQData::setType(EQType type)
{
	m_type = type;
}

EQType EQData::getType()
{
	return (m_type);
}

void EQData::setFrequency(qreal frequency)
{
	m_frequency = frequency;

	if (((m_type == EQ_HPF) || (m_type == EQ_LPF)) && (m_pfType == PF_FIR))
	{
		if (m_frequency < MIN_FIR_FREQ)
		{
			m_frequency = MIN_FIR_FREQ;
		}
	}
}

void EQData::setFrequency(QString frequencyString)
{
	qreal frequency = readFloat(frequencyString);

	if (frequencyString.right(3) == "kHz")
	{
		frequency *= 1000;
	}

	setFrequency(frequency);
}

qreal EQData::getFreqForQ(int index)
{
	qreal ratio = (sqrt((qreal)(4 * m_Q * m_Q + 1)) - 1 ) / ( 2 * m_Q);
	if (index == 0)
	{
		return (m_frequency * ratio);			
	}
	else
	{
		return (m_frequency / ratio);
	}

}

qreal EQData::getFrequency()
{
	return (m_frequency);
}

void EQData::setQ(qreal Q)
{
	m_Q = Q;
}

qreal EQData::getQ()
{
	return (m_Q);
}

void EQData::setGain(qreal gain)
{
	m_gain = gain;
}

qreal EQData::getGain()
{
	return (m_gain);
}

qreal EQData::calcLeftQ(qreal leftFreq)
{
	return (leftFreq * m_frequency / ((m_frequency * m_frequency) - (leftFreq * leftFreq)));
}

qreal EQData::calcRightQ(qreal rightFreq)
{
	qreal result = rightFreq * m_frequency;
	result /= ((rightFreq * rightFreq) - (m_frequency * m_frequency));

	return (result);
}

void EQData::enable(bool flag)
{
	m_enableFlag = flag;
}

bool EQData::getEnableFlag()
{
	return (m_enableFlag);
}

OutputChannel::OutputChannel() 
{
	m_lShelf.setType(EQ_L_SHELF);
	m_lShelf.setSlope(3);

	m_hShelf.setType(EQ_H_SHELF);
	m_hShelf.setSlope(3);


	for (int i = 0; i < 27; i++)
	{
		m_GEQ[i] = 0;
	}
	m_roomLevelGain = 3.5;
	m_roomLevelMute = false;
}


void OutputChannel::flattenGEQ()
{
	for (int i = 0; i < 27; i++)
	{
		m_GEQ[i] = 0;
	}
}
