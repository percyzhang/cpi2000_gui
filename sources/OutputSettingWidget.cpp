#include "OutputSettingWidget.h"
#include <QStylePainter>
#include "mainwidget.h"
#include <QCoreApplication>
#include "mainApplication.h"

OutputSettingWidget::OutputSettingWidget(QWidget *parent)
    : QWidget(parent)
{
	m_pOutputTabWidget = new OutputTabWidget(this);
	m_pOutputTabWidget->setObjectName(QStringLiteral("m_pOutputTabWidget"));

	m_pRoomLevelWidget = new RoomLevelWidget(this);
	m_pOutputTabWidget->addTab(m_pRoomLevelWidget, QString("Room Level"));

	m_pEQTuningWidget = new EQTuningWidget(this);
	m_pOutputTabWidget->addTab(m_pEQTuningWidget, QString("EQ Tuning"));

	connect(m_pOutputTabWidget, SIGNAL(currentChanged(int)), this, SLOT(onCurrentChanged(int)));
	connect(g_pApp, SIGNAL(refreshRoolLevel()), m_pRoomLevelWidget, SLOT(onRefresh()));
	connect(g_pApp, SIGNAL(refreshEQTuning()), m_pEQTuningWidget, SLOT(onRefresh()));

	connect(g_pApp, SIGNAL(setSignalMode(int)), this, SLOT(onSignalModeChanged(int)));
}

void OutputSettingWidget::onCurrentChanged(int index)
{
	g_pMainWidget->sendResizeEvent();
//	qDebug() << index;
	if (index == 0)
	{
		/* RoomLevel Widget is actived */
		m_pRoomLevelWidget->refresh();
	}
	else
	{
		/* EQ Tuning Widget is actived */
		CPi2000Data *pData = getCPi2000Data();
		RTAData *pRTA = pData->getRTA();

		if (pRTA->getRotate() == true)
		{
			pRTA->setRotate(false);
		}

		int pinkNoiseChannel = pRTA->getEQTuningPinkNoise();
		bool channelFlag[8] = {false, false, false, false, false, false, false, false};
		channelFlag[pinkNoiseChannel] = true;
		if (g_pApp->getConnectStatus() == DEVICE_NETWORK_CONNECTED)
		{
			g_pApp->getDeviceConnection()->setPinkNoise(channelFlag);
		}
		pRTA->setRoomLevelPinkNoise(channelFlag);
		pData->setCurrentOutputChannelIndex(pinkNoiseChannel);

		m_pEQTuningWidget->refresh();
	}
}

void OutputSettingWidget::resizeEvent(QResizeEvent * /*event*/)
{
	int margin = 10;
	m_pOutputTabWidget->setGeometry(margin, margin, width() - 2 * margin, height() - 2 * margin);
//	m_pRoomLevelWidget->setGeometry(0, 10, width() - 3 * margin, height() - 3 * margin);
//	m_pEQTuningWidget->setGeometry(margin, margin, width() - 2 * margin, height() - 2 * margin - 10);
}

void OutputSettingWidget::refresh()
{
	m_pRoomLevelWidget->refresh();
	m_pEQTuningWidget->refresh();
}

void OutputSettingWidget::retranslateUi()
{
	m_pRoomLevelWidget->retranslateUi();
	m_pEQTuningWidget->retranslateUi();

	m_pOutputTabWidget->setTabText(0, tr("Room Level"));
	m_pOutputTabWidget->setTabText(1, tr("EQ Tuning"));
}

void OutputSettingWidget::onSignalModeChanged(int currentMode)
{
	DeviceSocket *pSocket = g_pApp->getDeviceConnection();

	CPi2000Data *pData = getCPi2000Data();
	RTAData *pRTA = pData->getRTA();
	SIGNAL_MODE oldMode = pRTA->getSignalMode();
	pRTA->setSignalMode((SIGNAL_MODE)currentMode);
	int currentOutputIndex = pData->getCurrentOutputChannelIndex();

	bool roomLevelWidgetFlag = false;
	if (getCurrentTabIndex() == 0)
	{
		roomLevelWidgetFlag = true;
	}
	else
	{
		roomLevelWidgetFlag = false;
	}

	if ((oldMode == SIGNAL_OFF) && (currentMode != SIGNAL_OFF))
	{
		bool channelFlag[] = {false, false, false, false, false, false, false, false };
		/* Start RTA Test */
		pRTA->setMeasuredValue(0.0);
		pRTA->setRotate(false);
		if (roomLevelWidgetFlag == true)
		{
//			channelFlag[currentOutputIndex] = true;
			pRTA->setRoomLevelPinkNoise(channelFlag);
		}
		else
		{
			pRTA->setEQTuningPinkNoise(currentOutputIndex);
		}

		if (g_pApp->getConnectStatus() == DEVICE_NETWORK_CONNECTED)
		{
			pSocket->startSignal();

			if (roomLevelWidgetFlag == true)
			{
				pSocket->setPinkNoise(channelFlag);
			}
			else
			{
				channelFlag[currentOutputIndex] = true;
				pSocket->setPinkNoise(channelFlag);			
			}
		}
		g_pMainWidget->startSignalGenerator();
	}
	else if ((oldMode != SIGNAL_OFF) && (currentMode == SIGNAL_OFF))
	{
		/* Stop RTA */
//		if (pData->getInputSource() == INPUT_SOURCE_MIC)
		{
			pData->setInputSource(INPUT_SOURCE_DIGITAL);
			if (g_pApp->getConnectStatus() == DEVICE_NETWORK_CONNECTED)
			{
				g_pApp->getDeviceConnection()->setInputSource(INPUT_SOURCE_DIGITAL);
			}
			g_pMainWidget->refreshInputSource();
		}

		pRTA->setRotate(false);

		bool channelFlag[] = {false, false, false, false, false, false, false, false };
		pRTA->setRoomLevelPinkNoise(channelFlag);
		pRTA->setSPLValue(-60.0);
		pRTA->setMeasuredValue(0.0);
		pRTA->setAddValue(0.0);
		emit g_pApp->setSPLValue();

		if (g_pApp->getConnectStatus() == DEVICE_NETWORK_CONNECTED)
		{
			pSocket->setPinkNoise(channelFlag);
			pSocket->stopSignal();
		}
		g_pMainWidget->stopSignalGenerator();

		float currentValue[30]; 
		for (int i = 0; i < 30; i++)
		{
			currentValue[i] = -90.0;
		}
		pRTA->setRTAValue(currentValue);
		emit g_pApp->setRTAValue();
	}

	if (g_pApp->getConnectStatus() == DEVICE_NETWORK_CONNECTED)
	{
		pSocket->setSignalMode((SIGNAL_MODE)currentMode);
	}


	m_pRoomLevelWidget->refresh();
	m_pEQTuningWidget->refresh();
}