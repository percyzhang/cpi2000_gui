#include "OverviewWidget.h"
#include <QStylePainter>
#include "mainwidget.h"
#include "DeviceSocket.h"
#include "mainApplication.h"
#include <QMessageBox>
#include "PresetsDB.h"
#include <QDir>
#include "WaittingWidget.h"


OverviewWidget::OverviewWidget(QWidget *parent)
    : QWidget(parent)
{
	m_pDeviceImageWidget = new BackgroundWidget(":deviceOverview.png", this);


	m_pMenuButton = new MarkButton("", m_pDeviceImageWidget);
	m_pMenuButton->setObjectName(QStringLiteral("MenuButton"));
//	connect(m_pMenuButton, SIGNAL(clicked()), this, SLOT(onClickMuteButton()));

	m_pMuteButton = new MarkButton("", m_pDeviceImageWidget);
	m_pMuteButton->setObjectName(QStringLiteral("MuteButton"));
	connect(m_pMuteButton, SIGNAL(clicked()), this, SLOT(onClickMuteButton()));
	m_pMuteButton->mark(true);

	m_pMicButton = new MarkButton("", m_pDeviceImageWidget);
	m_pMicButton->setObjectName(QStringLiteral("MicButton"));
	connect(m_pMicButton, SIGNAL(clicked()), this, SLOT(onClickMicButton()));
	m_pMicButton->mark(false);

	m_pMusicButton = new MarkButton("", m_pDeviceImageWidget);
	m_pMusicButton->setObjectName(QStringLiteral("MusicButton"));
	connect(m_pMusicButton, SIGNAL(clicked()), this, SLOT(onClickMusicButton()));
	m_pMusicButton->mark(false);

	m_pDigitalButton = new MarkButton("", m_pDeviceImageWidget);
	m_pDigitalButton->setObjectName(QStringLiteral("DigitalButton"));
	connect(m_pDigitalButton, SIGNAL(clicked()), this, SLOT(onClickDigitalButton()));
	m_pDigitalButton->mark(true);

	m_pAnalogButton = new MarkButton("", m_pDeviceImageWidget);
	m_pAnalogButton->setObjectName(QStringLiteral("AnalogButton"));
	connect(m_pAnalogButton, SIGNAL(clicked()), this, SLOT(onClickAnalogButton()));
	m_pAnalogButton->mark(false);

//	m_pVolumeWidget = new VolumeChartWidget(m_pDeviceImageWidget);
	QStringList outputVolumeNameList;
//	outputVolumeNameList << "L" << "R" << "C" << "Ls" << "Rs" << "LFE" << "Bls" << "Brs";
//	connect(g_pApp, SIGNAL(outputVolumeChanged()), m_pVolumeWidget, SLOT(onOutputVolumeChanged()));

	m_pVolumeLabel = new QLabel("7.3", m_pDeviceImageWidget);
	m_pVolumeLabel->setAlignment(Qt::AlignLeft | Qt::AlignTop);
	m_pVolumeLabel->setObjectName(QStringLiteral("m_pVolumeLabel"));
//	m_pDeviceHallLabel = new QLabel("HALL: VIP_2", m_pDeviceImageWidget);
//	m_pDeviceHallLabel->setObjectName(QStringLiteral("m_pDeviceHallLabel"));

	m_pTheaterLabel = new QLabel(tr("Theater:"), this);
	m_pTheaterLabel->setAlignment(Qt::AlignRight);
	m_pTheaterLabel->setObjectName(QStringLiteral("m_pNormalLabel"));

	m_pHallLabel = new QLabel(tr("Hall:"), this);
	m_pHallLabel->setAlignment(Qt::AlignRight);
	m_pHallLabel->setObjectName(QStringLiteral("m_pNormalLabel"));
	
	m_pReminderLabel = new QLabel(tr("<p>Reminder: The Hall name can only contain the captial (A-Z), digital Number (0 - 9), '-' and '_'.</p>"), this);
	m_pReminderLabel->setWordWrap(true);
	m_pReminderLabel->setAlignment(Qt::AlignLeft);
	m_pReminderLabel->setObjectName(QStringLiteral("m_pNormalLabel"));

	m_pDHCPLabel = new QLabel(tr("DHCP:"), this);
	m_pDHCPLabel->setAlignment(Qt::AlignRight | Qt::AlignVCenter);
	m_pDHCPLabel->setObjectName(QStringLiteral("m_pNormalLabel"));
	
	m_pIPAddress = new QLabel(tr("IP Address:"), this);
	m_pIPAddress->setAlignment(Qt::AlignRight);
	m_pIPAddress->setObjectName(QStringLiteral("m_pNormalLabel"));
	
	m_pIPDot1 = new QLabel(".", this);
	m_pIPDot1->setAlignment(Qt::AlignHCenter | Qt::AlignBottom);
	m_pIPDot1->setObjectName(QStringLiteral("m_pNormalLabel"));

	m_pIPDot2 = new QLabel(".", this);
	m_pIPDot2->setAlignment(Qt::AlignHCenter | Qt::AlignBottom);
	m_pIPDot2->setObjectName(QStringLiteral("m_pNormalLabel"));

	m_pIPDot3 = new QLabel(".", this);
	m_pIPDot3->setAlignment(Qt::AlignHCenter | Qt::AlignBottom);
	m_pIPDot3->setObjectName(QStringLiteral("m_pNormalLabel"));

	m_pSubnetMaskLabel = new QLabel(tr("Subnet Mask:"), this);
	m_pSubnetMaskLabel->setAlignment(Qt::AlignRight);
	m_pSubnetMaskLabel->setObjectName(QStringLiteral("m_pNormalLabel"));

	m_pSubnetMaskDot1 = new QLabel(".", this);
	m_pSubnetMaskDot1->setAlignment(Qt::AlignHCenter | Qt::AlignBottom);
	m_pSubnetMaskDot1->setObjectName(QStringLiteral("m_pNormalLabel"));

	m_pSubnetMaskDot2 = new QLabel(".", this);
	m_pSubnetMaskDot2->setAlignment(Qt::AlignHCenter | Qt::AlignBottom);
	m_pSubnetMaskDot2->setObjectName(QStringLiteral("m_pNormalLabel"));

	m_pSubnetMaskDot3 = new QLabel(".", this);
	m_pSubnetMaskDot3->setAlignment(Qt::AlignHCenter | Qt::AlignBottom);
	m_pSubnetMaskDot3->setObjectName(QStringLiteral("m_pNormalLabel"));

	
	m_pGatewayLabel = new QLabel(tr("Default Gateway:"), this);
	m_pGatewayLabel->setAlignment(Qt::AlignRight);
	m_pGatewayLabel->setObjectName(QStringLiteral("m_pNormalLabel"));
	
	m_pGatewayDot1 = new QLabel(".", this);
	m_pGatewayDot1->setAlignment(Qt::AlignHCenter | Qt::AlignBottom);
	m_pGatewayDot1->setObjectName(QStringLiteral("m_pNormalLabel"));

	m_pGatewayDot2 = new QLabel(".", this);
	m_pGatewayDot2->setAlignment(Qt::AlignHCenter | Qt::AlignBottom);
	m_pGatewayDot2->setObjectName(QStringLiteral("m_pNormalLabel"));

	m_pGatewayDot3 = new QLabel(".", this);
	m_pGatewayDot3->setAlignment(Qt::AlignHCenter | Qt::AlignBottom);
	m_pGatewayDot3->setObjectName(QStringLiteral("m_pNormalLabel"));

	m_pServerLabel = new QLabel(tr("Server:"), this);
	m_pServerLabel->setAlignment(Qt::AlignRight | Qt::AlignVCenter);
	m_pServerLabel->setObjectName(QStringLiteral("m_pNormalLabel"));
	
	m_pProjectorLabel = new QLabel(tr("Projector:"), this);
	m_pProjectorLabel->setAlignment(Qt::AlignRight | Qt::AlignVCenter);
	m_pProjectorLabel->setObjectName(QStringLiteral("m_pNormalLabel"));
	
	m_pCPi2000SNLabel = new QLabel(tr("CPi2000 S/N:"), this);
	m_pCPi2000SNLabel->setAlignment(Qt::AlignRight | Qt::AlignVCenter);
	m_pCPi2000SNLabel->setObjectName(QStringLiteral("m_pNormalLabel"));
	
	m_pCPi2000SNValue = new QLabel(tr("Not Available"), this);
	m_pCPi2000SNValue->setAlignment(Qt::AlignLeft | Qt::AlignVCenter);
	m_pCPi2000SNValue->setObjectName(QStringLiteral("m_pNormalLabel"));

#if 0
	m_pCXM2000SNLabel = new QLabel(tr("CXM2000 S/N:"), this);
	m_pCXM2000SNLabel->setAlignment(Qt::AlignRight | Qt::AlignVCenter);
	m_pCXM2000SNLabel->setObjectName(QStringLiteral("m_pNormalLabel"));

	m_pCXM2000SNValue = new QLabel(tr("Unknown (Not Available)"), this);
	m_pCXM2000SNValue->setAlignment(Qt::AlignLeft | Qt::AlignVCenter);
	m_pCXM2000SNValue->setObjectName(QStringLiteral("m_pNormalLabel"));
#endif
	m_pSpeakerAndAmplifierLabel = new QLabel(tr("Speaker & Amplifier:"), this);
	m_pSpeakerAndAmplifierLabel->setObjectName(QStringLiteral("m_pNormalLabel"));

	m_pTheaterEdit = new QLineEdit(this);
	m_pTheaterEdit->setMaxLength(18);
	connect(m_pTheaterEdit, SIGNAL(textChanged(const QString &)), this, SLOT(onTheaterChanged(const QString &)));

	m_pHallEdit = new QLineEdit(this);
	QRegExp rx("[A-Z0-9_\\-]{1,19}"); 
//	QRegExp rx("[A-Z0-9+_\\.\\-\\\\/]{1,19}"); 
	QRegExpValidator *validator = new QRegExpValidator(rx, this);  
	m_pHallEdit->setValidator(validator);  
	connect(m_pHallEdit, SIGNAL(textChanged(const QString &)), this, SLOT(onHallChanged(const QString &)));

	m_pIPEdit1 = new QLineEdit(this);
	m_pIPEdit2 = new QLineEdit(this);
	m_pIPEdit3 = new QLineEdit(this);
	m_pIPEdit4 = new QLineEdit(this);
	m_pSubnetMaskEdit1 = new QLineEdit(this);
	m_pSubnetMaskEdit2 = new QLineEdit(this);
	m_pSubnetMaskEdit3 = new QLineEdit(this);
	m_pSubnetMaskEdit4 = new QLineEdit(this);
	m_pGatewayEdit1 = new QLineEdit(this);
	m_pGatewayEdit2 = new QLineEdit(this);
	m_pGatewayEdit3 = new QLineEdit(this);
	m_pGatewayEdit4 = new QLineEdit(this);

	m_pIPEdit1->setEnabled(false);
	m_pIPEdit2->setEnabled(false);
	m_pIPEdit3->setEnabled(false);
	m_pIPEdit4->setEnabled(false);

	m_pSubnetMaskEdit1->setEnabled(false);
	m_pSubnetMaskEdit2->setEnabled(false);
	m_pSubnetMaskEdit3->setEnabled(false);
	m_pSubnetMaskEdit4->setEnabled(false);

	m_pGatewayEdit4->setEnabled(false);
	m_pGatewayEdit3->setEnabled(false);
	m_pGatewayEdit2->setEnabled(false);
	m_pGatewayEdit1->setEnabled(false);

	m_pServerEdit = new QLineEdit(this);
	m_pServerEdit->setMaxLength(18);
	connect(m_pServerEdit, SIGNAL(textChanged(const QString &)), this, SLOT(onServerChanged(const QString &)));

	m_pProjectorEdit = new QLineEdit(this);
	m_pProjectorEdit->setMaxLength(18);
	connect(m_pProjectorEdit, SIGNAL(textChanged(const QString &)), this, SLOT(onProjectorChanged(const QString &)));

	m_pDHCPResultLabel = new QLabel(this);
	m_pDHCPResultLabel->setObjectName(QStringLiteral("m_pNormalLabel"));
	m_pDHCPResultLabel->setAlignment(Qt::AlignLeft | Qt::AlignVCenter);

	m_pSpeakerAndAmplifierWidget = new SpeakerAndAmplifierWidget(this);
	connect(m_pSpeakerAndAmplifierWidget, SIGNAL(changeLRSpeaker()), this, SLOT(onLRSpeakerChanged()));
	connect(m_pSpeakerAndAmplifierWidget, SIGNAL(changeSpeakerAndAmplifier()), this, SLOT(onSpeakerAndAmplifierSettingChanged()));


	m_pApplyButton = new QPushButton(tr("Apply"), this);
	m_pApplyButton->setObjectName(QStringLiteral("BigBlueButton"));
	connect(m_pApplyButton, SIGNAL(clicked()), this, SLOT(onClickApplyButton()));

	setDHCPEnable(false);
 	setIPAddress(QHostAddress("192.168.1.110"));
 	setSubMask(QHostAddress("255.255.255.0"));
 	setDefaultGateWay(QHostAddress("192.168.1.253"));

	m_pServerEdit->setText("Server-VIP2");
	m_pProjectorEdit->setText("Project-VIP2");

	refresh();
}

void OverviewWidget::onServerChanged(const QString &)
{
	refreshApplyButton();
}

void OverviewWidget::onProjectorChanged(const QString &)
{
	refreshApplyButton();
}

void OverviewWidget::onTheaterChanged(const QString & theaterName)
{
	if (theaterName.isEmpty())
	{
		m_pTheaterLabel->setObjectName(QStringLiteral("RedFont12"));
		setStyleSheet("#RedFont12 {"
			"font: 12px;"
			"color: #f00020;" 
			"} "
			);			
	}
	else
	{
		m_pTheaterLabel->setObjectName(QStringLiteral("m_pNormalLabel"));
		setStyleSheet("#m_pNormalLabel {"
			"font: 12px;"
			"color: #d0d0d0;" 
			"} "
			);		
	}
	refreshApplyButton();
}

void OverviewWidget::onHallChanged(const QString &HallName)
{
	if (HallName.isEmpty())
	{
		m_pHallLabel->setObjectName(QStringLiteral("RedFont12"));
		setStyleSheet("#RedFont12 {"
			"font: 12px;"
			"color: #f00020;" 
			"} "
			);	
	}
	else
	{
		m_pHallLabel->setObjectName(QStringLiteral("m_pNormalLabel"));
		setStyleSheet("#m_pNormalLabel {"
			"font: 12px;"
			"color: #d0d0d0;" 
			"} "
			);
	}
	refreshApplyButton();
}

void OverviewWidget::refreshApplyButton()
{
//	CPi2000Data *pData = getCPi2000Data();
	QString strLeftName = m_pSpeakerAndAmplifierWidget->getSpeakerName(0);
	QString strRightName = m_pSpeakerAndAmplifierWidget->getSpeakerName(1);
	bool enableFlag = true;		// Status of Apply Button.

	{
		/* L/R Speaker */
		SpeakerData leftSpeakerData, rightSpeakerData;

		if (g_database.getSpeaker(strLeftName, leftSpeakerData) != true)
		{
			enableFlag = false;
			goto END;
		}
		if (g_database.getSpeaker(strRightName, rightSpeakerData) != true)
		{
			enableFlag = false;
			goto END;
		}

		if (leftSpeakerData.m_speakerType != rightSpeakerData.m_speakerType)
		{
			m_pSpeakerAndAmplifierWidget->setLRTypeMatch(leftSpeakerData.m_speakerType, rightSpeakerData.m_speakerType);
			enableFlag = false;
			goto END;
		}
		else
		{
			m_pSpeakerAndAmplifierWidget->setLRTypeMatch(leftSpeakerData.m_speakerType, rightSpeakerData.m_speakerType);
		}
	}

	if (m_pHallEdit->text().isEmpty())
	{
		enableFlag = false;
		goto END;
	}

	if (m_pTheaterEdit->text().isEmpty())
	{
		enableFlag = false;
		goto END;
	}

	if (isAnythingChanged() == false)
	{
		enableFlag = false;
		goto END;
	}
//	if (g_pApp->getConnectStatus() == DEVICE_NETWORK_CONNECTED)

END:
	m_pApplyButton->setEnabled(enableFlag);
}

void OverviewWidget::onLRSpeakerChanged()
{
	refreshApplyButton();
}

void OverviewWidget::onSpeakerAndAmplifierSettingChanged()
{
	refreshApplyButton();
}

void OverviewWidget::refreshSpeaker()
{
	m_pSpeakerAndAmplifierWidget->refresh();
}

void OverviewWidget::setIPAddress(QHostAddress address)
{
	UINT32 addr = address.toIPv4Address();
	m_pIPEdit4->setText(QString::number(addr & 0xff));
	m_pIPEdit3->setText(QString::number((addr >> 8) & 0xff));
	m_pIPEdit2->setText(QString::number((addr >> 16) & 0xff));
	m_pIPEdit1->setText(QString::number((addr >> 24) & 0xff));
}

void OverviewWidget::setSubMask(QHostAddress address)
{
	UINT32 addr = address.toIPv4Address();
	m_pSubnetMaskEdit4->setText(QString::number(addr & 0xff));
	m_pSubnetMaskEdit3->setText(QString::number((addr >> 8) & 0xff));
	m_pSubnetMaskEdit2->setText(QString::number((addr >> 16) & 0xff));
	m_pSubnetMaskEdit1->setText(QString::number((addr >> 24) & 0xff));
}

void OverviewWidget::setDHCPEnable(bool flag)
{
	QString strResult[] = { tr("Disabled"), tr("Enabled") }; 
	m_pDHCPResultLabel->setText(strResult[(int)flag]);
}

void OverviewWidget::refreshNetwork(NETWORK_SETTING networkSetting)
{
	setSubMask(networkSetting.m_networkMask);
	setIPAddress(networkSetting.m_ipAddress);
	setDefaultGateWay(networkSetting.m_ipGateway);
	setDHCPEnable(networkSetting.m_dhcpEnable);
}

void OverviewWidget::setDefaultGateWay(QHostAddress address)
{
	UINT32 addr = address.toIPv4Address();
	m_pGatewayEdit4->setText(QString::number(addr & 0xff));
	m_pGatewayEdit3->setText(QString::number((addr >> 8) & 0xff));
	m_pGatewayEdit2->setText(QString::number((addr >> 16) & 0xff));
	m_pGatewayEdit1->setText(QString::number((addr >> 24) & 0xff));
}

void OverviewWidget::resizeEvent(QResizeEvent * /*event*/)
{
	int topMargin = 10 + m_pDeviceImageWidget->pixelHeight() + (height() - m_pDeviceImageWidget->pixelHeight()) / 25;
	int editTopMargin = topMargin - 4;
//	int imageWidth = width()  * 28 / 30;
//	int imageHeight = 363 * imageWidth / 1895;

	int right = width() / 8 + 50;
	int w = 140;
	int left = right - w;

	int unitHeight = 25;
	int margin = 20;

	int deviceHeighMargin = 0;
	if (height() > 400)
	{
		deviceHeighMargin = ((float)(height() - 400)) / 20; 
	}
	m_pDeviceImageWidget->setGeometry((width() - m_pDeviceImageWidget->pixelWidth()) / 2, 5 + deviceHeighMargin, m_pDeviceImageWidget->pixelWidth(), m_pDeviceImageWidget->pixelHeight()); 
//	m_pVolumeWidget->setGeometry(248, 50, 252, 75);
	m_pVolumeLabel->setGeometry(430, 125, 100, 30);
//	m_pDeviceHallLabel->setGeometry(255, 33, 100, 20);

	int size = 28;
	m_pMenuButton->setGeometry(593, 102, size, size);
	m_pMuteButton->setGeometry(610, 145, size, size);
	m_pAnalogButton->setGeometry(650, 45, size, size);
	m_pMicButton->setGeometry(692, 60, size, size);
	m_pDigitalButton->setGeometry(710, 103, size, size);
	m_pMusicButton->setGeometry(692, 146, size, size);

	int totalHeight = height() - m_pDeviceImageWidget->pixelHeight();

	m_pTheaterLabel->setGeometry(left, topMargin + totalHeight / 24, w, unitHeight); 
	m_pTheaterEdit->setGeometry(left + w + margin, editTopMargin + totalHeight / 24, w, unitHeight); 

	m_pHallLabel->setGeometry(left, topMargin + totalHeight * 3 / 24, w, unitHeight); 
	m_pHallEdit->setGeometry(left + w + margin, editTopMargin + totalHeight * 3/ 24, w, unitHeight); 

	m_pReminderLabel->setGeometry(left + w - 50, topMargin + totalHeight * 3 / 24 + 35, 270, unitHeight * 3); 

	m_pDHCPLabel->setGeometry(left, topMargin + totalHeight / 13 * 4, w, unitHeight); 
	m_pDHCPResultLabel->setGeometry(left + w + margin, topMargin + totalHeight / 13 * 4, w / 2 - 10, unitHeight);

	m_pIPAddress->setGeometry(left, topMargin + totalHeight / 13 * 5, w, unitHeight); 
	m_pIPEdit1->setGeometry(left + w + margin, editTopMargin + totalHeight / 13 * 5, w / 4 - 10, unitHeight); 
	m_pIPEdit2->setGeometry(left + w + margin + w / 4, editTopMargin + totalHeight / 13 * 5, w / 4 - 10, unitHeight); 
	m_pIPEdit3->setGeometry(left + w + margin + w / 2, editTopMargin + totalHeight / 13 * 5, w / 4 - 10, unitHeight); 
	m_pIPEdit4->setGeometry(left + w + margin + w / 4 * 3, editTopMargin + totalHeight / 13 * 5, w / 4 - 10, unitHeight); 
	m_pIPDot1->setGeometry(left + w + margin + w / 4 - 10, editTopMargin + totalHeight / 13 * 5, 8, unitHeight - 6); 
	m_pIPDot2->setGeometry(left + w + margin + w / 2 - 10, editTopMargin + totalHeight / 13 * 5, 8, unitHeight - 6); 
	m_pIPDot3->setGeometry(left + w + margin + w / 4 * 3 - 10, editTopMargin + totalHeight / 13 * 5, 8, unitHeight - 6); 

	m_pSubnetMaskLabel->setGeometry(left, topMargin + totalHeight / 13 * 6, w, unitHeight); 
	m_pSubnetMaskEdit1->setGeometry(left + w + margin, editTopMargin + totalHeight / 13 * 6, w / 4 - 10, unitHeight); 
	m_pSubnetMaskEdit2->setGeometry(left + w + margin + w / 4, editTopMargin + totalHeight / 13 * 6, w / 4 - 10, unitHeight); 
	m_pSubnetMaskEdit3->setGeometry(left + w + margin + w / 2, editTopMargin + totalHeight / 13 * 6, w / 4 - 10, unitHeight); 
	m_pSubnetMaskEdit4->setGeometry(left + w + margin + w / 4 * 3, editTopMargin + totalHeight / 13 * 6, w / 4 - 10, unitHeight); 
	m_pSubnetMaskDot1->setGeometry(left + w + margin + w / 4 - 10, editTopMargin + totalHeight / 13 * 6, 8, unitHeight - 6); 
	m_pSubnetMaskDot2->setGeometry(left + w + margin + w / 2 - 10, editTopMargin + totalHeight / 13 * 6, 8, unitHeight - 6); 
	m_pSubnetMaskDot3->setGeometry(left + w + margin + w / 4 * 3 - 10, editTopMargin + totalHeight / 13 * 6, 8, unitHeight - 6); 

	m_pGatewayLabel->setGeometry(left, topMargin + totalHeight / 13 * 7, w, unitHeight); 
	m_pGatewayEdit1->setGeometry(left + w + margin, editTopMargin + totalHeight / 13 * 7, w / 4 - 10, unitHeight); 
	m_pGatewayEdit2->setGeometry(left + w + margin + w / 4, editTopMargin + totalHeight / 13 * 7, w / 4 - 10, unitHeight); 
	m_pGatewayEdit3->setGeometry(left + w + margin + w / 2, editTopMargin + totalHeight / 13 * 7, w / 4 - 10, unitHeight); 
	m_pGatewayEdit4->setGeometry(left + w + margin + w / 4 * 3, editTopMargin + totalHeight / 13 * 7, w / 4 - 10, unitHeight); 
	m_pGatewayDot1->setGeometry(left + w + margin + w / 4 - 10, editTopMargin + totalHeight / 13 * 7, 8, unitHeight - 6); 
	m_pGatewayDot2->setGeometry(left + w + margin + w / 2 - 10, editTopMargin + totalHeight / 13 * 7, 8, unitHeight - 6); 
	m_pGatewayDot3->setGeometry(left + w + margin + w / 4 * 3 - 10, editTopMargin + totalHeight / 132 * 7, 8, unitHeight - 6); 

	m_pServerLabel->setGeometry(left, topMargin + totalHeight / 13 * 8, w, unitHeight); 
	m_pServerEdit->setGeometry(left + w + margin, topMargin + totalHeight / 13 * 8, w, unitHeight); 

	m_pProjectorLabel->setGeometry(left, topMargin + totalHeight / 13 * 9, w, unitHeight); 
	m_pProjectorEdit->setGeometry(left + w + margin, topMargin + totalHeight / 13 * 9, w, unitHeight); 

	m_pCPi2000SNLabel->setGeometry(left, topMargin + totalHeight / 13 * 10, w, unitHeight);
	m_pCPi2000SNValue->setGeometry(left + w + margin, topMargin + totalHeight / 13 * 10, w, unitHeight); 

//	m_pCXM2000SNLabel->setGeometry(left, topMargin + totalHeight / 12 * 10, w, unitHeight);
//	m_pCXM2000SNValue->setGeometry(left + w + margin, topMargin + totalHeight / 12 * 10, w, unitHeight); 

	right = width() * 4 / 6 + 20;
	left = right - w;

	m_pSpeakerAndAmplifierWidget->setGeometry(width() * 7 / 20 + 20 , topMargin + totalHeight / 24 + 20, width() * 9 / 18 + 100, totalHeight * 6 / 9);
	m_pSpeakerAndAmplifierLabel->setGeometry(width() * 7 / 20 + 20, topMargin + totalHeight / 24, 150, 20);

	int buttonWidth = 160;
	int buttonHeigh = 40;
	m_pApplyButton->setGeometry(width() * 17 / 20 - buttonWidth / 2, topMargin + totalHeight * 16 / 20, buttonWidth, buttonHeigh);
}

void OverviewWidget::onClickApplyButton()
{
	/* Let's validate the setting here... */
	QString theaterName = m_pTheaterEdit->text();
	if (theaterName.isEmpty())
	{
		QMessageBox::warning(this, tr("Invalid Theater Name"), "Theater Name can not be NULL", "OK");
		return;
	}

	QString hallName = m_pHallEdit->text();
	if (hallName.isEmpty())
	{
		QMessageBox::warning(this, tr("Invalid Hall Name"), "Hall Name can not be NULL", "OK");
		return;
	}

	m_pApplyButton->setText(tr("Applying..."));
	QString projectorName = m_pProjectorEdit->text();
	QString serverName = m_pServerEdit->text();

	/* We can save to the data now */
	CPi2000Data *pData = getCPi2000Data();
	pData->m_strTheaterName = theaterName;
	pData->m_strHallName = hallName;
	pData->m_projectorName = projectorName;
	pData->m_serverName = serverName;

	QString oldSpeakerName[8];
	for (int i = 0; i < 8; i++)
	{
		oldSpeakerName[i] = pData->m_speakerName[i];
	}

	pData->m_speakerName[0] = m_pSpeakerAndAmplifierWidget->getSpeakerName(0);
	pData->m_speakerName[1] = m_pSpeakerAndAmplifierWidget->getSpeakerName(1);
	pData->m_speakerName[2] = m_pSpeakerAndAmplifierWidget->getSpeakerName(2);
	pData->m_speakerName[3] = m_pSpeakerAndAmplifierWidget->getSpeakerName(3);
	pData->m_speakerName[4] = m_pSpeakerAndAmplifierWidget->getSpeakerName(4);
	pData->m_speakerName[5] = m_pSpeakerAndAmplifierWidget->getSpeakerName(5);
	pData->m_speakerName[6] = m_pSpeakerAndAmplifierWidget->getSpeakerName(6);
	pData->m_speakerName[7] = m_pSpeakerAndAmplifierWidget->getSpeakerName(7);

	pData->m_amplifierName[0] = m_pSpeakerAndAmplifierWidget->getAmplifierName(0);
	pData->m_amplifierName[1] = m_pSpeakerAndAmplifierWidget->getAmplifierName(1);
	pData->m_amplifierName[2] = m_pSpeakerAndAmplifierWidget->getAmplifierName(2);
	pData->m_amplifierName[3] = m_pSpeakerAndAmplifierWidget->getAmplifierName(3);
	pData->m_amplifierName[4] = m_pSpeakerAndAmplifierWidget->getAmplifierName(4);
	pData->m_amplifierName[5] = m_pSpeakerAndAmplifierWidget->getAmplifierName(5);
	pData->m_amplifierName[6] = m_pSpeakerAndAmplifierWidget->getAmplifierName(6);
	pData->m_amplifierName[7] = m_pSpeakerAndAmplifierWidget->getAmplifierName(7);

	/* Set Load Detection */
	SPEAKER_CHANNEL channel[] = {SPEAKER_CHANNEL_L, SPEAKER_CHANNEL_CENTER, SPEAKER_CHANNEL_LS, SPEAKER_CHANNEL_RS, SPEAKER_CHANNEL_BLS, SPEAKER_CHANNEL_BRS, SPEAKER_CHANNEL_SW};
	/* L/R Speaker */
	SpeakerData speakerData;
	for (int i = 0; i < sizeof(channel) / sizeof(SPEAKER_CHANNEL); i++)
	{
		if (g_database.getSpeaker(pData->m_speakerName[channel[i]], speakerData) == true)
		{
			g_pMainWidget->setLoadDetectionName(channel[i], speakerData.m_speakerType);
		}
	}
	g_pMainWidget->changeLoadDetection();


	m_pApplyButton->setEnabled(false);
	g_waittingWidget.startWaitting(this);

	DeviceSocket *pDeviceSocket = g_pApp->getDeviceConnection();
	if (pDeviceSocket != nullptr)
	{
//		pDeviceSocket->setTheaterName(theaterName);
//		pDeviceSocket->setHallName(hallName);
		pDeviceSocket->setNormalString(NODE_THEATER_NAME,	theaterName);
		pDeviceSocket->setNormalString(NODE_HALL_NAME,		hallName);
		pDeviceSocket->setNormalString(NODE_PROJECTOR_NAME,		projectorName);
		pDeviceSocket->setNormalString(NODE_SERVER_NAME,		serverName);

		QString speakerNameNode[8] = { NODE_SPEAKER1_NAME, NODE_SPEAKER2_NAME, NODE_SPEAKER3_NAME, NODE_SPEAKER4_NAME, 
			NODE_SPEAKER5_NAME,	NODE_SPEAKER6_NAME, NODE_SPEAKER7_NAME, NODE_SPEAKER8_NAME };
		for (int i = 0; i < 8; i++)
		{
			if (oldSpeakerName[i] != pData->m_speakerName[i])
			{
				pDeviceSocket->setNormalString(speakerNameNode[i], pData->m_speakerName[i]);				
			}
		}

		pDeviceSocket->setNormalString(NODE_AMPLIFIER1_NAME, m_pSpeakerAndAmplifierWidget->getAmplifierName(0));
		pDeviceSocket->setNormalString(NODE_AMPLIFIER2_NAME, m_pSpeakerAndAmplifierWidget->getAmplifierName(1));
		pDeviceSocket->setNormalString(NODE_AMPLIFIER3_NAME, m_pSpeakerAndAmplifierWidget->getAmplifierName(2));
		pDeviceSocket->setNormalString(NODE_AMPLIFIER4_NAME, m_pSpeakerAndAmplifierWidget->getAmplifierName(3));
		pDeviceSocket->setNormalString(NODE_AMPLIFIER5_NAME, m_pSpeakerAndAmplifierWidget->getAmplifierName(4));
		pDeviceSocket->setNormalString(NODE_AMPLIFIER6_NAME, m_pSpeakerAndAmplifierWidget->getAmplifierName(5));
		pDeviceSocket->setNormalString(NODE_AMPLIFIER7_NAME, m_pSpeakerAndAmplifierWidget->getAmplifierName(6));
		pDeviceSocket->setNormalString(NODE_AMPLIFIER8_NAME, m_pSpeakerAndAmplifierWidget->getAmplifierName(7));
		pDeviceSocket->getHallName(hallName, 10);
		g_pApp->msleep(2500);
	}

	g_pApp->msleep(500);
	g_waittingWidget.stopWaitting();
//	m_pApplyButton->setEnabled(true);
	m_pApplyButton->setText(tr("Apply"));
}

void OverviewWidget::refreshInputSource()
{
	CPi2000Data *pData = getCPi2000Data();
	INPUT_SOURCE inputSource = pData->getInputSource();

	m_pDigitalButton->mark(inputSource == INPUT_SOURCE_DIGITAL);
	m_pMicButton->mark(inputSource == INPUT_SOURCE_MIC);
	m_pMusicButton->mark(inputSource == INPUT_SOURCE_MUSIC);
	m_pAnalogButton->mark(inputSource == INPUT_SOURCE_ANALOG);
}

void OverviewWidget::refreshMasterVolume()
{
	CPi2000Data *pData = getCPi2000Data();
	float value = pData->getMasterVolume();
	QString volume;
	volume.sprintf("%4.1f", value);
	m_pVolumeLabel->setText(volume);
}

void OverviewWidget::onClickDigitalButton()
{
	CPi2000Data *pData = getCPi2000Data();
	pData->setInputSource(INPUT_SOURCE_DIGITAL);
	if (g_pApp->getConnectStatus() == DEVICE_NETWORK_CONNECTED)
	{
		g_pApp->getDeviceConnection()->setInputSource(INPUT_SOURCE_DIGITAL);
	}
	g_pMainWidget->refreshInputSource();
}

void OverviewWidget::onClickMusicButton()
{
	CPi2000Data *pData = getCPi2000Data();
	pData->setInputSource(INPUT_SOURCE_MUSIC);
	if (g_pApp->getConnectStatus() == DEVICE_NETWORK_CONNECTED)
	{
		g_pApp->getDeviceConnection()->setInputSource(INPUT_SOURCE_MUSIC);
	}
	g_pMainWidget->refreshInputSource();
}

void OverviewWidget::onClickMicButton()
{
	CPi2000Data *pData = getCPi2000Data();
	pData->setInputSource(INPUT_SOURCE_MIC);
	if (g_pApp->getConnectStatus() == DEVICE_NETWORK_CONNECTED)
	{
		g_pApp->getDeviceConnection()->setInputSource(INPUT_SOURCE_MIC);
	}
	g_pMainWidget->refreshInputSource();
}

void OverviewWidget::onClickAnalogButton()
{
	CPi2000Data *pData = getCPi2000Data();
	pData->setInputSource(INPUT_SOURCE_ANALOG);

	if (g_pApp->getConnectStatus() == DEVICE_NETWORK_CONNECTED)
	{
		g_pApp->getDeviceConnection()->setInputSource(INPUT_SOURCE_ANALOG);
	}

	g_pMainWidget->refreshInputSource();

}

void OverviewWidget::onClickMuteButton()
{
	bool flag = !m_pMuteButton->getMarkFlag();

	CPi2000Data *pData = getCPi2000Data();
	pData->setMasterOutputMuteFlag(flag);

	if (g_pApp->getConnectStatus() == DEVICE_NETWORK_CONNECTED)
	{
		g_pApp->getDeviceConnection()->setFadeInOutMuteFlag(flag);
	}

	g_pMainWidget->refreshMasterMuteFlag();
}

void OverviewWidget::refreshMasterMuteFlag()
{
	CPi2000Data *pData = getCPi2000Data();
	bool flag = pData->getMasterOutputMuteFlag();

	m_pMuteButton->mark(flag);
}

bool OverviewWidget::isAnythingChanged()
{
	CPi2000Data *pData = getCPi2000Data();

	if (pData->m_strHallName != m_pHallEdit->text())
	{
		return (true);
	}

	if (pData->m_strTheaterName != m_pTheaterEdit->text())
	{
		return (true);
	}

	if (pData->m_serverName != m_pServerEdit->text())
	{
		return (true);
	}

	if (pData->m_projectorName != m_pProjectorEdit->text())
	{
		return (true);
	}

	for (int i = 0; i < 8; i++)
	{
		if ((m_pSpeakerAndAmplifierWidget->getSpeakerName(i) != pData->m_speakerName[i]) && (pData->m_speakerName[i].isEmpty() == false))
		{
			qDebug() << m_pSpeakerAndAmplifierWidget->getSpeakerName(i) << ":" << pData->m_speakerName[i];
			return (true);
		}
		if (m_pSpeakerAndAmplifierWidget->getAmplifierName(i) != pData->m_amplifierName[i])
		{
			return (true);
		}
	}

	return (false);
}

bool OverviewWidget::isApplyEnabled()
{
	if (m_pApplyButton->isEnabled())
	{
		return (true);
	}
	else
	{
		return (false);
	}
}

void OverviewWidget::refresh()
{
	CPi2000Data *pData = getCPi2000Data();
	NETWORK_SETTING networkSetting = pData->getNetworkAddress();
	refreshNetwork(networkSetting);

	m_pHallEdit->setText(pData->m_strHallName);
	m_pTheaterEdit->setText(pData->m_strTheaterName);

	m_pServerEdit->setText(pData->m_serverName);
	m_pProjectorEdit->setText(pData->m_projectorName);

	m_pCPi2000SNValue->setText(pData->m_strSerialNumber);

	INPUT_SOURCE inputSource = pData->getInputSource();
	m_pMicButton->mark(inputSource == INPUT_SOURCE_MIC);
	m_pMusicButton->mark(inputSource == INPUT_SOURCE_MUSIC);
	m_pAnalogButton->mark(inputSource == INPUT_SOURCE_ANALOG);
	m_pDigitalButton->mark(inputSource == INPUT_SOURCE_DIGITAL);

	m_pMuteButton->mark(pData->getMasterOutputMuteFlag());

	float volume = (float)qRound(pData->getMasterVolume() * 10) / 10;
	QString volumeString;
	volumeString.sprintf("%6.1f", volume);
	m_pVolumeLabel->setText(volumeString);

	m_pSpeakerAndAmplifierWidget->refresh();

//	m_pApplyButton->setEnabled(g_pApp->getConnectStatus() == DEVICE_NETWORK_CONNECTED);
	refreshApplyButton();
}

void OverviewWidget::retranslateUi()
{
	m_pTheaterLabel->setText(tr("Theater:"));
	m_pHallLabel->setText(tr("Hall:"));
	m_pDHCPLabel->setText(tr("DHCP:"));
	m_pIPAddress->setText(tr("IP Address:"));
	m_pSubnetMaskLabel->setText(tr("Subnet Mask:"));
	m_pGatewayLabel->setText(tr("Default Gateway:"));
	m_pServerLabel->setText(tr("Server:"));
	m_pProjectorLabel->setText(tr("Projector:"));
	m_pCPi2000SNLabel->setText(tr("CPi2000 S/N:"));
	m_pSpeakerAndAmplifierLabel->setText(tr("Speaker & Amplifier:"));
	m_pApplyButton->setText(tr("Apply"));

	m_pSpeakerAndAmplifierWidget->retranslateUi();

	CPi2000Data *pData = getCPi2000Data();
	NETWORK_SETTING networkSetting = pData->getNetworkAddress();
	setDHCPEnable(networkSetting.m_dhcpEnable);
	m_pReminderLabel->setText(tr("<p>Reminder: The Hall name can only contain the captial (A-Z), digital Number (0 - 9), '-' and '_'.</p>"));


	refreshApplyButton();
}
