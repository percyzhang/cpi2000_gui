#include "PanelWidget.h"

PanelWidget::PanelWidget(QWidget *parent)	: QWidget(parent)
{
	m_pVolumeWidget = new VolumeWidget(this);
	QStringList xStringList, yStringList;
	xStringList << "L" << "C" << "R" << "Ls" << "Rs" << "Sw" << "Bls" << "Brs";
	yStringList << "-40" << "-20" << "0dB" << "+20";
	m_pVolumeWidget->setXMarkString(xStringList);
	m_pVolumeWidget->setYMarkString(yStringList);
	m_pVolumeWidget->setVolumeStep(-40, 20, 60);

	m_pTimer = new QTimer(this);
    connect(m_pTimer, SIGNAL(timeout()), this, SLOT(onTimer()));
	m_pTimer->start(100);

	m_pGeneralVolume = new ButtonSlider(this);
	m_pGeneralVolume->setTextColor(QColor(0xf0, 0xf0, 0xf0));
	QStringList sliderStringList;
	sliderStringList << "0.0" << "5.0" << "10.0";
	m_pGeneralVolume->setMark(3, 5, sliderStringList);
	m_pGeneralVolume->setRange(0, 100);
	m_pGeneralVolume->setSingleStep(1);
	m_pGeneralVolume->setPageStep(1);
	m_pGeneralVolume->setValue(50);

	m_pMuteButton = new MarkButton(QString(""), this);
    m_pMuteButton->setObjectName(QStringLiteral("PanelMuteButton"));
	connect(m_pMuteButton, SIGNAL(clicked()), this, SLOT(onClickMuteButton()));
	m_pMuteButton->mark(true);

	this->setObjectName(QStringLiteral("PanelWidget"));

//	m_pDolbyWidget = new BackgroundWidget(":dolby.png", this, 0, 0);

	Qt::WindowFlags flags = windowFlags(); 
	setWindowFlags(flags & (~Qt::WindowMaximizeButtonHint& ~Qt::WindowMinimizeButtonHint) | Qt::WindowStaysOnTopHint);

	setWindowTitle("CPi2000 - Hall 1");

	setMinimumSize(840, 220);
}

void PanelWidget::resizeEvent(QResizeEvent * /* event */)
{
	int margin = 10;
//	m_pInputLabel->setGeometry(0, 0, width(), height());

	m_pVolumeWidget->setGeometry(margin, margin, width() / 3, height() - 2 * margin);
	
	int sliderWidth = 52;
	m_pGeneralVolume->setGeometry(width() / 2 - sliderWidth / 2, margin + 20, sliderWidth, height() - 2 * margin - 43);
	int buttonWidth = 40;
	m_pMuteButton->setGeometry(width() / 2 - sliderWidth / 2, height() - 2 * margin - 19, buttonWidth, buttonWidth);

//	m_pDolbyWidget->setGeometry(width() / 3 + margin, 0, width() * 2 / 3 - margin, height());
}

void PanelWidget::onTimer()
{
/*
	int volumeValue[8];

	for (int i = 0; i < 8; i++)
	{
		volumeValue[i] = rand() % 60 - 40; 
	}
	m_pVolumeWidget->setVolume(volumeValue);
	m_pVolumeWidget->refreshValue();
*/
}

void PanelWidget::onClickMuteButton()
{
	bool muteFlag = m_pMuteButton->getMarkFlag();
	m_pMuteButton->mark(!muteFlag);
}