#include "PresetsDB.h"
#include <QSqlQuery>

#include <QDebug>

PresetsDB g_database;

PresetsDB::PresetsDB()
{
/*
      We can't call init() before the main QApplication class, or it will show the following error:
          QSqlDatabase: QSQLITE driver not loaded
          QSqlDatabase: available drivers:
          QSqlDatabase: an instance of QCoreApplication is required for loading driver plugins

      This error only happened at Android or Linux. For Windows, it work normally.

      Please see the following webpage for more information.
            https://forum.qt.io/topic/38486/solved-qt-sql-database-driver-not-loaded-in-android
*/

    //    init();
}

void PresetsDB::init(void)
{
    m_database = QSqlDatabase::addDatabase("QSQLITE");
}

PresetsDB::~PresetsDB(void)
{
}

bool PresetsDB::setJSON(const QString &filed, const QString &content)
{
	bool ret = false;
//	int count = 0;

	m_database.setDatabaseName(m_presetDBFileName);
	if (m_database.open())        
	{
		QSqlQuery query;

		QString script = QString("UPDATE Presets SET Settings='%1' where NameID = '%2'").arg(content).arg(filed);
		ret = query.exec(script);
	}

	m_database.close();
	return (ret);
}

QString PresetsDB::getJSON(QString name)
{
	bool ret = false;
//	int count = 0;
	QString result, nameID;

	qDebug() << m_presetDBFileName;
	m_database.setDatabaseName(m_presetDBFileName);
	if (m_database.open())        
	{	
		QSqlQuery query;
//		ret = query.exec("select Settings from Presets where NameID='0';");
		ret = query.exec("select * from Presets");
		if (ret == true)
		{
			//Let's export the words to XML files.
			while (query.next())
			{
//				qDebug() << "***********************************************";
				nameID = query.value(0).toString();
//				QString temp = query.value(1).toString();
//				temp = query.value(2).toString();
//				qDebug() << temp;
				result = query.value(3).toString();
//				qDebug() << "len = " << result.length() << " : "<< result.mid(0, 1000);
				
				if (nameID == name)
				{
					break;
				}
			}
		}
	}
	else
	{
		qDebug() << "Error: Can't open DBFile: " << m_presetDBFileName;
	}

	m_database.close();

	if (nameID == name)
	{
		return (result);
	}
	else
	{
		return ("");
	}
}

bool PresetsDB::getSpeakerList(QStringList &speakersList)
{
	bool ret = false;

	QStringList tempSpeakerList;
	int speakerCount = 0;

	QStringList bypassList;
	bypassList << "Passive-Bypass" << "Biamp-Bypass" << "Triamp-Bypass" << "Surround-Bypass" << "Subwoofer-Bypass";
	m_database.setDatabaseName(m_strSpeakerDBFileName);
	if (m_database.open())        
	{	
		QSqlQuery query;
		ret = query.exec("select * from Blocks");
		if (ret == true)
		{
			//Let's export the words to XML files.
			while (query.next())
			{
				QString speakerName = query.value(0).toString();
				
				if (bypassList.contains(speakerName))
				{
					continue;
				}
//				QString description = query.value(1).toString();
//				qDebug() << "speakerName :" << speakerName;
//				qDebug() << "description :" << description;
				tempSpeakerList << speakerName;
				speakerCount++;
//				descriptionList << description;
			}
		}
	}
	else
	{
		qDebug() << "Error: Can't open DBFile: " << m_strSpeakerDBFileName;
	}

	speakersList.clear();
	for (int i = 0; i < speakerCount; i++)
	{
		speakersList << tempSpeakerList.at(speakerCount - 1 - i);
	}

	m_database.close();
	return (ret);
}

bool PresetsDB::addSpeaker(QString speakerName, QString speakerDescription)
{
	bool ret = false;

	m_database.setDatabaseName(m_strSpeakerDBFileName);
	if (m_database.open())        
	{
		QSqlQuery query;

		speakerName.replace("'", "''");
		speakerDescription.replace("'", "''");
		QString script = QString("insert into Blocks values('%1', '%2')").arg(speakerName).arg(speakerDescription);
//		qDebug() << "Execute: " << script;
		ret = query.exec(script);
	}

	m_database.close();
	return (ret);
}

bool PresetsDB::addSpeaker(QString speakerName, SpeakerData *pSpeakerData)
{
	QString description = pSpeakerData->writeToJSON();

	bool flag = addSpeaker(speakerName, description);
	return (flag);
}

bool PresetsDB::deleteSpeaker(QString speakerName)
{
	bool ret = false;

	m_database.setDatabaseName(m_strSpeakerDBFileName);
	if (m_database.open())        
	{
		QSqlQuery query;

		QString script = QString("delete from Blocks where Name = '%1'").arg(speakerName);
//		qDebug() << "Execute: " << script;
		ret = query.exec(script);
	}

	m_database.close();
	return (ret);
}

bool PresetsDB::getSpeaker(QString speakerName, SpeakerData &speakerData)
{
	QString description;
	bool flag = getSpeaker(speakerName, description);
	if (flag == false)
	{
		return (false);
	}
	
	flag = speakerData.readFromJSON(description);
	return (flag);
}

bool PresetsDB::getSpeaker(SPEAKER_CHANNEL speakerChannel, SpeakerData &speakerData)
{
	CPi2000Data *pData = getCPi2000Data();
	QString speakerName = pData->m_speakerName[speakerChannel];

	bool ret = getSpeaker(speakerName, speakerData);
	return (ret);
}

bool PresetsDB::getSpeaker(QString speakerName, QString &description)
{
	bool ret = false;

	m_database.setDatabaseName(m_strSpeakerDBFileName);
	if (m_database.open())        
	{
		QSqlQuery query;

		QString script = QString("select * from Blocks where Name = '%1'").arg(speakerName);
//		qDebug() << "Execute: " << script;
		ret = query.exec(script);
		if (ret == true)
		{
			//Let's export the words to XML files.
			while (query.next())
			{
				description = query.value(1).toString();
				break;
			}
		}

	}

	m_database.close();
	return (ret);
}

bool PresetsDB::modifySpeaker(QString speakerName, QString description)
{
	bool ret = false;

	m_database.setDatabaseName(m_strSpeakerDBFileName);
	if (m_database.open())        
	{
		QSqlQuery query;

		QString script = QString("UPDATE Blocks SET Data='%1' where Name = '%2'").arg(description).arg(speakerName);
//		qDebug() << "Execute: " << script;
		ret = query.exec(script);
	}

	m_database.close();
	return (ret);
}

bool PresetsDB::modifySpeaker(QString speakerName, SpeakerData *pSpeakerData)
{
	QString description = pSpeakerData->writeToJSON();

	bool flag = modifySpeaker(speakerName, description);
	return (flag);
}