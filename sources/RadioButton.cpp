#include "RadioButton.h"
#include <QPainter>


RadioButton::RadioButton(QWidget *pParent)
	: QRadioButton(pParent)
{
	setObjectName(QStringLiteral("RadioButton"));
}

RadioButton::RadioButton(const QString &text, QWidget *parent)
	: QRadioButton(text, parent)
{
	setObjectName(QStringLiteral("RadioButton"));
}


void RadioButton::paintEvent(QPaintEvent *event)
{
	QRadioButton::paintEvent(event);
	QPainter painter(this);
	painter.setPen(QPen(QColor(0x70, 0x70, 0x70), 1));
	painter.drawArc(4, 6, 12, 12, 0, 360 * 16);
	painter.setPen(QPen(QColor(0xd0, 0xd0, 0xd0), 1));
	painter.drawArc(3, 5, 14, 14, 0, 360 * 16);
}
