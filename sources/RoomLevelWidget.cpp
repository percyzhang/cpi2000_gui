#include "RoomLevelWidget.h"
#include <QPainter>
#include "CPi2000Setting.h"
#include "mainApplication.h"
#include "mainWidget.h"
#include "qt_windows.h"

RoomLevelWidget::RoomLevelWidget(QWidget *parent)
    : QWidget(parent)
{
	m_pSignalModeGroup = new QGroupBox(tr(""), this);
	m_pSignalModeGroup->setObjectName(QStringLiteral("SpeakerGroupBox"));
	{
		m_pSignalModeCombo = new QComboBox(m_pSignalModeGroup);

		QString signal[] = {"OFF", "100 Hz", "1 kHz", "10 kHz",  "Pink Noise" };
		/*
				set to 100Hz:
				set "\\Preset\SignalGenerator\SV\SineFrequency"   "100Hz"
				set "\\Preset\SignalGenerator\SV\SignalType"   "Sine"

				set to 1kHz:
				set "\\Preset\SignalGenerator\SV\SineFrequency"   "1000Hz"
				set "\\Preset\SignalGenerator\SV\SignalType"   "Sine"

				set to Pink:
				set "\\Preset\SignalGenerator\SV\SignalType"   "Noise"

				set to Off:
				set "\\Preset\SignalGenerator\SV\SignalType"   "Off"
		*/
		for (int i = 0; i < 5; i++)
		{
			m_pSignalModeCombo->addItem(signal[i]);
		}
		connect(m_pSignalModeCombo, SIGNAL(currentIndexChanged(int)), this, SLOT(onSignalModeSelected(int)));
//		connect(g_pApp, SIGNAL(setSignalMode(int)), this, SLOT(onSignalModeSelected(int)));

		m_pReminderLabel = new QLabel(tr("To stop RTA setting, you need to set signal mode to 'OFF'."),  m_pSignalModeGroup);
		m_pReminderLabel->setWordWrap(true);
		m_pReminderLabel->setObjectName("BrownFont12");

		m_pSignalLabel = new QLabel(tr("Signal Mode"), m_pSignalModeGroup);
		m_pSignalLabel->setObjectName("BlackFont12");
		m_pSignalLabel->setAlignment(Qt::AlignLeft | Qt::AlignTop);
	}

	m_pCalibrationGroup = new QGroupBox(tr("Internal SPL Meter Calibration"), this);
	m_pCalibrationGroup->setObjectName(QStringLiteral("ActiveChannel"));
	{
		m_pCalWidget = new CalWidget(m_pCalibrationGroup);
//		m_pCalWidget = new ValueWidget(m_pCalibrationGroup);

		float minDB = -46;
		float maxDB = -3;
		float leftDB = -32.5;
		float rightDB = -15.5;
		m_pCalWidget->setScale(minDB, 0, maxDB, 100);
		m_pCalWidget->setFrontColor(QColor(237, 21, 26), QColor(42, 221, 60), QColor(237, 21, 26), leftDB, rightDB);
		m_pCalWidget->setValue(-12);

		m_pMeterLabel = new QLabel(tr("Internal SPL Meter Calibration"), m_pCalibrationGroup);
		m_pMeterLabel->setObjectName(QStringLiteral("WhiteFont12"));
			
		m_pValueLabel = new QLabel(tr("Measured Value"), m_pCalibrationGroup); 	
		m_pValueLabel->setObjectName(QStringLiteral("WhiteFont12"));
		m_pValueLabel->setAlignment(Qt::AlignRight);

		m_pCalSpin = new QDoubleSpinBox(m_pCalibrationGroup);
		m_pCalSpin->setRange(0.0, 100.0);
		m_pCalSpin->setSingleStep(0.1);
		m_pCalSpin->setDecimals(1);
		m_pCalSpin->setValue(0.0);
		m_pCalSpin->setSuffix(" dB");
		connect(m_pCalSpin, SIGNAL(valueChanged(double)), this, SLOT(onClacChanged(double)));

		/* 
		Get the CalValue by the following command:
			asyncget "\\Preset\SplMeters\SV\SplValue"
		*/
#if 0
		m_pCalValue = new QLabel(tr("0.0 dB"), m_pCalibrationGroup);
		m_pCalValue->setObjectName(QStringLiteral("BlackFont18"));
		m_pCalValue->setAlignment(Qt::AlignHCenter);	
#else
		m_pLineLabel = new QLabel(tr("Target Mic Level"), m_pCalibrationGroup);
		m_pLineLabel->setObjectName("BlackFont12");
		m_pLineLabel->setAlignment(Qt::AlignCenter);


#endif
		connect(g_pApp, SIGNAL(setSPLValue()), this, SLOT(onSPLChanged()));

		m_pMeterValue = new QLabel(tr("0.0 dB"), m_pCalibrationGroup);
		m_pMeterValue->setObjectName(QStringLiteral("WhiteFont18"));
		m_pMeterValue->setAlignment(Qt::AlignHCenter | Qt::AlignBottom);	
		m_pCalLabel = new QLabel(tr("uncal"), m_pCalibrationGroup);
		m_pCalLabel->setObjectName(QStringLiteral("WhiteFont12"));
		m_pCalLabel->setAlignment(Qt::AlignHCenter | Qt::AlignBottom);	
	}
	
	m_pOutputChannelGroup = new QGroupBox(tr(""), this);
	m_pOutputChannelGroup->setObjectName(QStringLiteral("SpeakerGroupBox"));

	m_pOutputLabel = new QLabel(tr("Output Channel Level"), m_pOutputChannelGroup);
	m_pOutputLabel->setObjectName(QStringLiteral("BlackFont18"));
	m_pOutputLabel->setAlignment(Qt::AlignCenter);

	QStringList pinkNoiseStringList;
	pinkNoiseStringList << tr("Left") << tr("Right") << tr("Center") << tr("Subwoofer") << tr("Left Surround") << tr("Right Surround") << tr("Back Left Surround") << tr("Back Right Surround");

	for (int i = 0; i < 8; i++)
	{
		m_pPinkNoiseButton[i] = new MarkButton(pinkNoiseStringList.at(i), m_pOutputChannelGroup);
		m_pPinkNoiseButton[i]->setObjectName(QStringLiteral("pinkNoise"));

		m_pSpeakerName[i] = new QLabel("JBL 3252", m_pOutputChannelGroup);
		m_pSpeakerName[i]->setAlignment(Qt::AlignCenter);
		m_pSpeakerName[i]->setObjectName(QStringLiteral("BalckFont14"));

		m_pAmpName[i] = new QLabel("Crown XLC1500", m_pOutputChannelGroup);
		m_pAmpName[i]->setAlignment(Qt::AlignCenter);
		m_pAmpName[i]->setObjectName(QStringLiteral("BalckFont14"));
	}

	m_pVolumeGroup[0] = new VolumeGroup(m_pOutputChannelGroup);
	m_pVolumeGroup[1] = new VolumeGroup(m_pOutputChannelGroup);
	m_pVolumeGroup[2] = new VolumeGroup(m_pOutputChannelGroup);
	m_pVolumeGroup[3] = new VolumeGroup(m_pOutputChannelGroup);
	m_pVolumeGroup[4] = new VolumeGroup(m_pOutputChannelGroup);
	m_pVolumeGroup[5] = new VolumeGroup(m_pOutputChannelGroup);
	m_pVolumeGroup[6] = new VolumeGroup(m_pOutputChannelGroup);
	m_pVolumeGroup[7] = new VolumeGroup(m_pOutputChannelGroup);


	connect(m_pVolumeGroup[0], SIGNAL(toggleMuteButton(bool)), this, SLOT(toggleMuteButton_0(bool)));
	connect(m_pVolumeGroup[1], SIGNAL(toggleMuteButton(bool)), this, SLOT(toggleMuteButton_1(bool)));
	connect(m_pVolumeGroup[2], SIGNAL(toggleMuteButton(bool)), this, SLOT(toggleMuteButton_2(bool)));
	connect(m_pVolumeGroup[3], SIGNAL(toggleMuteButton(bool)), this, SLOT(toggleMuteButton_3(bool)));
	connect(m_pVolumeGroup[4], SIGNAL(toggleMuteButton(bool)), this, SLOT(toggleMuteButton_4(bool)));
	connect(m_pVolumeGroup[5], SIGNAL(toggleMuteButton(bool)), this, SLOT(toggleMuteButton_5(bool)));
	connect(m_pVolumeGroup[6], SIGNAL(toggleMuteButton(bool)), this, SLOT(toggleMuteButton_6(bool)));
	connect(m_pVolumeGroup[7], SIGNAL(toggleMuteButton(bool)), this, SLOT(toggleMuteButton_7(bool)));

	connect(m_pVolumeGroup[0], SIGNAL(changeVolume(float)), this, SLOT(onChangeVolume_0(float)));
	connect(m_pVolumeGroup[1], SIGNAL(changeVolume(float)), this, SLOT(onChangeVolume_1(float)));
	connect(m_pVolumeGroup[2], SIGNAL(changeVolume(float)), this, SLOT(onChangeVolume_2(float)));
	connect(m_pVolumeGroup[3], SIGNAL(changeVolume(float)), this, SLOT(onChangeVolume_3(float)));
	connect(m_pVolumeGroup[4], SIGNAL(changeVolume(float)), this, SLOT(onChangeVolume_4(float)));
	connect(m_pVolumeGroup[5], SIGNAL(changeVolume(float)), this, SLOT(onChangeVolume_5(float)));
	connect(m_pVolumeGroup[6], SIGNAL(changeVolume(float)), this, SLOT(onChangeVolume_6(float)));
	connect(m_pVolumeGroup[7], SIGNAL(changeVolume(float)), this, SLOT(onChangeVolume_7(float)));



	connect(m_pPinkNoiseButton[0], SIGNAL(clicked()), this, SLOT(onClickPinkNoise_0()));
	connect(m_pPinkNoiseButton[1], SIGNAL(clicked()), this, SLOT(onClickPinkNoise_1()));
	connect(m_pPinkNoiseButton[2], SIGNAL(clicked()), this, SLOT(onClickPinkNoise_2()));
	connect(m_pPinkNoiseButton[3], SIGNAL(clicked()), this, SLOT(onClickPinkNoise_3()));
	connect(m_pPinkNoiseButton[4], SIGNAL(clicked()), this, SLOT(onClickPinkNoise_4()));
	connect(m_pPinkNoiseButton[5], SIGNAL(clicked()), this, SLOT(onClickPinkNoise_5()));
	connect(m_pPinkNoiseButton[6], SIGNAL(clicked()), this, SLOT(onClickPinkNoise_6()));
	connect(m_pPinkNoiseButton[7], SIGNAL(clicked()), this, SLOT(onClickPinkNoise_7()));


	m_pSpeakerLabel = new QLabel(tr("Speaker"), m_pOutputChannelGroup);
	m_pSpeakerLabel->setObjectName(QStringLiteral("BlackFont14"));
	m_pSpeakerLabel->setAlignment(Qt::AlignRight | Qt::AlignVCenter);

	m_pAmpLabel = new QLabel(tr("Amp"), m_pOutputChannelGroup);
	m_pAmpLabel->setObjectName(QStringLiteral("BlackFont14"));
	m_pAmpLabel->setAlignment(Qt::AlignRight | Qt::AlignVCenter);

	m_pRotateCheck = new QCheckBox(tr("Rotate"), m_pOutputChannelGroup);
	connect(m_pRotateCheck, SIGNAL(clicked()), this, SLOT(onClickRotate()));


	m_timer = new QTimer(this);
    connect(m_timer, SIGNAL(timeout()), this, SLOT(onTimer_200ms()));
	m_timer->start(200);

	m_slotEnableFlag = true;

	m_pCalSpin->activateWindow();
	m_pCalSpin->raise();
	refresh();
}

void RoomLevelWidget::resizeEvent(QResizeEvent * /*event*/)
{

	int h1 = 100;
	int h2 = 100 + height() / 8;
	int h3 = height() - h1 - h2;

	int w = width() / 5;
	m_pSignalModeGroup->setGeometry(0, 0, w, h1);
	{
		m_pSignalLabel->setGeometry(w / 2 - 60, h1 / 2 - 30, 120, 20);
		m_pSignalModeCombo->setGeometry(w / 2 - 60, h1 / 2 - 10, 120, 25);
		m_pReminderLabel->setGeometry(w / 2 - 90,  h1 / 2 - 0, 180, 50);
	}

	int i;
	int leftMargin = 0;
	int topGroupHeight = h1;

	m_pCalibrationGroup->setGeometry(width() / 5, 0, width() - width()/ 5, topGroupHeight);
	{
		int w = width() * 2 / 3 - 4 * leftMargin;
		m_pMeterLabel->setGeometry(30, 10, 200, 20);
			
//		int calLeft = 20;
//		int calTop = topGroupHeight / 2 - 12;
//		int calWidth = w - 40;
		m_pCalWidget->setGeometry(0, 0, width() - width()/ 5, topGroupHeight);

		m_pValueLabel->setGeometry(w - 210, topGroupHeight - 22, 100, 20);
		m_pCalSpin->setGeometry(w - 100, topGroupHeight - 25, 80, 20);

//		float minDB = -46;
//		float maxDB = -3;
//		float leftDB = -31.5;
//		float rightDB = -15.5;

//		float leftPercent = (leftDB - minDB) / (maxDB - minDB);
//		float rightPercent = (rightDB - minDB) / (maxDB - minDB);

//		int horizonalWidth = calWidth * (rightPercent - leftPercent);
		m_pLineLabel->setGeometry(0, 25, width() - width()/ 5, 20);

		m_pMeterValue->setGeometry( 0, topGroupHeight - 30, width() - width()/ 5, 20);
		m_pCalLabel->setGeometry( 0, topGroupHeight - 55, width() - width()/ 5, 20);
	}	
	w = width();
	int h = h3 + h2 ;

	m_pOutputChannelGroup->setGeometry(0, h1 , w, h);
	{
		int rightMargin = w / 100;
		int left = 70;

		int inverseTop = 10;
		int pinkNoiseTop = 40;
		int speakerTop = 65;
		int ampTop = 95;
		int sliderTop = 115;

		int markButtonWidth = (w - left) / 8 - rightMargin;
		int markButtonHeight = 17 + h / 50;
		int sliderCenterX[8];
		sliderCenterX[0] = left + markButtonWidth / 2;

		for (i = 1; i < 8; i++)
		{
			sliderCenterX[i] = sliderCenterX[i - 1] + markButtonWidth + rightMargin;
		}

		m_pOutputLabel->setGeometry(w / 2 - 100, inverseTop, 200, 22);
		m_pRotateCheck->setGeometry(w - 120, inverseTop, 100, 25);

		int shinkMargin = 4;
		for (i = 0; i < 8; i++)
		{
			m_pPinkNoiseButton[i]->setGeometry(sliderCenterX[i] - markButtonWidth / 2, pinkNoiseTop, markButtonWidth, markButtonHeight);
			m_pSpeakerName[i]->setGeometry(sliderCenterX[i] - markButtonWidth / 2, speakerTop + shinkMargin, markButtonWidth, markButtonHeight - 2 * shinkMargin);
			m_pAmpName[i]->setGeometry(sliderCenterX[i] - markButtonWidth / 2, ampTop + shinkMargin, markButtonWidth, markButtonHeight - 2 * shinkMargin);
			m_pVolumeGroup[i]->setGeometry(sliderCenterX[i] - markButtonWidth / 2 -30, sliderTop, markButtonWidth + 60, h - 10 - sliderTop);
		}
		m_pSpeakerLabel->setGeometry(0, speakerTop, left - 10, 20 + h / 50);
		m_pAmpLabel->setGeometry(0, ampTop, left - 10, 20 + h / 50);
	}
}

#if 0
void RoomLevelWidget::paintEvent(QPaintEvent * /*event */)
{
	QPainter painter(this);

	painter.setPen(QPen(QColor(0, 255, 0), 1));
	painter.drawRect(0, 0, width() - 1, height() - 1);
}
#endif

void RoomLevelWidget::onClickPinkNoise(int index)
{
	if (m_slotEnableFlag == false)
	{
		return;
	}

	m_pVolumeGroup[index]->getVolumeSlider()->setFocus();
	CPi2000Data *pData = getCPi2000Data();
	RTAData *pRTA = pData->getRTA();

	if (pRTA->getRotate() == true)
	{
		pRTA->setRotate(false);
		m_pRotateCheck->setChecked(false);
	}

	unsigned short ret = GetKeyState(VK_LCONTROL);
	qDebug() << ret;
	bool channelFlag[8] = {false, false, false, false, false, false, false, false };
	bool flag;
	if (ret & 0x8000)
	{
		// Left Control is pressed.
		pRTA->getRoomLevelPinkNoise(channelFlag);
		flag = !m_pPinkNoiseButton[index]->getMarkFlag();
	}
	else
	{
		flag = true;
	}

//	if (pRTA->getRotate() == false)
	{
		channelFlag[index] = flag;
		if (flag == true)
		{
			pData->setEQTuningPinkNoise(index);
		}
		pData->setRoomLevelPinkNoise(channelFlag);
		refreshPinkNoise();

		if (g_pApp->getConnectStatus() == DEVICE_NETWORK_CONNECTED)
		{
			g_pApp->getDeviceConnection()->setPinkNoise(channelFlag);
		}

		refreshLabel();
	}
}


void RoomLevelWidget::toggleMuteButton(int index, bool flag)
{
	CPi2000Data *pData = getCPi2000Data();
	pData->setSpeakerRoomLevelMuteFlag(index, flag);

	if (g_pApp->getConnectStatus() == DEVICE_NETWORK_CONNECTED)
	{
		g_pApp->getDeviceConnection()->setSpeakerMuteFlag(index, flag);
	}
}

void RoomLevelWidget::onChangeVolume(int index, float value)
{
	CPi2000Data *pData = getCPi2000Data();
	pData->setSpeakerRoomLevelGain(index, value);

	if (g_pApp->getConnectStatus() == DEVICE_NETWORK_CONNECTED)
	{
		g_pApp->getDeviceConnection()->setSpeakerVolume(index, value);
	}	
}

void RoomLevelWidget::refreshPinkNoise()
{
	bool oldSlotEnableFlag = m_slotEnableFlag;
	m_slotEnableFlag = false;

	CPi2000Data *pData = getCPi2000Data();
	RTAData *pRTA = pData->getRTA();

	bool channelFlag[8];
	pRTA->getRoomLevelPinkNoise(channelFlag);

	for (int i = 0; i < 8; i++)
	{
		m_pPinkNoiseButton[i]->mark(channelFlag[i]);
		m_pPinkNoiseButton[i]->setEnabled(pRTA->getSignalMode() != SIGNAL_OFF);
	}

	if (m_pRotateCheck->isChecked() != pRTA->getRotate())
	{
		m_pRotateCheck->setChecked(pRTA->getRotate());
	}
	m_pRotateCheck->setEnabled(pRTA->getSignalMode() != SIGNAL_OFF);

	m_slotEnableFlag = oldSlotEnableFlag;
}

void RoomLevelWidget::refresh()
{
	bool oldSlotEnableFlag = m_slotEnableFlag;
	m_slotEnableFlag = false;

	CPi2000Data *pData = getCPi2000Data();
	RTAData *pRTA = pData->getRTA();

	if (m_pSignalModeCombo->currentIndex() != (int)pRTA->getSignalMode())
	{
		m_pSignalModeCombo->setCurrentIndex((int)pRTA->getSignalMode());
	}

	if (pRTA->getSignalMode() == SIGNAL_OFF)
	{
		m_pReminderLabel->hide();
	}
	else
	{
//		m_pReminderLabel->show();
	}

	refreshPinkNoise();

	m_pCalSpin->setEnabled(SIGNAL_OFF != pRTA->getSignalMode());

	refreshLabel();
	refreshSPL();
	m_pCalSpin->setValue(pRTA->getMeasuredValue());
	

	for (int index = 0; index < 8; index++)
	{
		float volume = pData->getSpeakerRoomLevelGain(index);
		m_pVolumeGroup[index]->setVolume(volume);

		bool muteFlag = pData->getSpeakerRoomLevelMuteFlag(index);
		m_pVolumeGroup[index]->setMuteFlag(muteFlag);
	}

	for (int i = 0; i < 8; i++)
	{
		m_pSpeakerName[i]->setText(pData->m_speakerName[i]);
		m_pAmpName[i]->setText(pData->m_amplifierName[i]);
	}

	m_slotEnableFlag = oldSlotEnableFlag;
}

void RoomLevelWidget::refreshSPL()
{
	CPi2000Data *pData = getCPi2000Data();
	RTAData *pRTA = pData->getRTA();

	if (pRTA->getSignalMode() == SIGNAL_OFF)
	{
		pRTA->setSPLValue(-60);
		pRTA->setMeasuredValue(0);
	}

	float splValue = pRTA->getSPLValue();

	{
//		m_pCalValue->setText(QString("%1 dB").arg(splValue));
		m_pCalWidget->setValue(splValue);
		m_pCalWidget->refresh();

		if (fabs(pRTA->getMeasuredValue()) < 0.05)
		{
			m_pMeterValue->setText(QString("0.0 dB"));
			m_pMeterValue->setEnabled(false);
		}
		else
		{
			float addValue = (float)pRTA->getAddValue();
			m_pMeterValue->setEnabled(true);
			m_pMeterValue->setText(QString::number(splValue + addValue, 'f', 1) + QString(" dB"));
		}
	}
}

void RoomLevelWidget::refreshLabel()
{
	CPi2000Data *pData = getCPi2000Data();
	RTAData *pRTA = pData->getRTA();

	int pinkNoiseIndex = -1;
	bool channelFlag[8];
	pRTA->getRoomLevelPinkNoise(channelFlag);
	for (int index = 0; index < 8; index++)
	{
		if (channelFlag[index] == true)
		{
			pinkNoiseIndex = index;
			break;
		}
	}

	if (fabs(pRTA->getMeasuredValue()) < 0.05)
	{
		m_pCalLabel->setText(tr("uncal"));	
	}
	else
	{
		if (pinkNoiseIndex == -1)
		{
			m_pCalLabel->setText(tr(""));
		}
		else if (pinkNoiseIndex <= 3)
		{
			m_pCalLabel->setText(tr("(target 85 dBC)"));	
		}
		else if (pinkNoiseIndex <= 7)
		{
			m_pCalLabel->setText(tr("(target 82 dBC)"));	
		}
	}
}


void RoomLevelWidget::onSignalModeSelected(int currentMode)
{
	if (m_slotEnableFlag == false)
	{
		return;
	}
	CPi2000Data *pData = getCPi2000Data();
	RTAData *pRTA = pData->getRTA();
	SIGNAL_MODE oldMode = pRTA->getSignalMode();

	emit g_pApp->setSignalMode(currentMode);

	if ((oldMode == SIGNAL_OFF) && (currentMode != SIGNAL_OFF))
	{
		emit m_pPinkNoiseButton[2]->clicked();
	}

#if 0
	DeviceSocket *pSocket = g_pApp->getDeviceConnection();

	CPi2000Data *pData = getCPi2000Data();
	RTAData *pRTA = pData->getRTA();
	SIGNAL_MODE oldMode = pRTA->getSignalMode();
	pRTA->setSignalMode((SIGNAL_MODE)currentMode);

	if ((oldMode == SIGNAL_OFF) && (currentMode != SIGNAL_OFF))
	{
		pRTA->setMeasuredValue(0.0);
		pRTA->setRotate(false);
		pRTA->setPinkNoise(-1);

		// Start RTA
		if (g_pApp->getConnectStatus() == DEVICE_NETWORK_CONNECTED)
		{
			pSocket->startSignal();
			pSocket->setPinkNoise(-1);
		}
		g_pMainWidget->startSignalGenerator();
	}
	else if ((oldMode != SIGNAL_OFF) && (currentMode == SIGNAL_OFF))
	{
		pRTA->setMeasuredValue(0.0);
		pRTA->setRotate(false);
		pRTA->setPinkNoise(-1);
		pRTA->setSPLValue(-60.0);
		pRTA->setMeasuredValue(0.0);
		pRTA->setAddValue(0.0);

		// Stop RTA
		if (g_pApp->getConnectStatus() == DEVICE_NETWORK_CONNECTED)
		{
			pSocket->setPinkNoise(-1);
			pSocket->stopSignal();
		}
		g_pMainWidget->stopSignalGenerator();
	}

	if (g_pApp->getConnectStatus() == DEVICE_NETWORK_CONNECTED)
	{
		pSocket->setSignalMode((SIGNAL_MODE)currentMode);
	}

	if (pRTA->getSignalMode() == SIGNAL_OFF)
	{
		m_pReminderLabel->hide();
	}
	else
	{
//		m_pReminderLabel->show();
	}

	refresh();
#endif
}

void RoomLevelWidget::onClacChanged(double value)
{
	if (m_slotEnableFlag == false)
	{
		return;
	}

	CPi2000Data *pData = getCPi2000Data();
	RTAData *pRTA = pData->getRTA();

	bool refreshFlag = false;
	if ((fabs(pRTA->getMeasuredValue()) < 0.05) || (fabs(value) < 0.05))
	{
		refreshFlag = true;
	}
	pRTA->setMeasuredValue(value);
	float splValue = pRTA->getSPLValue();
	pRTA->setAddValue(value - splValue);

	if (refreshFlag == true)
	{
		refresh();
	}
}

void RoomLevelWidget::onTimer_200ms()
{
	static int s_count = 1;
	CPi2000Data *pData = getCPi2000Data();
	RTAData *pRTA = pData->getRTA();

	s_count++;
	if (s_count % 2 == 0)
	{
		// 		asyncget "\\Preset\SplMeters\SV\SplValue"
		if (pRTA->getSignalMode() != SIGNAL_OFF)
		{
			if (g_pApp->getConnectStatus() == DEVICE_NETWORK_CONNECTED)
			{
				g_pApp->getDeviceConnection()->getSPLValue();
			}
		}
	}

	if (s_count == 20)
	{
		s_count = 0;

		if (pRTA->getSignalMode() == SIGNAL_OFF)
		{
			return;
		}
		else
		{
			if (pRTA->getRotate() == true)
			{
				int index = pRTA->getEQTuningPinkNoise();
				index++;
				if (index >= 8)
				{
					index = 0;
				}

				pRTA->setEQTuningPinkNoise(index);

				bool channelFlag[8] = { false, false, false, false, false, false, false, false };
				channelFlag[index] = true;
				pRTA->setRoomLevelPinkNoise(channelFlag);

				refreshPinkNoise();

				if (g_pApp->getConnectStatus() == DEVICE_NETWORK_CONNECTED)
				{
					g_pApp->getDeviceConnection()->setPinkNoise(channelFlag);
				}
				refreshLabel();
			}
		}
	}
}

void RoomLevelWidget::onClickRotate(void)
{
	if (m_slotEnableFlag == false)
	{
		return;
	}

	CPi2000Data *pData = getCPi2000Data();
	RTAData *pRTA = pData->getRTA();
	pRTA->setRotate(m_pRotateCheck->isChecked());
}

void RoomLevelWidget::onSPLChanged()
{
	refreshSPL();
}

void RoomLevelWidget::retranslateUi()
{
	m_pSignalModeGroup->setTitle(tr(""));
	m_pSignalLabel->setText(tr("Signal Mode"));
	m_pReminderLabel->setText(tr("To stop RTA setting, you need to set signal mode to 'OFF'."));
	m_pCalibrationGroup->setTitle(tr(""));
	m_pMeterLabel->setText(tr("Internal SPL Meter Calibration"));
	m_pValueLabel->setText(tr("Measured Value")); 
	m_pLineLabel->setText(tr("Target Mic Level"));
	m_pCalLabel->setText(tr("uncal"));
	m_pOutputLabel->setText(tr("Output Channel Level"));

	QStringList pinkNoiseStringList;
	pinkNoiseStringList << tr("Left") << tr("Right") << tr("Center") << tr("Subwoofer") << tr("Left Surround") << tr("Right Surround") << tr("Back Left Surround") << tr("Back Right Surround");
	for (int i = 0; i < 8; i++)
	{
		m_pPinkNoiseButton[i]->setText(pinkNoiseStringList.at(i));
	}
	m_pSpeakerLabel->setText(tr("Speaker"));
	m_pAmpLabel->setText(tr("Amp"));
	m_pRotateCheck->setText(tr("Rotate"));
}

void RoomLevelWidget::keyPressEvent(QKeyEvent *event)
{
	CPi2000Data *pData = getCPi2000Data();
	RTAData *pRTA = pData->getRTA();
	int nextFocus = -1;

	if (pRTA->getSignalMode() == SIGNAL_OFF)
	{
		goto END;
	}

	if (m_pRotateCheck->isChecked() == true)
	{
		goto END;
	}

    switch (event->key())
    {
		case Qt::Key_Left:
		case Qt::Key_Right:
			{
				for (int i = 0; i < 8; i++)
				{
					if (m_pVolumeGroup[i]->getVolumeSlider()->hasFocus() == true)
					{
						if (m_pPinkNoiseButton[i]->getMarkFlag() == false)
						{
							goto END;
						}

						if (event->key() == Qt::Key_Right)
						{
							nextFocus = i + 1;
							if (nextFocus == 8)
							{
								nextFocus = 0;
							}
						}
						else
						{
							nextFocus = i - 1;
							if (nextFocus == -1)
							{
								nextFocus = 7;
							}
						}
						emit m_pPinkNoiseButton[nextFocus]->clicked();
						event->accept();
						return;
					}
				}
			}
	}

END:	
	QWidget::keyPressEvent(event);
}
