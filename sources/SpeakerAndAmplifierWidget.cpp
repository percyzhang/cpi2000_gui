#include "SpeakerAndAmplifierWidget.h"
#include <QPainter>
#include "CPi2000Setting.h"
#include <QFileDialog>
#include <QMessageBox>

SpeakerTypeDialog::SpeakerTypeDialog(QWidget *parent) : QDialog(parent)
{
	m_pSpeakerNameLabel = new QLabel(tr("Speaker Name:"), this);
	m_pSpeakerEQLabel = new QLabel(tr("Speaker EQ:"), this);
	m_pFileLabel = new QLabel(tr("EQ File:"), this);

	m_pSpeakerNameEdit = new QLineEdit(this);

	m_pFileEdit = new QLineEdit(this);
	m_pFileEdit->setReadOnly(true);

	m_pOKButton = new QPushButton(tr("OK"), this);

	m_pCancelButton = new QPushButton(tr("Cancel"), this);

	m_pFileButton = new QPushButton("...", this);
	connect(m_pFileButton,  SIGNAL(clicked()), this, SLOT(onFileButtonClicked()));	

	m_pEQBox = new QCheckBox(tr("Enable"), this);

	setFixedSize(380, 220);
	setWindowTitle(tr("Speaker Type"));
}


void SpeakerTypeDialog::onFileButtonClicked(void)
{
	QString directory = ".";
	QString fileName = QFileDialog::getOpenFileName(
        this,
        "Select Speaker EQ File",
        directory,
        tr("Speaker EQ File(*.eq);; All files (*.*)")
	);
	
	if (fileName.isEmpty() != false)
	{
		return;
	}

	QFile file(fileName);
    if (!file.open(QIODevice::ReadOnly)) 
	{
        QMessageBox::warning(this, tr("Open File Error"),
                             tr("Cannot open the speaker EQ file: %1.").arg(file.fileName()), tr("OK"));
		return;
    }

	m_pFileEdit->setText(fileName);
}

void SpeakerTypeDialog::resizeEvent(QResizeEvent * /* event */)
{
	int top = 60;
	int left = 35;
	m_pSpeakerNameLabel->setGeometry(80 - left, 102 - top, 100, 22);
	m_pSpeakerEQLabel->setGeometry(80 - left, 140 - top, 100, 22);
	m_pFileLabel->setGeometry(80 - left, 173 - top, 100, 22);

	m_pEQBox->setGeometry(180 - left, 140 - top, 100, 22);
	m_pSpeakerNameEdit->setGeometry(180 - left, 100 - top, 182, 27);

	m_pFileEdit->setGeometry(180 - left, 171 - top, 182, 27);
	m_pFileEdit->setReadOnly(true);

	m_pFileButton->setGeometry(362 - left, 171 - top, 25, 25);

	m_pOKButton->setGeometry(133 - left, 224 - top, 72, 27);
	m_pCancelButton->setGeometry(259 - left, 224 - top, 72, 27);
}


SpeakerAndAmplifierWidget::SpeakerAndAmplifierWidget(QWidget *parent)
	: QWidget(parent)
{
//	connect(this, SIGNAL(currentChanged(int)), this, SLOT(onTabChanged(int)));
	for (int i = 0; i < 8; i++)
	{
		m_pSpeakerNameLabel[i] = new QLabel(tr("Speaker:"), this);
		m_pSpeakerNameLabel[i]->setAlignment(Qt::AlignRight | Qt::AlignVCenter);
		m_pSpeakerNameLabel[i]->setObjectName(QStringLiteral("WhiteFont12"));

		m_pAmplifierNameLabel[i] = new QLabel(tr("Amplifier:"), this);
		m_pAmplifierNameLabel[i]->setAlignment(Qt::AlignRight | Qt::AlignVCenter);
		m_pAmplifierNameLabel[i]->setObjectName(QStringLiteral("WhiteFont12"));

		m_pComboBox[i] = new QComboBox(this);
		m_pComboBox[i]->setEditable(false);
		m_pAmplifierEdit[i] = new QLineEdit(this);
		m_pAmplifierEdit[i]->setMaxLength(18);
		connect(m_pAmplifierEdit[i], SIGNAL(textChanged(const QString &)), this, SLOT(onAmplifierChanged(const QString &)));
	}

	m_pWarningInfo = new QLabel("Warning", this);
	m_pWarningInfo->setObjectName(QStringLiteral("WhiteFont12"));
	m_pWarningInfo->setAlignment(Qt::AlignLeft | Qt::AlignTop);
	m_pWarningInfo->setWordWrap(true);
	m_pWarningInfo->hide();

	connect(m_pComboBox[0], SIGNAL(currentIndexChanged(int)), this, SLOT(onSpeakerChanged_0(int)));
	connect(m_pComboBox[1], SIGNAL(currentIndexChanged(int)), this, SLOT(onSpeakerChanged_1(int)));
	connect(m_pComboBox[2], SIGNAL(currentIndexChanged(int)), this, SLOT(onSpeakerChanged_2(int)));
	connect(m_pComboBox[3], SIGNAL(currentIndexChanged(int)), this, SLOT(onSpeakerChanged_3(int)));
	connect(m_pComboBox[4], SIGNAL(currentIndexChanged(int)), this, SLOT(onSpeakerChanged_4(int)));
	connect(m_pComboBox[5], SIGNAL(currentIndexChanged(int)), this, SLOT(onSpeakerChanged_5(int)));
	connect(m_pComboBox[6], SIGNAL(currentIndexChanged(int)), this, SLOT(onSpeakerChanged_6(int)));
	connect(m_pComboBox[7], SIGNAL(currentIndexChanged(int)), this, SLOT(onSpeakerChanged_7(int)));

	m_slotEnableFlag = true;

	initUpdate();
}

void SpeakerAndAmplifierWidget::onAmplifierChanged(const QString &)
{
	emit changeSpeakerAndAmplifier();
}

QString SpeakerAndAmplifierWidget::getSpeakerName(int i) 
{
	if ((i < 0) || (i > 7))
	{
		qDebug() << "Invalid getSpeakerName " << i;
		return ("");
	}

	QString speakerName = m_pComboBox[i]->currentText();
	qDebug() << "speakerName["<< i << "] = " << speakerName; 
	if (speakerName == "DSP OFF")
	{
		QStringList bypassSpeakerName;
		bypassSpeakerName << "Passive-Bypass" << "Passive-Bypass" << "Passive-Bypass" << "Subwoofer-Bypass" << "Surround-Bypass" << "Surround-Bypass" << "Surround-Bypass" << "Surround-Bypass";
		return bypassSpeakerName.at(i); 
	}
	else
	{
		return speakerName;
	}
}

void SpeakerAndAmplifierWidget::setLRTypeMatch(SPEAKER_TYPE left, SPEAKER_TYPE right)
{
	QString speakerType[3] = {tr("Full-range"), tr("Two-way"), tr("Three-way")};

	/* L/R Speaker */
	QPalette palette = m_pComboBox[0]->palette();
	if (left == right)
	{
		palette.setColor(QPalette::Text, Qt::black);
		m_pWarningInfo->hide();
	}
	else
	{
		m_pWarningInfo->setText(tr("Warning: The type of the Left Speaker is %1,\n and the right is %2;\nThey are not the same type!").arg(speakerType[left]).arg(speakerType[right]));
		palette.setColor(QPalette::Text, Qt::red);
		m_pWarningInfo->show();
	}
	m_pComboBox[0]->setPalette(palette);
	m_pComboBox[1]->setPalette(palette);
}

void SpeakerAndAmplifierWidget::onSpeakerChanged_0(int /* index */)
{
	if (m_slotEnableFlag == false)
		return;

//	emit changeLRSpeaker();
	emit changeSpeakerAndAmplifier();
}

void SpeakerAndAmplifierWidget::onSpeakerChanged_1(int /* index */)
{
	if (m_slotEnableFlag == false)
		return;

//	emit changeLRSpeaker();
	emit changeSpeakerAndAmplifier();
}

void SpeakerAndAmplifierWidget::onSpeakerChanged_2(int /* index */)
{
//	qDebug() << "index2 : " << index;
//	onSpeakerChanged(2, index); 
	emit changeSpeakerAndAmplifier();
}

void SpeakerAndAmplifierWidget::onSpeakerChanged_3(int /* index */)
{
//	qDebug() << "index3 : " << index;
//	onSpeakerChanged(3, index); 
	emit changeSpeakerAndAmplifier();
}

void SpeakerAndAmplifierWidget::onSpeakerChanged_4(int /* index */)
{
//	qDebug() << "index4 : " << index;
//	onSpeakerChanged(4, index); 
	emit changeSpeakerAndAmplifier();
}

void SpeakerAndAmplifierWidget::onSpeakerChanged_5(int /* index */)
{
//	qDebug() << "index5 : " << index;
//	onSpeakerChanged(5, index); 
	emit changeSpeakerAndAmplifier();
}

void SpeakerAndAmplifierWidget::onSpeakerChanged_6(int /* index */)
{
//	qDebug() << "index6 : " << index;
//	onSpeakerChanged(6, index); 
	emit changeSpeakerAndAmplifier();
}

void SpeakerAndAmplifierWidget::onSpeakerChanged_7(int /* index */)
{
//	qDebug() << "index7 : " << index;
//	onSpeakerChanged(7, index); 
	emit changeSpeakerAndAmplifier();
}

void SpeakerAndAmplifierWidget::onSpeakerChanged(int /* comboIndex */, int /*selectIndex*/)
{
#if 0
	qDebug() << m_pComboBox[comboIndex]->count() << selectIndex; 


	if (((selectIndex + 1) == m_pComboBox[comboIndex]->count()) && (selectIndex != 0))
	{
		/* Others... is selected */
		SpeakerTypeDialog speakerDlg(this);
		speakerDlg.setModal(true);
		int ret = speakerDlg.exec();
		if (ret == QDialog::Accepted) 
		{
		}
	}
#endif
}

void SpeakerAndAmplifierWidget::initUpdate()
{
	refreshAvailableSpeaker();
	refreshSpeakerAndAmplifierName();
}

void SpeakerAndAmplifierWidget::refreshSpeakerAndAmplifierName()
{
	CPi2000Data *pCPi2000Data = getCPi2000Data();

	bool oldSlotEnableFlag = m_slotEnableFlag;
	m_slotEnableFlag = false;

	m_pComboBox[0]->setCurrentText(pCPi2000Data->m_speakerName[0]);
	m_pComboBox[1]->setCurrentText(pCPi2000Data->m_speakerName[1]);
	m_pComboBox[2]->setCurrentText(pCPi2000Data->m_speakerName[2]);
	m_pComboBox[3]->setCurrentText(pCPi2000Data->m_speakerName[3]);
	m_pComboBox[4]->setCurrentText(pCPi2000Data->m_speakerName[4]);
	m_pComboBox[5]->setCurrentText(pCPi2000Data->m_speakerName[5]);
	m_pComboBox[6]->setCurrentText(pCPi2000Data->m_speakerName[6]);
	m_pComboBox[7]->setCurrentText(pCPi2000Data->m_speakerName[7]);

	m_pAmplifierEdit[0]->setText(pCPi2000Data->m_amplifierName[0]);
	m_pAmplifierEdit[1]->setText(pCPi2000Data->m_amplifierName[1]);
	m_pAmplifierEdit[2]->setText(pCPi2000Data->m_amplifierName[2]);
	m_pAmplifierEdit[3]->setText(pCPi2000Data->m_amplifierName[3]);
	m_pAmplifierEdit[4]->setText(pCPi2000Data->m_amplifierName[4]);
	m_pAmplifierEdit[5]->setText(pCPi2000Data->m_amplifierName[5]);
	m_pAmplifierEdit[6]->setText(pCPi2000Data->m_amplifierName[6]);
	m_pAmplifierEdit[7]->setText(pCPi2000Data->m_amplifierName[7]);

	m_slotEnableFlag = oldSlotEnableFlag;
}

void SpeakerAndAmplifierWidget::refresh() 
{
	bool oldSlotEnableFlag = m_slotEnableFlag;
	m_slotEnableFlag = false;

	refreshAvailableSpeaker();
	refreshSpeakerAndAmplifierName();

	m_slotEnableFlag = oldSlotEnableFlag;
}

void SpeakerAndAmplifierWidget::refreshAvailableSpeaker()
{
	int i;
//	int itemCount;
	bool oldSlotEnableFlag = m_slotEnableFlag;
	m_slotEnableFlag = false;

	disconnect(m_pComboBox[0], SIGNAL(currentIndexChanged(int)), this, SLOT(onSpeakerChanged_0(int)));
	disconnect(m_pComboBox[1], SIGNAL(currentIndexChanged(int)), this, SLOT(onSpeakerChanged_1(int)));
	disconnect(m_pComboBox[2], SIGNAL(currentIndexChanged(int)), this, SLOT(onSpeakerChanged_2(int)));
	disconnect(m_pComboBox[3], SIGNAL(currentIndexChanged(int)), this, SLOT(onSpeakerChanged_3(int)));
	disconnect(m_pComboBox[4], SIGNAL(currentIndexChanged(int)), this, SLOT(onSpeakerChanged_4(int)));
	disconnect(m_pComboBox[5], SIGNAL(currentIndexChanged(int)), this, SLOT(onSpeakerChanged_5(int)));
	disconnect(m_pComboBox[6], SIGNAL(currentIndexChanged(int)), this, SLOT(onSpeakerChanged_6(int)));
	disconnect(m_pComboBox[7], SIGNAL(currentIndexChanged(int)), this, SLOT(onSpeakerChanged_7(int)));

	for (i = 0; i < 8; i++)
	{
		m_pComboBox[i]->clear();

		/* Add "DSP OFF" for preset "Bypass" */
		m_pComboBox[i]->addItem("DSP OFF");

		QStringList availSpeakerName = getCPi2000Data()->getAvailableSpeakerName(SPEAKER_CHANNEL(i));
		m_pComboBox[i]->addItems(availSpeakerName);
//		for (itemCount = 0; itemCount < availSpeakerName.count(); itemCount++)
//		{
//			m_pComboBox[i]->addItem(availSpeakerName.at(itemCount));
//		}
	}

	connect(m_pComboBox[0], SIGNAL(currentIndexChanged(int)), this, SLOT(onSpeakerChanged_0(int)));
	connect(m_pComboBox[1], SIGNAL(currentIndexChanged(int)), this, SLOT(onSpeakerChanged_1(int)));
	connect(m_pComboBox[2], SIGNAL(currentIndexChanged(int)), this, SLOT(onSpeakerChanged_2(int)));
	connect(m_pComboBox[3], SIGNAL(currentIndexChanged(int)), this, SLOT(onSpeakerChanged_3(int)));
	connect(m_pComboBox[4], SIGNAL(currentIndexChanged(int)), this, SLOT(onSpeakerChanged_4(int)));
	connect(m_pComboBox[5], SIGNAL(currentIndexChanged(int)), this, SLOT(onSpeakerChanged_5(int)));
	connect(m_pComboBox[6], SIGNAL(currentIndexChanged(int)), this, SLOT(onSpeakerChanged_6(int)));
	connect(m_pComboBox[7], SIGNAL(currentIndexChanged(int)), this, SLOT(onSpeakerChanged_7(int)));

	m_slotEnableFlag = oldSlotEnableFlag;
}

void SpeakerAndAmplifierWidget::resizeEvent(QResizeEvent * /* event */)
{
	int leftTopX= width() / 6;
	int leftTopY = 12;

	QPoint speakerCenter[] = { QPoint(leftTopX + width() / 10, leftTopY), QPoint(leftTopX + width() * 5 / 9, leftTopY), QPoint(leftTopX + width() / 3 - 5, leftTopY), 
		QPoint(width() * 11 / 18, height() / 3 + 9), QPoint(width() / 6 + 15, height() / 2 + 6), QPoint(width() * 4 / 5, height() / 2 + 6), 
		QPoint(width() * 6 / 18, height() * 3 / 4 + 3), QPoint(width() * 12 / 18, height() * 3 / 4 + 3)			};

	int intervalHeight = height() / 16 + 8;
	for (int i = 0; i < 8; i++)
	{
		m_pSpeakerNameLabel[i]->setGeometry(speakerCenter[i].x() - 75, speakerCenter[i].y() + intervalHeight - 4, 50, 20);
		m_pAmplifierNameLabel[i]->setGeometry(speakerCenter[i].x() - 75, speakerCenter[i].y() + 2 * intervalHeight - 4, 50, 20);
		m_pComboBox[i]->setGeometry(speakerCenter[i].x() - 10 - 10, speakerCenter[i].y() + intervalHeight - 4, 40 + width() / 12, 20);
		m_pAmplifierEdit[i]->setGeometry(speakerCenter[i].x() - 20, speakerCenter[i].y() + 2 * intervalHeight - 4, 40 + width() / 12, 20);
	}
	m_pWarningInfo->setGeometry(speakerCenter[0].x() - 75, speakerCenter[0].y() + 3 * intervalHeight, 600, 60);
}

void SpeakerAndAmplifierWidget::paintEvent(QPaintEvent * /*event*/)
{
//	initUpdate();

	QPainter painter(this);
	painter.setPen(QPen(QColor(255, 255, 255), 1));
//	painter.drawRect(0, 0, width() - 1, height() - 1);
//	painter.fillRect(0, 0, width() - 1, height() - 1, QColor(255, 255, 255));

	int leftTopX= width() / 6;
	int leftTopY = 12;

	QPoint leftTop(leftTopX, leftTopY);
	QPoint rightTop(width() - leftTopX, leftTopY);
	QPoint leftBottom(0, height() - 1);
	QPoint rightBottom(width() - 1, height() - 1);

	painter.drawLine(leftTop, rightTop);
	painter.drawLine(leftTop, leftBottom);
	painter.drawLine(leftBottom, rightBottom);
	painter.drawLine(rightTop, rightBottom);

	QPoint speakerCenter[] = { QPoint(leftTopX + width() / 9, leftTopY), QPoint(leftTopX + width() / 3, leftTopY), QPoint(leftTopX + width() * 5 / 9, leftTopY),
		QPoint(width() * 11 / 18, height() / 3 + 9), QPoint(width() / 6, height() / 2 + 6), QPoint(width() * 5 / 6, height() / 2 + 6), 
		QPoint(width() * 6 / 18, height() * 3 / 4 + 3), QPoint(width() * 12 / 18, height() * 3 / 4 + 3)			};

	painter.setBrush(QColor(89, 89, 89));
	for (int i = 0; i < 8; i++)
	{
		int speakerWidth = 15;
		int speakerHeight = 22;
		painter.fillRect(speakerCenter[i].x() - speakerWidth / 2, speakerCenter[i].y() - speakerHeight / 2, speakerWidth, speakerHeight, QColor(255, 255, 255));

		int radius = 3;
		painter.drawEllipse(speakerCenter[i].x() - radius, speakerCenter[i].y() - 8, radius * 2, radius * 2);
		radius = 4;
		painter.drawEllipse(speakerCenter[i].x() - radius, speakerCenter[i].y() - 1, radius * 2, radius * 2);

	}
}

void SpeakerAndAmplifierWidget::retranslateUi()
{
	for (int i = 0; i < 8; i++)
	{
		m_pSpeakerNameLabel[i]->setText(tr("Speaker:"));
		m_pAmplifierNameLabel[i]->setText(tr("Amplifier:"));
	}
}
