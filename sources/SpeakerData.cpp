#include "SpeakerData.h"
#include "commonLib.h"
#include <QJsonObject>
#include <QJsonDocument>
#include <QDebug>
#include "plotFir.h"
#include "conv.h"

QString XOverToFIR(int tab_hpf, double frequency_hpf, int tab_lpf, double frequency_lpf)
{
	double coef_HPF[384], coef_LPF[384];
	double coef[384];
	int tab;

	// for HPF
	if (tab_hpf >= 50)
	{
		plotFir hpf_Fir;
		hpf_Fir.setFirParameter(true, tab_hpf, HIGHPASS, frequency_hpf, 0.1);
		hpf_Fir.getCoef(coef_HPF);
		hpf_Fir.getCoef(coef);
		tab = tab_hpf;
	}

	if (tab_lpf >= 50)
	{
		plotFir lpf_Fir;
		lpf_Fir.setFirParameter(true, tab_lpf, LOWPASS, frequency_lpf, 0.1);
		lpf_Fir.getCoef(coef_LPF);
		lpf_Fir.getCoef(coef);
		tab = tab_lpf;
	}

	if ((tab_hpf >= 50) && (tab_lpf >= 50))
	{
		conv testConv;
		testConv.convolution(coef, coef_HPF, tab_hpf, coef_LPF, tab_lpf);
		tab = tab_hpf + tab_lpf - 1;
	}

	QString str = QString::number(coef[0], 'f', 6);
	for (int i = 1; i < tab; i++)
	{
		str += QString(", %1").arg(QString::number(coef[i], 'f', 6));
	}

	qDebug() << str;
	return str;
}

SpeakerData::SpeakerData()
{
	int index;
	m_speakerName = "";
	m_readable = true;
	m_writable = true;
	m_speakerType = SPEAKER_TYPE_PASSIVE;

	for (int i = 0; i < 3; i++)
	{
		for (index = 0; index <= 10; index++)
		{
			m_channelData[i].m_EQData[index].enable(false);
		}

		m_channelData[i].m_limiter.m_enableFlag = true;
		m_channelData[i].m_limiter.m_autoFlag = false;
		m_channelData[i].m_limiter.m_holdTimeInMs = 3;
		m_channelData[i].m_limiter.m_attackTimeInMs = 10;
		m_channelData[i].m_limiter.m_release = 320;
		m_channelData[i].m_limiter.m_threshold = 5.5;

		m_channelData[i].m_polarityNormalFlag = true;
		m_channelData[i].m_delayInMs = 0;
		m_channelData[i].m_gain = 0.0;
		m_channelData[i].m_strFIR = QString("1.0");
		m_channelData[i].m_PEQEnableFlag = true;
		m_channelData[i].m_HPF_IIR_EnableFlag = true;
		m_channelData[i].m_LPF_IIR_EnableFlag = true;

		m_channelData[i].m_EQData[0].setType(EQ_L_SHELF);
		m_channelData[i].m_EQData[0].setSlope(3.5);
		m_channelData[i].m_EQData[0].setGain(9.5);
		m_channelData[i].m_EQData[0].setFrequency(100);

		m_channelData[i].m_EQData[1].setType(EQ_H_SHELF);
		m_channelData[i].m_EQData[1].setSlope(3.5);
		m_channelData[i].m_EQData[1].setGain(9.5);
		m_channelData[i].m_EQData[1].setFrequency(100);

		for (index = 2; index < 8; index++)
		{
			m_channelData[i].m_EQData[index].setType(EQ_PEAKING);
			m_channelData[i].m_EQData[index].setFrequency(2000 * index);
			m_channelData[i].m_EQData[index].setGain(2.4 + index * 1);
		}

		m_channelData[i].m_EQData[8].setType(EQ_HPF);
		m_channelData[i].m_EQData[8].setPFType(PF_IIR);
		m_channelData[i].m_EQData[8].setPFIIRType(PF_IIR_BS18);
		m_channelData[i].m_EQData[8].setFrequency(1000);

		m_channelData[i].m_EQData[9].setType(EQ_LPF);
		m_channelData[i].m_EQData[9].setPFType(PF_IIR);
		m_channelData[i].m_EQData[9].setPFIIRType(PF_IIR_BS18);
		m_channelData[i].m_EQData[9].setFrequency(14000);

		m_channelData[i].m_EQData[10].setType(EQ_USER_DEFINED_FIR);
		m_channelData[i].m_EQData[10].setUserDefinedFIR("1.0");
	}
}

void SpeakerData::setSpeakerName(QString speakerName)
{
	m_speakerName = speakerName;
}

QString SpeakerData::getSpeakerName()
{
	return (m_speakerName);
}

/* Set frequency for HPF and LPF */
qreal ChannelData::setPFFrequency(int index, qreal freq)
{
//	EQData *pData = m_EQData + index;
	qreal crossFreq;

	if (index == 8)
	{
		/* current EQ is HPF */
		crossFreq = m_EQData[9].getFrequency();
		if (freq > crossFreq)
		{
			freq = crossFreq;
		}
	}
	else if (index == 9)
	{
		/* current EQ is HPF */
		crossFreq = m_EQData[8].getFrequency();
		if (freq < crossFreq)
		{
			freq = crossFreq;
		}
	}
	m_EQData[index].setFrequency(freq);
	return (freq);
}

QByteArray SpeakerData::writeToJSON()
{
	/* We need to restore the EQData after this function */
	ChannelData oldChannelData[3];
	int i;
	int channelCount = getChannelCount();
	for (i = 0; i < channelCount; i++)
	{
		oldChannelData[i] = m_channelData[i];
	}

	QJsonObject mainObject;

	preWriteJSON(mainObject);

	writeJSON_common(mainObject);
	switch (m_speakerType)
	{
	case SPEAKER_TYPE_PASSIVE:
		writeJSON_Passive(mainObject);
		break;
	case SPEAKER_TYPE_BIAMP:
		writeJSON_Biamp(mainObject);
		break;
	case SPEAKER_TYPE_TRIAMP:
		writeJSON_Triamp(mainObject);
		break;
	case SPEAKER_TYPE_SURROUND:
		writeJSON_Surround(mainObject);
		break;
	case SPEAKER_TYPE_SUBWOOFER:
		writeJSON_Subwoofer(mainObject);
		break;
	}

	QByteArray byte_array = QJsonDocument(mainObject).toJson();


	for (i = 0; i < channelCount; i++)
	{
		m_channelData[i] = oldChannelData[i];
	}

	return (byte_array);
}

void SpeakerData::setSpeakerType(SPEAKER_TYPE speakerType)
{
	m_speakerType = speakerType;

	switch (speakerType)
	{
	case SPEAKER_TYPE_BIAMP:
		m_channelCount = 2;
		m_channelNameList << "LF" << "MF";
		break;

	case SPEAKER_TYPE_TRIAMP:
		m_channelCount = 3;
		m_channelNameList << "LF" << "MF" << "HF";
		break;

	case SPEAKER_TYPE_PASSIVE:
	case SPEAKER_TYPE_SURROUND:
	case SPEAKER_TYPE_SUBWOOFER:
		m_channelCount = 1;
		break;
	}
}

int SpeakerData::getChannelCount()
{
	int channelCount = 1;
	switch (m_speakerType)
	{
	case SPEAKER_TYPE_BIAMP:
		channelCount = 2;
		break;
	case SPEAKER_TYPE_TRIAMP:
		channelCount = 3;
		break;
	}
	return (channelCount);
}

void SpeakerData::preWriteJSON(QJsonObject & mainObject)
{
	int channelCount = getChannelCount();
	int index, i;
	QString tabString;
	QString freqString;
//	bool emptyflag;

//	int firCount = 0;
	QJsonObject xover_fir;

	int xover_fir_count = 0;
	for (i = 0; i < channelCount; i++)
	{
#if 0
		if (m_channelData[i].m_enableAll == false)
		{
			m_channelData[i].m_firEnableFlag = false;
			m_channelData[i].m_PEQEnableFlag = false;
			m_channelData[i].m_LPF_IIR_EnableFlag = false;
			m_channelData[i].m_HPF_IIR_EnableFlag = false;
		}
		else
#endif
		{
//			bool peqEnable = false;
			/* We need to judge the PEQ Enable flag here */
			for (index = 0; index < 8; index++)
			{
				/* Even one PEQ is enable, and we should set the PEQEnable to true; */
				if ((m_channelData[i].m_EQData[index].getEnableFlag() == true) && (fabs(m_channelData[i].m_EQData[index].getGain()) > 0.01)) 
				{
//					peqEnable = true;
				}
				else
				{
					m_channelData[i].m_EQData[index].setGain(0);
				}
			}
//			m_channelData[i].m_PEQEnableFlag = peqEnable;
/*
# The following is for FIR-HPF and FIR-LPF
		"XOver_FIR": {
		"HPF_1_TAB" : "10"
		"HPF_1_Frequency" : "2245Hz"
		"HPF_2_TAB" : "10"
		"HPF_2_Frequency" : "2245Hz"
		"HPF_3_TAB" : "10"
		"HPF_3_Frequency" : "2245Hz"
		"LPF_1_TAB" : "10"
		"LPF_1_Frequency" : "2245Hz"
		"LPF_2_TAB" : "10"
		"LPF_2_Frequency" : "2245Hz"
		"LPF_3_TAB" : "10"
		"LPF_3_Frequency" : "2245Hz"
	}		
*/
			if ((m_speakerType == SPEAKER_TYPE_BIAMP) || (m_speakerType == SPEAKER_TYPE_TRIAMP) || (m_speakerType == SPEAKER_TYPE_PASSIVE))
			{
				bool xoverFir = false;
				int xoverFirTab[2] = {0, 0};
				double xoverFirFreq[2] = {0.0, 0.0 };

				if ((m_channelData[i].m_EQData[8].getPFType() == PF_FIR) && (m_channelData[i].m_EQData[8].getEnableFlag() == true))
				{
					xover_fir[QString("HPF_%1_TAB").arg(i)] = QString::number(m_channelData[i].m_EQData[8].getFIRTab());
					xover_fir[QString("HPF_%1_Frequency").arg(i)] = QString::number(m_channelData[i].m_EQData[8].getFrequency(), 'f', 1);
					xoverFir = true;
					xoverFirTab[0] = m_channelData[i].m_EQData[8].getFIRTab();
					xoverFirFreq[0] = m_channelData[i].m_EQData[8].getFrequency();
					xover_fir_count++;
				}

				if ((m_channelData[i].m_EQData[9].getPFType() == PF_FIR) && (m_channelData[i].m_EQData[9].getEnableFlag() == true))
				{
					xover_fir[QString("LPF_%1_TAB").arg(i)] = QString::number(m_channelData[i].m_EQData[9].getFIRTab());
					xover_fir[QString("LPF_%1_Frequency").arg(i)] = QString::number(m_channelData[i].m_EQData[9].getFrequency(), 'f', 1);
					xoverFir = true;
					xoverFirTab[1] = m_channelData[i].m_EQData[9].getFIRTab();
					xoverFirFreq[1] = m_channelData[i].m_EQData[9].getFrequency();
					xover_fir_count++;
				}

				if (xoverFir == true)
				{
					m_channelData[i].m_firEnableFlag = true;
					m_channelData[i].m_strFIR = XOverToFIR(xoverFirTab[0], xoverFirFreq[0], xoverFirTab[1], xoverFirFreq[1]);
				}
				else
				{
					m_channelData[i].m_strFIR = m_channelData[i].m_EQData[10].getUserDefinedFIR();
					m_channelData[i].m_firEnableFlag = m_channelData[i].m_EQData[10].getEnableFlag();
				}
			}

			if (m_channelData[i].m_EQData[8].getEnableFlag() && (m_channelData[i].m_EQData[8].getPFType() == PF_IIR))
			{
				m_channelData[i].m_HPF_IIR_EnableFlag =  true;			
			}
			else
			{
				m_channelData[i].m_HPF_IIR_EnableFlag =  false;			
			}

			if (m_channelData[i].m_EQData[9].getEnableFlag() && (m_channelData[i].m_EQData[9].getPFType() == PF_IIR))
			{
				m_channelData[i].m_LPF_IIR_EnableFlag =  true;			
			}
			else
			{
				m_channelData[i].m_LPF_IIR_EnableFlag =  false;			
			}
		}
	}

	if (xover_fir_count != 0)
	{
		mainObject["XOver_FIR"] = xover_fir;
	}
}

void SpeakerData::readJSON_patch(QJsonObject & mainObject)
{
/*
# The following is for FIR-HPF and FIR-LPF
		"XOver_FIR": {
		"HPF_1_TAB" : "10"
		"HPF_1_Frequency" : "2245Hz"
		"HPF_2_TAB" : "10"
		"HPF_2_Frequency" : "2245Hz"
		"HPF_3_TAB" : "10"
		"HPF_3_Frequency" : "2245Hz"
		"LPF_1_TAB" : "10"
		"LPF_1_Frequency" : "2245Hz"
		"LPF_2_TAB" : "10"
		"LPF_2_Frequency" : "2245Hz"
		"LPF_3_TAB" : "10"
		"LPF_3_Frequency" : "2245Hz"
	}		
*/
	int channelCount = getChannelCount();
	int index;
	QString tabString;
	QString freqString;
	bool emptyflag;
	for (int i = 0; i < channelCount; i++)
	{
		m_channelData[i].m_enableAll = true;

		/* PEQ */
		for (index = 0; index < 8; index++)
		{
			if (m_channelData[i].m_PEQEnableFlag == false)
			{
				m_channelData[i].m_EQData[index].enable(false);
			}
			else
			{
				if (fabs(m_channelData[i].m_EQData[index].getGain()) < 0.01)
				{
					m_channelData[i].m_EQData[index].enable(false);
				}
				else
				{
					m_channelData[i].m_EQData[index].enable(true);
				}
			}
		}

		/* User Defined FIR */
		m_channelData[i].m_EQData[10].enable(m_channelData[i].m_firEnableFlag); 
		m_channelData[i].m_EQData[10].setUserDefinedFIR(m_channelData[i].m_strFIR);

		/* XOVER - HPF */
		QString sv = QString("XOver_FIR/HPF_%1_TAB").arg(i);
//		qDebug() << sv;
		tabString = readObject(mainObject, sv);
//		qDebug() << tabString;

		sv = QString("XOver_FIR/HPF_%1_Frequency").arg(i);
//		qDebug() << sv;
		freqString = readObject(mainObject, sv);
//		qDebug() << freqString;
		emptyflag = tabString.isEmpty() || freqString.isEmpty();
		if (!emptyflag)
		{
			m_channelData[i].m_EQData[8].enable(true);
			m_channelData[i].m_EQData[8].setPFType(PF_FIR);
			m_channelData[i].m_EQData[8].setFIRTab(tabString.toInt());
			m_channelData[i].m_EQData[8].setFrequency(freqString);
			m_channelData[i].m_EQData[10].enable(false);
		}
		else
		{
			m_channelData[i].m_EQData[8].enable(m_channelData[i].m_HPF_IIR_EnableFlag);
		}

		/* XOVER - LPF */
		tabString = readObject(mainObject, QString("XOver_FIR/LPF_%1_TAB").arg(i));
		freqString = readObject(mainObject, QString("XOver_FIR/LPF_%1_Frequency").arg(i));
		emptyflag = tabString.isEmpty() || freqString.isEmpty();
		if (!emptyflag)
		{
			m_channelData[i].m_EQData[9].enable(true);
			m_channelData[i].m_EQData[9].setPFType(PF_FIR);
			m_channelData[i].m_EQData[9].setFIRTab(tabString.toInt());
			m_channelData[i].m_EQData[9].setFrequency(freqString);
			m_channelData[i].m_EQData[10].enable(false);
		}
		else
		{
			m_channelData[i].m_EQData[9].enable(m_channelData[i].m_LPF_IIR_EnableFlag);
		}
	}
}

bool SpeakerData::readJSON_common(QJsonObject & mainObject)
{
	QString frequency_HF[8];
	int speakerType;

	/* We delete the priority: readable and password */
	m_readable = true;
	m_writable = true;
	m_password = "";
	const JSON_STRING_TABLE stringTable[] = 
	{
		{ JSON_ENUM,			"Type"	,								&speakerType,			g_speakerTypeTable },
//		{ JSON_BOOL,			"Readable",								&m_readable,			nullptr,		0  },
//		{ JSON_BOOL,			"Writable",								&m_writable,			nullptr,		0  },
//		{ JSON_STRING,			"Password",								&m_password,			nullptr,		0  },
		{ JSON_STRING,			"Version",								&m_strVersion,			nullptr,		0  },
		{ JSON_STRING,			"Vendor",								&m_strVendor,			nullptr,		0  },
	};

	QString *pStringValue;
	bool *pBoolValue;
	float *pFloatValue;
	int *pIntValue;
	QRegExp rx_float, rx_int;
	QString JSONKey;
	QString value;

	rx_float.setPattern(REGULAR_EXPRESSION_FLOAT); 
	rx_int.setPattern(REGULAR_EXPRESSION_INTEGER); 

	for (int i = 0; i < sizeof(stringTable) / sizeof(JSON_STRING_TABLE); i++)
	{
		QString value = readObject(mainObject, stringTable[i].m_JSONKey);
		if (value == QString("NULL"))
		{
			value = "";
		}

		if (stringTable[i].m_stringID == JSON_STRING)
		{
			pStringValue = (QString *)(stringTable[i].m_pValue);
			*pStringValue = value;
			qDebug() << i << value << " = " << *pStringValue;				
		}
		else if (stringTable[i].m_stringID == JSON_BOOL)
		{
			pBoolValue = (bool *)(stringTable[i].m_pValue);
			/* "On" or "Off" */
			if ((value == "On") || (value == "Yes"))
			{
				*pBoolValue = true;
				qDebug() << i << value << " = "  << "true";				
			}
			else if ((value == "Off") || (value == "No"))
			{
				*pBoolValue = false;
				qDebug() << i << value << " = "  << "false";				
			}
			else
			{
				qDebug() << i << value << "Invalid Bool Value ***************************";				
			}
		}
		else if (stringTable[i].m_stringID == JSON_INT)
		{
			pIntValue = (int *)(stringTable[i].m_pValue);
			// if match all, it should be : rx.setPattern("(^-?\\d+$)"); 
			// Here we just need to match the first digital munber.
			if (rx_int.indexIn(value) != -1) 
			{
				*pIntValue = rx_int.cap(1).toInt();
				qDebug() << i << value << " = "  << *pIntValue;				
			}
			else
			{
				qDebug() << i << value << "Invalid Int Value***********************************";							
			}
		}
		else if (stringTable[i].m_stringID == JSON_FLOAT)
		{
			pFloatValue = (float *)(stringTable[i].m_pValue);
			if (rx_float.indexIn(value) != -1) 
			{
				*pFloatValue = rx_float.cap(1).toFloat();
				qDebug() << i << value << " = " << *pFloatValue;				
			}
			else
			{
				qDebug() << i << value << "Invalid Float Value***********************************";							
			}
		}
		else if (stringTable[i].m_stringID == JSON_ENUM)
		{
			pIntValue = (int *)(stringTable[i].m_pValue);
			*pIntValue = getTableValue(stringTable[i].m_pExplainTable, value);
			qDebug() << i << value << " = " << *pIntValue;						
		}
		else
		{
			qDebug() << "Invalid JSON_ID: " << i << "  = " << stringTable[i].m_stringID;			
		}
	}

	m_speakerType = (SPEAKER_TYPE)speakerType;

#if 0
	m_channelData[0].m_polarityNormalFlag = polarityNormalFlag_0;
	m_channelData[1].m_polarityNormalFlag = polarityNormalFlag_1;
	m_channelData[2].m_polarityNormalFlag = polarityNormalFlag_2;

	for (int i = 0; i < 8; i++)
	{

		m_channelData[2].m_EQData[i].setType((EQType)EQType_HF[i]);
		m_channelData[2].m_EQData[i].setFrequency(frequency_HF[i]);
		m_channelData[2].m_EQData[i].setQ(Q_HF[i]);
		m_channelData[2].m_EQData[i].setGain(Gain_HF[i]);
		m_channelData[2].m_EQData[i].setSlope(Slope_HF[i]);
	}
#endif
	return (true);
}


void SpeakerData::dump()
{
	qDebug() << "Name: " << m_speakerName;
	
	QString speakerType[] = {"Passive", "Bi-amp", "Tri-amp", "Surround", "Subwoofer" };
	if ((m_speakerType >= 0) && (m_speakerType < 5))
	{
		qDebug() << "Type: " << speakerType[(int)m_speakerType];
	}
	else
	{
		qDebug() << "Type: " << "Invalid Type ****************************";
	}

	qDebug() << "Readable: " << m_readable;
	qDebug() << "Writeable: " << m_writable;

	qDebug() << "Channel Count: " << m_channelCount;

#if 0
	for (int i = 0; i < m_channelCount; i++)
	{
		qDebug() <<"";
		if (m_channelCount != 1)
		{
			qDebug() << "Channel " << i << " :" << m_channelNameList[i];
		}
		else
		{
			qDebug() << "Channel " << i ;			
		}

		qDebug() << "======================================================================";
		qDebug() << "PolarityNormalFlag = " << m_channelData[i].m_polarityNormalFlag;
	}
#endif
#if 0
	qDebug() << "Limiter - m_enableFlag: " << m_limiter.m_enableFlag;
	qDebug() << "Limiter - m_autoFlag: " << m_limiter.m_autoFlag;
	qDebug() << "Limiter - m_threshold: " << m_limiter.m_threshold;
	qDebug() << "Limiter - m_attackTime: " << m_limiter.m_attackTime;
	qDebug() << "Limiter - m_holdTime: " << m_limiter.m_holdTime;
	qDebug() << "Limiter - m_release: " << m_limiter.m_release;

	qDebug() << "PolarityNormalFlag: " << m_polarityNormalFlag;
	qDebug() << "Gain: " << m_gain;
	if ((m_speakerType != SPEAKER_TYPE_SURROUND) && (m_speakerType != SPEAKER_TYPE_SUBWOOFER))
	{
		qDebug() << "Delay: " << m_delayInMs;
	}
#endif
}

//static QString s_string[];
bool SpeakerData::readFromJSON(const QString &jsonContent)
{
	bool ret = false;
	QJsonDocument loadDoc(QJsonDocument::fromJson(jsonContent.toUtf8()));
	QJsonObject mainObject = loadDoc.object();

	QString version = readObject(mainObject, "Version");
	if (version != "1.0")
	{
		qDebug() << "Invalid Speaker Version" << version;
	}

	readJSON_common(mainObject);

	if (m_speakerType == SPEAKER_TYPE_TRIAMP)
	{
		ret = readJSON_Triamp(mainObject);
	}
	if (m_speakerType == SPEAKER_TYPE_PASSIVE)
	{
		ret = readJSON_Passive(mainObject);	
	}
	if (m_speakerType == SPEAKER_TYPE_BIAMP)
	{
		ret = readJSON_Biamp(mainObject);		
	}
	if (m_speakerType == SPEAKER_TYPE_SUBWOOFER)
	{
		ret = readJSON_Subwoofer(mainObject);		
	}
	else if (m_speakerType == SPEAKER_TYPE_SURROUND)
	{
		ret = readJSON_Surround(mainObject);		
	}
	else
	{
		qDebug() << "Invalid type: " << m_speakerType;
	}

	readJSON_patch(mainObject);		/* Patch for FIR-HPF / FIR-LPF */
	return ret;
}

bool SpeakerData::readJSON_Triamp(QJsonObject & mainObject)
{
#if 0
	int speakerType;

	EXPLAIN_TABLE eqTypeTable[] = 
	{
		{0, "Bell"	},
		{1,	"Low Shelf"	},
		{2,	"High Shelf"	},
	};
#endif

	int polarityNormalFlag_1, polarityNormalFlag_2, polarityNormalFlag_0;
	QString frequency_HF[8];
	bool firBypass0, firBypass1, firBypass2;
	QString hpfFreqChannel_1, lpfFreqChannel_1, hpfFreqChannel_2, lpfFreqChannel_2, hpfFreqChannel_0, lpfFreqChannel_0;
	int hpfTypeChannel_1, lpfTypeChannel_1, hpfTypeChannel_2, lpfTypeChannel_2, hpfTypeChannel_0, lpfTypeChannel_0;

	const JSON_STRING_TABLE stringTable[] = 
	{
//		{ JSON_STRING,			"Type"	,								&name,							nullptr,			0  },
		{ JSON_FLOAT,			"OutDelay_XYZ/Channel_1_Amount",		&m_channelData[0].m_delayInMs,	nullptr,			0 },
		{ JSON_FLOAT,			"OutDelay_XYZ/Channel_2_Amount",		&m_channelData[1].m_delayInMs,	nullptr,			0 },
		{ JSON_FLOAT,			"OutDelay_XYZ/Channel_3_Amount",		&m_channelData[2].m_delayInMs,	nullptr,			0 },

		/* For OutGain (1 - 3) */
		{ JSON_ENUM,			"OutGain_XYZ/Channel_1_Polarity",		&polarityNormalFlag_0,			g_polarityTable },
		{ JSON_ENUM,			"OutGain_XYZ/Channel_2_Polarity",		&polarityNormalFlag_1,			g_polarityTable},
		{ JSON_ENUM,			"OutGain_XYZ/Channel_3_Polarity",		&polarityNormalFlag_2,			g_polarityTable},
		{ JSON_FLOAT,			"OutGain_XYZ/Channel_1_Gain",			&m_channelData[0].m_gain,		nullptr,			0 },
		{ JSON_FLOAT,			"OutGain_XYZ/Channel_2_Gain",			&m_channelData[1].m_gain,		nullptr,			0 },
		{ JSON_FLOAT,			"OutGain_XYZ/Channel_3_Gain",			&m_channelData[2].m_gain,		nullptr,			0 },

		{ JSON_BOOL,			"OutFir_XYZ/Channel_1_Bypass",			&firBypass0,					nullptr,			0 },
		{ JSON_STRING,			"OutFir_XYZ/Channel_1_Coefficients",	&m_channelData[0].m_strFIR,		nullptr,			0 },
		{ JSON_BOOL,			"OutFir_XYZ/Channel_2_Bypass",			&firBypass1,					nullptr,			0 },
		{ JSON_STRING,			"OutFir_XYZ/Channel_2_Coefficients",	&m_channelData[1].m_strFIR,		nullptr,			0 },
		{ JSON_BOOL,			"OutFir_XYZ/Channel_3_Bypass",			&firBypass2,					nullptr,			0 },
		{ JSON_STRING,			"OutFir_XYZ/Channel_3_Coefficients",	&m_channelData[2].m_strFIR,		nullptr,			0 },

		/* Limiter */
		{ JSON_FLOAT,			"OutLimiter_XYZ/Channel_1_Attack",		&m_channelData[0].m_limiter.m_attackTimeInMs,		nullptr,			0 },
		{ JSON_BOOL,			"OutLimiter_XYZ/Channel_1_Auto",		&m_channelData[0].m_limiter.m_autoFlag,				nullptr,			0 },
		{ JSON_BOOL,			"OutLimiter_XYZ/Channel_1_Enable",		&m_channelData[0].m_limiter.m_enableFlag,			nullptr,			0 },
		{ JSON_FLOAT,				"OutLimiter_XYZ/Channel_1_Hold",		&m_channelData[0].m_limiter.m_holdTimeInMs,			nullptr,			0 },
		{ JSON_INT,				"OutLimiter_XYZ/Channel_1_Release",		&m_channelData[0].m_limiter.m_release,				nullptr,			0 },
		{ JSON_FLOAT,			"OutLimiter_XYZ/Channel_1_Threshold",	&m_channelData[0].m_limiter.m_threshold,			nullptr,			0 },

		{ JSON_FLOAT,			"OutLimiter_XYZ/Channel_2_Attack",		&m_channelData[1].m_limiter.m_attackTimeInMs,		nullptr,			0 },
		{ JSON_BOOL,			"OutLimiter_XYZ/Channel_2_Auto",		&m_channelData[1].m_limiter.m_autoFlag,				nullptr,			0 },
		{ JSON_BOOL,			"OutLimiter_XYZ/Channel_2_Enable",		&m_channelData[1].m_limiter.m_enableFlag,			nullptr,			0 },
		{ JSON_FLOAT,				"OutLimiter_XYZ/Channel_2_Hold",		&m_channelData[1].m_limiter.m_holdTimeInMs,			nullptr,			0 },
		{ JSON_INT,				"OutLimiter_XYZ/Channel_2_Release",		&m_channelData[1].m_limiter.m_release,				nullptr,			0 },
		{ JSON_FLOAT,			"OutLimiter_XYZ/Channel_2_Threshold",	&m_channelData[1].m_limiter.m_threshold,			nullptr,			0 },

		{ JSON_FLOAT,			"OutLimiter_XYZ/Channel_3_Attack",		&m_channelData[2].m_limiter.m_attackTimeInMs,		nullptr,			0 },
		{ JSON_BOOL,			"OutLimiter_XYZ/Channel_3_Auto",		&m_channelData[2].m_limiter.m_autoFlag,				nullptr,			0 },
		{ JSON_BOOL,			"OutLimiter_XYZ/Channel_3_Enable",		&m_channelData[2].m_limiter.m_enableFlag,			nullptr,			0 },
		{ JSON_FLOAT,				"OutLimiter_XYZ/Channel_3_Hold",		&m_channelData[2].m_limiter.m_holdTimeInMs,			nullptr,			0 },
		{ JSON_INT,				"OutLimiter_XYZ/Channel_3_Release",		&m_channelData[2].m_limiter.m_release,				nullptr,			0 },
		{ JSON_FLOAT,			"OutLimiter_XYZ/Channel_3_Threshold",	&m_channelData[2].m_limiter.m_threshold,			nullptr,			0 },

		/* GEQ */
		{ JSON_BOOL,			"OutPeq_XYZ/Channel_1_ParametricEQ",	&m_channelData[0].m_PEQEnableFlag,					nullptr,			0 },
		{ JSON_BOOL,			"OutPeq_XYZ/Channel_2_ParametricEQ",	&m_channelData[1].m_PEQEnableFlag,					nullptr,			0 },
		{ JSON_BOOL,			"OutPeq_XYZ/Channel_3_ParametricEQ",	&m_channelData[2].m_PEQEnableFlag,					nullptr,			0 },

		/* xover */
		{ JSON_BOOL,			"Xover_XYZ/Channel_1_EnableHighPass",	&m_channelData[0].m_HPF_IIR_EnableFlag,					nullptr,			0 },
		{ JSON_BOOL,			"Xover_XYZ/Channel_1_EnableLowPass",	&m_channelData[0].m_LPF_IIR_EnableFlag,					nullptr,			0 },
		{ JSON_BOOL,			"Xover_XYZ/Channel_2_EnableHighPass",	&m_channelData[1].m_HPF_IIR_EnableFlag,					nullptr,			0 },
		{ JSON_BOOL,			"Xover_XYZ/Channel_2_EnableLowPass",	&m_channelData[1].m_LPF_IIR_EnableFlag,					nullptr,			0 },
		{ JSON_BOOL,			"Xover_XYZ/Channel_3_EnableHighPass",	&m_channelData[2].m_HPF_IIR_EnableFlag,					nullptr,			0 },
		{ JSON_BOOL,			"Xover_XYZ/Channel_3_EnableLowPass",	&m_channelData[2].m_LPF_IIR_EnableFlag,					nullptr,			0 },

		{ JSON_STRING,			"Xover_XYZ/Channel_1_HPFrequency",		&hpfFreqChannel_0,									nullptr,			0 },
		{ JSON_STRING,			"Xover_XYZ/Channel_1_LPFrequency",		&lpfFreqChannel_0,									nullptr,			0 },
		{ JSON_STRING,			"Xover_XYZ/Channel_2_HPFrequency",		&hpfFreqChannel_1,									nullptr,			0 },
		{ JSON_STRING,			"Xover_XYZ/Channel_2_LPFrequency",		&lpfFreqChannel_1,									nullptr,			0 },
		{ JSON_STRING,			"Xover_XYZ/Channel_3_HPFrequency",		&hpfFreqChannel_2,									nullptr,			0 },
		{ JSON_STRING,			"Xover_XYZ/Channel_3_LPFrequency",		&lpfFreqChannel_2,									nullptr,			0 },

		{ JSON_ENUM,			"Xover_XYZ/Channel_1_HPType",			&hpfTypeChannel_0,			g_PFIIRTypeTable},
		{ JSON_ENUM,			"Xover_XYZ/Channel_1_LPType",			&lpfTypeChannel_0,			g_PFIIRTypeTable},
		{ JSON_ENUM,			"Xover_XYZ/Channel_2_HPType",			&hpfTypeChannel_1,			g_PFIIRTypeTable},
		{ JSON_ENUM,			"Xover_XYZ/Channel_2_LPType",			&lpfTypeChannel_1,			g_PFIIRTypeTable},
		{ JSON_ENUM,			"Xover_XYZ/Channel_3_HPType",			&hpfTypeChannel_2,			g_PFIIRTypeTable},
		{ JSON_ENUM,			"Xover_XYZ/Channel_3_LPType",			&lpfTypeChannel_2,			g_PFIIRTypeTable},

	};

	QString *pStringValue;
	bool *pBoolValue;
	float *pFloatValue;
	int *pIntValue;
	QRegExp rx_float, rx_int;
	QString JSONKey;
	QString value;

	rx_float.setPattern(REGULAR_EXPRESSION_FLOAT); 
	rx_int.setPattern(REGULAR_EXPRESSION_INTEGER); 

	for (int i = 0; i < sizeof(stringTable) / sizeof(JSON_STRING_TABLE); i++)
	{
		QString value = readObject(mainObject, stringTable[i].m_JSONKey);
		if (value == QString("NULL"))
		{
			value = "";
		}

		if (stringTable[i].m_stringID == JSON_STRING)
		{
			pStringValue = (QString *)(stringTable[i].m_pValue);
			*pStringValue = value;
			qDebug() << i << value << " = " << *pStringValue;				
		}
		else if (stringTable[i].m_stringID == JSON_BOOL)
		{
			pBoolValue = (bool *)(stringTable[i].m_pValue);
			/* "On" or "Off" */
			if ((value == "On") || (value == "Yes"))
			{
				*pBoolValue = true;
				qDebug() << i << value << " = "  << "true";				
			}
			else if ((value == "Off") || (value == "No"))
			{
				*pBoolValue = false;
				qDebug() << i << value << " = "  << "false";				
			}
			else
			{
				qDebug() << i << value << "Invalid Bool Value ***************************";				
			}
		}
		else if (stringTable[i].m_stringID == JSON_INT)
		{
			pIntValue = (int *)(stringTable[i].m_pValue);
			// if match all, it should be : rx.setPattern("(^-?\\d+$)"); 
			// Here we just need to match the first digital munber.
			if (rx_int.indexIn(value) != -1) 
			{
				*pIntValue = rx_int.cap(1).toInt();
				qDebug() << i << value << " = "  << *pIntValue;				
			}
			else
			{
				qDebug() << i << value << "Invalid Int Value***********************************";							
			}
		}
		else if (stringTable[i].m_stringID == JSON_FLOAT)
		{
			pFloatValue = (float *)(stringTable[i].m_pValue);
			if (rx_float.indexIn(value) != -1) 
			{
				*pFloatValue = rx_float.cap(1).toFloat();
				qDebug() << i << value << " = " << *pFloatValue;				
			}
			else
			{
				qDebug() << i << value << "Invalid Float Value***********************************";							
			}
		}
		else if (stringTable[i].m_stringID == JSON_ENUM)
		{
			pIntValue = (int *)(stringTable[i].m_pValue);
			*pIntValue = getTableValue(stringTable[i].m_pExplainTable, value);
			qDebug() << i << value << " = " << *pIntValue;						
		}
		else
		{
			qDebug() << "Invalid JSON_ID: " << i << "  = " << stringTable[i].m_stringID;			
		}
	}

	m_channelData[0].m_polarityNormalFlag = polarityNormalFlag_0;
	m_channelData[1].m_polarityNormalFlag = polarityNormalFlag_1;
	m_channelData[2].m_polarityNormalFlag = polarityNormalFlag_2;

	m_channelData[0].m_firEnableFlag = !firBypass0;
	m_channelData[1].m_firEnableFlag = !firBypass1;
	m_channelData[2].m_firEnableFlag = !firBypass2;

	
	/* Read PEQ */
	for (int channelIndex = 0; channelIndex < 3; channelIndex++)
	{
		QString value;
		for (int bandIndex = 0; bandIndex < 8; bandIndex++)
		{
			value = readObject(mainObject, QString("OutPeq_XYZ/Channel_%1_Band_%2_Frequency").arg(channelIndex + 1).arg(bandIndex + 1)).trimmed();
			m_channelData[channelIndex].m_EQData[bandIndex].setFrequency(value);
			
			value = readObject(mainObject, QString("OutPeq_XYZ/Channel_%1_Band_%2_Gain").arg(channelIndex + 1).arg(bandIndex + 1)).trimmed();
			m_channelData[channelIndex].m_EQData[bandIndex].setGain(readFloat(value));
			
			value = readObject(mainObject, QString("OutPeq_XYZ/Channel_%1_Band_%2_Q").arg(channelIndex + 1).arg(bandIndex + 1)).trimmed();
			m_channelData[channelIndex].m_EQData[bandIndex].setQ(readFloat(value));

			value = readObject(mainObject, QString("OutPeq_XYZ/Channel_%1_Band_%2_Slope").arg(channelIndex + 1).arg(bandIndex + 1)).trimmed();
			m_channelData[channelIndex].m_EQData[bandIndex].setSlope(readFloat(value));

			value = readObject(mainObject, QString("OutPeq_XYZ/Channel_%1_Band_%2_Type").arg(channelIndex + 1).arg(bandIndex + 1)).trimmed();
			EQType type = (EQType)getTableValue(g_PEQName, value);
			m_channelData[channelIndex].m_EQData[bandIndex].setType(type);
		}
	}

	/* Read xover */
	m_channelData[0].m_EQData[8].setFrequency(hpfFreqChannel_0);
	m_channelData[0].m_EQData[9].setFrequency(lpfFreqChannel_0);
	m_channelData[1].m_EQData[8].setFrequency(hpfFreqChannel_1);
	m_channelData[1].m_EQData[9].setFrequency(lpfFreqChannel_1);
	m_channelData[2].m_EQData[8].setFrequency(hpfFreqChannel_2);
	m_channelData[2].m_EQData[9].setFrequency(lpfFreqChannel_2);

	m_channelData[0].m_EQData[8].setPFType(PF_IIR);
	m_channelData[0].m_EQData[8].setPFIIRType((PF_IIR_Type)hpfTypeChannel_0);

	m_channelData[0].m_EQData[9].setPFType(PF_IIR);
	m_channelData[0].m_EQData[9].setPFIIRType((PF_IIR_Type)lpfTypeChannel_0);

	m_channelData[1].m_EQData[8].setPFType(PF_IIR);
	m_channelData[1].m_EQData[8].setPFIIRType((PF_IIR_Type)hpfTypeChannel_1);

	m_channelData[1].m_EQData[9].setPFType(PF_IIR);
	m_channelData[1].m_EQData[9].setPFIIRType((PF_IIR_Type)lpfTypeChannel_1);

	m_channelData[2].m_EQData[8].setPFType(PF_IIR);
	m_channelData[2].m_EQData[8].setPFIIRType((PF_IIR_Type)hpfTypeChannel_2);

	m_channelData[2].m_EQData[9].setPFType(PF_IIR);
	m_channelData[2].m_EQData[9].setPFIIRType((PF_IIR_Type)lpfTypeChannel_2);

	return (true);
}

bool SpeakerData::readJSON_Passive(QJsonObject & mainObject)
{
	int polarityNormalFlag_0;
	bool firBypass0;

	QString hpfFreqChannel_0, lpfFreqChannel_0;
	int hpfTypeChannel_0, lpfTypeChannel_0;

	const JSON_STRING_TABLE stringTable[] = 
	{
//		{ JSON_STRING,			"Type"	,								&name,							nullptr,			0  },

		/* For OutDelay 2 */
		{ JSON_FLOAT,			"OutDelay_XYZ/Channel_2_Amount",		&m_channelData[0].m_delayInMs,	nullptr,			0 },
	
		/* For OutGain 2 */
		{ JSON_ENUM,			"OutGain_XYZ/Channel_2_Polarity",		&polarityNormalFlag_0,			g_polarityTable},
		{ JSON_FLOAT,			"OutGain_XYZ/Channel_2_Gain",			&m_channelData[0].m_gain,		nullptr,			0 },

		/* For Fir */
		{ JSON_BOOL,			"OutFir_XYZ/Channel_2_Bypass",			&firBypass0,					nullptr,			0 },
		{ JSON_STRING,			"OutFir_XYZ/Channel_2_Coefficients",	&m_channelData[0].m_strFIR,		nullptr,			0 },

		/* Limiter */
		{ JSON_FLOAT,			"OutLimiter_XYZ/Channel_2_Attack",		&m_channelData[0].m_limiter.m_attackTimeInMs,		nullptr,			0 },
		{ JSON_BOOL,			"OutLimiter_XYZ/Channel_2_Auto",		&m_channelData[0].m_limiter.m_autoFlag,				nullptr,			0 },
		{ JSON_BOOL,			"OutLimiter_XYZ/Channel_2_Enable",		&m_channelData[0].m_limiter.m_enableFlag,			nullptr,			0 },
		{ JSON_FLOAT,				"OutLimiter_XYZ/Channel_2_Hold",		&m_channelData[0].m_limiter.m_holdTimeInMs,			nullptr,			0 },
		{ JSON_INT,				"OutLimiter_XYZ/Channel_2_Release",		&m_channelData[0].m_limiter.m_release,				nullptr,			0 },
		{ JSON_FLOAT,			"OutLimiter_XYZ/Channel_2_Threshold",	&m_channelData[0].m_limiter.m_threshold,			nullptr,			0 },

		/* GEQ */
		{ JSON_BOOL,			"OutPeq_XYZ/Channel_2_ParametricEQ",	&m_channelData[0].m_PEQEnableFlag,					nullptr,			0 },

		/* xover */
		{ JSON_BOOL,			"Xover_XYZ/Channel_2_EnableHighPass",	&m_channelData[0].m_HPF_IIR_EnableFlag,					nullptr,			0 },
		{ JSON_BOOL,			"Xover_XYZ/Channel_2_EnableLowPass",	&m_channelData[0].m_LPF_IIR_EnableFlag,					nullptr,			0 },

		{ JSON_STRING,			"Xover_XYZ/Channel_2_HPFrequency",		&hpfFreqChannel_0,									nullptr,			0 },
		{ JSON_STRING,			"Xover_XYZ/Channel_2_LPFrequency",		&lpfFreqChannel_0,									nullptr,			0 },

		{ JSON_ENUM,			"Xover_XYZ/Channel_2_HPType",			&hpfTypeChannel_0,			g_PFIIRTypeTable},
		{ JSON_ENUM,			"Xover_XYZ/Channel_2_LPType",			&lpfTypeChannel_0,			g_PFIIRTypeTable },
	};

	QString *pStringValue;
	bool *pBoolValue;
	float *pFloatValue;
	int *pIntValue;
	QRegExp rx_float, rx_int;
	QString JSONKey;
	QString value;

	rx_float.setPattern(REGULAR_EXPRESSION_FLOAT); 
	rx_int.setPattern(REGULAR_EXPRESSION_INTEGER); 

	for (int i = 0; i < sizeof(stringTable) / sizeof(JSON_STRING_TABLE); i++)
	{
		QString value = readObject(mainObject, stringTable[i].m_JSONKey);
		if (value == QString("NULL"))
		{
			value = "";
		}

		if (stringTable[i].m_stringID == JSON_STRING)
		{
			pStringValue = (QString *)(stringTable[i].m_pValue);
			*pStringValue = value;
			qDebug() << i << value << " = " << *pStringValue;				
		}
		else if (stringTable[i].m_stringID == JSON_BOOL)
		{
			pBoolValue = (bool *)(stringTable[i].m_pValue);
			/* "On" or "Off" */
			if ((value == "On") || (value == "Yes"))
			{
				*pBoolValue = true;
				qDebug() << i << value << " = "  << "true";				
			}
			else if ((value == "Off") || (value == "No"))
			{
				*pBoolValue = false;
				qDebug() << i << value << " = "  << "false";				
			}
			else
			{
				qDebug() << i << value << "Invalid Bool Value ***************************";				
			}
		}
		else if (stringTable[i].m_stringID == JSON_INT)
		{
			pIntValue = (int *)(stringTable[i].m_pValue);
			// if match all, it should be : rx.setPattern("(^-?\\d+$)"); 
			// Here we just need to match the first digital munber.
			if (rx_int.indexIn(value) != -1) 
			{
				*pIntValue = rx_int.cap(1).toInt();
				qDebug() << i << value << " = "  << *pIntValue;				
			}
			else
			{
				qDebug() << i << value << "Invalid Int Value***********************************";							
			}
		}
		else if (stringTable[i].m_stringID == JSON_FLOAT)
		{
			pFloatValue = (float *)(stringTable[i].m_pValue);
			if (rx_float.indexIn(value) != -1) 
			{
				*pFloatValue = rx_float.cap(1).toFloat();
				qDebug() << i << value << " = " << *pFloatValue;				
			}
			else
			{
				qDebug() << i << value << "Invalid Float Value***********************************";							
			}
		}
		else if (stringTable[i].m_stringID == JSON_ENUM)
		{
			pIntValue = (int *)(stringTable[i].m_pValue);
			*pIntValue = getTableValue(stringTable[i].m_pExplainTable, value);
			qDebug() << i << value << " = " << *pIntValue;						
		}
		else
		{
			qDebug() << "Invalid JSON_ID: " << i << "  = " << stringTable[i].m_stringID;			
		}
	}

	m_channelData[0].m_polarityNormalFlag = polarityNormalFlag_0;
	m_channelData[0].m_firEnableFlag = !firBypass0;

	/* Read PEQ */
	int channelIndex = 1;
	{
		QString value;
		for (int bandIndex = 0; bandIndex < 8; bandIndex++)
		{
			value = readObject(mainObject, QString("OutPeq_XYZ/Channel_%1_Band_%2_Frequency").arg(channelIndex + 1).arg(bandIndex + 1)).trimmed();
			m_channelData[0].m_EQData[bandIndex].setFrequency(value);
			
			value = readObject(mainObject, QString("OutPeq_XYZ/Channel_%1_Band_%2_Gain").arg(channelIndex + 1).arg(bandIndex + 1)).trimmed();
			m_channelData[0].m_EQData[bandIndex].setGain(readFloat(value));
			
			value = readObject(mainObject, QString("OutPeq_XYZ/Channel_%1_Band_%2_Q").arg(channelIndex + 1).arg(bandIndex + 1)).trimmed();
			m_channelData[0].m_EQData[bandIndex].setQ(readFloat(value));

			value = readObject(mainObject, QString("OutPeq_XYZ/Channel_%1_Band_%2_Slope").arg(channelIndex + 1).arg(bandIndex + 1)).trimmed();
			m_channelData[0].m_EQData[bandIndex].setSlope(readFloat(value));

			value = readObject(mainObject, QString("OutPeq_XYZ/Channel_%1_Band_%2_Type").arg(channelIndex + 1).arg(bandIndex + 1)).trimmed();
			EQType type = (EQType)getTableValue(g_PEQName, value);
			m_channelData[0].m_EQData[bandIndex].setType(type);
		}
	}

		
	/* Read xover */
	m_channelData[0].m_EQData[8].setFrequency(hpfFreqChannel_0);
	m_channelData[0].m_EQData[9].setFrequency(lpfFreqChannel_0);

	m_channelData[0].m_EQData[8].setPFType(PF_IIR);
	m_channelData[0].m_EQData[8].setPFIIRType((PF_IIR_Type)hpfTypeChannel_0);

	m_channelData[0].m_EQData[9].setPFType(PF_IIR);
	m_channelData[0].m_EQData[9].setPFIIRType((PF_IIR_Type)lpfTypeChannel_0);
	return (true);
}

bool SpeakerData::readJSON_Biamp(QJsonObject & mainObject)
{
	int polarityNormalFlag_0, polarityNormalFlag_1;
	bool firBypass0, firBypass1;

	QString hpfFreqChannel_1, lpfFreqChannel_1, hpfFreqChannel_0, lpfFreqChannel_0;
	int hpfTypeChannel_1, lpfTypeChannel_1, hpfTypeChannel_0, lpfTypeChannel_0;

	const JSON_STRING_TABLE stringTable[] = 
	{
//		{ JSON_STRING,			"Type"	,								&name,							nullptr,			0  },
		{ JSON_FLOAT,			"OutDelay_XYZ/Channel_2_Amount",		&m_channelData[0].m_delayInMs,	nullptr,			0 },
		{ JSON_FLOAT,			"OutDelay_XYZ/Channel_3_Amount",		&m_channelData[1].m_delayInMs,	nullptr,			0 },

		/* For OutGain (2 - 3) */
		{ JSON_ENUM,			"OutGain_XYZ/Channel_2_Polarity",		&polarityNormalFlag_0,			g_polarityTable },
		{ JSON_ENUM,			"OutGain_XYZ/Channel_3_Polarity",		&polarityNormalFlag_1,			g_polarityTable},
		{ JSON_FLOAT,			"OutGain_XYZ/Channel_2_Gain",			&m_channelData[0].m_gain,		nullptr,			0 },
		{ JSON_FLOAT,			"OutGain_XYZ/Channel_3_Gain",			&m_channelData[1].m_gain,		nullptr,			0 },

		/* For Fir */
		{ JSON_BOOL,			"OutFir_XYZ/Channel_2_Bypass",			&firBypass0,					nullptr,			0 },
		{ JSON_STRING,			"OutFir_XYZ/Channel_2_Coefficients",	&m_channelData[0].m_strFIR,		nullptr,			0 },
		{ JSON_BOOL,			"OutFir_XYZ/Channel_3_Bypass",			&firBypass1,					nullptr,			0 },
		{ JSON_STRING,			"OutFir_XYZ/Channel_3_Coefficients",	&m_channelData[1].m_strFIR,		nullptr,			0 },

		/* Limiter */
		{ JSON_FLOAT,			"OutLimiter_XYZ/Channel_2_Attack",		&m_channelData[0].m_limiter.m_attackTimeInMs,		nullptr,			0 },
		{ JSON_BOOL,			"OutLimiter_XYZ/Channel_2_Auto",		&m_channelData[0].m_limiter.m_autoFlag,				nullptr,			0 },
		{ JSON_BOOL,			"OutLimiter_XYZ/Channel_2_Enable",		&m_channelData[0].m_limiter.m_enableFlag,			nullptr,			0 },
		{ JSON_FLOAT,				"OutLimiter_XYZ/Channel_2_Hold",		&m_channelData[0].m_limiter.m_holdTimeInMs,			nullptr,			0 },
		{ JSON_INT,				"OutLimiter_XYZ/Channel_2_Release",		&m_channelData[0].m_limiter.m_release,				nullptr,			0 },
		{ JSON_FLOAT,			"OutLimiter_XYZ/Channel_2_Threshold",	&m_channelData[0].m_limiter.m_threshold,			nullptr,			0 },

		{ JSON_FLOAT,			"OutLimiter_XYZ/Channel_3_Attack",		&m_channelData[1].m_limiter.m_attackTimeInMs,		nullptr,			0 },
		{ JSON_BOOL,			"OutLimiter_XYZ/Channel_3_Auto",		&m_channelData[1].m_limiter.m_autoFlag,				nullptr,			0 },
		{ JSON_BOOL,			"OutLimiter_XYZ/Channel_3_Enable",		&m_channelData[1].m_limiter.m_enableFlag,			nullptr,			0 },
		{ JSON_FLOAT,				"OutLimiter_XYZ/Channel_3_Hold",		&m_channelData[1].m_limiter.m_holdTimeInMs,			nullptr,			0 },
		{ JSON_INT,				"OutLimiter_XYZ/Channel_3_Release",		&m_channelData[1].m_limiter.m_release,				nullptr,			0 },
		{ JSON_FLOAT,			"OutLimiter_XYZ/Channel_3_Threshold",	&m_channelData[1].m_limiter.m_threshold,			nullptr,			0 },

		/* GEQ */
		{ JSON_BOOL,			"OutPeq_XYZ/Channel_2_ParametricEQ",	&m_channelData[0].m_PEQEnableFlag,					nullptr,			0 },
		{ JSON_BOOL,			"OutPeq_XYZ/Channel_3_ParametricEQ",	&m_channelData[1].m_PEQEnableFlag,					nullptr,			0 },

		/* xover */
		{ JSON_BOOL,			"Xover_XYZ/Channel_2_EnableHighPass",	&m_channelData[0].m_HPF_IIR_EnableFlag,					nullptr,			0 },
		{ JSON_BOOL,			"Xover_XYZ/Channel_2_EnableLowPass",	&m_channelData[0].m_LPF_IIR_EnableFlag,					nullptr,			0 },
		{ JSON_BOOL,			"Xover_XYZ/Channel_3_EnableHighPass",	&m_channelData[1].m_HPF_IIR_EnableFlag,					nullptr,			0 },
		{ JSON_BOOL,			"Xover_XYZ/Channel_3_EnableLowPass",	&m_channelData[1].m_LPF_IIR_EnableFlag,					nullptr,			0 },

		{ JSON_STRING,			"Xover_XYZ/Channel_2_HPFrequency",		&hpfFreqChannel_0,									nullptr,			0 },
		{ JSON_STRING,			"Xover_XYZ/Channel_2_LPFrequency",		&lpfFreqChannel_0,									nullptr,			0 },
		{ JSON_STRING,			"Xover_XYZ/Channel_3_HPFrequency",		&hpfFreqChannel_1,									nullptr,			0 },
		{ JSON_STRING,			"Xover_XYZ/Channel_3_LPFrequency",		&lpfFreqChannel_1,									nullptr,			0 },

		{ JSON_ENUM,			"Xover_XYZ/Channel_2_HPType",			&hpfTypeChannel_0,			g_PFIIRTypeTable },
		{ JSON_ENUM,			"Xover_XYZ/Channel_2_LPType",			&lpfTypeChannel_0,			g_PFIIRTypeTable },
		{ JSON_ENUM,			"Xover_XYZ/Channel_3_HPType",			&hpfTypeChannel_1,			g_PFIIRTypeTable },
		{ JSON_ENUM,			"Xover_XYZ/Channel_3_LPType",			&lpfTypeChannel_1,			g_PFIIRTypeTable },
	};

	QString *pStringValue;
	bool *pBoolValue;
	float *pFloatValue;
	int *pIntValue;
	QRegExp rx_float, rx_int;
	QString JSONKey;
	QString value;

	rx_float.setPattern(REGULAR_EXPRESSION_FLOAT); 
	rx_int.setPattern(REGULAR_EXPRESSION_INTEGER); 

	for (int i = 0; i < sizeof(stringTable) / sizeof(JSON_STRING_TABLE); i++)
	{
		QString value = readObject(mainObject, stringTable[i].m_JSONKey);
		if (value == QString("NULL"))
		{
			value = "";
		}

		if (stringTable[i].m_stringID == JSON_STRING)
		{
			pStringValue = (QString *)(stringTable[i].m_pValue);
			*pStringValue = value;
			qDebug() << i << value << " = " << *pStringValue;				
		}
		else if (stringTable[i].m_stringID == JSON_BOOL)
		{
			pBoolValue = (bool *)(stringTable[i].m_pValue);
			/* "On" or "Off" */
			if ((value == "On") || (value == "Yes"))
			{
				*pBoolValue = true;
				qDebug() << i << value << " = "  << "true";				
			}
			else if ((value == "Off") || (value == "No"))
			{
				*pBoolValue = false;
				qDebug() << i << value << " = "  << "false";				
			}
			else
			{
				qDebug() << i << value << "Invalid Bool Value ***************************";				
			}
		}
		else if (stringTable[i].m_stringID == JSON_INT)
		{
			pIntValue = (int *)(stringTable[i].m_pValue);
			// if match all, it should be : rx.setPattern("(^-?\\d+$)"); 
			// Here we just need to match the first digital munber.
			if (rx_int.indexIn(value) != -1) 
			{
				*pIntValue = rx_int.cap(1).toInt();
				qDebug() << i << value << " = "  << *pIntValue;				
			}
			else
			{
				qDebug() << i << value << "Invalid Int Value***********************************";							
			}
		}
		else if (stringTable[i].m_stringID == JSON_FLOAT)
		{
			pFloatValue = (float *)(stringTable[i].m_pValue);
			if (rx_float.indexIn(value) != -1) 
			{
				*pFloatValue = rx_float.cap(1).toFloat();
				qDebug() << i << value << " = " << *pFloatValue;				
			}
			else
			{
				qDebug() << i << value << "Invalid Float Value***********************************";							
			}
		}
		else if (stringTable[i].m_stringID == JSON_ENUM)
		{
			pIntValue = (int *)(stringTable[i].m_pValue);
			*pIntValue = getTableValue(stringTable[i].m_pExplainTable, value);
			qDebug() << i << value << " = " << *pIntValue;						
		}
		else
		{
			qDebug() << "Invalid JSON_ID: " << i << "  = " << stringTable[i].m_stringID;			
		}
	}

	m_channelData[0].m_polarityNormalFlag = polarityNormalFlag_0;
	m_channelData[1].m_polarityNormalFlag = polarityNormalFlag_1;

	m_channelData[0].m_firEnableFlag = !firBypass0;
	m_channelData[1].m_firEnableFlag = !firBypass1;

	
	/* Read PEQ */
	for (int channelIndex = 0; channelIndex < 2; channelIndex++)
	{
		QString value;
		for (int bandIndex = 0; bandIndex < 8; bandIndex++)
		{
			value = readObject(mainObject, QString("OutPeq_XYZ/Channel_%1_Band_%2_Frequency").arg(channelIndex + 2).arg(bandIndex + 1)).trimmed();
			m_channelData[channelIndex].m_EQData[bandIndex].setFrequency(value);
			
			value = readObject(mainObject, QString("OutPeq_XYZ/Channel_%1_Band_%2_Gain").arg(channelIndex + 2).arg(bandIndex + 1)).trimmed();
			m_channelData[channelIndex].m_EQData[bandIndex].setGain(readFloat(value));
			
			value = readObject(mainObject, QString("OutPeq_XYZ/Channel_%1_Band_%2_Q").arg(channelIndex + 2).arg(bandIndex + 1)).trimmed();
			m_channelData[channelIndex].m_EQData[bandIndex].setQ(readFloat(value));

			value = readObject(mainObject, QString("OutPeq_XYZ/Channel_%1_Band_%2_Slope").arg(channelIndex + 2).arg(bandIndex + 1)).trimmed();
			m_channelData[channelIndex].m_EQData[bandIndex].setSlope(readFloat(value));

			value = readObject(mainObject, QString("OutPeq_XYZ/Channel_%1_Band_%2_Type").arg(channelIndex + 2).arg(bandIndex + 1)).trimmed();
			EQType type = (EQType)getTableValue(g_PEQName, value);
			m_channelData[channelIndex].m_EQData[bandIndex].setType(type);
		}
	}

		
	/* Read xover */
	m_channelData[0].m_EQData[8].setFrequency(hpfFreqChannel_0);
	m_channelData[0].m_EQData[9].setFrequency(lpfFreqChannel_0);
	m_channelData[1].m_EQData[8].setFrequency(hpfFreqChannel_1);
	m_channelData[1].m_EQData[9].setFrequency(lpfFreqChannel_1);

	m_channelData[0].m_EQData[8].setPFType(PF_IIR);
	m_channelData[0].m_EQData[8].setPFIIRType((PF_IIR_Type)hpfTypeChannel_0);

	m_channelData[0].m_EQData[9].setPFType(PF_IIR);
	m_channelData[0].m_EQData[9].setPFIIRType((PF_IIR_Type)lpfTypeChannel_0);

	m_channelData[1].m_EQData[8].setPFType(PF_IIR);
	m_channelData[1].m_EQData[8].setPFIIRType((PF_IIR_Type)hpfTypeChannel_1);

	m_channelData[1].m_EQData[9].setPFType(PF_IIR);
	m_channelData[1].m_EQData[9].setPFIIRType((PF_IIR_Type)lpfTypeChannel_1);

	return (true);
}

bool SpeakerData::readJSON_Subwoofer(QJsonObject & mainObject)
{
	int polarityNormalFlag_0;
	
	QString hpfFreqChannel_0, lpfFreqChannel_0;
	int hpfTypeChannel_0, lpfTypeChannel_0;

	const JSON_STRING_TABLE stringTable[] = 
	{
//		{ JSON_STRING,			"Type"	,								&name,							nullptr,			0  },

		/* For OutGain  */
		{ JSON_FLOAT,			"OutGain_MTX/Channel_XYZ_Gain",			&m_channelData[0].m_gain,		nullptr,			0 },
		{ JSON_ENUM,			"OutGain_MTX/Channel_XYZ_Polarity",		&polarityNormalFlag_0,			g_polarityTable },

		/* Limiter */
		{ JSON_FLOAT,			"OutLimiter_MTX/Channel_XYZ_Attack",	&m_channelData[0].m_limiter.m_attackTimeInMs,		nullptr,			0 },
		{ JSON_BOOL,			"OutLimiter_MTX/Channel_XYZ_Auto",		&m_channelData[0].m_limiter.m_autoFlag,				nullptr,			0 },
		{ JSON_BOOL,			"OutLimiter_MTX/Channel_XYZ_Enable",	&m_channelData[0].m_limiter.m_enableFlag,			nullptr,			0 },
		{ JSON_FLOAT,				"OutLimiter_MTX/Channel_XYZ_Hold",		&m_channelData[0].m_limiter.m_holdTimeInMs,			nullptr,			0 },
		{ JSON_INT,				"OutLimiter_MTX/Channel_XYZ_Release",	&m_channelData[0].m_limiter.m_release,				nullptr,			0 },
		{ JSON_FLOAT,			"OutLimiter_MTX/Channel_XYZ_Threshold",	&m_channelData[0].m_limiter.m_threshold,			nullptr,			0 },

		/* GEQ */
		{ JSON_BOOL,			"OutPeq_MTX/Channel_XYZ_ParametricEQ",	&m_channelData[0].m_PEQEnableFlag,					nullptr,			0 },

		/* xover */
		{ JSON_BOOL,			"Xover_MTX/Channel_XYZ_EnableHighPass",	&m_channelData[0].m_HPF_IIR_EnableFlag,					nullptr,			0 },
		{ JSON_BOOL,			"Xover_MTX/Channel_XYZ_EnableLowPass",	&m_channelData[0].m_LPF_IIR_EnableFlag,					nullptr,			0 },

		{ JSON_STRING,			"Xover_MTX/Channel_XYZ_HPFrequency",	&hpfFreqChannel_0,									nullptr,			0 },
		{ JSON_STRING,			"Xover_MTX/Channel_XYZ_LPFrequency",	&lpfFreqChannel_0,									nullptr,			0 },

		{ JSON_ENUM,			"Xover_MTX/Channel_XYZ_HPType",			&hpfTypeChannel_0,			g_PFIIRTypeTable },
		{ JSON_ENUM,			"Xover_MTX/Channel_XYZ_LPType",			&lpfTypeChannel_0,			g_PFIIRTypeTable },

	};

	QString *pStringValue;
	bool *pBoolValue;
	float *pFloatValue;
	int *pIntValue;
	QRegExp rx_float, rx_int;
	QString JSONKey;
	QString value;

	rx_float.setPattern(REGULAR_EXPRESSION_FLOAT); 
	rx_int.setPattern(REGULAR_EXPRESSION_INTEGER); 

	for (int i = 0; i < sizeof(stringTable) / sizeof(JSON_STRING_TABLE); i++)
	{
		QString value = readObject(mainObject, stringTable[i].m_JSONKey);
		if (value == QString("NULL"))
		{
			value = "";
		}

		if (stringTable[i].m_stringID == JSON_STRING)
		{
			pStringValue = (QString *)(stringTable[i].m_pValue);
			*pStringValue = value;
			qDebug() << i << value << " = " << *pStringValue;				
		}
		else if (stringTable[i].m_stringID == JSON_BOOL)
		{
			pBoolValue = (bool *)(stringTable[i].m_pValue);
			/* "On" or "Off" */
			if ((value == "On") || (value == "Yes"))
			{
				*pBoolValue = true;
				qDebug() << i << value << " = "  << "true";				
			}
			else if ((value == "Off") || (value == "No"))
			{
				*pBoolValue = false;
				qDebug() << i << value << " = "  << "false";				
			}
			else
			{
				qDebug() << i << value << "Invalid Bool Value ***************************";				
			}
		}
		else if (stringTable[i].m_stringID == JSON_INT)
		{
			pIntValue = (int *)(stringTable[i].m_pValue);
			// if match all, it should be : rx.setPattern("(^-?\\d+$)"); 
			// Here we just need to match the first digital munber.
			if (rx_int.indexIn(value) != -1) 
			{
				*pIntValue = rx_int.cap(1).toInt();
				qDebug() << i << value << " = "  << *pIntValue;				
			}
			else
			{
				qDebug() << i << value << "Invalid Int Value***********************************";							
			}
		}
		else if (stringTable[i].m_stringID == JSON_FLOAT)
		{
			pFloatValue = (float *)(stringTable[i].m_pValue);
			if (rx_float.indexIn(value) != -1) 
			{
				*pFloatValue = rx_float.cap(1).toFloat();
				qDebug() << i << value << " = " << *pFloatValue;				
			}
			else
			{
				qDebug() << i << value << "Invalid Float Value***********************************";							
			}
		}
		else if (stringTable[i].m_stringID == JSON_ENUM)
		{
			pIntValue = (int *)(stringTable[i].m_pValue);
			*pIntValue = getTableValue(stringTable[i].m_pExplainTable, value);
			qDebug() << i << value << " = " << *pIntValue;						
		}
		else
		{
			qDebug() << "Invalid JSON_ID: " << i << "  = " << stringTable[i].m_stringID;			
		}
	}

	m_channelData[0].m_polarityNormalFlag = polarityNormalFlag_0;

	/* 8 PEQs */
	for (int bandIndex = 0; bandIndex < 8; bandIndex++)
	{
		value = readObject(mainObject, QString("OutPeq_MTX/Channel_XYZ_Band_%1_Frequency").arg(bandIndex + 1)).trimmed();
		m_channelData[0].m_EQData[bandIndex].setFrequency(value);
		
		value = readObject(mainObject, QString("OutPeq_MTX/Channel_XYZ_Band_%1_Gain").arg(bandIndex + 1)).trimmed();
		m_channelData[0].m_EQData[bandIndex].setGain(readFloat(value));
		
		value = readObject(mainObject, QString("OutPeq_MTX/Channel_XYZ_Band_%1_Q").arg(bandIndex + 1)).trimmed();
		m_channelData[0].m_EQData[bandIndex].setQ(readFloat(value));

		value = readObject(mainObject, QString("OutPeq_MTX/Channel_XYZ_Band_%1_Slope").arg(bandIndex + 1)).trimmed();
		m_channelData[0].m_EQData[bandIndex].setSlope(readFloat(value));

		value = readObject(mainObject, QString("OutPeq_MTX/Channel_XYZ_Band_%1_Type").arg(bandIndex + 1)).trimmed();
		EQType type = (EQType)getTableValue(g_PEQName, value);
		m_channelData[0].m_EQData[bandIndex].setType(type);
	}

	/* Read xover */
	m_channelData[0].m_EQData[8].setFrequency(hpfFreqChannel_0);
	m_channelData[0].m_EQData[9].setFrequency(lpfFreqChannel_0);

	m_channelData[0].m_EQData[8].setPFType(PF_IIR);
	m_channelData[0].m_EQData[8].setPFIIRType((PF_IIR_Type)hpfTypeChannel_0);

	m_channelData[0].m_EQData[9].setPFType(PF_IIR);
	m_channelData[0].m_EQData[9].setPFIIRType((PF_IIR_Type)lpfTypeChannel_0);

	return (true);
}

bool SpeakerData::readJSON_Surround(QJsonObject & mainObject)
{
	int polarityNormalFlag_0;
	QString hpfFreqChannel_0, lpfFreqChannel_0;
	int hpfTypeChannel_0, lpfTypeChannel_0;

	const JSON_STRING_TABLE stringTable[] = 
	{
//		{ JSON_STRING,			"Type"	,								&name,							nullptr,			0  },

		/* For OutGain  */
		{ JSON_FLOAT,			"OutGain_MTX/Channel_XYZ_Gain",			&m_channelData[0].m_gain,		nullptr,			0 },
		{ JSON_ENUM,			"OutGain_MTX/Channel_XYZ_Polarity",		&polarityNormalFlag_0,			g_polarityTable },

		/* Limiter */
		{ JSON_FLOAT,			"OutLimiter_MTX/Channel_XYZ_Attack",	&m_channelData[0].m_limiter.m_attackTimeInMs,		nullptr,			0 },
		{ JSON_BOOL,			"OutLimiter_MTX/Channel_XYZ_Auto",		&m_channelData[0].m_limiter.m_autoFlag,				nullptr,			0 },
		{ JSON_BOOL,			"OutLimiter_MTX/Channel_XYZ_Enable",	&m_channelData[0].m_limiter.m_enableFlag,			nullptr,			0 },
		{ JSON_FLOAT,				"OutLimiter_MTX/Channel_XYZ_Hold",		&m_channelData[0].m_limiter.m_holdTimeInMs,			nullptr,			0 },
		{ JSON_INT,				"OutLimiter_MTX/Channel_XYZ_Release",	&m_channelData[0].m_limiter.m_release,				nullptr,			0 },
		{ JSON_FLOAT,			"OutLimiter_MTX/Channel_XYZ_Threshold",	&m_channelData[0].m_limiter.m_threshold,			nullptr,			0 },

		/* GEQ */
		{ JSON_BOOL,			"OutPeq_MTX/Channel_XYZ_ParametricEQ",	&m_channelData[0].m_PEQEnableFlag,					nullptr,			0 },

		/* xover */
		{ JSON_BOOL,			"Xover_MTX/Channel_XYZ_EnableHighPass",	&m_channelData[0].m_HPF_IIR_EnableFlag,					nullptr,			0 },
		{ JSON_BOOL,			"Xover_MTX/Channel_XYZ_EnableLowPass",	&m_channelData[0].m_LPF_IIR_EnableFlag,					nullptr,			0 },

		{ JSON_STRING,			"Xover_MTX/Channel_XYZ_HPFrequency",	&hpfFreqChannel_0,									nullptr,			0 },
		{ JSON_STRING,			"Xover_MTX/Channel_XYZ_LPFrequency",	&lpfFreqChannel_0,									nullptr,			0 },

		{ JSON_ENUM,			"Xover_MTX/Channel_XYZ_HPType",			&hpfTypeChannel_0,			g_PFIIRTypeTable },
		{ JSON_ENUM,			"Xover_MTX/Channel_XYZ_LPType",			&lpfTypeChannel_0,			g_PFIIRTypeTable },
	};

	QString *pStringValue;
	bool *pBoolValue;
	float *pFloatValue;
	int *pIntValue;
	QRegExp rx_float, rx_int;
	QString JSONKey;
	QString value;

	rx_float.setPattern(REGULAR_EXPRESSION_FLOAT); 
	rx_int.setPattern(REGULAR_EXPRESSION_INTEGER); 

	for (int i = 0; i < sizeof(stringTable) / sizeof(JSON_STRING_TABLE); i++)
	{
		QString value = readObject(mainObject, stringTable[i].m_JSONKey);
		if (value == QString("NULL"))
		{
			value = "";
		}

		if (stringTable[i].m_stringID == JSON_STRING)
		{
			pStringValue = (QString *)(stringTable[i].m_pValue);
			*pStringValue = value;
			qDebug() << i << value << " = " << *pStringValue;				
		}
		else if (stringTable[i].m_stringID == JSON_BOOL)
		{
			pBoolValue = (bool *)(stringTable[i].m_pValue);
			/* "On" or "Off" */
			if ((value == "On") || (value == "Yes"))
			{
				*pBoolValue = true;
				qDebug() << i << value << " = "  << "true";				
			}
			else if ((value == "Off") || (value == "No"))
			{
				*pBoolValue = false;
				qDebug() << i << value << " = "  << "false";				
			}
			else
			{
				qDebug() << i << value << "Invalid Bool Value ***************************";				
			}
		}
		else if (stringTable[i].m_stringID == JSON_INT)
		{
			pIntValue = (int *)(stringTable[i].m_pValue);
			// if match all, it should be : rx.setPattern("(^-?\\d+$)"); 
			// Here we just need to match the first digital munber.
			if (rx_int.indexIn(value) != -1) 
			{
				*pIntValue = rx_int.cap(1).toInt();
				qDebug() << i << value << " = "  << *pIntValue;				
			}
			else
			{
				qDebug() << i << value << "Invalid Int Value***********************************";							
			}
		}
		else if (stringTable[i].m_stringID == JSON_FLOAT)
		{
			pFloatValue = (float *)(stringTable[i].m_pValue);
			if (rx_float.indexIn(value) != -1) 
			{
				*pFloatValue = rx_float.cap(1).toFloat();
				qDebug() << i << value << " = " << *pFloatValue;				
			}
			else
			{
				qDebug() << i << value << "Invalid Float Value***********************************";							
			}
		}
		else if (stringTable[i].m_stringID == JSON_ENUM)
		{
			pIntValue = (int *)(stringTable[i].m_pValue);
			*pIntValue = getTableValue(stringTable[i].m_pExplainTable, value);
			qDebug() << i << value << " = " << *pIntValue;						
		}
		else
		{
			qDebug() << "Invalid JSON_ID: " << i << "  = " << stringTable[i].m_stringID;			
		}
	}

	/* 8 PEQs */
	m_channelData[0].m_polarityNormalFlag = polarityNormalFlag_0;
	for (int bandIndex = 0; bandIndex < 8; bandIndex++)
	{
		value = readObject(mainObject, QString("OutPeq_MTX/Channel_XYZ_Band_%1_Frequency").arg(bandIndex + 1)).trimmed();
		m_channelData[0].m_EQData[bandIndex].setFrequency(value);
		
		value = readObject(mainObject, QString("OutPeq_MTX/Channel_XYZ_Band_%1_Gain").arg(bandIndex + 1)).trimmed();
		m_channelData[0].m_EQData[bandIndex].setGain(readFloat(value));
		
		value = readObject(mainObject, QString("OutPeq_MTX/Channel_XYZ_Band_%1_Q").arg(bandIndex + 1)).trimmed();
		m_channelData[0].m_EQData[bandIndex].setQ(readFloat(value));

		value = readObject(mainObject, QString("OutPeq_MTX/Channel_XYZ_Band_%1_Slope").arg(bandIndex + 1)).trimmed();
		m_channelData[0].m_EQData[bandIndex].setSlope(readFloat(value));

		value = readObject(mainObject, QString("OutPeq_MTX/Channel_XYZ_Band_%1_Type").arg(bandIndex + 1)).trimmed();
		EQType type = (EQType)getTableValue(g_PEQName, value);
		m_channelData[0].m_EQData[bandIndex].setType(type);
	}

	/* Read xover */
	m_channelData[0].m_EQData[8].setFrequency(hpfFreqChannel_0);
	m_channelData[0].m_EQData[9].setFrequency(lpfFreqChannel_0);

	m_channelData[0].m_EQData[8].setPFType(PF_IIR);
	m_channelData[0].m_EQData[8].setPFIIRType((PF_IIR_Type)hpfTypeChannel_0);

	m_channelData[0].m_EQData[9].setPFType(PF_IIR);
	m_channelData[0].m_EQData[9].setPFIIRType((PF_IIR_Type)lpfTypeChannel_0);

	return (true);
}

void SpeakerData::writeJSON_common(QJsonObject & mainObject)
{
	mainObject["Type"] = getTableString(g_speakerTypeTable, (int)m_speakerType);
	mainObject["Readable"] = getTableString(g_yesNoTable, (int)m_readable);
    mainObject["Writable"] = getTableString(g_yesNoTable, (int)m_writable);
	if (m_password.isEmpty() == false)
	{
		mainObject["Password"] = m_password;
	}
	mainObject["Version"] = "1.0";
}

typedef struct
{
	int			m_stringID;
	QString		m_JSONKey;
	void		*m_pValue;
	EXPLAIN_TABLE	*m_pExplainTable;
	int			m_tableCount;
} JSON_WRITE_TABLE;

void SpeakerData::writeJSON_Passive(QJsonObject & mainObject)
{
	QJsonObject outDelay;
    outDelay["Channel_1_Amount"] = QString("0ms");
    outDelay["Channel_2_Amount"] = QString("%1ms").arg(m_channelData[0].m_delayInMs);
    outDelay["Channel_3_Amount"] = QString("0ms");
	mainObject["OutDelay_XYZ"] = outDelay;

	QJsonObject outGain;
    outGain["Channel_1_Gain"] = "0.0dB";
    outGain["Channel_1_Mute"] = "On";
    outGain["Channel_1_Polarity"] = "Normal";
    outGain["Channel_2_Gain"] = QString("%1dB").arg(m_channelData[0].m_gain);
    outGain["Channel_2_Mute"] = QString("Off");
    outGain["Channel_2_Polarity"] = getTableString(g_polarityTable, (int)m_channelData[0].m_polarityNormalFlag);
    outGain["Channel_3_Gain"] = "0.0dB";
    outGain["Channel_3_Mute"] = "On";
    outGain["Channel_3_Polarity"] = "Normal";
	mainObject["OutGain_XYZ"] = outGain;

	QJsonObject fir;
	fir["Channel_1_Bypass"] = "On";
	fir["Channel_1_Coefficients"] = "1.0";
	fir["Channel_1_NumTaps"] = "384";
    fir["Channel_2_Bypass"] = getTableString(g_onOffTable, (int)(!m_channelData[0].m_firEnableFlag));
	fir["Channel_2_Coefficients"] = m_channelData[0].m_strFIR;
	fir["Channel_2_NumTaps"] = "384";
    fir["Channel_3_Bypass"] = "On";
	fir["Channel_3_Coefficients"] = "1.0";
	fir["Channel_3_NumTaps"] = "384";
	mainObject["OutFir_XYZ"] = fir;

	QJsonObject limiter;
	limiter["Channel_1_Attack"] = "10.0 ms";
	limiter["Channel_1_Auto"] = "Off";
	limiter["Channel_1_Enable"] = "Off";
	limiter["Channel_1_Hold"] = "0ms";
	limiter["Channel_1_Release"] = "320dB/s";
	limiter["Channel_1_Threshold"] = "0.0dB";

	limiter["Channel_2_Attack"] = QString("%1 ms").arg(m_channelData[0].m_limiter.m_attackTimeInMs);
	limiter["Channel_2_Auto"] = getTableString(g_onOffTable, (int)(m_channelData[0].m_limiter.m_autoFlag));
	limiter["Channel_2_Enable"] = getTableString(g_onOffTable, (int)(m_channelData[0].m_limiter.m_enableFlag));
	limiter["Channel_2_Hold"] = QString("%1 ms").arg(m_channelData[0].m_limiter.m_holdTimeInMs);
	limiter["Channel_2_Release"] = QString("%1dB/s").arg(m_channelData[0].m_limiter.m_release);
	limiter["Channel_2_Threshold"] = QString("%1dB").arg(m_channelData[0].m_limiter.m_threshold);

	limiter["Channel_3_Attack"] = "10.0 ms";
	limiter["Channel_3_Auto"] = "Off";
	limiter["Channel_3_Enable"] = "Off";
	limiter["Channel_3_Hold"] = "0ms";
	limiter["Channel_3_Release"] = "320dB/s";
	limiter["Channel_3_Threshold"] = "0.0dB";
	mainObject["OutLimiter_XYZ"] = limiter;	

	/* 8 PEQs */
	QJsonObject peq;
	for (int channelIndex = 0; channelIndex < 3; channelIndex++)
	{
		for (int bandIndex = 0; bandIndex < 8; bandIndex++)
		{
			if ((channelIndex == 0) || (channelIndex == 2))
			{
				peq[QString("Channel_%1_Band_%2_Frequency").arg(channelIndex + 1).arg(bandIndex + 1)] = "1kHz";
				peq[QString("Channel_%1_Band_%2_Gain").arg(channelIndex + 1).arg(bandIndex + 1)] = "0.0dB";
				peq[QString("Channel_%1_Band_%2_Q").arg(channelIndex + 1).arg(bandIndex + 1)] = "1";
				peq[QString("Channel_%1_Band_%2_Slope").arg(channelIndex + 1).arg(bandIndex + 1)] = "3";
				peq[QString("Channel_%1_Band_%2_Type").arg(channelIndex + 1).arg(bandIndex + 1)] = "Bell";
			}
			else if (channelIndex == 1)
			{
				peq[QString("Channel_%1_Band_%2_Frequency").arg(channelIndex + 1).arg(bandIndex + 1)] = QString("%1Hz").arg(m_channelData[0].m_EQData[bandIndex].getFrequency());
				peq[QString("Channel_%1_Band_%2_Gain").arg(channelIndex + 1).arg(bandIndex + 1)] = QString("%1dB").arg(m_channelData[0].m_EQData[bandIndex].getGain());
				peq[QString("Channel_%1_Band_%2_Q").arg(channelIndex + 1).arg(bandIndex + 1)] = QString("%1").arg(m_channelData[0].m_EQData[bandIndex].getQ());
				peq[QString("Channel_%1_Band_%2_Slope").arg(channelIndex + 1).arg(bandIndex + 1)] = QString("%1").arg(m_channelData[0].m_EQData[bandIndex].getSlope());
				peq[QString("Channel_%1_Band_%2_Type").arg(channelIndex + 1).arg(bandIndex + 1)] = getTableString(g_PEQName, (int)(m_channelData[0].m_EQData[bandIndex].getType()));
			}
		}

		if ((channelIndex == 0) || (channelIndex == 2))
		{
			peq[QString("Channel_%1_ParametricEQ").arg(channelIndex + 1)] = "Off";
		}
		else if (channelIndex == 1)
		{
			peq[QString("Channel_%1_ParametricEQ").arg(channelIndex + 1)] = getTableString(g_onOffTable, m_channelData[0].m_PEQEnableFlag);		
		}
	}
	mainObject["OutPeq_XYZ"] = peq;	

	/* Xover */
	QJsonObject xover;
	for (int channelIndex = 0; channelIndex < 3; channelIndex++)
	{
		if ((channelIndex == 0) || (channelIndex == 2))
		{
			xover[QString("Channel_%1_EnableHighPass").arg(channelIndex + 1)] = "Off";
			xover[QString("Channel_%1_EnableLowPass").arg(channelIndex + 1)] = "Off";
			xover[QString("Channel_%1_HPFrequency").arg(channelIndex + 1)] = "16Hz";
			xover[QString("Channel_%1_HPType").arg(channelIndex + 1)] = "BW 48";
			xover[QString("Channel_%1_LPFrequency").arg(channelIndex + 1)] = "20kHz";
			xover[QString("Channel_%1_LPType").arg(channelIndex + 1)] = "BW 24";
		}
		else
		{
			xover[QString("Channel_%1_EnableHighPass").arg(channelIndex + 1)] = getTableString(g_onOffTable, m_channelData[channelIndex - 1].m_HPF_IIR_EnableFlag);
			xover[QString("Channel_%1_EnableLowPass").arg(channelIndex + 1)] = getTableString(g_onOffTable, m_channelData[channelIndex - 1].m_LPF_IIR_EnableFlag);
			xover[QString("Channel_%1_HPFrequency").arg(channelIndex + 1)] = QString("%1Hz").arg(m_channelData[channelIndex - 1].m_EQData[8].getFrequency());
			xover[QString("Channel_%1_HPType").arg(channelIndex + 1)] = getTableString(g_PFIIRTypeTable, (int)(m_channelData[channelIndex - 1].m_EQData[8].getPFIIRType()));
			xover[QString("Channel_%1_LPFrequency").arg(channelIndex + 1)] = QString("%1Hz").arg(m_channelData[channelIndex - 1].m_EQData[9].getFrequency());
			xover[QString("Channel_%1_LPType").arg(channelIndex + 1)] = getTableString(g_PFIIRTypeTable, (int)(m_channelData[channelIndex - 1].m_EQData[9].getPFIIRType()));
		}
	}
	mainObject["Xover_XYZ"] = xover;	
}

void SpeakerData::writeJSON_Biamp(QJsonObject & mainObject)
{
	QJsonObject outDelay;
    outDelay["Channel_1_Amount"] = QString("0ms");
    outDelay["Channel_2_Amount"] = QString("%1ms").arg(m_channelData[0].m_delayInMs);
    outDelay["Channel_3_Amount"] = QString("%1ms").arg(m_channelData[1].m_delayInMs);
	mainObject["OutDelay_XYZ"] = outDelay;

	QJsonObject outGain;
    outGain["Channel_1_Gain"] = "0.0dB";
    outGain["Channel_1_Mute"] = "On";
    outGain["Channel_1_Polarity"] = "Normal";
    outGain["Channel_2_Gain"] = QString("%1dB").arg(m_channelData[0].m_gain);
    outGain["Channel_2_Mute"] = QString("Off");
    outGain["Channel_2_Polarity"] = getTableString(g_polarityTable, (int)m_channelData[0].m_polarityNormalFlag);
    outGain["Channel_3_Gain"] = QString("%1dB").arg(m_channelData[1].m_gain);
    outGain["Channel_3_Mute"] = QString("Off");
    outGain["Channel_3_Polarity"] = getTableString(g_polarityTable, (int)m_channelData[1].m_polarityNormalFlag);
	mainObject["OutGain_XYZ"] = outGain;

	QJsonObject fir;
	fir["Channel_1_Bypass"] = "On";
	fir["Channel_1_Coefficients"] = "1.0";
	fir["Channel_1_NumTaps"] = "384";
    fir["Channel_2_Bypass"] = getTableString(g_onOffTable, (int)(!m_channelData[0].m_firEnableFlag));
	fir["Channel_2_Coefficients"] = m_channelData[0].m_strFIR;
	fir["Channel_2_NumTaps"] = "384";
    fir["Channel_3_Bypass"] = getTableString(g_onOffTable, (int)(!m_channelData[1].m_firEnableFlag));
	fir["Channel_3_Coefficients"] = m_channelData[1].m_strFIR;
	fir["Channel_3_NumTaps"] = "384";
	mainObject["OutFir_XYZ"] = fir;

	QJsonObject limiter;
	limiter["Channel_1_Attack"] = "10.0 ms";
	limiter["Channel_1_Auto"] = "Off";
	limiter["Channel_1_Enable"] = "Off";
	limiter["Channel_1_Hold"] = "0.0ms";
	limiter["Channel_1_Release"] = "320dB/s";
	limiter["Channel_1_Threshold"] = "0.0dB";

	limiter["Channel_2_Attack"] = QString("%1 ms").arg(m_channelData[0].m_limiter.m_attackTimeInMs);
	limiter["Channel_2_Auto"] = getTableString(g_onOffTable, (int)(m_channelData[0].m_limiter.m_autoFlag));
	limiter["Channel_2_Enable"] = getTableString(g_onOffTable, (int)(m_channelData[0].m_limiter.m_enableFlag));
	limiter["Channel_2_Hold"] = QString("%1 ms").arg(m_channelData[0].m_limiter.m_holdTimeInMs);
	limiter["Channel_2_Release"] = QString("%1dB/s").arg(m_channelData[0].m_limiter.m_release);
	limiter["Channel_2_Threshold"] = QString("%1dB").arg(m_channelData[0].m_limiter.m_threshold);

	limiter["Channel_3_Attack"] = QString("%1 ms").arg(m_channelData[1].m_limiter.m_attackTimeInMs);
	limiter["Channel_3_Auto"] = getTableString(g_onOffTable, (int)(m_channelData[1].m_limiter.m_autoFlag));
	limiter["Channel_3_Enable"] = getTableString(g_onOffTable, (int)(m_channelData[1].m_limiter.m_enableFlag));
	limiter["Channel_3_Hold"] = QString("%1 ms").arg(m_channelData[1].m_limiter.m_holdTimeInMs);
	limiter["Channel_3_Release"] = QString("%1dB/s").arg(m_channelData[1].m_limiter.m_release);
	limiter["Channel_3_Threshold"] = QString("%1dB").arg(m_channelData[1].m_limiter.m_threshold);
	mainObject["OutLimiter_XYZ"] = limiter;	

	/* 8 PEQs */
	QJsonObject peq;
	for (int channelIndex = 0; channelIndex < 3; channelIndex++)
	{
		for (int bandIndex = 0; bandIndex < 8; bandIndex++)
		{
			if (channelIndex == 0)
			{
				peq[QString("Channel_%1_Band_%2_Frequency").arg(channelIndex + 1).arg(bandIndex + 1)] = "1kHz";
				peq[QString("Channel_%1_Band_%2_Gain").arg(channelIndex + 1).arg(bandIndex + 1)] = "0.0dB";
				peq[QString("Channel_%1_Band_%2_Q").arg(channelIndex + 1).arg(bandIndex + 1)] = "1";
				peq[QString("Channel_%1_Band_%2_Slope").arg(channelIndex + 1).arg(bandIndex + 1)] = "3";
				peq[QString("Channel_%1_Band_%2_Type").arg(channelIndex + 1).arg(bandIndex + 1)] = "Bell";
			}
			else if ((channelIndex == 1) || (channelIndex == 2))
			{
				peq[QString("Channel_%1_Band_%2_Frequency").arg(channelIndex + 1).arg(bandIndex + 1)] = QString("%1Hz").arg(m_channelData[channelIndex - 1].m_EQData[bandIndex].getFrequency());
				peq[QString("Channel_%1_Band_%2_Gain").arg(channelIndex + 1).arg(bandIndex + 1)] = QString("%1dB").arg(m_channelData[channelIndex - 1].m_EQData[bandIndex].getGain());
				peq[QString("Channel_%1_Band_%2_Q").arg(channelIndex + 1).arg(bandIndex + 1)] = QString("%1").arg(m_channelData[channelIndex - 1].m_EQData[bandIndex].getQ());
				peq[QString("Channel_%1_Band_%2_Slope").arg(channelIndex + 1).arg(bandIndex + 1)] = QString("%1").arg(m_channelData[channelIndex - 1].m_EQData[bandIndex].getSlope());
				peq[QString("Channel_%1_Band_%2_Type").arg(channelIndex + 1).arg(bandIndex + 1)] = getTableString(g_PEQName, (int)(m_channelData[channelIndex - 1].m_EQData[bandIndex].getType()));
			}
		}

		if (channelIndex == 0)
		{
			peq[QString("Channel_%1_ParametricEQ").arg(channelIndex + 1)] = "Off";
		}
		else if ((channelIndex == 1) || (channelIndex == 2))
		{
			peq[QString("Channel_%1_ParametricEQ").arg(channelIndex + 1)] = getTableString(g_onOffTable, m_channelData[channelIndex - 1].m_PEQEnableFlag);		
		}
	}
	mainObject["OutPeq_XYZ"] = peq;	

	/* Xover */
	QJsonObject xover;
	for (int channelIndex = 0; channelIndex < 3; channelIndex++)
	{
		if (channelIndex == 0)
		{
			xover[QString("Channel_%1_EnableHighPass").arg(channelIndex + 1)] = "Off";
			xover[QString("Channel_%1_EnableLowPass").arg(channelIndex + 1)] = "Off";
			xover[QString("Channel_%1_HPFrequency").arg(channelIndex + 1)] = "16Hz";
			xover[QString("Channel_%1_HPType").arg(channelIndex + 1)] = "BW 48";
			xover[QString("Channel_%1_LPFrequency").arg(channelIndex + 1)] = "20kHz";
			xover[QString("Channel_%1_LPType").arg(channelIndex + 1)] = "BW 24";
		}
		else
		{
			xover[QString("Channel_%1_EnableHighPass").arg(channelIndex + 1)] = getTableString(g_onOffTable, m_channelData[channelIndex - 1].m_HPF_IIR_EnableFlag);
			xover[QString("Channel_%1_EnableLowPass").arg(channelIndex + 1)] = getTableString(g_onOffTable, m_channelData[channelIndex - 1].m_LPF_IIR_EnableFlag);
			xover[QString("Channel_%1_HPFrequency").arg(channelIndex + 1)] = QString("%1Hz").arg(m_channelData[channelIndex - 1].m_EQData[8].getFrequency());
			xover[QString("Channel_%1_HPType").arg(channelIndex + 1)] = getTableString(g_PFIIRTypeTable, (int)(m_channelData[channelIndex - 1].m_EQData[8].getPFIIRType()));
			xover[QString("Channel_%1_LPFrequency").arg(channelIndex + 1)] = QString("%1Hz").arg(m_channelData[channelIndex - 1].m_EQData[9].getFrequency());
			xover[QString("Channel_%1_LPType").arg(channelIndex + 1)] = getTableString(g_PFIIRTypeTable, (int)(m_channelData[channelIndex - 1].m_EQData[9].getPFIIRType()));
		}
	}
	mainObject["Xover_XYZ"] = xover;	

}

void SpeakerData::writeJSON_Triamp(QJsonObject & mainObject)
{
	QJsonObject outDelay;
    outDelay["Channel_1_Amount"] = QString("%1ms").arg(m_channelData[0].m_delayInMs);
    outDelay["Channel_2_Amount"] = QString("%1ms").arg(m_channelData[1].m_delayInMs);
    outDelay["Channel_3_Amount"] = QString("%1ms").arg(m_channelData[2].m_delayInMs);
	mainObject["OutDelay_XYZ"] = outDelay;

	QJsonObject outGain;
    outGain["Channel_1_Gain"] = QString("%1dB").arg(m_channelData[0].m_gain);
    outGain["Channel_1_Mute"] = QString("Off");
    outGain["Channel_1_Polarity"] = getTableString(g_polarityTable, (int)m_channelData[0].m_polarityNormalFlag);
    outGain["Channel_2_Gain"] = QString("%1dB").arg(m_channelData[1].m_gain);
    outGain["Channel_2_Mute"] = QString("Off");
    outGain["Channel_2_Polarity"] = getTableString(g_polarityTable, (int)m_channelData[1].m_polarityNormalFlag);
    outGain["Channel_3_Gain"] = QString("%1dB").arg(m_channelData[2].m_gain);
    outGain["Channel_3_Mute"] = QString("Off");
    outGain["Channel_3_Polarity"] = getTableString(g_polarityTable, (int)m_channelData[2].m_polarityNormalFlag);
	mainObject["OutGain_XYZ"] = outGain;

	QJsonObject fir;
	fir["Channel_1_Bypass"] = getTableString(g_onOffTable, (int)(!m_channelData[0].m_firEnableFlag));
	fir["Channel_1_Coefficients"] = m_channelData[0].m_strFIR;
	fir["Channel_1_NumTaps"] = "384";
    fir["Channel_2_Bypass"] = getTableString(g_onOffTable, (int)(!m_channelData[1].m_firEnableFlag));
	fir["Channel_2_Coefficients"] = m_channelData[1].m_strFIR;
	fir["Channel_2_NumTaps"] = "384";
    fir["Channel_3_Bypass"] = getTableString(g_onOffTable, (int)(!m_channelData[2].m_firEnableFlag));
	fir["Channel_3_Coefficients"] = m_channelData[2].m_strFIR;
	fir["Channel_3_NumTaps"] = "384";
	mainObject["OutFir_XYZ"] = fir;

	QJsonObject limiter;
	limiter["Channel_1_Attack"] = QString("%1 ms").arg(m_channelData[0].m_limiter.m_attackTimeInMs);
	limiter["Channel_1_Auto"] = getTableString(g_onOffTable, (int)(m_channelData[0].m_limiter.m_autoFlag));
	limiter["Channel_1_Enable"] = getTableString(g_onOffTable, (int)(m_channelData[0].m_limiter.m_enableFlag));
	limiter["Channel_1_Hold"] = QString("%1 ms").arg(m_channelData[0].m_limiter.m_holdTimeInMs);
	limiter["Channel_1_Release"] = QString("%1dB/s").arg(m_channelData[0].m_limiter.m_release);
	limiter["Channel_1_Threshold"] = QString("%1dB").arg(m_channelData[0].m_limiter.m_threshold);

	limiter["Channel_2_Attack"] = QString("%1 ms").arg(m_channelData[1].m_limiter.m_attackTimeInMs);
	limiter["Channel_2_Auto"] = getTableString(g_onOffTable, (int)(m_channelData[1].m_limiter.m_autoFlag));
	limiter["Channel_2_Enable"] = getTableString(g_onOffTable, (int)(m_channelData[1].m_limiter.m_enableFlag));
	limiter["Channel_2_Hold"] = QString("%1 ms").arg(m_channelData[1].m_limiter.m_holdTimeInMs);
	limiter["Channel_2_Release"] = QString("%1dB/s").arg(m_channelData[1].m_limiter.m_release);
	limiter["Channel_2_Threshold"] = QString("%1dB").arg(m_channelData[1].m_limiter.m_threshold);

	limiter["Channel_3_Attack"] = QString("%1 ms").arg(m_channelData[2].m_limiter.m_attackTimeInMs);
	limiter["Channel_3_Auto"] = getTableString(g_onOffTable, (int)(m_channelData[2].m_limiter.m_autoFlag));
	limiter["Channel_3_Enable"] = getTableString(g_onOffTable, (int)(m_channelData[2].m_limiter.m_enableFlag));
	limiter["Channel_3_Hold"] = QString("%1 ms").arg(m_channelData[2].m_limiter.m_holdTimeInMs);
	limiter["Channel_3_Release"] = QString("%1dB/s").arg(m_channelData[2].m_limiter.m_release);
	limiter["Channel_3_Threshold"] = QString("%1dB").arg(m_channelData[2].m_limiter.m_threshold);
	mainObject["OutLimiter_XYZ"] = limiter;	

	/* 8 PEQs */
	QJsonObject peq;
	for (int channelIndex = 0; channelIndex < 3; channelIndex++)
	{
		for (int bandIndex = 0; bandIndex < 8; bandIndex++)
		{
			peq[QString("Channel_%1_Band_%2_Frequency").arg(channelIndex + 1).arg(bandIndex + 1)] = QString("%1Hz").arg(m_channelData[channelIndex].m_EQData[bandIndex].getFrequency());
			peq[QString("Channel_%1_Band_%2_Gain").arg(channelIndex + 1).arg(bandIndex + 1)] = QString("%1dB").arg(m_channelData[channelIndex].m_EQData[bandIndex].getGain());
			peq[QString("Channel_%1_Band_%2_Q").arg(channelIndex + 1).arg(bandIndex + 1)] = QString("%1").arg(m_channelData[channelIndex].m_EQData[bandIndex].getQ());
			peq[QString("Channel_%1_Band_%2_Slope").arg(channelIndex + 1).arg(bandIndex + 1)] = QString("%1").arg(m_channelData[channelIndex].m_EQData[bandIndex].getSlope());
			peq[QString("Channel_%1_Band_%2_Type").arg(channelIndex + 1).arg(bandIndex + 1)] = getTableString(g_PEQName, (int)(m_channelData[channelIndex].m_EQData[bandIndex].getType()));
		}
		peq[QString("Channel_%1_ParametricEQ").arg(channelIndex + 1)] = getTableString(g_onOffTable, m_channelData[channelIndex].m_PEQEnableFlag);
	}
	mainObject["OutPeq_XYZ"] = peq;	

	/* Xover */
	QJsonObject xover;
	for (int channelIndex = 0; channelIndex < 3; channelIndex++)
	{
		xover[QString("Channel_%1_EnableHighPass").arg(channelIndex + 1)] = getTableString(g_onOffTable, m_channelData[channelIndex].m_HPF_IIR_EnableFlag);
		xover[QString("Channel_%1_EnableLowPass").arg(channelIndex + 1)] = getTableString(g_onOffTable, m_channelData[channelIndex].m_LPF_IIR_EnableFlag);
		xover[QString("Channel_%1_HPFrequency").arg(channelIndex + 1)] = QString("%1Hz").arg(m_channelData[channelIndex].m_EQData[8].getFrequency());
		xover[QString("Channel_%1_HPType").arg(channelIndex + 1)] = getTableString(g_PFIIRTypeTable, (int)(m_channelData[channelIndex].m_EQData[8].getPFIIRType()));
		xover[QString("Channel_%1_LPFrequency").arg(channelIndex + 1)] = QString("%1Hz").arg(m_channelData[channelIndex].m_EQData[9].getFrequency());
		xover[QString("Channel_%1_LPType").arg(channelIndex + 1)] = getTableString(g_PFIIRTypeTable, (int)(m_channelData[channelIndex].m_EQData[9].getPFIIRType()));
	}
	mainObject["Xover_XYZ"] = xover;	
}

void SpeakerData::writeJSON_Surround(QJsonObject & mainObject)
{
	QJsonObject outGain;
    outGain["Channel_XYZ_Gain"] = QString("%1dB").arg(m_channelData[0].m_gain);
    outGain["Channel_XYZ_Mute"] = "Off";
    outGain["Channel_XYZ_Polarity"] = getTableString(g_polarityTable, (int)m_channelData[0].m_polarityNormalFlag);
	mainObject["OutGain_MTX"] = outGain;

	QJsonObject limiter;
	limiter["Channel_XYZ_Attack"] = QString("%1 ms").arg(m_channelData[0].m_limiter.m_attackTimeInMs);
	limiter["Channel_XYZ_Auto"] = getTableString(g_onOffTable, (int)(m_channelData[0].m_limiter.m_autoFlag));
	limiter["Channel_XYZ_Enable"] = getTableString(g_onOffTable, (int)(m_channelData[0].m_limiter.m_enableFlag));
	limiter["Channel_XYZ_Hold"] = QString("%1 ms").arg(m_channelData[0].m_limiter.m_holdTimeInMs);
	limiter["Channel_XYZ_Release"] = QString("%1dB/s").arg(m_channelData[0].m_limiter.m_release);
	limiter["Channel_XYZ_Threshold"] = QString("%1dB").arg(m_channelData[0].m_limiter.m_threshold);
	mainObject["OutLimiter_MTX"] = limiter;	


	/* 8 PEQs */
	QJsonObject peq;
	for (int bandIndex = 0; bandIndex < 8; bandIndex++)
	{
		peq[QString("Channel_XYZ_Band_%1_Frequency").arg(bandIndex + 1)] = QString("%1Hz").arg(m_channelData[0].m_EQData[bandIndex].getFrequency());
		peq[QString("Channel_XYZ_Band_%1_Gain").arg(bandIndex + 1)] = QString("%1dB").arg(m_channelData[0].m_EQData[bandIndex].getGain());
		peq[QString("Channel_XYZ_Band_%1_Q").arg(bandIndex + 1)] = QString("%1").arg(m_channelData[0].m_EQData[bandIndex].getQ());
		peq[QString("Channel_XYZ_Band_%1_Slope").arg(bandIndex + 1)] = QString("%1").arg(m_channelData[0].m_EQData[bandIndex].getSlope());
		peq[QString("Channel_XYZ_Band_%1_Type").arg(bandIndex + 1)] = getTableString(g_PEQName, (int)(m_channelData[0].m_EQData[bandIndex].getType()));
	}

	peq[QString("Channel_XYZ_ParametricEQ")] = getTableString(g_onOffTable, m_channelData[0].m_PEQEnableFlag);		
	mainObject["OutPeq_MTX"] = peq;	

	/* xover */
	QJsonObject xover;
	xover[QString("Channel_XYZ_EnableHighPass")] = getTableString(g_onOffTable, m_channelData[0].m_HPF_IIR_EnableFlag);
	xover[QString("Channel_XYZ_EnableLowPass")] = getTableString(g_onOffTable, m_channelData[0].m_LPF_IIR_EnableFlag);
	xover[QString("Channel_XYZ_HPFrequency")] = QString("%1Hz").arg(m_channelData[0].m_EQData[8].getFrequency());
	xover[QString("Channel_XYZ_HPType")] = getTableString(g_PFIIRTypeTable, (int)(m_channelData[0].m_EQData[8].getPFIIRType()));
	xover[QString("Channel_XYZ_LPFrequency")] = QString("%1Hz").arg(m_channelData[0].m_EQData[9].getFrequency());
	xover[QString("Channel_XYZ_LPType")] = getTableString(g_PFIIRTypeTable, (int)(m_channelData[0].m_EQData[9].getPFIIRType()));
	mainObject["Xover_MTX"] = xover;	

}

void SpeakerData::writeJSON_Subwoofer(QJsonObject & mainObject)
{
	QJsonObject outGain;
    outGain["Channel_XYZ_Gain"] = QString("%1dB").arg(m_channelData[0].m_gain);
    outGain["Channel_XYZ_Mute"] = "Off";
    outGain["Channel_XYZ_Polarity"] = getTableString(g_polarityTable, (int)m_channelData[0].m_polarityNormalFlag);
	mainObject["OutGain_MTX"] = outGain;

	QJsonObject limiter;
	limiter["Channel_XYZ_Attack"] = QString("%1 ms").arg(m_channelData[0].m_limiter.m_attackTimeInMs);
	limiter["Channel_XYZ_Auto"] = getTableString(g_onOffTable, (int)(m_channelData[0].m_limiter.m_autoFlag));
	limiter["Channel_XYZ_Enable"] = getTableString(g_onOffTable, (int)(m_channelData[0].m_limiter.m_enableFlag));
	limiter["Channel_XYZ_Hold"] = QString("%1 ms").arg(m_channelData[0].m_limiter.m_holdTimeInMs);
	limiter["Channel_XYZ_Release"] = QString("%1dB/s").arg(m_channelData[0].m_limiter.m_release);
	limiter["Channel_XYZ_Threshold"] = QString("%1dB").arg(m_channelData[0].m_limiter.m_threshold);
	mainObject["OutLimiter_MTX"] = limiter;	

	/* 8 PEQs */
	QJsonObject peq;
	for (int bandIndex = 0; bandIndex < 8; bandIndex++)
	{
		peq[QString("Channel_XYZ_Band_%1_Frequency").arg(bandIndex + 1)] = QString("%1Hz").arg(m_channelData[0].m_EQData[bandIndex].getFrequency());
		peq[QString("Channel_XYZ_Band_%1_Gain").arg(bandIndex + 1)] = QString("%1dB").arg(m_channelData[0].m_EQData[bandIndex].getGain());
		peq[QString("Channel_XYZ_Band_%1_Q").arg(bandIndex + 1)] = QString("%1").arg(m_channelData[0].m_EQData[bandIndex].getQ());
		peq[QString("Channel_XYZ_Band_%1_Slope").arg(bandIndex + 1)] = QString("%1").arg(m_channelData[0].m_EQData[bandIndex].getSlope());
		peq[QString("Channel_XYZ_Band_%1_Type").arg(bandIndex + 1)] = getTableString(g_PEQName, (int)(m_channelData[0].m_EQData[bandIndex].getType()));
	}

	peq[QString("Channel_XYZ_ParametricEQ")] = getTableString(g_onOffTable, m_channelData[0].m_PEQEnableFlag);		
	mainObject["OutPeq_MTX"] = peq;	

	/* xover */
	QJsonObject xover;
	xover[QString("Channel_XYZ_EnableHighPass")] = getTableString(g_onOffTable, m_channelData[0].m_HPF_IIR_EnableFlag);
	xover[QString("Channel_XYZ_EnableLowPass")] = getTableString(g_onOffTable, m_channelData[0].m_LPF_IIR_EnableFlag);
	xover[QString("Channel_XYZ_HPFrequency")] = QString("%1Hz").arg(m_channelData[0].m_EQData[8].getFrequency());
	xover[QString("Channel_XYZ_HPType")] = getTableString(g_PFIIRTypeTable, (int)(m_channelData[0].m_EQData[8].getPFIIRType()));
	xover[QString("Channel_XYZ_LPFrequency")] = QString("%1Hz").arg(m_channelData[0].m_EQData[9].getFrequency());
	xover[QString("Channel_XYZ_LPType")] = getTableString(g_PFIIRTypeTable, (int)(m_channelData[0].m_EQData[9].getPFIIRType()));
	mainObject["Xover_MTX"] = xover;	
}

