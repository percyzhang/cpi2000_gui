#include "SpeakerEditor.h"
#include <QDebug>
#include "CPi2000Setting.h"
#include <QFileDialog>
#include <QMessageBox>
#include <QClipboard>
#include <QApplication>
#include "commonLib.h"
#include "mainApplication.h"
#include "mainWidget.h"

#define MIN_SCROLL_WIDTH 900
#define MIN_SCROLL_HEIGHT 720

extern EXPLAIN_TABLE g_PFIIRTypeTable[];

PasswordDlg::PasswordDlg(QWidget *parent): QDialog(parent)
{
	m_pEnableBox = new QCheckBox(tr("Enable Password Protection"), this);
	connect(m_pEnableBox, SIGNAL(clicked()), this, SLOT(onClickEnable()));

	m_pPasswordLabel1 = new QLabel(tr("Enter Password:"), this);
	m_pPasswordLabel1->setAlignment(Qt::AlignRight);

	m_pPasswordLabel2 = new QLabel(tr("Confirm Password:"), this);
	m_pPasswordLabel2->setAlignment(Qt::AlignRight);

	m_pPasswordEdit1 = new QLineEdit(this);
	connect(m_pPasswordEdit1, SIGNAL(textChanged(const QString &)), this, SLOT(onTextChanged(const QString &)));

	m_pPasswordEdit2 = new QLineEdit(this);
	connect(m_pPasswordEdit2, SIGNAL(textChanged(const QString &)), this, SLOT(onTextChanged(const QString &)));

	m_pDescription = new QLabel(tr("The password must contains at least 6 characters.\nThe password can only contain the following characters:\nthe captial letter(A-Z), the lowercase letter(a-z), \ndigital number (0 - 9), '-' and '_'."), this); 

	m_pOKButton = new QPushButton(tr("OK"), this);
	connect(m_pOKButton, SIGNAL(clicked()), this, SLOT(onClickOk()));

	m_pCancelButton = new QPushButton(tr("Cancel"), this);
	connect(m_pCancelButton, SIGNAL(clicked()), this, SLOT(onClickCancel()));

	setFixedSize(300, 200);
}

void PasswordDlg::onTextChanged(const QString &)
{
	refreshButtonStatus();
}

void PasswordDlg::setPassword(QString password)
{
	m_password = password;
	if (m_password.isEmpty())
	{
		m_pEnableBox->setChecked(false);
	}
	else
	{
		m_pEnableBox->setChecked(true);
		m_pPasswordEdit1->setText(password);
	}
	refresh();
}

void PasswordDlg::refreshButtonStatus()
{
	if (m_pEnableBox->isChecked())
	{
		QString text1 = m_pPasswordEdit1->text();
		QString text2 = m_pPasswordEdit2->text();

		if ((text1 == text2) && (text1.length() >= 6))
		{
			m_pOKButton->setEnabled(true);
		}
		else
		{
			m_pOKButton->setEnabled(false);
		}
	}
	else
	{
		m_pOKButton->setEnabled(true);
	}
}

void PasswordDlg::refresh(void)
{
	if (m_pEnableBox->isChecked())
	{
		m_pPasswordEdit1->setEnabled(true);
		m_pPasswordEdit2->setEnabled(true);
		refreshButtonStatus();
	}
	else
	{
		m_pPasswordEdit1->setEnabled(false);
		m_pPasswordEdit1->setText("");
		m_pPasswordEdit2->setEnabled(false);
		m_pPasswordEdit2->setText("");
		m_pOKButton->setEnabled(true);
	}
}

QString PasswordDlg::getPassword()
{
	return m_password;
}

void PasswordDlg::onClickOk()
{
	m_password = m_pPasswordEdit1->text();
	accept();
}

void PasswordDlg::onClickCancel()
{
	reject();
}

void PasswordDlg::onClickEnable()
{
	refresh();
}

void PasswordDlg::resizeEvent(QResizeEvent * /* event */)
{
	m_pEnableBox->setGeometry(65, 10, 151, 20);
	m_pPasswordLabel1->setGeometry(45, 40, 101, 16);
	m_pPasswordLabel2->setGeometry(45, 70, 101, 16);
	
	m_pPasswordEdit1->setGeometry(155, 40, 71, 20);
	m_pPasswordEdit2->setGeometry(155, 70, 71, 20);

	m_pDescription->setGeometry(15, 90, 275, 61);

	m_pOKButton->setGeometry(65, 160, 75, 23);
	m_pCancelButton->setGeometry(180, 160, 75, 23);
}


SpeakerEQWidget::SpeakerEQWidget(SpeakerEditor *pSpeakerEditor, QWidget *parent) : EQWidget(parent)
{
	m_pSpeakerEditor = pSpeakerEditor;
}

int SpeakerEQWidget::getEQCount()
{
//	return (10);
	return m_pSpeakerEditor->getEQCount();
//	return (5);
}

int SpeakerEQWidget::getCurrentEQIndex()
{
	return m_pSpeakerEditor->getCurrentEQIndex();
}

EQData * SpeakerEQWidget::getEQ(int index)
{
	return m_pSpeakerEditor->getEQ(index);
}

EQData * SpeakerEQWidget::getCurrentEQ()
{
	return m_pSpeakerEditor->getCurrentEQ();
}

bool SpeakerEQWidget::getEnableAllFlag()
{
	ChannelData *pCurrentChannelData = m_pSpeakerEditor->getCurrentChannelData();

	return (pCurrentChannelData->m_PEQEnableFlag);
}

void SpeakerEQWidget::clearFinalCurve()
{
	EQWidget::clearFinalCurve();

	qreal pVertical[4000];
	SpeakerData *pSpeakerData = m_pSpeakerEditor->getSpeakerData();
	if (pSpeakerData->m_speakerType == SPEAKER_TYPE_SUBWOOFER)
	{
		EQData EQ;
		EQ.setType(EQ_LPF);
		EQ.setFrequency(283);
		EQ.setPFType(PF_IIR);
		EQ.setPFIIRType(PF_IIR_BW24);
		EQ.enable(true);

		EQ.dBCalc(m_discreteCount, m_pDimensionX, pVertical);
		addToTotalCurve(pVertical);
	}
}

ChannelData *SpeakerEditor::getCurrentChannelData() 
{ 
	return m_speakerData.m_channelData + m_currentEQTab; 
}

SpeakerEditor::SpeakerEditor(QWidget *parent)	: QDialog(parent)
{
	setWindowTitle(tr("Speaker Editor"));
	m_pScrollArea = new QScrollArea(this);

	m_pWidget = new QWidget(m_pScrollArea);
//	m_pWidget->setObjectName(QStringLiteral("m_pTabFrame"));
	m_pWidget->setMinimumSize(MIN_SCROLL_WIDTH, MIN_SCROLL_HEIGHT);
	m_pWidget->setSizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::MinimumExpanding);

	m_pScrollArea = new QScrollArea(this);
	m_pScrollArea->setWidget(m_pWidget);
	m_pScrollArea->setWidgetResizable(true);
	m_pScrollArea->setHorizontalScrollBarPolicy(Qt::ScrollBarAsNeeded);
	m_pScrollArea->setVerticalScrollBarPolicy(Qt::ScrollBarAsNeeded);
	m_pScrollArea->setObjectName(QStringLiteral("ScrollArea"));

	m_slotEnableFlag = true;
	setObjectName(QStringLiteral("m_pMasterVolumeFrame"));

	m_pDefaultButton = new QPushButton(tr(""), m_pWidget);
	m_pDefaultButton->hide();

	m_pSpeakerNameLabel = new QLabel(tr("Speaker Name:"), m_pWidget);
	m_pSpeakerNameLabel->setAlignment(Qt::AlignRight | Qt::AlignVCenter);

	m_speakerTypeLabel = new QLabel(tr("Speaker Type:"), m_pWidget);
	m_speakerTypeLabel->setAlignment(Qt::AlignRight | Qt::AlignVCenter);
	
	m_pSpeakerNameEdit = new QLineEdit(m_pWidget);
	m_pSpeakerNameEdit->setEnabled(false);
	m_pSpeakerNameEdit->setFocus();

	m_pSpeakerTypeCombo = new QComboBox(m_pWidget);
	m_pSpeakerTypeCombo->addItem(tr("Full-range"));
	m_pSpeakerTypeCombo->addItem(tr("Two-way"));
	m_pSpeakerTypeCombo->addItem(tr("Three-way"));
	m_pSpeakerTypeCombo->addItem(tr("Surround"));
	m_pSpeakerTypeCombo->addItem(tr("Subwoofer"));
	m_pSpeakerTypeCombo->setEnabled(false);

	m_pGeneratorGroup = new QGroupBox((""), m_pWidget);
	{
		m_pSignalModeLabel = new QLabel(tr("Generator:"), m_pGeneratorGroup);
		m_pSignalModeLabel->setAlignment(Qt::AlignRight | Qt::AlignVCenter);

		m_pChannelLabel = new QLabel(tr("Assign to:"), m_pGeneratorGroup);
		m_pChannelLabel->setAlignment(Qt::AlignRight | Qt::AlignVCenter);

		for (int i = 0; i < 4; i++)
		{
			m_pChannelButton[i] = new MarkButton("", m_pGeneratorGroup); 
			m_pChannelButton[i]->mark(false);
			m_pChannelButton[i]->setObjectName(QStringLiteral("pinkNoise"));
		}
		connect(m_pChannelButton[0], SIGNAL(clicked()), this, SLOT(onClickChannelButton0()));
		connect(m_pChannelButton[1], SIGNAL(clicked()), this, SLOT(onClickChannelButton1()));
		connect(m_pChannelButton[2], SIGNAL(clicked()), this, SLOT(onClickChannelButton2()));
		connect(m_pChannelButton[3], SIGNAL(clicked()), this, SLOT(onClickChannelButton3()));

		m_pSignalModeCombo = new QComboBox(m_pGeneratorGroup);
		QString signal[] = {"OFF", "100 Hz", "1 kHz", "10 kHz",  "Pink Noise" };
		/*
					set to 100Hz:
					set "\\Preset\SignalGenerator\SV\SineFrequency"   "100Hz"
					set "\\Preset\SignalGenerator\SV\SignalType"   "Sine"

					set to 1kHz:
					set "\\Preset\SignalGenerator\SV\SineFrequency"   "1000Hz"
					set "\\Preset\SignalGenerator\SV\SignalType"   "Sine"

					set to Pink:
					set "\\Preset\SignalGenerator\SV\SignalType"   "Noise"

					set to Off:
					set "\\Preset\SignalGenerator\SV\SignalType"   "Off"
		*/
		for (int i = 0; i < 5; i++)
		{
			m_pSignalModeCombo->addItem(signal[i]);
		}
		connect(m_pSignalModeCombo, SIGNAL(currentIndexChanged(int)), this, SLOT(onSignalModeSelected(int)));
	}

	CPi2000Data *pData = getCPi2000Data();
	RTAData *pRTA = pData->getRTA();
	m_pSignalModeCombo->setCurrentIndex((int)pRTA->getSignalMode());

	m_pWritableCheck = new QCheckBox(QString(tr("Writable")), m_pWidget);
	connect(m_pWritableCheck, SIGNAL(clicked()), this, SLOT(onWritableClicked()));	
	m_pWritableCheck->hide();

#if 0
	m_pPasswordButton = new MarkButton(tr("Password Enabled"), this);
	m_pPasswordButton->setObjectName(QStringLiteral("enableGreenButton"));
	connect(m_pPasswordButton, SIGNAL(clicked()), this, SLOT(onClickPasswordButton()));
	m_pPasswordButton->mark(true);
	m_pPasswordButton->setDefault(false);
	m_pPasswordButton->setAutoDefault(false);
#endif

	m_pEQTabButton[0] = new MarkButton(tr("LF"), m_pWidget);
	m_pEQTabButton[0]->setObjectName(QStringLiteral("speakerEditorButton"));
	connect(m_pEQTabButton[0], SIGNAL(clicked()), this, SLOT(onClickEQTab_0()));
	m_pEQTabButton[0]->mark(true);

	m_pEQTabButton[1] = new MarkButton(tr("MF"), m_pWidget);
	m_pEQTabButton[1]->setObjectName(QStringLiteral("speakerEditorButton"));
	connect(m_pEQTabButton[1], SIGNAL(clicked()), this, SLOT(onClickEQTab_1()));
	m_pEQTabButton[1]->mark(false);
	
	m_pEQTabButton[2] = new MarkButton(tr("HF"), m_pWidget);
	m_pEQTabButton[2]->setObjectName(QStringLiteral("speakerEditorButton"));
	connect(m_pEQTabButton[2], SIGNAL(clicked()), this, SLOT(onClickEQTab_2()));
	m_pEQTabButton[2]->mark(false);

	m_pChannelFrame = new QFrame(m_pWidget);
	m_pChannelFrame->setFrameShadow(QFrame::Sunken);
	m_pChannelFrame->setFrameShape(QFrame::Panel);

	m_pEQWidget = new SpeakerEQWidget(this, m_pChannelFrame);
	m_pEQWidget->setGain(20, 4);
	connect(m_pEQWidget, SIGNAL(currentEQChanged(void)), this, SLOT(onCurrentEQChanged()));
	connect(m_pEQWidget, SIGNAL(setPFFreq(qreal)), this, SLOT(onSetPFFreq(qreal)));
	connect(m_pEQWidget, SIGNAL(currentEQIndexChanged(int)), this, SLOT(onCurrentEQIndexChanged(int)));

	//	        Raised = 0x0020, // raised shadow effect
 //       Sunken = 0x0030 // sunken shadow effect
//	m_pChannelFrame->setObjectName(QStringLiteral("backgroundFrame"));

	m_pLoadButton = new QPushButton(tr("Load from file"), m_pWidget);
	connect(m_pLoadButton, SIGNAL(clicked()), this, SLOT(onClickLoadButton()));
		
	m_pSaveButton = new QPushButton(tr("Save to file"), m_pWidget);
	connect(m_pSaveButton, SIGNAL(clicked()), this, SLOT(onClickSaveButton()));
	
	m_pApplyButton = new QPushButton(tr("Apply"), m_pWidget);
	connect(m_pApplyButton, SIGNAL(clicked()), this, SLOT(onClickApplyButton()));
	m_pApplyButton->setObjectName(QStringLiteral("yellowButton"));
	
	m_pCancelButton = new QPushButton(tr("Exit"), m_pWidget);
	connect(m_pCancelButton, SIGNAL(clicked()), this, SLOT(onClickCancelButton()));

	m_pBottomFrame = new QFrame(m_pChannelFrame);
	m_pBottomFrame->setFrameShadow(QFrame::Raised);
	m_pBottomFrame->setFrameShape(QFrame::Panel);

	m_pPEQEnableButton = new MarkButton(tr("PEQ Enabled"), m_pBottomFrame);
	m_pPEQEnableButton->setObjectName(QStringLiteral("enableGreenButton"));
	connect(m_pPEQEnableButton, SIGNAL(clicked()), this, SLOT(onClickAllPEQEnable()));
	m_pPEQEnableButton->mark(true);

	m_pPEQIndexLabel = new QLabel(tr("Parametric EQ:"), m_pBottomFrame);
	m_pPEQIndexLabel->setAlignment(Qt::AlignRight | Qt::AlignVCenter);

	
	m_pCrossOverEQIndexLabel = new QLabel(tr("Crossover EQ:"), m_pBottomFrame);
	m_pCrossOverEQIndexLabel->setAlignment(Qt::AlignRight | Qt::AlignVCenter);

	QString buttonText[] = {"1", "2", "3", "4", "5", "6", "7", "8", "HPF", "LPF", "FIR" };

	for (int i = 0; i < 11; i++)
	{
		m_pEQButton[i] = new EQButton(buttonText[i], m_pBottomFrame);
		m_pEQButton[i]->setObjectName(QStringLiteral("m_pEQButton"));
	}
	connect(m_pEQButton[0], SIGNAL(clicked()), this, SLOT(onClickEQIndex1()));
	connect(m_pEQButton[1], SIGNAL(clicked()), this, SLOT(onClickEQIndex2()));
	connect(m_pEQButton[2], SIGNAL(clicked()), this, SLOT(onClickEQIndex3()));
	connect(m_pEQButton[3], SIGNAL(clicked()), this, SLOT(onClickEQIndex4()));
	connect(m_pEQButton[4], SIGNAL(clicked()), this, SLOT(onClickEQIndex5()));
	connect(m_pEQButton[5], SIGNAL(clicked()), this, SLOT(onClickEQIndex6()));
	connect(m_pEQButton[6], SIGNAL(clicked()), this, SLOT(onClickEQIndex7()));
	connect(m_pEQButton[7], SIGNAL(clicked()), this, SLOT(onClickEQIndex8()));
	connect(m_pEQButton[8], SIGNAL(clicked()), this, SLOT(onClickEQIndex9()));
	connect(m_pEQButton[9], SIGNAL(clicked()), this, SLOT(onClickEQIndex10()));
	connect(m_pEQButton[10], SIGNAL(clicked()), this, SLOT(onClickEQIndex11()));

	connect(m_pEQButton[0], SIGNAL(doubleClick()), this,  SLOT(onDblClickEQIndex1()));
	connect(m_pEQButton[1], SIGNAL(doubleClick()), this,  SLOT(onDblClickEQIndex2()));
	connect(m_pEQButton[2], SIGNAL(doubleClick()), this,  SLOT(onDblClickEQIndex3()));
	connect(m_pEQButton[3], SIGNAL(doubleClick()), this,  SLOT(onDblClickEQIndex4()));
	connect(m_pEQButton[4], SIGNAL(doubleClick()), this,  SLOT(onDblClickEQIndex5()));
	connect(m_pEQButton[5], SIGNAL(doubleClick()), this,  SLOT(onDblClickEQIndex6()));
	connect(m_pEQButton[6], SIGNAL(doubleClick()), this,  SLOT(onDblClickEQIndex7()));
	connect(m_pEQButton[7], SIGNAL(doubleClick()), this,  SLOT(onDblClickEQIndex8()));
	connect(m_pEQButton[8], SIGNAL(doubleClick()), this,  SLOT(onDblClickEQIndex9()));
	connect(m_pEQButton[9], SIGNAL(doubleClick()), this,  SLOT(onDblClickEQIndex10()));
	connect(m_pEQButton[10], SIGNAL(doubleClick()), this, SLOT(onDblClickEQIndex11()));


	m_pPFGroupBox = new QGroupBox(tr("Band Pass Filter Equalizer"), m_pBottomFrame);
	m_pPFGroupBox->setObjectName(QStringLiteral("SpeakerGroupBox"));
	{
		m_pTypeGroup = new QGroupBox(tr("Type:"), m_pPFGroupBox);
		m_pTypeGroup->setObjectName(QStringLiteral("SpeakerGroupBox"));
		m_pTypeGroup->setFlat(true);

		m_pIIRRadio = new QRadioButton(tr("IIR"), m_pTypeGroup);
		m_pFIRRadio = new QRadioButton(tr("FIR"), m_pTypeGroup);
//		m_pFIRRadio->setObjectName(QStringLiteral("WhiteFont14"));
		connect(m_pIIRRadio, SIGNAL(toggled(bool)), this, SLOT(onPFTypeToggled(bool)));	
//		m_pFIRRadio->setChecked(true);



		m_pPFSlopeLabel = new QLabel(tr("Slope:"), m_pPFGroupBox);
		m_pPFSlopeLabel->setAlignment(Qt::AlignRight | Qt::AlignVCenter);

		m_pTabLabel = new QLabel(tr("Tab:"), m_pPFGroupBox);
		m_pTabLabel->setAlignment(Qt::AlignRight | Qt::AlignVCenter);

		m_pPFFrequencyLabel = new QLabel(tr("Frequency:"), m_pPFGroupBox);
		m_pPFFrequencyLabel->setAlignment(Qt::AlignRight | Qt::AlignVCenter);

		m_pPFFrequencyLabel1 = new QLabel(tr("Frequency"), m_pPFGroupBox);
		m_pPFFrequencyLabel1->setAlignment(Qt::AlignCenter);

		m_pSlopCombo_PF = new QComboBox(m_pPFGroupBox);
		int slopeCount = getTableCount(g_PFIIRTypeTable);
		for (int i = 0; i < slopeCount; i++)
		{
			m_pSlopCombo_PF->addItem(getTableString(g_PFIIRTypeTable, i));
		}
		connect(m_pSlopCombo_PF, SIGNAL(currentIndexChanged(int)), this, SLOT(onPFSlopeChanged(int)));

		m_pEnableButton_PF = new MarkButton(tr("Enabled"), m_pPFGroupBox);
		m_pEnableButton_PF->setObjectName(QStringLiteral("enableGreenButton"));
		m_pEnableButton_PF->mark(true);
		connect(m_pEnableButton_PF, SIGNAL(clicked()), this, SLOT(onClickPFEnable()));

		m_pTabSpin = new QSpinBox(m_pPFGroupBox);
		m_pTabSpin->setRange(50, 384);
		connect(m_pTabSpin, SIGNAL(valueChanged(int)), this, SLOT(onTabChanged(int)));

		m_pFreqSpin_PF = new QDoubleSpinBox(m_pPFGroupBox);
		m_pFreqSpin_PF->setSuffix(" Hz");
		m_pFreqSpin_PF->setRange(MIN_EQ_FREQUENCY, MAX_EQ_FREQUENCY);
		m_pFreqSpin_PF->setDecimals(1);
		connect(m_pFreqSpin_PF, SIGNAL(valueChanged(double)), this, SLOT(onFreqChanged_PF(double)));

		QStringList sliderStringList;
		sliderStringList << "20" << "200" << "2K" << "20K";
		m_pPFSlider = new ButtonSlider(m_pPFGroupBox);
		m_pPFSlider->setMark(4, 2, sliderStringList);
		m_pPFSlider->setMarkLength(7, 14);
		m_pPFSlider->setMarkSide(true, true);
		m_pPFSlider->setRange(0, 80);
		m_pPFSlider->setSingleStep(1);
		m_pPFSlider->setPageStep(10);
		m_pPFSlider->setGrooveHeight(30);
		connect(m_pPFSlider, SIGNAL(valueChanged(int)), this, SLOT(onPFSliderChanged(int)));
	}
	
	m_pFIRGroupBox = new QGroupBox(tr("FIR Equalizer"), m_pBottomFrame);
	m_pFIRGroupBox->setObjectName(QStringLiteral("SpeakerGroupBox"));
	{
		m_pFIRLabel = new QLabel(tr("384 tabs FIR:"), m_pFIRGroupBox);
		m_pFIRLabel->setAlignment(Qt::AlignLeft | Qt::AlignVCenter);

		m_pFirEnableButton = new MarkButton(tr("Enable FIR"), m_pFIRGroupBox);
		connect(m_pFirEnableButton, SIGNAL(clicked()), this, SLOT(onClickFIREnable()));
		m_pFirEnableButton->setObjectName(QStringLiteral("enableGreenButton"));

		m_pFIRDefaultButton = new QPushButton(tr("Set to Default"), m_pFIRGroupBox);
		connect(m_pFIRDefaultButton, SIGNAL(clicked()), this, SLOT(onClickFIRDefault()));

		m_pFIRPasteButton = new QPushButton(tr("Paste from Clipboard"), m_pFIRGroupBox);
		connect(m_pFIRPasteButton, SIGNAL(clicked()), this, SLOT(onClickFIRPaste()));

		m_pFIREdit = new QTextEdit(m_pFIRGroupBox);
//		m_pFIREdit->setWordWrap(true);
		m_pFIREdit->setReadOnly(true);
	}
	m_pFIRGroupBox->hide();

	m_pPEQGroupBox = new QGroupBox(tr("PEQ Equalizer"), m_pBottomFrame);
	m_pPEQGroupBox->setObjectName(QStringLiteral("SpeakerGroupBox"));
	{
		m_TypeLabel = new QLabel(tr("Type:"), m_pPEQGroupBox);
		m_TypeLabel->setAlignment(Qt::AlignRight | Qt::AlignVCenter);

		m_pSlopeLabel = new QLabel(tr("Slope:"), m_pPEQGroupBox);
		m_pSlopeLabel->setAlignment(Qt::AlignRight | Qt::AlignVCenter);

		m_pFrequencyLabel = new QLabel(tr("Frequency:"), m_pPEQGroupBox);
		m_pFrequencyLabel->setAlignment(Qt::AlignRight | Qt::AlignVCenter);

		m_pQLabel = new QLabel(tr("Q:"), m_pPEQGroupBox);
		m_pQLabel->setAlignment(Qt::AlignRight | Qt::AlignVCenter);

		m_pEnableButton_PEQ = new MarkButton(tr("Enabled"), m_pPEQGroupBox);
		m_pEnableButton_PEQ->setObjectName(QStringLiteral("enableGreenButton"));
		m_pEnableButton_PEQ->mark(true);
		connect(m_pEnableButton_PEQ, SIGNAL(clicked()), this, SLOT(onClickPEQEnable()));

		m_pTypeCombo = new QComboBox(m_pPEQGroupBox);
		m_pTypeCombo->addItem(tr("Bell"));
		m_pTypeCombo->addItem(tr("Low Shelf"));
		m_pTypeCombo->addItem(tr("High shelf"));
		connect(m_pTypeCombo, SIGNAL(currentIndexChanged(int)), this, SLOT(onPEQTypeChanged(int)));

		m_pDeleteButton = new QPushButton(tr("Delete"), m_pPEQGroupBox);
		connect(m_pDeleteButton, SIGNAL(clicked()), this, SLOT(onClickPEQDelete()));



		m_pSlopeBox = new QDoubleSpinBox(m_pPEQGroupBox);
		m_pSlopeBox->setSuffix(" dB/Oct");
		m_pSlopeBox->setRange(MIN_SLOPE, MAX_SLOPE);
		m_pSlopeBox->setSingleStep(0.01);
		m_pSlopeBox->setDecimals(2);
		connect(m_pSlopeBox, SIGNAL(valueChanged(double)), this, SLOT(onPEQSlopeChanged(double)));

		m_pGainLabel = new QLabel(tr("Gain:"), m_pPEQGroupBox);
		m_pGainLabel->setAlignment(Qt::AlignRight | Qt::AlignVCenter);

		m_pGainSpin = new QDoubleSpinBox(m_pPEQGroupBox);
		m_pGainSpin->setSuffix(" dB");
		m_pGainSpin->setRange(-20.0, 20.0);
		m_pGainSpin->setSingleStep(0.1);
		m_pGainSpin->setDecimals(1);
		connect(m_pGainSpin, SIGNAL(valueChanged(double)), this, SLOT(onGainChanged(double)));

		m_pFrequencySpin = new QDoubleSpinBox(m_pPEQGroupBox);
		m_pFrequencySpin->setSuffix(" Hz");
		m_pFrequencySpin->setRange(MIN_EQ_FREQUENCY, MAX_EQ_FREQUENCY);
		m_pFrequencySpin->setDecimals(1);
		connect(m_pFrequencySpin, SIGNAL(valueChanged(double)), this, SLOT(onFreqChanged(double)));

		m_pQSpin = new QDoubleSpinBox(m_pPEQGroupBox);
		m_pQSpin->setRange(MIN_Q, MAX_Q);
		m_pQSpin->setDecimals(3);
		connect(m_pQSpin, SIGNAL(valueChanged(double)), this, SLOT(onQChanged(double)));

		m_pGainSliderLabel = new QLabel(tr("Gain"), m_pPEQGroupBox);
		m_pGainSliderLabel->setAlignment(Qt::AlignCenter | Qt::AlignVCenter);

		m_pGainSlider = new ButtonSlider(m_pPEQGroupBox);
		m_pGainSlider->setTextColor(QColor(0x10, 0x10, 0x10));
		m_pGainSlider->setMarkColor(QColor(0x10, 0x10, 0x10));
	//	m_pGainSlider->addExtraLongMark(70, " 7", true);
	//	m_pGainSlider->addExtraShortMark(0, " 0", false);
	//	m_pGainSlider->addExtraShortMark(100, " 10", false);
		QStringList sliderStringList;
		sliderStringList << "-20dB" << "0dB" << "20dB";
		m_pGainSlider->setMark(3, 4, sliderStringList);
		m_pGainSlider->setMarkLength(7, 14);
		m_pGainSlider->setMarkSide(true, true);
		m_pGainSlider->setRange(-200, 200);
		m_pGainSlider->setGrooveHeight(30);
		m_pGainSlider->setSingleStep(1);
		m_pGainSlider->setPageStep(10);
		m_pGainSlider->setValue(0);
		connect(m_pGainSlider, SIGNAL(valueChanged(int)), this, SLOT(onGainSliderChanged(int)));
	}
	
	m_pGeneralGroupBox = new QGroupBox(tr("General setting"), m_pBottomFrame);
	m_pGeneralGroupBox->setObjectName(QStringLiteral("SpeakerGroupBox"));
	{
		m_pGainLabel2 = new QLabel(tr("Gain:"), m_pGeneralGroupBox); 
		m_pGainLabel2->setAlignment(Qt::AlignRight | Qt::AlignVCenter);

		m_pDelayLabel = new QLabel(tr("Delay:"), m_pGeneralGroupBox); 
		m_pDelayLabel->setAlignment(Qt::AlignRight | Qt::AlignVCenter);

//		m_pPolarityLabel = new QLabel(tr("Polarity:"), m_pGeneralGroupBox);
//		m_pPolarityLabel->setAlignment(Qt::AlignRight | Qt::AlignVCenter);

		m_pGainSpin2 = new QDoubleSpinBox(m_pGeneralGroupBox);
		m_pGainSpin2->setRange(-120.0, 20.0);
		m_pGainSpin2->setSingleStep(0.1);
		m_pGainSpin2->setDecimals(1);
		m_pGainSpin2->setSuffix(" dB");
		connect(m_pGainSpin2, SIGNAL(valueChanged(double)), this, SLOT(onGeneralGainChanged(double)));

		m_pDelaySpin = new QDoubleSpinBox(m_pGeneralGroupBox);
		m_pDelaySpin->setRange(0.0, 20.0);
		m_pDelaySpin->setSingleStep(0.05);
		m_pDelaySpin->setDecimals(2);
		m_pDelaySpin->setSuffix(" ms");
		connect(m_pDelaySpin, SIGNAL(valueChanged(double)), this, SLOT(onGeneralDelayChanged(double)));

		m_pPolarityButton = new MarkButton(tr("Polarity (+)"), m_pGeneralGroupBox);
		m_pPolarityButton->setObjectName(QStringLiteral("disabledYellowButton"));
		connect(m_pPolarityButton, SIGNAL(clicked()), this, SLOT(onClickGeneralPolarity()));
	}

	m_pLimiterGroupBox = new QGroupBox(tr("Output limiter"), m_pBottomFrame);
	m_pLimiterGroupBox->setObjectName(QStringLiteral("SpeakerGroupBox"));
	{
		m_pThresholdLabel = new QLabel(tr("Threshold:"), m_pLimiterGroupBox); 
		m_pThresholdLabel->setAlignment(Qt::AlignRight | Qt::AlignVCenter);

		m_pAttackLabel = new QLabel(tr("Attack time:"), m_pLimiterGroupBox); 
		m_pAttackLabel->setAlignment(Qt::AlignRight | Qt::AlignVCenter);

		m_pReleaseLabel = new QLabel(tr("Release:"), m_pLimiterGroupBox);
		m_pReleaseLabel->setAlignment(Qt::AlignRight | Qt::AlignVCenter);
	
		m_pHoldLabel = new QLabel(tr("Hold:"), m_pLimiterGroupBox);
		m_pHoldLabel->setAlignment(Qt::AlignRight | Qt::AlignVCenter);

		m_pLimiterEnableButton = new MarkButton(tr(" Enable "), m_pLimiterGroupBox);
		m_pLimiterEnableButton->setObjectName(QStringLiteral("enableGreenButton"));
		connect(m_pLimiterEnableButton, SIGNAL(clicked()), this, SLOT(onClickLimiterEnableButton()));
		
		m_pLimiterAutoCheck = new QCheckBox(tr("Auto"), m_pLimiterGroupBox);
		connect(m_pLimiterAutoCheck, SIGNAL(clicked()), this, SLOT(onClickLimiterAuto()));
//		m_pLimiterAutoCheck->setObjectName(QStringLiteral("enableGreenButton"));

		m_pThresholdSpin = new QDoubleSpinBox(m_pLimiterGroupBox);
		m_pThresholdSpin->setRange(-60.0, 0.0);
		m_pThresholdSpin->setSingleStep(0.1);
		m_pThresholdSpin->setDecimals(1);
		m_pThresholdSpin->setSuffix(" dB");
		connect(m_pThresholdSpin, SIGNAL(valueChanged(double)), this, SLOT(onLimiterThresholdChanged(double)));

		m_pAttackSpin = new QDoubleSpinBox(m_pLimiterGroupBox);
		m_pAttackSpin->setRange(0.1, 200.0);
		m_pAttackSpin->setSingleStep(0.1);
		m_pAttackSpin->setDecimals(1);
		m_pAttackSpin->setSuffix(" ms");
		connect(m_pAttackSpin, SIGNAL(valueChanged(double)), this, SLOT(onLimiterAttackChanged(double)));
		
			
		m_pReleaseSpin = new QSpinBox(m_pLimiterGroupBox);
		m_pReleaseSpin->setRange(5, 360);
		m_pReleaseSpin->setSingleStep(1);
		m_pReleaseSpin->setSuffix(" dB/s");
		connect(m_pReleaseSpin, SIGNAL(valueChanged(int)), this, SLOT(onLimiterReleaseChanged(int)));
		
		m_pHoldSpin = new QDoubleSpinBox(m_pLimiterGroupBox);
		m_pHoldSpin->setRange(0.0, 500.0);
		m_pHoldSpin->setSingleStep(0.1);
		m_pHoldSpin->setDecimals(1);
		m_pHoldSpin->setSuffix(" ms");
		connect(m_pHoldSpin, SIGNAL(valueChanged(double)), this, SLOT(onLimiterHoldChanged(double)));
	}


	for (int i = 0; i < 3; i++)
	{
		m_currentEQIndex[i] = 0;
		m_currentPEQIndex[i] = 0;
		m_currentCrossoverIndex[i] = 8;
	}

	m_pApplyButton->setEnabled(false);
	setMinimumSize(640, 480);
	resize(905, 725);
}

void SpeakerEditor::onClickEQIndex1()
{
	onClickEQIndex(0);
}

void SpeakerEditor::onClickEQIndex2()
{
	onClickEQIndex(1);
}

void SpeakerEditor::onClickEQIndex3()
{
	onClickEQIndex(2);
}

void SpeakerEditor::onClickEQIndex4()
{
	onClickEQIndex(3);
}

void SpeakerEditor::onClickEQIndex5()
{
	onClickEQIndex(4);
}

void SpeakerEditor::onClickEQIndex6()
{
	onClickEQIndex(5);
}

void SpeakerEditor::onClickEQIndex7()
{
	onClickEQIndex(6);
}

void SpeakerEditor::onClickEQIndex8()
{
	onClickEQIndex(7);
}

void SpeakerEditor::onClickEQIndex9()
{
	onClickEQIndex(8);
}

void SpeakerEditor::onClickEQIndex10()
{
	onClickEQIndex(9);
}

void SpeakerEditor::onClickEQIndex11()
{
	onClickEQIndex(10);
}


void SpeakerEditor::onDblClickEQIndex1()
{
	onDblClickEQIndex(0);
}

void SpeakerEditor::onDblClickEQIndex2()
{
	onDblClickEQIndex(1);
}

void SpeakerEditor::onDblClickEQIndex3()
{
	onDblClickEQIndex(2);
}

void SpeakerEditor::onDblClickEQIndex4()
{
	onDblClickEQIndex(3);
}

void SpeakerEditor::onDblClickEQIndex5()
{
	onDblClickEQIndex(4);
}

void SpeakerEditor::onDblClickEQIndex6()
{
	onDblClickEQIndex(5);
}

void SpeakerEditor::onDblClickEQIndex7()
{
	onDblClickEQIndex(6);
}

void SpeakerEditor::onDblClickEQIndex8()
{
	onDblClickEQIndex(7);
}

void SpeakerEditor::onDblClickEQIndex9()
{
	onDblClickEQIndex(8);
}

void SpeakerEditor::onDblClickEQIndex10()
{
	onDblClickEQIndex(9);
}

void SpeakerEditor::onDblClickEQIndex11()
{
//	onDblClickEQIndex(10);
	EQData *pHPFEQ = getEQ(8);
	EQData *pLPFEQ = getEQ(9);

	if ((pHPFEQ->getPFType() == PF_FIR) || (pLPFEQ->getPFType() == PF_FIR))
	{
		return;
	}
	else
	{
		onDblClickEQIndex(10);
	}
}

void SpeakerEditor::resizeEvent(QResizeEvent * /* event */)
{
	int labelWidth = 120;
	int labelHeight = 20;

	int buttonWidth = 80;
	int buttonHeight = 25;

	int bottomHeight = 270;

	m_pScrollArea->setGeometry(0, 0, width(), height());
	int dialogWidth = width();
	int dialogHeight = height();

	if (dialogWidth < MIN_SCROLL_WIDTH)
	{
		dialogWidth = MIN_SCROLL_WIDTH;
	}
	if (dialogHeight < MIN_SCROLL_HEIGHT)
	{
		dialogHeight = MIN_SCROLL_HEIGHT;
	}

	m_pSpeakerNameLabel->setGeometry(dialogWidth / 8, 20, buttonWidth, labelHeight);
	m_pSpeakerNameEdit->setGeometry(dialogWidth / 8 + buttonWidth + 5, 20, 100, labelHeight);

	m_speakerTypeLabel->setGeometry(dialogWidth / 8, 50, buttonWidth, labelHeight);
	m_pSpeakerTypeCombo->setGeometry(dialogWidth / 8 + buttonWidth + 5, 50, 100, labelHeight);

	m_pWritableCheck->setGeometry(dialogWidth / 3 + 20, 20, 100, labelHeight);

	m_pGeneratorGroup->setGeometry(dialogWidth *2/ 3 - 50, 5, 210, 100);
	{
		m_pSignalModeLabel->setGeometry(10, 15, 80, labelHeight);

		m_pSignalModeCombo->setGeometry(90, 15, 100, labelHeight);

		m_pChannelLabel->setGeometry(10, 45, 80, labelHeight);

		m_pChannelButton[0]->setGeometry(90, 45, 45, labelHeight);
		m_pChannelButton[1]->setGeometry(145, 45, 45, labelHeight);
		m_pChannelButton[2]->setGeometry(90, 70, 45, labelHeight);
		m_pChannelButton[3]->setGeometry(145, 70, 45, labelHeight);
	}


//	m_pReadableCheck->setGeometry(dialogWidth *2/ 3, 20, 100, labelHeight);

//	m_pPasswordButton->setGeometry(dialogWidth *2/ 3 + 100, 20, 120, 22); 

	m_pEQTabButton[0]->setGeometry(20, 80, buttonWidth + 2, buttonHeight);
	m_pEQTabButton[1]->setGeometry(20 + buttonWidth, 80, buttonWidth + 2, buttonHeight);
	m_pEQTabButton[2]->setGeometry(20 + buttonWidth * 2, 80, buttonWidth + 2, buttonHeight);

	int frameWidth = dialogWidth - 20 * 2;
	int frameHeight = dialogHeight - 150 - buttonHeight;
	m_pChannelFrame->setGeometry(20, 80 + buttonHeight, frameWidth, frameHeight);
	m_pEQWidget->setGeometry(10, 5, frameWidth - 20, frameHeight - bottomHeight - 20);

	int littleButtonHeight = 23;
	int littleButtonWidth = 23;

	int bottomWidth = frameWidth - 20;
	m_pBottomFrame->setGeometry(10, 5 + frameHeight - bottomHeight - 15, bottomWidth, bottomHeight);

#if 1
	int PEQOffset = 210;
	m_pPEQIndexLabel->setGeometry(bottomWidth / 2 - PEQOffset, 15, labelWidth, littleButtonHeight);
//	for (int i = 0; i < getEQCount(); i++)
	for (int i = 0; i < 8; i++)
	{
		m_pEQButton[i]->setGeometry(bottomWidth / 2 - PEQOffset + labelWidth + 5 + littleButtonWidth * i, 15, littleButtonWidth, littleButtonHeight);
	}
	m_pPEQEnableButton->setGeometry(bottomWidth / 2 - PEQOffset + labelWidth + 5 + 8.5 * littleButtonWidth, 14, buttonWidth, buttonHeight);

	int crossoverLabelWidth = 100;
	m_pCrossOverEQIndexLabel->setGeometry(0, 15, crossoverLabelWidth, littleButtonHeight);
	for (int i = 8; i < 11; i++)
	{
		m_pEQButton[i]->setGeometry(crossoverLabelWidth + 5 + littleButtonWidth * (i - 8), 15, littleButtonWidth, littleButtonHeight);
	}

#endif
	m_pPFGroupBox->setGeometry(5, 50, 300, bottomHeight - 55);
	{
		m_pTypeGroup->setGeometry(40, 25, 140, 45);
		m_pIIRRadio->setGeometry(30, 15, 80, 20);
		m_pFIRRadio->setGeometry(90, 15, 80, 20);

		m_pPFSlopeLabel->setGeometry(15, 80, 60, 20);
		m_pTabLabel->setGeometry(15, 110, 60, 20);
		m_pPFFrequencyLabel->setGeometry(15, 140, 60, 20);
		m_pEnableButton_PF->setGeometry(115, 175, buttonWidth, buttonHeight);
		m_pDeleteButton->setGeometry(20, 175, buttonWidth, buttonHeight);
		m_pPFFrequencyLabel1->setGeometry(190, 10, 100, 20);

		m_pSlopCombo_PF->setGeometry(80, 80, 80, 20);
		m_pTabSpin->setGeometry(80, 110, 80, 20);
		m_pFreqSpin_PF->setGeometry(80, 140, 80, 20);
		m_pPFSlider->setGeometry(190, 35, 100, 160);
	}
	m_pFIRGroupBox->setGeometry(5, 50, 300, bottomHeight - 55);
	{
		m_pFIRLabel->setGeometry(15, 20, 75, 20);
		m_pFIRDefaultButton->setGeometry(85, 20, 85, 22);
		m_pFIRPasteButton->setGeometry(175, 20, 110, 22);

		m_pFIREdit->setGeometry(15, 48, 300 - 30, 163 - 48);	

		m_pFirEnableButton->setGeometry(115, 175, buttonWidth, buttonHeight);
	}

	m_pPEQGroupBox->setGeometry(bottomWidth / 2 - 107, 50, 300, bottomHeight - 55);
	{
		int tempLabelWidth = 60;
		m_TypeLabel->setGeometry(15, 20, tempLabelWidth, labelHeight);
		m_pSlopeLabel->setGeometry(15, 50, tempLabelWidth, labelHeight);
		m_pGainLabel->setGeometry(15, 80, tempLabelWidth, labelHeight);
		m_pFrequencyLabel->setGeometry(15, 110, tempLabelWidth, labelHeight);
		m_pQLabel->setGeometry(15, 140, tempLabelWidth, labelHeight);
		m_pEnableButton_PEQ->setGeometry(115, 175, buttonWidth, buttonHeight);
		m_pTypeCombo->setGeometry(80, 20, 80, labelHeight);
		m_pSlopeBox->setGeometry(80, 50, 80, labelHeight);
		m_pFrequencySpin->setGeometry(80, 110, 80, labelHeight);
		m_pGainSpin->setGeometry(80, 80, 80, labelHeight);

		m_pQSpin->setGeometry(80, 140, 80, labelHeight);
		m_pGainSliderLabel->setGeometry(190, 10, 100, labelHeight);
		m_pGainSlider->setGeometry(190, 35, 100, 160);
	}

	int limiterWidth = 210;
	m_pGeneralGroupBox->setGeometry(bottomWidth - 220, 7, limiterWidth, 100);
//	m_pGeneralGroupBox->setGeometry(10, 3, 220, 140);
	{
		int tempLabelWidth = 50;
		m_pGainLabel2->setGeometry(30, 20, tempLabelWidth, labelHeight);
		m_pDelayLabel->setGeometry(30, 45, tempLabelWidth, labelHeight);
//		m_pPolarityLabel->setGeometry(10, 110, tempLabelWidth, labelHeight);

		m_pGainSpin2->setGeometry(95, 20, 80, labelHeight);
		m_pDelaySpin->setGeometry(95, 45, 80, labelHeight);
		m_pPolarityButton->setGeometry(70, 72, 80, 22);
	}

	m_pLimiterGroupBox->setGeometry(bottomWidth - 220, 115, limiterWidth, bottomHeight - 120);
	{
		m_pLimiterEnableButton->setGeometry(limiterWidth / 3 - 60 / 2 + 10, 17, 60, 22);
		m_pLimiterAutoCheck->setGeometry(limiterWidth * 2 / 3 - 60 / 2 + 10, 17, 60, 22);

		int top = 45;
		int interval = 25;

		m_pThresholdLabel->setGeometry(30, top + interval * 0, 60, 20);
		m_pAttackLabel->setGeometry(30, top + interval * 1, 60, 20);
		m_pReleaseLabel->setGeometry(30, top + interval * 2, 60, 20);
		m_pHoldLabel->setGeometry(30, top + interval * 3, 60, 20);

		m_pThresholdSpin->setGeometry(95, top + interval * 0, 80, 20);
		m_pAttackSpin->setGeometry(95, top + interval * 1, 80, 20);
		m_pReleaseSpin->setGeometry(95, top + interval * 2, 80, 20);
		m_pHoldSpin->setGeometry(95, top + interval * 3, 80, 20);
	}

	m_pLoadButton->setGeometry(dialogWidth / 5 - buttonWidth / 2, dialogHeight - 45, buttonWidth, buttonHeight);
	m_pSaveButton->setGeometry(dialogWidth / 5 * 2 - buttonWidth / 2, dialogHeight - 45, buttonWidth, buttonHeight);
	m_pApplyButton->setGeometry(dialogWidth / 5  * 3- buttonWidth / 2, dialogHeight - 45, buttonWidth, buttonHeight);
	m_pCancelButton->setGeometry(dialogWidth / 5 * 4 - buttonWidth / 2, dialogHeight - 45, buttonWidth, buttonHeight);
}

void SpeakerEditor::setSpeakerData(SpeakerData *pSpeakerData)
{
	m_speakerData = *pSpeakerData;
	m_currentEQTab = 0;
	refresh();

//	m_pApplyButton->setEnabled(pSpeakerData->m_writable);
	m_pLoadButton->setEnabled(pSpeakerData->m_writable);
}

void SpeakerEditor::refresh()
{
	bool oldFlag = m_slotEnableFlag;
	m_slotEnableFlag = false;

	m_pSpeakerNameEdit->setText(m_speakerData.getSpeakerName());

	refreshReadableButton();	/* refresh readable and password button */
	m_pWritableCheck->setChecked(m_speakerData.m_writable);
	m_pSpeakerTypeCombo->setCurrentIndex((int)m_speakerData.m_speakerType);

	/* TAB Button */
	bool delayVisualable = true;
	if (m_speakerData.m_speakerType == SPEAKER_TYPE_TRIAMP)
	{
		QString textList[] = {tr("LF"), tr("MF"), tr("HF")};
		for (int i = 0; i < 3; i++)
		{
			m_pEQTabButton[i]->show();
			m_pEQTabButton[i]->setText(textList[i]);
			m_pEQTabButton[i]->mark(m_currentEQTab == i);		
		}
	}
	else if (m_speakerData.m_speakerType == SPEAKER_TYPE_BIAMP)
	{
		QString textList[] = {tr("LF"), tr("HF")};
		for (int i = 0; i < 2; i++)
		{
			m_pEQTabButton[i]->show();
			m_pEQTabButton[i]->setText(textList[i]);
			m_pEQTabButton[i]->mark(m_currentEQTab == i);		
		}
		m_pEQTabButton[2]->hide();
	}
	else
	{
		for (int i = 0; i < 3; i++)
		{
			m_pEQTabButton[i]->hide();
	
		}	

		if (m_speakerData.m_speakerType == SPEAKER_TYPE_PASSIVE)
		{
		}
		else if (m_speakerData.m_speakerType == SPEAKER_TYPE_SURROUND)
		{
			delayVisualable = false;
		}
		else if (m_speakerData.m_speakerType == SPEAKER_TYPE_SUBWOOFER)
		{
			delayVisualable = false;
		}
	}

	m_pDelaySpin->setEnabled(delayVisualable);

	/* Output Limiter */
	refreshLimiter();

	m_pGainSpin2->setValue(m_speakerData.m_channelData[m_currentEQTab].m_gain);
	m_pDelaySpin->setValue(m_speakerData.m_channelData[m_currentEQTab].m_delayInMs);
	m_pPolarityButton->mark(m_speakerData.m_channelData[m_currentEQTab].m_polarityNormalFlag);
	if (m_speakerData.m_channelData[m_currentEQTab].m_polarityNormalFlag)
	{
		m_pPolarityButton->setText(tr("Polarity (+)"));
	}
	else
	{
		m_pPolarityButton->setText(tr("Polarity (-)"));
	}

	refreshEQIndexButton();

	refreshPEQGroup();
	if (m_currentCrossoverIndex[m_currentEQTab] < 10)
	{
		refreshPFGroup();
	}
	else
	{
		refreshFIRGroup();
	}

	m_pPEQEnableButton->mark(m_speakerData.m_channelData[m_currentEQTab].m_PEQEnableFlag);
	if (m_speakerData.m_channelData[m_currentEQTab].m_PEQEnableFlag)
	{
		m_pPEQEnableButton->setText(tr("PEQ Enabled"));
	}
	else
	{
		m_pPEQEnableButton->setText(tr("Enable All"));	
	}

	m_pEQWidget->update(1);

	m_slotEnableFlag = oldFlag;
}

void SpeakerEditor::refreshEQIndexButton()
{
	int channelIndex = m_currentEQTab;

	for (int i = 0; i <= 7; i++)
	{
		m_pEQButton[i]->enable(m_speakerData.m_channelData[channelIndex].m_EQData[i].getEnableFlag());
		m_pEQButton[i]->focus(i == m_currentPEQIndex[channelIndex]);
		m_pEQButton[i]->update();
	}
	for (int i = 8; i <= 10; i++)
	{
		m_pEQButton[i]->enable(m_speakerData.m_channelData[channelIndex].m_EQData[i].getEnableFlag());
		m_pEQButton[i]->focus(i == m_currentCrossoverIndex[channelIndex]);
		m_pEQButton[i]->update();
	}

	/* For FIR */
	if (getEQCount() == 10)
	{
		m_pEQButton[10]->hide();
	}
	else
	{
		m_pEQButton[10]->show();
	}
}

void SpeakerEditor::refreshLimiter()
{
	bool oldFlag = m_slotEnableFlag;
	m_slotEnableFlag = false;

	LIMITER *pLimiter = &(m_speakerData.m_channelData[m_currentEQTab].m_limiter);

	bool enableFlag = pLimiter->m_enableFlag;
	bool autoFlag = pLimiter->m_autoFlag;

	m_pLimiterEnableButton->mark(enableFlag);
	m_pLimiterAutoCheck->setEnabled(enableFlag);
	m_pLimiterAutoCheck->setChecked(autoFlag);
	m_pThresholdSpin->setValue(pLimiter->m_threshold);
	m_pAttackSpin->setValue(pLimiter->m_attackTimeInMs);
	m_pReleaseSpin->setValue(pLimiter->m_release);
	m_pHoldSpin->setValue(pLimiter->m_holdTimeInMs);

	if (enableFlag == false)
	{
		m_pLimiterEnableButton->setText(tr(" Enable "));
		m_pThresholdLabel->setEnabled(false);
		m_pAttackLabel->setEnabled(false);
		m_pReleaseLabel->setEnabled(false);
		m_pHoldLabel->setEnabled(false);

		m_pThresholdSpin->setEnabled(false);
		m_pAttackSpin->setEnabled(false);
		m_pReleaseSpin->setEnabled(false);
		m_pHoldSpin->setEnabled(false);
	}
	else 
	{
		m_pLimiterEnableButton->setText(tr("Enabled"));
		m_pThresholdLabel->setEnabled(true);
		m_pThresholdSpin->setEnabled(true);

		m_pAttackLabel->setEnabled(!autoFlag);
		m_pReleaseLabel->setEnabled(!autoFlag);
		m_pHoldLabel->setEnabled(!autoFlag);

		m_pAttackSpin->setEnabled(!autoFlag);
		m_pReleaseSpin->setEnabled(!autoFlag);
		m_pHoldSpin->setEnabled(!autoFlag);
	}

	m_slotEnableFlag = oldFlag;
}

void SpeakerEditor::onReadableClicked()
{
	if (m_slotEnableFlag == false)
	{
		return;
	}

//	bool readable = m_pReadableCheck->isChecked();
	m_speakerData.m_readable = true;

	refreshReadableButton();
}

void SpeakerEditor::onWritableClicked()
{
	if (m_slotEnableFlag == false)
	{
		return;
	}

	bool writable = m_pWritableCheck->isChecked();
	m_speakerData.m_writable = writable;

	if (m_speakerData.m_writable == false)
	{
		QMessageBox::warning(this, tr("Warning"), tr("The speaker you are editing is set to unwritable! If it takes effect, you won't modify the settings of this speaker any more!"), QMessageBox::Ok);
	}
}

void SpeakerEditor::refreshReadableButton()
{
	bool oldFlag = m_slotEnableFlag;
	m_slotEnableFlag = false;

#if 0
	bool readable = m_speakerData.m_readable;
//	m_pReadableCheck->setChecked(readable);
	m_pPasswordButton->setEnabled(readable);
	if (readable)
	{
		m_pPasswordButton->mark(!m_speakerData.m_password.isEmpty());
		if (m_speakerData.m_password.isEmpty())
		{
			m_pPasswordButton->setText(tr("Password Disabled"));
		}
		else
		{
			m_pPasswordButton->setText(tr("Password Enabled"));
		}
	}
#endif

	m_slotEnableFlag = oldFlag;
}

void SpeakerEditor::onClickLoadButton()
{
	QString directory = ".";
	QString fileName = QFileDialog::getOpenFileName(
		this,
		"Open Speaker File",
		directory,
		tr("Speaker File(*.spk);; All files (*.*)")
	);

	if (fileName.isEmpty() != false)
	{
		return;
	}

	QFile file(fileName);
    if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
	{
        QMessageBox::warning(this, tr("Save File Error"),
                             tr("Cannot save the speaker settings to file: %1.")
                             .arg(file.fileName()), tr("OK"));
        return;
    }

	QByteArray json = file.readAll();
	file.close();

	SpeakerData speakerData;
	speakerData.m_speakerName = m_speakerData.m_speakerName;
	bool readFlag = speakerData.readFromJSON(QString(json));
	if (readFlag == false)
	{
		QMessageBox::warning(this, tr("Warning"), "Loading failed: Not a valid speaker file!", QMessageBox::Ok);
		return;
	}
	if (speakerData.m_speakerType != m_speakerData.m_speakerType)
	{
		QMessageBox::warning(this, tr("Loading failed"), tr("The speaker type in the loaded file is %1, and you can't apply it at current output channel!").arg(getTableString(g_speakerTypeTable, (int)speakerData.m_speakerType)), QMessageBox::Ok);
		return;
	}
	else
	{
		setSpeakerData(&speakerData);
	}
	enableApplyButton();
}

void SpeakerEditor::onClickSaveButton()
{
	QString directory = ".";
	QString fileName = QFileDialog::getSaveFileName(
		this,
		"Save Speaker File",
		directory,
		tr("Speaker File(*.spk);; All files (*.*)")
	);

	if (fileName.isEmpty() == true)
	{
		return;
	}

	QFile file(fileName);
    if (!file.open(QIODevice::WriteOnly | QIODevice::Text))
	{
        QMessageBox::warning(this, tr("Save File Error"),
                             tr("Cannot save the speaker settings to file: %1.")
                             .arg(file.fileName()), tr("OK"));
        return;
    }

	QByteArray json = m_speakerData.writeToJSON();
	file.write(json);
	file.close();
}

void SpeakerEditor::onClickPasswordButton()
{
	PasswordDlg dlg(this);
	dlg.setPassword(m_speakerData.m_password);
	dlg.setModal(true);
	int ret = dlg.exec();

	if (ret == QDialog::Accepted)
	{
		m_speakerData.m_password = dlg.getPassword();
	}

	refreshReadableButton();
}

void SpeakerEditor::onClickEQTab_0()
{
	if (m_currentEQTab != 0)
	{
		m_currentEQTab = 0;
		refresh();
	}
}

void SpeakerEditor::onClickEQTab_1()
{
	if (m_currentEQTab != 1)
	{
		m_currentEQTab = 1;
		refresh();
	}
}

void SpeakerEditor::onClickEQTab_2()
{
	if (m_currentEQTab != 2)
	{
		m_currentEQTab = 2;
		refresh();
	}
}

void SpeakerEditor::onClickLimiterEnableButton()
{
	LIMITER *pLimiter = &(m_speakerData.m_channelData[m_currentEQTab].m_limiter);
	bool flag = !m_pLimiterEnableButton->getMarkFlag();
	
	pLimiter->m_enableFlag = flag;
	refreshLimiter();

	enableApplyButton();

	emit this->changeLimiterEnable(m_currentEQTab, flag);
}

void SpeakerEditor::onClickLimiterAuto()
{
	if (m_slotEnableFlag == false)
	{
		return;
	}

	LIMITER *pLimiter = &(m_speakerData.m_channelData[m_currentEQTab].m_limiter);
	bool autoFlag = m_pLimiterAutoCheck->isChecked();
	
	pLimiter->m_autoFlag = autoFlag;
	refreshLimiter();
	enableApplyButton();

	emit this->changeLimiterAutoFlag(m_currentEQTab, autoFlag);
}

void SpeakerEditor::onLimiterReleaseChanged(int value)
{
	if (m_slotEnableFlag == false)
	{
		return;
	}

	LIMITER *pLimiter = &(m_speakerData.m_channelData[m_currentEQTab].m_limiter);
	
	pLimiter->m_release = value;
	enableApplyButton();
	emit this->changeLimiterRelease(m_currentEQTab, value);
}

void SpeakerEditor::onLimiterAttackChanged(double value)
{
	if (m_slotEnableFlag == false)
	{
		return;
	}

	LIMITER *pLimiter = &(m_speakerData.m_channelData[m_currentEQTab].m_limiter);
	
	pLimiter->m_attackTimeInMs = value;
	enableApplyButton();
	emit this->changeLimiterAttack(m_currentEQTab, value);
}

void SpeakerEditor::onClickCancelButton()
{

	reject();
}

void SpeakerEditor::onLimiterHoldChanged(double value)
{
	if (m_slotEnableFlag == false)
	{
		return;
	}

	LIMITER *pLimiter = &(m_speakerData.m_channelData[m_currentEQTab].m_limiter);
	
	pLimiter->m_holdTimeInMs = value;
	enableApplyButton();
	emit this->changeLimiterHold(m_currentEQTab, value);
}

void SpeakerEditor::onGeneralGainChanged(double value)
{
	if (m_slotEnableFlag == false)
	{
		return;
	}

	m_speakerData.m_channelData[m_currentEQTab].m_gain = value;
	emit this->changeGeneralGain(m_currentEQTab, value);
	enableApplyButton();
}

void SpeakerEditor::onGeneralDelayChanged(double value)
{
	if (m_slotEnableFlag == false)
	{
		return;
	}

	m_speakerData.m_channelData[m_currentEQTab].m_delayInMs = value;
	emit this->changeGeneralDelay(m_currentEQTab, value);
	enableApplyButton();
}

void SpeakerEditor::onLimiterThresholdChanged(double value)
{
	if (m_slotEnableFlag == false)
	{
		return;
	}

	LIMITER *pLimiter = &(m_speakerData.m_channelData[m_currentEQTab].m_limiter);
	
	pLimiter->m_threshold = value;
	enableApplyButton();
	emit this->changeLimiterThreshold(m_currentEQTab, value);
}

void SpeakerEditor::onClickFIREnable()
{
	if (m_currentCrossoverIndex[m_currentEQTab] != m_currentEQIndex[m_currentEQTab])
	{
		m_currentEQIndex[m_currentEQTab] = m_currentCrossoverIndex[m_currentEQTab];
		m_pEQWidget->update(1);
	}

	EQData *pCurrentEQ = getCurrentEQ();

	EQData *pHPFEQ = getEQ(8);
	EQData *pLPFEQ = getEQ(9);

	if ((pHPFEQ->getPFIIRType() == PF_FIR) || (pLPFEQ->getPFIIRType() == PF_FIR))
	{
		pCurrentEQ->enable(false);
	}
	else
	{
		bool flag = !m_pFirEnableButton->getMarkFlag();
		pCurrentEQ->enable(flag);
	}

	refreshEQIndexButton();
	refreshEQGroup();
	m_pEQWidget->update(2);
	enableApplyButton();
}

void SpeakerEditor::onClickFIRDefault()
{
	if (m_currentCrossoverIndex[m_currentEQTab] != m_currentEQIndex[m_currentEQTab])
	{
		m_currentEQIndex[m_currentEQTab] = m_currentCrossoverIndex[m_currentEQTab];
		m_pEQWidget->update(1);
	}

//	QString defaultText = "1.0";
//	m_speakerData.m_channelData[m_currentEQTab].m_strFIR = defaultText;
//	m_pFIREdit->setText(defaultText);
//	emit this->changeFIRContent(m_currentEQTab, defaultText);
	EQData *pCurrentEQ = getCurrentEQ();
	pCurrentEQ->setUserDefinedFIR("1.0");
	refreshEQGroup();
	m_pEQWidget->update(2);
	enableApplyButton();
}

void SpeakerEditor::onClickFIRPaste()
{
	if (m_currentCrossoverIndex[m_currentEQTab] != m_currentEQIndex[m_currentEQTab])
	{
		m_currentEQIndex[m_currentEQTab] = m_currentCrossoverIndex[m_currentEQTab];
		m_pEQWidget->update(1);
	}

	QClipboard *clipboard = QApplication::clipboard();
	QString oldClip = clipboard->text();
	QString clipText = oldClip;

	clipText.replace("\r", " "); 
	clipText.replace("\n", " "); 
	clipText.replace(",", " "); 
	clipText.replace("\t", " "); 
	QStringList valueList = clipText.split(" ", QString::SkipEmptyParts);
	QRegExp rx(REGULAR_EXPRESSION_FLOAT); 
	int i;
	if (valueList.length() == 0)
	{
		QMessageBox::warning(this, tr("Warning"), QString(tr("Invalid FIR Data, please check the data in the clipboard!")), QMessageBox::Ok);
		return;
	}

	for (i = 0; i < valueList.length(); i++)
	{
		bool flag = rx.exactMatch(valueList.at(i));
		if (flag == false)
		{
			QMessageBox::warning(this, tr("Warning"), QString(tr("Invalid FIR Data, please check the data in the clipboard!")), QMessageBox::Ok);
//			QMessageBox::warning(this, tr("Warning"), QString(tr("Invalid FIR Data, please check the data at index %1: %2!")).arg(i + 1).arg(valueList.at(i)), QMessageBox::Ok);
			return;
		}
	}
//	qDebug() << clipText;

	QString newClip = valueList.at(0);
	for (i = 1; i < valueList.length(); i++)
	{
		newClip += QString(",");
		newClip += valueList.at(i);
	}

#if 0
//	QRegExp rx("(\\s*[-+]?[0-9]*\.?[0-9]+\\s*\.){0,383}(\\s*[-+]?[0-9]*\.?[0-9]+\\s*\.?\\s*)"); 
	QRegExp rx("(\\s*[-+]?[0-9]*\.?[0-9]+\\s*,?){0,383}(\\s*[-+]?[0-9]*\.?[0-9]+\\s*,?\\s*)"); 

	bool flag = rx.exactMatch(clipText);
	if (flag == false)
	{
		QMessageBox::warning(this, tr("Warning"), tr("Invalid FIR Data, please check it again!"), QMessageBox::Ok);
		return;
	}

//	m_speakerData.m_channelData[m_currentEQTab].m_strFIR = clipText;
//	m_pFIREdit->setText(clipText);	
//	emit this->changeFIRContent(m_currentEQTab, clipText);
#endif
	if (i > 384)
	{
		QMessageBox::warning(this, tr("Warning"), QString(tr("Invalid FIR Data: CPi2000 can only support those FIR which tab is not bigger than 384!")), QMessageBox::Ok);
		return;
	}

	EQData *pCurrentEQ = getCurrentEQ();
	pCurrentEQ->setUserDefinedFIR(newClip);
//	pCurrentEQ->setUserDefinedFIR(oldClip);

	refreshEQGroup();
	m_pEQWidget->update(2);
	enableApplyButton();
}

void SpeakerEditor::onClickGeneralPolarity()
{
	bool flag = !m_pPolarityButton->getMarkFlag();

	m_pPolarityButton->mark(flag);
	if (flag)
	{
		m_pPolarityButton->setText(tr("Polarity (+)"));
	}
	else
	{
		m_pPolarityButton->setText(tr("Polarity (-)"));
	}
	m_speakerData.m_channelData[m_currentEQTab].m_polarityNormalFlag = flag;
	emit this->changeGeneralPolarity(m_currentEQTab, flag);
	enableApplyButton();
}

void SpeakerEditor::onClickEQIndex(int eqIndex)
{
	m_currentEQIndex[m_currentEQTab] = eqIndex;

	if (eqIndex < 8)
	{
		m_currentPEQIndex[m_currentEQTab] = eqIndex;
	}
	else
	{
		m_currentCrossoverIndex[m_currentEQTab] = eqIndex;
	}

	refreshEQIndexButton();
	refreshEQGroup();
	m_pEQWidget->update(1);
}

void SpeakerEditor::onDblClickEQIndex(int eqIndex)
{
	bool flag = !m_speakerData.m_channelData[m_currentEQTab].m_EQData[eqIndex].getEnableFlag();
	m_speakerData.m_channelData[m_currentEQTab].m_EQData[eqIndex].enable(flag);

	refreshEQIndexButton();
	refreshEQGroup();
	m_pEQWidget->update(2);
	enableApplyButton();
}

int SpeakerEditor::getCurrentEQIndex()
{
	return (m_currentEQIndex[m_currentEQTab]);
}

EQData * SpeakerEditor::getCurrentEQ()
{
	EQData * pEQData = m_speakerData.m_channelData[m_currentEQTab].m_EQData + m_currentEQIndex[m_currentEQTab];
	return pEQData;
}

EQData * SpeakerEditor::getEQ(int index)
{
	EQData * pEQData = m_speakerData.m_channelData[m_currentEQTab].m_EQData + index;

	return (pEQData);
}

EQData * SpeakerEditor::getHPF()
{
	return (getEQ(8));
}

EQData * SpeakerEditor::getLPF()
{
	return (getEQ(9));
}

EQData * SpeakerEditor::getFIR()
{
	return (getEQ(10));
}

int SpeakerEditor::getEQCount()
{
	if ((m_speakerData.m_speakerType == SPEAKER_TYPE_SURROUND) || (m_speakerData.m_speakerType == SPEAKER_TYPE_SUBWOOFER))
	{
		return (10);
	}
	else
	{
		return (11);
	}
}

void SpeakerEditor::refreshEQGroup()
{
	bool oldFlag = m_slotEnableFlag;
	m_slotEnableFlag = false;

//	int eqIndex = m_currentEQIndex[m_currentEQTab];
	EQData * pCurrentEQ = getCurrentEQ();
//	EQData * pHPF = getHPF();
//	EQData * pLPF = getLPF();
//	EQData * pFIR = getFIR();

	EQType eqType = pCurrentEQ->getType();
	switch (eqType)
	{
	case EQ_PEAKING:
	case EQ_H_SHELF:
	case EQ_L_SHELF:
		refreshPEQGroup();
		break;
	case EQ_LPF:
	case EQ_HPF:
		refreshPFGroup();
		break;

	case EQ_USER_DEFINED_FIR:
		refreshFIRGroup();
		break;
	}

	m_slotEnableFlag = oldFlag;
}

void SpeakerEditor::refreshPEQGroup()
{
	bool oldFlag = m_slotEnableFlag;
	m_slotEnableFlag = false;

	m_pPEQGroupBox->show();

	int index = m_currentPEQIndex[m_currentEQTab];
	EQData *pCurrentEQ = getEQ(index);
	m_pEnableButton_PEQ->mark(pCurrentEQ->getEnableFlag());
	if (pCurrentEQ->getEnableFlag())
	{
		m_pEnableButton_PEQ->setText(tr("Enabled"));
	}
	else
	{
		m_pEnableButton_PEQ->setText(tr("Enable"));
	}

	if (pCurrentEQ->getType() == EQ_PEAKING)
	{
		m_pTypeCombo->setCurrentIndex(0);
	}
	else if (pCurrentEQ->getType() == EQ_L_SHELF)
	{
		m_pTypeCombo->setCurrentIndex(1);
	}
	else if (pCurrentEQ->getType() == EQ_H_SHELF)
	{
		m_pTypeCombo->setCurrentIndex(2);
	}

	m_pSlopeBox->setValue(pCurrentEQ->getSlope());
	m_pGainSpin->setValue(pCurrentEQ->getGain());
	m_pGainSlider->setValue(pCurrentEQ->getGain() * 10);

	m_pFrequencySpin->setValue(pCurrentEQ->getFrequency());
	if (pCurrentEQ->getFrequency() <= 200)
	{
		m_pFrequencySpin->setSingleStep(0.1);
	}
	else if (pCurrentEQ->getFrequency() <= 2000)
	{
		m_pFrequencySpin->setSingleStep(1);
	}
	else
	{
		m_pFrequencySpin->setSingleStep(10);
	}

	m_pQSpin->setValue(pCurrentEQ->getQ());
	if (pCurrentEQ->getQ() < 1)
	{
		m_pQSpin->setSingleStep(0.001);
	}
	else if (pCurrentEQ->getQ() < 10)
	{
		m_pQSpin->setSingleStep(0.01);
	}
	else
	{
		m_pQSpin->setSingleStep(0.01);		
	}

	if (pCurrentEQ->getType() == EQ_PEAKING)
	{
		m_pSlopeBox->setEnabled(false);
		m_pSlopeLabel->setEnabled(false);

		m_pQLabel->setEnabled(true);
		m_pQSpin->setEnabled(true);
	}
	else
	{
		m_pSlopeBox->setEnabled(true);
		m_pSlopeLabel->setEnabled(true);

		m_pQLabel->setEnabled(false);
		m_pQSpin->setEnabled(false);
	}

	m_slotEnableFlag = oldFlag;
}

void SpeakerEditor::refreshPFGroup()
{
	bool oldFlag = m_slotEnableFlag;
	m_slotEnableFlag = false;

	m_pPFGroupBox->show();
	m_pFIRGroupBox->hide();

	int index = m_currentCrossoverIndex[m_currentEQTab];
	EQData *pCurrentEQ = getEQ(index);
	m_pEnableButton_PF->mark(pCurrentEQ->getEnableFlag());
	if (pCurrentEQ->getEnableFlag())
	{
		m_pEnableButton_PF->setText(tr("Enabled"));
	}
	else
	{
		m_pEnableButton_PF->setText(tr("Enable"));
	}

	m_pSlopCombo_PF->setCurrentIndex((int)pCurrentEQ->getPFIIRType());
	qreal freq = pCurrentEQ->getFrequency();
	m_pFreqSpin_PF->setValue(freq);

	if (freq <= 200)
	{
		m_pFreqSpin_PF->setSingleStep(0.1);
	}
	else if (freq <= 2000)
	{
		m_pFreqSpin_PF->setSingleStep(1);
	}
	else
	{
		m_pFreqSpin_PF->setSingleStep(10);
	}


	EQData *pFIREQ = getEQ(10);
	if ((m_speakerData.m_speakerType == SPEAKER_TYPE_SUBWOOFER) || (m_speakerData.m_speakerType == SPEAKER_TYPE_SURROUND))
	{
		m_pFIRRadio->setEnabled(false);
	}
	else if (pFIREQ->getEnableFlag() == true)
	{
		m_pFIRRadio->setEnabled(false);
	}
	else
	{
//		m_pFIRRadio->setEnabled(true);

		EQData *pAnotherEQ; 
		int anotherFIRTab = 0;
		if (pCurrentEQ->getType() == EQ_HPF)
		{
			pAnotherEQ = getEQ(9);
		}
		else
		{
			pAnotherEQ = getEQ(8);
		}

		if (pAnotherEQ->getPFType() == PF_FIR)
		{
			anotherFIRTab = pAnotherEQ->getFIRTab();
			if ((anotherFIRTab <= (384 - 50)) && (freq >= MIN_FIR_FREQ))
			{
				m_pFIRRadio->setEnabled(true);
			}
			else
			{
				m_pFIRRadio->setEnabled(false);
			}
		}
		else
		{
			if (freq >= MIN_FIR_FREQ)
			{
				m_pFIRRadio->setEnabled(true);
			}
			else
			{
				m_pFIRRadio->setEnabled(false);
			}
		}
	}

	if (pCurrentEQ->getPFType() == PF_IIR)
	{
		m_pIIRRadio->setChecked(true);
//		m_pFIRRadio->setChecked(false);
		m_pPFSlopeLabel->setEnabled(true);
		m_pSlopCombo_PF->setEnabled(true);

		m_pTabLabel->setEnabled(false);
		m_pTabSpin->setEnabled(false);
	}
	else
	{
		m_pFIRRadio->setChecked(true);
		if (freq >= MIN_FIR_FREQ)
		{
			m_pFIRRadio->setEnabled(true);
		}
		else
		{
			m_pFIRRadio->setEnabled(false);
		}
		m_pPFSlopeLabel->setEnabled(false);
		m_pSlopCombo_PF->setEnabled(false);

		m_pTabLabel->setEnabled(true);
		m_pTabSpin->setEnabled(true);

		EQData *pAnotherEQ; 
		int anotherFIRTab = 0;
		if (pCurrentEQ->getType() == EQ_HPF)
		{
			pAnotherEQ = getEQ(9);
		}
		else
		{
			pAnotherEQ = getEQ(8);
		}
		if (pAnotherEQ->getPFType() == PF_IIR)
		{
			anotherFIRTab = 0;
		}
		else
		{
			anotherFIRTab = pAnotherEQ->getFIRTab();
		}
		m_pTabSpin->setRange(50, 384 - anotherFIRTab);
	}

	m_pTabSpin->setValue(pCurrentEQ->getFIRTab());

	qreal singleStep = pow(10, 3.0/80);
	int value = qRound(log10(freq/20) / log10(singleStep));
	m_pPFSlider->setValue(value);

	m_slotEnableFlag = oldFlag;
}

void SpeakerEditor::refreshFIRGroup()
{
	bool oldFlag = m_slotEnableFlag;
	m_slotEnableFlag = false;

	m_pPFGroupBox->hide();
	m_pFIRGroupBox->show();

	int index = m_currentCrossoverIndex[m_currentEQTab];
	EQData *pCurrentEQ = getEQ(index);
	bool firEnableFlag = pCurrentEQ->getEnableFlag();

	m_pFIRLabel->setEnabled(firEnableFlag);

	m_pFIREdit->setText(pCurrentEQ->getUserDefinedFIR());

	m_pFirEnableButton->mark(firEnableFlag);
	if (firEnableFlag == true)
	{
		m_pFirEnableButton->setText(tr("Enabled"));
	}
	else
	{
		m_pFirEnableButton->setText(tr("Enable"));
	}

	m_pFIRDefaultButton->setEnabled(firEnableFlag);
	m_pFIRPasteButton->setEnabled(firEnableFlag);
	m_pFIREdit->setEnabled(firEnableFlag);

	bool firEnabled = true;
	if ((m_speakerData.m_speakerType == SPEAKER_TYPE_SUBWOOFER) || (m_speakerData.m_speakerType == SPEAKER_TYPE_SURROUND))
	{
		firEnabled = false;
	}
	else
	{
		EQData *pHPFEQ = getEQ(8);
		EQData *pLPFEQ = getEQ(9);

		if ((pHPFEQ->getPFType() == PF_FIR) || (pLPFEQ->getPFType() == PF_FIR))
		{
			firEnabled = false;
		}
		else
		{
			firEnabled = true;
		}
	}
	m_pFirEnableButton->setEnabled(firEnabled);

	m_slotEnableFlag = oldFlag;
}

void SpeakerEditor::onClickApplyButton()
{
	g_waittingWidget.startWaitting(this);
	m_pApplyButton->setEnabled(false);

	emit this->applyToDevice(&m_speakerData);

	g_pApp->msleep(1000);
	g_waittingWidget.stopWaitting();
//	accept();
}

void SpeakerEditor::onClickPEQDelete()
{
	if (m_slotEnableFlag == false)
	{
		return;
	}

	if (m_currentPEQIndex[m_currentEQTab] != m_currentEQIndex[m_currentEQTab])
	{
		m_currentEQIndex[m_currentEQTab] = m_currentPEQIndex[m_currentEQTab];
		m_pEQWidget->update(1);
	}

	EQData *pCurrentEQ = getCurrentEQ();
	qreal oldGain = pCurrentEQ->getGain();
	pCurrentEQ->setGain(0);
	refreshEQGroup();
	m_pEQWidget->update(2);

	if ((fabs(oldGain) > 0.05) && (pCurrentEQ->getEnableFlag() == true))
	{
		enableApplyButton();
	}
}

void SpeakerEditor::onGainChanged(double value)
{
	if (m_slotEnableFlag == false)
	{
		return;
	}

	if (m_currentPEQIndex[m_currentEQTab] != m_currentEQIndex[m_currentEQTab])
	{
		m_currentEQIndex[m_currentEQTab] = m_currentPEQIndex[m_currentEQTab];
		m_pEQWidget->update(1);
	}

	EQData *pCurrentEQ = getCurrentEQ();
	pCurrentEQ->setGain(value);
	refreshEQGroup();
	m_pEQWidget->update(2);
	if (pCurrentEQ->getEnableFlag() == true)
	{
		enableApplyButton();
	}
}

void SpeakerEditor::onGainSliderChanged(int value)
{
	if (m_slotEnableFlag == false)
	{
		return;
	}

	if (m_currentPEQIndex[m_currentEQTab] != m_currentEQIndex[m_currentEQTab])
	{
		m_currentEQIndex[m_currentEQTab] = m_currentPEQIndex[m_currentEQTab];
		m_pEQWidget->update(1);
	}

	EQData *pCurrentEQ = getCurrentEQ();
	pCurrentEQ->setGain((double)value / 10);
	refreshEQGroup();
	m_pEQWidget->update(2);
	if (pCurrentEQ->getEnableFlag() == true)
	{
		enableApplyButton();
	}
}

void SpeakerEditor::onFreqChanged(double value)
{
	if (m_slotEnableFlag == false)
	{
		return;
	}
	
	if (m_currentPEQIndex[m_currentEQTab] != m_currentEQIndex[m_currentEQTab])
	{
		m_currentEQIndex[m_currentEQTab] = m_currentPEQIndex[m_currentEQTab];
		m_pEQWidget->update(1);
	}

	EQData *pCurrentEQ = getCurrentEQ();
	pCurrentEQ->setFrequency(value);

//	refreshEQGroup();
	m_pEQWidget->update(2);
	if (pCurrentEQ->getEnableFlag() == true)
	{
		enableApplyButton();
	}
}

void SpeakerEditor::onFreqChanged_PF(double value)
{
	if (m_slotEnableFlag == false)
	{
		return;
	}

	if (m_currentCrossoverIndex[m_currentEQTab] != m_currentEQIndex[m_currentEQTab])
	{
		m_currentEQIndex[m_currentEQTab] = m_currentCrossoverIndex[m_currentEQTab];
		m_pEQWidget->update(1);
	}
	int currentIndex = getCurrentEQIndex();
	EQData *pCurrentEQ = getCurrentEQ();
	m_speakerData.m_channelData[m_currentEQTab].setPFFrequency(currentIndex, value);

	refreshEQGroup();
	m_pEQWidget->update(2);
	if (pCurrentEQ->getEnableFlag() == true)
	{
		enableApplyButton();
	}
}

void SpeakerEditor::onQChanged(double value)
{
	if (m_slotEnableFlag == false)
	{
		return;
	}

	if (m_currentPEQIndex[m_currentEQTab] != m_currentEQIndex[m_currentEQTab])
	{
		m_currentEQIndex[m_currentEQTab] = m_currentPEQIndex[m_currentEQTab];
		m_pEQWidget->update(1);
	}
	EQData *pCurrentEQ = getCurrentEQ();
	pCurrentEQ->setQ(value);

//	refreshEQGroup();
	m_pEQWidget->update(2);
	if (pCurrentEQ->getEnableFlag() == true)
	{
		enableApplyButton();
	}
}

void SpeakerEditor::onClickPFEnable()
{
	if (m_slotEnableFlag == false)
	{
		return;
	}

	if (m_currentCrossoverIndex[m_currentEQTab] != m_currentEQIndex[m_currentEQTab])
	{
		m_currentEQIndex[m_currentEQTab] = m_currentCrossoverIndex[m_currentEQTab];
		m_pEQWidget->update(1);
	}	
	EQData *pCurrentEQ = getCurrentEQ();
	bool flag = !pCurrentEQ->getEnableFlag();
	pCurrentEQ->enable(flag);

	refreshEQIndexButton();
	refreshEQGroup();
	m_pEQWidget->update(2);
	enableApplyButton();
}


void SpeakerEditor::onPFSliderChanged(int nValue)
{
	if (m_slotEnableFlag == false)
	{
		return;
	}

	if (m_currentCrossoverIndex[m_currentEQTab] != m_currentEQIndex[m_currentEQTab])
	{
		m_currentEQIndex[m_currentEQTab] = m_currentCrossoverIndex[m_currentEQTab];
		m_pEQWidget->update(1);
	}	
	qreal singleStep = pow(10, 3.0/80);
	qreal freq = 20 * pow(singleStep, nValue);

	EQData *pCurrentEQ = getCurrentEQ();

	int currentIndex = getCurrentEQIndex();
	m_speakerData.m_channelData[m_currentEQTab].setPFFrequency(currentIndex, freq);

	refreshEQGroup();
	m_pEQWidget->update(2);
	if (pCurrentEQ->getEnableFlag() == true)
	{
		enableApplyButton();
	}
}


void SpeakerEditor::onClickAllPEQEnable()
{
//connect(m_pPEQEnableButton, SIGNAL(clicked()), this, SLOT(onClickEnableAll()));
	bool flag = !m_pPEQEnableButton->getMarkFlag();
	m_pPEQEnableButton->mark(flag);

	if (flag)
	{
		m_pPEQEnableButton->setText(tr("PEQ Enabled"));		
	}
	else
	{
		m_pPEQEnableButton->setText(tr("Enable All"));	
	}

	m_speakerData.m_channelData[m_currentEQTab].m_PEQEnableFlag = flag;
	m_pEQWidget->update(1);
	enableApplyButton();
}

void SpeakerEditor::onClickPEQEnable()
{
	if (m_slotEnableFlag == false)
	{
		return;
	}

	if (m_currentPEQIndex[m_currentEQTab] != m_currentEQIndex[m_currentEQTab])
	{
		m_currentEQIndex[m_currentEQTab] = m_currentPEQIndex[m_currentEQTab];
		m_pEQWidget->update(1);
	}
	EQData *pCurrentEQ = getCurrentEQ();
	bool flag = !pCurrentEQ->getEnableFlag();
	pCurrentEQ->enable(flag);

	refreshEQIndexButton();
	refreshEQGroup();
	m_pEQWidget->update(2);
	enableApplyButton();
}

void SpeakerEditor::onTabChanged(int value)
{
	if (m_slotEnableFlag == false)
	{
		return;
	}

	if (m_currentCrossoverIndex[m_currentEQTab] != m_currentEQIndex[m_currentEQTab])
	{
		m_currentEQIndex[m_currentEQTab] = m_currentCrossoverIndex[m_currentEQTab];
		m_pEQWidget->update(1);
	}
	EQData *pCurrentEQ = getCurrentEQ();
	pCurrentEQ->setFIRTab(value);

//	refreshEQIndexButton();
//	refreshEQGroup();
	m_pEQWidget->update(2);
	if (pCurrentEQ->getEnableFlag() == true)
	{
		enableApplyButton();
	}
}

void SpeakerEditor::onPEQTypeChanged(int nSelect)
{
	if (m_slotEnableFlag == false)
	{
		return;
	}

	if (m_currentPEQIndex[m_currentEQTab] != m_currentEQIndex[m_currentEQTab])
	{
		m_currentEQIndex[m_currentEQTab] = m_currentPEQIndex[m_currentEQTab];
		m_pEQWidget->update(1);
	}
	EQType PEQTypes[] = {EQ_PEAKING, EQ_L_SHELF, EQ_H_SHELF };
	EQData *pCurrentEQ = getCurrentEQ();
	if (pCurrentEQ->getType() != PEQTypes[nSelect])
	{
		pCurrentEQ->setType(PEQTypes[nSelect]);

		refreshEQIndexButton();
		refreshEQGroup();
		m_pEQWidget->update(2);
	}
	if (pCurrentEQ->getEnableFlag() == true)
	{
		enableApplyButton();
	}
}


void SpeakerEditor::onSetPFFreq(qreal freq)
{
	m_speakerData.m_channelData[m_currentEQTab].setPFFrequency(m_currentEQIndex[m_currentEQTab], freq);
	EQData *pCurrentEQ = getCurrentEQ();
	if (pCurrentEQ->getEnableFlag() == true)
	{
		enableApplyButton();
	}
}

void SpeakerEditor::onCurrentEQChanged()
{
	refreshEQIndexButton();
	refreshEQGroup();
	EQData *pCurrentEQ = getCurrentEQ();
	if (pCurrentEQ->getEnableFlag() == true)
	{
		enableApplyButton();
	}
}

void SpeakerEditor::onCurrentEQIndexChanged(int i)
{
	m_currentEQIndex[m_currentEQTab] = i;

	if (i < 8)
	{
		m_currentPEQIndex[m_currentEQTab] = i;
	}
	else
	{
		m_currentCrossoverIndex[m_currentEQTab] = i;
	}

	refreshEQIndexButton();
	refreshEQGroup();
}

void SpeakerEditor::onPFSlopeChanged(int slope)
{
	if (m_slotEnableFlag == false)
	{
		return;
	}

	if (m_currentCrossoverIndex[m_currentEQTab] != m_currentEQIndex[m_currentEQTab])
	{
		m_currentEQIndex[m_currentEQTab] = m_currentCrossoverIndex[m_currentEQTab];
		m_pEQWidget->update(1);
	}	

	EQData *pCurrentEQ = getCurrentEQ();
	pCurrentEQ->setPFIIRType((PF_IIR_Type) slope);
	m_pEQWidget->update(2);	
	if (pCurrentEQ->getEnableFlag() == true)
	{
		enableApplyButton();
	}
}


void SpeakerEditor::onPEQSlopeChanged(double slope)
{
	if (m_slotEnableFlag == false)
	{
		return;
	}

	if (m_currentPEQIndex[m_currentEQTab] != m_currentEQIndex[m_currentEQTab])
	{
		m_currentEQIndex[m_currentEQTab] = m_currentPEQIndex[m_currentEQTab];
		m_pEQWidget->update(1);
	}
	EQData *pCurrentEQ = getCurrentEQ();
	pCurrentEQ->setSlope(slope);

//	refreshEQGroup();
	m_pEQWidget->update(2);
	if (pCurrentEQ->getEnableFlag() == true)
	{
		enableApplyButton();
	}
}

void SpeakerEditor::onPFTypeToggled(bool /* checked */)
{
	if (m_slotEnableFlag == false)
	{
		return;
	}
	if (m_currentCrossoverIndex[m_currentEQTab] != m_currentEQIndex[m_currentEQTab])
	{
		m_currentEQIndex[m_currentEQTab] = m_currentCrossoverIndex[m_currentEQTab];
		m_pEQWidget->update(1);
	}	

	EQData *pCurrentEQ = getCurrentEQ();

	PF_TYPE type = pCurrentEQ->getPFType();
	if (type == PF_FIR)
	{
		pCurrentEQ->setPFType(PF_IIR);
	}
	else
	{
		pCurrentEQ->setPFType(PF_FIR);
	}

	refreshEQGroup();
	m_pEQWidget->update(2);
	if (pCurrentEQ->getEnableFlag() == true)
	{
		enableApplyButton();
	}
}

void SpeakerEditor::getSelectedChannel(bool channelFlag[8])
{
	int i;
	for (i = 0; i < 8; i++)
	{
		channelFlag[i] = false;
	}

	for (i = 0; i < m_channelCount; i++)
	{
		if (m_pChannelButton[i]->getMarkFlag() == true)
		{
			channelFlag[m_applyChannel[i]] = true;
		}
	}
}

void SpeakerEditor::onSignalModeSelected(int currentMode)
{
	DeviceSocket *pSocket = g_pApp->getDeviceConnection();
	bool channelFlag[8];
	getSelectedChannel(channelFlag);

	if (currentMode == 0)
	{
		/* Stop Signal Generator */
		for (int i = 0; i < m_channelCount; i++)
		{
			m_pChannelButton[i]->setEnabled(false);
		}
		if (g_pApp->getConnectStatus() == DEVICE_NETWORK_CONNECTED)
		{
			pSocket->setSignalMode((SIGNAL_MODE)SIGNAL_OFF);
//			pSocket->setPinkNoise(channelFlag);
			pSocket->stopSignal();
		}
	}
	else
	{
		for (int i = 0; i < m_channelCount; i++)
		{
			m_pChannelButton[i]->setEnabled(true);
		}
		if (g_pApp->getConnectStatus() == DEVICE_NETWORK_CONNECTED)
		{
			pSocket->setSignalMode((SIGNAL_MODE)currentMode);
			pSocket->setPinkNoise(channelFlag);
			pSocket->startSignal();
		}
	}
}

void SpeakerEditor::enableApplyButton()
{
	m_pApplyButton->setEnabled(m_speakerData.m_writable);
}

void SpeakerEditor::setApplyChannel(bool channelFlag[8])
{
	int i = 0;
	m_channelCount = 0;
	for (i = 0; i < 8; i++)
	{
		if (channelFlag[i] == true)
		{
			m_applyChannel[m_channelCount] = (SPEAKER_CHANNEL)i;
			m_channelCount++;
		}
	}

	QStringList channelNameList;
	channelNameList << "L" << "R" << "C" << "Sw" << "Ls" << "Rs" << "Bls" << "Brs";
	for (i = 0; i < 4; i++)
	{
		if (i < m_channelCount)
		{
			m_pChannelButton[i]->setText(channelNameList.at(m_applyChannel[i]));
			m_pChannelButton[i]->show();
			m_pChannelButton[i]->setEnabled(false);
			m_pChannelButton[i]->mark(false);
		}
		else
		{
			m_pChannelButton[i]->hide();
		}
	}
}

void SpeakerEditor::onClickChannelButton0()
{
	onClickChannelButton(0);
}
void SpeakerEditor::onClickChannelButton1()
{
	onClickChannelButton(1);
}
void SpeakerEditor::onClickChannelButton2()
{
	onClickChannelButton(2);
}
void SpeakerEditor::onClickChannelButton3()
{
	onClickChannelButton(3);
}

void SpeakerEditor::onClickChannelButton(int channel)
{
	bool flag = m_pChannelButton[channel]->getMarkFlag();
	m_pChannelButton[channel]->mark(!flag);

	DeviceSocket *pSocket = g_pApp->getDeviceConnection();
	bool channelFlag[8];
	getSelectedChannel(channelFlag);

	if (g_pApp->getConnectStatus() == DEVICE_NETWORK_CONNECTED)
	{
		pSocket->setPinkNoise(channelFlag);
	}
}

SpeakerEditor::~SpeakerEditor()
{
	DeviceSocket *pSocket = g_pApp->getDeviceConnection();
	bool channelFlag[8] = {0, 0, 0, 0, 0, 0, 0, 0};

	if (g_pApp->getConnectStatus() == DEVICE_NETWORK_CONNECTED)
	{
		pSocket->setSignalMode((SIGNAL_MODE)SIGNAL_OFF);
		pSocket->setPinkNoise(channelFlag);
		pSocket->stopSignal();

		CPi2000Data *pData = getCPi2000Data();
		pData->setInputSource(INPUT_SOURCE_DIGITAL);
		g_pApp->getDeviceConnection()->setInputSource(INPUT_SOURCE_DIGITAL);
		g_pMainWidget->refreshInputSource();
	}
}