#include "SpeakerImportDialog.h"
#include "PresetsDB.h"
#include <QMessageBox>
#include <QDebug>
#include <QInputDialog>

SpeakerImportDialog::SpeakerImportDialog(QWidget *parent)	: QDialog(parent)
{
#if 0
	m_pDescriptionLabel = new QLabel(tr("Speaker List:"), this);
	m_pExistLabel = new QLabel("*", this);

	m_pSpeakerListWidget = new QListWidget(this);
	connect(m_pSpeakerListWidget, SIGNAL(currentRowChanged(int)), this, SLOT(onSpeakerChanged(int)));

	m_pNewSpeakerCheck = new QCheckBox(tr("New Speaker"), this);
	connect(m_pNewSpeakerCheck, SIGNAL(clicked()), this, SLOT(onNewSpeakerClicked()));	
//	m_pNewSpeakerCheck->setChecked(true);

	m_pApplyGroup = new QGroupBox(tr("Apply channel:"), this);
	m_pLeft = new QCheckBox(tr("Left"), m_pApplyGroup);
	m_pRight = new QCheckBox(tr("Right"), m_pApplyGroup);
	m_pCenter = new QCheckBox(tr("Center"), m_pApplyGroup);
	m_pLS = new QCheckBox(tr("Left Surround"), m_pApplyGroup);
	m_pRS = new QCheckBox(tr("Right Surround"), m_pApplyGroup);
	m_pBLS = new QCheckBox(tr("Back Left Surround"), m_pApplyGroup);
	m_pBRS = new QCheckBox(tr("Back Right Surround"), m_pApplyGroup);
	m_pSW = new QCheckBox(tr("Subwoofer"), m_pApplyGroup);

	connect(m_pLeft, SIGNAL(clicked()), this, SLOT(onClickLeftChannel()));	
	connect(m_pRight, SIGNAL(clicked()), this, SLOT(onClickRightChannel()));	
	connect(m_pCenter, SIGNAL(clicked()), this, SLOT(onRefreshButton()));	
	connect(m_pSW, SIGNAL(clicked()), this, SLOT(onRefreshButton()));	
	connect(m_pLS, SIGNAL(clicked()), this, SLOT(onRefreshButton()));	
	connect(m_pRS, SIGNAL(clicked()), this, SLOT(onRefreshButton()));	
	connect(m_pBLS, SIGNAL(clicked()), this, SLOT(onRefreshButton()));	
	connect(m_pBRS, SIGNAL(clicked()), this, SLOT(onRefreshButton()));	

	m_pLeft->setEnabled(false);
	m_pRight->setEnabled(false);
	m_pCenter->setEnabled(false);
	m_pLS->hide();
	m_pRS->hide();
	m_pBLS->hide();
	m_pBRS->hide();
	m_pSW->hide();

	m_pDeleteButton = new QPushButton(tr("Delete"), this);
	connect(m_pDeleteButton, SIGNAL(clicked()), this, SLOT(onDelete()));

	m_pEditButton = new QPushButton(tr("Modify"), this);
	connect(m_pEditButton, SIGNAL(clicked()), this, SLOT(onClickEdit()));

	m_pCancelButton = new QPushButton(tr("Cancel"), this);
	connect(m_pCancelButton, SIGNAL(clicked()), this, SLOT(onCancel()));

	m_pSpeakerNameLabel = new QLabel(tr("Speaker name:"), this);
	m_pSpeakerNameEdit = new QLineEdit(this);
	m_pSpeakerNameEdit->setEnabled(false);
	connect(m_pSpeakerNameEdit, SIGNAL(textChanged(const QString &)), this, SLOT(onSpeakerNameChanged(const QString &)));


	m_pSpeakerTypeLabel = new QLabel(tr("Speaker type"), this);

	m_pSpeakerTypeCombo = new QComboBox(this);
	m_pSpeakerTypeCombo->addItem(tr("Passive"));
	m_pSpeakerTypeCombo->addItem(tr("Bi-Amp"));
	m_pSpeakerTypeCombo->addItem(tr("Tri-Amp"));
	m_pSpeakerTypeCombo->addItem(tr("Surround"));
	m_pSpeakerTypeCombo->addItem(tr("Subwoofer"));
	m_pSpeakerTypeCombo->setEnabled(false);
	connect(m_pSpeakerTypeCombo, SIGNAL(currentIndexChanged(int)), this, SLOT(onSpeakerTypeChanged(int)));

	m_pPriorityGroup = new QGroupBox(tr("Property"), this);
	m_pReadableRadio = new QCheckBox(tr("Readable"), m_pPriorityGroup);
	m_pWritableRadio = new QCheckBox(tr("Writable"), m_pPriorityGroup);
	m_pPriorityGroup->setEnabled(false);
	m_pReadableRadio->setEnabled(false);
	m_pWritableRadio->setEnabled(false);


	m_pDeleteButton->setEnabled(false);
//	m_pEditButton->setEnabled(false);

	m_result.m_operation = SPEAKER_OPERATION_CANCEL;
	refresh();

	setFixedSize(391, 386);
#endif	
}

void SpeakerImportDialog::resizeEvent(QResizeEvent * /* event */)
{
#if 0
	m_pNewSpeakerCheck->setGeometry(30, 20, 101, 17);

	int leftMargin = width() / 10;
	int buttonWidth = 130;
	
	m_pDescriptionLabel->setGeometry(30, 40, 91, 16);
	m_pSpeakerListWidget->setGeometry(30, 60, 171, 251);

	m_pEditButton->setGeometry(40, 340, 61, 23);
	m_pDeleteButton->setGeometry(160, 340, 61, 23);
	m_pCancelButton->setGeometry(280, 340, 61, 23);

	m_pSpeakerNameLabel->setGeometry(230, 40, 91, 16);
	m_pSpeakerNameEdit->setGeometry(230, 60, 111, 20);
	m_pExistLabel->setGeometry(345, 60, 20, 20);

	m_pSpeakerTypeLabel->setGeometry(230, 90, 91, 16);
	m_pSpeakerTypeCombo->setGeometry(230, 110, 131, 21);

	/*	Apply Group */
	m_pApplyGroup->setGeometry(230, 140, 131, 101);
	{
		m_pLeft->setGeometry(10, 20, 121, 17);
		m_pRight->setGeometry(10, 40, 121, 17);
		m_pCenter->setGeometry(10, 60, 121, 17);

		m_pLS->setGeometry(10, 20, 121, 17);
		m_pRS->setGeometry(10, 40, 121, 17);
		m_pBLS->setGeometry(10, 60, 121, 17);
		m_pBRS->setGeometry(10, 80, 121, 17);

		m_pSW->setGeometry(10, 20, 121, 17);
	}

	m_pPriorityGroup->setGeometry(230, 250, 131, 61);
	{
		m_pReadableRadio->setGeometry(10, 20, 81, 17);
		m_pWritableRadio->setGeometry(10, 40, 121, 17);
	}
#endif	
}

void SpeakerImportDialog::onCancel()
{
//	m_result.m_operation = SPEAKER_OPERATION_CANCEL;

	reject();
}

void SpeakerImportDialog::onImport()
{
//	m_result.m_operation = SPEAKER_OPERATION_DELETE;
//	m_result.m_speakerName = m_pSpeakerNameEdit->text();
	reject();
}

