#include "SpeakerListDialog.h"
#include "PresetsDB.h"
#include <QMessageBox>
#include <QDebug>
#include <QInputDialog>
#include "CPi2000Setting.h"
#include "simpleQtLogger.h"

SpeakerListDialog::SpeakerListDialog(QWidget *parent)	: QDialog(parent)
{
	setWindowTitle(tr("CPi2000 Speaker"));
	m_pDescriptionLabel = new QLabel(tr("Speaker List:"), this);
	m_pExistLabel = new QLabel(tr("Invalid Speaker Name"), this);
	m_pExistLabel->setObjectName(QStringLiteral("RedFont12"));

	m_pSpeakerListWidget = new QListWidget(this);
	m_pSpeakerListWidget->setObjectName(QStringLiteral("m_pSpeakerListWidget"));
	connect(m_pSpeakerListWidget, SIGNAL(currentRowChanged(int)), this, SLOT(onSpeakerChanged(int)));

	m_pNewSpeakerCheck = new QCheckBox(tr("New Speaker"), this);
	connect(m_pNewSpeakerCheck, SIGNAL(clicked()), this, SLOT(onNewSpeakerClicked()));	
//	m_pNewSpeakerCheck->setChecked(true);

	m_pApplyGroup = new QGroupBox(tr("Apply channel:"), this);
	m_pLeft = new QCheckBox(tr("Left"), m_pApplyGroup);
	m_pRight = new QCheckBox(tr("Right"), m_pApplyGroup);
	m_pCenter = new QCheckBox(tr("Center"), m_pApplyGroup);
	m_pLS = new QCheckBox(tr("Left Surround"), m_pApplyGroup);
	m_pRS = new QCheckBox(tr("Right Surround"), m_pApplyGroup);
	m_pBLS = new QCheckBox(tr("Back Left Surround"), m_pApplyGroup);
	m_pBRS = new QCheckBox(tr("Back Right Surround"), m_pApplyGroup);
	m_pSW = new QCheckBox(tr("Subwoofer"), m_pApplyGroup);

	connect(m_pLeft, SIGNAL(clicked()), this, SLOT(onClickLeftChannel()));	
	connect(m_pRight, SIGNAL(clicked()), this, SLOT(onClickRightChannel()));	
	connect(m_pCenter, SIGNAL(clicked()), this, SLOT(onClickC()));	
	connect(m_pSW, SIGNAL(clicked()), this, SLOT(onClickSW()));	
	connect(m_pLS, SIGNAL(clicked()), this, SLOT(onClickLS()));	
	connect(m_pRS, SIGNAL(clicked()), this, SLOT(onClickRS()));	
	connect(m_pBLS, SIGNAL(clicked()), this, SLOT(onClickBLS()));	
	connect(m_pBRS, SIGNAL(clicked()), this, SLOT(onClickBRS()));	

	m_pLeft->setEnabled(false);
	m_pRight->setEnabled(false);
	m_pCenter->setEnabled(false);
	m_pLS->hide();
	m_pRS->hide();
	m_pBLS->hide();
	m_pBRS->hide();
	m_pSW->hide();

	m_pDeleteButton = new QPushButton(tr("Delete"), this);
	connect(m_pDeleteButton, SIGNAL(clicked()), this, SLOT(onDelete()));

	m_pEditButton = new QPushButton(tr("Modify"), this);
	connect(m_pEditButton, SIGNAL(clicked()), this, SLOT(onClickEdit()));

	m_pCancelButton = new QPushButton(tr("Cancel"), this);
	connect(m_pCancelButton, SIGNAL(clicked()), this, SLOT(onCancel()));

	m_pSpeakerNameLabel = new QLabel(tr("Speaker name:"), this);
	m_pSpeakerNameEdit = new QLineEdit(this);
	m_pSpeakerNameEdit->setEnabled(false);
	connect(m_pSpeakerNameEdit, SIGNAL(textChanged(const QString &)), this, SLOT(onSpeakerNameChanged(const QString &)));


	m_pSpeakerTypeLabel = new QLabel(tr("Speaker type"), this);

	m_pSpeakerTypeCombo = new QComboBox(this);
	m_pSpeakerTypeCombo->addItem(tr("Full-range"));
	m_pSpeakerTypeCombo->addItem(tr("Two-way"));
	m_pSpeakerTypeCombo->addItem(tr("Three-way"));
	m_pSpeakerTypeCombo->addItem(tr("Surround"));
	m_pSpeakerTypeCombo->addItem(tr("Subwoofer"));
	m_pSpeakerTypeCombo->setEnabled(false);
	connect(m_pSpeakerTypeCombo, SIGNAL(currentIndexChanged(int)), this, SLOT(onSpeakerTypeChanged(int)));

	m_pPriorityGroup = new QGroupBox(tr("Property"), this);
	m_pReadableRadio = new QCheckBox(tr("Readable"), m_pPriorityGroup);
	m_pWritableRadio = new QCheckBox(tr("Writable"), m_pPriorityGroup);
	m_pPriorityGroup->setEnabled(false);
	m_pReadableRadio->setEnabled(false);
	m_pReadableRadio->hide();
	m_pWritableRadio->hide();
	m_pPriorityGroup->hide();
	m_pWritableRadio->setEnabled(false);

	m_pHideCheck = new QCheckBox(tr("Hide Unwritable"), this);
	m_pHideCheck->setChecked(false);
	connect(m_pHideCheck, SIGNAL(clicked()), this, SLOT(onHideClicked()));	
	m_pHideCheck->hide();

	m_pDeleteButton->setEnabled(false);
//	m_pEditButton->setEnabled(false);

	m_result.m_operation = SPEAKER_OPERATION_CANCEL;

	m_speakerListData.m_newSpeakerFlag = false;
	m_speakerListData.m_hideFlag = false;
	m_speakerListData.m_speakerName = "";
	m_speakerListData.m_speakerType = SPEAKER_TYPE_PASSIVE;
	m_speakerListData.m_channel_L = false;
	m_speakerListData.m_channel_R = false;
	m_speakerListData.m_channel_C = false;
	m_speakerListData.m_channel_SW = false;
	m_speakerListData.m_channel_LS = false;
	m_speakerListData.m_channel_RS = false;
	m_speakerListData.m_channel_BLS = false;
	m_speakerListData.m_channel_BRS = false;
	m_speakerListData.m_readable = false;
	m_speakerListData.m_writable = false;
	m_slotEnableFlag = true;

//	refresh();

	setFixedSize(401, 386);
}

void SpeakerListDialog::resizeEvent(QResizeEvent * /* event */)
{
	m_pNewSpeakerCheck->setGeometry(30, 15, 121, 25);

//	int leftMargin = width() / 10;
//	int buttonWidth = 130;
	
	m_pDescriptionLabel->setGeometry(30, 40, 91, 16);
	m_pHideCheck->setGeometry(100, 40, 100, 18);
	m_pSpeakerListWidget->setGeometry(30, 60, 171, 251);

	m_pEditButton->setGeometry(40, 340, 61, 23);
	m_pDeleteButton->setGeometry(160, 340, 61, 23);
	m_pCancelButton->setGeometry(280, 340, 61, 23);

	m_pSpeakerNameLabel->setGeometry(230, 40, 101, 16);
	m_pSpeakerNameEdit->setGeometry(230, 60, 111, 20);
	m_pExistLabel->setGeometry(230, 80, 120, 20);

	m_pSpeakerTypeLabel->setGeometry(230, 110, 91, 16);
	m_pSpeakerTypeCombo->setGeometry(230, 130, 141, 21);

	/*	Apply Group */
	m_pApplyGroup->setGeometry(230, 170, 141, 131);
	{
		m_pLeft->setGeometry(10, 20, 131, 25);
		m_pRight->setGeometry(10, 45, 131, 25);
		m_pCenter->setGeometry(10, 70, 131, 25);

		m_pLS->setGeometry(10, 20, 131, 25);
		m_pRS->setGeometry(10, 45, 131, 25);
		m_pBLS->setGeometry(10, 70, 131, 25);
		m_pBRS->setGeometry(10, 95, 131, 25);

		m_pSW->setGeometry(10, 20, 131, 25);
	}

	m_pPriorityGroup->setGeometry(230, 270, 131, 41);
	{
		m_pReadableRadio->setGeometry(10, 40, 81, 17);
		m_pWritableRadio->setGeometry(10, 20, 121, 17);
	}
}

void SpeakerListDialog::onCancel()
{
	if (m_slotEnableFlag == false)
	{
		return;
	}

	m_result.m_operation = SPEAKER_OPERATION_CANCEL;

	reject();
}

void SpeakerListDialog::onDelete()
{
	if (m_slotEnableFlag == false)
	{
		return;
	}

	m_result.m_operation = SPEAKER_OPERATION_DELETE;
	m_result.m_speakerName = m_pSpeakerNameEdit->text();
	reject();
}

void SpeakerListDialog::onClickEdit()
{
	if (m_slotEnableFlag == false)
	{
		return;
	}

	if (m_pNewSpeakerCheck->isChecked())
	{
		m_result.m_operation = SPEAKER_OPERATION_NEW;
	}
	else
	{
		m_result.m_operation = SPEAKER_OPERATION_MODIFY;
		if (m_password.isEmpty() == false)
		{
		    bool ok;
		    QString text = QInputDialog::getText(this, tr("Password protection"), tr("Please Enter Password:"), QLineEdit::Password, "", &ok);
		    if (!ok)
			{
				return;
			}
			if (text != m_password)
			{
				QMessageBox::warning(this, tr("Warning"), tr("Invalid password! You can't edit this speaker's setting"), QMessageBox::Ok);
				return;
			}
		}
	}
	m_result.m_speakerName = m_pSpeakerNameEdit->text();
	m_result.m_speakerType = (SPEAKER_TYPE)m_pSpeakerTypeCombo->currentIndex();

	QCheckBox *pCheckBox[8] = {m_pLeft, m_pRight, m_pCenter, m_pSW, m_pLS, m_pRS, m_pBLS, m_pBRS};
	for (int i = 0; i < 8; i++)
	{
		bool flag = pCheckBox[i]->isVisible() && pCheckBox[i]->isChecked();
		m_result.m_speakerChannelApplied[i] = flag;
	}
	reject();
}


void SpeakerListDialog::refreshSpeaker(void)
{
	bool oldSlotEnableFlag = m_slotEnableFlag;
	m_slotEnableFlag = false;

	m_pSpeakerNameEdit->setText(m_speakerListData.m_speakerName);
	m_pSpeakerNameEdit->setEnabled(m_speakerListData.m_newSpeakerFlag);
	qDebug() << "m_speakerListData.m_speakerName = " << m_speakerListData.m_speakerName;

	m_pReadableRadio->setChecked(m_speakerListData.m_readable);
	m_pWritableRadio->setChecked(m_speakerListData.m_writable);
	m_pSpeakerTypeCombo->setCurrentIndex(m_speakerListData.m_speakerType);

	m_pLeft->hide();
	m_pRight->hide();
	m_pCenter->hide();
	m_pLS->hide();
	m_pRS->hide();
	m_pBLS->hide();
	m_pBRS->hide();
	m_pSW->hide();

//	bool flag = (m_speakerListData.m_speakerType == SPEAKER_TYPE_PASSIVE) ||(m_speakerListData.m_speakerType == SPEAKER_TYPE_BIAMP) || (m_speakerListData.m_speakerType == SPEAKER_TYPE_TRIAMP);
	if (m_speakerListData.m_speakerType == SPEAKER_TYPE_SUBWOOFER)
	{
		m_pSW->show();
		if (m_speakerListData.m_speakerName.isEmpty())
		{
			m_pSW->setDisabled(true);
		}
		else
		{
			m_pSW->setEnabled(true);
		}
	}
	else if (m_speakerListData.m_speakerType == SPEAKER_TYPE_SURROUND)
	{
		m_pLS->show();
		m_pRS->show();
		m_pBLS->show();
		m_pBRS->show();
		if (m_speakerListData.m_speakerName.isEmpty())
		{
			m_pLS->setDisabled(true);
			m_pRS->setDisabled(true);
			m_pBLS->setDisabled(true);
			m_pBRS->setDisabled(true);
		}
		else
		{
			m_pLS->setEnabled(true);
			m_pRS->setEnabled(true);
			m_pBLS->setEnabled(true);
			m_pBRS->setEnabled(true);
		}
	}
	else
	{
		m_pLeft->show();
		m_pRight->show();
		m_pCenter->show();
		if (m_speakerListData.m_speakerName.isEmpty())
		{
			m_pLeft->setDisabled(true);
			m_pRight->setDisabled(true);
			m_pCenter->setDisabled(true);
		}
		else
		{
			m_pLeft->setEnabled(true);
			m_pRight->setEnabled(true);
			m_pCenter->setEnabled(true);
		}
	}

	/* enable or disable speaker type selection */
	if (m_speakerListData.m_newSpeakerFlag == false)
	{
		m_pSpeakerTypeCombo->setEnabled(false);	
	}
	else
	{
		m_pSpeakerTypeCombo->setEnabled(true);	
	}

	if (m_speakerListData.m_newSpeakerFlag == false)
	{
		QCheckBox *pCheckBox[] = { m_pLeft, m_pRight, m_pCenter, m_pSW, m_pLS, m_pRS, m_pBLS, m_pBRS };
		bool * checkFlag[] = {&m_speakerListData.m_channel_L,  &m_speakerListData.m_channel_R, &m_speakerListData.m_channel_C, &m_speakerListData.m_channel_SW, 
			&m_speakerListData.m_channel_LS, &m_speakerListData.m_channel_RS, &m_speakerListData.m_channel_BLS, &m_speakerListData.m_channel_BRS };
	
		for (int i = 0; i < 8; i++)
		{
			CPi2000Data *pData = getCPi2000Data();
//			if (pData->m_speakerName[i] == m_speakerListData.m_speakerName)
			if ((m_speakerListData.m_newSpeakerFlag == false) && (pData->m_speakerName[i] == m_speakerListData.m_speakerName))
			{
//				pCheckBox[i]->setChecked(true);
				*checkFlag[i] = true;
				pCheckBox[i]->setEnabled(false);
			}
			else
			{
				pCheckBox[i]->setEnabled(true);
			}
		}
	}

	m_slotEnableFlag = oldSlotEnableFlag;
}

void SpeakerListDialog::refreshButton(void)
{
	bool oldSlotEnableFlag = m_slotEnableFlag;
	m_slotEnableFlag = false;

	if (m_speakerListData.m_speakerType == SPEAKER_TYPE_SUBWOOFER)
	{
		m_pSW->setChecked(m_speakerListData.m_channel_SW);
	}
	else if (m_speakerListData.m_speakerType == SPEAKER_TYPE_SURROUND)
	{
		m_pLS->setChecked(m_speakerListData.m_channel_LS);
		m_pRS->setChecked(m_speakerListData.m_channel_RS);
		m_pBLS->setChecked(m_speakerListData.m_channel_BLS);
		m_pBRS->setChecked(m_speakerListData.m_channel_BRS);
	}
	else
	{
		m_pLeft->setChecked(m_speakerListData.m_channel_L);
		m_pRight->setChecked(m_speakerListData.m_channel_R);
		m_pCenter->setChecked(m_speakerListData.m_channel_C);
	}


	/* show or hide m_pExistLabel  */
	bool existFlag = false;
	m_pExistLabel->hide();	
	if (m_speakerListData.m_speakerName.isEmpty())
	{
		m_pExistLabel->show();
		existFlag = true;
	}
	else
	{
		QString newFileName = m_speakerListData.m_speakerName;
		bool newSpeakerFlag = m_speakerListData.m_newSpeakerFlag;
		if (newSpeakerFlag)
		{
			/* Check the speaker name is already exist or not */
			for (int i = 0; i < m_speakerList.count(); i++)
			{
				if (newFileName == m_speakerList.at(i))
				{
					m_pExistLabel->show();
					existFlag = true;
					break;
				}
			}
		}
	}

	/* For button Edit */
	bool editEnable = true;
	if (m_speakerListData.m_newSpeakerFlag)
	{
		if (existFlag == true)
		{
			editEnable = false;
		}
	}
	else
	{
		if ((m_speakerListData.m_readable == false) && (m_speakerListData.m_writable == false))
		{
			editEnable = false;
		}
	}

	if (editEnable == true)
	{
		switch (m_speakerListData.m_speakerType)
		{
		case SPEAKER_TYPE_PASSIVE:
		case SPEAKER_TYPE_BIAMP:
		case SPEAKER_TYPE_TRIAMP:
			if ((m_speakerListData.m_channel_L == false) && (m_speakerListData.m_channel_R == false) && (m_speakerListData.m_channel_C == false))
			{
				editEnable = false;
			}
			break;
		case SPEAKER_TYPE_SUBWOOFER:
			if (m_speakerListData.m_channel_SW == false) 
			{
				editEnable = false;			
			}
			break;
		case SPEAKER_TYPE_SURROUND:
			if ((m_speakerListData.m_channel_LS == false) && (m_speakerListData.m_channel_RS == false) && (m_speakerListData.m_channel_BLS == false) && (m_speakerListData.m_channel_BRS == false))
			{
				editEnable = false;			
			}
			break;
		}
	}
	m_pEditButton->setEnabled(editEnable);

	/* For button Delete */
	bool deleteFlag = true;
	if (m_speakerListData.m_newSpeakerFlag)
	{
		deleteFlag = false;
	}
	else if (m_speakerListData.m_speakerName.isEmpty())
	{
		deleteFlag = false;
	}
	else if (m_speakerListData.m_writable == false)
	{
		deleteFlag = false;
	}
	m_pDeleteButton->setEnabled(deleteFlag);




#if 0
	bool deleteEnable = false;
	bool modifyEnable = false;
	bool nameExist = false;

	QString newFileName = m_pSpeakerNameEdit->text();
	if (newFileName.isEmpty())
	{
		goto END;
	}

	bool newSpeakerFlag = m_pNewSpeakerCheck->isChecked();
	if (newSpeakerFlag)
	{
		/* Check the speaker name is already exist or not */
		for (int i = 0; i < m_speakerList.count(); i++)
		{
			if (newFileName == m_speakerList.at(i))
			{
				nameExist = true;
				goto END;
			}
		}
	}
	else
	{
		if (m_pWritableRadio->isChecked())
		{
			deleteEnable = true;
		}
	}

	bool b1 = m_pLeft->isChecked() && m_pLeft->isEnabled();
	bool b2 = m_pRight->isChecked() && m_pRight->isEnabled();
	bool b3 = m_pCenter->isChecked() && m_pCenter->isEnabled();
	bool b4 = m_pLS->isChecked() && m_pLS->isEnabled();
	bool b5 = m_pRS->isChecked() && m_pRS->isEnabled();
	bool b6 = m_pBLS->isChecked() && m_pBLS->isEnabled();
	bool b7 = m_pBRS->isChecked() && m_pBRS->isEnabled();
	bool b8 = m_pSW->isChecked() && m_pSW->isEnabled();

//#if 0  // just for test
#if 1
	if (m_pReadableRadio->isChecked() == false)
	{
		goto END;
	}
#endif

	if (b1 || b2 || b3 || b4 || b5 || b6 || b7 || b8)
	{
		modifyEnable = true;
	}

END:
	m_pDeleteButton->setEnabled(deleteEnable);
	m_pEditButton->setEnabled(modifyEnable);

	if (nameExist == true)
	{
		m_pExistLabel->show();
	}
	else
	{
		m_pExistLabel->hide();
	}
#endif
	m_slotEnableFlag = oldSlotEnableFlag;
}

void SpeakerListDialog::refresh(void)
{
	bool oldSlotEnableFlag = m_slotEnableFlag;
	m_slotEnableFlag = false;

	m_pNewSpeakerCheck->setChecked(m_speakerListData.m_newSpeakerFlag);
	m_pHideCheck->setChecked(m_speakerListData.m_hideFlag);
	m_pSpeakerListWidget->clear();
	bool addFlag = false;
	for (int i = 0; i < m_speakerList.count(); i++)
	{
		// Hide button is checked, we should hide all uneditable speaker.
		if (m_speakerListData.m_hideFlag)
		{
			QString speakerName = m_speakerList.at(i);
			SpeakerData speakerData;
				
			g_database.getSpeaker(speakerName, speakerData);
			if (speakerData.m_writable == false)
			{
				continue;
			}
		}
		m_pSpeakerListWidget->addItem(m_speakerList.at(i));
		addFlag = true;
	}
	m_pSpeakerTypeLabel->setDisabled(m_speakerListData.m_newSpeakerFlag);
	m_pHideCheck->setDisabled(m_speakerListData.m_newSpeakerFlag);
	m_pSpeakerListWidget->setDisabled(m_speakerListData.m_newSpeakerFlag);

	refreshSpeaker();
	refreshButton();

	if (m_speakerListData.m_newSpeakerFlag == true)
	{
		m_pEditButton->setText(tr("New"));
	}
	else
	{
		m_pEditButton->setText(tr("Modify"));
	}


#if 0
	m_pSpeakerListWidget->clear();
	bool addFlag = false;
	for (int i = 0; i < m_speakerList.count(); i++)
	{
		// Hide button is checked, we should hide all uneditable speaker.
		if (m_pHideCheck->isChecked())
		{
			QString speakerName = m_speakerList.at(i);
			SpeakerData speakerData;
				
			g_database.getSpeaker(speakerName, speakerData);
			if ((speakerData.m_readable == false) && (speakerData.m_writable == false))
			{
				continue;
			}
		}
		m_pSpeakerListWidget->addItem(m_speakerList.at(i));
		addFlag = true;
	}

	bool newSpeakerFlag = m_pNewSpeakerCheck->isChecked();
	m_pDeleteButton->setEnabled(false);
	if ((addFlag == true) && (!newSpeakerFlag))
	{
		m_pSpeakerListWidget->setCurrentRow(0);
		m_pDeleteButton->setEnabled(true);
	}

	m_pDescriptionLabel->setEnabled(!newSpeakerFlag);
	m_pSpeakerListWidget->setEnabled(!newSpeakerFlag);
	m_pSpeakerNameEdit->setEnabled(newSpeakerFlag);
	m_pSpeakerTypeCombo->setEnabled(newSpeakerFlag);
	m_pHideCheck->setEnabled(!newSpeakerFlag);

	if (newSpeakerFlag)
	{
//		m_pSpeakerNameEdit->setText("NewSpeaker");
		m_pReadableRadio->setChecked(true);
		m_pWritableRadio->setChecked(true);
		m_pSpeakerTypeCombo->setCurrentIndex(2);
		m_pEditButton->setText(tr("New"));
	}
	else
	{
		m_pEditButton->setText(tr("Modify"));
	}

	m_pEditButton->setEnabled(false);

	m_pLeft->setChecked(false);
	m_pRight->setChecked(false);
	m_pCenter->setChecked(false);
	m_pLS->setChecked(false);
	m_pRS->setChecked(false);
	m_pBLS->setChecked(false);
	m_pBRS->setChecked(false);
	m_pSW->setChecked(false);

	m_pExistLabel->hide();
#endif
	m_slotEnableFlag = oldSlotEnableFlag;
}

void SpeakerListDialog::setSpeakerList(QStringList speakerList)
{
	m_speakerList = speakerList;
	
	refresh();
}


void SpeakerListDialog::onSpeakerChanged(int currentIndex)
{
	if (m_slotEnableFlag == false)
	{
		return;
	}

	if (currentIndex < 0)
	{
		return;
	}

	QListWidgetItem *pItem = m_pSpeakerListWidget->item(currentIndex);
	QString speakerName = pItem->text();

	SpeakerData speakerData; 
	if (g_database.getSpeaker(speakerName, speakerData) == false)
	{
		L_ERROR("Failed to get speaker: " + speakerName);
		QMessageBox::warning(this, QString("Get speaker Error"), QString("Failed to get speaker: %1 from database!").arg(speakerName), QMessageBox::Ok);
		reject();
		return;
	}
	else
	{
		m_password = speakerData.m_password;
		m_speakerListData.m_speakerName = speakerName;
		m_speakerListData.m_speakerType = speakerData.m_speakerType;
		m_speakerListData.m_readable = speakerData.m_readable;
		m_speakerListData.m_writable = speakerData.m_writable;
		m_speakerListData.m_channel_C = false;
		m_speakerListData.m_channel_L = false;
		m_speakerListData.m_channel_R = false;
		m_speakerListData.m_channel_SW = false;
		m_speakerListData.m_channel_LS = false;
		m_speakerListData.m_channel_RS = false;
		m_speakerListData.m_channel_BLS = false;
		m_speakerListData.m_channel_BRS = false;
	}

	refreshSpeaker();
	refreshButton();
}

void SpeakerListDialog::onSpeakerTypeChanged(int index)
{
	if (m_slotEnableFlag == false)
	{
		return;
	}
	m_speakerListData.m_speakerType = (SPEAKER_TYPE)index;
	m_speakerListData.m_channel_C = false;
	m_speakerListData.m_channel_L = false;
	m_speakerListData.m_channel_R = false;
	m_speakerListData.m_channel_SW = false;
	m_speakerListData.m_channel_LS = false;
	m_speakerListData.m_channel_RS = false;
	m_speakerListData.m_channel_BLS = false;
	m_speakerListData.m_channel_BRS = false;

	refreshSpeaker();
	refreshButton();
}

void SpeakerListDialog::onNewSpeakerClicked()
{
	if (m_slotEnableFlag == false)
	{
		return;
	}

	m_speakerListData.m_newSpeakerFlag = !m_speakerListData.m_newSpeakerFlag;

	if (m_speakerListData.m_newSpeakerFlag)
	{
		m_speakerListData.m_speakerName = "NewSpeaker";
		m_speakerListData.m_speakerType = SPEAKER_TYPE_PASSIVE;
		m_speakerListData.m_channel_C = false;
		m_speakerListData.m_channel_L = false;
		m_speakerListData.m_channel_R = false;

		m_speakerListData.m_readable = true;
		m_speakerListData.m_writable = true;
	}
	else
	{
		m_speakerListData.m_speakerName = "";
		m_speakerListData.m_speakerType = SPEAKER_TYPE_PASSIVE;
		m_speakerListData.m_channel_C = false;
		m_speakerListData.m_channel_L = false;
		m_speakerListData.m_channel_R = false;

		m_speakerListData.m_readable = false;
		m_speakerListData.m_writable = false;	
	}
	refresh();
}

void SpeakerListDialog::onRefreshButton()
{
	if (m_slotEnableFlag == false)
	{
		return;
	}

	refreshButton();
}

void SpeakerListDialog::onSpeakerNameChanged(const QString & speakerName)
{
	if (m_slotEnableFlag == false)
	{
		return;
	}

	m_speakerListData.m_speakerName = speakerName;
	refresh();
	refreshButton();
}

void SpeakerListDialog::onClickLeftChannel()
{
	if (m_slotEnableFlag == false)
	{
		return;
	}

	SpeakerData speakerData;
	g_database.getSpeaker(SPEAKER_CHANNEL_L, speakerData);
	m_speakerListData.m_channel_L = !m_speakerListData.m_channel_L;
	if (m_speakerListData.m_channel_L != m_speakerListData.m_channel_R)
	{
		if (m_speakerListData.m_speakerType != speakerData.m_speakerType)
		{
			m_speakerListData.m_channel_R = m_speakerListData.m_channel_L;
		}
	}

//	m_speakerListData.m_channel_R = m_speakerListData.m_channel_L;

	refreshButton();
}

void SpeakerListDialog::onClickC()
{
	if (m_slotEnableFlag == false)
	{
		return;
	}

	m_speakerListData.m_channel_C = !m_speakerListData.m_channel_C;

	refreshButton();
}

void SpeakerListDialog::onClickSW()	
{
	if (m_slotEnableFlag == false)
	{
		return;
	}

	m_speakerListData.m_channel_SW = !m_speakerListData.m_channel_SW;

	refreshButton();
}

void SpeakerListDialog::onClickLS()	
{
	if (m_slotEnableFlag == false)
	{
		return;
	}

	m_speakerListData.m_channel_LS = !m_speakerListData.m_channel_LS;

	refreshButton();
}

void SpeakerListDialog::onClickRS()	
{
	if (m_slotEnableFlag == false)
	{
		return;
	}

	m_speakerListData.m_channel_RS = !m_speakerListData.m_channel_RS;

	refreshButton();
}

void SpeakerListDialog::onClickBLS()	
{
	if (m_slotEnableFlag == false)
	{
		return;
	}

	m_speakerListData.m_channel_BLS = !m_speakerListData.m_channel_BLS;

	refreshButton();
}

void SpeakerListDialog::onClickBRS()
{
	if (m_slotEnableFlag == false)
	{
		return;
	}

	m_speakerListData.m_channel_BRS = !m_speakerListData.m_channel_BRS;

	refreshButton();
}



void SpeakerListDialog::onClickRightChannel()
{
	if (m_slotEnableFlag == false)
	{
		return;
	}

	SpeakerData speakerData;
	g_database.getSpeaker(SPEAKER_CHANNEL_L, speakerData);
	m_speakerListData.m_channel_R = !m_speakerListData.m_channel_R;
	if (m_speakerListData.m_channel_L != m_speakerListData.m_channel_R)
	{
		if (m_speakerListData.m_speakerType != speakerData.m_speakerType)
		{
			m_speakerListData.m_channel_L = m_speakerListData.m_channel_R;
		}
	}

//	m_speakerListData.m_channel_R = m_speakerListData.m_channel_L;

	refreshButton();
}

void SpeakerListDialog::onHideClicked()
{
	if (m_slotEnableFlag == false)
	{
		return;
	}

	m_speakerListData.m_speakerName = "";
	m_speakerListData.m_speakerType = SPEAKER_TYPE_PASSIVE;
	m_speakerListData.m_hideFlag = !m_speakerListData.m_hideFlag;

	refresh();
}

