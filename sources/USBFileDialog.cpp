#include "USBFileDialog.h"
#include "mainApplication.h"
#include <QRegExp>

#define NEW_FILE_ITEM_TEXT  "New File"

USBFileDialog::USBFileDialog(QWidget *parent)	: QDialog(parent)
{
	m_pDescriptionLabel = new QLabel(tr("USB Disk on CPi2000"), this);
	
	m_pFileNameLabel = new QLabel(tr("File Name:"), this);

	m_pExplainLabel = new QLabel(tr("File name can only contain the following characters: \nthe captial (A-Z), digital Number (0 - 9), '-' and '_'"), this);
	m_pExplainLabel->setWordWrap(true);

	m_pFileNameEdit = new QLineEdit(this);
	QRegExp rx("[A-Z0-9_\\-]{1,20}\\.dev$"); 
	QRegExpValidator *validator = new QRegExpValidator(rx, this);  
	m_pFileNameEdit->setValidator(validator); 
	connect(m_pFileNameEdit, SIGNAL(textChanged(const QString &)), this, SLOT(onFileNameChanged(const QString &)));

	pUSBFileList = new QListWidget(this);
	connect(pUSBFileList, SIGNAL(itemClicked(QListWidgetItem *)), this, SLOT(fileSelected(QListWidgetItem *)));

	m_pLoadButton = new QPushButton(this);
	connect(m_pLoadButton, SIGNAL(clicked()), this, SLOT(onLoadSave()));

	m_pCancelButton = new QPushButton(tr("Cancel"), this);
	connect(m_pCancelButton, SIGNAL(clicked()), this, SLOT(onCancel()));

	m_pNewFileCheckBox = new QCheckBox(tr("New File"), this);
	connect(m_pNewFileCheckBox, SIGNAL(clicked()), this, SLOT(onNewFileClicked()));
	m_pNewFileCheckBox->setEnabled(true);

	m_saveMode = true;

	refresh();

	setFixedSize(390, 300);
}

void USBFileDialog::setToSaveMode(QStringList &fileNameList, QString newFileName)
{
	m_saveMode = true;
	m_usbFileList = fileNameList;
	qDebug() << "fileNameList = " << fileNameList;

	m_NewFileName = newFileName;

	m_pFileNameEdit->setEnabled(false);
	m_pLoadButton->setEnabled(false);
	m_pNewFileCheckBox->show();

	refresh();
}

void USBFileDialog::setToLoadMode(QStringList &fileNameList)
{
	m_saveMode = false;
	m_usbFileList = fileNameList;

	m_pFileNameLabel->hide();
	m_pFileNameEdit->hide();
	m_pExplainLabel->hide();
	m_pNewFileCheckBox->hide();

	refresh();
}

void USBFileDialog::onNewFileClicked()
{
	refresh();
}

void USBFileDialog::onCancel()
{
	reject();
}

void USBFileDialog::refresh()
{
	pUSBFileList->clear();

	if (m_saveMode)
	{
		bool newFileFlag = m_pNewFileCheckBox->isChecked();
		m_pDescriptionLabel->setEnabled(!newFileFlag);
		pUSBFileList->setEnabled(!newFileFlag);
		m_pFileNameEdit->setEnabled(newFileFlag);
		if (newFileFlag)
		{
			m_pFileNameEdit->setText(m_NewFileName);
		}
		m_pLoadButton->setEnabled(newFileFlag);

		m_pDescriptionLabel->setText(tr("Save as"));
		setWindowTitle(tr("Save to USB File on CPi2000"));
		m_pLoadButton->setText(tr("Save"));
	}
	else
	{
		m_pDescriptionLabel->setText(tr("Load USB file on CPi2000"));
		setWindowTitle(tr("Load from USB device on CPi2000"));	
		m_pLoadButton->setText(tr("Load"));
	}

	for (int i = 0; i < m_usbFileList.count(); i++)
	{
		pUSBFileList->addItem(m_usbFileList.at(i));
	}
}

void USBFileDialog::resizeEvent(QResizeEvent * /* event */)
{
	int leftMargin = 20;
	int buttonWidth = 130;

	m_pNewFileCheckBox->setGeometry(leftMargin, 15, width() - 2 * leftMargin, 20);

	if (m_saveMode)
	{
		m_pDescriptionLabel->setGeometry(leftMargin, 33, width() - 2 * leftMargin, 20);
		pUSBFileList->setGeometry(leftMargin, 53, width() - 3 * leftMargin - buttonWidth, height() - 55 - 20);
	}
	else
	{
		m_pDescriptionLabel->setGeometry(leftMargin, 15, width() - 2 * leftMargin, 20);
		pUSBFileList->setGeometry(leftMargin, 35, width() - 3 * leftMargin - buttonWidth, height() - 35 - 20);
	}
	
	m_pFileNameLabel->setGeometry(width() - buttonWidth - leftMargin, 15, buttonWidth, 20);
	m_pFileNameEdit->setGeometry(width() - buttonWidth - leftMargin, 35, buttonWidth, 25);
	m_pExplainLabel->setGeometry(width() - buttonWidth - leftMargin, 55, buttonWidth, 125);

	m_pLoadButton->setGeometry(width() - buttonWidth - leftMargin , height() - 90, buttonWidth, 25);
	m_pCancelButton->setGeometry(width() - buttonWidth - leftMargin, height() - 50, buttonWidth, 25);
}

void USBFileDialog::fileSelected(QListWidgetItem *item)
{
	m_pLoadButton->setEnabled(true);
	QString selectedText = item->text();
	m_selectedFileName = selectedText;

	if (m_saveMode == true)
	{
		if (selectedText.indexOf("New File") == 0)
		{
			m_pLoadButton->setText(tr("Save as New"));
			m_pFileNameEdit->setText(m_NewFileName);
			m_pFileNameEdit->setEnabled(true);
		}
		else
		{
			m_pLoadButton->setText(tr("Replace"));
			m_pFileNameEdit->setText(selectedText);
			m_pFileNameEdit->setEnabled(false);
		}
	}

}

void USBFileDialog::onLoadSave()
{
	if (m_saveMode == true)
	{
		m_selectedFileName = m_pFileNameEdit->text();
	}
	accept();
}

void USBFileDialog::onFileNameChanged(const QString &text)
{
	bool enableFlag = false;
	if (text.right(4) == ".dev")
	{
		enableFlag = true;	

#if 0
		if (m_selectedFileName == NEW_FILE_ITEM_TEXT)
		{
			for (int i = 0; i < m_usbFileList.length(); i++)
			{
				if (text == m_usbFileList.at(i))
				{
					enableFlag = false;
					break;
				}
			}
		}
#endif
	}

	m_pLoadButton->setEnabled(enableFlag);

}