#include "UpgradeDialog.h"
#include "mainWidget.h"
#include "mainApplication.h"
#include <QMessageBox>
#include <QDate>
#include <QTime>
#include "QTelnetPerso.h"

void UpgradeDialog::setReminder(QString reminder)
{
	m_pVerisonLabel->setText(reminder);
}

void UpgradeDialog::setOperation(UPDATE_OPERATION operaton) 
{ 
	m_operation = operaton;
	QString remainder;
	if (m_operation == UPGRADE_OPEATION)
	{
		remainder = QString ("\n") + tr("Do you want to start the upgrading process?");
	}
	else
	{
		remainder = QString ("\n") + tr("Do you want to start the downgrading process?");
	}
	m_pRemainderLabel->setText(remainder);
}

UpgradeDialog::UpgradeDialog(QWidget *parent)	: QDialog(parent)
{
	m_upgradeFlag = false;
	m_operation = UPGRADE_OPEATION;

	m_pVerisonLabel = new QLabel("", this);
	m_pVerisonLabel->setObjectName(QStringLiteral("BlackFont18"));
	m_pVerisonLabel->setWordWrap(true);
	
	QString remainder = QString ("\n") + tr("Do you want to start the upgrading process?");
	m_pRemainderLabel = new QLabel(remainder, this);
	m_pRemainderLabel->setObjectName(QStringLiteral("BlackFont24"));
	m_pRemainderLabel->setAlignment(Qt::AlignLeft | Qt::AlignTop);
	m_pRemainderLabel->setWordWrap(true);

	
	m_pPage1_Yes = new QPushButton(tr("Yes"), this);
	m_pPage1_Yes->setObjectName(QStringLiteral("BlackFont18"));
	connect(m_pPage1_Yes, SIGNAL(clicked()), this, SLOT(onClickPage1Yes()));

	m_pPage1_Cancel = new QPushButton(tr("No"), this);
	m_pPage1_Cancel->setObjectName(QStringLiteral("BlackFont18"));
	connect(m_pPage1_Cancel, SIGNAL(clicked()), this, SLOT(reject()));

	m_pWaitingWidget = new BackgroundWidget(QString(":ActivityIndicator.png"), this, 20, 2);
	m_pWaitingWidget->hide();

	setFixedSize(560, 340);
}

void UpgradeDialog::resizeEvent(QResizeEvent * /*event*/)
{
	int leftMargin = 40;
	int bottomMargin = 20;
	m_pVerisonLabel->setGeometry(leftMargin, bottomMargin, width() - 2 *  leftMargin, 80);
	m_pRemainderLabel->setGeometry(leftMargin, height() / 4 + 10, width() - 2 * leftMargin, 120);

//	int buttonTop = 310;
	int buttonWidth = 100;
	int buttonHeight = 35;
	m_pPage1_Yes->setGeometry(width() - 2 * leftMargin - 2 * buttonWidth, height() - bottomMargin - buttonHeight, buttonWidth, buttonHeight);
	m_pPage1_Cancel->setGeometry(width() - leftMargin - buttonWidth, height() - bottomMargin - buttonHeight, buttonWidth, buttonHeight);

	int w = m_pWaitingWidget->pixelWidth();
	int h = m_pWaitingWidget->pixelHeight();
	m_pWaitingWidget->setGeometry((width() - w) / 2, (height() - h) / 2, w, h);

}

void UpgradeDialog::onClickPage1Yes()
{
//	int leftMargin = 40;

	DeviceSocket *pDeviceSocket = g_pApp->getDeviceConnection();
	if (pDeviceSocket == nullptr)
	{
		QMessageBox::warning(this, tr("Warning"), tr("Can't connect to CPi2000 device, please check the network connection!"), QMessageBox::Ok);
		reject();
		return;
	}


	qDebug() << "Upgrading start...";
	m_pPage1_Yes->hide();
	m_pPage1_Cancel->hide();
	m_pVerisonLabel->hide();

	if (m_operation == UPGRADE_OPEATION)
	{
		m_pRemainderLabel->setText(tr("Upgrading..."));
	}
	else
	{
		m_pRemainderLabel->setText(tr("Downgrading..."));
	}

	m_pRemainderLabel->setAlignment(Qt::AlignHCenter);
	m_pWaitingWidget->show();

	
#if 0
	/* Before Upgrading, we need to backup the Preset.db first */
	do 
	{
		/* Add a telnet program to backup the presets.db in CPi2000 */
		BlockedTelnet telnet(this);
		QString strHostAddr = g_pApp->getDeviceConnection()->getDeviceAddr().toString();

	    if (!telnet.connectToHost(strHostAddr, 23, 5000))
		{
			qDebug() << "Failed to connect to Host:" << strHostAddr;
			break;
		}
		
		if (!telnet.waitFor("CPi2000 login", 3000))
		{
			qDebug() << "Failed to wait login";
			break;
		}
		telnet.sendData("root\r\n");

		if (!telnet.waitFor("Password:", 3000))
		{
			qDebug() << "Failed to Password";
			break;
		}
		telnet.sendData("cpidev\r\n");

		if (!telnet.waitFor("#", 3000))
		{
			qDebug() << "Failed to prompt";
			break;
		}

		telnet.sendData("cp /home/cpi/Software/Firmware/Model/Build/embBuild/Model/release/Presets.db /\r\n");
		if (!telnet.waitFor("#", 3000))
		{
			qDebug() << "Failed to prompt";
			break;
		}

		qDebug() << "telnet successfully!";
	} while (0);
#endif


	QString remoteFile = QString("ftp://%1/%2").arg(g_pApp->getDeviceConnection()->getDeviceAddr().toString()).arg("cpi2000.upd");
	if (g_pApp->putFtpFile(QString(":/binaries/cpi2000.upd"), remoteFile) == false)
	{
		QMessageBox::warning(this, tr("Network Abnormal"), tr("Failed to transfer CPi2000 device file!"), QMessageBox::Ok);
		setUpgradeFlag(true);
		setUpgradeResult(false);
		reject();
		return;
	}

	m_pWaitingWidget->hide();
	
	if (m_operation == UPGRADE_OPEATION)
	{
		m_pRemainderLabel->setText(tr("Firmware is transfered to CPi2000 device, please re-connect the CPi2000 after its upgrading."));
	}
	else
	{
		m_pRemainderLabel->setText(tr("Firmware is transfered to CPi2000 device, please re-connect the CPi2000 after its downgrading."));
	}

	setUpgradeFlag(true);
	setUpgradeResult(true);
	m_pPage1_Cancel->setText(tr("OK"));
	m_pPage1_Cancel->show();

}

IPAddressDialog::IPAddressDialog(QWidget *parent)	: QDialog(parent)
{
	setWindowTitle(tr("Set CPi2000 device IP Address"));
	m_pIPEdit1 = new QLineEdit(this);
	m_pIPEdit2 = new QLineEdit(this);
	m_pIPEdit3 = new QLineEdit(this);
	m_pIPEdit4 = new QLineEdit(this);
	m_pSubnetMaskEdit1 = new QLineEdit(this);
	m_pSubnetMaskEdit2 = new QLineEdit(this);
	m_pSubnetMaskEdit3 = new QLineEdit(this);
	m_pSubnetMaskEdit4 = new QLineEdit(this);
	m_pGatewayEdit1 = new QLineEdit(this);
	m_pGatewayEdit2 = new QLineEdit(this);
	m_pGatewayEdit3 = new QLineEdit(this);
	m_pGatewayEdit4 = new QLineEdit(this);

//	m_pGroupBox = new QGroupBox(this);

	m_pDHCPEnableButton = new QRadioButton(tr("Enable"), this);
	m_pDHCPEnableButton->setCheckable(true);
	m_pDHCPEnableButton->setObjectName(QStringLiteral("BlackFont12"));

	m_pDHCPDisableButton = new QRadioButton(tr("Disable"), this);
	m_pDHCPDisableButton->setCheckable(true);
	m_pDHCPDisableButton->setObjectName(QStringLiteral("BlackFont12"));

	connect(m_pDHCPEnableButton, SIGNAL(toggled(bool)), this, SLOT(onDHCPButtonToggle(bool)));
//	m_pDHCPEnableButton->setChecked(true);


	m_pDHCPLabel = new QLabel(tr("DHCP:"), this);
	m_pDHCPLabel->setAlignment(Qt::AlignRight | Qt::AlignVCenter);
	m_pDHCPLabel->setObjectName(QStringLiteral("BlackFont12"));
	
	m_pIPAddress = new QLabel(tr("IP Address:"), this);
	m_pIPAddress->setAlignment(Qt::AlignRight| Qt::AlignVCenter);
	m_pIPAddress->setObjectName(QStringLiteral("BlackFont12"));
	
	m_pIPDot1 = new QLabel(".", this);
	m_pIPDot1->setAlignment(Qt::AlignHCenter | Qt::AlignBottom);
	m_pIPDot1->setObjectName(QStringLiteral("BlackFont12"));

	m_pIPDot2 = new QLabel(".", this);
	m_pIPDot2->setAlignment(Qt::AlignHCenter | Qt::AlignBottom);
	m_pIPDot2->setObjectName(QStringLiteral("BlackFont12"));

	m_pIPDot3 = new QLabel(".", this);
	m_pIPDot3->setAlignment(Qt::AlignHCenter | Qt::AlignBottom);
	m_pIPDot3->setObjectName(QStringLiteral("BlackFont12"));

	m_pSubnetMaskLabel = new QLabel(tr("Subnet Mask:"), this);
	m_pSubnetMaskLabel->setAlignment(Qt::AlignRight| Qt::AlignVCenter);
	m_pSubnetMaskLabel->setObjectName(QStringLiteral("BlackFont12"));

	m_pSubnetMaskDot1 = new QLabel(".", this);
	m_pSubnetMaskDot1->setAlignment(Qt::AlignHCenter | Qt::AlignBottom);
	m_pSubnetMaskDot1->setObjectName(QStringLiteral("BlackFont12"));

	m_pSubnetMaskDot2 = new QLabel(".", this);
	m_pSubnetMaskDot2->setAlignment(Qt::AlignHCenter | Qt::AlignBottom);
	m_pSubnetMaskDot2->setObjectName(QStringLiteral("BlackFont12"));

	m_pSubnetMaskDot3 = new QLabel(".", this);
	m_pSubnetMaskDot3->setAlignment(Qt::AlignHCenter | Qt::AlignBottom);
	m_pSubnetMaskDot3->setObjectName(QStringLiteral("BlackFont12"));

	
	m_pGatewayLabel = new QLabel(tr("Default Gateway:"), this);
	m_pGatewayLabel->setAlignment(Qt::AlignRight| Qt::AlignVCenter);
	m_pGatewayLabel->setObjectName(QStringLiteral("BlackFont12"));
	
	m_pGatewayDot1 = new QLabel(".", this);
	m_pGatewayDot1->setAlignment(Qt::AlignHCenter | Qt::AlignBottom);
	m_pGatewayDot1->setObjectName(QStringLiteral("BlackFont12"));

	m_pGatewayDot2 = new QLabel(".", this);
	m_pGatewayDot2->setAlignment(Qt::AlignHCenter | Qt::AlignBottom);
	m_pGatewayDot2->setObjectName(QStringLiteral("BlackFont12"));

	m_pGatewayDot3 = new QLabel(".", this);
	m_pGatewayDot3->setAlignment(Qt::AlignHCenter | Qt::AlignBottom);
	m_pGatewayDot3->setObjectName(QStringLiteral("BlackFont12"));

	m_applyButton = new QPushButton(tr("Apply"), this);
//	m_applyButton->setObjectName(QStringLiteral("BlackFont12"));
	connect(m_applyButton, SIGNAL(clicked()), this, SLOT(applyIPAddress()));

	m_cancelButton = new QPushButton(tr("Cancel"), this);
//	m_cancelButton->setObjectName(QStringLiteral("BlackFont12"));
	connect(m_cancelButton, SIGNAL(clicked()), this, SLOT(reject()));

	QIntValidator* pIntValidator = new QIntValidator;
	pIntValidator->setRange(0, 255);
	m_pIPEdit1->setValidator(pIntValidator);
	m_pIPEdit2->setValidator(pIntValidator);
	m_pIPEdit3->setValidator(pIntValidator);
	m_pIPEdit4->setValidator(pIntValidator);
	m_pSubnetMaskEdit1->setValidator(pIntValidator);
	m_pSubnetMaskEdit2->setValidator(pIntValidator);
	m_pSubnetMaskEdit3->setValidator(pIntValidator);
	m_pSubnetMaskEdit4->setValidator(pIntValidator);
	m_pGatewayEdit1->setValidator(pIntValidator);
	m_pGatewayEdit2->setValidator(pIntValidator);
	m_pGatewayEdit3->setValidator(pIntValidator);
	m_pGatewayEdit4->setValidator(pIntValidator);

	CPi2000Data *pData = getCPi2000Data();
	NETWORK_SETTING networkSetting = pData->getNetworkAddress();
	refreshNetwork(networkSetting);

	setFixedSize(400, 280);
}

void IPAddressDialog::resizeEvent(QResizeEvent * /*event*/)
{
	int left = 10;
	int margin = 10;
	int topMargin = 10;
	int totalHeight = height() - topMargin;
	int unitHeight = 25;
	int w = 150;

	m_pDHCPLabel->setGeometry(left, topMargin + totalHeight / 12 * 1, w, unitHeight); 
	m_pDHCPEnableButton->setGeometry(left + w + margin, topMargin + totalHeight / 12 * 1, w / 2 - 10, unitHeight); 
	m_pDHCPDisableButton->setGeometry(left + w + margin + w / 2, topMargin + totalHeight / 12 * 1, w / 2 - 10, unitHeight); 

	m_pIPAddress->setGeometry(left, topMargin + totalHeight / 12 * 3, w, unitHeight); 
	m_pIPEdit1->setGeometry(left + w + margin, topMargin + totalHeight / 12 * 3, w / 4 - 10, unitHeight); 
	m_pIPEdit2->setGeometry(left + w + margin + w / 4, topMargin + totalHeight / 12 * 3, w / 4 - 10, unitHeight); 
	m_pIPEdit3->setGeometry(left + w + margin + w / 2, topMargin + totalHeight / 12 * 3, w / 4 - 10, unitHeight); 
	m_pIPEdit4->setGeometry(left + w + margin + w / 4 * 3, topMargin + totalHeight / 12 * 3, w / 4 - 10, unitHeight); 
	m_pIPDot1->setGeometry(left + w + margin + w / 4 - 10, topMargin + totalHeight / 12 * 3, 8, unitHeight - 6); 
	m_pIPDot2->setGeometry(left + w + margin + w / 2 - 10, topMargin + totalHeight / 12 * 3, 8, unitHeight - 6); 
	m_pIPDot3->setGeometry(left + w + margin + w / 4 * 3 - 10, topMargin + totalHeight / 12 * 3, 8, unitHeight - 6); 

	m_pSubnetMaskLabel->setGeometry(left, topMargin + totalHeight / 12 * 5, w, unitHeight); 
	m_pSubnetMaskEdit1->setGeometry(left + w + margin, topMargin + totalHeight / 12 * 5, w / 4 - 10, unitHeight); 
	m_pSubnetMaskEdit2->setGeometry(left + w + margin + w / 4, topMargin + totalHeight / 12 * 5, w / 4 - 10, unitHeight); 
	m_pSubnetMaskEdit3->setGeometry(left + w + margin + w / 2, topMargin + totalHeight / 12 * 5, w / 4 - 10, unitHeight); 
	m_pSubnetMaskEdit4->setGeometry(left + w + margin + w / 4 * 3, topMargin + totalHeight / 12 * 5, w / 4 - 10, unitHeight); 
	m_pSubnetMaskDot1->setGeometry(left + w + margin + w / 4 - 10, topMargin + totalHeight / 12 * 5, 8, unitHeight - 6); 
	m_pSubnetMaskDot2->setGeometry(left + w + margin + w / 2 - 10, topMargin + totalHeight / 12 * 5, 8, unitHeight - 6); 
	m_pSubnetMaskDot3->setGeometry(left + w + margin + w / 4 * 3 - 10, topMargin + totalHeight / 12 * 5, 8, unitHeight - 6); 

	m_pGatewayLabel->setGeometry(left, topMargin + totalHeight / 12 * 7, w, unitHeight); 
	m_pGatewayEdit1->setGeometry(left + w + margin, topMargin + totalHeight / 12 * 7, w / 4 - 10, unitHeight); 
	m_pGatewayEdit2->setGeometry(left + w + margin + w / 4, topMargin + totalHeight / 12 * 7, w / 4 - 10, unitHeight); 
	m_pGatewayEdit3->setGeometry(left + w + margin + w / 2, topMargin + totalHeight / 12 * 7, w / 4 - 10, unitHeight); 
	m_pGatewayEdit4->setGeometry(left + w + margin + w / 4 * 3, topMargin + totalHeight / 12 * 7, w / 4 - 10, unitHeight); 
	m_pGatewayDot1->setGeometry(left + w + margin + w / 4 - 10, topMargin + totalHeight / 12 * 7, 8, unitHeight - 6); 
	m_pGatewayDot2->setGeometry(left + w + margin + w / 2 - 10, topMargin + totalHeight / 12 * 7, 8, unitHeight - 6); 
	m_pGatewayDot3->setGeometry(left + w + margin + w / 4 * 3 - 10, topMargin + totalHeight / 12 * 7, 8, unitHeight - 6); 

	int buttonWidth = 80;
	m_applyButton->setGeometry(width() / 3 - buttonWidth / 2, totalHeight / 12 * 10, buttonWidth, 30);
	m_cancelButton->setGeometry(width() / 3 * 2 - buttonWidth / 2, totalHeight / 12 * 10, buttonWidth, 30);;
}

void IPAddressDialog::applyIPAddress()
{
	m_networkSetting.m_dhcpEnable = m_pDHCPEnableButton->isChecked();

	if (m_networkSetting.m_dhcpEnable == false)
	{
		QLineEdit *pEdit[] = {m_pIPEdit1, m_pIPEdit2, m_pIPEdit3, m_pIPEdit4, m_pSubnetMaskEdit1, m_pSubnetMaskEdit2, m_pSubnetMaskEdit3, m_pSubnetMaskEdit4, m_pGatewayEdit1, m_pGatewayEdit2, m_pGatewayEdit3, m_pGatewayEdit4};
		QString reminder[] = {tr("IP Address"), tr("IP Address"), tr("IP Address"), tr("IP Address"), tr("Subnet mask"), tr("Subnet mask"), tr("Subnet mask"), tr("Subnet mask"), tr("Gateway"), tr("Gateway"), tr("Gateway"), tr("Gateway") };
		for (int i = 0; i < 12; i++)
		{
			if (pEdit[i]->text().isEmpty())
			{
				QMessageBox::warning(this, tr("Format Error"), tr("Please input valid %1!").arg(reminder[i]));
				return;
			}
		}

		QString ipAddr = QString("%1.%2.%3.%4").arg(m_pIPEdit1->text()).arg(m_pIPEdit2->text()).arg(m_pIPEdit3->text()).arg(m_pIPEdit4->text());
		QString gateway = QString("%1.%2.%3.%4").arg(m_pGatewayEdit1->text()).arg(m_pGatewayEdit2->text()).arg(m_pGatewayEdit3->text()).arg(m_pGatewayEdit4->text());
		QString subnetMask = QString("%1.%2.%3.%4").arg(m_pSubnetMaskEdit1->text()).arg(m_pSubnetMaskEdit2->text()).arg(m_pSubnetMaskEdit3->text()).arg(m_pSubnetMaskEdit4->text());
		qDebug() << "ipAddr = " << ipAddr;
		qDebug() << "subnetMask = " << subnetMask;
		qDebug() << "gateway = " << gateway;

		m_networkSetting.m_ipAddress = QHostAddress(ipAddr);
		m_networkSetting.m_ipGateway = QHostAddress(gateway);
		m_networkSetting.m_networkMask = QHostAddress(subnetMask);

		/* Validate the subnet */
		UINT32 ulMask = m_networkSetting.m_networkMask.toIPv4Address();
		int netMask = 0;
		UINT32 ulValue = 0xFFFFFFFF;
		for (int i = 0; i < 32; i++)
		{
			UINT32 temp1 = ~(1 << i);
			ulValue = ulValue & temp1;
			qDebug() << ulValue;
			if (ulMask == ulValue)
			{
				netMask = 32 - i - 1;
				break;
			}
		}
		if ((netMask < 1) || (netMask > 31))
		{
			QMessageBox::warning(this, tr("Format Error"), tr("Subnet mask error: %1 is not valie subnet mask!").arg(subnetMask));
			return;	
		}

		/* gateway must be in the same network with ip address */
		if (!m_networkSetting.m_ipAddress.isInSubnet(m_networkSetting.m_ipGateway, netMask))
		{
			QMessageBox::warning(this, tr("Format Error"), tr("Gateway should be in the same subnet with IP address!"));
			return;			
		}

		/* host in Subnet shouldn't be all 0 or 1  */
		{
			int hostId = m_networkSetting.m_ipAddress.toIPv4Address() & ~(m_networkSetting.m_networkMask.toIPv4Address());
			int broadcase = (1 << (32 - netMask)) - 1;
			if ((hostId == 0) || (hostId == broadcase))
			{
				QMessageBox::warning(this, tr("Format Error"), tr("IP Address should not be broadcast address!"));
				return;						
			}

			int gatewayId = m_networkSetting.m_ipGateway.toIPv4Address() & ~(m_networkSetting.m_networkMask.toIPv4Address());
			if ((gatewayId == 0) || (gatewayId == broadcase))
			{
				QMessageBox::warning(this, tr("Format Error"), tr("Gateway should not be broadcast address!"));
				return;						
			}
		}
	}
	else
	{
		CPi2000Data *pData = getCPi2000Data();
		m_networkSetting = pData->getNetworkAddress();
		m_networkSetting.m_dhcpEnable = true;
	}

	accept();
}


void IPAddressDialog::onDHCPButtonToggle(bool flag)
{
	m_pIPEdit1->setEnabled(!flag);
	m_pIPEdit2->setEnabled(!flag);
	m_pIPEdit3->setEnabled(!flag);
	m_pIPEdit4->setEnabled(!flag);

	m_pSubnetMaskEdit1->setEnabled(!flag);
	m_pSubnetMaskEdit2->setEnabled(!flag);
	m_pSubnetMaskEdit3->setEnabled(!flag);
	m_pSubnetMaskEdit4->setEnabled(!flag);

	m_pGatewayEdit4->setEnabled(!flag);
	m_pGatewayEdit3->setEnabled(!flag);
	m_pGatewayEdit2->setEnabled(!flag);
	m_pGatewayEdit1->setEnabled(!flag);
}


void IPAddressDialog::setIPAddress(QHostAddress address)
{
	UINT32 addr = address.toIPv4Address();
	m_pIPEdit4->setText(QString::number(addr & 0xff));
	m_pIPEdit3->setText(QString::number((addr >> 8) & 0xff));
	m_pIPEdit2->setText(QString::number((addr >> 16) & 0xff));
	m_pIPEdit1->setText(QString::number((addr >> 24) & 0xff));
}

void IPAddressDialog::setSubMask(QHostAddress address)
{
	UINT32 addr = address.toIPv4Address();
	m_pSubnetMaskEdit4->setText(QString::number(addr & 0xff));
	m_pSubnetMaskEdit3->setText(QString::number((addr >> 8) & 0xff));
	m_pSubnetMaskEdit2->setText(QString::number((addr >> 16) & 0xff));
	m_pSubnetMaskEdit1->setText(QString::number((addr >> 24) & 0xff));
}

void IPAddressDialog::setDHCPEnable(bool flag)
{
	m_pDHCPEnableButton->setChecked(flag);
	m_pDHCPDisableButton->setChecked(!flag);
}

void IPAddressDialog::refreshNetwork(NETWORK_SETTING networkSetting)
{
	setSubMask(networkSetting.m_networkMask);
	setIPAddress(networkSetting.m_ipAddress);
	setDefaultGateWay(networkSetting.m_ipGateway);
	setDHCPEnable(networkSetting.m_dhcpEnable);
}

void IPAddressDialog::setDefaultGateWay(QHostAddress address)
{
	UINT32 addr = address.toIPv4Address();
	m_pGatewayEdit4->setText(QString::number(addr & 0xff));
	m_pGatewayEdit3->setText(QString::number((addr >> 8) & 0xff));
	m_pGatewayEdit2->setText(QString::number((addr >> 16) & 0xff));
	m_pGatewayEdit1->setText(QString::number((addr >> 24) & 0xff));
}


TimeAdjustDialog::TimeAdjustDialog(QWidget *parent)	: QDialog(parent)
{
	setWindowTitle(tr("Calibrate Time"));
	m_pCurrentTimeLabel = new QLabel(tr("PC Host Time:"), this);
	m_pCurrentTimeLabel->setAlignment(Qt::AlignRight);
	m_pCurrentTimeLabel->setObjectName(QStringLiteral("BlackFont12"));

	m_pDeviceTimeLabel = new QLabel(tr("Device Time:"), this);
	m_pDeviceTimeLabel->setAlignment(Qt::AlignRight);
	m_pDeviceTimeLabel->setObjectName(QStringLiteral("BlackFont12"));

	QString time = QDate::currentDate().toString(Qt::ISODate) + " " + QTime::currentTime().toString();
	m_pCurrentTimeEdit = new QLineEdit(time, this);
	m_pCurrentTimeEdit->setEnabled(false);

	m_pDeviceTimeEdit = new QLineEdit(this);
	m_pDeviceTimeEdit->setEnabled(false);

//	m_pSetButton = new QPushButton(tr("Set Time"), this);
	m_pSyncButton = new QPushButton(tr("Sync Time"), this);
	connect(m_pSyncButton, SIGNAL(clicked()), this, SLOT(onSyncTime()));

	m_pCancelButton = new QPushButton(tr("Exit"), this);
	connect(m_pCancelButton, SIGNAL(clicked()), this, SLOT(reject()));

	int ms = 1000; // 1000ms = 1s
	m_pTimer = new QTimer(this);
	m_pTimer->start(ms);
	connect(m_pTimer, SIGNAL(timeout()), this, SLOT(onTimerOut()));

	setFixedSize(380, 220);
}

void TimeAdjustDialog::resizeEvent(QResizeEvent * /* event */)
{
	int leftMargin = 20;
	m_pCurrentTimeLabel->setGeometry(leftMargin, 42, 90, 20);
	m_pDeviceTimeLabel->setGeometry(leftMargin, 82, 90, 20);

	m_pCurrentTimeEdit->setGeometry(leftMargin + 100, 40, width() - 2 * leftMargin - 140, 20);
	m_pDeviceTimeEdit->setGeometry(leftMargin + 100, 80, width() - 2 * leftMargin - 140, 20);

	int buttonWidth = 90;
//	m_pSetButton->setGeometry(width() / 4 - buttonWidth / 2 - 10, 160, buttonWidth, 30);
	m_pSyncButton->setGeometry(width() / 3 * 1- buttonWidth / 2, 160, buttonWidth, 30);
	m_pCancelButton->setGeometry(width() / 3 * 2 - buttonWidth / 2 + 10, 160, buttonWidth, 30);
}

void TimeAdjustDialog::onTimerOut()
{
//	bool enableFlag = false;
	DeviceSocket *pSocket = g_pApp->getDeviceConnection();

	QString strCurrentTime = QDate::currentDate().toString(Qt::ISODate) + " " + QTime::currentTime().toString();
	m_pCurrentTimeEdit->setText(strCurrentTime);

	if (pSocket == nullptr)
	{
		m_pDeviceTimeEdit->setText("Unavailable");
	}
	else
	{
		QDate deviceDate;
		QTime deviceTime;
		pSocket->getDeviceTime();
		g_pApp->msleep(200);
		CPi2000Data *pData = getCPi2000Data();
		pData->getDeviceTime(deviceDate, deviceTime);

		QDateTime deviceDateTime;
		deviceDateTime.setDate(deviceDate);
		deviceDateTime.setTime(deviceTime);
		int diff = deviceDateTime.secsTo(QDateTime::currentDateTime());
		if ((diff <= 5) && (diff >= -5))
		{
			m_pDeviceTimeEdit->setText(strCurrentTime);
		}
		else
		{
			QString strTime = deviceDate.toString(Qt::ISODate) + " " + deviceTime.toString();
			m_pDeviceTimeEdit->setText(strTime);
		}
	}
}
 
void TimeAdjustDialog::onSyncTime()
{
	DeviceSocket *pDeviceSocket = g_pApp->getDeviceConnection();
	if (pDeviceSocket == nullptr)
	{
		QMessageBox::warning(this, tr("Warning"), tr("Can't connect to CPi2000 device, please check the network connection!"), QMessageBox::Ok);
		return;
	}			
	else
	{
		g_waittingWidget.startWaitting(this);
		m_pSyncButton->setEnabled(false);

		pDeviceSocket->setDeviceTime(QDate::currentDate(), QTime::currentTime());

		g_pApp->msleep(3000);
		m_pSyncButton->setEnabled(true);
		g_waittingWidget.stopWaitting();
	}
}