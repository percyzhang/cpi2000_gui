#include "ValueWidget.h"
#include <QPainter>
#include <QRect>

ValueWidget::ValueWidget(QWidget *pParent) : QWidget(pParent)
{
	m_backgroundColor = QColor(50, 50, 50);
	m_standColor = QColor(0xf0, 0xf0, 0xf0);
	setScale(0, 0, 100, 100);
	m_oldValue = -10000;
	m_newValue = 0.0;
	m_extraGain = 0;
}

void ValueWidget::setValue(float currentValue)
{
	m_newValue = currentValue;
}

void ValueWidget::setExtraGain(float currentValue)
{
	m_extraGain = currentValue;
}

void ValueWidget::setScale(float value1, float scale1, float value2, float scale2)
{
	m_scaleValue.m_value1 = value1;
	m_scaleValue.m_scale1 = scale1;
	m_scaleValue.m_value2 = value2;
	m_scaleValue.m_scale2 = scale2;
	m_oldValue = -10000;
}

void ValueWidget::refresh()
{
	if (m_newValue == m_oldValue)
	{
		return;
	}
	else
	{
		update();
	}
}

QColor ValueWidget::getColor(float /* newValue */)
{
//	return (QColor(0, 240, 0));
	return m_solidColor;
}

void ValueWidget::paintEvent(QPaintEvent * /* event */ )
{
	QPainter painter(this);

	float newValue = m_newValue + m_extraGain;

	painter.fillRect(0, 0, width(), height(), m_backgroundColor);
	float percent = (m_scaleValue.m_scale2 - m_scaleValue.m_scale1) * (newValue - m_scaleValue.m_value1) / (m_scaleValue.m_value2 - m_scaleValue.m_value1) + m_scaleValue.m_scale1;

//	if (m_direction == DIRECTION_VERTICAL)
	{
		int h = percent * height() / 100;
		if (h > height())
		{
			h = height();
		}
		QColor color = getColor(newValue);

#if 0
		painter.setPen(color);
		for (int i = 0; i < h; i+=2)
		{
			painter.drawLine(0, height() - i, width(), height() - i);
		}
#else
		painter.fillRect(0, height() - h, width(), h, color);
#endif
		m_oldValue = m_newValue;

		float curPercent = (m_scaleValue.m_scale2 - m_scaleValue.m_scale1) * (m_standValue - m_scaleValue.m_value1) / (m_scaleValue.m_value2 - m_scaleValue.m_value1) + m_scaleValue.m_scale1;
		h = curPercent * height() / 100;
		painter.setPen(QPen(m_standColor, 2));
		painter.drawLine(0, height() - h, width(), height() - h);
	}
}

CalWidget::CalWidget(QWidget *pWidget) : QWidget(pWidget)
{
	m_backgroundColor = QColor(0x90, 0x90, 0x90);
	setScale(0, 0, 100, 100);
	m_oldValue = 0.0;
	m_newValue = 0.0;
	
	m_color1 = QColor(255, 0, 0);
	m_color2 = QColor(0, 255, 0);
	m_color3 = QColor(255, 0, 0);

	m_leftCal = 30;
	m_rightCal = 70;

	m_extraGain = 0;


	m_pTimer = new QTimer(this);
    connect(m_pTimer, SIGNAL(timeout()), this, SLOT(onTimer_100ms()));
	m_pTimer->start(100);
}

	/*	
		setScale(-60.0, 10, 0.0, 90) 
			means:	when the value = -60.0dB, it will reach at the 10% of the widget height.
					when the value = -0.0dB, it will reach at the 90% of the widget height.
	*/
void CalWidget::setScale(float value1, float scale1, float value2, float scale2)
{
	m_scaleValue.m_value1 = value1;
	m_scaleValue.m_scale1 = scale1;
	m_scaleValue.m_value2 = value2;
	m_scaleValue.m_scale2 = scale2;
	m_zeroValue = value1 - scale1 *(value2 - value1) / (scale2 - scale1);
	m_oldValue = m_zeroValue;
}

void CalWidget::setValue(float currentValue)
{
	m_newValue = currentValue;
	if (m_newValue < m_zeroValue)
	{
		m_newValue = m_zeroValue;
	}
}

void CalWidget::setFrontColor(QColor color1, QColor color2, QColor color3, float leftDB, float rightDB)
{
	m_color1 = color1;
	m_color2 = color2;
	m_color3 = color3;
	m_leftCal = leftDB;
	m_rightCal = rightDB;
	
	m_leftPercent = (m_scaleValue.m_scale2 - m_scaleValue.m_scale1) / (m_scaleValue.m_value2 - m_scaleValue.m_value1) * (leftDB - m_scaleValue.m_value1) + m_scaleValue.m_scale1;
	m_rightPercent = (m_scaleValue.m_scale2 - m_scaleValue.m_scale1) / (m_scaleValue.m_value2 - m_scaleValue.m_value1) * (rightDB - m_scaleValue.m_value1) + m_scaleValue.m_scale1;
}

#define STEP_VALUE 0.5

void CalWidget::refresh()
{
	if (m_newValue == m_oldValue)
	{
		return;
	}
	else
	{
		update();
	}
}

class  ArcCalc 
{
public:
	ArcCalc(int left, int right, int top, int bottom) { m_left = left; m_right = right; m_top = top; m_bottom = bottom; }
	int m_left, m_right; 
	int m_top, m_bottom;

	void getPercentPoint(int percent, int height, QPoint point[2])
	{
		if (percent > 50)
		{
			height = -height;
		}

		float x = m_left + (m_right - m_left) * percent / 100;
		float y = m_bottom - (1 - pow(((float)percent - 50) / 50, 2)) * (m_bottom - m_top);


		if (abs(2 * x - m_left - m_right) < 5)
		{
			point[0].setX(x);
			point[0].setY(y - height / 2 - 2);
			point[1].setX(x);
			point[1].setY(y + height / 2);
			return;
		}
		else
		{
			float differX = (m_left + m_right) / 2 - x;
			float differY = (y - m_top) * 3;

			float dx = height / 2 * differY / sqrt(differX * differX + differY * differY);
			float dy = height / 2 * differX / sqrt(differX * differX + differY * differY);
			point[0].setX(x - dx);
			point[0].setY(y - dy - 2);
			point[1].setX(x + dx);
			point[1].setY(y + dy);
			return;
		}
	}
};


#if 1
void CalWidget::paintEvent(QPaintEvent * /* event */)
{
	QPainter painter(this);
	int percent;
	painter.setRenderHint(QPainter::HighQualityAntialiasing);

	if (fabs(m_newValue - m_oldValue) < STEP_VALUE)
	{
		m_oldValue = m_newValue;
	}
	else
	{
		if (m_newValue > m_oldValue)
		{
			m_oldValue += fabs(m_newValue - m_oldValue) / 5;
		}
		else
		{
			m_oldValue -= fabs(m_newValue - m_oldValue) / 5;
		}
	}
	float newValue = m_oldValue + m_extraGain;

	QPixmap background(":Calibration.png");
//	painter.drawPixmap(0, 0, background.scaled(size()));

	painter.setPen(QPen(QColor(0, 0, 0), 1));
	painter.drawRect(0, 0, width() - 1, height() - 1);
#if 1

	percent = (m_scaleValue.m_scale2 - m_scaleValue.m_scale1) * (newValue - m_scaleValue.m_value1) / (m_scaleValue.m_value2 - m_scaleValue.m_value1) + m_scaleValue.m_scale1;

	if (percent < 0)
	{
		percent = 0;
	}
	if (percent > 100)
	{
		percent = 100;
	}

	/* Draw Percent */
	{
		int left = width() * 0.042;
		int right = width() * 0.958;

		int top = height() * 0.35;
		int bottom = height() * 0.85;

		ArcCalc theArc(left, right, top, bottom);

		QPoint point[202];
		for (int i = 0; i <= 100; i++)
		{
			theArc.getPercentPoint(i, 16, point + 2 * i);
		}
		QPoint center((point[200].x() + point[201].x()) / 2, (point[200].y() + point[201].y()) / 2);
		int radius = 9;

#if 1
		QPainterPath path;
		path.moveTo(point[0].x(), point[0].y());

		for (int i = 0; i < 100; i++)
		{
			path.lineTo(point[2 * i + 2].x(), point[2 * i + 2].y());
		}
		path.arcTo(QRect(center.x() - radius, center.y() - radius, 2 * radius, 2 * radius), 80, -180);
		path.lineTo(point[201].x(), point[201].y());
		for (int i = 99; i >= 0; i--)
		{
			path.lineTo(point[2 * i + 1].x(), point[2 * i + 1].y());
		}
		center = QPoint((point[0].x() + point[1].x()) / 2, (point[0].y() + point[1].y()) / 2 + 1);
		path.arcTo(QRect(center.x() - radius, center.y() - radius - 1, 2 * radius - 1, 2 * radius - 1), -80, -180);
		path.closeSubpath();
		painter.fillPath(path, QBrush(m_backgroundColor));
#endif
		{
			QPainterPath path;
			path.moveTo(point[m_leftPercent * 2].x(), point[m_leftPercent * 2].y());
			for (int i = m_leftPercent; i < m_rightPercent; i++)
			{
				path.lineTo(point[i * 2 + 2].x(), point[i * 2 + 2].y());
			}
			path.lineTo(point[m_rightPercent * 2 + 1].x(), point[m_rightPercent * 2 + 1].y());
			for (int i = m_rightPercent - 1; i >= m_leftPercent; i--)
			{
				path.lineTo(point[i * 2 + 1].x(), point[i * 2 + 1].y());
			}
			path.closeSubpath();
			QColor fillColor = Qt::green;
			fillColor.setAlpha(20);
			painter.fillPath(path, QBrush(fillColor));
		}
#if 1
		if (percent != 0)
		{
			/* Only draw Red */
			if (percent < m_leftPercent)
			{
				QPainterPath path;
				path.moveTo(point[0].x(), point[0].y());
				for (int i = 0; i < percent; i++)
				{
					path.lineTo(point[2 * i + 2].x(), point[2 * i + 2].y());
				}
				path.lineTo(point[2 * percent + 1].x(), point[2 * percent + 1].y());
				for (int i = percent - 1; i >= 0; i--)
				{
					path.lineTo(point[2 * i + 1].x(), point[2 * i + 1].y());
				}
				center = QPoint((point[0].x() + point[1].x()) / 2, (point[0].y() + point[1].y()) / 2 + 1);
				path.arcTo(QRect(center.x() - radius, center.y() - radius - 1, 2 * radius - 1, 2 * radius - 1), -80, -180);
				path.closeSubpath();
				painter.fillPath(path, QBrush(m_color1));				
			}
			else if (percent < m_rightPercent)
			{
				/* Left: Red */
				{
					QPainterPath path;
					path.moveTo(point[0].x(), point[0].y());
					for (int i = 0; i < m_leftPercent; i++)
					{
						path.lineTo(point[2 * i + 2].x(), point[2 * i + 2].y());
					}
					path.lineTo(point[2 * m_leftPercent + 1].x(), point[2 * m_leftPercent + 1].y());
					for (int i = m_leftPercent - 1; i >= 0; i--)
					{
						path.lineTo(point[2 * i + 1].x(), point[2 * i + 1].y());
					}
					center = QPoint((point[0].x() + point[1].x()) / 2, (point[0].y() + point[1].y()) / 2 + 1);
					path.arcTo(QRect(center.x() - radius, center.y() - radius - 1, 2 * radius - 1, 2 * radius - 1), -80, -180);
					path.closeSubpath();
					painter.fillPath(path, QBrush(m_color1));						
				}

				/* Then: Green */
				{
					QPainterPath path;
					path.moveTo(point[2 * m_leftPercent].x(), point[2 * m_leftPercent].y());
					for (int i = m_leftPercent; i < percent; i++)
					{
						path.lineTo(point[2 * i + 2].x(), point[2 * i + 2].y());
					}
					path.lineTo(point[2 * percent + 1].x(), point[2 * percent + 1].y());
					for (int i = percent - 1; i >= m_leftPercent; i--)
					{
						path.lineTo(point[2 * i + 1].x(), point[2 * i + 1].y());
					}
					path.closeSubpath();
					painter.fillPath(path, QBrush(m_color2));					
				}		
			}
			else if (percent != 100)
			{
				/* Left: Red */
				{
					QPainterPath path;
					path.moveTo(point[0].x(), point[0].y());
					for (int i = 0; i < m_leftPercent; i++)
					{
						path.lineTo(point[2 * i + 2].x(), point[2 * i + 2].y());
					}
					path.lineTo(point[2 * m_leftPercent + 1].x(), point[2 * m_leftPercent + 1].y());
					for (int i = m_leftPercent - 1; i >= 0; i--)
					{
						path.lineTo(point[2 * i + 1].x(), point[2 * i + 1].y());
					}
					center = QPoint((point[0].x() + point[1].x()) / 2, (point[0].y() + point[1].y()) / 2 + 1);
					path.arcTo(QRect(center.x() - radius, center.y() - radius - 1, 2 * radius - 1, 2 * radius - 1), -80, -180);
					path.closeSubpath();
					painter.fillPath(path, QBrush(m_color1));						
				}

				/* Then: Green */
				{
					QPainterPath path;
					path.moveTo(point[2 * m_leftPercent].x(), point[2 * m_leftPercent].y());
					for (int i = m_leftPercent; i < m_rightPercent; i++)
					{
						path.lineTo(point[2 * i + 2].x(), point[2 * i + 2].y());
					}
					path.lineTo(point[2 * m_rightPercent + 1].x(), point[2 * m_rightPercent + 1].y());
					for (int i = m_rightPercent - 1; i >= m_leftPercent; i--)
					{
						path.lineTo(point[2 * i + 1].x(), point[2 * i + 1].y());
					}
					path.closeSubpath();
					painter.fillPath(path, QBrush(m_color2));					
				}

				/* Then: Red */
				{
					QPainterPath path;
					path.moveTo(point[2 * m_rightPercent].x(), point[2 * m_rightPercent].y());
					for (int i = m_rightPercent; i < percent; i++)
					{
						path.lineTo(point[2 * i + 2].x(), point[2 * i + 2].y());
					}
					path.lineTo(point[2 * percent + 1].x(), point[2 * percent + 1].y());
					for (int i = percent - 1; i >= m_rightPercent; i--)
					{
						path.lineTo(point[2 * i + 1].x(), point[2 * i + 1].y());
					}
					path.closeSubpath();
					painter.fillPath(path, QBrush(m_color3));				
				}	
			}
			else  // percent = 100
			{
				/* Left: Red */
				{
					QPainterPath path;
					path.moveTo(point[0].x(), point[0].y());
					for (int i = 0; i < m_leftPercent; i++)
					{
						path.lineTo(point[2 * i + 2].x(), point[2 * i + 2].y());
					}
					path.lineTo(point[2 * m_leftPercent + 1].x(), point[2 * m_leftPercent + 1].y());
					for (int i = m_leftPercent - 1; i >= 0; i--)
					{
						path.lineTo(point[2 * i + 1].x(), point[2 * i + 1].y());
					}
					center = QPoint((point[0].x() + point[1].x()) / 2, (point[0].y() + point[1].y()) / 2 + 1);
					path.arcTo(QRect(center.x() - radius, center.y() - radius - 1, 2 * radius - 1, 2 * radius - 1), -80, -180);
					path.closeSubpath();
					painter.fillPath(path, QBrush(m_color1));						
				}

				/* Then: Green */
				{
					QPainterPath path;
					path.moveTo(point[2 * m_leftPercent].x(), point[2 * m_leftPercent].y());
					for (int i = m_leftPercent; i < m_rightPercent; i++)
					{
						path.lineTo(point[2 * i + 2].x(), point[2 * i + 2].y());
					}
					path.lineTo(point[2 * m_rightPercent + 1].x(), point[2 * m_rightPercent + 1].y());
					for (int i = m_rightPercent - 1; i >= m_leftPercent; i--)
					{
						path.lineTo(point[2 * i + 1].x(), point[2 * i + 1].y());
					}
					path.closeSubpath();
					painter.fillPath(path, QBrush(m_color2));					
				}

				/* Then: Red */
				{
					QPainterPath path;
					path.moveTo(point[2 * m_rightPercent].x(), point[2 * m_rightPercent].y());
					for (int i = m_rightPercent; i < 100; i++)
					{
						path.lineTo(point[2 * i + 2].x(), point[2 * i + 2].y());
					}
					QPoint center((point[200].x() + point[201].x()) / 2, (point[200].y() + point[201].y()) / 2);
					int radius = 9;
					path.arcTo(QRect(center.x() - radius, center.y() - radius, 2 * radius, 2 * radius), 80, -180);
					path.lineTo(point[201].x(), point[201].y());
					for (int i = 99; i >= m_rightPercent; i--)
					{
						path.lineTo(point[2 * i + 1].x(), point[2 * i + 1].y());
					}
					path.closeSubpath();
					painter.fillPath(path, QBrush(m_color3));				
				}					
			}
		}
#endif
#if 1
		QColor borderColor(0x30, 0x30, 0x30);
//		QColor borderColor = m_backgroundColor;
//		borderColor.setAlpha(0X80);
//		painter.setPen(QPen(borderColor, 1));
		painter.setPen(QPen(borderColor, 1, Qt::SolidLine, Qt::RoundCap));  
		painter.setRenderHint(QPainter::Antialiasing, true);  

		for (int i = 0; i < 100; i++)
		{
//			painter.drawLine(point[2 * i].x(), point[2 * i].y(), point[2 * i + 2].x(), point[2 * i + 2].y());
//			painter.drawLine(point[2 * i + 1].x(), point[2 * i + 1].y(), point[2 * i + 3].x(), point[2 * i + 3].y());
		}

		{
			/*
			 draw outer Circle
			*/
			int x2 = point[0].x();
			int y2 = point[0].y();
			int xc = point[100].x();
			int yc = point[100].y();
//			int x1 = point[200].x();
//			int y1 = point[200].y();

			int y = ((x2 - xc) * (x2 - xc) + y2 * y2 - yc * yc) / (2 * (y2 - yc)); 
			int radius = y - yc;
//			QRect rect(xc - radius, xc, yc, radius * 2, radius * 2);
			QRect circleRect(xc - radius, yc, radius * 2, radius * 2);

			painter.setPen(QPen(borderColor, 1));

			float sinx = float(radius - (y2 - yc)) / radius;
			qreal pi=3.1415926535897932384626433832795;
			float angle = asin(sinx) * 180 / pi;
			painter.drawArc(circleRect, angle * 16, (180 - 2 * angle) * 16);
		}

		{
			/*
			 draw inner Circle
			*/
			int x2 = point[1].x();
			int y2 = point[1].y();
			int xc = point[101].x();
			int yc = point[101].y();
//			int x1 = point[201].x();
//			int y1 = point[201].y();

			int y = ((x2 - xc) * (x2 - xc) + y2 * y2 - yc * yc) / (2 * (y2 - yc)); 
			int radius = y - yc;
//			QRect rect(xc - radius, xc, yc, radius * 2, radius * 2);
			QRect circleRect(xc - radius, yc, radius * 2, radius * 2);

			painter.setPen(QPen(borderColor, 1));

			float sinx = float(radius - (y2 - yc)) / radius;
			qreal pi=3.1415926535897932384626433832795;
			float angle = asin(sinx) * 180 / pi;
			painter.drawArc(circleRect, angle * 16, (180 - 2 * angle) * 16);
		}
		center = QPoint((point[0].x() + point[1].x()) / 2, (point[0].y() + point[1].y()) / 2 + 1);
		painter.drawArc(QRect(center.x() - radius, center.y() - radius, 2 * radius - 1, 2 * radius - 1), -80 * 16, -180 * 16);

		center = QPoint((point[200].x() + point[201].x()) / 2, (point[200].y() + point[201].y()) / 2);
		painter.drawArc(QRect(center.x() - radius, center.y() - radius, 2 * radius, 2 * radius), 80 * 16, -180 * 16);

		painter.drawLine(point[2 * m_leftPercent].x(), point[2 * m_leftPercent].y(), point[2 * m_leftPercent + 1].x(), point[2 * m_leftPercent + 1].y());
		painter.drawLine(point[2 * m_rightPercent].x(), point[2 * m_rightPercent].y(), point[2 * m_rightPercent + 1].x(), point[2 * m_rightPercent + 1].y());

		painter.setPen(QPen(QColor(0x0, 0x0, 0x0), 2));
		theArc.getPercentPoint(percent, 22, point);
		painter.drawLine(point[0].x(), point[0].y(), point[1].x(), point[1].y());
#endif
	}

#endif
}
#else
void CalWidget::paintEvent(QPaintEvent *event)
{
	QPainter painter(this);
	float percent;

	if (fabs(m_newValue - m_oldValue) < STEP_VALUE)
	{
		m_oldValue = m_newValue;
	}
	else
	{
		if (m_newValue > m_oldValue)
		{
			m_oldValue += fabs(m_newValue - m_oldValue) / 5;
		}
		else
		{
			m_oldValue -= fabs(m_newValue - m_oldValue) / 5;
		}
	}
	float newValue = m_oldValue + m_extraGain;

	QPixmap background(":Calibration.png");
	painter.drawPixmap(0, 0, background.scaled(size()));

	painter.setPen(QPen(QColor(0, 0, 0), 1));
	painter.drawRect(0, 0, width() - 1, height() - 1);
#if 1

	percent = (m_scaleValue.m_scale2 - m_scaleValue.m_scale1) * (newValue - m_scaleValue.m_value1) / (m_scaleValue.m_value2 - m_scaleValue.m_value1) + m_scaleValue.m_scale1;
	float leftPercent = (m_scaleValue.m_scale2 - m_scaleValue.m_scale1) * (m_leftCal - m_scaleValue.m_value1) / (m_scaleValue.m_value2 - m_scaleValue.m_value1) + m_scaleValue.m_scale1;
	float rightPercent = (m_scaleValue.m_scale2 - m_scaleValue.m_scale1) * (m_rightCal - m_scaleValue.m_value1) / (m_scaleValue.m_value2 - m_scaleValue.m_value1) + m_scaleValue.m_scale1;

	if (percent < 0)
	{
		percent = 0;
	}
	if (percent > 100)
	{
		percent = 100;
	}

	/* Draw Percent */
	{
		int left = width() * 0.04;
		int right = width() * 0.96;
		int x = left + (right - left) * percent / 100;

		int top = height() * 0.35;
		int bottom = height() * 0.85;
		int y = bottom - (1 - pow(((float)percent - 50) / 50, 2)) * (bottom - top);

		painter.setPen(QPen(QColor(10, 10, 10), 3));
		painter.drawLine(x, y, x, y + 6);
	}
#if 0
	float percent = (m_scaleValue.m_scale2 - m_scaleValue.m_scale1) * (newValue - m_scaleValue.m_value1) / (m_scaleValue.m_value2 - m_scaleValue.m_value1) + m_scaleValue.m_scale1;
	float leftPercent = (m_scaleValue.m_scale2 - m_scaleValue.m_scale1) * (m_leftCal - m_scaleValue.m_value1) / (m_scaleValue.m_value2 - m_scaleValue.m_value1) + m_scaleValue.m_scale1;
	float rightPercent = (m_scaleValue.m_scale2 - m_scaleValue.m_scale1) * (m_rightCal - m_scaleValue.m_value1) / (m_scaleValue.m_value2 - m_scaleValue.m_value1) + m_scaleValue.m_scale1;

	if (percent < 0)
	{
		percent = 0;
		return;
	}
	int w = percent * width() / 100;
	if (w > width())
	{
		w = width();
	}
	int leftPoint = leftPercent * width() / 100;
	int rightPoint = rightPercent * width() / 100;


	QColor oldColor = QColor(0, 0, 0);
	for (int i = 0; i < w; i += 2)
	{
		QColor color;
		if (i < leftPoint)
		{
			color = m_color1;
		}
		else if (i < rightPoint)
		{
			color = m_color2;
		}
		else
		{
			color = m_color3;
		}

		if (oldColor != color)
		{
			oldColor = color;
			painter.setPen(color);
		}
		painter.drawLine(i, 0, i, height());
	}
#endif
#endif
}
#endif

void CalWidget::onTimer_100ms(void)
{
	if (m_newValue != m_oldValue)
	{
		update();
	}
}