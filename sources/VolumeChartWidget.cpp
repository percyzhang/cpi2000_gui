#include "VolumeChartWidget.h"
#include <QPainter>
#include <QGlobal.h>
#include <QEvent>
#include "mainApplication.h"

VolumeChartWidget::VolumeChartWidget(QWidget *pParent)
    : QWidget(pParent)
{
	m_pDBValue = nullptr;

	m_volumeCount = 8;

	for (int i = 0; i < m_volumeCount; i++)
	{
		m_volumeValue[i] = 1;
	}

	m_pTimer = new QTimer(this);
	m_pTimer->start(200);
//	connect(m_pTimer, SIGNAL(timeout()), this, SLOT(onTimerOut()));
	m_volumeNameList << "L" << "R" << "C" << "Ls" << "Rs" << "Sw" << "Bls" << "Brs";

	if (!isEnabled())
	{
		hide();
	}

}

/* Return Value: 1 - MAX_VOLUME_VALUE */
UINT8 DBToOutputVolume(float DB)
{
#if 0
	int levelUpper[8] = {-80, -70, -60, -50, -40, -30, -20, -10}; // 1 : DB <=-80; 2: -80 <DB <= -70;  ... 8: -20 < DB <= -10; 9: 10 < DB
	
	int value = (int)DB;
	int i;
	for (i = 1; i <= 8; i++)
	{
		if (DB <= levelUpper[i - 1])
		{
			break;
		}
	}
	
	return i;
#else
	float minDB = -80;		// -80dB = VOL 0
	float minVol = 0;

	float maxDB = 0;		// maxDB
	float maxVol = MAX_VOLUME_VALUE;   // 0dB = VOL MAX_VOLUME_VALUE

	float value = (maxVol - minVol) / (maxDB - minDB) * (DB - minDB) + minVol;
	if (value < 1)
	{
		value = 1;
	}
	if (value > MAX_VOLUME_VALUE)
	{
		value = MAX_VOLUME_VALUE;
	}
	return qRound(value);
#endif
}

void VolumeChartWidget::refresh()
{
	onOutputVolumeChanged();
}

void VolumeChartWidget::onOutputVolumeChanged()
{
	for (int i = 0; i < m_volumeCount; i++)
	{
		m_volumeValue[i] = DBToOutputVolume(m_pDBValue[i]);
	}
	update();
}


void VolumeChartWidget::setVolumeData(float *pVolumeValue, QStringList voluemeNameList, int count)
{
	m_pDBValue = pVolumeValue;
	m_volumeNameList = voluemeNameList;
	m_volumeCount = count;
}

void VolumeChartWidget::changeEvent(QEvent *e)
{
    if(e->type() == QEvent::EnabledChange)
    {
        if(isEnabled())
        {
			show();
        }
        else
        {
			hide();
        }
    }
}

void VolumeChartWidget::onTimerOut()
{
//	bool updateFlag = false;
	int i;

	if (!isEnabled())
	{
		return;
	}

	/* Just for test */
	if (m_pDBValue == nullptr)
	{
		/* Just for Demo */
		for (i = 0; i < m_volumeCount; i++)
		{
			m_volumeValue[i] = qrand() % MAX_VOLUME_VALUE;
		}
		update();
	}
	else
	{
		onOutputVolumeChanged();
	}
}

QColor VolumeChartWidget::getVolumeValueColor(int value)
{
	if (value <= qRound(MAX_VOLUME_VALUE * 0.65))
	{
		return (QColor(0, 255, 0));
	}
	else if (value <= qRound(MAX_VOLUME_VALUE * 0.86))
	{
		return (QColor(255, 255, 0));
	}
	else
	{
		return (QColor(255, 0, 0));
	}
}

void VolumeChartWidget::paintEvent(QPaintEvent * /* event */)
{
	QPainter painter(this);
//	painter.setPen(QPen(QColor(10, 10, 10), 2));
//	painter.drawRect(0, 0, width(), height());

	int chartHeight = height() * 5 / 6;
	int volumeUnitHeight = (int)((float)chartHeight / (MAX_VOLUME_VALUE + 1) * 2 / 3);
	int volumeUnitWidth = width() / m_volumeCount / 3;

	for (int i = 0; i < m_volumeCount; i++)
	{
#if 1
		if (m_volumeValue[i] < 1)
		{
			m_volumeValue[i] = 1;
		}
		if (m_volumeValue[i] > MAX_VOLUME_VALUE)
		{
			m_volumeValue[i] = MAX_VOLUME_VALUE;
		}

		for (int value = 0; value < m_volumeValue[i]; value++)
		{
			QColor color = getVolumeValueColor(value);
			int left =  (i + 0.5) / m_volumeCount * width() - volumeUnitWidth / 2;
			int top = float(MAX_VOLUME_VALUE - value) / (MAX_VOLUME_VALUE + 1) * chartHeight;
			painter.fillRect(left, top, volumeUnitWidth, volumeUnitHeight, color);
		}
#endif
		painter.setPen(QPen(QColor(255, 255, 255), 1));
		painter.drawText(i * width() / m_volumeCount, chartHeight, width() / m_volumeCount, height() - chartHeight, Qt::AlignCenter, m_volumeNameList.at(i));
	}
#if 0
	int left = 14;
	int right = 20;
	int top = height() - 9; 
	QPainter painter(this);
	painter.setPen(QPen(QColor(10, 10, 10), 2));
	painter.drawLine(left, top, width() - right, top);

	int longMarkTop = top - 16;
	int shortMarkTop = top - 14;
	int markBottom = top - 10;

	int textWidth = 60;
	int textHeight = longMarkTop;
	
	painter.setPen(QPen(QColor(10, 10, 10), 1));

	if ((m_longMarkCount > 1) && (m_shotLongMarkRatio >= 1))
	{
		int unitCount = (m_longMarkCount - 1) * m_shotLongMarkRatio;
		for (int i = 0; i <= unitCount ; i++)
		{
			int x = left + (width() - left - right) * i / unitCount;
			if (( i % m_shotLongMarkRatio) == 0)
			{
				painter.drawLine(x, longMarkTop, x, markBottom);
				QString text =  m_textList[i / m_shotLongMarkRatio];
				painter.drawText(x - textWidth / 2, 0, textWidth, textHeight, Qt::AlignVCenter | Qt::AlignHCenter, text);
	//			painter.drawRect(x - textWidth / 2, 0, textWidth, textHeight);
			}
			else
			{
				painter.drawLine(x, shortMarkTop, x, markBottom);
			}
		}
	}

	QFont oldFont = painter.font();
	QFont newFont = oldFont;
	newFont.setBold(true);
	newFont.setPixelSize(12);

	for (int i = 0; i < m_extraLongMarkCount; i++)
	{
		int value = m_extraLongMark[i].m_value;
		if ((value >= minimum()) && (value <= maximum()))
		{
			int x = left + (width() - left - right) * (m_extraLongMark[i].m_value - minimum()) / (maximum() - minimum());
			painter.drawLine(x, longMarkTop, x, markBottom);
			if (m_extraLongMark[i].m_boldFlag == true)
			{
				painter.setFont(newFont);
			}
			painter.drawText(x - textWidth / 2, 0, textWidth, textHeight, Qt::AlignVCenter | Qt::AlignHCenter, m_extraLongMark[i].m_strValue);
			if (m_extraLongMark[i].m_boldFlag == true)
			{
				painter.setFont(oldFont);
			}
		}
	}


	for (int i = 0; i < m_extraShortMarkCount; i++)
	{
		int value = m_extraShortMark[i].m_value;
		if ((value >= minimum()) && (value <= maximum()))
		{
			int x = left + (width() - left - right) * (m_extraShortMark[i].m_value - minimum()) / (maximum() - minimum());
			painter.drawLine(x, shortMarkTop, x, markBottom);
			if (m_extraShortMark[i].m_boldFlag == true)
			{
				painter.setFont(newFont);			
			}
			painter.drawText(x - textWidth / 2, 0, textWidth, textHeight, Qt::AlignVCenter | Qt::AlignHCenter, m_extraShortMark[i].m_strValue);
			if (m_extraShortMark[i].m_boldFlag == true)
			{
				painter.setFont(oldFont);
			}
		}
	}
#endif	
}
