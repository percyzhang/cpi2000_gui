#include "VolumeGroup.h"
#include <QPainter>

VolumeGroup::VolumeGroup(QWidget *pParent)  : QWidget(pParent)
{
	m_pVolumeSlider = new ButtonSlider(this);
	m_pVolumeSlider->setTextColor(QColor(0x10, 0x10, 0x10));
	m_pVolumeSlider->setMarkColor(QColor(0x10, 0x10, 0x10));
//	m_pVolumeSlider->addExtraLongMark(70, " 7", true);
//	m_pVolumeSlider->addExtraShortMark(0, " 0", false);
//	m_pVolumeSlider->addExtraShortMark(100, " 10", false);
	QStringList sliderStringList;
	sliderStringList << "-16dB" << "0dB" << "16dB";
	m_pVolumeSlider->setMark(3, 4, sliderStringList);
	m_pVolumeSlider->setMarkLength(5, 7);
	m_pVolumeSlider->setMarkSide(true, true);
	m_pVolumeSlider->setRange(-160, 160);
	m_pVolumeSlider->setGrooveHeight(30);
	m_pVolumeSlider->setSingleStep(1);
	m_pVolumeSlider->setPageStep(1);
	m_pVolumeSlider->setValue(0);
	connect(m_pVolumeSlider, SIGNAL(valueChanged(int)), this, SLOT(onVolumeSliderChanged(int)));

	m_pMuteButton = new MarkButton(QString(""), this);
    m_pMuteButton->setObjectName(QStringLiteral("muteButton"));
	connect(m_pMuteButton, SIGNAL(clicked()), this, SLOT(onClickMuteButton()));
	m_pMuteButton->mark(false);

	m_pVolumeSpinBox = new QDoubleSpinBox(this);
	m_pVolumeSpinBox->setRange(-16.0, 16.0);
	m_pVolumeSpinBox->setSingleStep(0.1);
	m_pVolumeSpinBox->setDecimals(1);
	m_pVolumeSpinBox->setSuffix(" dB");
	connect(m_pVolumeSpinBox, SIGNAL(valueChanged(double)), this, SLOT(onVolumeSpinBoxChanged(double)));

	m_slotEnableFlag = true;
	m_volume = 7.0;
	refresh();
}

void VolumeGroup::resizeEvent(QResizeEvent * /*event*/)
{
	int xMargin = 30, yMargin = 20;

	m_pVolumeSlider->setGeometry(xMargin, yMargin, width() - xMargin * 2, height() - 2 * yMargin - 20);

	int spinBoxWidth = (width() - 80) / 3 + 40;
	m_pVolumeSpinBox->setGeometry((width() - spinBoxWidth) / 2 + width() / 10, height() - yMargin - 12, spinBoxWidth, 20); 

	m_pMuteButton->setGeometry((width() - spinBoxWidth) / 2 - width() / 10, height() - yMargin - 15, 26, 25); 
}

void VolumeGroup::paintEvent(QPaintEvent * /*event*/)
{
#if 0
	QPainter painter(this);
	painter.setPen(QPen(QColor(10, 10, 10), 2));

//	painter.drawRect(0, 0, width(), height());

	painter.drawRect(1, 1, width() - 2, height() - 2);
#endif
}

void VolumeGroup::onClickMuteButton()
{
	bool flag = !m_pMuteButton->getMarkFlag();
	m_pMuteButton->mark(flag);

	emit toggleMuteButton(flag);
}

void VolumeGroup::onVolumeSliderChanged(int i)
{
	if (m_slotEnableFlag == false)
	{
		return;
	}

	m_volume = float(i) / 10;
	refresh();

	emit changeVolume(m_volume);
}

void VolumeGroup::onVolumeSpinBoxChanged(double volume)
{
	if (m_slotEnableFlag == false)
	{
		return;
	}

	m_volume = volume;
	refresh();

	emit changeVolume(m_volume);
}

void VolumeGroup::refresh()
{
	bool oldFlag = m_slotEnableFlag; 
	m_slotEnableFlag = false;

	m_pVolumeSpinBox->setValue(m_volume);
	m_pVolumeSlider->setValue(qRound(m_volume * 10));

	m_slotEnableFlag = oldFlag;
}

void VolumeGroup::setVolume(float volume)
{
	m_volume = volume;
	refresh();
}

void VolumeGroup::setMuteFlag(bool flag)
{
	m_pMuteButton->mark(flag);
}