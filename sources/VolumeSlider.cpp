#include "VolumeSlider.h"

VolumeSlider::VolumeSlider(QWidget *pParent, Qt::Orientation orientation)
	: ButtonSlider(pParent, orientation)
{

}

void VolumeSlider::setVolumePreset(int value)
{
	m_extraLongMark[0].m_value = value;
	m_extraLongMark[0].m_strValue = QString::number(float(value) / 10, 'f', 1);
}
