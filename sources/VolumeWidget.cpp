#include "VolumeWidget.h"
#include <QStylePainter>

VolumeWidget::VolumeWidget(QWidget *parent)	: QWidget(parent)
{
	this->setObjectName(QStringLiteral("VolumeWidget"));

	for (int i = 0; i < 19; i++)
	{
		m_volumeValue[i] = 20 - 5 * i;
	}
}

void VolumeWidget::resizeEvent(QResizeEvent * /* event */)
{
	setOriginPosition(width() * 4 / 29, 9 * height() / 10);
	m_xAxialRight = width() * 27 / 29;
	m_yAxialTop = height() / 20;
}

void VolumeWidget::setOriginPosition(int x, int y)
{
	m_originXPosition = x;
	m_originYPositon = y;
}

void VolumeWidget::setXMarkString(QStringList &xStringList)
{
	m_xStringList = xStringList;
	m_volumeCount = xStringList.count();
}

void VolumeWidget::setYMarkString(QStringList &yStringList)
{
	m_yStringList = yStringList;
}

void VolumeWidget::setVolumeStep(int min, int max, int totalStep)
{
	m_minVolume = min;
	m_maxVolume = max;
	m_totalStep = totalStep;
}

void VolumeWidget::setVolume(int *aVolumeValue)
{
	int i;
	for (i = 0; i < m_volumeCount; i++)
	{
		m_volumeValue[i] = aVolumeValue[i];
	}
}

void VolumeWidget::updatePixmap()
{
	int margin = 0;
	int roundRadius = 18;
	int i;
	QColor backgroundColor(0X40, 0X40, 0X40);

	m_pixmap = QPixmap(size());
	QPainter painter(&m_pixmap);
	painter.initFrom(this);
	m_pixmap.fill(QColor(0x90, 0x90, 0x90));

	/* Draw RoundRect Background */
	painter.setPen(QPen(backgroundColor, 1));

	painter.setRenderHint(QPainter::Antialiasing);
	QPainterPath path;
	path.addRoundedRect(QRectF(margin, margin, width() - 2 * margin, height() - 2 * margin), roundRadius, roundRadius);

	painter.fillPath(path, backgroundColor);
	painter.drawPath(path);

	/* Draw Axial */
	painter.setPen(QPen(QColor(0xA0, 0xA0, 0xA0), 2));
	painter.drawLine(m_originXPosition, m_originYPositon, m_xAxialRight + 5, m_originYPositon);
	painter.drawLine(m_originXPosition, m_originYPositon, m_originXPosition, m_yAxialTop - 5);

	/* Draw Text */
	painter.setPen(QPen(QColor(0xe0, 0xe0, 0xe0), 1));
	int unitWidth = (m_xAxialRight - m_originXPosition) / m_volumeCount;
	for (i = 0; i < m_volumeCount; i++)
	{
		int left = i * unitWidth  + m_originXPosition;
		painter.drawText(QRectF(left, m_originYPositon, unitWidth, height() -  m_originYPositon), m_xStringList.at(i), QTextOption(Qt::AlignVCenter | Qt::AlignHCenter));
//		 void drawText(const QRectF &r, const QString &text, const QTextOption &o = QTextOption());
	}

	int yCount = m_yStringList.count();
	int unitHeight = (m_originYPositon - m_yAxialTop) / (yCount - 1);

	for (i = 0; i < yCount; i++)
	{
		int y = m_originYPositon - i * unitHeight;
		painter.drawText(QRect(0, y - 20, m_originXPosition, 40), m_yStringList.at(i), QTextOption(Qt::AlignVCenter | Qt::AlignHCenter));
	}
}

void VolumeWidget::drawVolume(QStylePainter &painter, int index, int volumeStep)
{
	int unitWidth = (m_xAxialRight - m_originXPosition - 3) / m_volumeCount;
	int margin = unitWidth / 8;
	int left = index * unitWidth  + m_originXPosition + 3 + margin;
	float stepHeigh = float(m_originYPositon - m_yAxialTop) / m_totalStep;
	int topY = m_originYPositon - qRound(stepHeigh * volumeStep);

	painter.fillRect(left, topY, unitWidth - 2 * margin, qRound(stepHeigh * volumeStep) - 1, QColor(0, 230, 0));

	int blankWidth = qRound(stepHeigh / 3);
	if (blankWidth == 0)
	{
		blankWidth = 1;
	}

	for (int step = 0; step < volumeStep; step++)
	{
		int topY = m_originYPositon - qRound(stepHeigh * step) - blankWidth - 1;
		painter.fillRect(left, topY, unitWidth - 2 * margin, blankWidth, QColor(0x40, 0x40, 0x40));
	}
}

void VolumeWidget::refreshValue()
{
	m_updateType = 1;
	QWidget::update();
}

void VolumeWidget::paintEvent(QPaintEvent * /*event*/)
{
	int i;
	if (m_updateType == 0)
	{
		/* Draw Pixal */
		updatePixmap();
	}

	QStylePainter painter(this);
	painter.drawPixmap(0, 0, m_pixmap);

	float unitStep = (m_maxVolume - m_minVolume) / m_totalStep;
	for (i = 0; i < m_volumeCount; i++)
	{
		int step = qRound((qreal)(m_volumeValue[i] - m_minVolume) / unitStep);
		drawVolume(painter, i, step);
	}	

	m_updateType = 0;
}

