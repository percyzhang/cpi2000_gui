#include "conv.h"
#include <math.h>

conv::conv(void)
{
}


conv::~conv(void)
{
}


void conv::convolution(double *out,double *coef1,int N1,double *coef2, int N2){


	for (int n = 0; n < N1 + N2 - 1; n++){
		int kmin, kmax, k;

		out[n] = 0;

		kmin = (n >= N2 - 1) ? n - (N2 - 1) : 0;
		kmax = (n < N1 - 1) ? n : N1 - 1;

		for (k = kmin; k <= kmax; k++)
			out[n] += coef1[k] * coef2[n - k];
		
	}



}