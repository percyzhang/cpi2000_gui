#include "MainWidget.h"
#include "MainApplication.h"
#include <QSharedMemory>
#include <QMessageBox>
#include "PresetsDB.h"
#include <QTextStream>
#include <QDir>
#include <QFile>
#include <QScreen>
#include <QFontDatabase>
#include "IniSetting.h"
#include "simpleQtLogger.h"

MainApplication *g_pApp = nullptr;
IniSetting *g_pIniSetting;

QTextStream g_log;
QFile g_logFile;
int g_defaultFontSize = 8;


int main(int argc, char *argv[])
{
	QString iniFileName = QString("%1/cpi2000.ini").arg(QDir::tempPath());
	qDebug() << iniFileName;
	g_pIniSetting = new IniSetting(iniFileName);
	g_pIniSetting->readAll();

	if (g_pIniSetting->m_scaleFlag == true)
	{
		qputenv("QT_AUTO_SCREEN_SCALE_FACTOR", "1");

		QString scaleFactorString =  QString::number(g_pIniSetting->m_scaleFactor, 'f', 1);
		qputenv("QT_SCALE_FACTOR", scaleFactorString.toLatin1());
		QGuiApplication::setAttribute(Qt::AA_EnableHighDpiScaling); 
	}
	else
	{
		QGuiApplication::setAttribute(Qt::AA_DisableHighDpiScaling); 
	}

	MainApplication app(argc, argv);
	app.installEventFilter(new mouseEventFilter());
	logInit(QDir::tempPath());

	/* We just need only one instance */
    QSharedMemory shared("62d60669-bba4-4a94-88bb-b964893a7e85");
	if( !shared.create( 512, QSharedMemory::ReadWrite) )
    {
//		qWarning() << "Can't start more than one instance of the application.";


		QMessageBox::warning(nullptr, QObject::tr("Warning"), QObject::tr("Another CPi2000 Application is already running now. You can't start more than one instance of the application!"), 
			QObject::tr("OK"));
		exit(0);
	}
	g_pApp = &app;

    QFile qss(":cpi2000.qss");
    qss.open(QFile::ReadOnly);
    app.setStyleSheet(qss.readAll());
    qss.close();

	g_database.init();

	/* Set the default font, so we can use it in all kinds of windows version */
	{
		QFont font("Tahoma");
//		int fontId = QFontDatabase::addApplicationFont(":/binaries/msyh.ttf"); 
//		QString msyh = QFontDatabase::applicationFontFamilies ( fontId ).at(0); 
//		QFont font(msyh); 

		QFontDatabase database;
		foreach(const QString &family, database.families(QFontDatabase::SimplifiedChinese))
		{
			qDebug() << family;
		}

		QScreen *pScreen = g_pApp->primaryScreen ();
		float currentDPI = pScreen->logicalDotsPerInch();
		int pointSize = qRound((float)8 * 96 / currentDPI);
		g_defaultFontSize = pointSize;
		font.setPointSize(pointSize);
		
//		QMessageBox::information(nullptr, "Information", QString("pointSize = %1, logicalDotsPerInch = %2, scaleFlag = %3, scaleFactor = %4").arg(pointSize).arg(currentDPI).arg(g_pIniSetting->m_scaleFlag).arg(g_pIniSetting->m_scaleFactor), "OK");
		app.setFont(font);
	}

    MainWidget w;
	g_pMainWidget = &w;
    w.show();

#if 1
	QStringList fileList;
//	fileList << "myFile1.txt" << "myFile2.txt";
//	g_pApp->zipFilesToFile("myfile.zip", fileList);
//	g_pApp->unzipFileToFolder("test.dev", "myFolder");
#endif

#ifdef LOG_FILE
	QString logFileName	= QString("%1/CPi2000Log.txt").arg(QDir::tempPath());
	g_logFile.setFileName(logFileName);
	qDebug() << "Set log file to: " << logFileName;
	g_logFile.open(QIODevice::WriteOnly);
	g_log.setDevice(&g_logFile);
#endif

    return app.exec();
}
