#include "MainApplication.h"
#include <QTime>
#include <QNetworkConfigurationManager>
#include <qnetworkaccessmanager.h>
#include <QUrl>
#include <QNetworkRequest>
#include <QDirIterator>
#include <QMessageBox.h>
#include "mainWidget.h"
#include "simpleQtLogger.h"

MainApplication::MainApplication(int argc, char *argv[])
	: QApplication(argc, argv)
{
	m_deviceConnecteStatus = DEVICE_DISCONNECTED;
	m_pDeviceSocket = nullptr;
}

MainApplication::~MainApplication()
{
	if (m_pDeviceSocket != nullptr)
	{
		delete m_pDeviceSocket;
		m_pDeviceSocket = nullptr;
	}
}


void MainApplication::msleep(int ms)
{
    QTime dieTime = QTime::currentTime().addMSecs(ms);

	for ( ; QTime::currentTime() <= dieTime; )
	{
		QCoreApplication::processEvents( QEventLoop::AllEvents);
	}
}

void MainApplication::setConnectStatus(DEVICE_CONNECT_STATUS status) 
{ 
	m_deviceConnecteStatus = status;
}

DeviceSocket *MainApplication::getDeviceConnection()
{
	if (m_deviceConnecteStatus != DEVICE_NETWORK_CONNECTED)
	{
		return nullptr;
	}
	else
	{
		return m_pDeviceSocket;
	}
}

bool MainApplication::connectDevice(QHostAddress localAddress, QHostAddress peerAddress)
{
	if (m_pDeviceSocket != nullptr)
	{
		delete m_pDeviceSocket;
		m_pDeviceSocket = nullptr;
	}
	setConnectStatus(DEVICE_DISCONNECTED);

	L_INFO("Connecting to Device: " + peerAddress.toString());
	m_pDeviceSocket = new DeviceSocket(localAddress, peerAddress);
	
	bool ret = m_pDeviceSocket->setConnection();
	if (ret == true)
	{
		setConnectStatus(DEVICE_NETWORK_CONNECTED);
		L_INFO("socket connected");
		emit deviceConnected();
	}
	else
	{
		L_ERROR("socket connect failed");
		delete m_pDeviceSocket;
		m_pDeviceSocket = nullptr;
	}
	return (ret);
}

void MainApplication::disconnectDevice() 
{
	if (m_pDeviceSocket != nullptr)
	{
		delete m_pDeviceSocket;
		m_pDeviceSocket = nullptr;
	}

	if (m_deviceConnecteStatus == DEVICE_NETWORK_CONNECTED)
	{
		setConnectStatus(DEVICE_DISCONNECTED);
		emit deviceDisconnected();
	}
}

bool MainApplication::putFtpFile(QString localFileName, QString remoteFileName)
{
	if (getConnectStatus() != DEVICE_NETWORK_CONNECTED)
	{
		qDebug() << "putFtpFile failed for not connected with device!";
		return (false);
	}

	QFile localFile(localFileName);
    if (localFile.open(QIODevice::ReadOnly) == false)
	{
		qDebug() << "putFtpFile failed for open local file failed :" << localFileName;
		return (false);
	}

    QByteArray data = localFile.readAll();
    localFile.close(); 

    QNetworkAccessManager networkManager(this);
    QUrl url(remoteFileName);
    url.setUserName("cpi2000");
    url.setPassword("ftpftp");
    QNetworkRequest req(url);


	connect(&networkManager, SIGNAL(finished(QNetworkReply*)),this,SLOT(uploadFinished(QNetworkReply*)));
    networkManager.put(QNetworkRequest(url), data);

	m_ftpFinished = false; 
	m_ftpSuccess = false;
	m_ftpData.clear();
	m_ftpErrorInfo = QString("Timeout");
	for (int i = 0; i < 6000; i++)
	{
		if (m_ftpFinished == true)
		{
			break;
		}
		msleep(10);
	}
	if (m_ftpSuccess == false)
	{
		L_WARN(QString("Failed to put file:%1. error info:%2").arg(remoteFileName).arg(m_ftpErrorInfo)); 
	}
	else
	{
		L_INFO(QString("Put file:%1 to device successfully!").arg(remoteFileName));
	}

	return (m_ftpSuccess);

}

#if 0
bool MainApplication::putFtpFile(QString remoteFileName, QString localFileName)
{
	if (getConnectStatus() != DEVICE_NETWORK_CONNECTED)
	{
		qDebug() << "getFtpFile failed for not connected with device!";
		return (false);
	}
	QFile file(localFileName);
	if (file.open(QIODevice::ReadOnly) == false)
	{
		QMessageBox::warning(g_pMainWidget, tr("Read file error"), tr("Can't read the file:%1").arg(localFileName), QMessageBox::Ok);
		return (false);
	}
		
	QByteArray data = file.readAll();
	file.close();

    QNetworkAccessManager *pManager = new QNetworkAccessManager(this);
	QString fileName = QString("ftp://%1/%2").arg(m_pDeviceSocket->getDeviceAddr().toString()).arg(remoteFileName);
	qDebug() << fileName;

    QUrl url(fileName);
    url.setUserName("cpi2000");
    url.setPassword("ftpftp");
    QNetworkRequest req(url);

	m_ftpFinished = false; 
	m_ftpSuccess = false;
	m_ftpData.clear();
	m_ftpErrorInfo = QString("Timeout");

    pManager->get(req);

	for (int i = 0; i < 500; i++)
	{
		if (m_ftpFinished == true)
		{
			break;
		}
		msleep(10);
	}
	if (m_ftpSuccess == false)
	{
		qDebug() << "Download file: " << fileName << " Failed: " << m_ftpErrorInfo;
	}
	else
	{
		qDebug() << "Download file: " << fileName << " Successfully";
		QFile file(localFileName);
		file.open(QIODevice::WriteOnly);
		file.write(m_ftpData);
		file.close();
	}

	return (m_ftpSuccess);
}
#endif

bool MainApplication::getFtpFile(QString remoteFileName, QString localFileName)
{
	if (getConnectStatus() != DEVICE_NETWORK_CONNECTED)
	{
		qDebug() << "getFtpFile failed for not connected with device!";
		return (false);
	}

    QNetworkAccessManager networkManager(this);
	QString fileName = QString("ftp://%1/%2").arg(m_pDeviceSocket->getDeviceAddr().toString()).arg(remoteFileName);
	qDebug() << fileName;

    QUrl url(fileName);
    url.setUserName("cpi2000");
    url.setPassword("ftpftp");
    QNetworkRequest req(url);

	m_ftpFinished = false; 
	m_ftpSuccess = false;
	m_ftpData.clear();
	m_ftpErrorInfo = QString("Timeout");

	connect(&networkManager, SIGNAL(finished(QNetworkReply*)),this,SLOT(downloadFinished(QNetworkReply*)));
    networkManager.get(req);

	for (int i = 0; i < 6000; i++)
	{
		if (m_ftpFinished == true)
		{
			break;
		}
		msleep(10);
	}
	if (m_ftpSuccess == false)
	{
		qDebug() << "Download file: " << fileName << " Failed: " << m_ftpErrorInfo;
	}
	else
	{
		qDebug() << "Download file: " << fileName << " Successfully";
		QFile file(localFileName);
		file.open(QIODevice::WriteOnly);
		file.write(m_ftpData);
		file.close();
	}

	return (m_ftpSuccess);
}

void MainApplication::downloadFinished(QNetworkReply* reply)
{
	m_ftpFinished = true;

	if (reply->error() == QNetworkReply::NoError)
	{
		m_ftpSuccess = true;
		m_ftpData = reply->readAll();
	}
	else
	{
		m_ftpSuccess = false;
	}

	m_ftpErrorInfo = reply->errorString();
}


void MainApplication::uploadFinished(QNetworkReply* reply)
{
	m_ftpFinished = true;

	if (reply->error() == QNetworkReply::NoError)
	{
		m_ftpSuccess = true;
	}
	else
	{
		m_ftpSuccess = false;
	}
	m_ftpErrorInfo = reply->errorString();
}

bool MainApplication::unzipFileToFolder(QString zipFileName, QString folder)
{
	QZipReader zip(zipFileName);
	if (zip.exists())
	{
		bool ret = true;
		if (zip.extractAll(folder) == false)
		{
			qDebug() << "extract failed!" << zip.status();
			ret = false;
		}
		else
		{
			qDebug() << "extract success!";
		}

		zip.close();
		return (ret);
	}
	return (false);
}

bool MainApplication::zipFolderToFile(QString zipFileName, QString path)
{
    QZipWriter zip(zipFileName);
    if (zip.status() != QZipWriter::NoError)
	{
        return (false);
	}

    zip.setCompressionPolicy(QZipWriter::AutoCompress);
 
    QDirIterator it(path, QDir::Files | QDir::Dirs | QDir::NoDotAndDotDot,
                        QDirIterator::Subdirectories);
    while (it.hasNext()) 
	{
        QString file_path = it.next();
        if (it.fileInfo().isDir()) 
		{
            zip.setCreationPermissions(QFile::permissions(file_path));
            zip.addDirectory(file_path.remove(path));
        } 
		else if (it.fileInfo().isFile()) 
		{
            QFile file(file_path);
            if (!file.open(QIODevice::ReadOnly))
                continue;
 
            zip.setCreationPermissions(QFile::permissions(file_path));
            QByteArray ba = file.readAll();
            zip.addFile(file_path.remove(path), ba);
        }
    }
        
	zip.close();
 
	return (true);
}

bool MainApplication::zipFilesToFile(QString zipFileName, QStringList fileNameList)
{
    QZipWriter zip(zipFileName);
    if (zip.status() != QZipWriter::NoError)
	{
        return (false);
	}

    zip.setCompressionPolicy(QZipWriter::AutoCompress);
 
    for (int i = 0; i < fileNameList.size(); ++i)
    {
		QFileInfo fileInfo(fileNameList.at(i));
		if (fileInfo.isFile() != true)
		{
			continue;
		}

		QString fileName = fileInfo.fileName();
		QString filePath = fileInfo.filePath();
        QFile file(filePath);
        if (!file.open(QIODevice::ReadOnly))
            continue;

        zip.setCreationPermissions(QFile::permissions(filePath));
        QByteArray ba = file.readAll();
        zip.addFile(fileName, ba);
    }
        
	zip.close();
	return (true);
}

void MainApplication::clearLog()
{
	if (m_deviceConnecteStatus != DEVICE_NETWORK_CONNECTED)
	{
		return;
	}

	DeviceSocket *m_pDeviceSocket = getDeviceConnection();

	/* First, we need to stop the log */
	m_pDeviceSocket->clearLog();

	LogManager *pLogManager = getLogManager();
	pLogManager->clearAll();
}

void MainApplication::getNewLog()
{
	if (m_deviceConnecteStatus != DEVICE_NETWORK_CONNECTED)
	{
		return;
	}

	LogManager *pLogManager = getLogManager();
	pLogManager->clearAll();

	DeviceSocket *m_pDeviceSocket = getDeviceConnection();

	/* First, we need to stop the log */
	m_pDeviceSocket->setLogOff();

	/* Then we can get the log file */
	m_pDeviceSocket->saveLogFile();

	QString hallName;
	m_pDeviceSocket->getHallName(hallName, 5);

	/* ftp log file: mylogfile and mylogfile.1 (maybe not exist) */
	QString fileName1 = "mylogfile";
	QString localLogFile1 = QString("%1/%2").arg(QDir::tempPath()).arg(fileName1);
	QStringList fileNameList;
	if (g_pApp->getFtpFile(fileName1, localLogFile1) == false)
	{
		QMessageBox::warning(g_pMainWidget, tr("Warning"), tr("Failed to get the log file: %1").arg(fileName1), QMessageBox::Ok); 
	}
	else
	{
		/* Now we will try to get another log file: mylogfile.1 */
		QString fileName2 = "mylogfile.1";
		QString localLogFile2 = QString("%1/%2").arg(QDir::tempPath()).arg(fileName2);
		if (g_pApp->getFtpFile(fileName2, localLogFile2) == true)
		{
			fileNameList << localLogFile2 << localLogFile1;
		}
		else
		{
			fileNameList << localLogFile1;
		}
	}

	for (int i = 0; i < fileNameList.length(); i++)
	{
		QString fileName = fileNameList.at(i);
		pLogManager->addLogFile(fileName);
	}

	/* At last, , we need to continue the log */
	m_pDeviceSocket->setLogOn();
}

QString MainApplication::getTempSpeakerDBFileName()
{
	QString tempDir = QDir::tempPath();
	QString speakerDBFileName = QString("%1/speaker.db").arg(tempDir);
	return (speakerDBFileName);
}

QString MainApplication::getTempPresetDBFileName()
{
	QString tempDir = QDir::tempPath();
	QString presetDBFileName = QString("%1/Presets.db").arg(tempDir);
	return (presetDBFileName);
}

QString MainApplication::getTempDevFileName()
{
	QString tempDir = QDir::tempPath();
	QString presetDBFileName = QString("%1/defaultDevice.dev").arg(tempDir);
	return (presetDBFileName);
}