#include "mainwidget.h"
#include "mainApplication.h"
#include <QMessageBox>
#include <QPainter>
#include "deviceSearchingDialog.h"
#include "commonLib.h"
#include <QFileDialog>
#include "BackgroundWidget.h"
#include "UpgradeDialog.h"
#include "PresetsDB.h"
#include "USBFileDialog.h"
#include "SpeakerListDialog.h"
#include "SpeakerEditor.h"
#include "SpeakerData.h"
#include "SpeakerImportDialog.h"
#include "ZoomDlg.h"
#include "simpleQtLogger.h"

//#define PRESET_DB "/cdi3/Software/Firmware/Model/Build/embBuild/Model/release/Presets.db"

#define MIN_SCROLL_WIDTH    1170
#define MIN_SCROLL_HEIGHT	645
MainWidget *g_pMainWidget = nullptr;

MainWidget::MainWidget(QWidget *parent)
    : QWidget(parent)
{
	m_recvCount = 0;
	m_debugActive = false;

	this->setObjectName(QStringLiteral("mainWidget"));

	m_pTopFrame = new QFrame(this);
	m_pTopFrame->setObjectName(QStringLiteral("topFrame"));

	/* Create the Menu Bar and the Menu */
	createMenuBar();

	m_pBottomFrame = new QFrame(this);
	m_pBottomFrame->setObjectName(QStringLiteral("bottomFrame"));

	m_pOverviewButton = new QPushButton(tr("OverView"), this);
    m_pOverviewButton->setObjectName(QStringLiteral("tabButton"));
	connect(m_pOverviewButton, SIGNAL(clicked()), this, SLOT(onOverViewClicked()));	
	m_pOverviewButton->setCheckable(true);

	m_pBasicSettingButton = new QPushButton(tr("Basic Setting"), this);
    m_pBasicSettingButton->setObjectName(QStringLiteral("tabButton"));
	connect(m_pBasicSettingButton, SIGNAL(clicked()), this, SLOT(onBasicSettingClicked()));		
	m_pBasicSettingButton->setCheckable(true);

	m_pInputSettingButton = new QPushButton(tr("Input Setting"), this);
    m_pInputSettingButton->setObjectName(QStringLiteral("tabButton"));
	connect(m_pInputSettingButton, SIGNAL(clicked()), this, SLOT(onInputSettingClicked()));		
	m_pInputSettingButton->setCheckable(true);

	m_pOutputSettingButton = new QPushButton(tr("Output Setting"), this);
    m_pOutputSettingButton->setObjectName(QStringLiteral("tabButton"));
	connect(m_pOutputSettingButton, SIGNAL(clicked()), this, SLOT(onOutputSettingClicked()));
	m_pOutputSettingButton->setCheckable(true);

	m_pLogButton = new QPushButton(tr("Log Page"), this);
    m_pLogButton->setObjectName(QStringLiteral("tabButton"));
	connect(m_pLogButton, SIGNAL(clicked()), this, SLOT(onLogClicked()));
	m_pLogButton->setCheckable(true);


	m_pTranslator = new QTranslator;

	m_pEnglishButton = new QPushButton(m_pTopFrame);
    m_pEnglishButton->setObjectName(QStringLiteral("englishButton"));
	connect(m_pEnglishButton, SIGNAL(clicked()), this, SLOT(onClickEnglishButton()));
	m_pEnglishButton->setCheckable(true);
	m_pEnglishButton->setChecked(true);

	m_pChineseButton = new QPushButton(m_pTopFrame);
    m_pChineseButton->setObjectName(QStringLiteral("chineseButton"));
	connect(m_pChineseButton, SIGNAL(clicked()), this, SLOT(onClickChineseButton()));
	m_pChineseButton->setCheckable(true);
	m_pChineseButton->setChecked(false);

	m_pLanguageButtonGroup = new QButtonGroup(m_pTopFrame);
	m_pLanguageButtonGroup->addButton(m_pEnglishButton);
	m_pLanguageButtonGroup->addButton(m_pChineseButton);

	m_pLine = new QFrame(m_pTopFrame);
    m_pLine->setObjectName(QStringLiteral("lineFrame"));
    m_pLine->setFrameShape(QFrame::VLine); 

	m_pConnectButton = new MarkButton(tr("Offline"), this);
    m_pConnectButton->setObjectName(QStringLiteral("connectButton"));
	connect(m_pConnectButton, SIGNAL(clicked()), this, SLOT(onClickConnectButton()));
	m_pConnectButton->mark(false);

	/* Timer */
	m_pTimer1S = new QTimer(this);
	m_pTimer1S->start(1000);
	connect(m_pTimer1S, SIGNAL(timeout()), this, SLOT(on1STimerOut()));

	/* We set the tab  */
	m_currentTabIndex = -1;

    setWindowTitle(tr("CPi2000 - Cinema processor"));
    setMinimumSize(800, 600);
    resize(1200, 800);

	m_pTabFrame = new QFrame(this);
	m_pTabFrame->setObjectName(QStringLiteral("m_pTabFrame"));
	m_pTabFrame->setMinimumSize(MIN_SCROLL_WIDTH, MIN_SCROLL_HEIGHT);
	m_pTabFrame->setSizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::MinimumExpanding);

	m_pScrollArea = new QScrollArea(this);
	m_pScrollArea->setWidget(m_pTabFrame);
	m_pScrollArea->setWidgetResizable(true);
	m_pScrollArea->setObjectName(QStringLiteral("ScrollArea"));

	m_pOverviewWidget = new OverviewWidget(m_pTabFrame);
	m_pBasicSettingWidget = new BasicSettingWidget(m_pTabFrame);
	m_pInputSettingWidget = new InputSettingWidget(m_pTabFrame);
	m_pOutputSettingWidget = new OutputSettingWidget(m_pTabFrame);
	m_pLogWidget = new LogWidget(m_pTabFrame);

	connect(g_pApp, SIGNAL(deviceConnected()), this, SLOT(onDeviceConnected()));
	connect(g_pApp, SIGNAL(deviceDisconnected()), this, SLOT(onDeviceDisconnected()));


//	m_pDeviceImageWidget = new BackgroundWidget(":deviceOverview.png", this);
	m_pVolumeChartWidget = new VolumeChartWidget(m_pBottomFrame);
	QStringList outputVolumeNameList;
	outputVolumeNameList << "L" << "R" << "C" << "Sw" << "Ls" << "Rs" << "Bls" << "Brs";
	m_pVolumeChartWidget->setVolumeData(getCPi2000Data()->getOutputDB(), outputVolumeNameList, 8); 
	connect(g_pApp, SIGNAL(outputVolumeChanged()), m_pVolumeChartWidget, SLOT(onOutputVolumeChanged()));

	m_pOutputLevelLabel = new QLabel(tr("Output Level"), m_pBottomFrame);
	m_pOutputLevelLabel->setAlignment(Qt::AlignRight);
	m_pOutputLevelLabel->setObjectName(QStringLiteral("m_pOutputLevelLabel"));
//	m_pVolumeChartWidget = new BackgroundWidget(":logo.png", m_pBottomFrame, 0, 0);

	m_pLogoWidget = new BackgroundWidget(":logo.png", m_pBottomFrame, 0, 0);

	m_pPanelWidget = new PanelWidget((QWidget *)QApplication::desktop());
//	m_pPanelWidget->setModal(false);
	m_pPanelWidget->hide();

	g_pBroadcastCommunication = new BroadcastCommunication();
//	g_pBroadcastCommunication->createSocket(this, BROADCAST_LOCAL_PORT, BROADCAST_PEER_PORT);

	m_pEnglishButton->emit clicked();
	m_pOverviewButton->emit clicked();

	refreshConnectedButton();
}

void MainWidget::onOverViewClicked()
{
	if (m_currentTabIndex != 0)
	{
		onTabChanged(0);
	}
	else
	{
		m_pOverviewButton->setChecked(true);
	}
}

void MainWidget::refreshOutputVolume()
{
	m_pVolumeChartWidget->refresh();
}

void MainWidget::onBasicSettingClicked()
{
	if (m_currentTabIndex != 1)
	{
		onTabChanged(1);
	}
	else
	{
		m_pBasicSettingButton->setChecked(true);
	}
}

void MainWidget::onInputSettingClicked()
{
	if (m_currentTabIndex != 2)
	{
		onTabChanged(2);
	}
	else
	{
		m_pInputSettingButton->setChecked(true);
	}
}

void MainWidget::onOutputSettingClicked()
{
	if (m_currentTabIndex != 3)
	{
		onTabChanged(3);
	}
	else
	{
		m_pOutputSettingButton->setChecked(true);
	}
}

void MainWidget::onLogClicked()
{
	if (m_currentTabIndex != 4)
	{
		onTabChanged(4);
	}
	else
	{
		m_pLogButton->setChecked(true);
	}
}

void MainWidget::onTabChanged(int nIndex)
{
	int oldTabIndex = m_currentTabIndex;

	/* When we leave the Overview page, we should remind the user to Apply the Changes */
	if (oldTabIndex == 0) 
	{
		if (m_pOverviewWidget->isAnythingChanged() == true)
		{
			if (m_pOverviewWidget->isApplyEnabled() == true)
			{
				int ret = QMessageBox::question(this, tr("Warning"), tr("You just did some changes on this page. Do you want to apply these changes to the device before you leave this page?"), tr("Apply"), tr("Discard"), tr("Cancel"));
				if (ret == 0)
				{
					// Apply the changes.
					qDebug() << "Apply the changes";
					m_pOverviewWidget->onClickApplyButton();
				}
				else if (ret == 1)
				{
					// Discard the changes, and do nothing;
					qDebug() << "Discard the changeds";
				}
				else
				{
					// we will stay at this page (Overview).
					m_pBasicSettingButton->setChecked(false);
					m_pInputSettingButton->setChecked(false);
					m_pOutputSettingButton->setChecked(false);
					m_pLogButton->setChecked(false);
					return;				
				}
			}
			else
			{
				int ret = QMessageBox::question(this, tr("Warning"), tr("You are about to leave this page. Do you want to discard the changes?"), tr("Discard"), tr("Cancel"));
				if (ret == 0)
				{
					// Discard the changes, and do nothing;
				}
				else if (ret == 1)
				{
					// we will stay at this page (Overview).
					m_pBasicSettingButton->setChecked(false);
					m_pInputSettingButton->setChecked(false);
					m_pOutputSettingButton->setChecked(false);
					m_pLogButton->setChecked(false);
					return;
				}
			}
		}
	}

	m_currentTabIndex = nIndex;

	if (m_currentTabIndex != 3) 
	{
		CPi2000Data *pData = getCPi2000Data();
		RTAData *pRTA = pData->getRTA();
		SIGNAL_MODE oldMode = pRTA->getSignalMode();
		if (oldMode != SIGNAL_OFF)
		{
			/* we need to set SIGNAL mode to SIGNAL_OFF now  */
			emit g_pApp->setSignalMode((int)SIGNAL_OFF);
		}
	}

	m_pOverviewButton->setChecked(m_currentTabIndex == 0);
	if (m_currentTabIndex == 0)
	{
		m_pOverviewWidget->show();
		m_pOverviewWidget->refresh();
	}
	else
	{
		m_pOverviewWidget->hide();
	}

	m_pBasicSettingButton->setChecked(m_currentTabIndex == 1);
	if (m_currentTabIndex == 1)
	{
		m_pBasicSettingWidget->show();
		m_pBasicSettingWidget->refresh();
	}
	else
	{
		m_pBasicSettingWidget->hide();
	}

	m_pInputSettingButton->setChecked(m_currentTabIndex == 2);
	if (m_currentTabIndex == 2)
	{
		m_pInputSettingWidget->show();
		m_pInputSettingWidget->refresh();
	}
	else
	{
		m_pInputSettingWidget->hide();
	}

	m_pOutputSettingButton->setChecked(m_currentTabIndex == 3);
	if (m_currentTabIndex == 3)
	{
		m_pOutputSettingWidget->show();
		m_pOutputSettingWidget->refresh();
	}
	else
	{
		m_pOutputSettingWidget->hide();
	}

	m_pLogButton->setChecked(m_currentTabIndex == 4);
	if (m_currentTabIndex == 4)
	{
		m_pLogWidget->show();
	}
	else
	{
		m_pLogWidget->hide();
	}

	/* SUB Input Volume */
	if (g_pApp->getConnectStatus() == DEVICE_NETWORK_CONNECTED)
	{
		if (m_currentTabIndex == 2)
		{
			g_pApp->getDeviceConnection()->subInputVolume();
		}
		else
		{
			g_pApp->getDeviceConnection()->unsubInputVolume();		
		}
		
	}

	if ((oldTabIndex == 3) || (m_currentTabIndex == 3))
	{
		sendResizeEvent();
	}
}

MainWidget::~MainWidget()
{
	delete g_pBroadcastCommunication;
}

void MainWidget::sendResizeEvent()
{
	int w = g_pMainWidget->width();
	int h = g_pMainWidget->height();

	QResizeEvent *event = new QResizeEvent(QSize(w, h), QSize(w, h));
	QCoreApplication::postEvent(this, event);	
}

void MainWidget::resizeEvent(QResizeEvent * /* event */)
{
	int topFrameHeight = 24;
	int bottomFrameHeight = 90;

//	qDebug() << "Refreshed...";

	bool bottomWidgetFlag = true;
#if 0
	if ((m_currentTabIndex == 3) && (m_pOutputSettingWidget->getCurrentTabIndex() == 1))
	{
		bottomWidgetFlag = false;
	}
#endif

	m_pMenuBar->setGeometry(0, 2, width() / 2, topFrameHeight);
    m_pTopFrame->setGeometry(0, 0, width(), topFrameHeight);

	m_pChineseButton->setGeometry(width() - 150, 0, 60, topFrameHeight);
	m_pEnglishButton->setGeometry(width() - 80, 0, 60, topFrameHeight);
	m_pLine->setGeometry(width() - 85, 0, 1, topFrameHeight);

	m_pConnectButton->setGeometry(width() - 85, topFrameHeight + 4, 75, 25);

	int topMargin = 12;
	int tabButtonHeight = 25;
	int tabWidth = width() - 20;
	int tabHeight = height() - (topFrameHeight + topMargin) - bottomFrameHeight - tabButtonHeight;
//	m_pBottomWidget->setGeometry(0, width() - bottomWidgetHeight, width(), bottomWidgetHeight); 
	if (bottomWidgetFlag == false)
	{
		tabHeight = height() - (topFrameHeight + topMargin) - tabButtonHeight;
	}
//	m_pTabFrame->setGeometry(10, topFrameHeight + topMargin + tabButtonHeight, tabWidth, tabHeight); 
//	m_pTabScroller->setGeometry(10, topFrameHeight + topMargin + tabButtonHeight, tabWidth, tabHeight); 
	m_pScrollArea->setGeometry(10, topFrameHeight + topMargin + tabButtonHeight, tabWidth, tabHeight); 

	if (tabWidth < MIN_SCROLL_WIDTH)
	{
		tabWidth = MIN_SCROLL_WIDTH;
	}
	if (tabHeight < MIN_SCROLL_HEIGHT)
	{
		tabHeight = MIN_SCROLL_HEIGHT;
	}

	{
		int borderWidth = 6;
		m_pOverviewWidget->setGeometry(borderWidth, borderWidth, tabWidth - 2 * borderWidth, tabHeight - 2 * borderWidth);
		m_pBasicSettingWidget->setGeometry(borderWidth, borderWidth, tabWidth - 2 * borderWidth, tabHeight - 2 * borderWidth);
		m_pInputSettingWidget->setGeometry(borderWidth, borderWidth, tabWidth - 2 * borderWidth, tabHeight - 2 * borderWidth);
		m_pOutputSettingWidget->setGeometry(borderWidth, borderWidth, tabWidth - 2 * borderWidth, tabHeight - 2 * borderWidth);
		m_pLogWidget->setGeometry(borderWidth, borderWidth, tabWidth - 2 * borderWidth, tabHeight - 2 * borderWidth);
	}

	int tabButtonX = 15;
	int tabButtonY = topFrameHeight + topMargin;
	int tabButtonWidth = 120;
	m_pOverviewButton->setGeometry(tabButtonX, tabButtonY, tabButtonWidth, tabButtonHeight);
	m_pBasicSettingButton->setGeometry(tabButtonX + tabButtonWidth, tabButtonY, tabButtonWidth, tabButtonHeight);
	m_pInputSettingButton->setGeometry(tabButtonX + tabButtonWidth * 2, tabButtonY, tabButtonWidth, tabButtonHeight);
	m_pOutputSettingButton->setGeometry(tabButtonX + tabButtonWidth * 3, tabButtonY, tabButtonWidth, tabButtonHeight);
	m_pLogButton->setGeometry(tabButtonX + tabButtonWidth * 4, tabButtonY, tabButtonWidth, tabButtonHeight);

	if (bottomWidgetFlag == true)
	{
		m_pBottomFrame->show();
		m_pVolumeChartWidget->show();
		m_pOutputLevelLabel->show();
		m_pLogoWidget->show();
	}
	else
	{
		m_pBottomFrame->hide();
		m_pVolumeChartWidget->hide();
		m_pOutputLevelLabel->hide();
		m_pLogoWidget->hide();
	}

	m_pBottomFrame->setGeometry(0, height() - bottomFrameHeight, width(), bottomFrameHeight);
//	int volumeChartWidth = 294;
//	int dolbyWidth = 520;
//	int imageHeight = 209;
//	int margin = (width() - volumeChartWidth - dolbyWidth) / 3;
	m_pVolumeChartWidget->setGeometry(width() / 8, bottomFrameHeight / 17, width() / 3, bottomFrameHeight * 15 / 17 - 2);
	m_pOutputLevelLabel->setGeometry(width() / 8 - 150, bottomFrameHeight * 3 / 4, 130, 30);
	m_pLogoWidget->setGeometry(width() / 17 * 16 - 40, bottomFrameHeight - 80, 70, 70);
}

void MainWidget::onClickChineseButton()
{
	m_pConnectButton->setEnabled(true);
    g_pApp->removeTranslator(m_pTranslator);
    QString qmFileName = ":/cpi2000_zh.qm";
	if (m_pTranslator->load(qmFileName))
    {
        qApp->installTranslator(m_pTranslator);
		retranslateUi();
    }
}

void MainWidget::onClickEnglishButton()
{
//	m_pConnectButton->setEnabled(false);
    g_pApp->removeTranslator(m_pTranslator);
    QString qmFileName = ":/cpi2000_en.qm";
	if (m_pTranslator->load(qmFileName))
    {
        qApp->installTranslator(m_pTranslator);
		retranslateUi();
    }
}

static bool increaseVersion(UINT32 *pVersion, int step)
{
	*pVersion = *pVersion + step;
	qDebug() << "increaseVersion = " << *pVersion;
	return (true);
}

static bool verifyMemory(UINT32 *dstBuffer, int )
{
	UINT32 *pTemp = (UINT32 *)dstBuffer;

	for (int i = 0; i < 8 * 1024; i++)
	{
		if (*pTemp != i)
		{
			qDebug() << "\r\n\r\n\r\n ************************************************Verify Error ******************************\r\n\r\n\r\n";
			return (false);
		}
		*pTemp = 0;
		pTemp++;
	}

	qDebug() << " ************************************************Verify Successfully ******************************";
	return (true);
}

void MainWidget::onClickConnectButton()
{
	if ((g_pApp->getConnectStatus() == DEVICE_USB_CONNECTED) || (g_pApp->getConnectStatus() == DEVICE_NETWORK_CONNECTED))
	{
		DeviceSocket *pDeviceSocket = g_pApp->getDeviceConnection();
		pDeviceSocket->endHeartBeat();
		pDeviceSocket->sendBye();
		g_pApp->msleep(20);
		g_pApp->disconnectDevice();
	}
	else
	{
		DeviceSearchingDialog searchingDlg(this);
		searchingDlg.setModal(true);
		int ret = searchingDlg.exec();
		if (ret == QDialog::Accepted) 
		{
			// connect Button clicked 
			DEVICE_INFO *device = searchingDlg.getSelectedDevice();
			g_pApp->connectDevice(QHostAddress(device->m_localIPAddress), QHostAddress(device->m_deviceAddr)); 
		}
	}

//	refreshConnectedButton();
}

bool MainWidget::loadFromDeviceFile(QString tempDevFile)
{
	CPi2000Data *pData = getCPi2000Data();
	QString tempPresetFile = MainApplication::getTempPresetDBFileName();
	QString tempSpeakerFile = MainApplication::getTempSpeakerDBFileName();

	/* Extract Presets.db and speaker.db */
	g_pApp->unzipFileToFolder(tempDevFile, QDir::tempPath());

	if (!QFileInfo::exists(tempPresetFile))
	{
		QString errorInfo = QString("Failed to load File: ") + tempPresetFile;
		L_ERROR(errorInfo);
		QMessageBox::warning(this, QString("Internal Error"), errorInfo, QMessageBox::Ok);
		return (false);
	}
	QFile::setPermissions(tempPresetFile, QFileDevice::WriteOwner | QFileDevice::WriteUser | QFileDevice::WriteGroup | QFileDevice::WriteOther);

	if (!QFileInfo::exists(tempSpeakerFile))
	{
		QString errorInfo = QString("Failed to load File: ") + tempSpeakerFile;
		L_ERROR(errorInfo);
		QMessageBox::warning(this, QString("Internal Error"), errorInfo, QMessageBox::Ok);
		return (false);
	}
	QFile::setPermissions(tempSpeakerFile, QFileDevice::WriteOwner | QFileDevice::WriteUser | QFileDevice::WriteGroup | QFileDevice::WriteOther);

	L_INFO("Loading Presets.db...");

	/* Load Presets.db */
	{
		bool flag = pData->loadFromJSON(tempPresetFile);
		if (flag == false)
		{
			L_ERROR("Failed to load Preset.db");
			QMessageBox::warning(this, QString("Internal Error"), QString("Failed to load JSON File"), QMessageBox::Ok);
			return false;
		}
	}

	L_INFO("Loading Speaker.db...");
	/* Load speaker.db */
	{
		g_database.setSpeakerDBFileName(tempSpeakerFile);
		refreshSpeakerSetting();

		/* Set Load Detection */
		SPEAKER_CHANNEL channel[] = {SPEAKER_CHANNEL_L, SPEAKER_CHANNEL_CENTER, SPEAKER_CHANNEL_LS, SPEAKER_CHANNEL_RS, SPEAKER_CHANNEL_BLS, SPEAKER_CHANNEL_BRS, SPEAKER_CHANNEL_SW};
		/* L/R Speaker */
		SpeakerData speakerData;
		for (int i = 0; i < sizeof(channel) / sizeof(SPEAKER_CHANNEL); i++)
		{
			if (g_database.getSpeaker(pData->m_speakerName[channel[i]], speakerData) == true)
			{
				g_pMainWidget->setLoadDetectionName(channel[i], speakerData.m_speakerType);
			}
		}
	}

	return (true);
}

void MainWidget::onDeviceConnected()
{
	CPi2000Data *pData = getCPi2000Data();

	QCursor oldCursor = cursor();
	setCursor(QCursor(Qt::WaitCursor));

	if (g_pApp->getConnectStatus() == DEVICE_NETWORK_CONNECTED)
	{
		m_pConnectButton->mark(true);
		m_pConnectButton->setText(tr("Connecting..."));
		g_pApp->getDeviceConnection()->saveDB();
		g_pApp->getDeviceConnection()->SaveDeviceFile(REMOTE_DEFAULT_DEVICE_FILE);


		// We can use this to replace g_pApp->msleep(3000);
		QString hallName;
		if (g_pApp->getDeviceConnection()->getHallName(hallName, 3))
		{
			pData->m_strHallName = hallName;
		}
		g_pApp->msleep(1000);
	}
	else
	{   
		/* Connection failed, we need to disconnect it */
//		emit m_pConnectButton->clicked();
		setCursor(oldCursor);
		return;	
	}

	QString strVersion;
	if (g_pApp->getDeviceConnection()->getDeviceFWVersion(strVersion, 2) == false)
	{
		L_ERROR("Failed to get firmware version in 2 seconds!");
		QMessageBox::warning(this, QString("Connection Error"), QString("Failed to get Firmware version!"), QMessageBox::Ok);
		emit m_pConnectButton->clicked();
		setCursor(oldCursor);
		return;
	}
	if (strVersion != CPi2000_NEW_FW_VERSION)
	{
		if (strVersion.indexOf(CPI2000_MAIN_VERSION_MASK) == 0)
		{
			QRegExp rx("(\\d+).(\\d+).(\\d+).(\\d+)"); 
//			bool deviceFlag = false;
			int deviceVersion = 0, currentVersion = 0;
	
			rx.exactMatch(CPi2000_NEW_FW_VERSION);
			currentVersion = rx.cap(4).toInt();

			rx.exactMatch(strVersion);
			deviceVersion = rx.cap(4).toInt();

			if (currentVersion > deviceVersion)
			{
				// Reminder the user to upgrade device
				UpgradeDialog upgradeDlg(this);
				upgradeDlg.setModal(true);

				QString reminder = QString(tr("The device's firmware version is %1. It can be upgraded to %2. However it can still work even you don't upgrade the firmware.").arg(strVersion).arg(CPi2000_NEW_FW_VERSION));
				upgradeDlg.setReminder(reminder);
				upgradeDlg.exec();
	
				if (upgradeDlg.getUpgradeFlag() == true)
				{
					if (upgradeDlg.getUpgradeResult() == true)
					{
						L_INFO("Upgrade Firmware");
						g_pApp->getDeviceConnection()->resetDevice();
						g_pApp->msleep(1000);
					}
					emit m_pConnectButton->clicked();
					setCursor(oldCursor);
					return;
				}
			}
			else if (currentVersion < deviceVersion)
			{
				// Reminder the user to upgrade GUI
				QString reminder = QString(tr("There is new version of GUI application for this device's firmware. Please upgrade the GUI application ASAP.")); 
				QMessageBox::warning(this, tr("Waring"), reminder, tr("Ok"));
			}
		}
		else
		{
			UpgradeDialog upgradeDlg(this);
			upgradeDlg.setModal(true);

			QString reminder = QString(tr("The device's firmware version is %1, and it can't match current GUI application. It must be upgraded to %2 first.").arg(strVersion).arg(CPi2000_NEW_FW_VERSION));
			upgradeDlg.setReminder(reminder);
			upgradeDlg.exec();
	
			if ((upgradeDlg.getUpgradeFlag() == true) && (upgradeDlg.getUpgradeResult() == true))
			{
				L_INFO("Upgrade Firmware");
				g_pApp->getDeviceConnection()->resetDevice();
				g_pApp->msleep(1000);
			}
			emit m_pConnectButton->clicked();
			setCursor(oldCursor);
			return;
		}
	}
	emit g_pApp->setSignalMode((int)SIGNAL_OFF);

	/* 	Add by percy for get .dev file	*/
#if 1
	QString tempDevFile = MainApplication::getTempDevFileName();
	QString tempSpeakerFile = MainApplication::getTempSpeakerDBFileName();
	QString tempPresetFile = MainApplication::getTempPresetDBFileName();

	QFile::remove(tempDevFile);
	QFile::remove(tempSpeakerFile);
	QFile::remove(tempPresetFile);

	L_INFO("Geting device file...");
	if (g_pApp->getFtpFile(REMOTE_DEFAULT_DEVICE_FILE, tempDevFile) == false)
	{
		setCursor(oldCursor);
		L_ERROR("Failed to get device file");
		QMessageBox::warning(this, "Error", "Failed to load CPi2000 device file!", QMessageBox::Ok);
		emit m_pConnectButton->clicked();
		return;
	}
	else
	{
		if (loadFromDeviceFile(tempDevFile) == false)
		{
			emit m_pConnectButton->clicked();
			setCursor(oldCursor);
			return;		
		}
	}
#else
	QString localPresetDB = QString("%1/%2").arg(QDir::tempPath()).arg(PRESET_DB_FILE);
	qDebug() << localPresetDB;

	qDebug() << "==============================================================================";

//	bool initFlag = false;

	if (g_pApp->getFtpFile(PRESET_DB_FILE, localPresetDB) == false)
	{
		QMessageBox::warning(this, "Read Device Setting Error", "Can't get Presets.db from CPi2000", QMessageBox::Ok);
	}
	else
	{
		bool flag = pData->loadFromJSON(localPresetDB);
		if (flag == false)
		{
			QMessageBox::warning(this, QString("Internal Error"), QString("Failed to load JSON File"), QMessageBox::Ok);
			emit m_pConnectButton->clicked();
			setCursor(oldCursor);
			return;
		}

	}

//	QString localSpeakertDB("c://temp//speaker.db");

	QString localSpeakertDB	= QString("%1/%2").arg(QDir::tempPath()).arg(SPEAKER_DB_FILE);
	if (g_pApp->getFtpFile(SPEAKER_DB_FILE, localSpeakertDB) == false)
	{
		QMessageBox::warning(this, "Read Device Setting Error", "Can't get speaker.db from CPi2000", QMessageBox::Ok);
	}
	else
	{
		g_database.setSpeakerDBFileName(localSpeakertDB);

		refreshSpeakerSetting();
	}

	/* Set Load Detection */
	SPEAKER_CHANNEL channel[] = {SPEAKER_CHANNEL_L, SPEAKER_CHANNEL_CENTER, SPEAKER_CHANNEL_LS, SPEAKER_CHANNEL_RS, SPEAKER_CHANNEL_BLS, SPEAKER_CHANNEL_BRS, SPEAKER_CHANNEL_SW};
	/* L/R Speaker */
	SpeakerData speakerData;
	for (int i = 0; i < sizeof(channel) / sizeof(SPEAKER_CHANNEL); i++)
	{
		if (g_database.getSpeaker(pData->m_speakerName[channel[i]], speakerData) == true)
		{
			g_pMainWidget->setLoadDetectionName(channel[i], speakerData.m_speakerType);
		}
	}
#endif

	qDebug() << "==============================================================================";

	/* SUB Master Volume */
	DeviceSocket *pDeviceSocket = g_pApp->getDeviceConnection();
	pDeviceSocket->subOutputVolume();

	if (m_currentTabIndex == 2)
	{
		g_pApp->getDeviceConnection()->subInputVolume();
	}
	else
	{
		g_pApp->getDeviceConnection()->unsubInputVolume();		
	}


//	bool dhcpEnable;
	NETWORK_SETTING networkSetting;
	if (pDeviceSocket->getDeviceNetworkAddr(networkSetting, 2) == true)
	{
		pData->setNetworkAddress(networkSetting);
	}

	/* Get Serial Number */
	QString serialNumber = "Not available";
	pDeviceSocket->getSerialNumber(serialNumber);
	pData->m_strSerialNumber = serialNumber;

	float masterVolumeinDB;
	if (pDeviceSocket->getMasterVolume(masterVolumeinDB))
	{
		float msaterVolume = DBToMasterVolume(masterVolumeinDB);
		pData->setMasterVolume(msaterVolume);
	}

	g_pApp->getDeviceConnection()->subUSBStatus();

	m_pLogWidget->clearLog();
	m_pBasicSettingWidget->setSyncChecked(false);
	refresh();

	if (g_debugEnable == false)
	{
		clearBit(g_debug, LOG_SOCKET_MESSAGE);
	}

	setCursor(oldCursor);
}

void MainWidget::refreshSpeakerSetting()
{
	CPi2000Data *pData = getCPi2000Data();

	QStringList speakerList;
	pData->clearSpeaker();
	g_database.getSpeakerList(speakerList);
	for (int i = 0; i < speakerList.length(); i++)
	{
		SpeakerData speakerData;
		if (g_database.getSpeaker(speakerList[i], speakerData) == true)
		{
			pData->addSpeaker(speakerList[i], speakerData.m_speakerType);
		}
		else
		{
			qDebug() << "**********************Invalid Speaker: " << speakerList[i] << "**************************************";
		}
	}

	DeviceSocket *pDeviceSocket = g_pApp->getDeviceConnection();
	if (pDeviceSocket != nullptr)
	{
		QString nodeList[] = {
			NODE_SPEAKER1_NAME, NODE_SPEAKER2_NAME, NODE_SPEAKER3_NAME, NODE_SPEAKER4_NAME, 
			NODE_SPEAKER5_NAME, NODE_SPEAKER6_NAME, NODE_SPEAKER7_NAME, NODE_SPEAKER8_NAME };

		for (int i = 0; i < 8; i++)
		{
			pData->m_speakerName[i] = pDeviceSocket->getValueByNode(nodeList[i], 3);
			if (pData->m_speakerName[i].isEmpty())
			{
				L_ERROR(QString("Failed to get speaker name, speaker index = %1").arg(i));
			}
			if ((i >= 0) && (i <= 2))
			{
				/* check speaker name for L/C/R */
				if ((pData->m_speakerName[i] == QString("Biamp-Bypass")) || (pData->m_speakerName[i] == QString("Triamp-Bypass")))
				{
					L_ERROR(QString("Speaker%1 Name error: %2").arg(i).arg(pData->m_speakerName[i]));
				}
			}
		}
	}

	m_pOverviewWidget->refreshSpeaker();
}

void MainWidget::refresh()
{
	refreshConnectedButton();

	m_pOverviewWidget->refresh();
	m_pBasicSettingWidget->refresh();
	m_pInputSettingWidget->refresh();
	m_pOutputSettingWidget->refresh();
//	m_pLogWidget->refresh();
}

void MainWidget::onDeviceDisconnected()
{
	emit g_pApp->setSignalMode((int)SIGNAL_OFF);
	refreshConnectedButton();

	float outputDB[8];
	for (int i = 0; i < 8; i++)
	{
		outputDB[i] = -90.0;		
	}

	CPi2000Data *pData = getCPi2000Data();
	pData->setOutputDB(outputDB);

	g_pMainWidget->refreshOutputVolume();
	refresh();
}

void MainWidget::refreshUSBFileMenu()
{
	DEVICE_CONNECT_STATUS status = g_pApp->getConnectStatus();
	bool USBConnectFlag = getCPi2000Data()->getUSBStatus();

	if ((status == DEVICE_NETWORK_CONNECTED))
	{
		m_pActionLoadFromUSB->setEnabled(USBConnectFlag);
		m_pActionSaveToUSB->setEnabled(USBConnectFlag);
	}
	else
	{
		m_pActionLoadFromUSB->setEnabled(true);
		m_pActionSaveToUSB->setEnabled(true);
	}
}

void MainWidget::refreshConnectedButton()
{
	DEVICE_CONNECT_STATUS status = g_pApp->getConnectStatus();

	bool connectedFlag = false;

	switch (status)
	{
	case DEVICE_SEARCHING:
	case DEVICE_CONNECTING:
	case DEVICE_DISCONNECTED:
		m_pConnectButton->mark(0);
		m_pConnectButton->setText(tr("Offline"));
//		m_pSendButton->setEnabled(false);
		break;
	default:
		m_pConnectButton->mark(1);
		m_pConnectButton->setText(tr("Connected"));
		connectedFlag = true;
//		m_pSendButton->setEnabled(true);
	}

	m_pActionLoadDeviceFile->setEnabled(true);
	m_pActionSaveDeviceFile->setEnabled(true);
	m_pActionRestore->setEnabled(true);
	m_pActionAdjustTime->setEnabled(true);
	m_pActionSpeakerEditor->setEnabled(true);
	m_pActionImportSpeaker->setEnabled(connectedFlag);
	if (connectedFlag)
	{
		m_pActionUpgrade->setEnabled(true);
	}
	else
	{
		m_pActionUpgrade->setEnabled(false);
	}

	m_pSetIPAddress->setEnabled(true);
	refreshUSBFileMenu();
	m_pLogWidget->refreshButtonStatus(connectedFlag);

	return;
}

void MainWidget::closeEvent(QCloseEvent *event)  
{		
	m_pPanelWidget->close();

	event->accept();
}



void MainWidget::on1STimerOut()
{
	static int s_sendCount = 0;		/* Time stamp that send the message to CPi2000 */

	if (g_pApp->getConnectStatus() == DEVICE_NETWORK_CONNECTED)
	{
		DeviceSocket *pDeviceSocket = g_pApp->getDeviceConnection();
		pDeviceSocket->sendHeartBeat();

		s_sendCount++;
		if (pDeviceSocket->getRecvFlag() == true)
		{
			s_sendCount = 0;					
		}

		if (s_sendCount > 5)
		{
			g_waittingWidget.stopWaitting();
			L_ERROR("Recvive no message from device in 5 seconds, socket is broken and CPi2000 is disconnected!");
			emit m_pConnectButton->clicked();
			QMessageBox::warning(this, tr("Connection broken"), tr("The CPi2000 is disconnected. Please check the device status!"), QMessageBox::Ok);
		}
	}
#if 0
	// refresh the recv count. 
	static int oldRecvCount = m_recvCount;
	if (oldRecvCount != m_recvCount)
	{
		m_pRecvEdit->setText(QString::number(m_recvCount, 10));
		oldRecvCount = m_recvCount;
	}
#endif
}


void MainWidget::createMenuBar()
{
	m_pActionLoadDeviceFile = new QAction(tr("Device File..."), this);
    connect(m_pActionLoadDeviceFile, SIGNAL(triggered()), this, SLOT(onOpenDeviceFile()));

	m_pActionLoadFromUSB = new QAction(tr("from USB..."), this);
    connect(m_pActionLoadFromUSB, SIGNAL(triggered()), this, SLOT(onLoadUSBFile()));

	m_pActionSaveDeviceFile = new QAction(tr("Device File..."), this);
    connect(m_pActionSaveDeviceFile, SIGNAL(triggered()), this, SLOT(onSaveDeviceFile()));

	m_pActionSaveToUSB = new QAction(tr("to USB..."), this);
    connect(m_pActionSaveToUSB, SIGNAL(triggered()), this, SLOT(onSaveUSBFile()));

	m_pActionExit = new QAction(tr("Exit"), this);
	connect(m_pActionExit, SIGNAL(triggered()), this, SLOT(close()));

	m_pMenuBar = new QMenuBar(this);
	m_pMenuBar->setObjectName(QStringLiteral("m_pMenuBar"));

    m_pMenu1 = m_pMenuBar->addMenu(tr("File"));
	
	m_pMenuOpen = new QMenu(tr("Load"));
    m_pMenuOpen->addAction(m_pActionLoadDeviceFile);
    m_pMenuOpen->addAction(m_pActionLoadFromUSB);
	m_pMenu1->addMenu(m_pMenuOpen);

	m_pMenuSave = new QMenu(tr("Save"));
    m_pMenuSave->addAction(m_pActionSaveDeviceFile);
    m_pMenuSave->addAction(m_pActionSaveToUSB);
	m_pMenu1->addMenu(m_pMenuSave);
	m_pMenu1->addAction(m_pActionExit);

	m_pActionSpeakerEditor = new QAction(tr("Speaker Editor..."), this);
    connect(m_pActionSpeakerEditor, SIGNAL(triggered()), this, SLOT(onSpeakerEditor()));

	m_pActionImportSpeaker = new QAction(tr("Import Speaker..."), this);
    connect(m_pActionImportSpeaker, SIGNAL(triggered()), this, SLOT(onSpeakerImport()));

	m_pActionAdjustTime = new QAction(tr("Calibrate Time..."), this);
    connect(m_pActionAdjustTime, SIGNAL(triggered()), this, SLOT(onAdjustTime()));

	m_pActionUpgrade = new QAction(tr("Upgrade Firmware..."), this);
    connect(m_pActionUpgrade, SIGNAL(triggered()), this, SLOT(onUpgradeFirmware()));
	m_pActionUpgrade->setEnabled(false);

	m_pActionRestore = new QAction(tr("Restore Factory Settings..."), this);
    connect(m_pActionRestore, SIGNAL(triggered()), this, SLOT(onRestoreFactory()));

	m_pSetIPAddress = new QAction(tr("Set IP Address..."), this);
    connect(m_pSetIPAddress, SIGNAL(triggered()), this, SLOT(onSetIPAddress()));

	m_pActionZoom = new QAction(tr("Zoom..."), this);
    connect(m_pActionZoom, SIGNAL(triggered()), this, SLOT(onZoom()));

	m_pActionOption = new QAction(tr("Debug Option..."), this);
    connect(m_pActionOption, SIGNAL(triggered()), this, SLOT(onOption()));

	m_pMenu2 = m_pMenuBar->addMenu(tr("Tools"));
	m_pMenu2->addAction(m_pActionSpeakerEditor);
//	m_pMenu2->addAction(m_pActionImportSpeaker);
	m_pMenu2->addAction(m_pActionAdjustTime);
	m_pMenu2->addAction(m_pActionUpgrade);
	m_pMenu2->addAction(m_pActionRestore);
	m_pMenu2->addAction(m_pSetIPAddress);
//	m_pMenu2->addSeparator();
	m_pMenu2->addAction(m_pActionZoom);
	m_pActionZoom->setVisible(false);
	m_pMenu2->addAction(m_pActionOption);
	m_pActionOption->setVisible(false);

	m_pActionAbout = m_pMenuBar->addAction(tr("About"));
	connect(m_pActionAbout, SIGNAL(triggered()), this, SLOT(about()));

	connect(m_pMenu1, SIGNAL(aboutToShow()), this, SLOT(refreshAction())); 

    m_pMenuShortcut = new QShortcut(QKeySequence((Qt::CTRL + Qt::Key_C), (Qt::CTRL + Qt::Key_F)), this);
    m_pMenuShortcut->setContext(Qt::ApplicationShortcut);
    connect(m_pMenuShortcut, SIGNAL(activated()), this, SLOT(onMenuShortcutActivated()));
}

void MainWidget::about()
{
	QString deviceFWVersion = "unknown", a5FWVersion = "Not available", a5ModelVersion = "Not available", dspVersion = "Not available", frontPanelVersion = "Not available";
	QString strVersion = tr("Build Time: %1 %2").arg(__DATE__, __TIME__);

	DeviceSocket *pDeviceSocket = g_pApp->getDeviceConnection();
	if (pDeviceSocket != nullptr)
	{
		QString version;
		if (pDeviceSocket->getDeviceFWVersion(version, 3) == true)
		{
			deviceFWVersion = version;
		}
		if (pDeviceSocket->getDSPFWVersion(version, 3) == true)
		{
			dspVersion = version;
		}
		if (pDeviceSocket->getFrontPanelFWVersion(version, 3) == true)
		{
			frontPanelVersion = version;
		}
		if (pDeviceSocket->getA5FWVersion(version, 3) == true)
		{
			a5FWVersion = version;
		}
		if (pDeviceSocket->getA5ModelVersion(version, 3) == true)
		{
			a5ModelVersion = version;
		}
	}

	QString about(tr("<h2>CPi2000 Application Version: %1</h2>").arg(CPi2000_GUI_VERSION));
	QString deviceVersion(tr("<p>CPi2000 Device Firmware Version: %1</p>").arg(deviceFWVersion));
	QString a5_FWVersion(tr("<p>CPi2000 A5 Firmware Version: %1</p>").arg(a5FWVersion));
	QString a5_SWVersion(tr("<p>CPi2000 A5 Software Version: %1</p>").arg(a5ModelVersion));
	QString dsp_Version(tr("<p>CPi2000 DSP Firmware Version: %1</p>").arg(dspVersion));
	QString FTVersion(tr("<p>CPi2000 Front Panel Firmware Version: %1</p>").arg(frontPanelVersion));
	QString copyright(tr("<p>Copyright &copy; 2017 Harman Inc.</p>"));
	if (m_debugActive)
	{
		strVersion += QString("<p>Designed by Harman Employee ID 28200455 - Percy Zhang</p>");
	}

	int ret = QMessageBox::information(this, tr("About CPi2000"), about + deviceVersion + dsp_Version + a5_FWVersion + a5_SWVersion + FTVersion + copyright  + strVersion, tr("OK"));

	if (ret == 0)
	{
		return;
	}


#if 0
	if (m_pPanelWidget->isVisible())
	{
		m_pPanelWidget->hide();
	}
	else
	{
		m_pPanelWidget->show();
	}
#endif
}

static SpeakerDialogResult s_result;

void MainWidget::onMenuShortcutActivated()
{
	m_debugActive = true;
	m_pActionZoom->setVisible(m_debugActive);
	m_pActionOption->setVisible(m_debugActive);
}

void MainWidget::SaveSpeakerToDevice(SpeakerData *pSpeakerData)
{
	DeviceSocket *pDeviceSocket = g_pApp->getDeviceConnection();
	qDebug() << s_result.m_operation << pSpeakerData->getSpeakerName();

	static QCursor s_oldCursor = cursor();
	setCursor(QCursor(Qt::WaitCursor));

	// 2. Let's modify the database.
	g_database.modifySpeaker(pSpeakerData->getSpeakerName(), pSpeakerData);

	// 3. Send the speaker.db back to CPi2000.
	if (pDeviceSocket != nullptr)
	{
		QString localSpeakertDB	= MainApplication::getTempSpeakerDBFileName();
		QString remoteFile = QString("ftp://%1/%2").arg(g_pApp->getDeviceConnection()->getDeviceAddr().toString()).arg(SPEAKER_DB_FILE);
		qDebug() << remoteFile;

		if (g_pApp->putFtpFile(localSpeakertDB, remoteFile) == false)
		{
			QMessageBox::warning(this, "Read Device Setting Error", "Can't get speaker.db from CPi2000", QMessageBox::Ok);
			setCursor(s_oldCursor);
			return;
		}
		// Nofify the CPi2000 to dispatch the speaker.db
		pDeviceSocket->loadSpeakerFile();
	}

	//4. set the channel with the correct speaker name
	QString speakerNameList[] = {
		NODE_SPEAKER1_NAME, NODE_SPEAKER2_NAME, NODE_SPEAKER3_NAME, NODE_SPEAKER4_NAME, 
		NODE_SPEAKER5_NAME, NODE_SPEAKER6_NAME, NODE_SPEAKER7_NAME, NODE_SPEAKER8_NAME };

	// 1. Let's set the speaker
	CPi2000Data *pData = getCPi2000Data();
	for (int i = 0; i < 8; i++)
	{
		if (s_result.m_speakerChannelApplied[i] != 0)
		{
			pData->m_speakerName[i] = pSpeakerData->getSpeakerName();
			if (pDeviceSocket != nullptr)
			{
				pDeviceSocket->setNormalString(speakerNameList[i], pSpeakerData->getSpeakerName());
			}
		}
	}
	setCursor(s_oldCursor);
}

void MainWidget::onSpeakerEditor()
{
	CPi2000Data *pData = getCPi2000Data();
	QStringList speakerList;

	QString localSpeakertDB	= MainApplication::getTempSpeakerDBFileName();
	qDebug() << localSpeakertDB;

	DeviceSocket *pDeviceSocket = g_pApp->getDeviceConnection();

	emit g_pApp->setSignalMode((int)SIGNAL_OFF);

	if (pDeviceSocket == nullptr)
	{
		QFile localFile(localSpeakertDB);
		if (localFile.open(QIODevice::ReadOnly) == false)
		{
			QFile::copy(QString(":/binaries/speaker.db"), localSpeakertDB);
			QFile::setPermissions(localSpeakertDB, QFileDevice::WriteOwner | QFileDevice::WriteUser | QFileDevice::WriteGroup | QFileDevice::WriteOther);
		}
		else
		{
			localFile.close();
		}
	}
	else
	{
		if (g_pApp->getFtpFile(SPEAKER_DB_FILE, localSpeakertDB) == false)
		{
			QMessageBox::warning(this, "Read Device Setting Error", "Can't get speaker.db from CPi2000", QMessageBox::Ok);
			return;
		}
	}
	g_database.setSpeakerDBFileName(localSpeakertDB);
	g_database.getSpeakerList(speakerList);

	SpeakerListDialog dlg(this);
	dlg.setSpeakerList(speakerList);

	dlg.setModal(true);
	dlg.exec();

	s_result = *dlg.getOperation();
	if (s_result.m_operation == SPEAKER_OPERATION_CANCEL)
	{
		return;	
	}

	int editorResult = QDialog::Rejected;

	if (s_result.m_operation == SPEAKER_OPERATION_NEW)
	{
#if 0
		QString speakerNames[] = {"paspass", "bipass", "tripass", "surpass", "swpass" };
		QString speakerName = speakerNames[s_result.m_speakerType];
		SpeakerData speakerData;
		if (g_database.getSpeaker(speakerName, speakerData) == false)
		{
			QMessageBox::warning(this, tr("Warning"), tr("Can't find the speaker: %1!").arg(speakerName), QMessageBox::Ok);
			return;
		}
		speakerData.m_writable = true;
		speakerData.m_readable = true;

		QString speakerNameList[] = {
			NODE_SPEAKER1_NAME, NODE_SPEAKER2_NAME, NODE_SPEAKER3_NAME, NODE_SPEAKER4_NAME, 
			NODE_SPEAKER5_NAME, NODE_SPEAKER6_NAME, NODE_SPEAKER7_NAME, NODE_SPEAKER8_NAME };

		// 1. Let's set the speaker
		for (int i = 0; i < 8; i++)
		{
			if (s_result.m_speakerChannelApplied[i] != 0)
			{
				if (pDeviceSocket != nullptr)
				{
					pDeviceSocket->setNormalString(speakerNameList[i], speakerName);
				}
			}
		}
#else
		// 1. Read speaker data from speaker template file.
		QString speakerNames[] = {":default_passive.spk", ":default_biamp.spk", ":default_triamp.spk", ":default_surround.spk", ":default_subwoofer.spk" };
		QString fileName = speakerNames[s_result.m_speakerType];
		QFile file(fileName);
		if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
		{
			QMessageBox::warning(this, tr("System Error"), tr("Can't find the file name: %1!").arg(fileName), QMessageBox::Ok);
			return;
		}
		QByteArray json = file.readAll();
		file.close();

		SpeakerData speakerData;
		speakerData.m_speakerName = s_result.m_speakerName;
		bool readFlag = speakerData.readFromJSON(QString(json));
		if (readFlag == false)
		{
			QMessageBox::warning(this, tr("Warning"), tr("Can't find the speaker: %1!").arg(s_result.m_speakerName), QMessageBox::Ok);
			return;
		}
		speakerData.m_writable = true;
		speakerData.m_readable = true;

		//2. add speaker to speaker.db
		g_database.addSpeaker(speakerData.getSpeakerName(), &speakerData);
		qDebug() << speakerData.getSpeakerName();

		if (pDeviceSocket != nullptr)
		{
			// 3. Send the speaker.db back to CPi2000.
			QString localSpeakertDB	= MainApplication::getTempSpeakerDBFileName();;
			QString remoteFile = QString("ftp://%1/%2").arg(g_pApp->getDeviceConnection()->getDeviceAddr().toString()).arg(SPEAKER_DB_FILE);

			if (g_pApp->putFtpFile(localSpeakertDB, remoteFile) == false)
			{
				QMessageBox::warning(this, "Read Device Setting Error", "Can't get speaker.db from CPi2000", QMessageBox::Ok);
				return;
			}
			// Nofify the CPi2000 to dispatch the speaker.db
			pDeviceSocket->loadSpeakerFile();
		}

		//4. set the channel with the correct speaker name
		QString speakerNameList[] = {
			NODE_SPEAKER1_NAME, NODE_SPEAKER2_NAME, NODE_SPEAKER3_NAME, NODE_SPEAKER4_NAME, 
			NODE_SPEAKER5_NAME, NODE_SPEAKER6_NAME, NODE_SPEAKER7_NAME, NODE_SPEAKER8_NAME };

		// 1. Let's set the speaker
		for (int i = 0; i < 8; i++)
		{
			if (s_result.m_speakerChannelApplied[i] != 0)
			{
				pData->m_speakerName[i] = speakerData.getSpeakerName();
				if (pDeviceSocket != nullptr)
				{
					pDeviceSocket->setNormalString(speakerNameList[i], speakerData.getSpeakerName());
				}
			}
		}
#endif
		SpeakerEditor editor(this);
		/* 临时解决方案，保证可以在editor中使用save to device 功能*/
		connect(&editor, SIGNAL(applyToDevice(SpeakerData * )), this, SLOT(SaveSpeakerToDevice(SpeakerData *)));
		speakerData.setSpeakerName(s_result.m_speakerName);
		editor.setSpeakerData(&speakerData);
		editor.setApplyChannel(s_result.m_speakerChannelApplied);
		editor.setModal(true);
		editorResult = editor.exec();

		if (editorResult == QDialog::Accepted)
		{
			SaveSpeakerToDevice(editor.getSpeakerData());
		}
		disconnect(&editor, SIGNAL(applyToDevice(SpeakerData * )), this, SLOT(SaveSpeakerToDevice(SpeakerData *)));
	}
	else if (s_result.m_operation == SPEAKER_OPERATION_DELETE)
	{
		for (int i = 0; i < 8; i++)
		{
			if (pData->m_speakerName[i] == s_result.m_speakerName)
			{
				QMessageBox::warning(this, tr("Warning"), tr("The selected speaker:%1 is being used by CPi2000 now, and it can't be deleted from the device!").arg(s_result.m_speakerName), tr("OK"));
				return;
			}
		}
		int ret = QMessageBox::question(this, tr("Warning"), tr("Do you want to delete the speaker: %1 from the speaker presets?").arg(s_result.m_speakerName), tr("Yes"), tr("No"));
		if (ret == 0)
		{
			g_database.deleteSpeaker(s_result.m_speakerName);	
		}

		// 3. Send the speaker.db back to CPi2000.
		if (pDeviceSocket != nullptr)
		{
			QString localSpeakertDB	= MainApplication::getTempSpeakerDBFileName();;
			QString remoteFile = QString("ftp://%1/%2").arg(g_pApp->getDeviceConnection()->getDeviceAddr().toString()).arg(SPEAKER_DB_FILE);

			if (g_pApp->putFtpFile(localSpeakertDB, remoteFile) == false)
			{
				QMessageBox::warning(this, "Read Device Setting Error", "Can't get speaker.db from CPi2000", QMessageBox::Ok);
				return;
			}
		}
	}
	else if (s_result.m_operation == SPEAKER_OPERATION_MODIFY)
	{
		QString description;
		if (g_database.getSpeaker(s_result.m_speakerName, description) == false)
		{
			QMessageBox::warning(this, tr("Warning"), tr("Can't find the speaker: %1 in the speaker presets!").arg(s_result.m_speakerName), tr("Ok"));
			return;
		}

		QString speakerNameList[] = {
			NODE_SPEAKER1_NAME, NODE_SPEAKER2_NAME, NODE_SPEAKER3_NAME, NODE_SPEAKER4_NAME, 
			NODE_SPEAKER5_NAME, NODE_SPEAKER6_NAME, NODE_SPEAKER7_NAME, NODE_SPEAKER8_NAME };

		// 1. Let's set the speaker
		for (int i = 0; i < 8; i++)
		{
			if (s_result.m_speakerChannelApplied[i] != 0)
			{
				pData->m_speakerName[i] = s_result.m_speakerName;
				if (pDeviceSocket != nullptr)
				{
					pDeviceSocket->setNormalString(speakerNameList[i], s_result.m_speakerName);
				}
			}
		}

		SpeakerData speakerData;
		speakerData.setSpeakerName(s_result.m_speakerName);
		speakerData.readFromJSON(description);

		SpeakerEditor editor(this);
		/* 临时解决方案，保证可以在editor中使用save to device*/
		connect(&editor, SIGNAL(applyToDevice(SpeakerData * )), this, SLOT(SaveSpeakerToDevice(SpeakerData *)));
		editor.setSpeakerData(&speakerData);
		editor.setApplyChannel(s_result.m_speakerChannelApplied);
		editor.setModal(true);
		editorResult = editor.exec();

		if (editorResult == QDialog::Accepted)
		{
			SaveSpeakerToDevice(editor.getSpeakerData());
		}
		disconnect(&editor, SIGNAL(applyToDevice(SpeakerData * )), this, SLOT(SaveSpeakerToDevice(SpeakerData *)));
	}

	refreshSpeakerSetting();
	m_pBasicSettingWidget->refreshLoadDetection();
}

void MainWidget::onAdjustTime()
{
	TimeAdjustDialog timeDlg(this);
	timeDlg.setModal(true);
//	upgradeDlg.setCurrentFWVersion(deviceVersion);
	int ret = timeDlg.exec();	

	if (ret != 0)
	{
	}
}

void MainWidget::onRestoreFactory()
{
	DeviceSocket *pDeviceSocket = g_pApp->getDeviceConnection();
	if (QMessageBox::question(this, tr("Warning"), tr("Do you want to restore the factory setting? All user data will be lost if you restore the factory setting!"), tr("Yes"), tr("No")) != 0)
	{
		return;
	}

	if (pDeviceSocket != nullptr)
	{
		pDeviceSocket->restoreFactorySetting();
		pDeviceSocket->resetDevice();

		emit m_pConnectButton->clicked();
	}

	g_pApp->msleep(1000);
	QMessageBox::information(this, tr("Restore factory Setting"), tr("The factory setting is restored, please re-connect the CPi2000 with new IP Address"), QMessageBox::Ok);
}

void MainWidget::onUpgradeFirmware()
{
	DeviceSocket *pDeviceSocket = g_pApp->getDeviceConnection();
	if (pDeviceSocket == nullptr)
	{
		QMessageBox::warning(this, tr("Warning"), tr("Can't connect to CPi2000 device, please check the network connection!"), QMessageBox::Ok);
		return;
	}

	UpgradeDialog upgradeDlg(this);
	upgradeDlg.setModal(true);
	QString deviceVersion;
	pDeviceSocket->getDeviceFWVersion(deviceVersion, 3);

	int devicePositon = deviceVersion.lastIndexOf(".");
	QString deviceMinorVersion = deviceVersion.mid(devicePositon + 1);
	int minorDeviceVersion = deviceMinorVersion.toInt();

	QString internalFWVersionString = QString(CPi2000_NEW_FW_VERSION);
	int internalFWPositon = internalFWVersionString.lastIndexOf(".");
	QString internalFWMinorVersion = internalFWVersionString.mid(internalFWPositon + 1);
	int internalFWVersion = internalFWMinorVersion.toInt();

	QString remainder;
	if (minorDeviceVersion < internalFWVersion)
	{
		remainder = QString(tr("Current CPi2000 device firmware version is %1, and it will be upgraded to %2. Please backup your device settings before upgrading! ").arg(deviceVersion).arg(CPi2000_NEW_FW_VERSION));	
		upgradeDlg.setOperation(UPGRADE_OPEATION);
	}
	else if (minorDeviceVersion > internalFWVersion)
	{
		remainder = QString(tr("Current CPi2000 device firmware version is %1, and it will be downgraded to %2. Please backup your device settings before downgrading! ").arg(deviceVersion).arg(CPi2000_NEW_FW_VERSION));	
		upgradeDlg.setOperation(DOWNGRADE_OPEATION);
	}
	else
	{
		QMessageBox::warning(this, tr("Information"), tr("Current CPi2000 device firmware matches this GUI application well, and no upgrading is needed!"), QMessageBox::Ok);
		return;
	}

	upgradeDlg.setReminder(remainder);
	upgradeDlg.exec();
	
	if (upgradeDlg.getUpgradeFlag() == true)
	{
		if (upgradeDlg.getUpgradeResult() == true)
		{
			L_INFO("Upgrade Firmware");
	//		QMessageBox::warning(this, tr("Warning"), tr("Can't connect to CPi2000 device, please check the network connection!"), QMessageBox::Ok);
			pDeviceSocket->resetDevice();
			g_pApp->msleep(1000);
		}
		emit m_pConnectButton->clicked();
	}

}

void MainWidget::onZoom()
{
	ZoomDlg zoomDlg(this);
	zoomDlg.setModal(true);

	zoomDlg.exec();
}

void MainWidget::onOption()
{
	OptionDlg optionDlg(this);
	optionDlg.setModal(true);

	optionDlg.exec();
}

void MainWidget::onSetIPAddress()
{
	IPAddressDialog ipAddressDlg(this);
	ipAddressDlg.setModal(true);
	int ret = ipAddressDlg.exec();

	if (ret == QDialog::Accepted)
	{
		DeviceSocket *pDeviceSocket = g_pApp->getDeviceConnection();
		NETWORK_SETTING newSetting = ipAddressDlg.getNetworkSetting();
		if (pDeviceSocket != nullptr)
		{
			pDeviceSocket->setDeviceNetworkAddress(newSetting);
			g_pApp->msleep(500);
			emit m_pConnectButton->clicked();
		}

		QMessageBox::information(this, tr("Set IP Address"), tr("IP Address has been set, please re-connect the CPi2000 with new IP Address"), QMessageBox::Ok);
	}
}

void MainWidget::refreshMasterVolume()
{
	m_pOverviewWidget->refreshMasterVolume();
	m_pBasicSettingWidget->refreshMasterVolume();
}

void MainWidget::refreshInputSource()
{
	m_pOverviewWidget->refreshInputSource();
	m_pInputSettingWidget->refreshInputSource();
}

void MainWidget::refreshMasterMuteFlag()
{
	m_pOverviewWidget->refreshMasterMuteFlag();
	m_pBasicSettingWidget->refreshMasterMuteFlag();
}


void MainWidget::onSaveUSBFile()
{
	DeviceSocket *pDeviceSocket = g_pApp->getDeviceConnection();
	QStringList fileNameList;
	QString newFileName = "PRESET000.dev";

	if (pDeviceSocket != nullptr)
	{
		if (pDeviceSocket->getUSBFileList(fileNameList) == false)
		{
			QMessageBox::warning(this, "Warning", tr("Getting USB File Name failed!"), QMessageBox::Ok);
			return;
		}

		if (pDeviceSocket->getUSBNewFileName(newFileName) == false)
		{
			QMessageBox::warning(this, "Warning", tr("Getting USB New File Name failed!"), QMessageBox::Ok);
			return;	
		}
	}

	USBFileDialog dlg(this);
	dlg.setToSaveMode(fileNameList, newFileName);

	dlg.setModal(true);
	int ret = dlg.exec();
	if (ret == QDialog::Accepted) 
	{
		if (pDeviceSocket != nullptr)
		{
			QString fileName = dlg.getSelectedFileName();
			pDeviceSocket->saveUSBFile(fileName);
			QMessageBox::information(this, tr("File is saved successfully"), tr("Current setting is saved in USB as:%1").arg(fileName), QMessageBox::Ok);
		}
		else
		{
			QMessageBox::information(this, "Warning", tr("Save file failed: No CPi2000 is connected!"), QMessageBox::Ok);
		}
	}
}

void MainWidget::onLoadUSBFile()
{
	DeviceSocket *pDeviceSocket = g_pApp->getDeviceConnection();

	QStringList fileNameList;
	if (pDeviceSocket != nullptr)
	{
		if (pDeviceSocket->getUSBFileList(fileNameList) == false)
		{
			QMessageBox::warning(this, "Warning", tr("Getting USB File Name failed!"), QMessageBox::Ok);
			return;
		}
	}
	
	USBFileDialog dlg(this);
	dlg.setToLoadMode(fileNameList);
	dlg.setModal(true);
	int ret = dlg.exec();
	if (ret == QDialog::Accepted) 
	{
		QString FileName = dlg.getSelectedFileName();

		if (pDeviceSocket != nullptr)
		{
			qDebug() << "Let's load file: " << FileName;
			pDeviceSocket->loadUSBFile(FileName);
			pDeviceSocket->resetDevice();

			QMessageBox::warning(this, tr("USB File is loaded successfully"), tr("The device will reboot to reload the device setting. Please re-connect the CPi2000 after the device's rebooting"), tr("OK")); 
			emit m_pConnectButton->clicked();
		}
	}
}


void MainWidget::onSaveDeviceFile()
{
	if (g_pApp->getConnectStatus() == DEVICE_NETWORK_CONNECTED)
	{
		g_pApp->getDeviceConnection()->saveDB();
		g_pApp->getDeviceConnection()->SaveDeviceFile("SAVE_DEFAULT.dev");
	}

	QString directory = ".";
	QString fileName = QFileDialog::getSaveFileName(
		this,
		"Save Device File",
		directory,
		tr("Device File(*.dev);; All files (*.*)")
	);

	if (fileName.isEmpty() == true)
	{
		return;
	}

	QFile file(fileName);
    if (!file.open(QIODevice::WriteOnly)) {
        QMessageBox::warning(this, tr("Save File Error"),
                             tr("Cannot save the device settings to file: %1.")
                             .arg(file.fileName()), tr("OK"));
        return;
    }
	file.close();
	QFile::remove(fileName);

//	qDebug() << "Save to " << fileName << "Successfully! Extract to " << QDir::tempPath();

	if (g_pApp->getConnectStatus() == DEVICE_NETWORK_CONNECTED)
	{
		if (g_pApp->getFtpFile(REMOTE_DEFAULT_DEVICE_FILE, fileName) == false)
		{
			QMessageBox::warning(this, "Save Device file Failed", "Can't save CPi2000 device file!", QMessageBox::Ok);
		}
	}
	else
	{
		CPi2000Data *pData = getCPi2000Data();
		QString localPresetFileName = MainApplication::getTempPresetDBFileName();

		if (pData->saveToJSON(localPresetFileName) == false)
		{
			QMessageBox::warning(this, "Save Device file Failed", "Can't save CPi2000 device file!", QMessageBox::Ok);
			return;
		}

		QString localSpeakertDB	= MainApplication::getTempSpeakerDBFileName();
		QFile localFile(localSpeakertDB);
		if (localFile.open(QIODevice::ReadOnly) == false)
		{
			QFile::copy(QString(":/binaries/speaker.db"), localSpeakertDB);
			QFile::setPermissions(localSpeakertDB, QFileDevice::WriteOwner | QFileDevice::WriteUser | QFileDevice::WriteGroup | QFileDevice::WriteOther);
		}
		else
		{
			localFile.close();
		}

		QStringList fileList;
		fileList << localPresetFileName << localSpeakertDB;
		g_pApp->zipFilesToFile(fileName, fileList);
	}
}

void MainWidget::onOpenDeviceFile()
{
	QString directory = ".";
	QString fileName = QFileDialog::getOpenFileName(
		this,
		"Open Device File",
		directory,
		tr("Device File(*.dev);; All files (*.*)")
	);

	if (fileName.isEmpty() != false)
	{
		return;
	}

	if (g_pApp->getConnectStatus() != DEVICE_NETWORK_CONNECTED)
	{
		if (loadFromDeviceFile(fileName) == true)
		{
			m_pBasicSettingWidget->setSyncChecked(false);
			refresh();
		}
		return;
	}	
	else
	{
		QString remoteFile = QString("ftp://%1/%2").arg(g_pApp->getDeviceConnection()->getDeviceAddr().toString()).arg(REMOTE_DEFAULT_DEVICE_FILE);
		qDebug() << remoteFile;


		if (g_pApp->putFtpFile(fileName, remoteFile) == false)
		{
			QMessageBox::warning(this, tr("Loading Device File Failed"), tr("Can't load CPi2000 device file!"), QMessageBox::Ok);
		}

		DeviceSocket *pDeviceSocket = g_pApp->getDeviceConnection();
		pDeviceSocket->loadDeviceFile(REMOTE_DEFAULT_DEVICE_FILE);
		pDeviceSocket->resetDevice();

		QMessageBox::warning(this, tr("Device File is loaded successfully"), tr("The device will reboot to reload the device setting. Please re-connect the CPi2000 after the device's rebooting"), tr("OK")); 
		emit m_pConnectButton->clicked();
	}
}

void MainWidget::setLoadDetectionName(SPEAKER_CHANNEL channel, SPEAKER_TYPE type)
{
	m_pBasicSettingWidget->setLoadDetectionName(channel, type);
}

void MainWidget::changeLoadDetection()
{
	m_pBasicSettingWidget->changeLoadDetection();
}

void MainWidget::refreshInputVolume()
{
	m_pInputSettingWidget->refreshInputVolume();
}

void MainWidget::startSignalGenerator()
{
	if (g_pApp->getConnectStatus() == DEVICE_NETWORK_CONNECTED)
	{
		g_pApp->getDeviceConnection()->subRTAMeter();

	}
//	m_pOverviewButton->setEnabled(false);
//	m_pBasicSettingButton->setEnabled(false);
//	m_pInputSettingButton->setEnabled(false);
//	m_pLogButton->setEnabled(false);

//	m_pActionLoadDeviceFile->setEnabled(false);
//	m_pActionSaveDeviceFile->setEnabled(false);
//	m_pActionUpgrade->setEnabled(false);
//	m_pActionRestore->setEnabled(false);
//	m_pActionAdjustTime->setEnabled(false);
//	m_pActionSpeakerEditor->setEnabled(false);
//	m_pActionImportSpeaker->setEnabled(false);
//	m_pSetIPAddress->setEnabled(false);

//	m_pActionLoadFromUSB->setEnabled(false);
//	m_pActionSaveToUSB->setEnabled(false);
}

void MainWidget::stopSignalGenerator()
{
	if (g_pApp->getConnectStatus() == DEVICE_NETWORK_CONNECTED)
	{
		g_pApp->getDeviceConnection()->unsubRTAMeter();
	}
//	m_pOverviewButton->setEnabled(true);
//	m_pBasicSettingButton->setEnabled(true);
//	m_pInputSettingButton->setEnabled(true);
//	m_pLogButton->setEnabled(true);

	refreshConnectedButton();
}


void MainWidget::onSpeakerImport()
{
//	CPi2000Data *pData = getCPi2000Data();

	QString localSpeakertDB	= MainApplication::getTempSpeakerDBFileName();
	qDebug() << localSpeakertDB;

	DeviceSocket *pDeviceSocket = g_pApp->getDeviceConnection();

#if 1

	if (pDeviceSocket == nullptr)
	{
		QMessageBox::warning(this, tr("Warning"), tr("Can't connect to CPi2000 device, please check the network connection!"), QMessageBox::Ok);	
		return;
	}

	if (g_pApp->getFtpFile(SPEAKER_DB_FILE, localSpeakertDB) == false)
	{
		QMessageBox::warning(this, "Read Device Setting Error", "Can't get speaker.db from CPi2000", QMessageBox::Ok);
		return;
	}
#endif

	QString directory = ".";
	QString importFileName = QFileDialog::getOpenFileName(
		this,
		"Open Speakers File",
		directory,
		tr("Speaker File(*.db);; All files (*.*)")
	);

	if (importFileName.isEmpty() != false)
	{
		return;
	}

	{
		QMessageBox::warning(this, tr("Invalid Speakers File"), tr("Failed to import speakers. We will finish it later."), tr("OK"));
		return;
	}

	SpeakerImportDialog dlg(this);
	dlg.setSpeakerDB(localSpeakertDB, importFileName);

	dlg.setModal(true);
	dlg.exec();
}


void MainWidget::retranslateUi()
{
	m_pOverviewWidget->retranslateUi();
	m_pBasicSettingWidget->retranslateUi();
	m_pInputSettingWidget->retranslateUi();
	m_pOutputSettingWidget->retranslateUi();
	m_pLogWidget->retranslateUi();

	m_pOverviewButton->setText(tr("OverView"));
	m_pBasicSettingButton->setText(tr("Basic Setting"));	
	m_pInputSettingButton->setText(tr("Input Setting"));	
	m_pOutputSettingButton->setText(tr("Output Setting"));	
	m_pLogButton->setText(tr("Log Page"));

	/* For Menu */
	m_pMenu1->setTitle(tr("File"));
	m_pActionLoadDeviceFile->setText(tr("Device File..."));
	m_pActionLoadFromUSB->setText(tr("from USB..."));
	m_pActionSaveDeviceFile->setText(tr("Device File..."));
	m_pActionSaveToUSB->setText(tr("to USB..."));
	m_pActionExit->setText(tr("Exit"));
    m_pMenu1->setTitle(tr("File"));
	m_pMenuOpen->setTitle(tr("Load"));
	m_pMenuSave->setTitle(tr("Save"));
	m_pActionSpeakerEditor->setText(tr("Speaker Editor..."));
	m_pActionImportSpeaker->setText(tr("Import Speaker..."));
	m_pActionAdjustTime->setText(tr("Calibrate Time..."));
	m_pActionUpgrade->setText(tr("Upgrade Firmware..."));
	m_pActionRestore->setText(tr("Restore Factory Settings..."));
	m_pSetIPAddress->setText(tr("Set IP Address..."));
	m_pMenu2->setTitle(tr("Tools"));
	m_pActionAbout->setText(tr("About"));
	m_pActionZoom->setText(tr("Zoom..."));
	m_pActionOption->setText(tr("Debug Option..."));

	m_pOutputLevelLabel->setText(tr("Output Level"));

	DEVICE_CONNECT_STATUS status = g_pApp->getConnectStatus();
	switch (status)
	{
	case DEVICE_SEARCHING:
	case DEVICE_CONNECTING:
	case DEVICE_DISCONNECTED:
		m_pConnectButton->setText(tr("Offline"));
		break;
	default:
		m_pConnectButton->setText(tr("Connected"));
	}
}


void MainWidget::showEvent(QShowEvent * /*event*/)
{
	CPi2000Data *pData = getCPi2000Data();
	static int count = 0;

	if (count == 0)
	{
		/* WM_INITUPDATE */
		QString localSpeakertDB	= MainApplication::getTempSpeakerDBFileName();
		QFile::remove(localSpeakertDB);
		QFile::copy(QString(":/binaries/speaker.db"), localSpeakertDB);
		QFile::setPermissions(localSpeakertDB, QFileDevice::WriteOwner | QFileDevice::WriteUser | QFileDevice::WriteGroup | QFileDevice::WriteOther);

		QString localPresetDB	= MainApplication::getTempPresetDBFileName();
		QFile::remove(localPresetDB);
		QFile::copy(QString(":/binaries/Presets.db"), localPresetDB);
		QFile::setPermissions(localPresetDB, QFileDevice::WriteOwner | QFileDevice::WriteUser | QFileDevice::WriteGroup | QFileDevice::WriteOther);

		/* Load Presets.db */
		{
			bool flag = pData->loadFromJSON(localPresetDB);
			if (flag == false)
			{
				QMessageBox::warning(this, QString("Internal Error"), QString("Failed to load presets.db"), QMessageBox::Ok);
			}
		}

		/* Load speaker.db */
		{
			g_database.setSpeakerDBFileName(localSpeakertDB);
			refreshSpeakerSetting();

			/* Set Load Detection */
			SPEAKER_CHANNEL channel[] = {SPEAKER_CHANNEL_L, SPEAKER_CHANNEL_CENTER, SPEAKER_CHANNEL_LS, SPEAKER_CHANNEL_RS, SPEAKER_CHANNEL_BLS, SPEAKER_CHANNEL_BRS, SPEAKER_CHANNEL_SW};
			/* L/R Speaker */
			SpeakerData speakerData;
			for (int i = 0; i < sizeof(channel) / sizeof(SPEAKER_CHANNEL); i++)
			{
				if (g_database.getSpeaker(pData->m_speakerName[channel[i]], speakerData) == true)
				{
					g_pMainWidget->setLoadDetectionName(channel[i], speakerData.m_speakerType);
				}
			}
		}

		refresh();
	}
	count++;
}