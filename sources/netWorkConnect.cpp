/*
#include <QThread>
#include <QFile>
#include <QDir>
#include <QTextStream>
#include <QTime>
#include <QVector>
#include <qdebug.h>

#include "networkConnect.h"
#include "MainApplication.h"
#include "commonLib.h"

#include <TCHAR.H> 
#include "Shlwapi.h"
#include <windows.h>
#include <setupapi.h>
#include <devguid.h>
*/
#include "networkConnect.h"
#include <QHostAddress>
#include <QDebug>
#include "DeviceManagerThread.h"

#define CONNECT_TIME_OUT  1000
#define DISCONNECT_TIME_OUT  2000

NetworkConnect::NetworkConnect()
{
    // Initialize the system
	m_pSocket = new QTcpSocket(this);
    connect(m_pSocket, SIGNAL(connected()),	this, SLOT(onNetworkConnected()));
    connect(m_pSocket, SIGNAL(disconnected()),	this, SLOT(onNetworkDisconnected()));
}

NetworkConnect::~NetworkConnect()
{
	if (m_connectFlag == true)
	{
		m_pSocket->close();
	}

	delete m_pSocket;
}

//-----------------------------------------------------------------------------
// Make the connection to the device
//
bool NetworkConnect::connectDevice(UINT32 ipAddr, UINT32 port)
{
	QHostAddress hostAddr(ipAddr);

	/* If the socket is connected, we will disconnect it first */
	if (m_pSocket->state() == QAbstractSocket::ConnectedState)
	{
		m_pSocket->disconnectFromHost();
        m_pSocket->waitForDisconnected(DISCONNECT_TIME_OUT);
	}

	m_pSocket->connectToHost( hostAddr, UINT16(port));
	if (m_pSocket->waitForConnected(CONNECT_TIME_OUT)) 
	{
		// socket is connected.
		return (true);
	}
	else 
	{
		//socket is not connected.
		return (false);
	}
}

bool NetworkConnect::findDevice()
{
	return (m_pSocket->state() == QAbstractSocket::ConnectedState);
}

//-----------------------------------------------------------------------------
// Close the connection
//
void NetworkConnect::disconnect()
{
	if (m_pSocket->state() == QAbstractSocket::ConnectedState)
	{
		m_pSocket->disconnectFromHost();
        m_pSocket->waitForDisconnected(DISCONNECT_TIME_OUT);
	}
}

bool NetworkConnect::sendData(BYTE *pBuffer, int nLen)
{
	bool ret = false;

	if (m_pSocket->state() == QAbstractSocket::ConnectedState)
	{
		if (m_pSocket->write((const char *)pBuffer, nLen) == nLen)
		{
			ret = true;
		}
	}

	if (ret != true)
	{
		qDebug() << "Warning: sendData failed!";
	}

	return (ret);
}

//-----------------------------------------------------------------------------
// Send a packet of data
//
int NetworkConnect::recvData(BYTE *pBuffer, int nMaxLen)
{
	if (m_pSocket->state() != QAbstractSocket::ConnectedState)
	{
		/* Socket is not connected */
		return 0;
	}

	int bytesRead = m_pSocket->read((char *)pBuffer, nMaxLen);
	return (bytesRead);
}

void NetworkConnect::onNetworkConnected()
{
	qDebug() << "NetworkConnect::onDeviceConnected called here..." << endl;
}

void NetworkConnect::onNetworkDisconnected()
{
//	qDebug() << "NetworkConnect::onConnectBroken called here..." << endl;
	g_pDeviceMgrThread->emit networkDisconnect();
}