#include "plotXover.h"
#include "BesselAnalogLUT.h"

#include <math.h>

void calcButterworthSOS(double *s2, double *s, int slope, int biquadNumber);	
void calcBesselSOS(double *A, double *B, int filterOrder, int biquadNumber, bool isLowpass);
void bltSOS(bool isLowpass, double alpha, double A, double B, double gain, int filterOrder, int biquadNumber, struct sBiquadCoefs *pFilter);




void FilterDesignSOS(double freq, double gaindB, double q_or_slope, double sampleRate,
				eFilterTypeSOS type, struct sBiquadCoefs *pFilter, int biquadNumber)
{
//	double q = q_or_slope;
	double slope = q_or_slope;
	int filterOrder = (int)(slope/-6.0f);
		//double test = slope/-6.0f - filterOrder;
		//_ASSERT(test==0); //Make sure slope is an integer multiple of -6
	double A = 0.0f;
	double B = 0.0f;					
	bool isLowpass = true; //Set to false if highpass filter

    switch (type)
    {
        case kLowPass1:
			{
				// First-order low-pass filter
				double alpha   = tan((double)(M_PI)*freq/sampleRate);
				double onePlusAlphaInv = 1.0 / (1.0+alpha);
				double gain = pow(10.0,gaindB*0.05);

				pFilter->b0   = alpha*onePlusAlphaInv*gain;
				pFilter->b1   = pFilter->b0;
				pFilter->b2   = 0.0f;
				pFilter->a1   = (1.0f-alpha)*onePlusAlphaInv;
				pFilter->a2   = 0.0f;
			}
            break;
        case kLowPass2:
			{
				// Second-order low-pass filter
				double alpha = tan((double)(M_PI)*freq/sampleRate);
				double alpha_sq = alpha*alpha;
				double beta = 1.0 / (alpha_sq + (double)(M_SQRT2)*alpha + 1.0);
				double gain = pow(10.0,gaindB*0.05);

				pFilter->b0   = beta*alpha_sq*gain;
				pFilter->b1   = 2.0f*(pFilter->b0);
				pFilter->b2   = pFilter->b0;
				pFilter->a1   = -beta*2.0f*(alpha_sq-1.0f);
				pFilter->a2   = -beta*(alpha_sq - (double)(M_SQRT2)*alpha + 1.0f);
			}
            break;
       case kHighPass1:
			{
				// First-order high-pass filter
				double alpha = tan((double)(M_PI)*freq/sampleRate);
				double beta = (1.0 - alpha)/(1.0 + alpha);
				double gain = pow(10.0,gaindB*0.05);

				pFilter->b0   = (1.0 + beta)*0.5*gain;
				pFilter->b1   = -pFilter->b0;
				pFilter->b2   = 0.0;
				pFilter->a1   = beta;
				pFilter->a2   = 0.0;
			}
            break;

        case kHighPass2:
			{
				// Second-order high-pass filter
				double omega = 2.0*(double)(M_PI)*freq/sampleRate;
				double alpha = sin(omega)*(double)(M_SQRT1_2); //sin(omega) / sqrt(2)
				double beta = cos(omega);
				double onePlusAlphaInv = 1.0 / (1.0+alpha);
				double gain = pow(10.0,gaindB*0.05);

				pFilter->b0   = 0.5*(1.0f+beta) * onePlusAlphaInv * gain;
				pFilter->b1   = -2.0*(pFilter->b0);
				pFilter->b2   = pFilter->b0;
				pFilter->a1   = 2.0*beta * onePlusAlphaInv;
				pFilter->a2   = -(1.0-alpha) * onePlusAlphaInv;
			}
            break;
		case kLowPassNSOS:
			{
				isLowpass = true;
				double gain = 1.0;
				double alpha = tan( (double)(M_PI) * freq / sampleRate); // tanf(pi*f0/Fs)*omega0;
				calcButterworthSOS(&A, &B, filterOrder, biquadNumber); // Get the analog SOS coefficients
				bltSOS(isLowpass, alpha, A, B, gain, filterOrder, biquadNumber, pFilter); //Get the digital filter coefficients
			}
			break; 
		case kHighPassNSOS:
			{
				isLowpass = false;
				double gain = 1.0f;
				double alpha = tan( (double)(M_PI) * freq / sampleRate); // tanf(pi*f0/Fs)*omega0;
				calcButterworthSOS(&A, &B, filterOrder, biquadNumber); // Get the analog SOS coefficients
				bltSOS(isLowpass, alpha, A, B, gain, filterOrder, biquadNumber, pFilter); //Get the digital filter coefficients
			}
			break; 
		case kLowPassBesselNSOS:	
			{
				isLowpass = true;
				int numBiquads = 0;

				if(filterOrder & 1) //Odd filter order
					numBiquads = filterOrder/2 + 1;
				else
					numBiquads = filterOrder/2;
	
				double gain = pow(BesselGains[filterOrder-1], 1.0/numBiquads);
				double f0NormFactor = f0NormFactors[filterOrder-1];
				double alpha = 1/f0NormFactor * tan( (double)(M_PI) * (freq) / sampleRate); // tanf(pi*f0/Fs)*omega0;
				calcBesselSOS(&A, &B, filterOrder, biquadNumber, isLowpass); //Get the analog SOS coefficients	
				bltSOS(isLowpass, alpha, A, B, gain, filterOrder, biquadNumber, pFilter); //Get the digital filter coefficients
			}
			break; 
		case kHighPassBesselNSOS:
			{
				isLowpass = false;
				double gain = 1.0; //Bessel Gains are 1 for highpass
				double f0NormFactor = f0NormFactors[filterOrder-1];
				double alpha = f0NormFactor * tan( (double)(M_PI) * (freq) / sampleRate); // tanf(pi*f0/Fs)*omega0;
				calcBesselSOS(&A, &B, filterOrder, biquadNumber, isLowpass); //Get the analog SOS coefficients	
				bltSOS(isLowpass, alpha, A, B, gain, filterOrder, biquadNumber, pFilter); //Get the digital filter coefficients
			}
			break;
        default:
            //_ASSERT(false);
			break;
    }
}

void calcButterworthSOS(double *A, double *B, int filterOrder, int biquadNumber) 
//Calculates normalized analog Butterworth SOS polynomials
{
	int k = 0;
	if(filterOrder & 1) //Odd filter order
	{
		if (biquadNumber == 0)
		{
			*A = 1.0;
			*B = 1.0;
		}
		else
		{
			k = biquadNumber;
			*A = -2.0*cos( (2*k+filterOrder-1) * (double)M_PI_2 / filterOrder);
			*B = 1.0;
		}
	}
	else				//Even filter order
	{
		k = biquadNumber+1;
		*A = -2.0*cos( (2*k+filterOrder-1) * (double)M_PI_2 / filterOrder);
		*B = 1.0;
	}
}

void calcBesselSOS(double *A, double *B, int filterOrder, int biquadNumber, bool isLowpass) 
//Calculates normalized analog Bessel SOS polynomials
{		
	switch (filterOrder)
	{
		case 1:
			{
				*A = Bessel1[1];
				*B = Bessel1[2];
			}
			break; 
		case 2:
			{
				*A = Bessel2[1];
				*B = Bessel2[2];
			}
			break; 
		case 3:
			{
				*A = Bessel3[biquadNumber][1];
				*B = Bessel3[biquadNumber][2];
			}
			break; 
		case 4:
			{
				*A = Bessel4[biquadNumber][1];
				*B = Bessel4[biquadNumber][2];
			}
			break; 
		case 5:
			{
				*A = Bessel5[biquadNumber][1];
				*B = Bessel5[biquadNumber][2];
			}
			break; 
		case 6:
			{
				*A = Bessel6[biquadNumber][1];
				*B = Bessel6[biquadNumber][2];
			}
			break; 
		case 7:
			{
				*A = Bessel7[biquadNumber][1];
				*B = Bessel7[biquadNumber][2];
			}
			break; 
		case 8:
			{
				*A = Bessel8[biquadNumber][1];
				*B = Bessel8[biquadNumber][2];
			}
			break; 
		default:
			//_ASSERT(false);
			break;
	}
	if(isLowpass==false) //For highpass the coefficients are reversed then normalized to Besselx[biquadNumber][0]
	{
		*A = *A / *B;
		*B = 1.0f / *B;
	}
}

void bltSOS(bool isLowpass, double alpha, double A, double B, double gain, int filterOrder, int biquadNumber, struct sBiquadCoefs *pFilter)
//bltSOS takes the analog SOS pole coefficients A and B and transforms them into the necessary digital SOS coefficients
//Uses the bilinear transform (blt) with compensation for frequency warping
//For lowpass:  1 / (s^2 + A*s + B)    -->  (b0 + b1*z^-1 + b2*z^-2) / (1 - a1*z^-1 - a2*z^-2)
//For highpass: s^2 / (s^2 + A*s + B)  -->  (b0 + b1*z^-1 + b2*z^-2) / (1 - a1*z^-1 - a2*z^-2)
{
	double alpha2 = alpha * alpha;
	if(isLowpass==true)	//Lowpass filter
	{
		if((filterOrder & 1) && (biquadNumber==0)) //Odd filter order and 1st biquad means we need a 1st order section
		{
			double Balpha = B * alpha;
			double beta = 1 / (1.0f + Balpha);
			pFilter->b0   = gain * alpha * beta;
			pFilter->b1   = pFilter->b0;
			pFilter->b2   = 0.0f;
			pFilter->a1   = (1.0f-Balpha) * beta;
			pFilter->a2   = 0.0f;
		}
		else									//Standard 2nd order section
		{	
			double Aalpha = A * alpha;
			double Balpha2 = B * alpha2;
			double beta = 1 / (1 + Aalpha + Balpha2);
			pFilter->b0   = gain * alpha2 * beta; 
			pFilter->b1   = 2*(pFilter->b0);
			pFilter->b2   = pFilter->b0;
			pFilter->a1   = -2*(-1+Balpha2) * beta;
			pFilter->a2   = -(1-Aalpha+Balpha2) * beta;
		}
	}
	else				//Highpass filter
	{
		if((filterOrder & 1) && (biquadNumber==0)) //Odd filter order and 1st biquad means we need a 1st order section
		{
			double Balpha = B * alpha;
			double beta = 1 / (1.0f + Balpha);
			pFilter->b0   = gain * beta;
			pFilter->b1   = -(pFilter->b0);
			pFilter->b2   = 0.0f;
			pFilter->a1   = (1.0f-Balpha) * beta;
			pFilter->a2   = 0.0f;
		}
		else									//Standard 2nd order section
		{	
			double Aalpha = A * alpha;
			double Balpha2 = B * alpha2;
			double beta = 1 / (1 + Aalpha + Balpha2);
			pFilter->b0   = gain * beta; 
			pFilter->b1   = -2*(pFilter->b0);
			pFilter->b2   = pFilter->b0;
			pFilter->a1   = -2*(-1+Balpha2) * beta;
			pFilter->a2   = -(1-Aalpha+Balpha2) * beta;
		}
	}
}

plotXover::plotXover(void)
{
}


plotXover::~plotXover(void)
{
}


void plotXover::setLPFParameter(bool enable, int slope, double freq){

	
	double sampleRate 	= 48000;
//	double moddedFreq = 0.0f;

	for(int i=0;i<4;i++)
	 biquad[i]	= CBiquad::_bypassCoefs;


	if(enable)
	{
		switch(slope)
		{
		case BS_6:
			// First order Bessel lowpass filter
			FilterDesignSOS(freq,0,-6,sampleRate,kLowPassBesselNSOS, &biquad[0], 0);
			break;

		case BS_12:
			// Second order Bessel lowpass filter
			FilterDesignSOS(freq,0,-12,sampleRate,kLowPassBesselNSOS, &biquad[0], 0);
			break;

		case BS_18:
			// Third order Bessel lowpass filter
			FilterDesignSOS(freq,0,-18,sampleRate,kLowPassBesselNSOS, &biquad[0], 0);
			FilterDesignSOS(freq,0,-18,sampleRate,kLowPassBesselNSOS, &biquad[1], 1);
			break;

		case BS_24:
			// Fourth order Bessel lowpass filter
			FilterDesignSOS(freq,0,-24,sampleRate,kLowPassBesselNSOS, &biquad[0], 0);
			FilterDesignSOS(freq,0,-24,sampleRate,kLowPassBesselNSOS, &biquad[1], 1);
			break;

		case BS_30:
			// Fifth order Bessel lowpass filter
			FilterDesignSOS(freq,0,-30,sampleRate,kLowPassBesselNSOS, &biquad[0], 0);
			FilterDesignSOS(freq,0,-30,sampleRate,kLowPassBesselNSOS, &biquad[1], 1);
			FilterDesignSOS(freq,0,-30,sampleRate,kLowPassBesselNSOS, &biquad[2], 2);
			break;

		case BS_36:
			// Sixth order Bessel lowpass filter
			FilterDesignSOS(freq,0,-36,sampleRate,kLowPassBesselNSOS, &biquad[0], 0);
			FilterDesignSOS(freq,0,-36,sampleRate,kLowPassBesselNSOS, &biquad[1], 1);
			FilterDesignSOS(freq,0,-36,sampleRate,kLowPassBesselNSOS, &biquad[2], 2);
			break;

		case BS_42:
			// Seveth order Bessel lowpass filter
			FilterDesignSOS(freq,0,-42,sampleRate,kLowPassBesselNSOS, &biquad[0], 0);
			FilterDesignSOS(freq,0,-42,sampleRate,kLowPassBesselNSOS, &biquad[1], 1);
			FilterDesignSOS(freq,0,-42,sampleRate,kLowPassBesselNSOS, &biquad[2], 2);
			FilterDesignSOS(freq,0,-42,sampleRate,kLowPassBesselNSOS, &biquad[3], 3);
			break;

		case BS_48:
			// Eighth order Bessel lowpass filter
			FilterDesignSOS(freq,0,-48,sampleRate,kLowPassBesselNSOS, &biquad[0], 0);
			FilterDesignSOS(freq,0,-48,sampleRate,kLowPassBesselNSOS, &biquad[1], 1);
			FilterDesignSOS(freq,0,-48,sampleRate,kLowPassBesselNSOS, &biquad[2], 2);
			FilterDesignSOS(freq,0,-48,sampleRate,kLowPassBesselNSOS, &biquad[3], 3);
			break;

		case BW_6:
			// First order Butterworth lowpass filter
			FilterDesignSOS(freq,0,0,sampleRate,kLowPass1, &biquad[0],0);
			break;

		case BW_12:
			// Second order Butterworth lowpass filter
			FilterDesignSOS(freq,0,0,sampleRate,kLowPass2, &biquad[0],0);
			break;

		case BW_18:
			// Third order Butterworth lowpass filter
			// Made by implementing normalized Butterworth 2nd order sections
			FilterDesignSOS(freq,0,-18,sampleRate,kLowPassNSOS, &biquad[0], 0);
			FilterDesignSOS(freq,0,-18,sampleRate,kLowPassNSOS, &biquad[1], 1);
			break;

		case BW_24:
			// Fourth order Butterworth lowpass filter
			// Made by implementing normalized Butterworth 2nd order sections
			FilterDesignSOS(freq,0,-24,sampleRate,kLowPassNSOS, &biquad[0], 0);
			FilterDesignSOS(freq,0,-24,sampleRate,kLowPassNSOS, &biquad[1], 1);
			break;

		case BW_30:
			// Fifth order Butterworth lowpass filter
			// Made by implementing normalized Butterworth 2nd order sections
			//FilterDesign(freq,0,0,sampleRate,kLowPass1, &biquad[0]);
			FilterDesignSOS(freq,0,-30,sampleRate,kLowPassNSOS, &biquad[0], 0);
			FilterDesignSOS(freq,0,-30,sampleRate,kLowPassNSOS, &biquad[1], 1);
			FilterDesignSOS(freq,0,-30,sampleRate,kLowPassNSOS, &biquad[2], 2);
			break;

		case BW_36:
			// Sixth order Butterworth lowpass filter
			// Made by implementing normalized Butterworth 2nd order sections
			FilterDesignSOS(freq,0,-36,sampleRate,kLowPassNSOS, &biquad[0], 0);
			FilterDesignSOS(freq,0,-36,sampleRate,kLowPassNSOS, &biquad[1], 1);
			FilterDesignSOS(freq,0,-36,sampleRate,kLowPassNSOS, &biquad[2], 2);
			break;

		case BW_42:
			// Seventh order Butterworth lowpass filter
			// Made by implementing normalized Butterworth 2nd order sections
			FilterDesignSOS(freq,0,-42,sampleRate,kLowPassNSOS, &biquad[0], 0);
			FilterDesignSOS(freq,0,-42,sampleRate,kLowPassNSOS, &biquad[1], 1);
			FilterDesignSOS(freq,0,-42,sampleRate,kLowPassNSOS, &biquad[2], 2);
			FilterDesignSOS(freq,0,-42,sampleRate,kLowPassNSOS, &biquad[3], 3);
			break;

		case BW_48:
			// Eighth order Butterworth lowpass filter
			// Made by implementing normalized Butterworth 2nd order sections
			FilterDesignSOS(freq,0,-48,sampleRate,kLowPassNSOS, &biquad[0], 0);
			FilterDesignSOS(freq,0,-48,sampleRate,kLowPassNSOS, &biquad[1], 1);
			FilterDesignSOS(freq,0,-48,sampleRate,kLowPassNSOS, &biquad[2], 2);
			FilterDesignSOS(freq,0,-48,sampleRate,kLowPassNSOS, &biquad[3], 3);
			break;

		case LR_12:
			// Second order Linkwitz Riley lowpass filter
			// (Two cascaded first order Butterworth filters)
			FilterDesignSOS(freq,0,0,sampleRate,kLowPass1, &biquad[0],0);
			biquad[1] = biquad[0];
			break;

		case LR_24:
			// Forth order Linkwitz Riley lowpass filter
			// (Two cascaded second order Butterworth filters)
			FilterDesignSOS(freq,0,0,sampleRate,kLowPass2, &biquad[0],0);
			biquad[1] = biquad[0];
			break;

		case LR_36:
			// Sixth order Linkwitz Riley lowpass filter
			// (Two cascaded third order Butterworth filters)
			FilterDesignSOS(freq,0,-18,sampleRate,kLowPassNSOS, &biquad[0], 0);
			FilterDesignSOS(freq,0,-18,sampleRate,kLowPassNSOS, &biquad[1], 1);
			biquad[2] = biquad[0];
			biquad[3] = biquad[1];
			break;

		case LR_48:
			// Eighth order Linkwitz Riley lowpass filter
			// (Two cascaded Fourth order Butterworth filters)
			FilterDesignSOS(freq,0,-24,sampleRate,kLowPassNSOS, &biquad[0], 0);
			FilterDesignSOS(freq,0,-24,sampleRate,kLowPassNSOS, &biquad[1], 1);
			biquad[2] = biquad[0];
			biquad[3] = biquad[1];
			break;

		default:
			break;
		}
	}
}


void plotXover::setHPFParameter(bool enable, int slope, double freq){

	double sampleRate 	= 48000;
//	double moddedFreq = 0.0f;
//	int bypass =enable;

	 biquad[0]	= CBiquad::_bypassCoefs;
	 biquad[1] 	= CBiquad::_bypassCoefs;
	 biquad[2] 	= CBiquad::_bypassCoefs;
	 biquad[3] 	= CBiquad::_bypassCoefs;

	if(enable)
	{
		switch(slope)
		{
		case BS_6:
			// First order Bessel highpass filter
			FilterDesignSOS(freq,0,-6,sampleRate,kHighPassBesselNSOS, &biquad[0], 0);
			break;

		case BS_12:
			// Second order Bessel highpass filter
			FilterDesignSOS(freq,0,-12,sampleRate,kHighPassBesselNSOS, &biquad[0], 0);
			break;

		case BS_18:
			// Third order Bessel highpass filter
			FilterDesignSOS(freq,0,-18,sampleRate,kHighPassBesselNSOS, &biquad[0], 0);
			FilterDesignSOS(freq,0,-18,sampleRate,kHighPassBesselNSOS, &biquad[1], 1);
			break;

		case BS_24:
			// Fourth order Bessel highpass filter
			FilterDesignSOS(freq,0,-24,sampleRate,kHighPassBesselNSOS, &biquad[0], 0);
			FilterDesignSOS(freq,0,-24,sampleRate,kHighPassBesselNSOS, &biquad[1], 1);
			break;

		case BS_30:
			// Fifth order Bessel highpass filter
			FilterDesignSOS(freq,0,-30,sampleRate,kHighPassBesselNSOS, &biquad[0], 0);
			FilterDesignSOS(freq,0,-30,sampleRate,kHighPassBesselNSOS, &biquad[1], 1);
			FilterDesignSOS(freq,0,-30,sampleRate,kHighPassBesselNSOS, &biquad[2], 2);
			break;

		case BS_36:
			// Sixth order Bessel highpass filter
			FilterDesignSOS(freq,0,-36,sampleRate,kHighPassBesselNSOS, &biquad[0], 0);
			FilterDesignSOS(freq,0,-36,sampleRate,kHighPassBesselNSOS, &biquad[1], 1);
			FilterDesignSOS(freq,0,-36,sampleRate,kHighPassBesselNSOS, &biquad[2], 2);
			break;

		case BS_42:
			// Seveth order Bessel highpass filter
			FilterDesignSOS(freq,0,-42,sampleRate,kHighPassBesselNSOS, &biquad[0], 0);
			FilterDesignSOS(freq,0,-42,sampleRate,kHighPassBesselNSOS, &biquad[1], 1);
			FilterDesignSOS(freq,0,-42,sampleRate,kHighPassBesselNSOS, &biquad[2], 2);
			FilterDesignSOS(freq,0,-42,sampleRate,kHighPassBesselNSOS, &biquad[3], 3);
			break;

		case BS_48:
			// Eighth order Bessel highpass filter
			FilterDesignSOS(freq,0,-48,sampleRate,kHighPassBesselNSOS, &biquad[0], 0);
			FilterDesignSOS(freq,0,-48,sampleRate,kHighPassBesselNSOS, &biquad[1], 1);
			FilterDesignSOS(freq,0,-48,sampleRate,kHighPassBesselNSOS, &biquad[2], 2);
			FilterDesignSOS(freq,0,-48,sampleRate,kHighPassBesselNSOS, &biquad[3], 3);
			break;

		case BW_6:
			// First order Butterworth highpass filter
			FilterDesignSOS(freq,0,0,sampleRate,kHighPass1, &biquad[0],0);
			break;

		case BW_12:
			// Second order Butterworth highpass filter
			FilterDesignSOS(freq,0,0,sampleRate,kHighPass2, &biquad[0],0);
			break;

		case BW_18:
			// Third order Butterworth highpass filter
			// Made by implementing normalized Butterworth 2nd order sections
			FilterDesignSOS(freq,0,-18,sampleRate,kHighPassNSOS, &biquad[0], 0);
			FilterDesignSOS(freq,0,-18,sampleRate,kHighPassNSOS, &biquad[1], 1);
			break;

		case BW_24:
			// Fourth order Butterworth highpass filter
			// Made by implementing normalized Butterworth 2nd order sections
			FilterDesignSOS(freq,0,-24,sampleRate,kHighPassNSOS, &biquad[0], 0);
			FilterDesignSOS(freq,0,-24,sampleRate,kHighPassNSOS, &biquad[1], 1);
			break;

		case BW_30:
			// Fifth order Butterworth highpass filter
			// Made by implementing normalized Butterworth 2nd order sections
			FilterDesignSOS(freq,0,-30,sampleRate,kHighPassNSOS, &biquad[0], 0);
			FilterDesignSOS(freq,0,-30,sampleRate,kHighPassNSOS, &biquad[1], 1);
			FilterDesignSOS(freq,0,-30,sampleRate,kHighPassNSOS, &biquad[2], 2);
			break;

		case BW_36:
			// Sixth order Butterworth highpass filter
			// Made by implementing normalized Butterworth 2nd order sections
			FilterDesignSOS(freq,0,-36,sampleRate,kHighPassNSOS, &biquad[0], 0);
			FilterDesignSOS(freq,0,-36,sampleRate,kHighPassNSOS, &biquad[1], 1);
			FilterDesignSOS(freq,0,-36,sampleRate,kHighPassNSOS, &biquad[2], 2);
			break;

		case BW_42:
			// Seventh order Butterworth highpass filter
			// Made by implementing normalized Butterworth 2nd order sections
			FilterDesignSOS(freq,0,-42,sampleRate,kHighPassNSOS, &biquad[0], 0);
			FilterDesignSOS(freq,0,-42,sampleRate,kHighPassNSOS, &biquad[1], 1);
			FilterDesignSOS(freq,0,-42,sampleRate,kHighPassNSOS, &biquad[2], 2);
			FilterDesignSOS(freq,0,-42,sampleRate,kHighPassNSOS, &biquad[3], 3);
			break;

		case BW_48:
			// Eighth order Butterworth highpass filter
			// Made by implementing normalized Butterworth 2nd order sections
			FilterDesignSOS(freq,0,-48,sampleRate,kHighPassNSOS, &biquad[0], 0);
			FilterDesignSOS(freq,0,-48,sampleRate,kHighPassNSOS, &biquad[1], 1);
			FilterDesignSOS(freq,0,-48,sampleRate,kHighPassNSOS, &biquad[2], 2);
			FilterDesignSOS(freq,0,-48,sampleRate,kHighPassNSOS, &biquad[3], 3);
			break;

		case LR_12:
			// Second order Linkwitz Riley highpass filter
			// (Two cascaded first order Butterworth filters)
			FilterDesignSOS(freq,0,0,sampleRate,kHighPass1, &biquad[0],0);
			biquad[1] = biquad[0];
			break;

		case LR_24:
			// Forth order Linkwitz Riley highpass filter
			// (Two cascaded second order Butterworth filters)
			FilterDesignSOS(freq,0,0,sampleRate,kHighPass2, &biquad[0],0);
			biquad[1] = biquad[0];
			break;

		case LR_36:
			// Sixth order Linkwitz Riley highpass filter
			// (Two cascaded third order Butterworth filters)
			FilterDesignSOS(freq,0,-18,sampleRate,kHighPassNSOS, &biquad[0], 0);
			FilterDesignSOS(freq,0,-18,sampleRate,kHighPassNSOS, &biquad[1], 1);
			biquad[2] = biquad[0];
			biquad[3] = biquad[1];
			break;

		case LR_48:
			// Eighth order Linkwitz Riley highpass filter
			// (Two cascaded Fourth order Butterworth filters)
			FilterDesignSOS(freq,0,-24,sampleRate,kHighPassNSOS, &biquad[0], 0);
			FilterDesignSOS(freq,0,-24,sampleRate,kHighPassNSOS, &biquad[1], 1);
			biquad[2] = biquad[0];
			biquad[3] = biquad[1];
			break;

		default:
			break;
		}
	}
}


void plotXover::plotCurve(double *x, double *y, long pointslength){

	double w;
	double numerator;
	double denominator;
	double magnitude;
    double cs,cs2;
	double temp[4][8000];

	for(int j=0;j<4;j++){
		for (int i = 0; i < pointslength; i++) {
			w = 2.0L*M_PI*x[i] / 48000; //x : frequency point
			cs=cos(w) ;
			cs2=cos(2*w);

			//numerator = B0*B0 + B1*B1 + B2*B2 + 2.0*(B0*B1 + B1*B2)*cs+ 2.0*B0*B2*cs2;
			//denominator = 1.0 + A1*A1 + A2*A2 + 2.0*(A1 + A1*A2)*cs + 2.0*A2*cs2;	

			numerator = biquad[j].b0*biquad[j].b0 + biquad[j].b1*biquad[j].b1 + biquad[j].b2*biquad[j].b2 + 2.0*(biquad[j].b0*biquad[j].b1 + biquad[j].b1*biquad[j].b2)*cs+ 2.0*biquad[j].b0*biquad[j].b2*cs2;
			 //a1,a2 need to add minus sign , so the derived equation is liked this
			denominator = 1.0 + biquad[j].a1*biquad[j].a1 + biquad[j].a2*biquad[j].a2 + 2.0*(-biquad[j].a1 + biquad[j].a1*biquad[j].a2)*cs - 2.0*biquad[j].a2*cs2;

			
			magnitude = (numerator / denominator);
			y[i]=temp[j][i] = 10*log10(magnitude); //FrequencyResponse
		}
	}

	for (int i = 0; i < pointslength; i++) {
		y[i]=temp[0][i]+temp[1][i]+temp[2][i]+temp[3][i];
	}
}