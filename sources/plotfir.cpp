#include "plotfir.h"

#include <stdio.h>
#include <new>

#define NUM_FREQ_ERR_PTS  1000    // these are only used in the FIRFreqError function.
#define dNUM_FREQ_ERR_PTS 1000.0

//void firwin(double* h, int n,int band,double fln,double fhn,int wn);
//static double window(int type,int n,int i,double beta);
//static double kaiser(int i,int n,double beta);
//static double bessel0(double x );

// This gets used with the Kaiser window.
double Bessel(double x)
{
 double Sum=0.0, XtoIpower;
 int i, j, Factorial;
 for(i=1; i<10; i++)
  {
   XtoIpower = pow(x/2.0, (double)i);
   Factorial = 1;
   for(j=1; j<=i; j++)Factorial *= j;
   Sum += pow(XtoIpower / (double)Factorial, 2.0);
  }
 return(1.0 + Sum);
}

// Goertzel is essentially a single frequency DFT, but without phase information.
// Its simplicity allows it to run about 3 times faster than a single frequency DFT.
// It is typically used to find a tone embedded in a signal. A DTMF tone for example.
// 256 pts in 6 us
double Goertzel(double *Samples, int N, double Omega)
{
 int j;
 double Reg0, Reg1, Reg2;        // 3 shift registers
 double CosVal, Mag;
 Reg1 = Reg2 = 0.0;

 CosVal = 2.0 * cos(M_PI * Omega );
 for (j=0; j<N; j++)
  {
   Reg0 = Samples[j] + CosVal * Reg1 - Reg2;
   Reg2 = Reg1;  // Shift the values.
   Reg1 = Reg0;
  }
 Mag = Reg2 * Reg2 + Reg1 * Reg1 - CosVal * Reg1 * Reg2;

 if(Mag > 0.0)Mag = sqrt(Mag);
 else Mag = 1.0E-12;

 return(Mag);
}


// Rectangular Windowed FIR. The equations used here are developed in numerous textbooks.
void plotFir::RectWinFIR(double *FirCoeff, int NumTaps, int PassType, double OmegaC, double BW)
{
 int j;
 double Arg, OmegaLow, OmegaHigh;

 switch(PassType)
  {
   case LOWPASS:    // Low Pass
	for(j=0; j<NumTaps; j++)
     {
      Arg = (double)j - (double)(NumTaps-1) / 2.0;
      FirCoeff[j] = OmegaC * Sinc(OmegaC * Arg * M_PI);
     }
    break;

   case HIGHPASS:     // High Pass
    if(NumTaps % 2 == 1) // Odd tap counts
     {
      for(j=0; j<NumTaps; j++)
       {
        Arg = (double)j - (double)(NumTaps-1) / 2.0;
        FirCoeff[j] = Sinc(Arg * M_PI) - OmegaC * Sinc(OmegaC * Arg * M_PI);
       }
     }

    else  // Even tap counts
      {
       for(j=0; j<NumTaps; j++)
        {
         Arg = (double)j - (double)(NumTaps-1) / 2.0;
         if(Arg == 0.0)FirCoeff[j] = 0.0;
         else FirCoeff[j] = cos(OmegaC * Arg * M_PI) / M_PI / Arg  + cos(Arg * M_PI);
        }
      }
   break;

   case BANDPASS:   // Band Pass
    OmegaLow  = OmegaC - BW/2.0;
    OmegaHigh = OmegaC + BW/2.0;
	for(j=0; j<NumTaps; j++)
     {
      Arg = (double)j - (double)(NumTaps-1) / 2.0;
      if(Arg == 0.0)FirCoeff[j] = 0.0;
      else FirCoeff[j] =  ( cos(OmegaLow * Arg * M_PI) - cos(OmegaHigh * Arg * M_PI) ) / M_PI / Arg ;
     }
   break;

   case BANDSTOP:  // Notch,  if NumTaps is even, the response at Pi is attenuated.
    OmegaLow  = OmegaC - BW/2.0;
    OmegaHigh = OmegaC + BW/2.0;
	for(j=0; j<NumTaps; j++)
     {
      Arg = (double)j - (double)(NumTaps-1) / 2.0;
      FirCoeff[j] =  Sinc(Arg * M_PI) - OmegaHigh * Sinc(OmegaHigh * Arg * M_PI) - OmegaLow * Sinc(OmegaLow * Arg * M_PI);
     }
   break;

   case ALLPASS: // All Pass, this is trivial, but it shows how an fir all pass (delay) can be done.
	for(j=0; j<NumTaps; j++)FirCoeff[j] = 0.0;
    FirCoeff[(NumTaps-1) / 2] = 1.0;
    break;
  }
 // Now use the FIRFilterWindow() function to reduce the sinc(x) effects.
}

// Used to reduce the sinc(x) effects on a set of FIR coefficients. This will, unfortunately,
// widen the filter's transition band, but the stop band attenuation will improve dramatically.
void plotFir::FIRFilterWindow(double *FIRCoeff, int N, int WindowType, double Beta)
{
 if(WindowType == wtNONE) return;

 int j;
 double dN, *WinCoeff;

 if(Beta < 0.0)Beta = 0.0;
 if(Beta > 10.0)Beta = 10.0;

 WinCoeff  = new(std::nothrow) double[N+2];
 if(WinCoeff == NULL)
  {
   // ShowMessage("Failed to allocate memory in WindowData() ");
   return;
  }

 // Calculate the window for N/2 points, then fold the window over (at the bottom).
 dN = N + 1; // a double
 if(WindowType == wtKAISER)
  {
   double Arg;
   for(j=0; j<N; j++)
	{
	 Arg = Beta * sqrt(1.0 - pow( ((double)(2*j+2) - dN) / dN, 2.0) );
	 WinCoeff[j] = Bessel(Arg) / Bessel(Beta);
	}
  }

 else if(WindowType == wtSINC)  // Lanczos
  {
   for(j=0; j<N; j++)WinCoeff[j] = Sinc((double)(2*j+1-N)/dN * M_PI );
   for(j=0; j<N; j++)WinCoeff[j] = pow(WinCoeff[j], Beta);
  }

 else if(WindowType == wtSINE)  // Hanning if Beta = 2
  {
   for(j=0; j<N/2; j++)WinCoeff[j] = sin((double)(j+1) * M_PI / dN);
   for(j=0; j<N/2; j++)WinCoeff[j] = pow(WinCoeff[j], Beta);
  }

 else // Error.
  {
   // ShowMessage("Incorrect window type in WindowFFTData");
   delete[] WinCoeff;
   return;
  }

 // Fold the coefficients over.
 for(j=0; j<N/2; j++)WinCoeff[N-j-1] = WinCoeff[j];

 // Apply the window to the FIR coefficients.
 for(j=0; j<N; j++)FIRCoeff[j] *= WinCoeff[j];

 delete[] WinCoeff;

}

//---------------------------------------------------------------------------

// This function is used to correct the corner frequency values on FIR filters.
// We normally specify the 3 dB frequencies when specifing a filter. The Parks McClellan routine
// uses OmegaC and BW to set the 0 dB band edges, so its final OmegaC and BW values are not close
// to -3 dB. The Rectangular Windowed filters are better for high tap counts, but for low tap counts,
// their 3 dB frequencies are also well off the mark.

// To use this function, first calculate a set of FIR coefficients, then pass them here, along with
// OmegaC and BW. This calculates a corrected OmegaC for low and high pass filters. It calcultes a
// corrected BW for band pass and notch filters. Use these corrected values to recalculate the FIR filter.

// The Goertzel algorithm is used to calculate the filter's magnitude response at the single
// frequency defined in the loop. We start in the pass band and work out to the -20dB freq.

void FIRFreqError(double *Coeff, int NumTaps, int PassType, double *OmegaC, double *BW)
{
 int j, J3dB, CenterJ;
 double Omega, CorrectedOmega, CorrectedBW, Omega1, Omega2, Mag;

 // In these loops, we break at -20 dB to ensure that large ripple is ignored.
 if(PassType == LOWPASS)
  {
   J3dB = 10;
   for(j=0; j<NUM_FREQ_ERR_PTS; j++)
	{
     Omega = (double)j / dNUM_FREQ_ERR_PTS;
     Mag = Goertzel(Coeff, NumTaps, Omega);
	 if(Mag > 0.707)J3dB = j;   // J3dB will be the last j where the response was > -3 dB
	 if(Mag < 0.1)break;        // Stop when the response is down to -20 dB.
	}
   Omega = (double)J3dB / dNUM_FREQ_ERR_PTS;
  }

 else if(PassType == HIGHPASS)
  {
   J3dB = NUM_FREQ_ERR_PTS - 10;
   for(j=NUM_FREQ_ERR_PTS-1; j>=0; j--)
	{
     Omega = (double)j / dNUM_FREQ_ERR_PTS;
     Mag = Goertzel(Coeff, NumTaps, Omega);
	 if(Mag > 0.707)J3dB = j;  // J3dB will be the last j where the response was > -3 dB
	 if(Mag < 0.1)break;       // Stop when the response is down to -20 dB.
	}
   Omega = (double)J3dB / dNUM_FREQ_ERR_PTS;
  }

 else if(PassType == BANDPASS)
  {
   CenterJ = (int)(dNUM_FREQ_ERR_PTS * *OmegaC);
   J3dB = CenterJ;
   for(j=CenterJ; j>=0; j--)
	{
     Omega = (double)j / dNUM_FREQ_ERR_PTS;
     Mag = Goertzel(Coeff, NumTaps, Omega);
	 if(Mag > 0.707)J3dB = j;
	 if(Mag < 0.1)break;
	}
   Omega1 = (double)J3dB / dNUM_FREQ_ERR_PTS;

   J3dB = CenterJ;
   for(j=CenterJ; j<NUM_FREQ_ERR_PTS; j++)
	{
     Omega = (double)j / dNUM_FREQ_ERR_PTS;
     Mag = Goertzel(Coeff, NumTaps, Omega);
	 if(Mag > 0.707)J3dB = j;
	 if(Mag < 0.1)break;
	}
   Omega2 = (double)J3dB / dNUM_FREQ_ERR_PTS;
  }

 // The code above starts in the pass band. This starts in the stop band.
 else // PassType == firNOTCH
  {
   CenterJ = (int)(dNUM_FREQ_ERR_PTS * *OmegaC);
   J3dB = CenterJ;
   for(j=CenterJ; j>=0; j--)
	{
     Omega = (double)j / dNUM_FREQ_ERR_PTS;
     Mag = Goertzel(Coeff, NumTaps, Omega);
	 if(Mag <= 0.707)J3dB = j;
	 if(Mag > 0.99)break;
	}
   Omega1 = (double)J3dB/dNUM_FREQ_ERR_PTS;

   J3dB = CenterJ;
   for(j=CenterJ; j<NUM_FREQ_ERR_PTS; j++)
	{
     Omega = (double)j / dNUM_FREQ_ERR_PTS;
     Mag = Goertzel(Coeff, NumTaps, Omega);
	 if(Mag <= 0.707)J3dB = j;
	 if(Mag > 0.99)break;
	}
   Omega2 = (double)J3dB / dNUM_FREQ_ERR_PTS;
  }


 // This calculates the corrected OmegaC and BW and error checks the values.
 if(PassType == LOWPASS || PassType == HIGHPASS )
  {
   CorrectedOmega = *OmegaC * 2.0 - Omega;  // This is usually OK.
   if(CorrectedOmega < 0.001)CorrectedOmega = 0.001;
   if(CorrectedOmega > 0.99)CorrectedOmega = 0.99;
   *OmegaC = CorrectedOmega;
  }

 else // PassType == firBPF || PassType == firNOTCH
  {
   CorrectedBW = *BW * 2.0 - (Omega2 - Omega1);  // This routinely goes neg with Notch.
   if(CorrectedBW < 0.01)CorrectedBW = 0.01;
   if(CorrectedBW > *BW * 2.0)CorrectedBW = *BW * 2.0;
   if(CorrectedBW > 0.98)CorrectedBW = 0.98;
   *BW = CorrectedBW;
  }

}

/*static double window(int type,int n,int i,double beta)
{
    int k;
    double pi,w;

    pi=4.0*atan(1.0);
    w=1.0;
    switch(type)
    {
        case RETANGULAR:
        	w=1.0;
        	break;
        case TAPERED_RETANGULAR:
        	 k=(n-2)/10;
        	 if(i<=k)
        	 	w=0.5*(1.0-cos(i*pi/(k+1)));
                if(i>n-k-2)
        		w=0.5*(1.0-cos((n-i-1)*pi/(k+1)));	
        	break;
        case TRIANGULAR:
        	  w=1.0-fabs(1.0-2*i/(n-1.0));
        	  break;
        case HANNING:
        	 w=0.5*(1.0-cos(2*i*pi/(n-1)));
        	 break;
        case HAMMING:
        	  w=0.54-0.46*cos(2*i*pi/(n-1));
        	  break;
        case BLACKMAN:
        	w=0.42-0.5*cos(2*i*pi/(n-1))+0.08*cos(4*i*pi/(n-1));
        	break;
        case KAISER:
        	 w=kaiser(i,n,beta);
        	  break;
        default: ;	  
    }
    return (w);
}

 
static double kaiser(int i,int n,double beta)
{
    double a,w,a2,b1,b2,beta1;

    b1=bessel0(beta);
    a=2.0*i/(double)(n-1)-1.0;
    a2=a*a;
    beta1=beta*sqrt(1.0-a2);
    b2=bessel0(beta1);
    w=b2/b1;
    return (w);
}

static double bessel0(double x )
{
    int i;
    double d,y,d2,sum;

    y=x/2.0;
    d=1.0;
    sum=1.0;
    for(i=1;i<=25;i++)
    {
        d=d*y/i;
        d2=d*d;
        sum=sum+d2;
        if(d2<sum*(1.0e-8))
        	break;
    }
    return (sum);
}

void firwin(double* h, int n,int band,double fln,double fhn,int wn)
{
    int i,n2,mid;
    double s,pi,wc1,wc2,beta,delay;

    beta=0;
    if(wn==7)
    {
        printf("input beta parameter of Kaiser window ( 3 <beta  <10) \n");
        scanf("%lf",&beta);

    }
    pi=4.0*atan(1.0);
     if((n%2)==0)
     	{
            n2=n/2-1;
            mid=1;
     	}
     else
     	{
            n2=n/2;
    	    mid=0;
     	}
     delay=n/2.0;	
     wc1=2.0*pi*fln;
     if(band>=3)
     	wc2=2.0*pi*fhn;
    switch(band)
    {
        case LOWPASS: 
        	    for(i=0;i<=n2;i++)
                {
                    s=i-delay;
                    h[i]=(sin(wc1*s)/(pi*s))*window(wn,n+1,i,beta);
                    h[n-i]=h[i];	
                }
                if(mid==1)
    			h[n/2]=wc1/pi;
        			
                break;
        case HIGHPASS:
                for(i=0;i<=n2;i++)
        	   	{
            	   	s=i-delay;
            		h[i]=(sin(pi*s)-sin(wc1*s))/(pi*s);
            		h[i]=h[i]*window(wn,n+1,i,beta);
            		h[n-i]=h[i];
        	   	}
        	   if(mid==1)
        	   	  h[n/2]=1.0-wc1/pi;
        	   break;
        case BANDPASS: 
        	    for(i=0;i<=n2;i++)
        	   	{
            	   	s=i-delay;
            		h[i]=(sin(wc2*s)-sin(wc1*s))/(pi*s);
            		h[i]=h[i]*window(wn,n+1,i,beta);
            		h[n-i]=h[i];
        	   	}
        	   if(mid==1)
        	   	   h[n/2]=(wc2-wc1)/pi;
        	   break;
        case BANDSTOP: 
        	    for(i=0;i<=n2;i++)
        	    {
                    s=i-delay;
                    h[i]=(sin(wc1*s)+sin(pi*s)-sin(wc2*s))/(pi*s);
                    h[i]=h[i]*window(wn,n+1,i,beta);
                    h[n-i]=h[i];
        	    }
        		if(mid==1)
        			h[n/2]=(wc1+pi-wc2)/pi;
        		break;
        default: ;		
    }
}*/

plotFir::plotFir(void)
{
}


plotFir::~plotFir(void)
{
}


void plotFir::setFirParameter(bool enable, int N, int filterType, double fc, double BW)
{
    double OmegaC; 

    if(enable)
	{
		
       OmegaC=2*fc/SampleRate;
		nTap=N;
		//OmegaC=0.2;
		RectWinFIR(FirCoefs, nTap, filterType, OmegaC, BW);
		FIRFilterWindow(FirCoefs, nTap, wtKAISER, 8.0); // Use a window with RectWinFIR.
		FIRFreqError(FirCoefs, nTap, filterType, &OmegaC, &BW);
		RectWinFIR(FirCoefs, nTap, filterType, OmegaC, BW);
		FIRFilterWindow(FirCoefs, nTap, wtKAISER, 8.0); // Use a window with RectWinFIR.

    }else{
		
		for(int i=0;i<1024;i++) FirCoefs[i]=0.0;
		FirCoefs[0]=1.0;

	}
}

void plotFir::getCoef(double *coef){

	for(int i=0;i<nTap;i++){
		coef[i]=FirCoefs[i];
	}
}
void plotFir::plotCurve(double *x, double *y, long pointslength){

    //double W = 2*PI/nLen; // W = 2*PI/N

    //double W = PI/nLen; // W = PI/N
    //static double pha_cur = 0;
    //static double pha_prev = 0;
    //static int cycle = 0;

    #if 0 // phase output
	double pha_cur = 0;
    double pha_prev = 0;
    int cycle = 0;
    #endif
    
    


    double Real;
    double Image;
    for(int k=0; k<pointslength; k++)
    {    
        Real = 0;
        Image = 0;
        for(int n=0; n<nTap; n++)
        {
            Real += FirCoefs[n]*cos(2*M_PI*x[k]/SampleRate*n);
            Image += FirCoefs[n]*sin(2*M_PI*x[k]/SampleRate*n);        
        }

        //y[k] = 20*log10(sqrt(Real*Real+Image*Image));
		 y[k] = 10*log10(Real*Real+Image*Image); //save one sqrt
        #if 0
        pha_cur = atan2(Real, Image);
        if(pha_cur - pha_prev >= PI)
        {
        	cycle --;
        }
        if (pha_prev - pha_cur >= PI)
        {
        	cycle ++;
        }
        pPR[k] = (pha_cur + 2 * PI * cycle)*180/PI;
        pha_prev = pha_cur;
        #endif
     
    }
 
}



void plotFir::plotCurve(double *coef,int N,double *x, double *y, long pointslength){

    //double W = 2*PI/nLen; // W = 2*PI/N

    //double W = PI/nLen; // W = PI/N
    //static double pha_cur = 0;
    //static double pha_prev = 0;
    //static int cycle = 0;

    #if 0 // phase output
	double pha_cur = 0;
    double pha_prev = 0;
    int cycle = 0;
    #endif
    
    


    double Real;
    double Image;
    for(int k=0; k<pointslength; k++)
    {    
        Real = 0;
        Image = 0;
        for(int n=0; n<N; n++)
        {
            Real += coef[n]*cos(2*M_PI*x[k]/SampleRate*n);
            Image += coef[n]*sin(2*M_PI*x[k]/SampleRate*n);        
        }

        //y[k] = 20*log10(sqrt(Real*Real+Image*Image));
		 y[k] = 10*log10(Real*Real+Image*Image); //save one sqrt
        #if 0
        pha_cur = atan2(Real, Image);
        if(pha_cur - pha_prev >= PI)
        {
        	cycle --;
        }
        if (pha_prev - pha_cur >= PI)
        {
        	cycle ++;
        }
        pPR[k] = (pha_cur + 2 * PI * cycle)*180/PI;
        pha_prev = pha_cur;
        #endif
     
    }
 
}
