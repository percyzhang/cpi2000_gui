#include "qtelnetperso.h"

#include <QtDebug>

QTelnetPerso::QTelnetPerso(QObject *parent) : QObject(parent), socket(0), notifier(0), connected(false)
{
    setSocket(new QTcpSocket(this));
}

QTelnetPerso::~QTelnetPerso()
{
    delete socket;
    delete notifier;
}

void QTelnetPerso::setSocket(QTcpSocket *s)
{
    delete socket;

    connected = false;
    socket = s;
    if (socket)
    {
        connect(socket, SIGNAL(connected()), this, SLOT(socketConnected()));
        connect(socket, SIGNAL(disconnected()), this, SLOT(socketConnectionClosed()));
        connect(socket, SIGNAL(readyRead()), this, SLOT(socketReadyRead()));
        connect(socket, SIGNAL(error(QAbstractSocket::SocketError)), this, SLOT(socketError(QAbstractSocket::SocketError)));
    }
}

void QTelnetPerso::connectToHost(const QString &host, quint16 port)
{
    if (connected)
        return;
    socket->connectToHost(host, port);
}

void QTelnetPerso::disconnectTelnet()
{
    socket->disconnectFromHost();
}

bool QTelnetPerso::isConnected() const
{
    return connected;
}

void QTelnetPerso::consume()
{
    const QByteArray data = buffer;
    buffer.clear();
    int currpos = 0;
    int prevpos = -1;;
    while (prevpos < currpos && currpos < data.size())
    {
        prevpos = currpos;
        const uchar c = uchar(data[currpos]);
        if (c == Common::DM)
            ++currpos;
        else if (c == Common::IAC)
            currpos += 3;//parseIAC(data.mid(currpos));
        else // Assume plain text
            currpos += parsePlaintext(data.mid(currpos));
    }
    if (currpos < data.size())
        buffer.push_back(data.mid(currpos));
}

void QTelnetPerso::socketConnected()
{
    delete notifier;

    connected = true;
    notifier = new QSocketNotifier(socket->socketDescriptor(), QSocketNotifier::Exception, this);
    connect(notifier, SIGNAL(activated(int)), this, SLOT(socketException(int)));
    emit sockConnected();
    //sendOptions();
}

void QTelnetPerso::socketConnectionClosed()
{
    delete notifier;
    notifier = 0;
    connected = false;
    emit sockDisconnected();
}

void QTelnetPerso::socketReadyRead()
{
    buffer.append(socket->readAll());
    consume();
}

void QTelnetPerso::socketError(QAbstractSocket::SocketError /*error*/)
{
    //emit q->connectionError(error);
}

void QTelnetPerso::socketException(int)
{
    qDebug("socketException :: out-of-band data received, should handle that here!");
}

int QTelnetPerso::parsePlaintext(const QByteArray &data)
{
    int consumed = 0;
    int length = data.indexOf('\0');
    if (length == -1) {
        length = data.size();
        consumed = length;
    } else {
        consumed = length + 1; // + 1 for removing '\0'
    }

    const QString text = QString::fromLocal8Bit(data.constData(), length);

    if (!text.isEmpty())
        emit message(text);

    return consumed;
}

void QTelnetPerso::sendData(const QString &data)
{
    if (!connected)
        return;

    QByteArray str = data.toLocal8Bit();

    if (str.size()>0)
        socket->write(str);
}

void QTelnetPerso::sendData2(const QByteArray &data)
{
    if (!connected)
        return;

    if (data.size()>0)
        socket->write(data);
}


BlockedTelnet::BlockedTelnet(QObject *parent): QTelnetPerso(parent)
{

}
	
bool BlockedTelnet::connectToHost(const QString &host, quint16 port, int msInWait)
{
	m_bConnectedFlag = false;
    QTelnetPerso::connectToHost(host, port);
    connect(this, SIGNAL(sockConnected()), this, SLOT(onSockConnected()));

	unsigned int timeInMs;
	timerLoad(&timeInMs);

	for (;;)
	{
		if (m_bConnectedFlag == true)
		{
			return (true);
		}

		if (timerExpired(&timeInMs, msInWait, false))
		{
			disconnectHost();
			return (false);
		}

		msSleep(10);
	}
}

bool BlockedTelnet::waitFor(QString strWaitMsg, int msInWait)
{
	unsigned int timeInMs;
	timerLoad(&timeInMs);

	for (;;)
	{
		if (m_bConnectedFlag == false)
		{
			return (false);
		}

		if (m_receivedMsg.indexOf(strWaitMsg)!= -1)
		{
			return (true);
		}
		
		if (timerExpired(&timeInMs, msInWait, false))
		{
//			disconnectHost();
			return (false);
		}

		msSleep(10);
	}
}


void BlockedTelnet::onSockConnected()
{
    connect(this, SIGNAL(message(const QString &)), this, SLOT(processIncomingData(const QString &)));
//    connect(this, SIGNAL(sockDisconnected()), this, SLOT(telnetDisconnected()));
	m_bConnectedFlag = true;
	m_receivedMsg.clear();
    emit connected();
}

void BlockedTelnet::disconnectHost()
{
	m_bConnectedFlag = false;
	disconnect();
	disconnectTelnet();
}

void BlockedTelnet::onSockDisconnected()
{
	m_bConnectedFlag = false;
	disconnect();
	disconnectTelnet();
}

void BlockedTelnet::sendData(const QString &data)
{
	m_receivedMsg.clear();
	QTelnetPerso::sendData(data);
}

void BlockedTelnet::sendData2(const QByteArray &data)
{
	m_receivedMsg.clear();
	QTelnetPerso::sendData2(data);
}

void BlockedTelnet::processIncomingData(const QString & newMsg)
{
	m_receivedMsg.append(newMsg);
	qDebug() << "recv: " << newMsg;
}